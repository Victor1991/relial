<?php  
 // no direct access 
 defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" /> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="http://relial.org/templates/naumann/css/960.css" rel="stylesheet" />
  <link href="http://relial.org/templates/naumann/css/reset.css" rel="stylesheet" />
  <link href="http://relial.org/templates/naumann/css/css_style.css" rel="stylesheet" />
  
   <script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
    <script>
    $(document).ready(function(){
    
      // set the wrapper width and height to match the img size
      $('#wrapper').css({'width':$('#wrapper img').width(),
                'height':$('#wrapper img').height()
      })
      
      //tooltip direction
      var tooltipDirection;
             
      for (i=0; i<$(".pin").length; i++)
      {        
        // set tooltip direction type - up or down             
        if ($(".pin").eq(i).hasClass('pin-down')) {
          tooltipDirection = 'tooltip-down';
        } else {
          tooltipDirection = 'tooltip-up';
          }
      
        // append the tooltip
        $("#wrapper").append("<div style='left:"+$(".pin").eq(i).data('xpos')+"px;top:"+$(".pin").eq(i).data('ypos')+"px' class='" + tooltipDirection +"'>\
                          <div class='tooltip'>" + $(".pin").eq(i).html() + "</div>\
                      </div>");
      }    
      
      // show/hide the tooltip
      $('.tooltip-up, .tooltip-down').mouseenter(function(){
            $(this).children('.tooltip').fadeIn(100);
          }).mouseleave(function(){
            $(this).children('.tooltip').fadeOut(100);
          })
    });
    </script>
</head>

<body>
  
<div class="beige">
<div class="container_12">

<div id="logo" class="grid_8">
<jdoc:include type="modules" name="logo" style="xhtml" />
</div>

  
<div id="search" class="grid_3">
<jdoc:include type="modules" name="search" style="xhtml" />
</div>


<div id="social" class="grid_1">
<jdoc:include type="modules" name="social" style="xhtml" />
</div>



<div id="menu" class="grid_12">
<jdoc:include type="modules" name="menu" style="xhtml" />
</div>
 </div>
<div class="container_12">

<div class="grid_8">
<div id="slide" >
<jdoc:include type="modules" name="slide" style="xhtml" />
</div>
</div>

<div id="featureright" class="grid_4">
<jdoc:include type="modules" name="featureright" style="xhtml" />
</div>

<div class="grid_12">
<div id="bread" >
<jdoc:include type="modules" name="bread" style="xhtml" />
</div>
</div>

<div class="grid_8">
 <div id="content">
   <jdoc:include type="message" /> <jdoc:include type="component" />
</div>
</div>


<div class="grid_4">
<div id="right" >
<jdoc:include type="modules" name="right" style="xhtml" />
</div>
</div>

<div class="clear"></div></div>
  <div id="iniciologos">
   <div class="container_12"> 
<div class="grid_12">

<jdoc:include type="modules" name="iniciologos" style="xhtml" />
</div>
</div>
</div>
  


<div id="footerouter">

<div class="container_12">

<div id="footerleft" class="grid_1">
 <jdoc:include type="modules" name="footerleft" style="xhtml" />
 
 </div>

<div id="footercenter" class="grid_7">
<jdoc:include type="modules" name="footercenter" style="xhtml" />
</div>
  
  
  
<div id="footerright" class="grid_3">
<jdoc:include type="modules" name="footerright" style="xhtml" />
</div>

</div>
</div>
</div>


</body>
</html>
