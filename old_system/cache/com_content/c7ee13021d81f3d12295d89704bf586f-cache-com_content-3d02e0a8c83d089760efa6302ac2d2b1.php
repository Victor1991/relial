<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:3127:"<div class="item-page">









<h1>RELIAL: Potenciando la Sinergia entre Think Tanks y Partidos Políticos</h1>
<p>La Red Liberal de América Latina, RELIAL, llevará a cabo su VII Congreso, esta vez en Buenos Aires, Argentina.</p>
<p>&nbsp;</p>
<p>En este Congreso, RELIAL invitará a sus miembros a discutir propuestas que fortalezcan los lazos de cooperación entre los think tanks y partidos políticos alrededor de los temas estratégicos que promueve la Red. Encontramos que la sinergia, entre intelectuales, académicos, hacedores de política pública y representantes de la sociedad civil, es la manera que permitirá encaminar proyectos liberales para el progreso de la región.</p>
<p>&nbsp;</p>
<p>PROGRAMA<br />VII CONGRESO DE RELIAL <br />(con invitación)</p>
<p>&nbsp;</p>
<p>Jueves 11 de abril<br />Inauguración del VII Congreso de RELIAL <br />Sesión I: ¿Cómo formar la agenda política? <br />El rol y la incidencia de los think tanks en la política de sus países<br /> </p>
<p>Sesión II: ¿Cómo establecer y promover políticas liberales? Retos y necesidades para el posicionamiento de los partidos políticos</p>
<p>&nbsp;</p>
<p>Viernes 12 de abril<br />Sesión III – parte 1: Experiencias, ideas y propuestas<br />¿Qué modelos de cooperación entre think tanks y partidos políticos hay?<br />Sesión III – parte 2: Presentación de grupo de trabajo think tanks y partidos RELIAL<br />“Posiciones Liberales en Política Económica para América Latina”<br />Sesión IV: Hora institucional y cierre</p>
<p>&nbsp;</p>
<p>Puede solicitar más información a:<br /><span id="cloak93044">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak93044').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy93044 = 's&#105;lv&#105;&#97;.m&#101;rc&#97;d&#111;' + '&#64;';
 addy93044 = addy93044 + 'fnst' + '&#46;' + '&#111;rg';
 var addy_text93044 = 's&#105;lv&#105;&#97;.m&#101;rc&#97;d&#111;' + '&#64;' + 'fnst' + '&#46;' + '&#111;rg';
 document.getElementById('cloak93044').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy93044 + '\'>'+addy_text93044+'<\/a>';
 //-->
 </script><br /><span id="cloak14227">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak14227').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy14227 = 'm&#105;g&#117;&#101;l.&#97;ng&#101;l.t&#111;rr&#101;s' + '&#64;';
 addy14227 = addy14227 + 'fnst' + '&#46;' + '&#111;rg';
 var addy_text14227 = 'm&#105;g&#117;&#101;l.&#97;ng&#101;l.t&#111;rr&#101;s' + '&#64;' + 'fnst' + '&#46;' + '&#111;rg';
 document.getElementById('cloak14227').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy14227 + '\'>'+addy_text14227+'<\/a>';
 //-->
 </script></p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:21:"Feature News - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"Feature News";s:4:"link";s:20:"index.php?Itemid=146";}}s:6:"module";a:0:{}}