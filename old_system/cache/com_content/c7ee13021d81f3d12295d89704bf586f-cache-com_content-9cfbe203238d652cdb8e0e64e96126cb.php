<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:2117:"<div class="item-page">









<h1>Medio Ambiente&nbsp;</h1>
<p><em>Protegiendo la naturaleza, de la mano del mercado</em></p>
<p>&nbsp;</p>
<p><br />Como liberales de RELIAL promovemos un desarrollo sostenible que combine desarrollo económico con el propósito de mejorar la calidad de vida. Sin crecimiento económico no se pueden desarrollar tecnologías limpias. Abrir la puerta al liberalismo en la política ambiental es preferir soluciones privadas por encima de otras. RELIAL formula una agenda ambiental basada en la propiedad privada y los siguientes instrumentos ambientales de mercado:</p>
<p>&nbsp;</p>
<ul>
<li>Impulsar una regulación inteligente de los bienes públicos a través de la cual la gente modere y cambie sus preferencias. Hacemos énfasis en incentivos por encima de reglas coercitivas.</li>
<li>Establecer sistemas de derechos de propiedad para regular y proteger bienes comunes de libre acceso.</li>
<li>Implementar y estimular el pago de servicios ambientales que están basados en pagar el costo de oportunidad al propietario por no disponer de la tierra como normalmente lo hace.</li>
<li>Alterar el sistema de precios por la vía fiscal e implementar un carbon tax a cambio de la eliminación de impuestos relacionados al sector productivo. Debe acompañarse del comercio de derechos de emisión.</li>
<li>Reducir y desacoplar los subsidios a combustibles fósiles que distorsionan el mercado y las señales del mercado y no benefician a la población que vive en pobreza.</li>
<li>Motivar iniciativas ambientales del sector privado y la producción sustentable de empresas.</li>
</ul>
<p>&nbsp;</p>
<p><br />Fomentando estas buenas prácticas no sólo contribuimos al cuidado medioambiental, sino sobre todo demostramos que las propuestas liberales para mitigar el cambio climático, por ejemplo, son las más pertinentes, sostenibles y reales; demostramos también que el enfoque liberal no es antagónico a las preocupaciones ambientales.</p>
<p>&nbsp;</p>
<p><img src="images/content/collage/politicas_ambientales.jpg" width="620" height="78" /></p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:42:"Políticas Ambientales de Mercado - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:4:"Ejes";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:33:"Políticas Ambientales de Mercado";s:4:"link";s:20:"index.php?Itemid=145";}}s:6:"module";a:0:{}}