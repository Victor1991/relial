<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:1638:"<div class="item-page">









<h1>Propiedad Privada</h1>
<p><em>Impulsando una cultura de propietarios</em></p>
<p>Para RELIAL la propiedad es fundamento base de la prosperidad y la libertad. La protección de la propiedad privada y la existencia de certeza jurídica para las poblaciones en América Latina son elementos clave en las propuestas e iniciativas que las organizaciones miembro de RELIAL impulsan para posicionar el tema en la agenda política de la región.</p>
<p>&nbsp;</p>
<p>Los miembros RELIAL promueven en sus países conceptos y mecanismos que, a través de marcos institucionales y jurídicos, logren garantizar el respeto de los derechos de propiedad con el fin de que éstos no puedan ser vulnerados por ninguna autoridad. El respeto a la propiedad privada constituye uno de los instrumentos más poderosos que limita el actuar desmedido del Estado.</p>
<p>&nbsp;</p>
<p>RELIAL centra su labor en la concientización de los pueblos en este tema; asumiendo que el reconocimiento social y la protección de la propiedad privada son la base bienestar de toda sociedad. <br />Los derechos de propiedad incluyentes contribuyen a la estabilidad de una sociedad, dando a los ciudadanos -sobre todo a quienes viven en pobreza- la seguridad y la confianza de que su propiedad será respetada. La propiedad es el fundamento para el uso de bienes o ideas, exige una actitud responsable, fomenta la iniciativa, la inversión y el comercio y, por ende, el crecimiento económico y el desarrollo.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img src="images/content/collage/prop.jpg" width="620" height="78" /></p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:26:"Propiedad Privada - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:4:"Ejes";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:17:"Propiedad Privada";s:4:"link";s:20:"index.php?Itemid=144";}}s:6:"module";a:0:{}}