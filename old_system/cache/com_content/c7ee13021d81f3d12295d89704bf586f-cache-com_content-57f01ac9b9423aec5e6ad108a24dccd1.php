<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17580:"<div class="blog">

	





<div class="items-leading">
			<div class="leading-0">
			





<h1>ALUMNI</h1>
<p class="p1">&nbsp;</p>
<p><strong>Este es el espacio de los ex becarios que, con apoyo del Proyecto RELIAL, participaron en seminarios de la IAF, la Academia Internacional de Liderazgo Theodor Heuss de la Fundación Friedrich Naumann para la Libertad. En la IAF, jóvenes líderes de distintos países de América Latina tienen la oportunidad de capacitarse en temas políticos así como de discutir temas internacionales relativos a la política, la economía, la ciencia y los medios de comunicación.</strong></p>
<p><strong><br /></strong></p>
<p class="p1"><a href="http://www.visit.fnst.org/Alumni/1344c15157i999/index.html">http://www.visit.fnst.org/Alumni/1344c15157i999/index.html</a>&nbsp;</p>
<p class="p1">&nbsp;</p>
<p class="p1">&nbsp;</p>
<p><strong>2014</strong></p>
<p class="p1">&nbsp;</p>
<div class="greybox"><img src="images/alumni/10.jpg" alt="" width="100" height="100" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Sergio Daga</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Bolivia&nbsp;</p>
<p class="p1">&nbsp;</p>
<p class="p1">Profiling Political Liberalism as an Effective Force for Progress</p>
<p class="p1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p class="p1">27.04-04.05.2014</p>
<p>&nbsp; &nbsp;</p>
<p>“El seminario sobre el liberalismo como una fuerza efectiva para el progreso me ha ayudado a reforzar los argumentos teóricos y empíricos en favor de la iniciativa privada, el orden espontáneo de los individuos y su capacidad creadora para lograr mejoras significativas para el progreso de los países en un sociedad donde el conocimiento se transmite de una manera cada vez más rápida.”</p>
<p class="p1">&nbsp;</p>
<p class="p1"><a href="https://www.facebook.com/sergio.d.merida" target="_blank"><img src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" style="border-style: solid; border-color: #000000;" /></a>&nbsp;<a href="https://twitter.com/sergiodm27" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;<a href="bo.linkedin.com/pub/sergio-daga/a/aa0/486/" target="_blank"><img src="images/alumni/linkedin.jpg" alt="twitter" width="30" height="30" /></a></p>
&nbsp;&nbsp;&nbsp;&nbsp;</div>
<p><strong><br /></strong></p>
<p><strong>2013</strong></p>
<p class="p1">&nbsp;</p>
<div class="greybox"><img src="images/alumni/4.png" alt="" width="100" height="100" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Gustavo Villasmil</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Venezuela&nbsp;</p>
<p class="p1">&nbsp;</p>
<p class="p1">Liderazgo político</p>
<p class="p1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p class="p1">16-23.06.2013</p>
<p>&nbsp; &nbsp;</p>
<p>“El seminario provee de herramientas prácticas destinadas a facilitar el posicionamiento de discurso liberal como "marca" (brand) en mercados políticos de países bajo intensa amenaza de los autoritarismos.”</p>
<p class="p1">&nbsp;</p>
<p class="p1"><a href="https://twitter.com/Gvillasmil99" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a></p>
&nbsp;&nbsp;&nbsp;&nbsp;</div>
<div class="greybox"><img src="images/alumni/5.png" alt="" width="100" height="100" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Edwin Iván Zarco Nieva</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Perú</p>
<p class="p1">&nbsp;</p>
<p class="p1">Climate Change and Resource Management – Is There a Liberal Approach?</p>
<p class="p1">&nbsp;</p>
<p class="p1">26.05 – 7.06.2013</p>
<p>&nbsp;&nbsp;</p>
<p class="p1">&nbsp;“Los conocimientos obtenidos en el seminario me han permitido disponer de una base teórica y técnica para poder emprender un programa de investigación sobre Cambio Climático en el Centro de Investigación que dirijo en el Perú. Además, los conceptos que vimos en la IAF serán difundidos en los debates académicos y técnicos que se organicen en el Perú en el marco de las acciones sobre el Cambio Climático.”</p>
<p class="p1">&nbsp;</p>
<p class="p1">&nbsp;<a href="https://www.facebook.com/edwin.zarco.7?fref=ts" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/edwinzarconiev" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;&nbsp;<a href="http://www.linkedin.com/profile/view?id=140844622&amp;trk=tab_pro" target="_blank"><img src="images/alumni/linkedin.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;</p>
</div>
<div class="greybox"><img src="images/alumni/6.png" alt="" width="100" height="100" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Angélica Polanco Guevara</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Colombia</p>
<p class="p1">&nbsp;</p>
<p class="p1">Gobierno Local y Sociedad Civil</p>
<p class="p1">&nbsp;</p>
<p class="p1">20-28.07.2013</p>
<p>&nbsp; &nbsp;</p>
<p class="p1">“El seminario de Gobierno Local y Sociedad Civil fue muy útil para fortalecer las posiciones y enfoques de las propuestas y recomendaciones que hace el Instituto, enmarcadas en el pensamiento liberal, desde una manera práctica.”</p>
<p class="p1">&nbsp;</p>
<a href="http://www.linkedin.com/home?trk=hb_tab_home_top" target="_blank"><img src="images/alumni/linkedin.jpg" alt="twitter" width="30" height="30" /></a></div>
<p>&nbsp;</p>
<p><strong>2012</strong></p>
<p class="p1">&nbsp;</p>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/alumni/1.png" alt="" width="100" height="100" /> <strong>Wenerd Alexis Ocampos&nbsp;</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Paraguay</p>
<p class="p1">&nbsp;</p>
<p class="p1">Internacional Politics and Civil Society: The role onf NGOs an Political Parties</p>
<p class="p1">&nbsp;</p>
<p class="p1">7-19.10.2012</p>
<p class="p1">&nbsp;</p>
<p class="p1">“Este Seminario ha repercutido positivamente en mi vida profesional. Desde el punto de vista técnico ha abierto una visión mucho más amplia y positiva sobre la realidad política de mi país PARAGUAY y de otros países.”</p>
<br /><a href="https://www.facebook.com/wenerdalexis?fref=ts" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/LiberAlexis" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;&nbsp;</div>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/alumni/2.png" alt="" width="100" height="100" /> <strong>Lisbeth Prieto&nbsp;</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Venezuela</p>
<p class="p1">&nbsp;</p>
<p class="p1">Liberalism Today - Freedom First</p>
<p class="p1">&nbsp;</p>
<p class="p1">15-27.04.2013</p>
<p class="p1">&nbsp;</p>
<p class="p1">“El seminario dio una visión más internacional de muchos problemas públicos gracias a la interacción con personas de varias partes del mundo en particular de Asia, hoy como profesional me siento más segura al abordar temas que, en Venezuela, siguen siendo parte de la opinión pública.”</p>
<br /><a href="https://www.facebook.com/lisbprieto" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/lisbprieto" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;</div>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/alumni/3.png" alt="" width="100" height="100" /> <strong>Yuraima Becerra Rivas&nbsp;</strong>
<p class="p1">&nbsp;</p>
<p class="p1">Venezuela</p>
<p class="p1">&nbsp;</p>
<p class="p1">Fortalecimiento de Organizaciones Políticas Juveniles</p>
<p class="p1">&nbsp;</p>
<p class="p1">24.11-7.12.2012</p>
<p class="p1">&nbsp;</p>
<p class="p1">“El seminario fue muy enriquecedor a nivel personal y profesional: pude conocer a fondo otras culturas, hacer nuevos amigos y establecer una red de contactos en 4 continentes, tanto para difundir el mensaje de mi organización, como para tomar nuevas ideas que me ayuden seguir trabajando en la erradicación de la discriminación en Venezuela. Lo aprendido en la IAF es utilizado hoy en día durante la elaboración de proyectos y dictado de talleres en la Iniciativa Venezolana contra la Discriminación.”</p>
<br /><a href="http://www.facebook.com/YuritaBecerra" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/yuritabecerra" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;&nbsp;<img src="images/alumni/linkedin.jpg" alt="twitter" width="30" height="30" /></div>
<div class="greybox">
<p><img src="images/alumni/foto Xavier.JPG" alt="" width="100" height="100" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Xavier José Andrade Espinoza&nbsp;</strong></p>
<p class="p1">&nbsp;</p>
<p class="p1">Ecuador</p>
<p class="p1">&nbsp;</p>
<p class="p1">El liberalismo hoy: En primer lugar, la libertad</p>
<p class="p1">&nbsp;</p>
<p class="p1">15-27.04.2012</p>
<p class="p1">&nbsp;</p>
<p class="p1">“Dentro de mi rol de académico (docente universitario) y de director de desarrollo institucional de un think tank en Ecuador (IEEP) el seminario “Liberalismo Hoy-Primero la Libertad” repercutió de gran manera en mis actividades profesionales, al proporcionarme mejores herramientas de discusión y al tratar de encontrar un punto de encuentro entra las ideas liberales y su aplicación en la política.”</p>
<br /><a href="https://www.facebook.com/xavier.andradeespinoza" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/Xavierlibertas" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a>&nbsp;&nbsp;</div>
<div class="greybox">
<p><img src="images/alumni/Foto BGH.jpg" alt="" width="100" height="100" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Beltrán Gómez Híjar&nbsp;</strong></p>
<p class="p1">&nbsp;</p>
<p class="p1">Perú</p>
<p class="p1">&nbsp;</p>
<p class="p1">Cambio climático y getión de los recursos: Un consenso liberal?</p>
<p class="p1">&nbsp;</p>
<p class="p1">04-16.11.2012</p>
<p class="p1">&nbsp;</p>
<p class="p1">“El seminario ”Cambio Climático y Gestión de los Recursos” me ayudó a comprender el debate que sobre su existencia y origen se viene dando en el mundo. Asimismo, entender sus consecuencias y cómo el Estado puede enfrentarse a este hecho desde la aplicación de políticas públicas inspiradas en la filosofía de la libertad, me ha facilitado diseñar y proponer propuestas de solución políticas frente a este problema global.”</p>
<br /><a href="https://www.facebook.com/beltran.gomezhijar?fref=ts" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/Beltrangh" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a></div>
<div class="greybox">
<p><img src="images/alumni/foto.png" width="85" height="100" alt="foto" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Rocio Nuñez&nbsp;</strong></p>
<p class="p1">&nbsp;</p>
<p class="p1">Paraguay</p>
<p class="p1">&nbsp;</p>
<p class="p1">Politics and Civil Society</p>
<p class="p1">&nbsp;</p>
<p class="p1">7-18.10.2012</p>
<p class="p1">&nbsp;</p>
<p class="p1">“El seminario en mi vida profesional repercutió de una manera brillante, ya que pude ver cuestiones que no están normalmente a la vista. Las informaciones y los niveles de debate que surgen en la academia te animan a expresar tu opinión y luchar por que lo que creemos es la libertad, además de crear oportunidades para interactuar con colegas de todo el mundo y enriquecer así todos nuestros conceptos.”</p>
<br /><a href="https://www.facebook.com/Rorru.n?ref=tn_tnmn" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="https://twitter.com/loulinunez" target="_blank"><img src="images/alumni/twitter.jpg" alt="twitter" width="30" height="30" /></a></div>
<div class="greybox">
<p><img src="images/alumni/foto Luis Ceciliano1.png" width="85" height="100" alt="foto" style="margin: 0px 20px 0px 0px; float: left;" /><strong>Luis F. Ceciliano&nbsp;</strong></p>
<p class="p1">&nbsp;</p>
<p class="p1">Costa Rica</p>
<p class="p1">&nbsp;</p>
<p class="p1">NGO Leadership</p>
<p class="p1">&nbsp;</p>
<p class="p1">3-9.06.2012</p>
<p class="p1">&nbsp;</p>
<p class="p1">“Instrucción y herramientas de primer nivel acerca de cómo posicionar efectivamente una organización no gubernamental que sea capaz de promover los valores de la libertad, la responsabilidad individual, el Estado de derecho y la democracia ante la sociedad.”</p>
<br /><a href="https://www.facebook.com/profile.php?id=568515080" target="_blank"><img style="border: 0px solid #000000;" src="images/alumni/facebook.jpg" alt="facebook" width="30" height="30" /></a>&nbsp;&nbsp;<a href="http://www.linkedin.com/in/lfceciliano" target="_blank"><img src="images/alumni/linkedin.jpg" alt="twitter" width="30" height="30" /></a></div>
</div>


<div class="item-separator"></div>
		</div>
			</div>

			<div class="items-row cols-2 row-0">
		<div class="item column-1">
		





<h1>Junta Honorífica</h1>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/content/mesa/Mario%20Vargas%20Llosa.jpg" alt="" width="100" height="100" /> <a href="http://www.mvargasllosa.com/" target="_blank"><strong>Mario Vargas Llosa&nbsp;</strong></a></div>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/content/mesa/carlos.jpg" alt="" width="100" height="100" /> <a href="http://www.elblogdemontaner.com/" target="_blank"><strong>Carlos Alberto Montaner&nbsp;</strong></a><br /><br /></div>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/content/mesa/carlos_sabino.jpg" alt="" width="100" height="100" /> <strong><a href="http://paginas.ufm.edu/sabino/" target="_blank">Carlos Sabino</a>&nbsp;</strong><br /><br /></div>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/content/mesa/plineo.jpg" alt="" width="100" height="100" /> <strong>Plinio Apuleyo Mendoza</strong><br /><br /></div>
<div class="greybox"><img style="margin: 0px 20px 0px 0px; float: left;" src="images/content/mesa/sergio.jpg" alt="" width="100" height="100" /> <a href="http://www.sergiosarmiento.com/" target="_blank"><strong>Sergio Sarmiento&nbsp;</strong></a><br /><br /></div>


<div class="item-separator"></div>
	</div>
					<div class="item column-2">
		





<p>oisnmdoiamdoisamdoimadas</p>
<p>sa</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>s</p>
<p>da</p>
<p>sd</p>
<p>as</p>
<p>d</p>
<p>sasad sadok sadk sadosa d</p>


<div class="item-separator"></div>
	</div>
						<span class="row-separator"></span>
				</div>

						<div class="items-row cols-2 row-1">
		<div class="item column-1">
		





<p>akakalala;lclkdok ;pp' ;poioujjjkk ;;;</p>


<div class="item-separator"></div>
	</div>
					<div class="item column-2">
		





<h1>Mathias</h1>
<p>&nbsp;</p>
<p><a href="http://www.google.com" target="_blank">sfsdf</a></p>
<p>&nbsp;</p>
<p><a href="index.php?option=com_content&amp;view=article&amp;id=15&amp;Itemid=140">sd</a></p>
<p>f</p>
<p><span style="font-family: 'comic sans ms', sans-serif;">sdf</span></p>
<p>s</p>
<p>dfsdf</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>asd</p>
<p>&nbsp;</p>
<p><img src="images/arrow_left.png" width="31" height="59" alt="arrow left" style="margin-right: 20px; border: 0px solid #000000; float: left;" />fhs f sdpfj sñi fsd jfsdf sapdojf sjf sjf sdjf ñsnf ñsjfñljds ñlfja j &nbsp;fhs f sdpfj sñi fsd jfsdf sapdojf sjf sjf sdjf ñsnf ñsjfñljds ñlfja j&nbsp;fhs f sdpfj sñi fsd jfsdf sapdojf sjf sjf sdjf ñsnf ñsjfñljds ñlfja j&nbsp;fhs f sdpfj sñi fsd jfsdf sapdojf sjf sjf sdjf ñsnf ñsjfñljds ñlfja jfhs f sdpfj sñi fsd jfsdf sapdojf sjf sjf sdjf ñsnf ñsjfñljds ñlfja j</p>


<div class="item-separator"></div>
	</div>
						<span class="row-separator"></span>
				</div>

				



	

<div class="items-more">

<h3>Más artículos...</h3>
<ol>
	<li>
		<a href="/index.php/productos/indices-y-analisis/2-uncategorised/23-miguel">
			Miguel</a>
	</li>
	<li>
		<a href="/index.php/aviso-legal-privacidad">
			Aviso Legal / Privacidad / Diseño Web</a>
	</li>
	<li>
		<a href="/index.php/productos/indices-y-analisis/2-uncategorised/21-nuestra-presencia-en-america-latina">
			Nuestra presencia en América Latina</a>
	</li>
	<li>
		<a href="/index.php/feature-news">
			RELIAL: Potenciando la Sinergia entre Think Tanks y Partidos Políticos</a>
	</li>
</ol>
</div>



	

</div>
";s:4:"head";a:10:{s:5:"title";s:28:"Indices y Análisis - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:80:"/index.php/productos/indices-y-analisis/2-uncategorised?format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:81:"/index.php/productos/indices-y-analisis/2-uncategorised?format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:8:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:582:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:19:"Indices y Análisis";s:4:"link";s:20:"index.php?Itemid=127";}i:2;O:8:"stdClass":2:{s:4:"name";s:13:"Uncategorised";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}