<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6541:"<div class="item-page">









<h1>Legal / Privacidad</h1>
<p>&nbsp;</p>
<p><strong>Legal</strong></p>
<p><strong></strong><br /> <br />Contacto</p>
<p>Fundación Friedrich Naumann para la Libertad<br />RELIAL Red Liberal de América Latina<br />Cerrada de la Cerca 82<br />Col. San Ángel Inn<br />01060 México, D.F.<br />Tel.: +52 55 5550 1039; 5616 3325<br />Fax: +52 55 5550 6223</p>
<p>Nombre de dominio<br />www.relial.org</p>
<p>RELIAL, La Red Liberal de América Latina representa la unión de organizaciones liberales más representativas de la región. Comprometidos con el progreso y el desarrollo de sus países, partidos políticos y centros de investigación difunden e implementan valores liberales. <br />RELIAL fue creada en el 2004 con el impulso de la Fundación Friedrich Naumann para la Libertad es una fundación política alemana sin fines de lucro, registrada ante la Secretaría de Relaciones Exteriores del Gobierno de los Estados Unidos Mexicanos.</p>
<p>E-Mail: <span id="cloak72778">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak72778').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy72778 = 'm&#101;x&#105;c&#111;' + '&#64;';
 addy72778 = addy72778 + 'fnst' + '&#46;' + '&#111;rg';
 var addy_text72778 = 'm&#101;x&#105;c&#111;' + '&#64;' + 'fnst' + '&#46;' + '&#111;rg';
 document.getElementById('cloak72778').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy72778 + '\'>'+addy_text72778+'<\/a>';
 //-->
 </script><br />Mail al Webmaster</p>
<p>Realización / <a href="http://www.scoomadesign.com" target="_blank">Diseño Web</a>: Scooma Design</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Privacidad</strong></p>
<p>Los usuarios, al entrar en contacto con RELIAL, La Red Liberal de América Latina que pertenece a la Fundación Friedrich Naumann para la Libertad, aceptan expresamente y de forma libre e inequívoca que sus datos personales sean tratados por parte del prestador para realizar la siguiente finalidad:</p>
<p>1. Tramitar encargos, solicitudes o cualquier tipo de petición que sea realizada por el usuario a través de cualquiera de las formas de contacto que se ponen a disposición del usuario en el sitio web de la compañía.</p>
<p>El prestador informa y garantiza expresamente a los usuarios que sus datos personales no serán cedidos en ningún caso a terceras compañías, y que siempre que fuera a realizarse algún tipo de cesión de datos personales, de forma previa, se solicitaría el consentimiento expreso, informado, e inequívoco por parte de los titulares.</p>
<p>Todos los datos solicitados a través del sitio web son obligatorios, ya que son necesarios para la prestación de un servicio óptimo al usuario. En caso de que no sean facilitados todos los datos, el prestador no garantiza que la información y servicios facilitados sean completamente ajustados a sus necesidades.</p>
<p>Del mismo modo, el prestador ha adoptado todas las medidas técnicas y de organización necesarias para garantizar la seguridad e integridad de los datos de carácter personal que trate, así como para evitar su pérdida, alteración y/o acceso por parte de terceros no autorizados.</p>
<p>Uso de Cookies y del Fichero de Actividad</p>
<p>El prestador por su propia cuenta o la de un tercero contratado para la prestación de servicios de medición, pueden utilizar cookies cuando un usuario navega por el sitio web. Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación.</p>
<p>Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario.</p>
<p><br />Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.</p>
<p>El usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.</p>
<p>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o "login".</p>
<p>Las cookies utilizadas en este sitio web tienen, en todo caso, carácter temporal con la única finalidad de hacer más eficaz su transmisión ulterior. En ningún caso se utilizarán las cookies para recoger información de carácter personal.</p>
<p>&nbsp;</p>
<p><strong>Direcciones IP</strong></p>
<p>Los servidores del sitio web podrán detectar de manera automática la dirección IP y el nombre de dominio utilizados por el usuario. Una dirección IP es un número asignado automáticamente a un ordenador cuando ésta se conecta a Internet. Toda esta información es registrada en un fichero de actividad del servidor debidamente inscrito que permite el posterior procesamiento de los datos con el fin de obtener mediciones únicamente estadísticas que permitan conocer el número de impresiones de páginas, el número de visitas realizadas a los servicios web, el orden de visitas, el punto de acceso, etc.</p>
<p>&nbsp;</p>
<p><strong>Seguridad</strong></p>
<p>El sitio web utiliza técnicas de seguridad de la información generalmente aceptadas en la industria, tales como firewalls, procedimientos de control de acceso y mecanismos criptográficos, todo ello con el objeto de evitar el acceso no autorizado a los datos. Para lograr estos fines, el usuario/cliente acepta que el prestador obtenga datos para efectos de la correspondiente autenticación de los controles de acceso.</p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:33:"Aviso Legal / Privacidad - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:24:"Aviso Legal / Privacidad";s:4:"link";s:20:"index.php?Itemid=154";}}s:6:"module";a:0:{}}