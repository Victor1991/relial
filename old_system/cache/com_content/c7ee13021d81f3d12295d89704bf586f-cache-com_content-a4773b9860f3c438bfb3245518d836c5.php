<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:1331:"<div class="item-page">









<h1>Democracia Liberal</h1>
<p><em>Defendiendo la alternancia, el pluralismo político y el Estado de derecho</em></p>
<p>&nbsp;</p>
<p><br />Para RELIAL, la democracia no sólo es el paso a la participación en el proceso político, es por sobre todo la garantía que debemos exigir para que esta participación conduzca a la promoción y consolidación de la libertad. La democracia debe promover la libertad y no engendrar tiranías; la democracia liberal proporciona esta garantía.</p>
<p>&nbsp;</p>
<p><br />Los liberales damos un paso más para entender la democracia, asumimos como compromiso abogar por la democracia liberal ante la cual el Estado de Derecho es la característica fundamental. Apoyamos incondicionalmente la regulación de elecciones y reales para la oposición, el sistema de monitoreos y balances que limiten el poder, el pluralismo y fuertes instituciones cívicas y la economía de mercado. Este es el marco de estos principios que une y vincula a los liberales de América Latina y que brinda a los think tanks y partidos políticos miembros de RELIAL los fundamentos esenciales para la formación de nuevos ciudadanos libres y responsables.</p>
<p>&nbsp;</p>
<p><img src="images/content/collage/democracialiberal.jpg" width="620" height="81" /></p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:27:"Democracia Liberal - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:4:"Ejes";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:18:"Democracia Liberal";s:4:"link";s:20:"index.php?Itemid=142";}}s:6:"module";a:0:{}}