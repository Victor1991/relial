<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10772:"<div class="item-page">









<h1>RELIAL</h1>
<p><strong>La Red Liberal de América Latina</strong>&nbsp;es la unión de organizaciones liberales más representativa de la región. Comprometidos con el progreso y el desarrollo de sus países, partidos políticos y&nbsp;centros de investigación (think tanks) difunden e implementan principios liberales asumiendo como bandera la defensa de la democracia, el respeto de los derechos humanos, la primacía del Estado de derecho y el fomento de la economía de mercado; valores propios de individuos responsables consigo mismos y con su sociedad. Conozca más y súmese a las acciones de <strong>RELIAL</strong>.</p>
<p>&nbsp;</p>
<h1>Actualidad y Opinión</h1>
<p>		<div class="moduletable">
					
<div id="k2ModuleBox90" class="k2ItemsBlock">

	
	  <ul>
        <li class="even">

      <!-- Plugins: BeforeDisplay -->
      
      <!-- K2 Plugins: K2BeforeDisplay -->
      
      
            <a class="moduleItemTitle" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">EDUCAR -además del Estado- SÍ ES POSIBLE</a>
      
      
      <!-- Plugins: AfterDisplayTitle -->
      
      <!-- K2 Plugins: K2AfterDisplayTitle -->
      
      <!-- Plugins: BeforeDisplayContent -->
      
      <!-- K2 Plugins: K2BeforeDisplayContent -->
      
            <div class="moduleItemIntrotext">
	      	      <a class="moduleItemImage" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible" title="K2_CONTINUE_READING &quot;EDUCAR -adem&aacute;s del Estado- S&Iacute; ES POSIBLE&quot;">
	      	<img src="/media/k2/items/cache/271073fa434dfbedecc5cddef10cff3e_XS.jpg" alt="EDUCAR -adem&aacute;s del Estado- S&Iacute; ES POSIBLE"/>
	      </a>
	      
      	      	<p>"La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una sociedad empoderada cuestiona y defiende sus derechos y libertades; no permite abusos ni se conforma con migajas".&nbsp;</p>
<p>&nbsp;</p>
<p>Un artículo de: Tatiana Macías Muentes, Ecuador.&nbsp;</p>
<p>&nbsp;</p>
      	      </div>
      
      
      <div class="clr"></div>

      
      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      
      <!-- K2 Plugins: K2AfterDisplayContent -->
      
      
      
      
      
			
			
						<a class="moduleItemReadMore" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
				leer más			</a>
			
      <!-- Plugins: AfterDisplay -->
      
      <!-- K2 Plugins: K2AfterDisplay -->
      
      <div class="clr"></div>
    </li>
        <li class="odd">

      <!-- Plugins: BeforeDisplay -->
      
      <!-- K2 Plugins: K2BeforeDisplay -->
      
      
            <a class="moduleItemTitle" href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">RELIAL alerta sobre democracia en Nicaragua</a>
      
      
      <!-- Plugins: AfterDisplayTitle -->
      
      <!-- K2 Plugins: K2AfterDisplayTitle -->
      
      <!-- Plugins: BeforeDisplayContent -->
      
      <!-- K2 Plugins: K2BeforeDisplayContent -->
      
            <div class="moduleItemIntrotext">
	      	      <a class="moduleItemImage" href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua" title="K2_CONTINUE_READING &quot;RELIAL alerta sobre democracia en Nicaragua&quot;">
	      	<img src="/media/k2/items/cache/8240a1907e29481b04619a0df33df9ab_XS.jpg" alt="RELIAL alerta sobre democracia en Nicaragua"/>
	      </a>
	      
      	      	<p><strong>Miembros de la Red Liberal de América Latina (RELIAL)</strong>, ante la decisión del Tribunal Electoral de Nicaragua de despojar a 28 diputados electos por el pueblo nicaragüense en el año 2011 de sus credenciales como miembros del Poder Legislativo de ese país, manifestamos nuestra indignación por estos hechos que violentan la decisión soberana de un pueblo que fue a las urnas y eligió a sus representantes ante el parlamento y preocupación por las actuaciones de las instituciones nicaragüenses que han orquestado la desaparición de la oposición con el propósito de dejar como único candidato presidencial al señor Daniel Ortega para el proceso electoral de noviembre del 2016.</p>
<p>&nbsp;</p>
<p>Reconocemos que la democracia y los derechos de los ciudadanos en Nicaragua han recibido un fuerte ataque y nos sumamos a aquellas personas de todo el mundo que hoy levantan la voz ante este golpe dado a los legisladores separados de sus cargos solo por representar los intereses de la población que no piensa como este gobierno.</p>      	      </div>
      
      
      <div class="clr"></div>

      
      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      
      <!-- K2 Plugins: K2AfterDisplayContent -->
      
      
      
      
      
			
			
			
      <!-- Plugins: AfterDisplay -->
      
      <!-- K2 Plugins: K2AfterDisplay -->
      
      <div class="clr"></div>
    </li>
        <li class="even">

      <!-- Plugins: BeforeDisplay -->
      
      <!-- K2 Plugins: K2BeforeDisplay -->
      
      
            <a class="moduleItemTitle" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">Olas de Cambio - tercer número de la Mirada Liberal</a>
      
      
      <!-- Plugins: AfterDisplayTitle -->
      
      <!-- K2 Plugins: K2AfterDisplayTitle -->
      
      <!-- Plugins: BeforeDisplayContent -->
      
      <!-- K2 Plugins: K2BeforeDisplayContent -->
      
            <div class="moduleItemIntrotext">
	      	      <a class="moduleItemImage" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal" title="K2_CONTINUE_READING &quot;Olas de Cambio - tercer n&uacute;mero de la Mirada Liberal&quot;">
	      	<img src="/media/k2/items/cache/e791ab626e6785062374d45b25cc6e7f_XS.jpg" alt="Olas de Cambio - tercer n&uacute;mero de la Mirada Liberal"/>
	      </a>
	      
      	      	<p>L<strong>a Mirada Liberal,</strong> revista de análisis y coyuntura política de América Latina, esta vez en su tercer número les ofrece artículos relacionadosa los procesos electorales de los dos últimos años.&nbsp;</p>
<p>&nbsp;</p>
<p>Quedan invitados a leer la reflexión de nuestros expertos <a href="images/Miradas/FNF%20RELIAL_MLiberal_OlasDC-digital_22jun-16.pdf" target="_blank"><strong>aquí</strong></a></p>      	      </div>
      
      
      <div class="clr"></div>

      
      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      
      <!-- K2 Plugins: K2AfterDisplayContent -->
      
      
      
      
      
			
			
			
      <!-- Plugins: AfterDisplay -->
      
      <!-- K2 Plugins: K2AfterDisplay -->
      
      <div class="clr"></div>
    </li>
        <li class="odd">

      <!-- Plugins: BeforeDisplay -->
      
      <!-- K2 Plugins: K2BeforeDisplay -->
      
      
            <a class="moduleItemTitle" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">Las elecciones peruanas del 2016</a>
      
      
      <!-- Plugins: AfterDisplayTitle -->
      
      <!-- K2 Plugins: K2AfterDisplayTitle -->
      
      <!-- Plugins: BeforeDisplayContent -->
      
      <!-- K2 Plugins: K2BeforeDisplayContent -->
      
            <div class="moduleItemIntrotext">
	      	      <a class="moduleItemImage" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016" title="K2_CONTINUE_READING &quot;Las elecciones peruanas del 2016&quot;">
	      	<img src="/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_XS.jpg" alt="Las elecciones peruanas del 2016"/>
	      </a>
	      
      	      	<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
      	      </div>
      
      
      <div class="clr"></div>

      
      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      
      <!-- K2 Plugins: K2AfterDisplayContent -->
      
      
      
      
      
			
			
						<a class="moduleItemReadMore" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
				leer más			</a>
			
      <!-- Plugins: AfterDisplay -->
      
      <!-- K2 Plugins: K2AfterDisplay -->
      
      <div class="clr"></div>
    </li>
        <li class="even lastItem">

      <!-- Plugins: BeforeDisplay -->
      
      <!-- K2 Plugins: K2BeforeDisplay -->
      
      
            <a class="moduleItemTitle" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">“No hay comida”: el fantasma del hambre se apodera de Venezuela</a>
      
      
      <!-- Plugins: AfterDisplayTitle -->
      
      <!-- K2 Plugins: K2AfterDisplayTitle -->
      
      <!-- Plugins: BeforeDisplayContent -->
      
      <!-- K2 Plugins: K2BeforeDisplayContent -->
      
            <div class="moduleItemIntrotext">
	      	      <a class="moduleItemImage" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela" title="K2_CONTINUE_READING &quot;&ldquo;No hay comida&rdquo;: el fantasma del hambre se apodera de Venezuela&quot;">
	      	<img src="/media/k2/items/cache/072519f74a95ea36f571d1e83f1c23bd_XS.jpg" alt="&ldquo;No hay comida&rdquo;: el fantasma del hambre se apodera de Venezuela"/>
	      </a>
	      
      	      	<p>¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo</p>
      	      </div>
      
      
      <div class="clr"></div>

      
      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      
      <!-- K2 Plugins: K2AfterDisplayContent -->
      
      
      
      
      
			
			
						<a class="moduleItemReadMore" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
				leer más			</a>
			
      <!-- Plugins: AfterDisplay -->
      
      <!-- K2 Plugins: K2AfterDisplay -->
      
      <div class="clr"></div>
    </li>
        <li class="clearList"></li>
  </ul>
  
		<a class="moduleCustomLink" href="/index.php/productos/archivo" title="Ir al archivo">Ir al archivo</a>
	
	
</div>
		</div>
	</p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:45:"RELIAL Red Liberal de America Latina - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}