<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:2171:"<div class="item-page">









<h1>Libertad Económica</h1>
<p><em></em>&nbsp;</p>
<p><em></em>&nbsp;</p>
<p><em>Una economía libre, una sociedad prospera</em></p>
<p><em></em>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Para RELIAL es prioritario&nbsp;el tema de la libertad económica, destacar su importancia para desarrollar una agenda de un alto crecimiento sostenible y para consolidar una sociedad libre.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>En cooperación con el <a href="http://www.fraserinstitute.org/" target="_blank">Instituto Fraser</a> de Canadá, anualmente las instituciones miembro de RELIAL contribuyen a la elaboración del Índice de Libertad Económica en el Mundo, analizan las repercusiones y presentan propuestas para generar políticas económicas de corte liberal para la región. El Índice compila la medición de toda una gama de políticas gubernamentales que influyen en el comportamiento económico y posibilitan que las personas con distintos valores políticos puedan conversar sobre la efectividad de las políticas que se basan en evidencia comparativa internacional y objetiva. Además, el documento pone a líderes políticos en posibilidad de elegir y decidir sobre las políticas asociadas al éxito económico.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El Índice analiza más de 40 variables, derivadas de elementos clave de la libertad económica, como lo son la elección personal, el intercambio voluntario, la libre entrada y competencia en los mercados y la protección de los derechos de las personas y sus propiedades.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El Índice de Libertad Económica en el Mundo, en su vida como publicación anual, ha demostrado en forma fehaciente, con hechos y correlaciones estadísticas, que existe una relación causal entre mayores calificaciones de libertad económica y mayores índices de crecimiento. Es decir, a más libertad, más prosperidad. Esta tesis es una realidad empíricamente demostrable.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img src="images/content/collage/libertadeconomica.jpg" width="620" height="100" /></p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:28:"Libertad Económica - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:4:"Ejes";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:19:"Libertad Económica";s:4:"link";s:20:"index.php?Itemid=143";}}s:6:"module";a:0:{}}