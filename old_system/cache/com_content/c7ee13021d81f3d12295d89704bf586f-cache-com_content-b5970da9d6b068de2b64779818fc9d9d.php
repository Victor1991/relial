<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:2210:"<div class="item-page">









<h1>Sobre RELIAL</h1>
<p>La Red Liberal de América Latina (RELIAL) es la unión de organizaciones liberales más representativa de la región.</p>
<p>Conformada por&nbsp; instituciones -think tanks y partidos políticos- asume como compromiso difundir e implementar principios y valores liberales tales como la Democracia, la Economía de Mercado, el Estado de Derecho y los Derechos Humanos y Civiles, así como fomentar una agenda temática liberal que dé respuesta a las diversas problemáticas que aquejan al continente, y en ese marco, fortalecer la cooperación y coordinación entre sus miembros.</p>
<p>&nbsp;</p>
<p>La máxima instancia para la toma de decisiones de RELIAL es la <a href="index.php/relial/mesa-directiva" target="_blank">Mesa Directiva</a>, elegida democráticamente por los miembros plenos, de acuerdo a un acta constitutiva y un reglamento interno.</p>
<p>Acompaña a las acciones de la Red, la Junta Honorífica, prestigioso concejo consultivo compuesto por destacados referentes de la intelectualidad latinoamericana.</p>
<p>&nbsp;</p>
<p>RELIAL fue creada en 2004 con el apoyo e impulso de la <a href="http://www.la.fnst.org/" target="_blank">Fundación Friedrich Naumann para la Libertad en América Latina</a>. Desde el 2008, RELIAL cuenta con personería jurídica como Fundación RELIAL, la cual le da la posibilidad de obtener aportes y financiamiento externo.</p>
<p>&nbsp;</p>
<p>Además de sus miembros latinoamericanos, RELIAL cuenta con cuatro instituciones cooperantes, son aquellas interesadas en el trabajo conjunto con la Red y que se encuentran fuera del ámbito regional (España, Gran Bretaña y Estados Unidos). RELIAL es institución cooperante de la Internacional Liberal.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="color: #c0c0c0;">Haga click en la imagen para descargar el folleto institucional</span></p>
<p>&nbsp;<a href="index.php/biblioteca/item/72-folleto-relial-2013"><img style="margin-right: auto; margin-left: auto; display: block;" alt="folleto gif para la web" src="images/fotos/folleto%20gif%20para%20la%20web.jpg" width="523" height="231" /> </a></p>
<p>&nbsp;</p>
<p>&nbsp;</p> 
	
</div>
";s:4:"head";a:10:{s:5:"title";s:15:"Relial - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:241:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';window.addEvent('load', function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}