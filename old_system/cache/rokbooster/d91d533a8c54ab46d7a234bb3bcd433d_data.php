













#sbox-overlay {
	position: absolute;
	background-color: #000;
	left: 0px;
	top: 0px;
}

#sbox-window {
	position: absolute;
	background-color: #fff;
	text-align: left;
	overflow: visible;
	padding: 10px;
	
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
}

#sbox-window[aria-hidden=true],
#sbox-overlay[aria-hidden=true] {
	display: none;
}

#sbox-btn-close {
	position: absolute;
	width: 30px;
	height: 30px;
	right: -15px;
	top: -15px;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAGKUlEQVR4XpVWbUxTVxhub29bWgsdBYEB8jVQCshEo9miAxIT+YpTRtDgZDMzwUQlccRICIrRzSyRmZgYNcZkZCb+mIkYwvzhDzMNAxTMUMIPNDIgClG+nJZ+3HJ7u+e9Obe5XSmMkzy9974973ner/Oeo1lmaAGOUFxczG/atEmfm5tryMzMNLa2tq66fPmyhb4J9B/NUeYz3bCDW4JQB/CAYd26dauys7Nt+/fvTwJSysrKknU6nZ3n+Vyz2Ry/ZcuW+J07d8anp6dHr1692kw6TFcXzgBtWFIAi+gLCgrMFRUVcevXr08BiqxWa6Fer/9MrSCK4lOHw9E/MDDQiSgM9vX1OQRB8E5PTy+Aw8fgX4qYY9CnpaUZN2/eHFVaWpqyZ8+e7y0Wy1dqxbdv30oej0eTmpoaFLUPHz78hgg0IRLC2NiY8927d16IyQCJEM57HjAnJydTWDM7Ojq+9Pl8Y34Mr9crXbt2zbtjxw4XFBxqwEDnhQsXvG63W2Jzn96/f78GeU9BhKJpTba2NhypKSoqylZSUpJ569atSvAJRHrv3j0Rns0TyVJAHTifPHkikg7T+xqyNEZuWoxcBxgBa35+fvr58+dLkbdxUm5ra/OikBjB8qC5586dExTyq1evViYmJq6htYlDXXD0owcssbGxiYcPH942Nzf3u+IpI10xrly54iViFF0PZDlAAnEwLo5TqhghNuTk5FhRGFnR0dEVLpdLOnjwoAc5DoRl48aN3NatW2Fx8IiLi9Nirh4FFZAdO3ZMmJycFFGUnz98+LAadRMJsUHtMZGvglJidXX1ttHR0V/J0kuXLnnVHuTl5TmV8J05c0ZQ5BkZGc6JiQmJ5Ci+IJ26ujoPyWdmZv6ALB/4mHnNKx7z2JsRTqfTEh8f/ylZc/PmTVHtFULmR9XKgpaWFgPIDVlZWVxXV5cJOdSyrRS0V2/fvi1SxGJiYoo/woAoAlC6m/wSA8K8Xbt27SMLsYBvsbyhkbgwFMdpnqS8t7e3LyxWD8+ePZMLrbGxsT4yMjIbslggQvYYedChsEzoVCTUvHnzBpaHjrt37/qQDje6kizAQrKnd+7cESFX6iEI4+Pj8hMNKVmSpEBVyy5DoNNqtXooRtD3wsKCJtwYGRnxz8/PBxk2NDQkMdIQKHIiRSqJlMDJxAifdmpqSvPy5Us5iQkJCYuSohlwDx48MCFnWvWip06dknO+mA7yL9ET7dNhMpl4dUXLAz3V9/r1axe8nbbZbDqUf1CHQffREilqIRDeyspKt7rgjh49qlfrIOdkLE8C9IRJNKWQY9EPSNhS4qtXr4ZJUFVVxauJ7XY7B9KgnHZ2dlLOA+SFhYVBe3z79u3UH3gcKEODg4P/wCkfcamJSbCA4cHB8IIETU1NOnVDePToke/QoUNCfX29oC4kKriioiLX2bNnvQ0NDYKa+OTJkzJJf3//GB4CIDIuKbCdALvRaCxHiE+gIQz4MXDLYI1i5Thw4ICbtcxJ1EQjZKXEod5OfmYJeeFAoc2ePn26jyw6fvy4AQvwmhUOaqvXr1+Xi+3GjRuPZ2dnpyB2EgfjkoKIkXzH+/fv55C7UZwq7aSI0ymCCuf/ktbW1vI4i408z3M9PT39R44c6YV4DnCoiZU8RwA2IBMoRDV+i4PiR/TrDj8bw8PDnr1797qR90VDSxeE3t7eQFvr7u7+C/IfgG+AQra2jXHptKpLgBGwsBwkgTwVHS2jpqbGjiKxJyUlZZGV6FrS8+fPRRz4cuHg/NasXbtWrl76xpE6iSi9QJoe4/NvYByYAGaAeVZkvpCLALAGKADKQV4HD8nqXy5evPgnGsywP8xAaxwBYR8OmzbmaR1QztYKuQiEXH2AaCAF2ACUsFCdQNf5mQxAI2nH/uxqbm4eIpSXl3cjGh30H0BzTpAO093A1gp79VGTm4mcWZkHfAHsBr4DGoAWFM5PQCtkrfTOcVwL/cfm7GY6eWyNsJc9NTiV51Z2XfmELGeLlQFVwD6glmEfk5Uxwg2kw3StKk+55S/0zAAGA2BUwfCfhSS2RbyAoIB9iwzS0hf6UDkzgj2V91A9P4OkIlGT+cMRhMfK54Jk+fEvJO9JKCsxrU4AAAAASUVORK5CYII=') no-repeat center;
	border: none;
}

.sbox-loading #sbox-content {
	background-image: url('data:image/gif;base64,R0lGODlhEAAQANU/AAoKCiUlJTY2NkRERExMTFRUVFtbW2RkZGxsbHR0dHt7e4ODg4mJiY2NjZGRkZaWlpqamqKioqSkpKampqioqKurq62trbCwsLKysrW1tbe3t7m5ubu7u7y8vL29vcDAwMLCwsTExMbGxsrKys3Nzc/Pz9HR0dTU1NbW1tjY2Nra2tvb297e3uHh4eTk5OXl5ebm5ujo6Ozs7O3t7e7u7vDw8PLy8vT09PX19ff39/j4+Pr6+vz8/P39/f7+/v///yH/C05FVFNDQVBFMi4wAwEAAAAh+QQFBQA/ACwAAAAAEAAQAAAGtsCf8NcTbgq8oVIIQ5x+EABvt1ktfzGB4ecI/EoRmBIW+2UCHkaAJBn9djFfb2Hw4AwOU0ZywcEyFDk/LAsFCTg6QlMiERkuSiMSRUonKUMcEhIULVc3Ih8fIxMNDg8sVzYbGBceQy4mPkssj0M5IRAVO0seESA2Pz4aESSeJzc1LR04KRMViStiMREpIxkvESg/NSmTPzoYGT4hFj8hEjRXMRUvPyAVPjgW2Us+gj8kHLE6SUJBACH5BAUFAD8ALAAAAAANAAsAAAZYwJ9w+IMkekShK7H6IQQ9HeEzlBkUv4Tg9wCIfi/aD1QAKQgpQEL4WJByCsjvQwnIhLCI4pHbCXc2GkQoGz5JQiQeGhkwh0IfFxcZMY5EMyyVPzolExeVQQAh+QQFBQA/ACwBAAAADwAIAAAGUcCfcFhpDI9C2OP1Yxx+NkMHWVtAfo1nIwAaxmy/kuIEQaACi5+O9ctQVDrIhkW6DH4nA4BG+0QyOkM8NwoAAhk+Qi4lSE0PQx8fIjONMVdCQQAh+QQFBQA/ACwDAAAADQAKAAAGT8Cf7/fzTIhIoiwT+00YP9yihMRVMj8KNEIwJVkRlKWxKkB+N+JqtwG5UB/EL0QY4H6aHa/6CxQ8SYEQR4GBLgwNDxaFagIBAQSMPxcdSEEAIfkEBQUAPwAsBQAAAAsADgAABlrA38/08QmPv9kPVPHlIKcj5xea/DaKFbIlirgUmR+uJSzJViRHrYQwIH893aOQIL2FlvBdGFsgEA13LQYEAwh3GCJ7Pz4EARFKSCkHLg8ABXsePygQPHuRP0EAIfkEBQUAPwAsCAABAAgADwAABlDA32+3WQl/ll8pAjteZJLRT+f6vVgXm8qxOCIVj5QXJPIKZxDGAnKEKRAIxjETMv8Ov0Tr56oUYhECAjgNGgAZPyoYPUIGATFmLgGIZjZeQQAh+QQFBQA/ACwGAAMACgANAAAGT8CfcPjbxYhCHCxDyRF3osiFhfyNRlUhzTKRZJAyiMMxQYJI2d+i0Hj9JC2V5vDLGA66C6IgCkB+LVg9BgQ/CT8oRAWFMAMeRAoJPT9OQ0EAIfkEBQUAPwAsAwAFAA0ACwAABlbAn3BIzLiIyFMKybyJPp8RU0irVC6e3xJZQhELOGREQZH9Nq3Dw8UKKWijxUL4CWTmpMLlFyv9aAEJPwkDPw4FLEMiAis/CAI9MgciQz1hhgg8PzpEQQAh+QQFBQA/ACwAAAgADwAIAAAGUcCfcIISGn+2DeZYivSOP5bLWFMAAtAfJwKy8QQASEwhhp0gNtSk8tOwfqNfBZJIKUC/WopKIPwcBz8WCjBQJz8fPw0GPjUMcUc+N0ISC09HQQAh+QQFBQA/ACwAAAYADQAKAAAGTsCfMATpCY9CRaFwICGRC0PBEHpaUxgrshEY5J6eX+8QYMAelZkM1cGlfqPqqaC5OFYRFPKGSPgoDD8eEjRHLk0/Ews/NhZ6R18/HBBIQQAh+QQFBQA/ACwAAAMACwANAAAGVcCfiPUz/Y7InwEAsSFCyV+sERDYdtGjqJF1IBCJU1YxIBRGWeTqk75NCgfdmACZXT61WutYKv1YCiIfGS9JOA4OPx4WP1BHLwspPx8VPzhpJBw+P0EAIfkEBQUAPwAsAAAAAAgADwAABlPAn/DHGw5XApPxVwsMhqjWTwS4XH4FQQVXUKR+KAQWtwQ5hhJGw7EaNhCIBGr5c5V8wxxHwdgNIQoZNSEnNz8mKj8wESkjQzoYGT8hQzEVLz8gQQA7');
	background-repeat: no-repeat;
	background-position: center;
}

#sbox-content {
	clear: both;
	overflow: auto;
	background-color: #fff;
	height: 100%;
	width: 100%;
}

.sbox-content-image#sbox-content {
	overflow: visible;
}

#sbox-image {
	display: block;
}

.sbox-content-image img {
	display: block;
	width: 100%;
	height: 100%;
}

.sbox-content-iframe#sbox-content {
	overflow: visible;
}


.body-overlayed {
	overflow: hidden;
}


.body-overlayed embed, .body-overlayed object, .body-overlayed select {
	visibility: hidden;
}

#sbox-window embed, #sbox-window object, #sbox-window select {
	visibility: visible;
}


#sbox-window.shadow {
	-webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.7);
	-moz-box-shadow: 0 0 10px rgba(0, 0, 0, 0.7);
	box-shadow: 0 0 10px rgba(0, 0, 0, 0.7);
}

.sbox-bg {
	position: absolute;
	width: 33px;
	height: 40px;
}

.sbox-bg-n {
	left: 0;
	top: -40px;
	width: 100%;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAoCAQAAACVv1j8AAAAS0lEQVR4Xh3EgQaDQAAA0Lfb1KamKck0kiQSM5Jj//9fcTyPSyBBSF+5BcjSOXceFJQ8qXhR09DS8abnw8DIxMzCysaXHzsHkT/xBHJWA5OpOIFyAAAAAElFTkSuQmCC') repeat-x;
}
.sbox-bg-ne {
	right: -33px;
	top: -40px;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAoCAQAAADa4lssAAABkElEQVR4XqXT6VICMRAE4F5EVFyQwwMRL0SQQ8Cr9P2fzAlTVIekJrJu9f+vuie7QAaUTRalPMEUJMpQFWRlmQrzX+ZAQ6g4UnUJoWJIlbGZNHLoEjCE9kJqLoTsNjZyJKlpTCZN4Fiyw3DUvsiJhAy7RIhN1CURUww53aSuiCTqQsQicjTIsEuM2ERT0kAedCmEnEkECRkbiYkW2sqkkDTRkbTR2naJEasHiS7O0d0PsXpcSBzS0UE+kh5D4kpiIOkeJHq4FuTSMSHij2GPmOjjRpBehOS8iN1DiQFuYwRNIuxhEfcSE+FFUlMe8LiD6GFbehGjR0AM8WQj7JGaMsIzEX0djtGXYQ9O8YkxJorgDgNF9CJ2j5CYYrZBhnjYIH09q44JehjEHAthJhg5xI1RRMewB6fExBIrYaYYY+RfhD34LjolOCiANd7wihleBNleJO5hTwHwjg+sdIxeZKcH36VuE5/4kh5LzMMe/rv4rxIT3/gRZq3I9mVcD/3I/ClK2C0WPCmvwa8j+FdIZL/cnirM8XykGAAAAABJRU5ErkJggg==') no-repeat;
}
.sbox-bg-e {
	right: -33px;
	top: 0;
	height: 100%;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAABCAQAAAD6QQ/SAAAAKElEQVR4Xl2FwQ0AIBDCqku5/14HeiHGhzQtg8WkUGsUHH1/t68f2QG+UhwDJ+GVZgAAAABJRU5ErkJggg==') repeat-y;
}
.sbox-bg-se {
	right: -33px;
	bottom: -40px;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAQAAAD97QrkAAABJ0lEQVR4XpWT0UrFMBAFxzaIiorvfoT//2+2KxpYNwxbaEJyl8CZPSfpfeCLjW9Ojr91EL9VziA4iGYCsPPJVg+gVBD61dg4Stej9J57gWcVQkg6d4ogrnwMTpjyBUPiINRfiBTP3TFKRUJLkLPMSEyVrNersfNBVEhOYwIMIzZ7IHItYYRSkCkvAt1BE2fnHUhAevGztmPnLREJsHXFiIp4BQN8D/2rDAKa70H30gV5AQE6+4lbEc8TAStgnknsUIzFtAGn/6MO8lRcsADCPny5qwtUY5HHzmORYsClh0gXcA/gIFR5D+iDjBTlMkCVXKh/EfpkBTIkr9XqqhlDcmptgEMNTnXsYNcuQPJrQPg6uSH3i4h8KTd43OttgINYbLmDSHwLAD8YGbr5h7oOUwAAAABJRU5ErkJggg==') no-repeat;
}
.sbox-bg-s {
	left: 0;
	bottom: -40px;
	width: 100%;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAhCAQAAACysAk0AAAAQklEQVR4Xh3E2waEUAAAwHG67q5aKUmKJJFIRP//adHLcHG/nhzsbKwszEyMDPR0tDTUVPwpKfjx5UNORkpCHCAiPM4jAwGbSxEFAAAAAElFTkSuQmCC') repeat-x;
}
.sbox-bg-sw {
	left: -33px;
	bottom: -40px;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAQAAAD97QrkAAAA9UlEQVR4Xo3RAQ6EIAwF0SLe/8h2Ny7rBEeBGoxKfP7WEiV+VR6P7XuUc9V2V8/rGtu59jj2cGWUi2z1fwLLVb2lCFb33PvR7g8TAKCwobMI4kKHOBO97VwkgWJyEET2KD0f9rIiuncFZ5c6IPw1oAAR06dgE0g85yEBxJM0ImLwLY/SjYxy9GX2kQCcI/cUZGCZciPPOdSMkXwjPLzsSBUEiFe2r0LndZ8DwqDaUIpBMx5iN84pkjCwUO8EiF9OABHKAWKOn7qCwIhLBZ0mASloYwQmgGEmhF8UwzgXERiwOTFnrFJmzEEYWcsjYp2BnHa/Qn0AOU8iiTKIE+kAAAAASUVORK5CYII=') no-repeat;
}
.sbox-bg-w {
	left: -33px;
	top: 0;
	height: 100%;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAABCAQAAAD6QQ/SAAAAJElEQVR4Xl3GCwoAMAgCUJ27/5X7UFDkA5UgKjxemFa+Xi0f5gbyATo0RQ1XAAAAAElFTkSuQmCC') repeat-y;
}
.sbox-bg-nw {
	left: -33px;
	top: -40px;
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAoCAQAAADa4lssAAABEklEQVR4XqWVUQ7CMAxD42z3PzKgSlQWvJpRjWhd+Mib7XZa3f5J+yNg7A/uIrRP31co3VenG9qUERkg3onIAAESBrQ1LiJ0IV4r6PlX8oowuY258PnfKKrIAPcGCe4uAIYqqyCAne/CRmUArgnqegYVAWAdBmmFUAZ43GtScQ0YRSM8BUtAj97Vvw9VAPQb0NXIYnkOCgBDQpyGGJQAjTihIQPmWo3xHKYBPdfqGSfLY3JNwOgNOeGcGJcB1mEjlG4kUNGIaIUGrGMW48y7UhgHIr7geLoxRx15Rxgks6AK2nEaBsFIHgxqrIBGuJUGAEMVOQ2A2oYCIn34spJOH2KnESwc4xp12P9H1bK3Rv9/vADJXgXDGDvZTAAAAABJRU5ErkJggg==') no-repeat;
}
@-moz-document url-prefix() {
    .body-overlayed {
	overflow: visible;
    }
}
 


























a:active,
a:focus {outline:0;}
img {border:none;}


#k2Container {padding:0 0 24px 0;}
body.contentpane #k2Container {padding:16px;} 


.k2Padding {padding:4px;}


.clr {clear:both;height:0;line-height:0;display:block;float:none;padding:0;margin:0;border:none;}


.even {background:#fff;padding-top:10px; padding-bottom:10px; border-bottom:1px solid #ccc; }
.odd {background:#fff;padding-top:10px; padding-bottom:10px; border-bottom:1px solid #ccc;}


div.k2FeedIcon {padding:4px 8px;}
div.k2FeedIcon a,
div.k2FeedIcon a:hover {display:block;float:right;margin:0;padding:0;width:16px;height:16px;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAjRJREFUeNqkUj1oFEEU/mZmb/NzZ3FHQlRCLihBLYSACIrERuSaiCBoo4XdES0sBC0Eq1SCjSZgChtBsbAQraJdMAoHZ5JC4ZQYj4seB7LC6f3t3u743uzd5gixysC3b3b3fd/75r0RWmvsZln8WDojElIiqyQyykLKUvTD2hlSwZECi0JgYXhe/zUCTB4aS14bPpActQdtW1CmVAqSYxf0LgiB57n1UvFg80eJqfeNAFdmcqNVsz2/BUWJFpG6QIcsqJISwo6PpUdJILMlQLa5MpP33t0EKAbOBoLiO+jPLyBrFXIpIci3cWz32dS6lNmbRtCZ2SZXbn2Yh7f6DGj8hjV5BbFLz6GOnI/I3PQQPU0MmxPadt8/MFHz+9AE5NQt4PQdyhLQhVeGDN+PBGQk0DnvnpsFDMzkEJt+aETx9jZQXgFO3IBO7CMukdttBP8TCPKPodffQIwcBaYfAYkR6OV7lNQPHL6ANpMJOtgmIDo90Lk5uhizwMurYcbx69DON2hyIdKnIgF/m4DLdllAzVBiNg/8+QkUl4D9x4zt4NcXiOT4lkAbbiRADSnznHlU3WU63XCAWDwkuXWad1+4J8FaDWXOE5z46aJYOHRu6rLVrMd7R8WVmdAL/mbbg7X8669Pzy7rrBljtYq1jY/fT6YnxyfoQvWjMypBBEXoRpu+NaqtZiHnrFcqWIvugefhyeZqaaC0UsoEAVIE02WOfica+EDbh9NsYJE50RF2s/4JMAC1ZRxNC8A8cgAAAABJRU5ErkJggg==') no-repeat 50% 50%;}
div.k2FeedIcon a span,
div.k2FeedIcon a:hover span {display:none;}


.itemRatingForm {display:block;vertical-align:middle;line-height:25px;float:left;}
.itemRatingLog {font-size:11px;margin:0;padding:0 0 0 4px;float:left;}
div.itemRatingForm .formLogLoading {background:url('data:image/gif;base64,R0lGODlhEAAQAKUAAAQCBISChERGRMTCxGRmZKSipCQiJOzq7FRWVHR2dBQSFLS2tJSSlNTW1PT29AwKDExOTGxubDQyNIyKjMzKzKyqrFxeXHx+fLy+vPz+/AQGBExKTMTGxGxqbCQmJPTy9Hx6fBweHLy6vJyenNze3Pz6/AwODFRSVHRydDw6PIyOjKyurGRiZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQIBgAAACwAAAAAEAAQAAAGhcCMcEgsDktIpLFY8BgMIcbHIiotJ4Ds5ZOFfIysLODi6GgAm+IAoEGZLkKKCVAghi2ZFWMIQhMlABhCVkINACF/ABxGJIeEGRYAHUMDASUBAAJECwAPFEIYAAQKACNFJwAmIAciYikORR8bWQutWQiwRSUFEBgDIU8eKEtEDg4lx8TKGUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIaExMbEREJE5ObkJCYkZGJkrKqsFBIU1NbUVFJU9Pb0nJ6ctLa0DAoMjI6MzM7MNDI0fHp8HBoc3N7cXFpcTEpM9PL0bGps/P78vL68BAYEjIqMzMrMREZE7OrsLC4srK6sFBYU3NrcVFZU/Pr8vLq8DA4MlJKU1NLUPDo8HB4c5OLkXF5cbG5sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABofAjFC4qHSGyKSwssGwlEnNAAAQmaCZkoFKFbE6C+V20wIBHpeCpYTUADZXDuhCOQEOyG1LOLpmJAAWSBEAGkoJACuDAAJKIwATbEsALkIEIUIBAANIDQAOKRkYnCMIAAxJFhsQHQ4nEqYqYUgLDCUqXAAqBFgEhFUMs1gXKCIrF1hJBBgQSUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIKExMbEREJELCos7OrspKakZGJkFBIU1NbU9Pb0lJKUVFJUNDY0vLq8DAoM3N7cjI6MTE5M9PL0tLK0fHp8HB4c/P78PD48BAYEhIaEzM7MREZENDI07O7srKqsZGZkFBYU3Nrc/Pr8nJ6cXF5cPDo8vL68DA4M5OLkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoDAi3A4nFSISOTmIUgOBYdOpwHAjJKjEmDLBRiSB0Cm5HCgtiXkSewQijIEw0hBDB+GJ43HA2IQOwAnSSIAFn8ATUiEFldCJRIpIyQaQxoAA0QTIycmACRuCJ5ICxlbFCIBoSZ0RClnXQANHk6WIRYWHCSNSRMEESO7TkIfG8IXQQAh+QQIBgAAACwAAAAAEAAQAIUEAgSEgoTEwsREQkSkoqQsLizk5uRkYmQUEhS0srT09vSUkpRUVlQ8Ojx8enwMCgzU0tRMSkzs7uy8uryMioysqqw0NjRsbmwcHhz8/vycmpwEBgSEhoTExsRERkQ0MjTs6uxkZmQUFhS0trT8+vxcXlw8Pjx8fnwMDgzc2txMTkz08vS8vrysrqycnpwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGfMCMcEgsGo/IjCD0+ZQmpMxiRSSVANisioOQEA+AzZN1eWBRXqEgPCEGsAjQEFwiJswA9LAzMthdGhouUUIJLgQaBIQZJBJUQw1ZJ4scAANEkQAliykIAC5EJlgPDhApXAANixkNDBFZWRZyRBQKJBUqGBgeg0lCJKu+SUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIKEREZExMLE5OLkJCYkZGJk9PL0FBIUrKqs1NLUNDY0dHZ0XF5cDAoMTE5M7OrsLC4sbGps/Pr8tLa03NrcnJqczMrMHBocPD48BAYETEpM5ObkLCosZGZk9Pb0tLK01NbUPDo8fHp8DA4MVFJU7O7sNDI0bG5s/P78vL683N7cnJ6czM7MHB4cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoPAlHBILBqPyNTAczo1KMhJA0CtPg7Gi0tjUKkkDsBmQqxwDi3ihQRIpAipyQJj+RQZgEJGkGJVFwNCCigFVBoDEC5UCA0gQisNGlQNKSMnhRJGAwsIFSkcExQAGhdEKxUfaUQlACQjChUBCA4WRgcbVVUiEEcTCQ8uGAIsdklxxshDQQA7') no-repeat left center;height:25px;padding:0 0 0 20px;}
.itemRatingList,
.itemRatingList a:hover,
.itemRatingList .itemCurrentRating {background:transparent url('data:image/gif;base64,R0lGODlhGQBLAOZgAOvq69/e3ufm5uTi49vY2evq6uPg4ff39/Py8u/v7/ilRfTz9P7dSv7fUfmtSv7XQ/aPOf3LXP7FO9fV1fidQf7PQ/7+9f748/7xYf7WSv3GWf7oWfqyTPu0TtTR0f7kUveUQvaSOvWGNf784f70Y/756P3v5fioT/3ANv7iav7emP75sf7Uafm1gv7tpv73k/712P/fefy8U/7ncfioWP/Mmf7mif/QO/7sXf738v7UUf7ngv72gP3kzv7vlP3guvq/df7Zef/MM/u3T/mvU/zYpv76uv7wzfzGcv7yq/743PilTv7w3f7hZf3ixP7dh/vVrPnAmf7uv/7ql/7vuP7uw/u+bf76xP70bP7Xc/q5af786v73nP3kvf/nnP///////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAGAALAAAAAAZAEsAAAf/gGCCg4SFhoeIiYqGBQKLiwZfCwaPiAAAX1+OlYYBB5lfAZyEAAGgXwkEAKNgBAmnoQSyBAYFhwAGEwQCsL2atoO4AAu+xQLAggLExb6lhQADzL2Ttwaf0l8HBIoECNgEyIjd0tuLBa/Mx4/oxQmUigDe5IvKpwIBANdfA5DLBeBgCgQYQGxTIoIHBvArJMCgooasIkqcSLEiqwEOJQZAoJDipUwTKHqSAiOBqIilSgipcMHAu1GuvNy4EeOALgGrFAn04OFChgdAqRxAQPRSRjABAghIcKCEDaBQU6Sw4cLCl0uEBnxSMiUFg69gvzYYCyMUw087wI4d+6FtWx/7/w4NSGBhRgOgEiSg2Cuhwoy4iAZaCJK3cIXDOi50TBQgUozDGSJHbvLFwyNwLhisbbCh85aXiQQgSNIZBw4MqDFcwfpwARfUWFYY4UGCxAoEJxkjeEHihQUEr0asGGFWkScLxO8NUHotnNxMuBkOq7SAtcXr2LNr3y4IWk6KCLRVjIQqd0QBJqLkUKVxQQsRLbR9ryRgwgUI+HsI8DALn6VcEyBQAwgE0mACdEXNB4Z3xJgAxRIUUKDAhApoAcQP2WSETg0nUKiAAyA6wMGIRQBGSAGfEBGiiA5Q2EEHSCiGyFxMWEEBfiKIgB9+Jyi20CED5UBDCERGGKECThQQkl1xAVzQ4YQgcjBEFwKAhogHX7zYgQwyaOClCiY9MsEXXkZg5plZfFGOIpGYyUIVXxyhQgQsaPIIL0+oYEEqAhxgQQmS3AmKSwUIFMArCFRylX8nHoVIodxFKil3gQAAOw==') left -1000px repeat-x;}
.itemRatingList {position:relative;float:left;width:125px;height:25px;overflow:hidden;list-style:none;margin:0;padding:0;background-position:left top;}
.itemRatingList li {display:inline;background:none;padding:0;}
.itemRatingList a,
.itemRatingList .itemCurrentRating {position:absolute;top:0;left:0;text-indent:-1000px;height:25px;line-height:25px;outline:none;overflow:hidden;border:none;cursor:pointer;}
.itemRatingList a:hover {background-position:left bottom;}
.itemRatingList a.one-star {width:20%;z-index:6;}
.itemRatingList a.two-stars {width:40%;z-index:5;}
.itemRatingList a.three-stars {width:60%;z-index:4;}
.itemRatingList a.four-stars {width:80%;z-index:3;}
.itemRatingList a.five-stars {width:100%;z-index:2;}
.itemRatingList .itemCurrentRating {z-index:1;background-position:0 center;margin:0;padding:0;}
span.siteRoot {display:none;}


.smallerFontSize {font-size:100%;line-height:inherit;}
.largerFontSize {font-size:150%;line-height:140%;}


.recaptchatable .recaptcha_image_cell,
#recaptcha_table {background-color:#fff !important;}
#recaptcha_table {border-color: #ccc !important;}
#recaptcha_response_field {border-color: #ccc !important;background-color:#fff !important;}


div.k2LatestCommentsBlock ul,
div.k2TopCommentersBlock ul,

div.k2LoginBlock ul,
div.k2UserBlock ul.k2UserBlockActions,
div.k2UserBlock ul.k2UserBlockRenderedMenu,
div.k2ArchivesBlock ul,
div.k2AuthorsListBlock ul,
div.k2CategoriesListBlock ul,
div.k2UsersBlock ul {} 
div.k2ItemsBlock ul{
list-style:none;
}



div.k2LatestCommentsBlock ul li,
div.k2TopCommentersBlock ul li,
div.k2ItemsBlock ul li,
div.k2LoginBlock ul li,
div.k2UserBlock ul.k2UserBlockActions li,
div.k2UserBlock ul.k2UserBlockRenderedMenu li,
div.k2ArchivesBlock ul li,
div.k2AuthorsListBlock ul li,
div.k2CategoriesListBlock ul li,
div.k2UsersBlock ul li {} 

.clearList {display:none;float:none;clear:both;} 
.lastItem {border:none;} 


.k2Avatar img {display:block;float:left;background:#fff;border:1px solid #ccc;padding:2px;margin:2px 4px 4px 0;}


a.k2ReadMore {
  padding-left:25px;
  background:url('/components/com_k2/images/fugue/leermasicon.gif') no-repeat top left;}

.onecatlink {
  padding-left:25px;
  background:url('/components/com_k2/images/fugue/leermasicon.gif') no-repeat top left;
  float:right;
  margin-bottom:20px;} 
  
a.k2ReadMore:hover {}


div.k2Pagination {padding:8px;margin:24px 0 4px 0;text-align:center;color:#999;}


div.k2Pagination ul {text-align:center;}
div.k2Pagination ul li {display:inline;}


table.csvData {}
table.csvData tr th {}
table.csvData tr td {}


div.itemIsFeatured,
div.catItemIsFeatured,
div.userItemIsFeatured {background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADLlJREFUeNrcmgmQFNUZx//v9Zy7M7s7y8ICcuyCHEoE5BRUVMCjSrFMKZBKJZoqA1hBSCVRUMoq412iKRPQlAqa8ooSMYkKFQ2YkkvZVSkgyOEBLPe95+zO7Ez3y//1HNu9O4tAuPSrarrnTU/3+/X/+773fb2In/t8jzea1syEQn5Hw4AEuAl7b3ATSth7Iz3u3qNlr3J85/pt+juVOm6EhW9VI4aJMEfRxO2fPG92VHp34/8wDy80p0Aa8YSl5kRN01fm9XEComVCwg3jnKThGPMIx+f0WEHvMsS2V7nGjPR5+vrlwoMtVhQjRWGwAdZEbkaBlXyoTno2nyqQ1ArwHo8GpLgvKCW2N8ezMNL5ZLMQbWFaFGiZ+GVvvYzRH/wd4V49s2Ot1S+EF4Op0CZVjyCEpxjGJM5lbsQyBxdbpjglIH3xsVaB/vVCn5DTCgwjtrW5SUO6Jm5PxjEpF5TzPJ4zkjBFQwajdt16jCBUfnlZGkK43E/S/cIcuYRQu1UTkjBRAuNG3nueUBjS0TTlyQIZQw0PypUfZcrfvEc0b/FCfOMX8spdZjy/i+FzuwvaupXeexyAWpnIkEGovOFW7FvwKiKXXwZfpAjJ/QdhVde2uK9CFtLHrVh4UY0EY8tEKbw9Y1DDCFYZlfLAybmc48PlVrhxlAq/HhDyNx2lb9fmRGOr4BY5ATNP/LI3X0ancVfpVJId+3LynbbaQyqXoXjcGBeMRItb+3lcRscL8Yrfqig6wXMph18qNc3RJ+lybV11uAq9ERTydx2kd8+WRMr9nDdvCygw6s2UMqtHjkMdXa3LbbfYEy+bNRM9Zs3A0cXvoe+ihSiyoVquJ7KbsJXuSm/pRbD1qBWd4R3E757vYpo3nrDLDafL9eBFWltn+DbXiOSXVGvsAau5ICIM+IVoA+PhNvCxB1FKZSroZnFmtcOL30f96ooszLbJv8TB+QvgpevprWlNpesBZeHSyuXxyh3oiJ+iWgxAqJTZb3CBUtVhpbbUSx157Zv4lTeA0Va43RO2iegoXuGVuGX26UI/78BUm8v1wr3KbBgj7VLlaRhtXxOo4aNVdhLQEy6eeDPCVOrAXffaMLYHKKQ9IaWWPq5nmljFyBpLvP1IVHHoAW6L9zFxtavQSOlFNz6N9qxaJPczUXzshxzeqMyuzcpExIZyu14m4PVYuUOZ5h1VKJ5wPWrfft+ebI/nn0an2TOQf8UIqFgcMa1WDhhhx5VEV/67AscwCKGiKNQYnrorpNS2BinNnArN9AQxQoW+0zd3o2kkb/gEYF2joS4S+ba7OdXSKbvLbTdjwKKXUHX/I9g3d34qaej0PHSQDePjumRW19gPoGr49VmYTJxmIjozpi3KzEf3w5UppRoSUPccMIwXcio0igp1PY5CGeMiuJcpdbMXsiwfRm9dtnQTgTbrUGzzV3zyMXSjQk1fbESCbtj9iQfQ7Y+PoZnHDUuWIe+Kkdg/eSpMpvLOLzyFhP5NdR1kDhhtPn7ThUpVohZ9kOdrBq4JKCuZr9Sn0VYxZYyWPp7sO6EMEoJnXwzJtbxxr4j09PzSbPCUy0A2bjLrSjQd9MU3XYdOU29HIV3OiBSiesFr6DBzCo7NfRYNdEF/eU/kT7gWRbPvRtOS5YSqbQOTrdF4xc6E2sTIisDjo5ZDkpxSUKm1jVIms0BXUKHOJwgEOwN5jsWRXMYL9mWC6L/Bqhe9RNC1Jul94+pKJHbsQoQJYC+Dv3nzNpRSKZ3hDt01y3Y1DRCeOAG+Af0QmniTDWWloZyWkUBft4RQ+xCjE6o8PsrBCQE/oTY0CdnUZmE9USsUgcOc+C84/Tf6yXystKpdNVomHce+2IBvhl+HxuUrEeQaZXKyGkak46bz316Ef+hA7Bt2A2LLV9nX9jLGcsGo9Kbj9kI+Vl1dHESsIA9yloK6t9BKltpAxilWtXnC38CnfJ9S4rn+Mow1Vo2rRnOuLwFOMsiJH7n/USS/rbLHMjAHrv0JTMbW0WmzkE+VOn/+ASTdMxdMZtOpoysCdgx/rRq8+ZCzOf77kJXsZIyhy3U8CZdzBaAw6qUyN+r7lEjviK1WVHajS6Rqu5aFUwd8lMkgtmyVrUyXVjC26rOno4CxdGTSVDtJtFYodaxawHghHb16XaxUdeghgsNisLoZVxOo5BSBUinJqPMos4JPLVkkvIN2qKZAmDcJEsWpEo7VnhCMdj0PFe20bBHizJI6E6pWMJZI7/mvbkk7Cz9WqWoWuP7+OWu5k7Wk9NU2S++DvNIfSkXg6H7EsZeBmy1t0jGj1yAd9BomeVyYt+zvmwlk8LNGsRzKZGAse59K62NEBKtVrdcYy7RdzEbrdFhCGCv9yjK5Tl3NSRj7VRylwpctQrXrNdH1Mum5PRgNcmjSNH43HZ1encfkstFewzSMlYZxxpT+rIUJ0DNOOSm0W76zOeNNZ3jgSZQQplLVOCpq96IZmvoz1D35bLswen3S65VOIJ7ePdMqqTREC5yVdkM7dsfT/wpPk0La4kJaXBe28nCrqTCGSSJ/DcuWC5HvgrGPgwFbIQ0T+dMjiOs1ygFzgMrVzFuI4PgrEbx8BOrYgljtwOhjFlSpFvx0W7U0olxrFvM+U3mTr4ahCEtxyAWjre7J52yFApxwLfdOGFttpm9LtGQ7PXkNnwsmAynmGmE7p58pY8d5K+/5UATGgNWsmm9hL9rewulURqsS4bGuBSV7qB1coHVVUcIacSer+Bou1hkYMw1TpZpPfwy1toOG8Q7ve28Nk/QYFKslVAo5Spt8TjYDoysLndrrGT81C153wdS8/R7KFi1AaPwYl/uZaZU8Emfe9hvGv7qa5k+rYb48CsUXL8dRjGcroBwK6eDXpZJuEjWcn+3GTrYX8e070YGgKWWmoHb5CiTYfuTx+8YdOxHl+U4weTaAtLHLrOBuEpX6bAjT0GomiihMR2mjUqmZx9rVtDpOGDjiZtecR3Fk8bu46MPF6LdoYVYdi4Fr3MQsp/P32bCwUke5W9ME1bc7K7wdaEq5G9f7TNDrtUYWF+Eom8PIr6fYMFVUJs7Ot5DtyNF33mcB28OGqV+3ARG28opp5tgnFeyWTIjnWFiyesbZtC6mOYQADxfCuLGaVUWSU+pDLGdJE6LbdWes7GdBe+DJ+ak4EcjCNBBm0+Q72UjOxAVTbseKvkOxWyV0UhA428aYWseJz2ZMvVsIfzLMdfBz1LlKmhq6nIbpNOvuVAJoBaPf91mOUiiT6Yyb6XLes+RyTmPrfJgt9IoYVA8/ZK+Owutdy6qiO5vFTLKoX1PBCQsUTLjOdi8njAYoHH8V+j3zGL55ZC6q162ny1kQC2UBY8iPc2Ulphnk0/9zCPKOEIT4mFXzONGhZX1J128DKv9t91UVfYYjzixXRJhL3lqIXQtexeY5D9uJYa9KQvxFsPCRPpxLi1hmAbPc42zUprMDxUfMHdeLkmzcaDjdyf6ICjUxVTezuNWvlbc/NQ9fz51nw2j4PUjA+DETghTGOQWKCRn3KWs93U8RYnQfkYdlhCqXeVmlNMThpR8i7+L+MGMx7Jz/InZSnQyMyZRdp13uNSokzrFCGctnC02A6Ww4flsgZGg12/qrZCQd8C0TN7NJwD2mXc64Tafsc6xQSz8lo4ay1iag4jFhDewt8kKfWXXQbYguaS3hrrTNtEtmAPU6ZAOp8wQI9gSNJLc1pjLpimpImQjkbVNReIRkNhbpyeeqtBXqdftw/qC0Klil7+lmpe4/jGSiXObjCAN+DzvgDEBKGZUFzKh0Wt4pnDG1gFeagdsPqUR1qfDbr122WY2tIFp6IV3LyfMXB3o5SXDC/0gCdxxUyR15DI1S6ccGq75V65BKDGe12j5VCwp/nBNdQrAZh1Vyo64c+hkhVCTrXCk7U/pIAYHz3cLCr4pEYCmTwJyjytwY49Qv9YTxabLWdrNz0g+dljIJgaVUYgahNkVhmUO9BfgkUeeKpe8VkN16ILCSE598DNa6KOuKYYT6LFGXjSUp8P2z7ghuZvqeXA3rPw1crwZ6w9iYjNoqfS9iKJf1Unk7WNDezSh6TyvVz5uHKjNu/58jXcK3+3YzF646zp86Ml2ncqwPKp2NlOMNTaYpM9NNWiYGzFbv3JzxYabXGkcdt4XjD9DhEkElbr3A4Frl/KPtmYFx761Wr3IzMCrH21DT0Ym6YVrGH09E/8vf3lOv1F/520aZC+R4MGgHRrULo9q+unVMyHKp5YZx/kYJlfOa2uYmojs5dhc/v9EmKYjvgFG5VBJwvUhXrW7cFiyjqNvNlKNDdavlVMbtvhl7JtFYz8/ThFIKPyST+IHZ/wQYAOyAUMF1MRpUAAAAAElFTkSuQmCC') no-repeat 100% 0;}






a.itemPrintThisPage {display:block;width:160px;margin:4px auto 16px;padding:4px;background:#F7FAFE;border:1px solid #ccc;text-align:center;color:#555;font-size:13px;}
a.itemPrintThisPage:hover {background:#eee;text-decoration:none;}

div.itemView {padding:8px 0 24px 0;margin:0 0 24px 0;border-bottom:1px dotted #ccc;} 
div.itemIsFeatured {} 

span.itemEditLink {float:right;display:block;padding:4px 0;margin:0;width:120px;text-align:right;}
span.itemEditLink a {padding:2px 12px;border:1px solid #ccc;background:#eee;text-decoration:none;font-size:11px;font-weight:normal;font-family:Arial, Helvetica, sans-serif;}
span.itemEditLink a:hover {background:#ffffcc;}

div.itemHeader {}
	div.itemHeader span.itemDateCreated {color:#999;font-size:11px;}
	div.itemHeader h2.itemTitle {border-left: #EC0623 4px solid;
  font-size:20px;
  font-weight:normal;
  padding-left:10px;
  margin-bottom:20px;
  color:#414042;}}
	div.itemHeader h2.itemTitle span {}
	div.itemHeader h2.itemTitle span sup {font-size:12px;color:#CF1919;text-decoration:none;} 
	div.itemHeader span.itemAuthor {display:block;padding:0;margin:0;}
	div.itemHeader span.itemAuthor a {}
	div.itemHeader span.itemAuthor a:hover {}

div.itemToolbar {padding:2px 0;margin:16px 0 0 0;border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;background:#f7fafe;}
	div.itemToolbar ul {text-align:right;list-style:none;padding:0;margin:0;}
	div.itemToolbar ul li {display:inline;list-style:none;padding:0 4px 0 8px;margin:0;border-left:1px solid #ccc;text-align:center;background:none;font-size:12px;}
	div.itemToolbar ul > li:first-child {border:none;} 
	div.itemToolbar ul li a {font-size:12px;font-weight:normal;}
	div.itemToolbar ul li a:hover {}
	div.itemToolbar ul li a span {}
	div.itemToolbar ul li a.itemPrintLink {}
	div.itemToolbar ul li a.itemPrintLink span {}
	div.itemToolbar ul li a.itemEmailLink {}
	div.itemToolbar ul li a.itemEmailLink span {}
	div.itemToolbar ul li a.itemVideoLink {}
	div.itemToolbar ul li a.itemVideoLink span {}
	div.itemToolbar ul li a.itemImageGalleryLink {}
	div.itemToolbar ul li a.itemImageGalleryLink span {}
	div.itemToolbar ul li a.itemCommentsLink {}
	div.itemToolbar ul li a.itemCommentsLink span {}
	div.itemToolbar ul li a img {vertical-align:middle;}
	div.itemToolbar ul li span.itemTextResizerTitle {}
	div.itemToolbar ul li a#fontDecrease {margin:0 0 0 2px;}
	div.itemToolbar ul li a#fontDecrease img {width:13px;height:13px;background:url('data:image/gif;base64,R0lGODlhDQANALMPAObm5ufn5////+7u7vHx8ezs7Orq6vr6+v7+/vPz8+/v7/v7+/n5+ff39+3t7f///yH5BAEAAA8ALAAAAAANAA0AAAQ38Ek2AghjSYkI+GCQCNIAnh/xNGh7FC2qWDFonEKeh3QNGDAfQMESHh6mmurRaYlIG4bDgtFIIgA7') no-repeat;}
	div.itemToolbar ul li a#fontIncrease {margin:0 0 0 2px;}
	div.itemToolbar ul li a#fontIncrease img {width:13px;height:13px;background:url('data:image/gif;base64,R0lGODlhDQANAMQaALy8vL6+vvr6+tra2tDQ0PPz8/7+/v39/cXFxd/f38DAwPv7+9HR0fn5+e7u7urq6tXV1dLS0szMzPLy8tTU1L+/v83NzfHx8enp6dnZ2f///wAAAAAAAAAAAAAAAAAAACH5BAEAABoALAAAAAANAA0AAAVCoCY6RAAoUSGKxwC8cJAYIgPf76A9eD9ZOAEOYroJbwhjQ7A4noqwBQ4hwTVwFEwPd9EQtq8My4WT0VYkU4WhEoUAADs=') no-repeat;}
	div.itemToolbar ul li a#fontDecrease span,
	div.itemToolbar ul li a#fontIncrease span {display:none;}

div.itemRatingBlock {padding:8px 0;}
	div.itemRatingBlock span {display:block;float:left;font-style:normal;padding:0 4px 0 0;margin:0;color:#999;}

div.itemBody {padding:8px 0;margin:0;}

div.itemImageBlock {padding:0px;margin:0px 20px 5px 0px;float:left;}
	span.itemImage {display:block;text-align:center;margin: 12px 0px 0;}
	span.itemImage img {border:4px solid #ccc;padding:0px;}
	span.itemImageCaption {color:#666;float:left;display:block;font-size:11px;}
	span.itemImageCredits {color:#999;float:right;display:block;font-style:italic;font-size:11px;}

div.itemIntroText {}
	div.itemIntroText img {}

div.itemFullText {}
	div.itemFullText h3 {margin:0;padding:16px 0 4px 0;}
	div.itemFullText p {}
	div.itemFullText img {border:4px solid #ccc;padding:0px; margin-top:4px;}

div.itemExtraFields {margin:16px 0 0 0;padding:8px 0 0 0;border-top:1px dotted #ddd;}
	div.itemExtraFields h3 {margin:0;padding:0 0 8px 0;line-height:normal !important;}
	div.itemExtraFields ul {margin:0;padding:0;list-style:none;}
	div.itemExtraFields ul li {display:block;}
	div.itemExtraFields ul li span.itemExtraFieldsLabel {display:block;float:left;font-weight:bold;margin:0 4px 0 0;width:30%;}
	div.itemExtraFields ul li span.itemExtraFieldsValue {}

div.itemContentFooter {display:block;text-align:right;padding:4px;margin:16px 0 4px 0;border-top:1px solid #ddd;color:#999;}
	span.itemHits {float:left;}
	span.itemDateModified {}
	
div.itemSocialSharing {padding:8px 0;}
	div.itemTwitterButton {float:left;margin:2px 24px 0 0;}
	div.itemFacebookButton {float:left;margin-right:24px;width:200px;}
	div.itemGooglePlusOneButton {}
	
div.itemLinks {margin:16px 0;padding:0;}

div.itemCategory {padding:4px;border-bottom:1px dotted #ccc;}
	div.itemCategory span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.itemCategory a {}
div.itemTagsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	div.itemTagsBlock span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.itemTagsBlock ul.itemTags {list-style:none;padding:0;margin:0;display:inline;}
	div.itemTagsBlock ul.itemTags li {display:inline;list-style:none;padding:0 4px 0 0;margin:0;text-align:center;}
	div.itemTagsBlock ul.itemTags li a {}
	div.itemTagsBlock ul.itemTags li a:hover {}

div.itemAttachmentsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	div.itemAttachmentsBlock span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.itemAttachmentsBlock ul.itemAttachments {list-style:none;padding:0;margin:0;display:inline;}
	div.itemAttachmentsBlock ul.itemAttachments li {display:inline;list-style:none;padding:0 4px;margin:0;text-align:center;}
	div.itemAttachmentsBlock ul.itemAttachments li a {}
	div.itemAttachmentsBlock ul.itemAttachments li a:hover {}
	div.itemAttachmentsBlock ul.itemAttachments li span {font-size:10px;color:#999;font-weight:normal;}


div.itemAuthorBlock {background:#f7fafe;border:1px solid #ddd;margin:0 0 16px 0;padding:8px;}
	div.itemAuthorBlock img.itemAuthorAvatar {float:left;display:block;background:#fff;padding:4px;border:1px solid #ddd;margin:0 8px 0 0;}
	div.itemAuthorBlock div.itemAuthorDetails {margin:0;padding:4px 0 0 0;}
	div.itemAuthorBlock div.itemAuthorDetails h3.authorName {margin:0 0 4px 0;padding:0;}
	div.itemAuthorBlock div.itemAuthorDetails h3.authorName a {font-family:Georgia, "Times New Roman", Times, serif;font-size:16px;}
	div.itemAuthorBlock div.itemAuthorDetails h3.authorName a:hover {}
	div.itemAuthorBlock div.itemAuthorDetails p {}
	div.itemAuthorBlock div.itemAuthorDetails span.itemAuthorUrl {font-weight:bold;color:#555;border-right:1px solid #ccc;padding:0 8px 0 0;margin:0 4px 0 0;}
	div.itemAuthorBlock div.itemAuthorDetails span.itemAuthorEmail {font-weight:bold;color:#555;}
	div.itemAuthorBlock div.itemAuthorDetails span.itemAuthorUrl a,
	div.itemAuthorBlock div.itemAuthorDetails span.itemAuthorEmail a {font-weight:normal;}


div.itemAuthorLatest {margin-bottom:16px;padding:0;}
	div.itemAuthorLatest h3 {}
	div.itemAuthorLatest ul {}
		div.itemAuthorLatest ul li {}
			div.itemAuthorLatest ul li a {}
			div.itemAuthorLatest ul li a:hover {}


div.itemRelated {margin-bottom:16px;padding:0;} 
	div.itemRelated h3 {}
	div.itemRelated ul {}
		div.itemRelated ul li {}
		div.itemRelated ul li.k2ScrollerElement {float:left;overflow:hidden;border:1px solid #ccc;padding:4px;margin:0 4px 4px 0;background:#fff;} 
		div.itemRelated ul li.clr {clear:both;height:0;line-height:0;display:block;float:none;padding:0;margin:0;border:none;}
			a.itemRelTitle {}
			div.itemRelCat {}
				div.itemRelCat a {}
			div.itemRelAuthor {}
				div.itemRelAuthor a {}
			img.itemRelImg {}
			div.itemRelIntrotext {}
			div.itemRelFulltext {}
			div.itemRelMedia {}
			div.itemRelImageGallery {}


div.itemVideoBlock {margin:0 0 16px 0;padding:16px;background:#010101 url('data:image/gif;base64,R0lGODlhBQCnAfcAABQUFBISEhAQEA4ODgwMDAoKCggICAYGBgQEBAICAjIyMjExMS0tLS8vLzAwMCgoKCsrKyEhISQkJB4eHi4uLicnJykpKRoaGhcXFywsLCUlJRYWFiYmJhwcHBsbGyoqKiMjIx8fHxkZGRgYGCIiIh0dHSAgIBgXGB8gIDExMjIyMQ0NDQUFBCwrKyoqKwgHBy8vMCsrLAwMCykpKiYnJx8fHjAxMDEwMQcHByQkJRsbHB8eHzAwMRIRES4uLSwtLCkoKTAwLyAhIRkaGhwdHRoZGhkYGQ8QEAQDBAkKCS8uLyopKiUmJSgpKCkoKAYGBTEyMiQjIyMiIiAhIBUWFhESERQUEwwLCwMDAi0uLSwrLCwsLSUlJiYlJiMiIy8uLgUGBSQlJRcWFxIREgsMDAMEBAwLDAoJCi4vLgcICCYlJSsqKzIxMiEiISEiIiIiIR0eHTAvLx0cHBkZGh0cHR8fIBYXFhYWFQMDAysrKiUmJisqKh8gHyMjIiYmJyYnJigoJzAvMB4fHh4eHRAQDxwcHRwbHBYVFhsaGjEyMS0tLA4NDS4tLTExMC4uLy0sLAUFBQYFBicmJyAgISEgIRobGhobGxcYGBcXFhUVFRQTFAsLCxoZGQMEAwQEAw0ODg8PDy0uLjEwMAkJCS4tLgkKCgkJCAoKCRAPEC4vLy8vLiwtLQ4ODSAfICkpKBwcGyopKSgnKCcoKDIxMSQjJCMkIyUkJCIjIx0dHhscHB4fHx4dHhcXGB0dHBoaGRMUFCcnJhgXFxUWFRgYGRkYGA8QDyYmJSUlJBISEQsMCywsKyMjJC8wLykqKSkqKgcIByoqKS0sLSgpKQUGBiMkJCgnJyEhIhQTEyIhISIhIiIiIxMTExwbGxwdHBkaGRoaGxMUExsaGxsbGhYWFxcWFhgZGBgZGRgYFxERERESEissLA0ODS0tLjAxMS8wMCcoJycnKCgoKQgIByorKiorKyAgHycmJiQlJCUkJSIjIh0eHh4dHRscGyEgICEhIAEBASH5BAAAAAAALAAAAAAFAKcBAAj/ACEJFPivoMGDCBMqXMgQYYIEBSH+kxhxosWKFCE+nPiwY8aNHjl6HOkRC8mTJvGYTIBF5UMsK1niYflQ5cybLRPYxMOzZUueQIMKBYoEjydPRY0GLcMTSVI8CBAg6VSmaKdOSKTiQVJGalQEXb9C/Uq2bFQWLM5+hRR1IAKBaBGwgDQXLVy6b93eHch3IJi+kSD9fQIp0pNIgSMdiARm2mFIB8AEhnxgseInBwhXnla5c2ccnkN/rgwah2nSpk8fSM26tOvWrF/geGZatmzTaUzLe2GA92zeBoIbyJ2mt/DjxY8fNxWc+ajlypkzNzDK1Kjr16lbx16dO/cko5Kc/8F+ptT46wVOjToD3nyBUgXCp09y6kyB+wXs499fYFP//wD6t8mAAf4nYH8DJqjggptcoSAZDcpghoObyGChGWQkI+EVBHBIwCbJkEEGASTKQCKJZpxI4goqEsDiiy6yuOKKL65g4404xojjCuvc+ImNrCwC5CIDfCKkjUUOoOQAK7Cy5JNQDgCKlFNKqWSVVloJypZUbunll2CCUgwohBQjAChHgILKmgIcgQohhBxRzBEC1IlKmwLcSUidfPYpADp1AsonoIT+aaig6CSqKKKLolOFo+j0gEwPVVTRQwCJUopOAGNwOsalyAQg6hjpiGqqqducmuqpqG7j6qsBuP8aa6yyvmqrFa5e88s24OS6jSauWmEFAL8AYIUmAICDLADEDsvss89mAi0A0mZS7bXMYnuttdZSwa21d2RyiDDhZuLtuHdsIMwhh1CxwQZUUHEHu+++e0i979pBjh12bIDJu+NsgMG//I5DjhgIi4EBBv4u7DAGvDjMyyUYnFDxwhFfck4wJ1wywgnBnHPxCB+f8PEIwZCsshEkGzEMMSwbUQ4xxJAsggjmjCDCMCKUc7MRO5tzszc331zEHN5cIAInRXBidBG+zDHEHJxMDfUFvgwxxAVcX7B111xXcoHY33CNyDfhWGLJBYh4YEkl4SAijgdce2C3B3PffTc3OnD/Y7cOHhiiwysdFO5BB4YkPngHueTSAT866JAL4YVX3kEhhXcjh+ZyENELEZeXUIgcl/dCBxElnI46Han3UsLrJQwCe+y74AIHLrDDAYc+cAyyywT7+I7LBMQXb/zxxe9AvCCC1LBDDbrssEP0gjwfwvW6hCBICDVw3/3119cRAh/g18GH+CigUEc946ePggkotNLK+3y8b0IrJkxhwv4mCDFJ/vmLgBCEMAUhmGASk6BEP/wxBUpEwIFCiIAEJ0hBCmJDgtggQQTa8IY2uMENEchGB7PhBmuQwBodJAEJ3kACN6jwhSSQghS0QYJbkCAfKvRCH6TQh1uAAAQz7AMI/7zgBSD+kIg+/CEI+hCFKICgFiBwYhSo8UMo0iIKtZDAMkAgAQlQo4vL6KIEaEELMZpRjDnIwTFyIIEw5MAWGmBjG8NwjDjaAh8auIcGbBEGDfjxj4D0oxqY4McucIEJTOCCGrrABDUoUg9M6IIe9MABDnCBCxzogjEo2QUOGKOSlfQDKGlAgz/8AZSVlMQfJGEPPwCjAn5YJQ38UAFgkLICNHhlBXbJy2oAgpcVAEQsKiALYMKjGg/4JSBk8YBYvOMB0GxmNJnZhGhas5rQrKYTgPAAJ3jTAkAAQhPi0QRXWGCbFpCGNCxgTnC20wKwsIA8ZyBPWNBTnhaYgT4tsP8EZ/TzA82Axgcs0IwPLAEWH/jADJZg0A9AwwUJhWhCP+ACie7BBXnIw0UTCoE1uGANe6DHGiCwBwhAIKMjNalJ56HSlkIgBhBoAQTUEdMWKKMFMtXCS2OghRi0wKfKyEAGeCrUDGihqEI9alG3kIFo/OARRX3qFn6giAw8YgtbWAUDfvADBmyBAatQBAOioVUGmJUBYj2rWs2aBQa0ta0M8EEWQkEKBjCCrhRgADsYQAof+DWujGAEBeZKgcIa9rCHRcMXDIsGNFBACV9IxRfQoARHKEEJFGhAZBvQAFWoIhWOUIUjOEta0iqhtJyNQwOYsdoGxAEGDXBAA4IQiNiHujYQ7oBBIGSr2zgEwQEwgMFvgwADBxjXAb89rgNsoFzjisIGPHAAD6DrgBvYwAaikO4C2nGDG/DgBgtYQCPC2whRLAC80w2veM+r3vDOIrwpaC98U5CIRLRXBYlQQQpmAQUFKEC/s0gBFNgAhRSo4MBsYEMKFMAGBUBBBf6NcH/9C+EIKyAgADs=') repeat-x bottom;}
	div.itemVideoBlock div.itemVideoEmbedded {text-align:center;} 
	div.itemVideoBlock span.itemVideo {display:block;overflow:hidden;}
	div.itemVideoBlock span.itemVideoCaption {color:#eee;float:left;display:block;font-size:11px;font-weight:bold;width:60%;}
	div.itemVideoBlock span.itemVideoCredits {color:#eee;float:right;display:block;font-style:italic;font-size:11px;width:35%;text-align:right;}


div.itemImageGallery {margin:0 0 16px 0;padding:0;}


div.itemNavigation {padding:4px 8px;margin:0 0 24px 0;border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;background:#fffff0;}
	div.itemNavigation span.itemNavigationTitle {color:#999;}
	div.itemNavigation a.itemPrevious {padding:0 12px;}
	div.itemNavigation a.itemNext {padding:0 12px;}


div.itemComments {background:#f7fafe;border:1px solid #ddd;padding:16px;}

	div.itemComments ul.itemCommentsList {margin:0 0 16px;padding:0;list-style:none;}
	div.itemComments ul.itemCommentsList li {padding:4px;margin:0;border-bottom:1px dotted #ddd;}
	div.itemComments ul.itemCommentsList li.authorResponse {background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAAPklEQVQYlX3NwQkAMAhD0egI2X9FXcFeLEWs/usjRMwMN5Ko6WLurovlejIAEhGTle9uJHWxsu72+GvJkwE4MzAg1wqNPpcAAAAASUVORK5CYII=') repeat;}
	div.itemComments ul.itemCommentsList li.unpublishedComment {background:#ffeaea;}
	div.itemComments ul.itemCommentsList li img {float:left;margin:4px 4px 4px 0;padding:4px;background:#fff;border-bottom:1px solid #d7d7d7;border-left:1px solid #f2f2f2;border-right:1px solid #f2f2f2;}
	div.itemComments ul.itemCommentsList li span.commentDate {padding:0 4px 0 0;margin:0 8px 0 0;border-right:1px solid #ccc;font-weight:bold;font-size:14px;}
	div.itemComments ul.itemCommentsList li span.commentAuthorName {font-weight:bold;font-size:14px;}
	div.itemComments ul.itemCommentsList li p {padding:4px 0;}
	div.itemComments ul.itemCommentsList li span.commentAuthorEmail {display:none;}
	div.itemComments ul.itemCommentsList li span.commentLink {float:right;margin-left:8px;}
	div.itemComments ul.itemCommentsList li span.commentLink a {font-size:11px;color:#999;text-decoration:underline;}
	div.itemComments ul.itemCommentsList li span.commentLink a:hover {font-size:11px;color:#555;text-decoration:underline;}
	
	div.itemComments ul.itemCommentsList li span.commentToolbar {display:block;clear:both;}
	div.itemComments ul.itemCommentsList li span.commentToolbar a {font-size:11px;color:#999;text-decoration:underline;margin-right:4px;}
	div.itemComments ul.itemCommentsList li span.commentToolbar a:hover {font-size:11px;color:#555;text-decoration:underline;}
	div.itemComments ul.itemCommentsList li span.commentToolbarLoading {background:url('data:image/gif;base64,R0lGODlhEAALAPQAAP///wAAANra2tDQ0Orq6gYGBgAAAC4uLoKCgmBgYLq6uiIiIkpKSoqKimRkZL6+viYmJgQEBE5OTubm5tjY2PT09Dg4ONzc3PLy8ra2tqCgoMrKyu7u7gAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCwAAACwAAAAAEAALAAAFLSAgjmRpnqSgCuLKAq5AEIM4zDVw03ve27ifDgfkEYe04kDIDC5zrtYKRa2WQgAh+QQJCwAAACwAAAAAEAALAAAFJGBhGAVgnqhpHIeRvsDawqns0qeN5+y967tYLyicBYE7EYkYAgAh+QQJCwAAACwAAAAAEAALAAAFNiAgjothLOOIJAkiGgxjpGKiKMkbz7SN6zIawJcDwIK9W/HISxGBzdHTuBNOmcJVCyoUlk7CEAAh+QQJCwAAACwAAAAAEAALAAAFNSAgjqQIRRFUAo3jNGIkSdHqPI8Tz3V55zuaDacDyIQ+YrBH+hWPzJFzOQQaeavWi7oqnVIhACH5BAkLAAAALAAAAAAQAAsAAAUyICCOZGme1rJY5kRRk7hI0mJSVUXJtF3iOl7tltsBZsNfUegjAY3I5sgFY55KqdX1GgIAIfkECQsAAAAsAAAAABAACwAABTcgII5kaZ4kcV2EqLJipmnZhWGXaOOitm2aXQ4g7P2Ct2ER4AMul00kj5g0Al8tADY2y6C+4FIIACH5BAkLAAAALAAAAAAQAAsAAAUvICCOZGme5ERRk6iy7qpyHCVStA3gNa/7txxwlwv2isSacYUc+l4tADQGQ1mvpBAAIfkECQsAAAAsAAAAABAACwAABS8gII5kaZ7kRFGTqLLuqnIcJVK0DeA1r/u3HHCXC/aKxJpxhRz6Xi0ANAZDWa+kEAA7AAAAAAAAAAAA') no-repeat 100% 50%;}

div.itemCommentsPagination {padding:4px;margin:0 0 24px 0;}
	div.itemCommentsPagination span.pagination {display:block;float:right;clear:both;}

div.itemCommentsForm h3 {margin:0;padding:0 0 4px 0;}
	div.itemCommentsForm p.itemCommentsFormNotes {border-top:2px solid #ccc;}
	div.itemCommentsForm form {}
	div.itemCommentsForm form label.formComment {display:block;margin:12px 0 0 2px;}
	div.itemCommentsForm form label.formName {display:block;margin:12px 0 0 2px;}
	div.itemCommentsForm form label.formEmail {display:block;margin:12px 0 0 2px;}
	div.itemCommentsForm form label.formUrl {display:block;margin:12px 0 0 2px;}
	div.itemCommentsForm form label.formRecaptcha {display:block;margin:12px 0 0 2px;}
	div.itemCommentsForm form textarea.inputbox {display:block;width:350px;height:160px;margin:0;}
	div.itemCommentsForm form input.inputbox {display:block;width:350px;margin:0;}
	div.itemCommentsForm form input#submitCommentButton {display:block;margin:16px 0 0 0;padding:4px;border:1px solid #ccc;background:#eee;font-size:16px;}
	div.itemCommentsForm form span#formLog {margin:0 0 0 20px;padding:0 0 0 20px;font-weight:bold;color:#CF1919;}
	div.itemCommentsForm form .formLogLoading {background:url('data:image/gif;base64,R0lGODlhEAAQAKUAAAQCBISChERGRMTCxGRmZKSipCQiJOzq7FRWVHR2dBQSFLS2tJSSlNTW1PT29AwKDExOTGxubDQyNIyKjMzKzKyqrFxeXHx+fLy+vPz+/AQGBExKTMTGxGxqbCQmJPTy9Hx6fBweHLy6vJyenNze3Pz6/AwODFRSVHRydDw6PIyOjKyurGRiZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQIBgAAACwAAAAAEAAQAAAGhcCMcEgsDktIpLFY8BgMIcbHIiotJ4Ds5ZOFfIysLODi6GgAm+IAoEGZLkKKCVAghi2ZFWMIQhMlABhCVkINACF/ABxGJIeEGRYAHUMDASUBAAJECwAPFEIYAAQKACNFJwAmIAciYikORR8bWQutWQiwRSUFEBgDIU8eKEtEDg4lx8TKGUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIaExMbEREJE5ObkJCYkZGJkrKqsFBIU1NbUVFJU9Pb0nJ6ctLa0DAoMjI6MzM7MNDI0fHp8HBoc3N7cXFpcTEpM9PL0bGps/P78vL68BAYEjIqMzMrMREZE7OrsLC4srK6sFBYU3NrcVFZU/Pr8vLq8DA4MlJKU1NLUPDo8HB4c5OLkXF5cbG5sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABofAjFC4qHSGyKSwssGwlEnNAAAQmaCZkoFKFbE6C+V20wIBHpeCpYTUADZXDuhCOQEOyG1LOLpmJAAWSBEAGkoJACuDAAJKIwATbEsALkIEIUIBAANIDQAOKRkYnCMIAAxJFhsQHQ4nEqYqYUgLDCUqXAAqBFgEhFUMs1gXKCIrF1hJBBgQSUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIKExMbEREJELCos7OrspKakZGJkFBIU1NbU9Pb0lJKUVFJUNDY0vLq8DAoM3N7cjI6MTE5M9PL0tLK0fHp8HB4c/P78PD48BAYEhIaEzM7MREZENDI07O7srKqsZGZkFBYU3Nrc/Pr8nJ6cXF5cPDo8vL68DA4M5OLkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoDAi3A4nFSISOTmIUgOBYdOpwHAjJKjEmDLBRiSB0Cm5HCgtiXkSewQijIEw0hBDB+GJ43HA2IQOwAnSSIAFn8ATUiEFldCJRIpIyQaQxoAA0QTIycmACRuCJ5ICxlbFCIBoSZ0RClnXQANHk6WIRYWHCSNSRMEESO7TkIfG8IXQQAh+QQIBgAAACwAAAAAEAAQAIUEAgSEgoTEwsREQkSkoqQsLizk5uRkYmQUEhS0srT09vSUkpRUVlQ8Ojx8enwMCgzU0tRMSkzs7uy8uryMioysqqw0NjRsbmwcHhz8/vycmpwEBgSEhoTExsRERkQ0MjTs6uxkZmQUFhS0trT8+vxcXlw8Pjx8fnwMDgzc2txMTkz08vS8vrysrqycnpwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGfMCMcEgsGo/IjCD0+ZQmpMxiRSSVANisioOQEA+AzZN1eWBRXqEgPCEGsAjQEFwiJswA9LAzMthdGhouUUIJLgQaBIQZJBJUQw1ZJ4scAANEkQAliykIAC5EJlgPDhApXAANixkNDBFZWRZyRBQKJBUqGBgeg0lCJKu+SUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIKEREZExMLE5OLkJCYkZGJk9PL0FBIUrKqs1NLUNDY0dHZ0XF5cDAoMTE5M7OrsLC4sbGps/Pr8tLa03NrcnJqczMrMHBocPD48BAYETEpM5ObkLCosZGZk9Pb0tLK01NbUPDo8fHp8DA4MVFJU7O7sNDI0bG5s/P78vL683N7cnJ6czM7MHB4cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoPAlHBILBqPyNTAczo1KMhJA0CtPg7Gi0tjUKkkDsBmQqxwDi3ihQRIpAipyQJj+RQZgEJGkGJVFwNCCigFVBoDEC5UCA0gQisNGlQNKSMnhRJGAwsIFSkcExQAGhdEKxUfaUQlACQjChUBCA4WRgcbVVUiEEcTCQ8uGAIsdklxxshDQQA7') no-repeat left center;}


div.k2ReportCommentFormContainer {padding:8px;width:480px;margin:0 auto;}
	div.k2ReportCommentFormContainer blockquote {width:462px;background:#f7fafe;border:1px solid #ddd;padding:8px;margin:0 0 8px 0;}
	div.k2ReportCommentFormContainer blockquote span.quoteIconLeft {font-style:italic;font-weight:bold;font-size:40px;color:#135CAE;line-height:30px;vertical-align:top;display:block;float:left;}
	div.k2ReportCommentFormContainer blockquote span.quoteIconRight {font-style:italic;font-weight:bold;font-size:40px;color:#135CAE;line-height:30px;vertical-align:top;display:block;float:right;}
	div.k2ReportCommentFormContainer blockquote span.theComment {font-family:Georgia, "Times New Roman", Times, serif;font-style:italic;font-size:12px;font-weight:normal;color:#000;padding:0 4px;}
	div.k2ReportCommentFormContainer form label {display:block;font-weight:bold;}
	div.k2ReportCommentFormContainer form input,
	div.k2ReportCommentFormContainer form textarea {display:block;border:1px solid #ddd;font-size:12px;padding:2px;margin:0 0 8px 0;width:474px;}
	div.k2ReportCommentFormContainer form #recaptcha {margin-bottom:24px;}
	div.k2ReportCommentFormContainer form span#formLog {margin:0 0 0 20px;padding:0 0 0 20px;font-weight:bold;color:#CF1919;}
	div.k2ReportCommentFormContainer form .formLogLoading {background:url('data:image/gif;base64,R0lGODlhEAAQAKUAAAQCBISChERGRMTCxGRmZKSipCQiJOzq7FRWVHR2dBQSFLS2tJSSlNTW1PT29AwKDExOTGxubDQyNIyKjMzKzKyqrFxeXHx+fLy+vPz+/AQGBExKTMTGxGxqbCQmJPTy9Hx6fBweHLy6vJyenNze3Pz6/AwODFRSVHRydDw6PIyOjKyurGRiZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQIBgAAACwAAAAAEAAQAAAGhcCMcEgsDktIpLFY8BgMIcbHIiotJ4Ds5ZOFfIysLODi6GgAm+IAoEGZLkKKCVAghi2ZFWMIQhMlABhCVkINACF/ABxGJIeEGRYAHUMDASUBAAJECwAPFEIYAAQKACNFJwAmIAciYikORR8bWQutWQiwRSUFEBgDIU8eKEtEDg4lx8TKGUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIaExMbEREJE5ObkJCYkZGJkrKqsFBIU1NbUVFJU9Pb0nJ6ctLa0DAoMjI6MzM7MNDI0fHp8HBoc3N7cXFpcTEpM9PL0bGps/P78vL68BAYEjIqMzMrMREZE7OrsLC4srK6sFBYU3NrcVFZU/Pr8vLq8DA4MlJKU1NLUPDo8HB4c5OLkXF5cbG5sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABofAjFC4qHSGyKSwssGwlEnNAAAQmaCZkoFKFbE6C+V20wIBHpeCpYTUADZXDuhCOQEOyG1LOLpmJAAWSBEAGkoJACuDAAJKIwATbEsALkIEIUIBAANIDQAOKRkYnCMIAAxJFhsQHQ4nEqYqYUgLDCUqXAAqBFgEhFUMs1gXKCIrF1hJBBgQSUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIKExMbEREJELCos7OrspKakZGJkFBIU1NbU9Pb0lJKUVFJUNDY0vLq8DAoM3N7cjI6MTE5M9PL0tLK0fHp8HB4c/P78PD48BAYEhIaEzM7MREZENDI07O7srKqsZGZkFBYU3Nrc/Pr8nJ6cXF5cPDo8vL68DA4M5OLkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoDAi3A4nFSISOTmIUgOBYdOpwHAjJKjEmDLBRiSB0Cm5HCgtiXkSewQijIEw0hBDB+GJ43HA2IQOwAnSSIAFn8ATUiEFldCJRIpIyQaQxoAA0QTIycmACRuCJ5ICxlbFCIBoSZ0RClnXQANHk6WIRYWHCSNSRMEESO7TkIfG8IXQQAh+QQIBgAAACwAAAAAEAAQAIUEAgSEgoTEwsREQkSkoqQsLizk5uRkYmQUEhS0srT09vSUkpRUVlQ8Ojx8enwMCgzU0tRMSkzs7uy8uryMioysqqw0NjRsbmwcHhz8/vycmpwEBgSEhoTExsRERkQ0MjTs6uxkZmQUFhS0trT8+vxcXlw8Pjx8fnwMDgzc2txMTkz08vS8vrysrqycnpwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGfMCMcEgsGo/IjCD0+ZQmpMxiRSSVANisioOQEA+AzZN1eWBRXqEgPCEGsAjQEFwiJswA9LAzMthdGhouUUIJLgQaBIQZJBJUQw1ZJ4scAANEkQAliykIAC5EJlgPDhApXAANixkNDBFZWRZyRBQKJBUqGBgeg0lCJKu+SUEAIfkECAYAAAAsAAAAABAAEACFBAIEhIKEREZExMLE5OLkJCYkZGJk9PL0FBIUrKqs1NLUNDY0dHZ0XF5cDAoMTE5M7OrsLC4sbGps/Pr8tLa03NrcnJqczMrMHBocPD48BAYETEpM5ObkLCosZGZk9Pb0tLK01NbUPDo8fHp8DA4MVFJU7O7sNDI0bG5s/P78vL683N7cnJ6czM7MHB4cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoPAlHBILBqPyNTAczo1KMhJA0CtPg7Gi0tjUKkkDsBmQqxwDi3ihQRIpAipyQJj+RQZgEJGkGJVFwNCCigFVBoDEC5UCA0gQisNGlQNKSMnhRJGAwsIFSkcExQAGhdEKxUfaUQlACQjChUBCA4WRgcbVVUiEEcTCQ8uGAIsdklxxshDQQA7') no-repeat left center;}


div.itemBackToTop {text-align:right;}
	div.itemBackToTop a {text-decoration:underline;}
	div.itemBackToTop a:hover {text-decoration:underline;}







div.itemListCategoriesBlock {border-bottom:1px solid #ccc; margin-bottom:20px;padding-bottom:20px;}


div.itemListCategory {}
	span.catItemAddLink {display:block;padding:8px 0;margin:0 0 4px 0;border-bottom:1px dotted #ccc;text-align:right;}
	span.catItemAddLink a {padding:4px 16px;border:1px solid #ccc;background:#eee;text-decoration:none;}
	span.catItemAddLink a:hover {background:#ffffcc;}
	div.itemListCategory img {float:left;display:block;background:#fff;padding:4px;border:1px solid #ddd;margin:0 8px 0 0;}
	div.itemListCategory h2 {
  border-left: #EC0623 4px solid;
  font-size:20px;
  font-weight:normal;
  padding-left:10px;
  margin-bottom:20px;
  color:#414042;}
	div.itemListCategory p {}


div.itemListSubCategories {}
	div.itemListSubCategories h3 {}
		div.subCategoryContainer {float:left;}
		div.subCategoryContainerLast {} 
			div.subCategory {background:#f7fafe;border:1px solid #ddd;margin:4px;padding:8px;}
				div.subCategory a.subCategoryImage,
				div.subCategory a.subCategoryImage:hover {text-align:center;display:block;}
				div.subCategory a.subCategoryImage img,
				div.subCategory a.subCategoryImage:hover img {background:#fff;padding:4px;border:1px solid #ddd;margin:0 8px 0 0;}
				div.subCategory h2 {}
				div.subCategory h2 a {}
				div.subCategory h2 a:hover {}
				div.subCategory p {}


div.itemList {}
	div#itemListLeading {}
	div#itemListPrimary {}
	div#itemListSecondary {}
	div#itemListLinks {background:#f7fafe;border:1px solid #ddd;margin:8px 0;padding:8px;}

		div.itemContainer {float:left; border-bottom:1px solid #ccc; padding-bottom:20px; margin-bottom:20px;}
		div.itemContainerLast {} 


div.catItemView {padding:4px;} 

	
	div.groupLeading {}
	div.groupPrimary {}
	div.groupSecondary {}
	div.groupLinks {padding:0;margin:0;}

	div.catItemIsFeatured {} 

span.catItemEditLink {float:right;display:block;padding:4px 0;margin:0;width:120px;text-align:right;}
span.catItemEditLink a {padding:2px 12px;border:1px solid #ccc;background:#eee;text-decoration:none;font-size:11px;font-weight:normal;font-family:Arial, Helvetica, sans-serif;}
span.catItemEditLink a:hover {background:#ffffcc;}

div.catItemHeader {}
	div.catItemHeader span.catItemDateCreated {color:#999;font-size:11px;}
	div.catItemHeader h3.catItemTitle {border-left: #EC0623 4px solid;
  font-size:20px;
  font-weight:normal;
  padding-left:10px;
  margin-bottom:20px;
  color:#414042;
border-bottom:none;}
	div.catItemHeader h3.catItemTitle span {}
	div.catItemHeader h3.catItemTitle span sup {font-size:12px;color:#CF1919;text-decoration:none;} 
	div.catItemHeader span.catItemAuthor {display:block;padding:0;margin:0;}
	div.catItemHeader span.catItemAuthor a {}
	div.catItemHeader span.catItemAuthor a:hover {}

div.catItemRatingBlock {padding:8px 0;}
	div.catItemRatingBlock span {display:block;float:left;font-style:normal;padding:0 4px 0 0;margin:0;color:#999;}

div.catItemBody {padding:8px 0;margin:0;}

div.catItemImageBlock {padding:0px;margin:0px 20px 5px 0px; float:left;}
	span.catItemImage {display:block;text-align:center;margin:0 0 0px 0;}
	span.catItemImage img {border:4px solid #ccc;padding:0px;margin-top:8px;}

div.catItemIntroText {font-size:inherit;font-weight:normal;line-height:inherit;padding:4px 0 12px 0;}
	div.catItemIntroText img {}

div.catItemExtraFields, div.genericItemExtraFields {margin:16px 0 0 0;padding:8px 0 0 0;border-top:1px dotted #ddd;}
	div.catItemExtraFields h4, div.genericItemExtraFields h4 {margin:0;padding:0 0 8px 0;line-height:normal !important;}
	div.catItemExtraFields ul, div.genericItemExtraFields ul {margin:0;padding:0;list-style:none;}
	div.catItemExtraFields ul li, div.genericItemExtraFields ul li {display:block;}
	div.catItemExtraFields ul li span.catItemExtraFieldsLabel, div.genericItemExtraFields ul li span.genericItemExtraFieldsLabel {display:block;float:left;font-weight:bold;margin:0 4px 0 0;width:30%;}
	div.catItemExtraFields ul li span.catItemExtraFieldsValue {}

div.catItemLinks {margin:0 0 16px 0;padding:0;}

div.catItemHitsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	span.catItemHits {}

div.catItemCategory {padding:4px;border-bottom:1px dotted #ccc;}
	div.catItemCategory span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.catItemCategory a {}

div.catItemTagsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	div.catItemTagsBlock span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.catItemTagsBlock ul.catItemTags {list-style:none;padding:0;margin:0;display:inline;}
	div.catItemTagsBlock ul.catItemTags li {display:inline;list-style:none;padding:0 4px 0 0;margin:0;text-align:center;}
	div.catItemTagsBlock ul.catItemTags li a {}
	div.catItemTagsBlock ul.catItemTags li a:hover {}

div.catItemAttachmentsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	div.catItemAttachmentsBlock span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.catItemAttachmentsBlock ul.catItemAttachments {list-style:none;padding:0;margin:0;display:inline;}
	div.catItemAttachmentsBlock ul.catItemAttachments li {display:inline;list-style:none;padding:0 4px;margin:0;text-align:center;}
	div.catItemAttachmentsBlock ul.catItemAttachments li a {}
	div.catItemAttachmentsBlock ul.catItemAttachments li a:hover {}
	div.catItemAttachmentsBlock ul.catItemAttachments li span {font-size:10px;color:#999;font-weight:normal;}


div.catItemVideoBlock {margin:0 0 16px 0;padding:16px;background:#010101 url('data:image/gif;base64,R0lGODlhBQCnAfcAABQUFBISEhAQEA4ODgwMDAoKCggICAYGBgQEBAICAjIyMjExMS0tLS8vLzAwMCgoKCsrKyEhISQkJB4eHi4uLicnJykpKRoaGhcXFywsLCUlJRYWFiYmJhwcHBsbGyoqKiMjIx8fHxkZGRgYGCIiIh0dHSAgIBgXGB8gIDExMjIyMQ0NDQUFBCwrKyoqKwgHBy8vMCsrLAwMCykpKiYnJx8fHjAxMDEwMQcHByQkJRsbHB8eHzAwMRIRES4uLSwtLCkoKTAwLyAhIRkaGhwdHRoZGhkYGQ8QEAQDBAkKCS8uLyopKiUmJSgpKCkoKAYGBTEyMiQjIyMiIiAhIBUWFhESERQUEwwLCwMDAi0uLSwrLCwsLSUlJiYlJiMiIy8uLgUGBSQlJRcWFxIREgsMDAMEBAwLDAoJCi4vLgcICCYlJSsqKzIxMiEiISEiIiIiIR0eHTAvLx0cHBkZGh0cHR8fIBYXFhYWFQMDAysrKiUmJisqKh8gHyMjIiYmJyYnJigoJzAvMB4fHh4eHRAQDxwcHRwbHBYVFhsaGjEyMS0tLA4NDS4tLTExMC4uLy0sLAUFBQYFBicmJyAgISEgIRobGhobGxcYGBcXFhUVFRQTFAsLCxoZGQMEAwQEAw0ODg8PDy0uLjEwMAkJCS4tLgkKCgkJCAoKCRAPEC4vLy8vLiwtLQ4ODSAfICkpKBwcGyopKSgnKCcoKDIxMSQjJCMkIyUkJCIjIx0dHhscHB4fHx4dHhcXGB0dHBoaGRMUFCcnJhgXFxUWFRgYGRkYGA8QDyYmJSUlJBISEQsMCywsKyMjJC8wLykqKSkqKgcIByoqKS0sLSgpKQUGBiMkJCgnJyEhIhQTEyIhISIhIiIiIxMTExwbGxwdHBkaGRoaGxMUExsaGxsbGhYWFxcWFhgZGBgZGRgYFxERERESEissLA0ODS0tLjAxMS8wMCcoJycnKCgoKQgIByorKiorKyAgHycmJiQlJCUkJSIjIh0eHh4dHRscGyEgICEhIAEBASH5BAAAAAAALAAAAAAFAKcBAAj/ACEJFPivoMGDCBMqXMgQYYIEBSH+kxhxosWKFCE+nPiwY8aNHjl6HOkRC8mTJvGYTIBF5UMsK1niYflQ5cybLRPYxMOzZUueQIMKBYoEjydPRY0GLcMTSVI8CBAg6VSmaKdOSKTiQVJGalQEXb9C/Uq2bFQWLM5+hRR1IAKBaBGwgDQXLVy6b93eHch3IJi+kSD9fQIp0pNIgSMdiARm2mFIB8AEhnxgseInBwhXnla5c2ccnkN/rgwah2nSpk8fSM26tOvWrF/geGZatmzTaUzLe2GA92zeBoIbyJ2mt/DjxY8fNxWc+ajlypkzNzDK1Kjr16lbx16dO/cko5Kc/8F+ptT46wVOjToD3nyBUgXCp09y6kyB+wXs499fYFP//wD6t8mAAf4nYH8DJqjggptcoSAZDcpghoObyGChGWQkI+EVBHBIwCbJkEEGASTKQCKJZpxI4goqEsDiiy6yuOKKL65g4404xojjCuvc+ImNrCwC5CIDfCKkjUUOoOQAK7Cy5JNQDgCKlFNKqWSVVloJypZUbunll2CCUgwohBQjAChHgILKmgIcgQohhBxRzBEC1IlKmwLcSUidfPYpADp1AsonoIT+aaig6CSqKKKLolOFo+j0gEwPVVTRQwCJUopOAGNwOsalyAQg6hjpiGqqqducmuqpqG7j6qsBuP8aa6yyvmqrFa5e88s24OS6jSauWmEFAL8AYIUmAICDLADEDsvss89mAi0A0mZS7bXMYnuttdZSwa21d2RyiDDhZuLtuHdsIMwhh1CxwQZUUHEHu+++e0i979pBjh12bIDJu+NsgMG//I5DjhgIi4EBBv4u7DAGvDjMyyUYnFDxwhFfck4wJ1wywgnBnHPxCB+f8PEIwZCsshEkGzEMMSwbUQ4xxJAsggjmjCDCMCKUc7MRO5tzszc331zEHN5cIAInRXBidBG+zDHEHJxMDfUFvgwxxAVcX7B111xXcoHY33CNyDfhWGLJBYh4YEkl4SAijgdce2C3B3PffTc3OnD/Y7cOHhiiwysdFO5BB4YkPngHueTSAT866JAL4YVX3kEhhXcjh+ZyENELEZeXUIgcl/dCBxElnI46Han3UsLrJQwCe+y74AIHLrDDAYc+cAyyywT7+I7LBMQXb/zxxe9AvCCC1LBDDbrssEP0gjwfwvW6hCBICDVw3/3119cRAh/g18GH+CigUEc946ePggkotNLK+3y8b0IrJkxhwv4mCDFJ/vmLgBCEMAUhmGASk6BEP/wxBUpEwIFCiIAEJ0hBCmJDgtggQQTa8IY2uMENEchGB7PhBmuQwBodJAEJ3kACN6jwhSSQghS0QYJbkCAfKvRCH6TQh1uAAAQz7AMI/7zgBSD+kIg+/CEI+hCFKICgFiBwYhSo8UMo0iIKtZDAMkAgAQlQo4vL6KIEaEELMZpRjDnIwTFyIIEw5MAWGmBjG8NwjDjaAh8auIcGbBEGDfjxj4D0oxqY4McucIEJTOCCGrrABDUoUg9M6IIe9MABDnCBCxzogjEo2QUOGKOSlfQDKGlAgz/8AZSVlMQfJGEPPwCjAn5YJQ38UAFgkLICNHhlBXbJy2oAgpcVAEQsKiALYMKjGg/4JSBk8YBYvOMB0GxmNJnZhGhas5rQrKYTgPAAJ3jTAkAAQhPi0QRXWGCbFpCGNCxgTnC20wKwsIA8ZyBPWNBTnhaYgT4tsP8EZ/TzA82Axgcs0IwPLAEWH/jADJZg0A9AwwUJhWhCP+ACie7BBXnIw0UTCoE1uGANe6DHGiCwBwhAIKMjNalJ56HSlkIgBhBoAQTUEdMWKKMFMtXCS2OghRi0wKfKyEAGeCrUDGihqEI9alG3kIFo/OARRX3qFn6giAw8YgtbWAUDfvADBmyBAatQBAOioVUGmJUBYj2rWs2aBQa0ta0M8EEWQkEKBjCCrhRgADsYQAof+DWujGAEBeZKgcIa9rCHRcMXDIsGNFBACV9IxRfQoARHKEEJFGhAZBvQAFWoIhWOUIUjOEta0iqhtJyNQwOYsdoGxAEGDXBAA4IQiNiHujYQ7oBBIGSr2zgEwQEwgMFvgwADBxjXAb89rgNsoFzjisIGPHAAD6DrgBvYwAaikO4C2nGDG/DgBgtYQCPC2whRLAC80w2veM+r3vDOIrwpaC98U5CIRLRXBYlQQQpmAQUFKEC/s0gBFNgAhRSo4MBsYEMKFMAGBUBBBf6NcH/9C+EIKyAgADs=') repeat-x bottom;}
	div.catItemVideoBlock div.catItemVideoEmbedded {text-align:center;} 
	div.catItemVideoBlock span.catItemVideo {display:block;}


div.catItemImageGallery {margin:0 0 16px 0;padding:0;}


div.catItemCommentsLink {display:inline;margin:0 8px 0 0;padding:0 8px 0 0;border-right:1px solid #ccc;}
	div.catItemCommentsLink a {}
	div.catItemCommentsLink a:hover {}


div.catItemReadMore {display:inline; float:right;}
	div.catItemReadMore a {}
	div.catItemReadMore a:hover {}


span.catItemDateModified {display:block;text-align:right;padding:4px;margin:4px 0;color:#999;border-top:1px solid #ddd;}








div.userView {}
	div.userBlock {background:#f7fafe;border:1px solid #ddd;margin:0 0 16px 0;padding:8px;clear:both;}

		span.userItemAddLink {display:block;padding:8px 0;margin:0 0 4px 0;border-bottom:1px dotted #ccc;text-align:right;}
		span.userItemAddLink a {padding:4px 16px;border:1px solid #ccc;background:#eee;text-decoration:none;}
		span.userItemAddLink a:hover {background:#ffffcc;}

		div.userBlock img {display:block;float:left;background:#fff;padding:4px;border:1px solid #ddd;margin:0 8px 0 0;}
		div.userBlock h2 {}
		div.userBlock div.userDescription {padding:4px 0;}
		div.userBlock div.userAdditionalInfo {padding:4px 0;margin:8px 0 0 0;}
			span.userURL {font-weight:bold;color:#555;display:block;}
			span.userEmail {font-weight:bold;color:#555;display:block;}

		div.userItemList {}


div.userItemView {} 
div.userItemIsFeatured {} 

div.userItemViewUnpublished {opacity:0.9;border:4px dashed #ccc;background:#fffff2;padding:8px;margin:8px 0;}

span.userItemEditLink {float:right;display:block;padding:4px 0;margin:0;width:120px;text-align:right;}
	span.userItemEditLink a {padding:2px 12px;border:1px solid #ccc;background:#eee;text-decoration:none;font-size:11px;font-weight:normal;font-family:Arial, Helvetica, sans-serif;}
	span.userItemEditLink a:hover {background:#ffffcc;}

div.userItemHeader {}
	div.userItemHeader span.userItemDateCreated {color:#999;font-size:11px;}
	div.userItemHeader h3.userItemTitle {font-family:Georgia, "Times New Roman", Times, serif;font-size:24px;font-weight:normal;line-height:110%;padding:10px 0 4px 0;margin:0;}
	div.userItemHeader h3.userItemTitle span sup {font-size:12px;color:#CF1919;text-decoration:none;} 

div.userItemBody {padding:8px 0;margin:0;}

div.userItemImageBlock {padding:0;margin:0;float:left;}
	span.userItemImage {display:block;text-align:center;margin:0 8px 8px 0;}
	span.userItemImage img {border:1px solid #ccc;padding:8px;}

div.userItemIntroText {font-size:inherit;font-weight:normal;line-height:inherit;padding:4px 0 12px 0;}
	div.userItemIntroText img {}

div.userItemLinks {margin:0 0 16px 0;padding:0;}

div.userItemCategory {padding:4px;border-bottom:1px dotted #ccc;}
	div.userItemCategory span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.userItemCategory a {}

div.userItemTagsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	div.userItemTagsBlock span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.userItemTagsBlock ul.userItemTags {list-style:none;padding:0;margin:0;display:inline;}
	div.userItemTagsBlock ul.userItemTags li {display:inline;list-style:none;padding:0 4px 0 0;margin:0;text-align:center;}
	div.userItemTagsBlock ul.userItemTags li a {}
	div.userItemTagsBlock ul.userItemTags li a:hover {}


div.userItemCommentsLink {display:inline;margin:0 8px 0 0;padding:0 8px 0 0;border-right:1px solid #ccc;}
	div.userItemCommentsLink a {}
	div.userItemCommentsLink a:hover {}


div.userItemReadMore {display:inline;}
	div.userItemReadMore a {}
	div.userItemReadMore a:hover {}






div.tagView {}

div.tagItemList {}

div.tagItemView {border-bottom:1px dotted #ccc;padding:8px 0;margin:0 0 16px 0;} 

div.tagItemHeader {}
	div.tagItemHeader span.tagItemDateCreated {color:#999;font-size:11px;}
	div.tagItemHeader h2.tagItemTitle {font-family:Georgia, "Times New Roman", Times, serif;font-size:24px;font-weight:normal;line-height:110%;padding:10px 0 4px 0;margin:0;}

div.tagItemBody {padding:8px 0;margin:0;}

div.tagItemImageBlock {padding:0;margin:0;float:left;}
	span.tagItemImage {display:block;text-align:center;margin:0 8px 8px 0;}
	span.tagItemImage img {border:1px solid #ccc;padding:8px;}

div.tagItemIntroText {font-size:inherit;font-weight:normal;line-height:inherit;padding:4px 0 12px 0;}
	div.tagItemIntroText img {}
	
	div.tagItemExtraFields {}
		div.tagItemExtraFields h4 {}
		div.tagItemExtraFields ul {}
			div.tagItemExtraFields ul li {}
				div.tagItemExtraFields ul li span.tagItemExtraFieldsLabel {}
				div.tagItemExtraFields ul li span.tagItemExtraFieldsValue {}

	div.tagItemCategory {display:inline;margin:0 8px 0 0;padding:0 8px 0 0;border-right:1px solid #ccc;}
		div.tagItemCategory span {font-weight:bold;color:#555;padding:0 4px 0 0;}
		div.tagItemCategory a {}


div.tagItemReadMore {display:inline;}
	div.tagItemReadMore a {}
	div.tagItemReadMore a:hover {}






div.genericView {}

div.genericItemList {}

div.genericItemView {border-bottom:1px dotted #ccc;padding:8px 0;margin:0 0 16px 0;} 

div.genericItemHeader {}
	div.genericItemHeader span.genericItemDateCreated {color:#999;font-size:11px;}
	div.genericItemHeader h2.genericItemTitle {font-family:Georgia, "Times New Roman", Times, serif;font-size:24px;font-weight:normal;line-height:110%;padding:10px 0 4px 0;margin:0;}

div.genericItemBody {padding:8px 0;margin:0;}

div.genericItemImageBlock {padding:0;margin:0;float:left;}
	span.genericItemImage {display:block;text-align:center;margin:0 8px 8px 0;}
	span.genericItemImage img {border:1px solid #ccc;padding:8px;}

div.genericItemIntroText {font-size:inherit;font-weight:normal;line-height:inherit;padding:4px 0 12px 0;}
	div.genericItemIntroText img {}
	
	div.genericItemExtraFields {}
		div.genericItemExtraFields h4 {}
		div.genericItemExtraFields ul {}
			div.genericItemExtraFields ul li {}
				div.genericItemExtraFields ul li span.genericItemExtraFieldsLabel {}
				div.genericItemExtraFields ul li span.genericItemExtraFieldsValue {}

	div.genericItemCategory {display:inline;margin:0 8px 0 0;padding:0 8px 0 0;border-right:1px solid #ccc;}
		div.genericItemCategory span {font-weight:bold;color:#555;padding:0 4px 0 0;}
		div.genericItemCategory a {}


div.genericItemReadMore {display:inline;}
	div.genericItemReadMore a {}
	div.genericItemReadMore a:hover {}


#k2Container div.gsc-branding-text {text-align:right;}
#k2Container div.gsc-control {width:100%;}
#k2Container div.gs-visibleUrl {display:none;}







div.latestItemsContainer {float:left;}


div.latestItemsCategory {background:#f7fafe;border:1px solid #ddd;margin:0 8px 8px 0;padding:8px;}
	div.latestItemsCategoryImage {text-align:center;}
	div.latestItemsCategoryImage img {background:#fff;padding:4px;border:1px solid #ddd;margin:0 8px 0 0;}
div.latestItemsCategory h2 {}
div.latestItemsCategory p {}


div.latestItemsUser {background:#f7fafe;border:1px solid #ddd;margin:0 8px 8px 0;padding:8px;clear:both;}
	div.latestItemsUser img {display:block;float:left;background:#fff;padding:4px;border:1px solid #ddd;margin:0 8px 0 0;}
	div.latestItemsUser h2 {}
	div.latestItemsUser p.latestItemsUserDescription {padding:4px 0;}
	div.latestItemsUser p.latestItemsUserAdditionalInfo {padding:4px 0;margin:8px 0 0 0;}
		span.latestItemsUserURL {font-weight:bold;color:#555;display:block;}
		span.latestItemsUserEmail {font-weight:bold;color:#555;display:block;}


div.latestItemList {padding:0 8px 8px 0;}

div.latestItemView {} 

div.latestItemHeader {}
	div.latestItemHeader h3.latestItemTitle {font-family:Georgia, "Times New Roman", Times, serif;font-size:24px;font-weight:normal;line-height:110%;padding:10px 0 4px 0;margin:0;}

span.latestItemDateCreated {color:#999;font-size:11px;}

div.latestItemBody {padding:8px 0;margin:0;}

div.latestItemImageBlock {padding:0;margin:0;float:left;}
	span.latestItemImage {display:block;text-align:center;margin:0 8px 8px 0;}
	span.latestItemImage img {border:1px solid #ccc;padding:8px;}

div.latestItemIntroText {font-size:inherit;font-weight:normal;line-height:inherit;padding:4px 0 12px 0;}
	div.latestItemIntroText img {}

div.latestItemLinks {margin:0 0 16px 0;padding:0;}

div.latestItemCategory {padding:4px;border-bottom:1px dotted #ccc;}
	div.latestItemCategory span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.latestItemCategory a {}

div.latestItemTagsBlock {padding:4px;border-bottom:1px dotted #ccc;}
	div.latestItemTagsBlock span {font-weight:bold;color:#555;padding:0 4px 0 0;}
	div.latestItemTagsBlock ul.latestItemTags {list-style:none;padding:0;margin:0;display:inline;}
	div.latestItemTagsBlock ul.latestItemTags li {display:inline;list-style:none;padding:0 4px 0 0;margin:0;text-align:center;}
	div.latestItemTagsBlock ul.latestItemTags li a {}
	div.latestItemTagsBlock ul.latestItemTags li a:hover {}


div.latestItemVideoBlock {margin:0 0 16px 0;padding:16px;background:#010101 url('data:image/gif;base64,R0lGODlhBQCnAfcAABQUFBISEhAQEA4ODgwMDAoKCggICAYGBgQEBAICAjIyMjExMS0tLS8vLzAwMCgoKCsrKyEhISQkJB4eHi4uLicnJykpKRoaGhcXFywsLCUlJRYWFiYmJhwcHBsbGyoqKiMjIx8fHxkZGRgYGCIiIh0dHSAgIBgXGB8gIDExMjIyMQ0NDQUFBCwrKyoqKwgHBy8vMCsrLAwMCykpKiYnJx8fHjAxMDEwMQcHByQkJRsbHB8eHzAwMRIRES4uLSwtLCkoKTAwLyAhIRkaGhwdHRoZGhkYGQ8QEAQDBAkKCS8uLyopKiUmJSgpKCkoKAYGBTEyMiQjIyMiIiAhIBUWFhESERQUEwwLCwMDAi0uLSwrLCwsLSUlJiYlJiMiIy8uLgUGBSQlJRcWFxIREgsMDAMEBAwLDAoJCi4vLgcICCYlJSsqKzIxMiEiISEiIiIiIR0eHTAvLx0cHBkZGh0cHR8fIBYXFhYWFQMDAysrKiUmJisqKh8gHyMjIiYmJyYnJigoJzAvMB4fHh4eHRAQDxwcHRwbHBYVFhsaGjEyMS0tLA4NDS4tLTExMC4uLy0sLAUFBQYFBicmJyAgISEgIRobGhobGxcYGBcXFhUVFRQTFAsLCxoZGQMEAwQEAw0ODg8PDy0uLjEwMAkJCS4tLgkKCgkJCAoKCRAPEC4vLy8vLiwtLQ4ODSAfICkpKBwcGyopKSgnKCcoKDIxMSQjJCMkIyUkJCIjIx0dHhscHB4fHx4dHhcXGB0dHBoaGRMUFCcnJhgXFxUWFRgYGRkYGA8QDyYmJSUlJBISEQsMCywsKyMjJC8wLykqKSkqKgcIByoqKS0sLSgpKQUGBiMkJCgnJyEhIhQTEyIhISIhIiIiIxMTExwbGxwdHBkaGRoaGxMUExsaGxsbGhYWFxcWFhgZGBgZGRgYFxERERESEissLA0ODS0tLjAxMS8wMCcoJycnKCgoKQgIByorKiorKyAgHycmJiQlJCUkJSIjIh0eHh4dHRscGyEgICEhIAEBASH5BAAAAAAALAAAAAAFAKcBAAj/ACEJFPivoMGDCBMqXMgQYYIEBSH+kxhxosWKFCE+nPiwY8aNHjl6HOkRC8mTJvGYTIBF5UMsK1niYflQ5cybLRPYxMOzZUueQIMKBYoEjydPRY0GLcMTSVI8CBAg6VSmaKdOSKTiQVJGalQEXb9C/Uq2bFQWLM5+hRR1IAKBaBGwgDQXLVy6b93eHch3IJi+kSD9fQIp0pNIgSMdiARm2mFIB8AEhnxgseInBwhXnla5c2ccnkN/rgwah2nSpk8fSM26tOvWrF/geGZatmzTaUzLe2GA92zeBoIbyJ2mt/DjxY8fNxWc+ajlypkzNzDK1Kjr16lbx16dO/cko5Kc/8F+ptT46wVOjToD3nyBUgXCp09y6kyB+wXs499fYFP//wD6t8mAAf4nYH8DJqjggptcoSAZDcpghoObyGChGWQkI+EVBHBIwCbJkEEGASTKQCKJZpxI4goqEsDiiy6yuOKKL65g4404xojjCuvc+ImNrCwC5CIDfCKkjUUOoOQAK7Cy5JNQDgCKlFNKqWSVVloJypZUbunll2CCUgwohBQjAChHgILKmgIcgQohhBxRzBEC1IlKmwLcSUidfPYpADp1AsonoIT+aaig6CSqKKKLolOFo+j0gEwPVVTRQwCJUopOAGNwOsalyAQg6hjpiGqqqducmuqpqG7j6qsBuP8aa6yyvmqrFa5e88s24OS6jSauWmEFAL8AYIUmAICDLADEDsvss89mAi0A0mZS7bXMYnuttdZSwa21d2RyiDDhZuLtuHdsIMwhh1CxwQZUUHEHu+++e0i979pBjh12bIDJu+NsgMG//I5DjhgIi4EBBv4u7DAGvDjMyyUYnFDxwhFfck4wJ1wywgnBnHPxCB+f8PEIwZCsshEkGzEMMSwbUQ4xxJAsggjmjCDCMCKUc7MRO5tzszc331zEHN5cIAInRXBidBG+zDHEHJxMDfUFvgwxxAVcX7B111xXcoHY33CNyDfhWGLJBYh4YEkl4SAijgdce2C3B3PffTc3OnD/Y7cOHhiiwysdFO5BB4YkPngHueTSAT866JAL4YVX3kEhhXcjh+ZyENELEZeXUIgcl/dCBxElnI46Han3UsLrJQwCe+y74AIHLrDDAYc+cAyyywT7+I7LBMQXb/zxxe9AvCCC1LBDDbrssEP0gjwfwvW6hCBICDVw3/3119cRAh/g18GH+CigUEc946ePggkotNLK+3y8b0IrJkxhwv4mCDFJ/vmLgBCEMAUhmGASk6BEP/wxBUpEwIFCiIAEJ0hBCmJDgtggQQTa8IY2uMENEchGB7PhBmuQwBodJAEJ3kACN6jwhSSQghS0QYJbkCAfKvRCH6TQh1uAAAQz7AMI/7zgBSD+kIg+/CEI+hCFKICgFiBwYhSo8UMo0iIKtZDAMkAgAQlQo4vL6KIEaEELMZpRjDnIwTFyIIEw5MAWGmBjG8NwjDjaAh8auIcGbBEGDfjxj4D0oxqY4McucIEJTOCCGrrABDUoUg9M6IIe9MABDnCBCxzogjEo2QUOGKOSlfQDKGlAgz/8AZSVlMQfJGEPPwCjAn5YJQ38UAFgkLICNHhlBXbJy2oAgpcVAEQsKiALYMKjGg/4JSBk8YBYvOMB0GxmNJnZhGhas5rQrKYTgPAAJ3jTAkAAQhPi0QRXWGCbFpCGNCxgTnC20wKwsIA8ZyBPWNBTnhaYgT4tsP8EZ/TzA82Axgcs0IwPLAEWH/jADJZg0A9AwwUJhWhCP+ACie7BBXnIw0UTCoE1uGANe6DHGiCwBwhAIKMjNalJ56HSlkIgBhBoAQTUEdMWKKMFMtXCS2OghRi0wKfKyEAGeCrUDGihqEI9alG3kIFo/OARRX3qFn6giAw8YgtbWAUDfvADBmyBAatQBAOioVUGmJUBYj2rWs2aBQa0ta0M8EEWQkEKBjCCrhRgADsYQAof+DWujGAEBeZKgcIa9rCHRcMXDIsGNFBACV9IxRfQoARHKEEJFGhAZBvQAFWoIhWOUIUjOEta0iqhtJyNQwOYsdoGxAEGDXBAA4IQiNiHujYQ7oBBIGSr2zgEwQEwgMFvgwADBxjXAb89rgNsoFzjisIGPHAAD6DrgBvYwAaikO4C2nGDG/DgBgtYQCPC2whRLAC80w2veM+r3vDOIrwpaC98U5CIRLRXBYlQQQpmAQUFKEC/s0gBFNgAhRSo4MBsYEMKFMAGBUBBBf6NcH/9C+EIKyAgADs=') repeat-x bottom;}
	div.latestItemVideoBlock span.latestItemVideo {display:block;}


div.latestItemCommentsLink {display:inline;margin:0 8px 0 0;padding:0 8px 0 0;border-right:1px solid #ccc;}
	div.latestItemCommentsLink a {}
	div.latestItemCommentsLink a:hover {}


div.latestItemReadMore {display:inline;}
	div.latestItemReadMore a {}
	div.latestItemReadMore a:hover {}


h2.latestItemTitleList {font-size:14px;padding:2px 0;margin:8px 0 2px 0;font-family:Arial, Helvetica, sans-serif;border-bottom:1px dotted #ccc;line-height:normal;}






.k2AccountPage {}
.k2AccountPage table {}
.k2AccountPage table tr th {}
.k2AccountPage table tr td {}
.k2AccountPage table tr td label {white-space:nowrap;}
img.k2AccountPageImage {border:4px solid #ddd;margin:10px 0;padding:0;display:block;}
.k2AccountPage div.k2AccountPageNotice {padding:8px;}
.k2AccountPage div.k2AccountPageUpdate {border-top:1px dotted #ccc;margin:8px 0;padding:8px;text-align:right;}

.k2AccountPage th.k2ProfileHeading {text-align:left;font-size:18px;padding:8px;background:#f6f6f6;}
.k2AccountPage td#userAdminParams {padding:0;margin:0;}
.k2AccountPage table.admintable td.key,
.k2AccountPage table.admintable td.paramlist_key {background:#f6f6f6;border-bottom:1px solid #e9e9e9;border-right:1px solid #e9e9e9;color:#666;font-weight:bold;text-align:right;font-size:11px;width:140px;}


.k2AccountPage table.admintable {}
.k2AccountPage table.admintable tr td {}
.k2AccountPage table.admintable tr td span {}
.k2AccountPage table.admintable tr td span label {}








div.k2LatestCommentsBlock {}
div.k2LatestCommentsBlock ul {}
div.k2LatestCommentsBlock ul li {}
div.k2LatestCommentsBlock ul li.lastItem {}
div.k2LatestCommentsBlock ul li a.lcAvatar img {}
div.k2LatestCommentsBlock ul li a {}
div.k2LatestCommentsBlock ul li a:hover {}
div.k2LatestCommentsBlock ul li span.lcComment {}
div.k2LatestCommentsBlock ul li span.lcUsername {}
div.k2LatestCommentsBlock ul li span.lcCommentDate {color:#999;}
div.k2LatestCommentsBlock ul li span.lcItemTitle {}
div.k2LatestCommentsBlock ul li span.lcItemCategory {}


div.k2TopCommentersBlock {}
div.k2TopCommentersBlock ul {}
div.k2TopCommentersBlock ul li {}
div.k2TopCommentersBlock ul li.lastItem {}
div.k2TopCommentersBlock ul li a.tcAvatar img {}
div.k2TopCommentersBlock ul li a.tcLink {}
div.k2TopCommentersBlock ul li a.tcLink:hover {}
div.k2TopCommentersBlock ul li span.tcUsername {}
div.k2TopCommentersBlock ul li span.tcCommentsCounter {}
div.k2TopCommentersBlock ul li a.tcLatestComment {}
div.k2TopCommentersBlock ul li a.tcLatestComment:hover {}
div.k2TopCommentersBlock ul li span.tcLatestCommentDate {color:#999;}







div.k2ItemsBlock {}

div.k2ItemsBlock p.modulePretext {}

div.k2ItemsBlock ul {}
div.k2ItemsBlock ul li {}
div.k2ItemsBlock ul li a {}
div.k2ItemsBlock ul li a:hover {}
div.k2ItemsBlock ul li.lastItem {}

div.k2ItemsBlock ul li a.moduleItemTitle {color:#333; font-weight:bold;}
div.k2ItemsBlock ul li a.moduleItemTitle:hover {color:#000; text-decoration:none;}

div.k2ItemsBlock ul li div.moduleItemAuthor {}
div.k2ItemsBlock ul li div.moduleItemAuthor a {}
div.k2ItemsBlock ul li div.moduleItemAuthor a:hover {}

div.k2ItemsBlock ul li a.moduleItemAuthorAvatar img {}

div.k2ItemsBlock ul li div.moduleItemIntrotext {display:block;padding:4px 0;line-height:120%;}
div.k2ItemsBlock ul li div.moduleItemIntrotext a.moduleItemImage img {float:left;margin:0px 10px 10px 0px;padding:0;border:2px solid #ddd;}

div.k2ItemsBlock ul li div.moduleItemExtraFields {}
	div.moduleItemExtraFields ul {}
	div.moduleItemExtraFields ul li {}
	div.moduleItemExtraFields ul li span.moduleItemExtraFieldsLabel {display:block;float:left;font-weight:bold;margin:0 4px 0 0;width:30%;}
	div.moduleItemExtraFields ul li span.moduleItemExtraFieldsValue {}

div.k2ItemsBlock ul li div.moduleItemVideo {}
div.k2ItemsBlock ul li div.moduleItemVideo span.moduleItemVideoCaption {}
div.k2ItemsBlock ul li div.moduleItemVideo span.moduleItemVideoCredits {}

div.k2ItemsBlock ul li span.moduleItemDateCreated {}

div.k2ItemsBlock ul li a.moduleItemCategory {}

div.k2ItemsBlock ul li div.moduleItemTags {}
div.k2ItemsBlock ul li div.moduleItemTags b {}
div.k2ItemsBlock ul li div.moduleItemTags a {padding:0 2px;}
div.k2ItemsBlock ul li div.moduleItemTags a:hover {}

div.k2ItemsBlock ul li div.moduleAttachments {}

div.k2ItemsBlock ul li a.moduleItemComments {border-right:1px solid #ccc;padding:0 4px 0 0;margin:0 8px 0 0;}
div.k2ItemsBlock ul li a.moduleItemComments:hover {}
div.k2ItemsBlock ul li span.moduleItemHits {border-right:1px solid #ccc;padding:0 4px 0 0;margin:0 8px 0 0;}
div.k2ItemsBlock ul li a.moduleItemReadMore  {float:right;padding-left:25px;
  background:url('/components/com_k2/images/fugue/leermasicon.gif') no-repeat top left;}
div.k2ItemsBlock ul li a.moduleItemReadMore:hover {}

div.k2ItemsBlock a.moduleCustomLink {float:right; padding-left:25px;
  background:url('/components/com_k2/images/fugue/leermasicon.gif') no-repeat top left;}
div.k2ItemsBlock a.moduleCustomLink:hover {}







div.k2LoginBlock {}
	div.k2LoginBlock p.preText {}

	div.k2LoginBlock fieldset.input {margin:0;padding:0 0 8px 0;}
	div.k2LoginBlock fieldset.input p {margin:0;padding:0 0 4px 0;}
	div.k2LoginBlock fieldset.input p label {display:block;}
	div.k2LoginBlock fieldset.input p input {display:block;}
	div.k2LoginBlock fieldset.input p#form-login-remember label,
	div.k2LoginBlock fieldset.input p#form-login-remember input {display:inline;}
	div.k2LoginBlock fieldset.input input.button {}

	div.k2LoginBlock ul {}
	div.k2LoginBlock ul li {}

	div.k2LoginBlock p.postText {}

div.k2UserBlock {}
	div.k2UserBlock p.ubGreeting {border-bottom:1px dotted #ccc;}
	div.k2UserBlock div.k2UserBlockDetails a.ubAvatar img {}
	div.k2UserBlock div.k2UserBlockDetails span.ubName {display:block;font-weight:bold;font-size:14px;}
	div.k2UserBlock div.k2UserBlockDetails span.ubCommentsCount {}

	div.k2UserBlock ul.k2UserBlockActions {}
		div.k2UserBlock ul.k2UserBlockActions li {}
		div.k2UserBlock ul.k2UserBlockActions li a {}
		div.k2UserBlock ul.k2UserBlockActions li a:hover {}

	div.k2UserBlock ul.k2UserBlockRenderedMenu {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li a {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li a:hover {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li ul {} 
		div.k2UserBlock ul.k2UserBlockRenderedMenu li ul li {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li ul li a {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li ul ul {} 
		div.k2UserBlock ul.k2UserBlockRenderedMenu li ul ul li {}
		div.k2UserBlock ul.k2UserBlockRenderedMenu li ul ul li a {}

	div.k2UserBlock form {}
	div.k2UserBlock form input.ubLogout {}








div.k2ArchivesBlock {}
div.k2ArchivesBlock ul {}
div.k2ArchivesBlock ul li {}
div.k2ArchivesBlock ul li a {}
div.k2ArchivesBlock ul li a:hover {}


div.k2AuthorsListBlock {}
div.k2AuthorsListBlock ul {}
div.k2AuthorsListBlock ul li {}
div.k2AuthorsListBlock ul li a.abAuthorAvatar img {}
div.k2AuthorsListBlock ul li a.abAuthorName {}
div.k2AuthorsListBlock ul li a.abAuthorName:hover {}
div.k2AuthorsListBlock ul li a.abAuthorLatestItem {display:block;clear:both;}
div.k2AuthorsListBlock ul li a.abAuthorLatestItem:hover {}
div.k2AuthorsListBlock ul li span.abAuthorCommentsCount {}


div.k2BreadcrumbsBlock {}
div.k2BreadcrumbsBlock span.bcTitle {padding:0 4px 0 0;color:#999;}
div.k2BreadcrumbsBlock a {}
div.k2BreadcrumbsBlock a:hover {}
div.k2BreadcrumbsBlock span.bcSeparator {padding:0 4px;font-size:14px;}


div.k2CalendarBlock {height:190px;margin-bottom:8px;} 
div.k2CalendarLoader {background:#fff url('data:image/gif;base64,R0lGODlhMAAQAIQAALSytNza3MTGxOzu7Ly+vPz6/OTm5MzOzLy6vPT29LS2tOTi5MzKzPTy9MTCxPz+/Ozq7NTS1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQIBgAAACwAAAAAMAAQAAAFv+AjjmRpnmh6FtACFerYGEYTjy2UmA0D/ABFBIYyEIAAwiJViCiQB5uogUACCcRSwAoMrBxcAMJWOIYBApPh/DOYBGxCYssGQErmM6EEqQMCeWcHJA1+AAMkEX4ET3VpIwuGSyNwdQqNbI8ikX6TIpV1gWGDIwOGiCOKoXRsbiSiVgp8fgEJsFcmnGEKriSgXLIPA5dcciesVhEntruoDwm/Cgc7KAuBvEwHmGjUJAkGAQbdKgPhzjHg4jfr7OshACH5BAgGAAAALAAAAAAwABAAhJSWlMzOzLSytOzq7KSmpNze3MTCxPT29JyenNTW1Ly6vPTy9KyurOTm5MzKzPz+/JyanNTS1LS2tOzu7KyqrOTi5MTGxPz6/KSipNza3Ly+vAAAAAAAAAAAAAAAAAAAAAX64COOZGmeaHpeRVZd6rg0zRKPQzUc5iIBQKABhmpoBEiBppK6RCRJQcAmGiCCWAq1lEkyopmVwZtU2A4ErFpgakSRDKigYbLA5V+G5hJQ+5kkRwJ5UHkaJQN3ilAZFH5qCiQLiklyAhMkEVFxcEgaj2oUJBWDhRKEcYAidpyUcRCgQRijb0iWAqoPdqWtcRISabEAkSMTg7ynvJgjEa13choOwgBhgQK3156Id71IGQcYsRREI27IhHQlu17QPAOwfhgL5CNdb78JKxqmcBrLD4kgzUNRQUOcOApylXDyq9SUHhkcZBgYY0GFBv9UXGhwkR4JGB5viIwRAgAh+QQIBgAAACwAAAAAMAAQAIR0dnS8vrzk4uScmpzMzsz08vSMioysrqyEhoTExsTs6uzU1tT8+vy0trR8fnykpqSUkpR8enzEwsTk5uTU0tT09vS0srTMyszs7uzc2tz8/vy8urysqqyUlpQAAAAAAAAF/qAmjmRpnmh6FtmVMepYTFMRj4qgVKbyRICgw8JDTQKWpCUgSDEoDaWFYBNNEEAAEAipljLKgzRzYkjCyo2tYMhug5GBaSJNHqKWiSlhx4sPAWZwWoNaZCRIFn9RfwElCn2RURlthYUcJAWRSngWGCQUUnd2SQEOhIRuAHIjAoqMDYt3TSN8o5t3b1moERAkrnUWnRa0Inyvt3cNDRCFu1qYIxiKyLHInyMUt314AQGqlocjSMPCpY99yUkZbLyDAzAkdNWLeiXHYd08AgioQQBdToCps2xBmQCw7ATApsGHmwgWvJgQEODOnQ3FTDxZ9oqKiQIUXEhMUUDABIYqCxhMMBnvhsuXMUIAACH5BAgGAAAALAAAAAAwABAAhFxaXLSytNza3Hx+fOzu7MTGxJSSlGxqbLy+vOTm5Pz6/MzOzLy6vOTi5ISGhPT29JyanHRydFxeXLS2tNze3ISChPTy9MzKzMTCxOzq7Pz+/NTS1JyenHR2dAAAAAAAAAXOoCaOZGmeaHpaC7Y96mgliRWPWZPBZWIAQICEY0MlEIFkANFIKTYTZWBR1DQOweygShJIpYKTAvNNMmyWSHZdMSXK0oSpAE8iHoG1flFC1pclGX9JAh16awYkFoNJBCQbjAgSh1okDYwBFCR0gxOTlEADlphyI5yDhqAAiSMEmI4jkINIqgB8JH51E4GMFARYlB0ml7qaJadlEzAUwGsRsCVecBsnD7lSE9AJDojQJhTXE8ZiC1FKBTwkBAUT6DcaBBS+7xoJFAnp9Pr7JCEAIfkECAYAAAAsAAAAADAAEACFPDo8pKak1NbUZGZk7O7svL68TE5MfH585OLkzMrM/Pr8XFpctLK0bG5sREJE3N7c9Pb0xMbEVFZUjI6M7Ors1NLUvLq8PD483NrcbGps9PL0xMLEVFJUhIaE5ObkzM7M/P78XF5ctLa0dHJ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABu5AkHBILBqPyORRsxElIMqhxuPRRIcUBAVaRBwAYIBjYkV6Cow0o4BIKioiNeNTBmE44AtADxhQjhhychhHChuCaRZWBAthYXoXI0YeiHIeRhGVaQUQAY97jgAJRWiaa0UUpmkYIaCfkB1EGqppBEQVtAUOea6OA0QItAwPRJmqIhd8vJB9wMKXQ8aqA7yhe7FDBMK2Q7iqBZ7LoHoRpMeotA8UEsqhDUbBmiLERdKIIlAYBuOgA9BFAuIgEnAEQilBIriBePDl0QSFRh4cZDDPzQeBaSJwIUKggAiNV0AQePAAohIPDzxsDMmy5ZEgACH5BAgGAAAALAAAAAAwABAAhRweHJSWlMzOzFRSVOzq7LSytDw+PMTCxOTi5PT29GxqbDQ2NNTW1Ly6vExOTCQmJPTy9ERGRMzKzPz+/HR2dCQiJKyqrNTS1FxaXOzu7LS2tERCRMTGxOTm5Pz6/Nza3Ly+vHx+fAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+wIlwSCwaj8jkkQCyHBLKIaTTgUSHBAQBWvwoKoDwI2RFdkCFdAGESHouGnVBUJ4wFmEAGOwgHD9ych9HHgeBaQ1WBBt6eY4YRh2Hch1GHJNpIAkBjY6OB0VomGtFBKNpHxFgno4URBCHFnGyBRlEF4GytGsPrHthA0QIuXFxaW1Dl2qys8UPq2HQesFDw8saGrpxyELKBcxp2XERjtCrFB5DGXLFBdjftkO44d/icSAB0p1hoESi2tpAlErDrBitDwQMRCsHYEA6Ye7atatU5FIzcJomXMDTaJUDbkQYXKPF4EgCEBK/gYg3wUs5CgQeRkJTjI0bAc3msMTSwEITIplJMiBAsFMJlQ5cCAG9wvRKEAAh+QQIBgAAACwAAAAAMAAQAIUEAgSMiozExsTk5uQ8Ojy0srTU1tT09vQkJiRUVlS8vrzMzszs7uwcGhxEQkS8urzk4uT8/vw0NjRkYmQMCgycnpzMyszs6uy0trTc2tz8+vwsLizEwsTU0tT08vRERkRsamwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG2cCIcEgsGo/I5PFSqDw8yqFnMIBGhRfI5WDMJABgAAXESA4UhXRBAUlqOhh1YWGNdBrhvORyzMjlGUcaHH9pTxEXCHmLDkYDhXIDRgKQaQoHAYuaD0VolWtFTJ8FGRuaixNEHqNpZUMdrAoUp3kERBCsBW1DlKMYtLW3ubtCvaOmwACpQwy5rkKwowqZyZxEnpUYoawZA4q0jUW42ZJFxoXaERZ4mhLERAZxhQZHB9hyGM8GX2Fj5UcQ7mH4Z0TDAnlpBHAp8qhCgWdKGHSDqGRAt4VXMmqMEgQAIfkECAYAAAAsAAAAADAAEACFHB4clJaUzM7MVFJU7OrstLK0PD48xMLE5OLk9Pb0bGpsNDY0pKKk1NbUvLq8TE5MJCYk9PL0REZEzMrM/P78dHZ0rKqsJCIk1NLUXFpc7O7stLa0REJExMbE5Obk/Pr8pKak3NrcvL68fH58AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABv5AinBILBqPyOSRILIcEsohIRQiRKUIArQYUlwAYMgokgyBAugAI5T8YDaFeEFAFjYWYMD3+7AaJ2mBARNHHwdyiA5kBBx6eY8ZRiGCgmxFHYiZIgkBjo+PB0UMlIEMRQSZqSESX5+PFUQEpIJ+QhiZFhu5cSIQrntgA0STs2kYRJhyuXC7GxCtYNB6wkPEgiOBx0PJBctxunASj9CtFR9SxWkIRLff3eBwIgHSnmChRKNp2KXnUnHLcMAVqGIg2jgAA/pVS4dBoRBMzOAV2EQBAx5HrR6sK2Jo1gGHQhKICFiAmQgN1RSMq0AA5BAM+dAwaOiSwgcBEeegPOXAgheimkQ+IMCAAcEHoEQ8KN1SCGmbK0qCAAAh+QQIBgAAACwAAAAAMAAQAIU8OjykpqTU1tRkZmTs7uy8vrx8fnxMTkzk4uR0cnTMysz8+vxcWly0srSMioxEQkTc3txsbmz09vTExsRUVlTs6ux8enzU0tS8urw8Pjzc2txsamz08vTEwsSEhoRUUlTk5uR0dnTMzsz8/vxcXly0trSMjowAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/sCRcEgsGo/I5JHTKSkkyiHockFEhxVEBVpEWABgwMPESV4cobTFIUouLqWGvCEqCzUfQEbPH1SOBRYhgoQWGEcLHXOLGGUEDGGReglGF4KDagmDbUUTi58FEgFge6WRCkVolxaahA5FFZ+yGiSSpHweRAiYrIRpg1ZDF7KgD3ymYRkDRCKXmGqYnEKexHMlGXuRyMtDIrwJhOBp0iPU1XIDt9m3uUO7rNC8IcFCw+cNBaPafHoTqfLO1sC61wBCBQrIbkWo9KzXoATkpp0rAUXDgWRhBoA4giEctENGJBQgVoLAEAgGkmUwYRKJCFWD2CxAskBEnDkTuBAhUKBEE84rIxCIEEEvCggIIHQCXcr0SBAAIfkECAYAAAAsAAAAADAAEACEXFpcrK6s3NrcfH58xMLE7O7slJKUbGpsvLq85ObkzM7M/Pr8tLa05OLkhIaEzMrM9Pb0nJqcdHJ0XF5ctLK03N7chIKExMbE9PL0vL687Ors1NLU/P78nJ6cdHZ0AAAABeQgJ45kaZ5oemIKsUHq2FxXE49ao8FlYgBAwKSDSV0kQYCEkFpsGJQoRVEUNQ7J4KBaomSDgdOCIA1IEUUM8gu0mC7sILN0iQagFHMgs/DGgQola3ESJRp2eHcUUAIef0AGJAKPQAIkG1J5iVEZE5QDJASUABkkdYqIiJ6PoCOilKUjdZqodwwMjo+RI5OUG5eoiHgZfn+BJFh/B4aItVECBclsHm+PsaaZmpwwFdJJEgUnAXEdYhlQtRnhIgkOWQbrJxneE9cmTreaVCYFFwwXPGIIyJDB0o0FCRokWHCjocOGIQAAIfkECAYAAAAsAAAAADAAEACFPDo8nJ6c1NLUdHZ0vLq87OrsjIqMrK6sxMbE9Pb05OLkVFZUhIaElJaUpKak3NrcfH58xMLE9PL0tLa0zM7M/P78PD481NbUfHp8vL687O7slJKUtLK0zMrM/Pr85ObkXFpcnJqcrKqsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABv7AinBILBqPyORR8ug8PMrhg0B4RIcFRSFhLDgwgzCEw0USQACABQAiJD2CCWfOoUiGHwZ4AAZv7kYBa2mEFgFHHhF0B3QEdxIGe31hGCFGBGuZamoWbkUIcwdyHIwHGYmUfKl8VkRohLBpC0UFoaOiHHIPkaurIkQXaZrDABdEAnSkt3MZEKqqkgOWQwSx1gATRKC4traTe88YG0QTm8SZ2UOgytyiExMbq+B8v0PB17DGQwLctqMZGaL1ajUEzTk1s4jUYreMwwNI4VKFgEJukDUL6bQlU8aMiwIGz8IM+HMkhIVzh4x4yCCnXQYNWL5QwsAB0JEJCywuyKgyThrLCXaMSBDgxKaSC+/0RfHwQcEHileiSo0SBAAh+QQIBgAAACwAAAAAMAAQAIQcHhyUkpTMysysrqzk5uSkoqTc2ty8vrz09vSMjoycmpzU0tS0trTs7uw8Pjysqqzk4uTExsT8/vwkIiSUlpTMzsy0srTs6uykpqTc3tzEwsT8+vycnpzU1tS8urz08vQF/qAkjmRpnmh6bpkBbeq4WNYSjxd0IebHBEAKRQNDWRyAJMBhSW0WDBqt8sFxKAFhNvDgmQITQFgsDqw00oHUU0Vgttg4pVmyKMnKycAUoQ2iFmoDBxsVcIdZECUOYWNJYw4lF36AfxZRBg+HclkHJAt3oY8Cn1KBlTQHWnFbW3sjD3iiSQ8kfZaUlAqbWXGvIgOzjgC1I32nuH8MDG+szgGeMqFjjQA2MriUgAcVz86KJEiPog5FIpPIqBYGCAXeWXQkFsPVE8W2pqepPBdXmw9VSmxIQC1PAnMjNhyIkuxAgxE+4Cg44OXEA3FiHDzYgDAhFIYMqJhAAGEBhIoqExY8sCCAY4wNBCAQ6HjjZU0VIQAAIfkECAYAAAAsAAAAADAAEACEBAIEhIKExMLE5OLk9PL0pKKk1NLUtLK0fH58zMrM7Ors/Pr8vLq8JCYkBAYExMbE5Obk9Pb0pKak3NrctLa0zM7M7O7s/P78vL68AAAAAAAAAAAAAAAAAAAAAAAAAAAABc3gJY5kaZ5oei7KoCzqWElSFY+tEplEcvwHigGGkjQASEBDkloYKMBDhTAiMKJADLEUcCS/gZUA+2NQFxgy8GGSfN8AZumh/mEikzpQUTrCkw0lCno/E2mENiMJf28JJAaEBxhQhGwjBYxfBSR0hBSUepYimJlImyOdhId6iSKLpQCiF5CqeYQQfaWBJIOEExGrZBgmpIynnHoUIhafwjsmCIwIJ8BqFBYjEalBFc8nBX5IDccmCxWgBw/e2RATEOspCQUFsirt7zf5+vkhACH5BAgGAAAALAAAAAAwABAAhBweHJSSlMzKzKyurOTm5KSipNza3Ly+vPT29IyOjJyanNTS1LS2tOzu7Dw+PKyqrOTi5MTGxPz+/CQiJJSWlMzOzLSytOzq7KSmpNze3MTCxPz6/JyenNTW1Ly6vPTy9AX+oCSOZGmeaHpuF3Rt6rhY1hKLG9RBiPkJlgGNsYChLA6AEuCwpDYaijRA8TRGHw+NJrRoeqbABDAmkwMnxIPKDgQKF8nmEGQMGFxGxGRZmpcTA3xUFISFARgIBnVbd1txJA5jZUplDiUQhZpuUxQCdHkWeI4VJAt+qJUCJAdsmoeFGHiMXVt7Iw9/qUoPJBauwFIKs6PFQrciA7uUAL0jA8HBdLWMeKUyqGWTADYjrZvgAQ+LduVcQVeRgKkORiKZ0ZoCc1uhenzM2xPOvvFvPQ083HEU5IuJDQm0AUrgDsuDcBTgjEBQoRGDDg1LPEhSycGDDRlFIIiyiUE6EggXCLi4IWLBAwvzQpbI0SEDGJYsZeIUEQIAIfkECAYAAAAsAAAAADAAEACFPDo8nJ6c1NLUdHZ0vLq87OrsjIqMrK6sxMbE9Pb05OLkVFZUhIaElJaUpKak3NrcfH58xMLE9PL0tLa0zM7M/P78PD481NbUfHp8vL687O7slJKUtLK0zMrM/Pr85ObkXFpcnJqcrKqsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABv7AinBILBqPyOTRU1AUPMrhg0B4RIWeR+chMUo6nAOHMxFAkQQQAGABgAjJBAczqGMchaGEMB6LOREJRwFta4YWAUcSDQN0jgMMHxUeGWETBxN+EwhGBG2fbGwWcEUhdI2ojQYSD5Z9mH15RGqGtWsLRQ+PqacRlZpkrhREF2ugxwAXRAd2vKgbma5/fZxDBLbYABNEIanejRjRmbCYYtVCE6HIn9tDDY+np42V066Zw0PF2bXKQyLNdZph2NDqkkE/YTQUUbOODS4iugLuqhOBUh9gm4xMKITNQjtu3xytqqCBQLloBwIdCWFhXSIvG1DJY6BgSAIKryZcOHNkwiICjgs+epkTDw9PIQk+OLki5MIEnUw9XOggQMJRplizFgkCACH5BAgGAAAALAAAAAAwABAAhFxaXKyurNza3Hx+fMTCxOzu7JSSlGxqbLy6vOTm5MzOzPz6/LS2tOTi5ISGhMzKzPT29JyanHRydFxeXLSytNze3ISChMTGxPTy9Ly+vOzq7NTS1Pz+/JyenHR2dAAAAAXlICeOZGmeaHouWqMt6thcVxOLy5ZthYk9lACFwtjAUBcJYAmQEFKYzoQJMCRGGMRwKKQQICcKlRo4YQbj5aHCWWSCjACDy7iYLmnqs2TJLyUFAnBbclsaJUp+TSUbiksBb3REgwokAo5LAiQRmB5zg11bdiMEmAAZJH2Yn3OFckKjIqWYqCNotEFbg3OVI5eYG5uYEoJxxlxBPSQHjgeMmAFuuruxI3iKtSS3eRM9BQivnwFfJwF+HScFHtxsIhAKhEVHJxnMTBPZPh1jDlclEAlc3BAhIEMGTTcgXKjjb6DDhyhCAAAh+QQIBgAAACwAAAAAMAAQAIU8OjykpqTU1tRkZmTs7uy8vrx8fnxMTkzk4uR0cnTMysz8+vy0srSMioxcWlxEQkTc3txsbmz09vTExsTs6ux8enzU0tS8urw8Pjzc2txsamz08vTEwsSEhoRUVlTk5uR0dnTMzsz8/vy0trSMjoxcXlwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/kCRcEgsGo/I5HFBQVAWyuHHYkFEhQsFY7IxbrSM8MgCRVoaoHSlEUpuSA+AHGCwCjeXsD5cKBcLFSCBgxUXRwQDcxhzBxkiCwV7khNGFoGCagmCbUUJcoufcg4EGZKmFEVolxWagw1FCnOhoQGRpnucQgiYrINpgnZCDQCLoMRzJSO3e5RDIZeYapi5Ihqyx6EYysthzUIhvAmD4mnU1sWKnxi23NS7rNK8IMEiHdfGciWl3AwfqfLQ1hQJccwYugAS2JkqUClaL0EJqAmJIAufBwIiCIzYtqeAhCMXxkkzZIRCIoMADggYImHCnhEhPiIJoUoQGz9FCJDAAAqDGgEIRiR8gPBBZhQEIULQS9JyRAGMV6JKvRIEACH5BAgGAAAALAAAAAAwABAAhRweHJSWlMzOzFRSVOzq7LSytDw+PMTCxOTi5PT29GxqbDQ2NKSipNTW1Ly6vExOTCQmJPTy9ERGRMzKzPz+/HR2dKyqrCQiJNTS1FxaXOzu7LS2tERCRMTGxOTm5Pz6/KSmpNza3Ly+vHx+fAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+QIpwSCwaj8jk8UNAED7KISEUIkSFiYPFYS1GJoVwYYOBIkOggDrACCUjIwhgflG4hRGHeC8yFydrgQETRwQPABeIcwALDRQfInuSHUYhgoJ3RBmKi4ocU5IWG3tdQwyXgQxFB5ydiwGRYqIFswUCRASogqUUFa6/EqNioxsbs5RDlrprGEQDiZ3QdMK0xsRhyELKgiOBzUMDrtCJFxexY6Oi6rdSy2sIRL6L0osSIbLXxB5Fp2vdqX4oHKCniFyABLGsqduQLZm7MkU20XFlwIqGYmMybhCRwMiHA7oOBBSCINy8OQsgUkjQIYyoDQI6IsHQTw2DMiOFEBhRjo4cApVDEngYeuURAgwYEHzI6cXBFqVFoz6SurRIEAAh+QQIBgAAACwAAAAAMAAQAIUEAgSMiozExsTk5uQ8Ojy0srTU1tT09vQkJiRUVlS8vrzMzszs7uwcGhxEQkS8urzk4uT8/vw0NjRkYmQMCgycnpzMyszs6uy0trTc2tz8+vwsLizEwsTU0tT08vRERkRsamwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG4cCIcEgsGo/I5FFzgVw0yqFnMPBEhYdHpXAxeiyFcAHTgSIHCnFBAUkyQBSAHJDISB9qscJczOTFdkYXEnOFDQsRGml/YQJGA4xiA0YOhZYIF36RYV1Ei5EKRQqWpAGfkYhSm2IMRBOklhsYqwWOQxC0BW1DBLCWs6u2Qri0u0K9vnIUp4ypQgy5rUOvyQAbmpuTnqsYRQ/VAAEHzGqhRcSMGNpElb4IkwwYwOUHRwbzagZHEISkDcIHBKjBsKAeEgin1CW5AKdQAn1FDgzIMMBgFAYUpUXxgGGLsSsgQyoJAgAh+QQIBgAAACwAAAAAMAAQAIUcHhyUlpTMzsxUUlTs6uy0srQ8PjzEwsTk4uT09vRsamw0NjTU1tS8urxMTkwkJiT08vRERkTMysz8/vx0dnQkIiSsqqzU0tRcWlzs7uy0trREQkTExsTk5uT8+vzc2ty8vrx8fnwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/sCJcEgsGo/I5NFDQBA8yiGk04FEhYmDpUEwQiSFcEFzgSI7IHEBhEhCQg+AvKL4SBtqMchc/OTFdkYEDgAVhXIACwwTHml/YRxGHY9iHUYYh4iHGwR+ahYaal1EjpQgRQeZmogBpQWgr6EFAkQQf7CwGUQUq70RsmGhGhqwkUMIebDAbUMDhprPc8CgypBEyGKgxNsFzEIDq8+GFRWlw7HUtEMZasJjoRa6Q7yI0YgRnrFj+xqWpGHUtoE6ReSAvUPjAiRwJHCbsWvv+O3zRwTTnFUGumQ4504DiARHGGTjtsgIAnD15Cwog4UDwGECQJ5JI4xNEgIhyM1RwJJIGQIqFKNkQIBAXhQIDbYg4HOlqVMhHphOCAIAIfkECAYAAAAsAAAAADAAEACFPDo8pKak1NbUZGZk7O7svL68TE5MfH585OLkzMrM/Pr8tLK0bG5sXFpcREJE3N7c9Pb0xMbEjI6M7Ors1NLUvLq8PD483NrcbGps9PL0xMLEVFZUhIaE5ObkzM7M/P78tLa0dHJ0XF5cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABvXAj3BILBqPyORRMUFMFMphptPJRIWKxCJiLWa0izCIAkV2CuFwAZHMSByAOODAFmYq6XyhXLzk8xdHBANyFnIGgQpof2kRRh2MeR1GIXGGlnENBH6RaRNFi50FRQlymJgBoZ0eRBmdeQREHIaXpwAiIK9hjkMIumEPRBiFALW0ubq8Qr6/k0PDtacWFqqRrEMEvwuxQxyYxnEinK/OQ9V/IEUepsXEARDnaaNFzJEgwUUMtLYbsQQgyPRAOCIgYB4BRyYQMmXIAEIhECLkAeFhIJIHqu614eDgkoUD+IhA6PCgg8UoBB484BYlIogCLK/InKkkCAAh+QQIBgAAACwAAAAAMAAQAIRcWly0srTc2tx8fnzs7uzExsSUkpRsamy8vrzk5uT8+vzMzsy8urzk4uSEhoT09vScmpx0cnRcXly0trTc3tyEgoT08vTMyszEwsTs6uz8/vzU0tScnpx0dnQAAAAAAAAF0qAmjmRpnmh6KlmTKepoJYkVi8qGbIRpXYFgYLKBoRIIYQDRSFk4EoAUYEjIGEohwlgSZIWCk2UwLR8oGkXyGyyYEmyhtVQp2yMEbzyYKa3jCCUbdoRLe0ELJBaHQT0jEIR2HROMbiMNjAFoI3WRZZSHliKYjHMiZJ5Tf3GJIwSZjiKQqQARenumIqtfE4K0AAEPu0qBJaRsE5skqJESPQQToMQPJ7dZGycEHc3KDwVKEwvUKBSryU4cdg65Ig8JFAnjMQQUFLEq3hMF7Df9/ighAAAh+QQIBgAAACwAAAAAMAAQAIR0dnS8vrzk4uScmpzMzsz08vSMioysrqyEhoTExsTs6uzU1tT8+vy0trR8fnykpqSUkpR8enzEwsTk5uTU0tT09vS0srTMyszs7uzc2tz8/vy8urysqqyUlpQAAAAAAAAF/qAmjmRpnmh6MoqgMOpYTFMRi0x2ZXZZXJaDxdKgwFCTwHAYEKQqlghgGnkoZJtlcCipnDJL4TJzKnQA0jQAMdEwlIdGfBhPmCZaemPYLg2kaIFoBgVgc3pLVyRKW3NCBwElGWqCgBKMQXt7cwQkBYhLe0MYJAdUlYEQoo55diMCmZmPcg1OIwOCuWgRopu+Qq4isHlEWrYiHWqAgGhwWnN7nSMYjbSzB6QjHKdTpxEQYNarQdkjSqKhTJLMlFMSb3nQwSN4snKZfSS4zIERhBoYNsQ5dKDLF2INGiwoA6FfNwTHKhAIo/DICQEBBjbYcMzEpwjLrFgUUWGCixsiEwoImFBOBYMFFygUGImyps0SIQAAIfkECAYAAAAsAAAAADAAEACElJaUzM7MtLK07OrspKak3N7cxMLE9Pb0nJ6c1NbUvLq89PL0rK6s5ObkzMrM/P78nJqc1NLUtLa07O7srKqs5OLkxMbE/Pr8pKKk3NrcvL68AAAAAAAAAAAAAAAAAAAABf7gI45kaZ5oel5DNVzquDTNEotXlVSHuTgChkAgicBQDc1wqKmkLgaAVKqYyBTL4NDQM2WWwmXmdKBMz5jB46JkSNxDt8XUyMYlw4ZJcO5jDl9wd0tqJEpacEIMGiUVfY8Oh0F4eHABJAuDS3hDViMaj36ciXZzIxWTk4pvEk4jfKFnnJW0QqYiqHZEWa4iZrFTbVlweJcjE4isqwyeIqDAABhfyqNBzc67WZyMJI7QDmx2xLcjdapvk3olv6EQNhMKboIMXCdfdhISCScL7GcQL0QcCABG35ETFTTIk6Cglw8FfSgEJHGggYsbIhZUaHAtxYIMESIsOIixZEmSIgJCAAA7') no-repeat 50% 50%;}
table.calendar {margin:0 auto;background:#fff;border-collapse:collapse;}
table.calendar tr td {text-align:center;vertical-align:middle;padding:2px;border:1px solid #f4f4f4;background:#fff;}
table.calendar tr td.calendarNavMonthPrev {background:#f3f3f3;text-align:left;}
table.calendar tr td.calendarNavMonthPrev a {font-size:20px;text-decoration:none;}
table.calendar tr td.calendarNavMonthPrev a:hover {font-size:20px;text-decoration:none;}
table.calendar tr td.calendarCurrentMonth {background:#f3f3f3;}
table.calendar tr td.calendarNavMonthNext {background:#f3f3f3;text-align:right;}
table.calendar tr td.calendarNavMonthNext a {font-size:20px;text-decoration:none;}
table.calendar tr td.calendarNavMonthNext a:hover {font-size:20px;text-decoration:none;}
table.calendar tr td.calendarDayName {background:#e9e9e9;font-size:11px;width:14.2%;}
table.calendar tr td.calendarDateEmpty {background:#fbfbfb;}
table.calendar tr td.calendarDate {}
table.calendar tr td.calendarDateLinked {padding:0;}
table.calendar tr td.calendarDateLinked a {display:block;padding:2px;text-decoration:none;background:#eee;}
table.calendar tr td.calendarDateLinked a:hover {display:block;background:#135cae;color:#fff;padding:2px;text-decoration:none;}
table.calendar tr td.calendarToday {background:#135cae;color:#fff;}
table.calendar tr td.calendarTodayLinked {background:#135cae;color:#fff;padding:0;}
table.calendar tr td.calendarTodayLinked a {display:block;padding:2px;color:#fff;text-decoration:none;}
table.calendar tr td.calendarTodayLinked a:hover {display:block;background:#BFD9FF;padding:2px;text-decoration:none;}


div.k2CategorySelectBlock {}
div.k2CategorySelectBlock form select {width:auto;}
div.k2CategorySelectBlock form select option {}


div.k2CategoriesListBlock {}
div.k2CategoriesListBlock ul {}
div.k2CategoriesListBlock ul li {}
div.k2CategoriesListBlock ul li a {}
div.k2CategoriesListBlock ul li a:hover {}
div.k2CategoriesListBlock ul li a span.catTitle {padding-right:4px;}
div.k2CategoriesListBlock ul li a span.catCounter {}
div.k2CategoriesListBlock ul li a:hover span.catTitle {}
div.k2CategoriesListBlock ul li a:hover span.catCounter {}
div.k2CategoriesListBlock ul li.activeCategory {}
div.k2CategoriesListBlock ul li.activeCategory a {font-weight:bold;}

	
	ul.level0 {}
	ul.level0 li {}
	ul.level0 li a {}
	ul.level0 li a:hover {}
	ul.level0 li a span {}
	ul.level0 li a:hover span {}

		
		ul.level1 {}
		ul.level1 li {}
		ul.level1 li a {}
		ul.level1 li a:hover {}
		ul.level1 li a span {}
		ul.level1 li a:hover span {}

			


div.k2SearchBlock {position:relative;}
div.k2SearchBlock form {}
div.k2SearchBlock form input.inputbox {}
div.k2SearchBlock form input.button {}
div.k2SearchBlock form input.k2SearchLoading {background:url('data:image/gif;base64,R0lGODlhEAALAPQAAP///wAAANra2tDQ0Orq6gYGBgAAAC4uLoKCgmBgYLq6uiIiIkpKSoqKimRkZL6+viYmJgQEBE5OTubm5tjY2PT09Dg4ONzc3PLy8ra2tqCgoMrKyu7u7gAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCwAAACwAAAAAEAALAAAFLSAgjmRpnqSgCuLKAq5AEIM4zDVw03ve27ifDgfkEYe04kDIDC5zrtYKRa2WQgAh+QQJCwAAACwAAAAAEAALAAAFJGBhGAVgnqhpHIeRvsDawqns0qeN5+y967tYLyicBYE7EYkYAgAh+QQJCwAAACwAAAAAEAALAAAFNiAgjothLOOIJAkiGgxjpGKiKMkbz7SN6zIawJcDwIK9W/HISxGBzdHTuBNOmcJVCyoUlk7CEAAh+QQJCwAAACwAAAAAEAALAAAFNSAgjqQIRRFUAo3jNGIkSdHqPI8Tz3V55zuaDacDyIQ+YrBH+hWPzJFzOQQaeavWi7oqnVIhACH5BAkLAAAALAAAAAAQAAsAAAUyICCOZGme1rJY5kRRk7hI0mJSVUXJtF3iOl7tltsBZsNfUegjAY3I5sgFY55KqdX1GgIAIfkECQsAAAAsAAAAABAACwAABTcgII5kaZ4kcV2EqLJipmnZhWGXaOOitm2aXQ4g7P2Ct2ER4AMul00kj5g0Al8tADY2y6C+4FIIACH5BAkLAAAALAAAAAAQAAsAAAUvICCOZGme5ERRk6iy7qpyHCVStA3gNa/7txxwlwv2isSacYUc+l4tADQGQ1mvpBAAIfkECQsAAAAsAAAAABAACwAABS8gII5kaZ7kRFGTqLLuqnIcJVK0DeA1r/u3HHCXC/aKxJpxhRz6Xi0ANAZDWa+kEAA7AAAAAAAAAAAA') no-repeat 100% 50%;}
div.k2SearchBlock div.k2LiveSearchResults {display:none;background:#fff;position:absolute;z-index:99;border:1px solid #ccc;margin-top:-1px;}
	
	div.k2SearchBlock div.k2LiveSearchResults ul.liveSearchResults {list-style:none;margin:0;padding:0;}
	div.k2SearchBlock div.k2LiveSearchResults ul.liveSearchResults li {border:none;margin:0;padding:0;}
	div.k2SearchBlock div.k2LiveSearchResults ul.liveSearchResults li a {display:block;padding:1px 2px;border-top:1px dotted #eee;}
	div.k2SearchBlock div.k2LiveSearchResults ul.liveSearchResults li a:hover {background:#fffff0;}
	

div.k2TagCloudBlock {padding:8px 0;}
div.k2TagCloudBlock a {padding:4px;float:left;display:block;}
div.k2TagCloudBlock a:hover {padding:4px;float:left;display:block;background:#135cae;color:#fff;text-decoration:none;}


div.k2CustomCodeBlock {}







div.k2UsersBlock {}
div.k2UsersBlock ul {}
div.k2UsersBlock ul li {}
div.k2UsersBlock ul li.lastItem {}
div.k2UsersBlock ul li a.ubUserAvatar img {}
div.k2UsersBlock ul li a.ubUserName {}
div.k2UsersBlock ul li a.ubUserName:hover {}
div.k2UsersBlock ul li div.ubUserDescription {}
div.k2UsersBlock ul li div.ubUserAdditionalInfo {}
	a.ubUserFeedIcon,
	a.ubUserFeedIcon:hover {display:inline-block;margin:0 2px 0 0;padding:0;width:16px;height:16px;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAjRJREFUeNqkUj1oFEEU/mZmb/NzZ3FHQlRCLihBLYSACIrERuSaiCBoo4XdES0sBC0Eq1SCjSZgChtBsbAQraJdMAoHZ5JC4ZQYj4seB7LC6f3t3u743uzd5gixysC3b3b3fd/75r0RWmvsZln8WDojElIiqyQyykLKUvTD2hlSwZECi0JgYXhe/zUCTB4aS14bPpActQdtW1CmVAqSYxf0LgiB57n1UvFg80eJqfeNAFdmcqNVsz2/BUWJFpG6QIcsqJISwo6PpUdJILMlQLa5MpP33t0EKAbOBoLiO+jPLyBrFXIpIci3cWz32dS6lNmbRtCZ2SZXbn2Yh7f6DGj8hjV5BbFLz6GOnI/I3PQQPU0MmxPadt8/MFHz+9AE5NQt4PQdyhLQhVeGDN+PBGQk0DnvnpsFDMzkEJt+aETx9jZQXgFO3IBO7CMukdttBP8TCPKPodffQIwcBaYfAYkR6OV7lNQPHL6ANpMJOtgmIDo90Lk5uhizwMurYcbx69DON2hyIdKnIgF/m4DLdllAzVBiNg/8+QkUl4D9x4zt4NcXiOT4lkAbbiRADSnznHlU3WU63XCAWDwkuXWad1+4J8FaDWXOE5z46aJYOHRu6rLVrMd7R8WVmdAL/mbbg7X8669Pzy7rrBljtYq1jY/fT6YnxyfoQvWjMypBBEXoRpu+NaqtZiHnrFcqWIvugefhyeZqaaC0UsoEAVIE02WOfica+EDbh9NsYJE50RF2s/4JMAC1ZRxNC8A8cgAAAABJRU5ErkJggg==') no-repeat 50% 50%;}
	a.ubUserFeedIcon span,
	a.ubUserFeedIcon:hover span {display:none;}
	a.ubUserURL,
	a.ubUserURL:hover {display:inline-block;margin:0 2px 0 0;padding:0;width:16px;height:16px;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAvNJREFUeNqkU0tPE1EU/u482k6npQUKfVEozxRdGBAoC2JcKBs2/gQ2Ji5dmBg3TXTjwo0/wR8BGl0YEhVMJRoIKsijRcqjUIa2M522wzy809rI3jM5yZm55/vOY75LmEcrAMuBePwA53DReAaEzAIYoO6jXqK+B8t6B0P/CF2rWUoRNIbxYhocWmaZcXr4ZKKLTN1LuHv62h1eDwdHNOTWNs80+VW6ePf9rpamec9pdrYF41pgj6G+nJ/onJoZ8ob7o15sHVSQr5gI1HXneK/gHI8JgTfrUvDpwu+gbJkPWyQMZEmAUkrNj7UlI34+7PMLjc+UFB0iC8MCZNWArAPTox3hx3eCSTu/gbMzrUrpVsJvJot1I5S5qOG4rOP16hE28xVoFoHOObHwKYutXAUnCjAS7woNinrSxjVHqJTmJgYTkXzFgFE1cZzOweWkw7MMCnIB8ucMynIV7T29+Hkk4USSMZkIRXZ29uco/i1nqeWRzlBEPMgWIGl1CIITbE0HSwjMuoaBUBCjw0HsntawvrGPsm4g4mVFG9fsQKu1r6Q3OLFvFGoxh7oBEPshdA2GAXX3BNmDErpjvcgcnqErHIWiKA1ck8Cyiqzo0/dyBzzLczBMYq+wYQQsZE2j8mhDuUbfL4Fw/Doc6qlu45oELL9dPD9P8h0RH8MQcCx1Wt2ObTNZttFNPrOD2LUb0OlfqZUV1cY1CXjX4vHR2Wx/ZMhHz+DiGOoEPEsr2nswLVzSuU3GRDQWbJAub6yd2rgmgUNYkmQ13X0utUeisYDHyUB0EDgpCW2moYM6LXuhmQhRtf/Y2C7kjs/SNq5JIHiqIExqJ5PtdgvusZ7+noDX1eyiRVCz+xbdONrPFD4sr32DS0xRoVX/SZmQPZ11PPj6a/fZ6UVp8uZwPBjt8LhFJ89V65f6oaSoq5uZ/KEkfQH4FIhGL9fVu/CXhC7mfq6k3M6tfrdFMkydNg1723Rh1iI9X4JxWcUVI5Zl4X/sjwADAJ3pVAMGctmSAAAAAElFTkSuQmCC') no-repeat 50% 50%;}
	a.ubUserURL span,
	a.ubUserURL:hover span {display:none;}
	span.ubUserEmail {display:inline-block;margin:0 2px 0 0;padding:0;width:16px;height:16px;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAZBJREFUeNrEk99KAkEUxr8d19Xyz5YFQglKN6kgbYWP4W1PEEFv0VsEvUavkHUREkEXXklXYZqbCZG4s2vnG0zooj/kRQeGnT3n+317ZnbGmk6nWCQUFgz74OSkIc+NP/IPdhiGm0eNxmm9XEYum/0V5Y9GuG63cXZ+fqxCra19ga/u7tAbDqGj6NtBDbVkyCqttXLTaexub+Py9ha95+evYalRQy0ZskoHgQrlT2QlsVepoHlzg57vQ4fhp8Eca9RQS4asMaB7IKIMTapVXLRaeBwMTI6Dc+ZYo4Y505WwdkADcZtobTYonkigUCigKUB9Z8fkrqXtUqlkah86y7YRGIPJRIWzDiZBgKeXFxTyeazKl67EhFH3PKRSKXRlGeuuCyceh01G2HkHr+MxBrLDaysrsJRCOpOBV6sZA84Z3LiuLIeaeDI57yD2JnC338d6LgcVi5n1MTKzc/HxzporZtQmaCCsPfR9p3N/j1KxCMdxfjxES8vLZpAha7medxhF0dafLpJSHevfb+O7AAMAq4AQUHG7TrMAAAAASUVORK5CYII=') no-repeat 50% 50%;overflow:hidden;}
	span.ubUserEmail a {display:inline-block;margin:0;padding:0;width:16px;height:16px;text-indent:-9999px;}

div.k2UsersBlock ul li h3 {clear:both;margin:8px 0 0 0;padding:0;}
div.k2UsersBlock ul li ul.ubUserItems {}
div.k2UsersBlock ul li ul.ubUserItems li {}











 








.avPlayerWrapper div,
.avPlayerWrapper iframe,
.avPlayerWrapper object,
.avPlayerWrapper embed { outline:0; padding:0; margin:0; }


.avPlayerWrapper { display:block; text-align:center; clear:both; }
.avPlayerWrapper .avPlayerContainer { display:block; margin:4px auto; }
	
	.avVideo .avPlayerContainer { padding:16px 20px 14px; border-radius:4px; background:url('data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MSA2NC4xNDA5NDksIDIwMTAvMTIvMDctMTA6NTc6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUuMSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RkZEOEY3MDk0NUU3MTFFMUE3NThCQkRGNjJDQUQwNUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RkZEOEY3MEE0NUU3MTFFMUE3NThCQkRGNjJDQUQwNUIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGRkQ4RjcwNzQ1RTcxMUUxQTc1OEJCREY2MkNBRDA1QiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGRkQ4RjcwODQ1RTcxMUUxQTc1OEJCREY2MkNBRDA1QiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAICAgICAgICAgIDAgICAwQDAgIDBAUEBAQEBAUGBQUFBQUFBgYHBwgHBwYJCQoKCQkMDAwMDAwMDAwMDAwMDAwBAwMDBQQFCQYGCQ0LCQsNDw4ODg4PDwwMDAwMDw8MDAwMDAwPDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAiYD6AMBEQACEQEDEQH/xABpAAEBAQEAAwEBAQAAAAAAAAAAAwIBBAYHCAUJAQEBAQEBAAAAAAAAAAAAAAAAAgMBBBABAAICAgMBAAIDAQEBAAAAAAISAWExQRFR4aEhkfDRUqIiQhEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/wAXwAAAAAAAAAAAAAAAAAAAAAAZrgGQAAATAAABmuwZAAAAAAABMAAAAAAAAFAAaj2DceQbh0CgNR7BSPIKR4BoG49rFodIS2Kbj2sVh0JUQpqPa0rQ6BaPYNx5BuHQLR7BuPIKx4BYFIdAtHsG48grHgFgUh0DUeBm8iPK2jcOgWj2M2gWjwNF48jNuHQNR4BYFIdAtGWwaBSHQLRlsGgaU6rGSXG4y2C19gyONWwpSltA3bIN32DSXGlOs2wBbAK20ILaAtoC2gLaAtoHLaFp2wDQNJcTBK2VOsW0CdsAJcfkJi2AAAAUBmuwK7ArsCuwK7ArsCuwK7BoAEwAAAAAAAAATpoAAAEwAAAZrsGQAAAAAAZrgGQAAAAAAUABqPYNAoCgKA1HkGodAoDce1i0OgbQNx7WNR5EvIQNR7WpqPIlWPALApDoGo8AvHkG4dAtHsG48grHgFgUh0DUeAeRHlYpHhAutm7GQ0VjwM148g3DoFo9g0CkZAtGWwaBSMgWjLYNApGQLRlsGgaU6rGSXG4y2DcZA6OLDqdsKdUtoG7ZBu+waS4AoDKnWkuAMqdaS4AmDN9qdYtkC2QYtoE7YBlKWr7HX5Erti2K7BoAAAAAAAAAAAAAAAGa7ArsGgTAAAAAAABMAAEwAAAATAAAAABmuAZAAAAABqPANAAoCgKAA1HsFodAoCgOx5WlWPCFNArHlaVIdIFFqUjyJVjwCwOx5BWPALApDoGo8AvHkG4dAtHsG48grHgFgUh0DUeBm8iPK2jcOgWj2M2gXjIaNx7GbQLxkC0eQcBeMgbjLYNApGQLRlsGgaU6rGSXG4y2DcZA2DI41bClFsApbQN2yBbIN32BfYF9gX2BfYF9gxbIFsgxbQFtAnbACXAGLZBi2gfkxi2AAAarkGQAdEqAAAAAkKAarkCuQZAAAABmuwaBMAAAAAAEwAATAABmuwZAAAAABmuAZAAAABqPANAA1HsG48grHgGgaj2CkeQUjwDQKx5BSHQlsU1HtaVodAohTUe1pWh0C0ewbjyDcOgWj2DceQVjwCwKQ6BqPALApDoFo9gpHlY1DpA8lbN2PI0VjwCkexmtDoGo8DRYZqQ6BuMgVBQG4yBaMgcBSMgWjLYN2yDCnVL7BS2EuKW0Ddsg2DI40OlsKdLYBS2gLaAtoC2gLaAtoE7YAtgC2AEuAF9gxbIMW0DEpA/J7FsqJcFAFNAU0BTQFNA1XANAAAnTQFNAU0BTQAAOiUhTVcgyAAADNdgyAAAAAACYAJgAAAmAAAAAACYAAAAKAA1HsG48g2CgKA1HkGodAoDce1i0OgbQNx7WNR5EvIQNR7WpqPIlWPALApDoGo8AsCkOgajwC8eQbh0C0ewbjyCseAWBSMgajwC6xSHQNR4Ga8eQbh0NFoy2M2gUh0C0ZbBoFAbjIFoyBwFL7Bq2AXtkHAaU6pbCXC2AUtoG7ZBu+wAZHAGh1lToDSXGRwBodL7Bi2QLZBi2gTtgE77U6mD8uU087VquAaB0S4KAAAAAAAAAAAAdEuCk6aApoAHRKQoAAAABMAAAAEwAATAAABMAAAAAEwAAAUABQAFAUABqPYLQ6BQFAdjytK8eEAKVjytLcOgVFKR5Etw6BaPYKR5FNQ6ErR7BuPIKx4BYFIdA1HgFgUh0DUeAeRHlY1DpAtHsZqR5W0bh0C0ZbGbQKRkNFoy2M2gUjIFoy2DQKA3GQNxlsGgUvsG4yBuMtg3bIMKdUvsGr7S41bAKW0BbQN2yBbIFsgWyBbIFsgWyBbIMW0BbQJ2wBbAJ32p1MGkuM20D8wMWwAAAAAAAAAAAAAAAAAAADoliuBTNNAAmAAAACYAAAAAJgAmAAACYAAAAAJgAA1HgGgAUBSHQNR4BoGo9gpHkFI8A0Dce1isOhKiFNx7WNR5EvIQNR7WpqPIlWPALApDoGo8AsCkOgajwC8eQbBaPYNx5BuHQPIBqPKxSPCBdbN2MhotGQNxlsZtAvGQ0bjLYzaBSMgWjLYNAoDcZA3GWwaABS+watgFLaBu2QYU6ApfYF9gpbCXC2ALYAtgC2ALYAtgC2AZvsGb7U6AmDSXGbaAtoGJSB+Y2LYBVY4gZrsCuwaAB0SAAAA4KZrsCuwaB1YkgAAAAAATpoAGBLgoAABMAAAAEwATAAABMAAAAGZcAyAACgANR7BuPINgoCgNR5BqHQKA1HsFI8rFkCgOx5WlSHSBRalI8iW4dAtHsG48g3DoFo9g3HkFY8AsDseQVjwCwOxkCseAWBSMgajwC6xSMgUGbUZbBYG4yGjcZbGbQKRkC0ZbBu2QcBSMgatgF7ZBwAFL7Bq2AUtoG7ZAtkHAZU6AoAAACYANJcAdtkGLaBO2AZvsC+wTB+bK7YtmxIAtQABXKArkCuQK5ArkCuQK5ArkHa5WOAAISA4KAdWJIAAAAE6aABMAAAAEwAAATABMAAAEwAAAAZlwDIAKAAoACgKAA1HsFodAoAC0eVjcOkJbFNx7WKw6EqoGo9rU1HkSrHgFgUh0DUeAWBSHQNR4BYFIdA1HgFgUh0DUeAXWNw6QNR4Ga62ikZAoM1YyBsG4yGjcZbGbQKX2DcZA3bQNApfYNWwCltA3bIOAApfYF9g1bAFsApbQFtAW0DdsgWyBbIMW0BbQFtAnbAFsAzfYAJg7bIMW0D84obFcoCuQbWkAAAApoCmgKaApoCmgAAAAZrkU5XKAWACEuCgEwAAAATpoHRKQoAABMAAAEwATAAABMAAAAGZcAyDUeAaABqPYNx5BWPANA1HsFI8g1DoFAbj2sbjyCyBQHY8rSpDpAotSkeRLcOgWj2DceQbh0C0ewbjyDYLR7BuPINgtHsG48g3DoFo9gpHlY1DpAoM1VtFIyBQZtRlsGgXjIaNxlsZtApfYNxkDcZbBu2QcBS+watgFLaAtoG7ZBwAFL7AvsC+wL7AvsC+wL7AvsC+wL7BMAAHbZBi2gTtgGb7B+ekNACmgdppAoAAAAAAAAAAADlcAnTSwAABiuUKFgAhLgpMAAAAEwTAAABMAAAEwATAAABMAAAAEwAUABqPYNAoCgANR7BaHQKAoDseVpUh0gbFNx7WKw6EqoGo9rU1HkSrHgFgdjyCseAWB2PIKx4BYHY8guCgOxkCseAWBqMljUZIFBmqtopGQNWwM1Iy2DQKX2NFoy2DdsjNwFL7Bq2AUtoGgAUvsC+watgFLaAtoG7ZAtkHAAAAAAdtkC2QYtoC2gTtgC2AZvsEwAfA2LQABwUAAAAAAAAAAAAA6JAATpoHKaWAMCiuUAsEJcFJgAA6JSpoU6JSFAAJgAAAmACYAAAJgAAAAmDUeAaABQFIdA1HgGgaj2CkeQah0CgNR7BuPK0roUoDseVpbh0CopsSpDoFo9g0CgNR4BYFIdA1HgFgUjIG4yBUHYyBWPALA1GSxqMkCgzVW0UjIGrYBSMtjNoFL7BuMho3GWxm0ACl9g1bAKW0Ddsg4ACl9gX2DVsAWwBbAFsAWwBbAFsAWwBbAFsAzfYF9gmADtsgxbQJ2wD4QxbAAAKrGa7ArsCuwdEgAAOV2KK7ArsGgASQAAAOiQEqaWAOoE1qAcQOrEkAACdNAAmAACYAAAJgAmAACYAAAAMy4BmHQKAA1HsGgUBQAGo9gtDoFAUBqPKxqHQlRCm49rGo8iXkIFFqI8iVY8ApHsFI8imodCVo9g0CgNR4BYFIyBuMgVBSMgUBQGoyWNRkgatgZrraOxkCtsApbQzbtkGwL7GjVsApbQzaABS+watgFLaAtoG7ZBwAAAAFL7AvsAEwAAAdtkGLaAtoE7YBm+wL7B8PQ2ZrsHRIAAADtcgVyBXIFcgVyBXIFcg4AAADldimgSQAAOiQEqaWAJoU6sEJcFJgAAAmCYAAJgAAAmACYAAJgAAAAnPsGo8A0ACgKQ6BqPANA1HsFI8g1DoFAaj2CkeViyABqPa0rQ6BRCm49rGo8iVY8AsDseQVjwCwOx5BsFo9g0CkOgbjIFQUjIFAUBqMljUZIGrYGa6x22Ro3fYNWwM1LaBoFL7Bq2BoWwCltDNoAFL7AvsGrYAtgFLaAtoC2gLaAtoC2gLaAtoC2gLaBO2ALYBm+wL7BMAHbZB8RQ0AdrkCuQbAAAAAAAAAAABiuQK5BwAAHK7FNAkgAdEgJLAE0KdWOIEwAAATBMAAEwAAATABMAGZdAyAAAACYKAA1HsGgUBQAGo9gtDoFAAbj2sVh0JUQpVYR5EqQ6QKLU2JUjIGo8AsCkOgajwCwOx5BsFoy2DcZA2DcZAqDsZAuDVtApbKxqMkDVsAutm7bI0bjIGrYAtgZqW0DQKX2BfY0atgFLaGbdsg4AACl9gX2BfYF9gX2BfYF9gX2BfYF9gmAADtsgxbQJ2wNC2AfGa5QNgAA7TSBQAAAAAAAAAE6aBymlgAADFcg4ADldimgSQAOiU6aBxYmhTqxxA6sSQAAJgmAACYAAAJgAmAACYAAAMy4BoAAFAUh0DUeAaBQGo8g1DoFAUBqPKxuHSEtilVhHkS8hAotRHkS3DoFo9g0CgNxkCoOxkC4KA7GQNg3GQKg7GQN32DVsAusdjIGr7QNWwM11jtsjRu+watgFLaGZbQN2yDgKX2BfY0atgZqW0NC2hmW0DdsgWyBbIFsgWyBbIFsgWyBbIMW0BbQ0TtgC2BmzfY0L7GaYPkVNIaKoAAAABagAAAAAABCQAAAEqaWAAMVyDgAOV2KdQJg6JASWMIBahCRakkAACYJgAAmAAACYJgAAmAAAADP/AOgaABqPYNAoCgANR7BSPIKR4BoFVjseQWQAKLSpDoFUCi1EeRK0ZA3HsGxTYlqPALA7GQLg1bQN2yDcZA3GQKg7GQN32DVsAsDVsrGr7QNWwCltDNtbR22QbvsGrYAtgZqW0DdsgWyDgKX2BfY0L7Bq2ALYGZbAFsAWwBbAFsDQtgZs32NC+wL7GaYAO2yDFtAW0D5SxaAC1AKAzXYFdgV2DQOCQAHK7FFdgV2DQJgAISAAmDiwBiuQcAB0UkgAdEpLAGBQhItSSAABMEwAATAABMAEwAZl0DIAAAMy4AjwDQAKA7HkG4dAoCgNR5BoFAUBqPKxqHQlRClVhHkS3DoFEKVWEZCVY8AsDUeRTQluMgVB2MgXBqMtg0CkZA1bAKW0Ddsg3GQNWwCltA2sdtkGr7QNWwCltDNtbR22QbvsC+watgC2BmpbQFtA3bIFsgWyDgAAAAAO2yBbIFsgxbQFtAnbAFsAzfY0L7B8vQpQGa7B0SAAAA7XIFcgVyBXIFcg4AAADldiiuwZAAQkBMHFgDFcg4DopJAA6JSWAMChCRakkAADAlIUAAmAACYAJgAmAAAADP8A+gaABqPYNAoDUeAaBqPYKR5BqHQKAoDUeVjcOkJbFKrCPIlSHSBRamxKkZA3GQKg6KbEtxkCoOxkDYLW0DQO2yDYNWwCwO2yDd9g1bAKW0DttLG7ZBq+0DVsAWwM11jNtDRu2QLZBu+wL7Bq2ALYGZbAFsAWwCltAW0BbQJ2wBbAFsAWwNC2AZvsC+wYtkC2QcGb5shoAA7XIFcg2AAAAAAAAADFcgVyDgAAOV2KaBMBCQElgDFcg4DopJAA6JSWAMCnEDqxJAAnPsHRKQoBMAAAEwTAABMAAAGZcAR4BoAGo9g3HkGwUABqPYKR5BYAGo9gpHlYtHhCQUqsIyEvIQC1NiVIyBuMgVB0U2JbjIFQdtkGwatgFgdtkG77Bq2AWB22QbvsC+watgFLaB22ljdsg1faAvsGrYAtgZrrAaO2yBbIFsgWyDd9gX2BfYF9gX2BfYMWyBbIFsgWyDgAzYtpAnbAFsDR8+rkCuQbAApoGq4BoAAAAAAAAE6aApoAEwdrkHAAcrsUyAhICYOLGK5BwHRSSAB0SksdQJrU4gTAB0SkKTAABMAAEwATAABMAAAEwUAABQFIdA1HgGgUABQFAAVWEeRK6FAKLS7GQPIQC1NiVIyBuMgVBq2RTQluMgVB22QbvsGrYBS2gaBS+wL7Bq2AWB22QbvsC+watgFLaBoGrZWFsg1faAvsGrYAtgC2BmpbQFtA2sAAAAAZtoHLaQJ2wBbAFsDRm+wL7Bm2VhbIPRUBTQKAAAA1XYNoAAAAAHFiNcA0AACdNAAAxXIOAA6KTAQkBJYmADopJAA6JSWJoU6scQJgAAmCYAAJgAAmCYAAJgAAAzLgGYdAoADUewaBQGo8A0CgAKAoACqxuHQlRCgFFpUjIFUAtTYlSMgbjIFQAUvsGrYBS2gaBS+wL7Bq2AWB22QbvsGrYAtgFgdtkC2QbvsGrYAtgFLaBoGrZWFsgWyDV9oC+wL7AvsC+wL7AvsC+wL7AvsC+wL7Bm2VhbIFsg4DiBG2AeogAAqgAcWAAAAAAAAAOoAHFiYAJ00ACYAAOikwEJASWJgA6KSQAOiUliaFOiRakkACc+wTAABMAAEwATABMAAAAEwajwDQANR7BoFAUABqPYKR5BqHQKAoAtKkOgUQoBRaVIyBVALU2JdjIFoyBu2gaBS+wL7Bq2AWB22QbvsGrYBS2gaB22QbvsGrYAtgFgZtoG7ZBu+wL7Bq2ALYAtgFLaBoAHVjNtA3bIFsgWyBbIMW0BbQNA4gZtoC2gTtgC2AZvsC+wepgqgcWAAAOigHa5ArkCuQK5ArkHAcEgAAOoHFiYAJ00ADFcg4DopMBCQEljEuQcB0UkgdEpz7BxYwKcQJgAAmCYAAJgAAmCYAMy6BkAAAGZcAyCgAAKA7HkGwUABQGo8g0CgKA1HlY0JUQoBRaVL7BVALU2JdjIG77Ba2gaBq2RTV9iWrYBS2gaB22QbvsGrYBS2gaB22QbvsC+watgFLaBoGbaBu2QLZBu+wL7AvsGrYAtgC2ALYAtgC2ALYAtgC2ALYBm+wL7AvsGLZAtkC2QcB62AADop2uQZEqAAAAAAAAAzXIorkGRIADqBxYmACYMVyDgOikwEJASWMS5BwHRSSB0SlPtYmhTqxxAmACc+wdEpCgEwAATABMAEwAAAATAh0CgAKAApDoGo8A0ACgAPIABQHYyWlsFEKAUWl2MgXthAClVhbIlu+watgFgdtkG77AvsGrYBYHbZBu+wL7Bq2AUtoGgdtkC2QbvsC+watgC2AUtoGgAdtkC2QLZAtkC2QLZAtkC2QLZAtkC2QLZBi2gaBm2gLaBO2ALYB/BB0UA2JAKaApoFAAAAAATApoAAAGBTgkAB1AksATBMAHRSYCEpg4sYlyDgOikkDolJYwKEJSFAOiUhSYAAJgAzLgGQTABmXQMgAAAmADUeAaABqPYNAoDUeAaBQAFAUABQHYyWlu+wUQoBVYWyJUvtA2KVWFsiW4yBq2AUtoGgdtkG77Bq2AUtoGgdtkG77AvsGrYAtgFgAdtkC2QbvsC+wL7Bq2ALYAtgC2AUtoC2gLaAtoC2gLaAtoE7YAtgC2ALYBm+wL7Bi2QLZB/HFNiQAFAAAZrgCuAK4BSuwTrgCuAaAAABOmgAAYFOCQHUCSwBMGJcg4DopMBCUlgCYOS6FMIHRKc+wATWpxAmACc+wYEuCmZdAyAACYMS5BwAEwAAAZlwDIAKAAA1GWwaBQFAAUABQFAAUBq2VjQlRCgFVhbIlu+wVQC1NiXbZBu+watgFLaBoHbZBu+watgFLaBoAHbZBu+wL7Bq2ALYBS2gaAB22QLZAtkG77AvsC+wL7AvsC+wL7AvsC+wL7Bi2QLZAtkC2QcABm2gfzQAUABmuAWAAAAAAAAABmuwZAABMAGBTgkB1AksATBiXIOA6KTAQlJYxLkHAdFJIHRKSxiXKFAlIUAAmCYAJgAAmACYAJgAAAAmAACgAANR7BoFAUABQAFAUABQHLaWK32JUQoBVYWyJbvsFLYQApu2ljVsiW77Bq2ALYBYAHbZBu+watgC2AUtoGgdtkC2QbvsC+watgC2ALYBS2gLaBoAAAAAAAAAGbaAtoE7YAtgC2AZvsC+weOADVdg0AAAAAAAAAAAAAACYAJgAwKcEgOoElgCYJgAwKEJTBxYmDopJA6JTn2DixgU4gTABOfYJgAAmAACYJgAzLoGQAAATAAABQAAFAAUBQAFAAUBq2AaBQHVjtsg3faEtilAFpdtkG77BVALU1bQNWyJbvsGrYBS2gaAB22QbvsC+watgFLaAtoGgdtkC2QLZBu+wL7AvsC+watgC2ALYAtgC2ALYAtgC2AZvsC+wL7AvsGLZAtkC2QcBMFAAAAAAdrkCuQK5ArkCuQK5BwAAAAAEwATABgU4JAdQJLEwTAByXQpkBCUljEuQcB0UkgdEpLGBTiBMAGZcA4JSFAJgAAmDEuQcBmXQMgAAAnPsAAACHQKAAAoACgKAAoAACl9goACgNWysavsSohQDdtLGrZEt32ClsIAUqsYtoS3bIN32DVsAWwCltA0DtsgWyDd9gX2DVsAWwCltAW0DQAAO2yBbIFsgWyBbIFsgWyBbIFsgWyBbIOAAAzbQJ2wBbALAAAA7XIN00AAAAAAAAADFcg4AAAACYJgAwKcEgAJgmDEuQcB0UmhICSxMHJdCmEDolKfaxiXKFAkWpJA6JSFJgAmAACYAJgAmAAADMuAZAAAAh0CgAAKAAoCgAAKA1bINAoACgNWysavsS1bCFNAqscEu2yDd9gqgcFN20sctoS3bIN32BfYNWwCltA0ADtsgWyDd9gX2BfYNWwBbAFsAWwCltAW0BbQFtAW0BbQFtAW0CdsAWwBbAFsAzfYF9gX2DFsg8sAAFKaAAABQAAAAAEwAAAYrkHAAAATBMAGBTgl1AksATBMHJdCmQEJSWMS5BwGBTiAlwJTWMCnECYAJz7BMAAEwAZlwDIJgAmAAAACYAAAANxkDoAAKAAoCgAAKA1bINAoACgOW0sbtkGr7EqIUAqscEu2yDd9gqgcFKrGbaBq2RJbIN32DVsAWwCltAW0DQAO2yBbIFsgWyDd9gX2BfYF9gX2BfYF9gX2BfYF9gX2DFsgWyBbIOAAA8wAFAAAUAABmuAK4ArgCuAK4BoAAEwAYrkHAAATABMAGBTgl1AksTBiXIOAwKEJTn2DixMHJdCmEDolJYzLkUygTABmXAOCUhTMugZAABMEwAZl0DIAAAJgAAAAA3GQOgAAoACgNWwDQANW0DQKAX2CgANW0ClsrG77AvtCWxQDdtLHLaEt2yDd9gpbCAFOrGraBy2hLdsgWyDd9gX2DVsAWwCltAW0BbQNAAAAAAAAAAAAzbQFtAW0CdsAWwDN9g/qAoACgAM1wCldg0AAAAAACNcA0ACYAMS5BwAAEwATBmXIpkS6gSWMy4BkEwcl0KZAQlJYxLkGZdCmEDolKfaxiXKFAlNanEACYJgAmAACYMS5BwGZdAyAAACYAAAAAANxkDoAANW0DQKAAoACgANWyDV9goACgOW0satkSpfYpu2EJBQDdtLHLaEt2yDd9g7faBQHBTqxq2gLaBq2RJbIFsg3fYF9gX2DVsAWwBbAFsAWwBbAFsAWwBbAFsAWwBbAM32BfYF9gxbIFsgWyD+2ACgAM1wCwAAAAAAAAAAJgAmADEuQcAABME59gAwKcEgJgmDEuQcBgUISksYlyDgJoUAxPsS4sYFOIEwdEpCkwATAABMCfYJgAmAAACc+wAAAAAAAL7BQAAGraBoHbZBsFAAAatoFLZBq+wL7BQAFAdWFsiW77BS2EAKAVWM20DVsiS2QbvsHb7QN2wAKAbtpYW0BbQOW0JLaBu2QLZAtkC2QLZAtkC2QLZAtkC2QLZAtkC2QYtoHbaFFtAW0DIP74lQAFAAAAAAAAAAAAAATABMAEwAARlwDQJgzLkUyJdQJLE59gxLkHAYFCEpT7WAJg6KSQEuBKaxgUxLpAyACc+wZlyDIJgAAmACYAJgAAAzLgGQAAAAAAAAUAABQAHbZBsGrYBoAFAZtoFLZBq+watgGgAUBy2ljdsg1fYl2+0DYoABu2ljltCW7ZAtkG77AvsHb7QN2wDoOCgHVjNsApbQFtAW0BbQFtAW0BbQFtAnbANA4gAdEgJ32Dl9rHsgKAzHgFgAAAAdrkGwAYrkCuQcAAAABMAEwYlyDgAAJgmDApwSAmDMuAZBMHRSaEgJLEwcl0KYQOiUljMuRScukDIAJz7BMAAEwATn2ACYAJgAAAzLgGQAAAAAAAAAUAAABq2gaBQAFAAAatoFLZAtkGr7BQAAFActpY1bIl22RTd9oS7bApoAAG7aWFtA5bQlu2QLZAtkG77AvsC+wdvtAX2BfYN2wBbAFsAWwDF9gX2BfYOX2sL7AvsGLZAtkC2QLZB7WACgAAAAKAAAAAAmAAAACMuAaBMAEwAATBOfYAMCnBICYJgmDkuhTICEpLEwcl0KYQOiUljEuUKcBMAE59gmADMugZABmXAMgmACYAAAMy4BkAAAAAAAAAAGrYBoAAGraBoHbZBsGrYBoAFAZtoFLZAtkGr7Bq2AaABQHLaWN2yDlsiVL7FF9oS3bAAoAABVYzbQFtA0DFtCW7ZAtkC2QLZAtkC2QLZAtkC2QLZBi2gLaB22hRbQFtA6gTB7gtLMeAWAAB2PINgAAoACYAAAJgAAAmACYMS5BwAEZcA0CYMCnBLqBJYmDEuQZl0KZAQlJYxLkGZdCmEDolKfaxiXKFAlIUAzLgHBKQpmXQMgAAmCYAAJgAAzLgGQAAAAAAAAAAAatgGgAAUAABS+wAatgGgAUBm2gUtkC2QavsGrYBoAAFActpY1bKAtlaXbZFNX2JdvtA3bAAoAAAAABQAAAAAEwAAAAAALYEsX2D3NYoAAACgAKAAAAAAmADEuQcAABGXANAmDEuQcABGXANAmDMuRTIl1AksTBiXIOAwKEJSWMS5BwE0KdEpT7WMS5QoEpCgAGBKQpmXQMgAAmCYAAJgAAAmAAAAAAAAAAAAACgAAANW0DQO2yDYF9goAACgM20ClsgWyDV9gX2DVsA0AACgM20DttLGrZQFsrSWyDtsim77AvtCS+wL7AvsG7YAtgC2ALYAtgC2AYvsC+wL7AvsHL7WO32KTtkSWyDVsoHuqxQAAHY8g2ACgAAM1wDQAAJgAmAACMuAaBMGJcg4ADMugZBMGZcimRLqBJYnPsGJcg4DAoQlJYxLkHATQp0SlPtYxLlCgSmtTiABgSkKATAABMEwAZl0DIAAAJgAAAAAAAAAAAAAA1bANAAAA1bQNA7bINgX2CgAAANW0BbQKWyBbINX2BfYNWwDQAAAKAzbQO20sLaAtoGrZQNWyJZtlYWyDtsinLZElsgWyDtsinLZElsoUWyDNtLC2gLaBy2kDQJg96jwtKwAAOx5BsAFAAUGYCMuBo0ACYAJgAAjLgGgTAn2CYAMy6BkEwZlyKZEuoElic+wYlyDgMChCUljEuQcBNCnRKU+1jEuUKBKa1OIAGBKQoBMAAEwTABmXQMgAAAmAAAAAAAAAAAAAAAACgAAAKAzbQNA7bINgX2DVsA0AACgM20BbQKWyBbINX2BfYF9g1bANAAAAAAAoAAAAAAACYAAAAAAM2wD3yPC0rAAA7HkGwAUABQZgIy4GjQAJgAmAACMuAaBMCfYJgAzLoGQTBmXIpkS6gSWJz7BiXIOAwKEJSWMS5BwE0KdEpT7WMS5QoEprU4gAYEpCgEwAATBMAGZdAyAAACYAAAAAAAAAAAAAAAAANWwDQAAAKAzbQNA7bINgA1bANAAAAoDNtAW0ClsgWyBbINX2BfYF9gX2BfYNWwBbAFsA0AAAAADNsAWwBbAFsAzfYF9gX2BfYM2yD6EtKgAAOx5BsAFAAAAAAATABMAAEZcA0CYMS5BwAGZdAyCYMy5FMiXUCSxOfYMS5BwGBQhKSxiXIOAmhTolKfaxiXKFAlNanEADAlIUAmAACYJgAzLoGQAAATAAAAAAAAAAAAAAAAAAABq2AaAAABQGbaBoHbZAtkGwAatgGgAAAAUBm2gLaAtoFLZAtkC2QLZAtkC2QavsC+wL7AvsC+wL7Bm2QLZAtkC2QLZAtkE7aAtoC2gfRlpUAAABQAAFAAAAATABiXIOAAAjLgGgTBiXIOAAjLgGgTBmXIpkS6gSWJgxLkHAYFCEpLGJcg4CaFOiUp9rGJcoUCUhQADAlIUzLoGQAATBMAAEwAAATAAAAAAAAAAAAAAAAAAAAABq2AaAAAABq2gaAB22QbvsAAGrYAtgGgAAAAAUBm2gLaAtoC2gLaAtoC2gLaAtoC2gLaAtoC2gLaBoEwAAAfSFpZjwCwAAOx5BsAAFAATAABMAAAAEwATBiXIOAAjLgGgTBgU4JdQJLEwTByXQpkBCUljEuQZl0KYQOiUp9rGJcoUCUhQDMuAcEpCmZdAyAACYJgAAmAADMuAZAAAAAAAAAAAAAAAAAAAAAAABq2AaAAAABQGbaBoAHbZAtkG77AAAvsGrYAtgGgAAAAAAAAAAAAAAAAAAZtgC2AfSlpAUAAAB22QbAAAAABMAAAAEZcA0CYAJgAAmCc+wATABGXANAmCYOS6FMgISksTByXQphA6JSWMS5QpwEwATn2CYAMy6BkAGZcAyCYAJgAAAzLgGQAAAAAAAAAAAAAAAAAAAAAAAAAL7Bq2AaAAAABQGbaAtoGgAdtkC2QbvsC+wAAAL7AvsGrYAtgC2ALYAtgC2ALYAtgGb7AvsAAAAC+wYtkH0taVAZjwCwAAAAO2yBbIFsgWyBbIOAAAAAmACYMS5BwAGZdAyCYMy5FMiQEwTn2DEuQcB0UmhLkuATWJg5LoUwgdEpLGBTEukDIAJz7BMAAEwATn2ACYAJgAAAzLgGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAatgGgAAAAAAUBm2gLaAtoGgAAAdtkC2QLZAtkC2QLZAtkC2QLZAtkC2QLZAtkHAAAAZtoH05aVAAZjwCwAAAAAAAAAAAAJgAmACYAAJgAmDMuRTIl1AksTn2DEuQcBgUISlPtYxLkHAdFJICXAlNYwKYl0gZABOfYMy5BkEwAATABMAEwAAAZlwDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANWwBbANAAAAAAAAA1bQFtAW0BbQFtAW0BbQFtAW0BbQFtAW0BbQFtAW0BbQNAmAAD6gtICgAM2wCwAAAAAAAAAAJgAmACYAAAJgnPsAEwAATBMEwdFJgISksYlyDgJoU6JTn2Ca1AAJIHRKQpMAEwAATBiXIOAAmAAACc+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL7Bq2ALYBoAAAAAAAAAAAAAAAAAAAAAAAAH0xaVAAUAABq2gaAAAAABm2gTtgGgATABiUgcAABMAEwYFOCXUCSxOfYJgA5LoUwgdEpLGJcgzLoUwgdEpT7WMS5QoEpCgAGBKQoBMAAEwYlyDgMy6BkAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL7Bq2ALYAtgGgAAAAAAAAAAAAAZtgC2ALYAtgGb7B9QWkBQAAFAAAZtgC2ALYAtgGgAAATABi2QcAABMAEwTAAABMEwYlyDgMChCU59g4sTB0UkgdEpLGZcimUCYAJz7B0SkKZl0DIAAJgmADMugZAAABMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC+wL7AvsC+wAAAAAAAAAAfUFpAAdtkGwAL7BQAAAAAEwAAATAAAABMEwAYFOCXUCSxOfYAJg5LoUyAhKSxiXIOAmhQBLgSmsYFOIEwATn2CYAAJgAzLgGQTABMAAAGZcAyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6gtIAAADtsg2AAAAAAABfYMWyDgAAAJgAmADApwSAmACYMS5BwHRSaEpg4sTByXQphA6JSn2sYlyhQJFqSQOiUhSYAJgAAmACYAJgAAAzLgGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfTFpUAAAAAB22QLZAtkC2QLZAtkHAAAAAATABMAGBTgkB1AksTBMAHJdCmQEJSWMS5BwHRSSB0SksYFOIEwAZlwDglIUzLoGQAATBiXIOAzLoGQAAATn2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6YtIDNsAsAAAAAAAAAAAAAACYAJgAwKcEgOoElgCYJgAwKEJTBxYmDopJA6JSn2sYFAOIEwATn2CYAAJgAAmCYAMy6BkAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfSFpL7BQAGbYBYAAAAAAAAAEbYBoAEwAAYFOCQHUCSwBMGJcg4DopNCQEljEuQcB0UkgdEpLGJcoUCUhQACYJgAmAACYAJgAmAAADMuAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9GtlY0JAL7BQAAAGbYBoAGbYAtgGgAATAABgUA4JAdQJLAEwYlyDgOikwEJSWAJg5LoUwgdEpT7WMIBanECYAJz7BMAGZdAyAACYMS5BwAEwAAAZlwDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPoi0uinbZBoSAAX2ACgAAAJgX2BfYAAMCgHBIADqBJYAmDEpA4DopMBCUwcWJgAmhQDolJYwKEJSFAAJgmACYAAMy4BkEwAZl0DIAAAJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+iLSAA6KdtkC2QaEgAAAAAAMCnbZBwHBIADqBJYAmADEpA4DopMBCQEljEuQcB0UkgdEpLE0KdWOIEwATn2DAlwUAmAACYAJgAmAAAACYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPoC0qoHFgAAADooB22QLZAtkHAAcEgAAOoHFiYAJgAxbIOA6KTAQkBJYxLkHAdFJIHRKc+wcWMCnECYAAJgmAACYAAJgmADMugZAAABmXAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gLSAAoDqBxYAAAAAAAAA6gASWAAJ32ADFsg4ADopMBCQEljEpA4DopJAA6JSWMChCRakkACc+wTAABMAAEwATABMAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAe+LSX2DVsA0AADNsApbQNA6gAAcWI2wDQAAAJ32ACYO2yDgAOikwEJASWMSkDgOikkADolJYmhTqxxAmAADAlIUAmAAACYJgAzLoGQAAAZlwDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPeFpdtkGwAL7AvsGrYBoAAAAAAGbYBm+wAAYtkC2QcAB0UmAhICYOLGLZBwHRSSAB0SksTQp1Y4gTAB0SkKTAABMAAEwATABMAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAe720sdEgO2yBbIFsg2AAAAAAACYO2yBbIOAAA5bQpkABCQElgDFsg4DopJAA6JSWOoE1qcQJgAAnPsEwAATAAABMEwAZl0DIAAAMy4BkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHuqxQGbaB0SAAAAAAAAAAAAA5bQpoEwAEJASWAJgAA6KSQAOiUlgDApxA6sSQAJz7B0SkKAATAABMEwAATAAAABMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHuokBwU6sAUBm2gLaAtoC2gLaAtoC2gLaBoEwAAEJAATBxYAxbIOAA6KSQOiQEljCAWoQkWpJAAAmCYAAJgAAmACYAMy6BkAAAGZcAyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD3BaXb7QKAAAAA4KdWAAAAOIHRIAAACV9rAAGLZBwAHRSSAB0SAksTQp1Y4gdWJIAAEwTAABMAAAEwTAABMAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAe3/xpY74z/wBA54z/ANCWvGf+kCnnP/IOfzoD+dAfzoD+dAfzoD+dAfzoD+dAfzoD+dA75z/yCX8aBzxn/pY74z/0Kc/jQO/zoEv6A/oD+gY/pAf0B/QH9A5/OgY/+feFjn8+8IE/594A8Y9x/wDIMeN4/wDIHjeP/IHjeP8AyB43j/yCXjPvP5/oDxn3n8/0CXnH/P7j/QNf/Xr9x/oEvOf+c/3j/QHnP/Of7x/oDzn/AJz/AHj/AEB5z/zn+8f6BP8Aj1+/AP49fvwE/wCf8z8A/n/M/Ac8bx/fwGP5/wAz8A/n/M/AP5/zPwD+f8z8Al/n8/AT8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwDxn3+/APGff78A8Z9/vwH/2Q==') repeat 50% 50%; }
	
	.avAudio .avPlayerContainer { }
.avPlayerWrapper .avPlayerContainer .avPlayerBlock { display:block; padding:0; margin:0; line-height:normal; text-align:center; }
	.avPlayerWrapper .avPlayerContainer .avPlayerBlock div { text-align:center !important; }
	.avPlayerWrapper .avPlayerContainer .avPlayerBlock .avDownloadLink { text-align:center; padding:4px; font-size:11px; }
		.avPlayerWrapper .avPlayerContainer .avPlayerBlock .avDownloadLink a {}
		.avPlayerWrapper .avPlayerContainer .avPlayerBlock .avDownloadLink a:hover {}
			.avPlayerWrapper .avPlayerContainer .avPlayerBlock .avDownloadLink a span {}
			.avPlayerWrapper .avPlayerContainer .avPlayerBlock .avDownloadLink a:hover span {}
		.avPlayerWrapper .avPlayerContainer .avPlayerBlock .avDownloadLink span.hint {padding-left:4px;color:#999;font-style:italic;}
 




#rokbox-wrapper.rokbox-light{}
#rokbox-wrapper .clr {clear:both;}
#rokbox-close{height:30px;width:20px;display:block;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAAA3NCSVQICAjb4U/gAAAAOVBMVEV7e3vf39+FhYW1tbX//vqZmZnt7e3MzMzFxcWmpqaTk5Pm5ub3//+NjY29vb2rq6v29vbY2Nj///9pvpTmAAAACXBIWXMAAAsSAAALEgHS3X78AAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1MzmNZGAwAAABR0RVh0Q3JlYXRpb24gVGltZQA1LzUvMDi/lkjgAAAAqklEQVQYlVWR2xaFIAhEMTU060T8/8ceBmlV8+BlL4QBSYtJTSI6te/ksHAnU+US0JQ2CtWBO+BFL7UJE33EDg8885xHXmwVgwym2og28Us2iJjNDDRjDhel1ROdbqzMkitFmbM8NhLFoaKfNs8XjYfddJDA0WLsQt4foJBi756vz0rdLAkMZw+pDc8SOuJvm3kOJL9ZjyndViIuoK4+CQvzcQYEZx73h/wBMdULfShJfYYAAAAASUVORK5CYII=') 0 0 no-repeat;clear:both;}
#rokbox-close span{display:none;}

#rokbox-top.rokbox-left{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAATFJREFUOI2VlDFOxDAQRZ+TbDYgEGshhEBIiAS2oAWUhjI9yym4FdegnQNwAS6RJkXK0KxXtrGdMNJo7Inn6Y8nsuJ/piLxsFYsM5WAOd/mgD7IX//JpYDBgr1nAWiy5dBhH5YFckGFc5AssFcx4BJQzBWQ+WNPwfJEPJwrFrRqF4bcPqMKC2aD/XZyoIhEB2oDY+pMse12LheRXdM0z5vNZlsE1PkKQ9AVUIjIe9u2H1VVXeFBTEEJHAEnwBlwDlwCN8Ad8AA8dl332vf99xQwLBUrYA0cA6eABi6Aa+AWuDewYRh+QrBpmqbUAExrxkug7Pv+U2v9RMSymftz7lBEdimYfX8pdeXe1+M4fvkDiCkM/TKOQhF5m4MZYOyNc8B1Xb/MwXyFPtR5nrTW2yXAX1nyTmK6gctAAAAAAElFTkSuQmCC') 0 0 no-repeat;clear:both;}
#rokbox-top .rokbox-right{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAYBJREFUOI2NlL1OwzAURo+TAAkCIRchBEJCVMDAitgYMnRhgmfgrXgKeISslcoDFKkzW1spS6E0ZoiT3jp/vdKnazm5J5/t6yggIA/DOkxD7gwf2AEU4NlMS+6MwEKN40ZKiWfKebcRWAcyQNZSXDsfkC/ZBRRj5cy5kAo0sHJdZeR76kKzLqcBsOsUSCmhTABccAmVwAK6smOZ5SlL9xVoATSi2LfZE7CVLSrAGdVWKoFhjaMV8Me6N5VbKMAbLaXSNB3P5/OvyWQyiuP4Q8D+nLGcKyS3ygCZMsaUX1wsFt/D4fAtjuN3W7wUoOVWUFMT0+l0NBgMHoE74Aa4Ai6AU+AYOAIOgMieQXFBVC3QGGPSNB0L6DVwCZwDJ4AGDoF9YI/8cviAt7FkN2az2Wev13sFfq2WQnL5Ze96TTAArfV9kiQvYkk++clLbXRBq0PIDyqKoifgp8ZpxWWrQ4AwDM+SJHlucKgc0QkE6Pf7Dx2g8ke8FVBrfeuAXChF/gd3renTPyNfPQAAAABJRU5ErkJggg==') 100% 0 no-repeat;}
#rokbox-top .rokbox-center{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALcAAAAUCAYAAADP5qzfAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAHNJREFUaIHt17ENA0EMA0G+rbfdf8Pn5Fs4HEDMAMoZbKIrySdQaJK8T4+AHcRNrXkO6kyS+/QI2GHioaSUuKklbmpNkt/pEbDDJPmeHgE7XGutdXoE7PA6PQB2ETe1xE0tcVNL3NQSN7XETS1xU0vc1PoDq2QEa0AoQ8QAAAAASUVORK5CYII=') 0 0 repeat-x;height:20px;margin-left:20px;margin-right:20px;}
#rokbox-middle {overflow: hidden;}
#rokbox-middle.rokbox-left{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAACaCAYAAACpM2ilAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAGFJREFUaIHtzLkNhFAAQ8HHsUD/XVHVJ4GcDZHGkhPLmqnaquXuWv3u7elR7WOMsxeZ35z+CRAIBAKBQCAQCAQCgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoHA74IXDZEFMAlVIXEAAAAASUVORK5CYII=') 0 0 repeat-y;clear:both;}
#rokbox-middle .rokbox-right{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAACaCAYAAACpM2ilAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAGJJREFUaIHtzLENwkAQAMF9mqL/rgDzOlJCO7Q0K206a2amE621ntW7elWfv4/qW+1qP85gVwICgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIBAIBAKBQCAQeF/wB/U9Djpyito4AAAAAElFTkSuQmCC') 100% 0 repeat-y;}
#rokbox-middle .rokbox-center{background:#fff;margin-left:20px;margin-right:20px;}
#rokbox-bottom.rokbox-left{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAU9JREFUOI2VlDFOwzAUhr/EoQ1SJBQxICQWItGBFaEOLL0BqKdg63WYOAAXYM1awQkYcgRnaKUuTcJih1fXSc2Tnv5fVvLpf7aTCEgABcTGJ8AZMDE6BaabzeYjy7IZJyo22gm13Upf1/XPKZgF+mAS2gJtVVVfIcCIv3Fj4+XYdvQJMN3tdp9pml6HJBwa2XYDNOv1+i0kYey0L2WfVGv9nuf5w1hCN5lM14jeA/vlcrnabrejB2T3MTEpzoEMuAAugSvgBrgF7oD7xWLxpLX+7jwVGaBtObbiePwDX5bly3w+f5UHFYnRQ6C25Zoqy/K5KIrHPM9nkQC7QBecDKgSz8XK2cuI43LXfAdotVEMlw/u3lcLa4xvx4Bj0KGEJ4G+dD5w/1X9ByihUuVfqQsFdh7v/TslgRB3vePwqmG9b9N95b7oau9DgRLs097/Am7lkwumRvoDAAAAAElFTkSuQmCC') 0 0 no-repeat;}
#rokbox-bottom .rokbox-right{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAVNJREFUOI2NlDFOwzAYhb9AhBSJCqkMLCwMLEzsLF5ZM3AALsB1egw4RlR6gQxZu9WLB5PimgG7dR3XrqUnW0nel/f/iV1Zay2FoZTqZ7PZG/DjtAVGN/867QBzUYIBSCl7Z7BO4doHsgBnAYdhWDqIl00IwFalkrXW66ZpXvkvdeRQaliu8S8rJuy6buEMe1Om5HxCKeVqPp+/J5Il02UTKqX6tm0/ImOcdNpHmxibzeZbCPECPAGPwANwD9wBt8ANcA00wBVQA5dAVYeptNbrrusWQohPlyosLUx5spe1UqqXUvbDMCyFEF/OEALOgflhK+DZ3TTBbCKgyUCPUtaA5vD3x9BYKdA+Ha6ZY/SAScB35L/uHhwCLdMEKaX28gQY3oiNJdDRxqhds1OnSAkygXngNmhqrJN7NgXzQJMxxNfidRaYMsZz8XT3wNB4CnbW+APKHkDW9sCXLgAAAABJRU5ErkJggg==') 100% 0 no-repeat;}
#rokbox-bottom .rokbox-center{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALcAAAAUCAYAAADP5qzfAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAUdEVYdENyZWF0aW9uIFRpbWUANS8yLzA4IkFwWQAABBF0RVh0WE1MOmNvbS5hZG9iZS54bXAAPD94cGFja2V0IGJlZ2luPSIgICAiIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNC4xLWMwMzQgNDYuMjcyOTc2LCBTYXQgSmFuIDI3IDIwMDcgMjI6MTE6NDEgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhhcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+CiAgICAgICAgIDx4YXA6Q3JlYXRvclRvb2w+QWRvYmUgRmlyZXdvcmtzIENTMzwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOkNyZWF0ZURhdGU+MjAwOC0wNS0wMlQyMzozMzoyNlo8L3hhcDpDcmVhdGVEYXRlPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAwOC0wNS0wM1QwMjo0Njo0OVo8L3hhcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdVeAFgAAAHRJREFUaIHt0tkJwEAMxUAluKb0X1ZONlUYw0NTgT60rbUWUqB9OkDq4tyK5dyK5dyK5dyK5dyK5dyK5dyK5dyKtQHHdITUoYBrOkLqUMA5HSF1KOCejpA6OLdiObdiFfBMR0gdCninI6QOBXzTEVIH51asH3bRDTsyVtklAAAAAElFTkSuQmCC') 0 0 repeat-x;height:20px;margin-left:20px;margin-right:20px;}

#rokbox-container{}

#rokbox-container.spinner{background:url('data:image/gif;base64,R0lGODlhIAAgAPMAAP///zMzM9HR0ZycnMTExK6url5eXnd3d9/f3+np6cnJyUpKSjY2NgAAAAAAAAAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1ZBApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYtyWTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/nmOM82XiHRLYKhKP1oZmADdEAAAh+QQACgABACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDUolIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXiloUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx61WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQACgACACwAAAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZKYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCEWBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKUMIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJpQg484enXIdQFSS1u6UhksENEQAAIfkEAAoAAwAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFhlQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWMPaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgojwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkEAAoABAAsAAAAACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQkWyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8ccwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIGwAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhkPJMgTwKCdFjyPHEnKxFCDhEAACH5BAAKAAUALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBShpkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuHjYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOUqjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQCdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5BAAKAAYALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQULXAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3xEgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQACgAHACwAAAAAIAAgAAAE7xDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTESJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMDOR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIAACH5BAAKAAgALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwIDaH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLrROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAAKAAkALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOUjY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgGBqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY0KtEBAAh+QQACgAKACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCXaiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgevr0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfLzOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkEAAoACwAsAAAAACAAIAAABPAQyElpUqnqzaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLKF0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBuzsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaLCwg1RAAAOwAAAAAAAAAAAA==') 50% 50% no-repeat;}

#rokbox-container.warning{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAABkCAYAAABNcPQyAAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH1wUeEC4HMowWFAAAHbJJREFUeJztnXmUHdV95z+31rf2W3pTa19ACIQ2BLKDWAVeEGYzgcEQGAIEG2zAxhgMcmZIJiaxTZwTxiQnk5jEzvh4ogQh0CA4wQbbiecMXhIGh8GcxGTYJGtptfR6f+9V3fnjVtWrt1e3etXx75zXXbde1b2/e7+/7d761X3wK/oVnSgkH0abbR5mmsRsMzDdJF8iNjjK3cCNwGrARfKq0Hgi9TJ/Lh7GnWUWp5VOaICHnqbXNfl7YH2j70tlfvDqO1x64ScZmmHWZoxOWJMld6K7FrtoAi6AaXDe0jw7ga6Z42xm6YQFuJDiDiRn+2XdzhPvOZtY1xY0I6VOSuju4JKdD3MdsGCWWJ1W0mebgemgoWdZgGAXEAMQepxE3wfQrRy6lcOIL6Q09CZICcDSXjY8+zLPHzrKMaA4i6xPOZ2QGiwFfwhk/HIsvxGhmSB0EBqamcLOnBZcbxks+u8PchuwiBMsLjnhNHjwf3KhFHwFDyg91oud3wDChO5rIXk6jPxfdDtPefgdpKMUNpdindB58R9fZQAYnsUuTCmdUBosf4IpBX+Cr4VCI9a5WX2Z2gRml/qkz0QisPObgnuFwLrzch4CegBrxpmfJjqhAB48xGcRrPHLVscaNDMNRg7SZ1UuTJ0JRgY93ouRWhKczsR5395H2A4snEm+p5NOGIAHnmWZkPy2X9aMJFbW87OZc5T/DVPHVkBi59Yp/6ziLbacyn1nr6MP6JgZzqeXThiAdY0/lpDwy3b+DITQIX4yxFZULpQOSBcRWwqxVQg9jpk5NfjaMuj+83u4ExVwzfvxOSGCrMKzfAT4Hb9sxBdh59aCsCD/EdBs9YUHLrggHYTVCSM/R7c7KI/sRzrjAHQkWJvv4B9e+Cn9ML9Xuea9hMqdxIHH/LJAx+70gqf0mWCkvQurwQUXtCQiuQkQ2J0blJmWIAT6b1zEDsuiB7BntkdTS/Me4EKKHQiUDZZgZdeiGUkwuyHlRdCNwJXeJ7kG9ByancdILw3qTcXZ8NwXuQplquctzWsTXXiOUwT8NWAgQbM6iHe/D4QG+Q+BkWkNLqosjDRy9E10O0d56G3vHliQY+PP3mTvv+7jGDA2m32dLM1rDZYuX0Ni+xFwLL9ZgZs4BezF7cH1z5k9iPgKhGZhZdcEEbWukf3qndyNmjbNS2WYl0wDDD7HdUju98tmahlW5hTQ4iqwEloTcJ0G51yEmUWOvolmpnBHDyEdpbDpOKcu6uTlvT/mEDA4G309HpqXGty/lw7p8qhfFpqJnd+oCun3qah5AuCCA8JGJNeCAKvzdIIlaYm4+jx2ZJN0AfEZ7egU0LwE2FJToiD4sXLrEHoMrAWQPG3i4HrXi/hyMPNoVgdmellgqpM2p+z5Pa5nHgZc885EDz3LRgl/gSecmpUj3nUWaDrkPghaksmAq76T6v7xt9FiWcqD73nnoTvNxn2H2fvqv1MARmej75OheaXBUiJceBww1BlBrGszCAHxU8HsoRpIZwLgeosfRlatcgkDM+8ta0vQdZIP38R9qMQAY6b7PlmaVwAPPsst4GVpSDDTK9HtTqV16S3Ug9sqgq4Ht2KqTwIsjGQfWiwftN+d4ZJv3sfZQN+Mdvw4aN4AXNhFJxpfAtRqk25j57x0q44toFmTA1c2ut5AS6wGJFb+VBWRe/Ths3hwZR+9QHIGuz9pmjcAY/H7SDr9wMfOb0DoFtgLIb568uA2POci7AUIM4dmJjE7lqlGJcRMVvzN57mJeZL9MS+CrGN7eb+Ax5F+lkYXsa4z1CPA7AdD06KpATf4Tksix99Di3XgDv0S6ZYB6EyzoTDG3//4DY4BI7MwJJFpzmuw3ImOy5/64AaBFUBiLRjZaOAiwOpRGR1S1l9fC650EUYCYS9CCB0zf0rAkxDE7r+azwO9gDlTYzEZmvMAF+J8SsBGv2xlVqNZWdA7VBpOFHCNHHRdAbkPQP4S6P4o6Glaget/p8WWgLDQE53o8a5gbpxPcf7f7WAbczzgmtMADz9PnxD8rl8WRhwrd7oqpM9EeZh2mqtB5ly1hOmTkYHsttB1Lcy4AC22FAmY+ZNVEoFHF6zjgbUrWACkp3ckJk9zGmC3zB8SSp1R6a8G2EvAXk4kn5s8HfQGAa/Vo1a9asH1585hU23lEWYGYcYwsksrVRgs/Na93I56GDEnA645G2QN7mWblHzFLxvxBeqhvDAgczFoRntwjQ7InE3TsbcWwtDPQI7XrHjVarOLEHFk6RCalcIZOQxOCYBsinW6xkv/8NrcTLedkxos92JLl8dVAUDD7jpDfZlYB0YyQrTseJmULbqo2ZA9ry24SAehW2hmDwgNK39SUIUQmJ/YPnfTbeckwIMunwXW+AGNlfXSX/UMJNdGAze2Qj18aEfJtUqTW4DrH2t2F0Kz0WIZjGS3ul9CJsGWZx/mUubgw4g5Z6IHnmO5LvkfSDX90Mwk8d6z1WpS5hwVPbcDVxiQu1j9B0ZGRvjhD3/I66+/zs9//nMAurpCLxTaC2Hwp0C5KbgqYJOAhiwfQ4ulcQZ/if9+08I8G//xNfa8c5hBYHwmxioKzTkN1l0ekzKU/uovaMSWhTStWXaGB3JqE2gxBgYGuPnmm+nq6uLCCy9k+/btXH755axZs4ZVq1bxjW98QzVidqvlzlbgeuc0M4Uw0qBb6KGAyzTo+ou7uAsVcM2ZcZ1TGlzYw2VIHvbLRnJxJf01c4EXWLVIvZEuGHnIbEVK2L59O08//TTlcrmurYGBAXbv3k2pVOKiiy5SkfnwP4Mz2hTcIODSLNzSMTQ7gTMyoAIuCekEa/MpfvDCKxxhjqTbzhlJ27eHBCKU/ioM7C4v/TVxOujx9uBKCZlfAwRPPvkk3/ve99q2+8gjj/Diiy+qhxX57W3BBQchdDQjCwjMzhXB4ocQaDdu4wtJkx68V1dnm+YMwCn4ApLlftnKnYZmJJRGJtZEANeFxGplboHnn38+cts7d+5UB8nTVR0twPXb0q0MQjPRrBR6qieoK2mzfs9/5qPMkYBrTgBc2MMa4F6/rBkdWDlv7Te1CQj7xibgalbVC2ajo9GTLvbv318pdF1ZSdhrAm6wjGlkATDyS9QCjKfJm1Zy9+VnsQjITXw0ppbmBMCoLA31BoGEWM9mQAN7paeRbcDFUQ/8tco01Lajv5AwPh4Kes0ubxmzNbjgoukmQo8jNAMjvySoQtfJfuVWPoNap57VOGfWAT62h+uBbQAqS2MZerwHRAyS64kErtmrTGuIMpkMUSmdrllKzn8AzM6W4PrLmbqRUu+6JLvQ7FRQRV+WKx7/OGcyy3t/zCrA/XvpEHjpr9JLf+3yHhwlTwPdCpnKJuACZM+pq/u4ABYm9FxHY3DDa9UuQoCmJZCA0bkUEL6pFr++lYe6s3Qzi+m2swqw5fBfkPQFWRqdXvqr0QnxVTV+sMXDBCNfV/dxAQxqhSt1RgNw67VZN2yEpiOsOHq6O6gibnHKUw/ObrrtrAE89CwbpeSTflm3c5gdJ6kAJ7VRTXnagasnvMeG9XTcAAP0fCyU69XMVCt+dM0GCXq2D3QvB0DC2sXcecvFLAc6IzM0hTQrAMuH0VyHP8UPQITA7jlTpb/aK9UD+ijprh1nK3PagLLZbGR+mgJs5lWiQBtwkS6apqNpBkLTMPOLgoha10g+9Ouzl247KwAPbuZW4P1+WaW/5tVD+cSp0cC1Fysz3oQmosEdHS12a8hfAtbiluD63+m6iRACLZFFxCoBV0+GD/313WxlFrI/Zhzgwh66kPy+Xxa6jd3ppb8mTvMeELQBFyBzXst2pgxgoUPfrYBsCS7SQQgXzRtSs2sxofeb+PBmHpqNdNuZ12DJH+D7Iwl2p5f+anaDvZRIGZGpzSrtpgVNiQ/2KXmaem7cENzqBx26rqFpAmFY6JnuwFTbJsv+5rPczAyn284owIXdnA3cAqiAJNaF2bEC0CHhz3nbgKt3QPqMtm1NKcAAC37Ty+uqBbdemzVACtAy3Qizsvhych+3feoSVjGDm5/OGMDyJQw0b5MyCSCI9XgRcGwlGKn24EoXmTkveM7biqYcYCOD7L2pLbgg0XQNHYEQAj3nuV2p1uY+dwUPMoPptjMGcGGQTwEbgiyN3Go0O6PeK0qcEglc116BtJdGas+2bWKxaA90WvrgELm5S5Cxk2gFrk+6riMQaIkUWrwiQLk05/7d57iIGdpsbUYAHtnDIgG/EzxWM+JYeS/9Ne5vTNcaXISG23EuUsoGLTSmqFOlKBqs2hWUF/hT9+bgAkp7NU25ovwChFYZ6gtP54H1y2cm3XZGAC65PIoMpb92b1JPX6xesPuI8opJOXEWrlCJHlFBnkqA/XZlfDXlzCUtwfVJNwwv4DLQMp7blWDq9H3zLm5nBgKuaQd4aDcXC7jOLxvJBWp/SKFDPJSX3AJcV8tSip2uBtj7RKFcrv3TOiEEqVSq7XV+m1JKyj23IjUvN6wJuD7pmnK1ejqHMCoB14pebvrC1axBZWNOG00rwHIvtiu89FcAoWN3exGwvdLL0mgNLtJlPHEOUipBn4iJzufr16hrKar/DQuXKxKMd3+cduACKuDSDKQQ6J29wXkBxsc/yEOWRS/TmG47rQAPFbkP9UsnAFg5P/01GXqYUBuRVoNbNE+mrKsnbuFBjgJ0FA2OAnBtm1JKSh0XU441/TmIKjIMCyFAxOJoyTT+jnqZBGc9dd/0pttOG8BHn2GFFOwIGjJS2Hm16acsHkIW/gncIo0jUm9KJExGbZWlUTvAUwVw9ACrlgcY7b6n7ZStNFbkyFsHKI2opAI9140IvVD+a6dw74VrWUhoh/qppGkDWJM8BsR9abV7vPRX18Ed3od75Ac4730TOfQGQT5yjakeMTcjRbwhuDMFcCPt9Y/L5lJG01c1vM8plTm2r5/+N/dTHB5j5NAw0pWga4h85cGSIej6k9v4JNOUbjstABee4QrgI76LMlKLMZJqwu8WD4Esq6Xd8iDOoRdw3tuJO/iGd16BWyLPqL66KbhRQI4SRU8E4EZtj2Svx9ErC1NOsUxh3xH6/20/owNDnjmWOOMO40dVnpie7gDLDlz4wjz/4cs3sR61ADKlNOUA79tDQkgeC+a8mkGsR6W/SmcEOX5UHSODDrrFAZzD36f83t/iFF4DZ4xBU6W/thrgdgAfrwY3a68q4CLGYPZ2SiPjHHuvn8O/2M/o0SFc1/Ur8V5+kIwdGcEtKeE28p3BBEkItBvPYcd0pNtOOcApyW9LSbDcZOXXIgxv/jp6QP33zLY69oGWuKUhnP4fUSgUKYnOttorpawMZAPq7W2vEBMFuJGpHoudzZHh1YwVhhWglc55R6qPrgsjB0cAibBtRKrSdtJm3bM7uJopDrimFODBXZyKenFMVW5lsHIqiJbFI0hn3AO3Mkg+uL6Uu3qOkfxNkcFtpck9Pe2nmM0AjmI5wsfOSfeDsKoskwz++MeS4nCJ4lARiUTP5tQGbt41G5Zx91VnsZgpTLedMoClREiNxwktosd6vd1f3TLu2OHAH/k31IKLhMH8bTgi0RbcKKZ60aL2ytAI4KhtVn3ii3GW3lyZGof6R41QDx8cUaGGJtByleBZ18h8+QY+zRSm204ZwIVnuB640C+b6eXocZWA5o4eBNdtYLoqeCNh3F7PaPKCyJrTzlR3d3ej663HqdFTp4kIVbhcWvwfceNLqvwusnqaBeCWXEb7VcClpVIIP4dbQm+WK/7sds5iitJtpwTgIzvJCBna/VWY2D0bAJClYWS50MBchU9IJDpHc3cowW8DZFRN1nWdBQtaj1NtIObX366dhqZasyituq8huIQEGSkZOzKOUywDEr0zF14UE1eeyUPdWXoI/cjIZGlKADZNfg9f4iTY3etV+quUuGMH2vpdgELqCkrGogmb5jAY/v8wbdy4kVa0ZcuW4LgW1Kgghz9O9v2Uu7YFltkfExky1RKQrsvQ/lF1zjTQOirr4XGL1U/fG6TbHtfDiOMG+OgezpCCOwBUlkYeM6OS4eR4PzjFtn63pHVzLHXtcYEbLoeBvu6662hGW7duZdOmTXX1NDuOaqrHV3waqccrQk2o3/44AKWREuPHioBEZDvAdycSTlvMnbdcwDKg/YJ6CzouRy4fRiv1sguJejFHCOKLzkEzE+AWcUf2IwnNBz3uAyWTqq+HOj5FyVyGEGLCH9Vs9f8wrVu3joMHD/KTn/yk6vySJUvYvXs3uVzuuLW2Tii0OK7UMY79KGRRQv2mIvPlUYdYzkQIQNeQw2P+UJqbV7Lgsef5ITBAkG04MTougO/7NLcj+YRfNrMnYWWV9roj+9W2+A2mDOqEKgxbmxhIXafSTTVtUgADdcf+wAohuPTSS1mwYAEDAwOsXbuWG2+8kSeeeIK+vr7IlmGiQDvJUzH6v48oDeAv1/r9Dlsv13GRjsRMGQjLgPEilNUrOUmLVWuX8LNdP2I/UJgMRpO274M76ZYmr+NlSArDJrniUoRmIkuDOMPvNfe73nlHmryT/yplvRdN09B1HU3TIn/CQtFMOKCxZgd8Ue8Sak102Me3+ziOExwbhf9D8l8+CdINTHMY3LBfzq5MYsQ0ZNHB3X8YXMXbeIm337eDq988zOtMYpumSftgafAlQq9j2N3eb/RKF2fkYNugSkrJ0eRHKeuV1aap8r2NgGkVmNV+JuuLwwIDUO7YQLHnw1V+tyr6kpXi0D7PNJsaIl0Jnm2TpX/7GX6TSQZckzLRhV2cg+CP/Qb1eHew3uyOHUKWve0pGvkfz++WjEUcTN9V9aORE/W77TRUsdBYS1t9dzwm2r/XJye9DuuXzyDcome9QnyFtNgtuWiGwIzriJgBw+OBFueTrB8p8sLL/8ZRJri77YQ1WL6EgXphu/Ibvb1q91fpjOOOH6maDviAer0KtPhQ8lZk6L2i4/F3zcDwzxeLRb71rW/xsY99jK1bt3LZZZfx2GOPMTg4WKe1rTR3Iloc9MvMMb7sjjrr5X1bFZwM/XIMt+yCAJFLBpcIgX3f9sntbjthlS/s4l7UT6iDBCu/JljUcIbeRpaGK51o4HellAzZ53Cg4556ZjxfOhW+1//s27ePa665hldeeaWuvd7eXp566inWr18fSYCi+uJakJEuiVduRR98vW4+TKDJ6jiWNelYZAMS92ABOVIMqnnxX7j3yj9iF/BWVLwmpMEje1iE8LY5kiCMBFbXWlUsHmsObkhyXZHgcPLGhvVP1vc2u3Z4eJjLLrusIbgABw4cYPv27ezfvz+yiY46L64ioTF20gNIoVX53epjdd/oQJHicFkJQj5ZcT8Szl3D585YTh8TSLedEMClMl8F0r4fifV66a/SxRk54DHSeDHD7/eRxLU4evO5e1RwmwVU4fLXv/71YGe7ZnT06FEeffTRpoHWRE11M3LTayj1XVUXPYfB9RVjcJ966iZ0AZlY4LdNnb6/+sTE0m0jA3z0KT4o4Fq/MSPZh5FerJgfPYhKlpMNHyL4QI/ryykkLmnZzkTAbRdJP/PMM5H69p3vfCeyxZiw9oaouPwOXCtXUYDQQFWsHpRGHUYPK9MsOmwwK4Hosk5ufHgC6baRAJZ7sTX4WoCepmMvUOmvsjyKWxxQx9V/alAWHErfhozQ5EQGtJm2ua7LwYMHo3SPd955p6EFaOaDm7Xbtl9GiuKqe6iaKoWGy+87wNCBIk5JBVxavpLkIcD4rQvY4aXbtt1KKBLAQ6Pcj+Rkv2znT0UzUyq8HzngaWlzvwtQiG9j3DylvvIGFAXQZoCEz69YsaJ9Y6iNSRtZgIkKVxQq934YN7u5JsCSlTH0zrtlyeB7Y+qamIFImoEgpGOc+cxn+AgR3m9qC/DRJ1kpBQ8FN1hpLO8XwdziANIZbQxuiGFHpOhP3hBpAHyKYiZbgeG6Ltdee22ktrZt29bSErRyCxMBV5FgfPUD+EMf+OM6qwdjRx3GB9UutyJng1Zxu1tWBOm2LTML2wKsafxXQolgdu8mL82k7Pne5osZ/vGgfQ6uNrH3rKL63lYrWJdffjnnnntuy3ZyuRz33HNPS0vQTpAmBjC4yRU4+S31ilGFsfLRx9710px0gchawZgaGp2P38RdqOyPpji2BLjwJFch2e6XjdSSIP3VGTkIrtNyMcPXYldM7iW6KAPbCmghBE888QTnn39+w/oXL17Mt7/9bZYsWRKpnWZaPBmSRqYa0AYWEClxxiVDB4qqnbSJMCuQLcpyzVeuZwMt0m2bhtoHd5KKm7wmvQxJIQySqy5BGAlkeQRn8K0GElgdEfrmp6gv493OL1ctS0alRosatYsb4bJ/T/hYSslzzz3Hd7/7Xfr7+8lms2zZsoUrr7ySeDwe8BzV59YK04SpPET8f10B40er/G4j94aUIKB7jY1uCyg6yH2VX5sfHuO1kz7LDcMl3qDBz9A3BbjwJF9CcL8vZXbvBs/3SpzCv+OWxxpKnawB2T8ejm2lP31zyzlwMwqvcDUCdTLPiQMea/5HDe4mY5oBxMhbWK9/Ee3IT2muGNQFYHaHIL/KVoLQX4ShcjC+//wWXzz/Ef4S+EVde42YGNzNadLlFfxt9e0MyZUfAgTOaD9y9EDFT1SBGz4OSWaIYUdLNDHp4eNmwiIVyzXABG3U1NFo8Gr9HcH5mvrCfIRmBNKVDepuBFLFzPr9F0hkabABHw14blBHfoVJLKupBMZ3i8HDCMelcMsTXP7Uj3kVlRzQHGApEYNP8RKSwHEllm1DT3SDW6J87BdI6daDGAHc+ulAxDoanm9UXwNgaga5nTC1FLKIPLdzXQ2vacGHX4duCXpOsxGahKEy8nBlJ/sDR3nm5Ad4EHiDYBPPBkFWYTc3hME1s8sVuIAzciAAN8SpP2JV0l4PrndL6HJqr21WR+Wr0PFEwQ2zOnFhCv630fLa8QgodCxD17TmI1QdUB6XFPaV1MmkDrYIxrQ3w2X/7Wa2UJNuWwXwwFNkhRtKf9Us7G6VlegWB3GLhXqpCzEsQ52qwi/EaXjeJ2sGrAqYqjpk0zoaDWpYmGR1Re0FktB9wXGtsDQTpkZtV18TyTKFBDE8NiAZPlCmPCZVfZ162AaLKzazY2GGXkLptlUAay5fJBRy2z3rEYb66VbprVg1l7rKGIYZCkt9c9MVvpc219ZKf81YhArNhKnOT1Jht5Uwhc9XSUEAaL0g1NZRxWxNNQ0Frk5A4Ojbnmk2BKQrEMYNTnrybm4g9DAi+PbITpYKuN0v63YeM7sSAHe0H9ctVg+qrOlrrd+t0ZBwxFkPTM01rQSkru36uWNtHe2EqW17TYQpfE0zQaj2u1QfRxSmsCACjA+6jB5RK1xkhMrL8b5f08dvnbGcTrx02wBgQ+c38HdDlWD3qd1fXaeIM95f1UB0v+sPWHhAWwtC/fnwvdQLk2xVRzRhamSRoghTO4EM971ZUNVUIBvwH6bCu47aWkwDchU7rQvSv/tRzsfb3bay/4Bgtc+QMOPo8Zx3WkOL94QYcgPeVet+3nMYUJ9ptwKW99+fZoS5l4GNCw9UWJorgZLapCV8b6iNugEL7gz4CcTADY9czZzWlSGeKnVX8RnwL0Pf+2MgK7wGQyaRPu/hManlM+DDbSgIlb8SoXttx0WoPuhNsQSl1/kKwJLDwTXlMWR5XG2pr2lqR7q6DoU1LXS+5rqwmau+rsmgVZXDpqr5dXVg1Fwrq9qmjg8p69ut5VnWtl3TZ1kDdsN+1N5X0xdZxaPWuB9B/aJyXKqACzBS5KhXcTIAWEpeEHjv9krJyP97ETN/MpqVDC6oZqq2E1RfV/W3tuNUgJG191XXXyUgNfVXJLsGlNB1FQFpUkft4MtGdTTqc6hcy1NdnxtcVztWtYJL9X0yXH+4z0WgEG4H+fQ/8b+948rzJykRg7u8BY5w/eF+NaKo187WubnGzzTzeKDA0yd/nv/kFY8EQZYQSOFwDZKXI1c6lwYpKo9zke8p4vHIMN+/8ms8Evr2SP1S5Z9hHkxxW9nhlpjJck002IVtjkjrjJ5rRo1eCZtBHh3J6Mg4//rquzx91eO8UCoFVx8C9rfKzLOBJUzBS8i/ohklBzgAHIZoqZdx7zOnfor2V9SQisAgIbvy/wEVKncMhTdI4AAAAABJRU5ErkJggg==') 50% 50% no-repeat;}
#rokbox-container.warning h1 {position: absolute;text-align: center;margin-left: -20px;}

#rokbox-arrows{margin-top:15px;position:absolute;right:0;width:70px;height:20px;}
#rokbox-arrows a{float:left;display:block;width:20px;height:20px;margin-right:5px;}

#rokbox-arrows #rokbox-previous{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAoBAMAAAAMH8foAAAAA3NCSVQICAjb4U/gAAAAJFBMVEV5eXny8vLV1dWBgYGrq6ve3t6ZmZn///+8vLzm5uaKior39/dmYlX3AAAACXBIWXMAAAsSAAALEgHS3X78AAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1MzmNZGAwAAABR0RVh0Q3JlYXRpb24gVGltZQA1LzUvMDi/lkjgAAAAvklEQVQYlWXPsQrCQAwG4B+ECn0BcQ2UDi6uPoA4OFkEBxcXBzcVS6x9AGcRrODi5i5I6duZtCi/GLi7jyO55KDfgK3pPRw2vMBi5cz6zlZhPLtCDIwLU3BFoEj9Lt4DM+ws76jGEQ7AWJ0PrLHUmm0mJVAZPUYtuDGNQ0Py6PwhYjeRU8OXWMydudRRGp8Ne8bEzqiSSLHxq05mC7ZJobYXSF3OGFuZ6D8pgcryz2Mlt+DGNA4NqVolcvv90BtZ+nYxJaOW3QAAAABJRU5ErkJggg==') 0 0 no-repeat}
#rokbox-arrows #rokbox-previous span{display:none;}


#rokbox-arrows #rokbox-next{background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAoCAMAAADJ7yrpAAAAA3NCSVQICAjb4U/gAAAAM1BMVEV5eXnv7+/V1dWBgYGZmZne3t6rq6v3//+8vLzm5uaKior39/fMzMz///qjo6PFxcX///+DH5/CAAAACXBIWXMAAAsSAAALEgHS3X78AAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1MzmNZGAwAAABR0RVh0Q3JlYXRpb24gVGltZQA1LzUvMDi/lkjgAAAAtElEQVQokX2Q2xLDIAhEiQmmSKf1/7+2gJfazDb7EPEYZF06z7O6xuqituFciErmDv0jB3UdMqDsNLVLg8H2lRqMXn4OfDjkKLnqo1M2mHtV6/aKMhssE5oz3xSDtMBa33b1Pwjb4SBoCZrHz4SB4OhwyFfdQRVOKYkuUJ2EWAfUtEgb/GFBDUavbvMGhzrOZf5KvY4BbZ54dF/YbRhMC7SgbyBsh4OgJWgePxMGgqPDIV/1AVFTGQr7vtJxAAAAAElFTkSuQmCC') 0 0 no-repeat}
#rokbox-arrows #rokbox-next span{display:none;}

#rokbox-arrows #rokbox-previous.inactive,
#rokbox-arrows #rokbox-next.inactive{background-position:0 -20px;}

#rokbox-caption{padding-top:25px;font-family:Helvetica,Arial,sans-serif;}
#rokbox-caption h2{
margin:0 0 10px 0;padding:0;color:#333;font-size:18px;font-weight:bold;}
#rokbox-caption p{
margin:0;color:#999;font-size:12px;} 








.ux-menu-init-hidden
{visibility:hidden;position:absolute;}

.ux-menu-container
{position:relative;}

.ux-menu,.ux-menu ul
{list-style-image:none;list-style-position:outside;list-style-type:none;margin:0;padding:0;line-height:1;}

.ux-menu li
{position:relative;float:left;}

.ux-menu ul
{position:absolute;top:0;left:0;}

.ext-border-box .ux-menu-ie-iframe,.ext-ie7 .ux-menu-ie-iframe
{padding:0;margin:0;position:absolute;top:0;left:0;display:none;z-index:-1;}

.ux-menu-vertical .ux-menu-item-main
{clear:left;
}

.ux-menu a
{display:block;position:relative;text-decoration:none;border-left:0px solid #e3dad1;border-top:0px solid #e3dad1;border-bottom:0px solid #e3dad1;background:#url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAiCAIAAAARTyRGAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAADFJREFUeNpi+PXjG9Pv3z+ZGBgYmBj+/4fQWPC////g7P///qHKofGxY4jZ/7HbARgANwAVBDPUNQoAAAAASUVORK5CYII=') repeat;font-size:12px;line-height:10px   ;padding:10px 14px;background:#ECEDEF;color:#838586;}

.ux-menu a.ux-menu-link-last
{border-right:0px solid #e3dad1;}

ux.menu ul a
{width:100%;color:#838586;}

.ux-menu a:focus,.ux-menu a:hover,.ux-menu a.ux-menu-link-hover
{ border-color:#d2e3f4;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAiCAIAAAARTyRGAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAADNJREFUeNpieP/5K9O37/+YGBgYmP7//8/EwMCAFf9jQMj9RVP37z8DTn0wDDMbhx2AAQBFwhXxxRHeigAAAABJRU5ErkJggg==') repeat;outline:0;background:#EC0623; color:#fff; text-decoration:none;}

.ux-menu-clearfix:after
{content:".";display:block;height:0;clear:both;visibility:hidden;}

.ux-menu a.ux-menu-link-parent
{padding-right:24px;min-width:1px;}

.ux-menu ul a,.ux-menu.ux-menu-vertical a
{border-bottom:0;border-right:0px solid #e3dad1;color:#838586;}

.ux-menu ul a.ux-menu-link-last,.ux-menu.ux-menu-vertical a.ux-menu-link-last
{border-bottom:0px solid #e3dad1;}

.ux-menu-arrow
{display:block;width:8px;height:6px;right:10px;top:14px;position:absolute;background:url('/modules/mod_ariextmenu/mod_ariextmenu/js/css/images/menu-arrow-down.png') no-repeat;font-size:0;}

.ux-menu.ux-menu-vertical .ux-menu-arrow,.ux-menu ul .ux-menu-arrow
{width:6px;height:8px;top:13px;background:url('/modules/mod_ariextmenu/mod_ariextmenu/js/css/images/menu-arrow-right.png') no-repeat;}

.ext-border-box .ux-menu-arrow
{top:17px;}

.ext-border-box .ux-menu.ux-menu-vertical .ux-menu-arrow,.ext-border-box .ux-menu ul .ux-menu-arrow
{top:16px;}

.ux-menu ul.ux-menu-hidden
{display:none;}

.ux-menu a.current
{background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAiCAMAAACp80MjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAwBQTFRF6uXj6uPX6NzP6N3V6NnS59vO6eLW6dzQDw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGRkZGhoaGxsbHBwcHR0dHh4eHh4eHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OTk5OT09PUFBQUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4eXl5enp6e3t7fHx8fX19fn5+fn5+f39/gICAgICAgYGBgoKCgoKCg4ODhISEhYWFhoaGh4eHiIiIiYmJioqKi4uLjIyMjY2Njo6Oj4+PkJCQkZGRkpKSk5OTlJSUlZWVlpaWl5eXmJiYmZmZmpqam5ubnJycnZ2dnp6en5+foKCgoaGhoqKioqKio6OjpKSkpaWlpqamp6enqKioqampqqqqq6urrKysra2tra2trq6ur6+vsLCwsbGxsrKys7OztLS0tLS0tbW1tra2t7e3uLi4ubm5urq6u7u7vLy8vb29vr6+v7+/wMDAwcHBwcHBwsLCw8PDxMTExMTExcXFxsbGx8fHyMjIycnJysrKy8vLzMzMzc3Nzs7Oz8/P0NDQ0dHR0tLS09PT1NTU1dXV1tbW19fX2NjY2dnZ2tra29vb3Nzc3Nzc3d3d3t7e39/f4ODg4eHh4uLi4+Pj5OTk5eXl5ubm5ubm5ubm5+fn6Ojo6enp6urq6+vr7Ozs7e3t7u7u7+/v8PDw8fHx8vLy8/PzMsPemQAAACRJREFUeNpiYGBgZGBkYEOBzAzMDMwMLFDIjgGZGJgYWBEQMAAU0gCnP+W/oQAAAABJRU5ErkJggg==');border-color:#cbc0b7;color:#fff;background:#FE0000;}

ux.menu ul a:visited
{width:100%;color:#838586;}
 
#pillmenu .ux-menu LI A {
	float: none;
} 