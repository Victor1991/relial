














body {
  min-width: 960px;
}




.container_12,
.container_16 {
  margin-left: auto;
  margin-right: auto;
  width: 960px;
}




.grid_1,
.grid_2,
.grid_3,
.grid_4,
.grid_5,
.grid_6,
.grid_7,
.grid_8,
.grid_9,
.grid_10,
.grid_11,
.grid_12,
.grid_13,
.grid_14,
.grid_15,
.grid_16 {
  display: inline;
  float: left;
  margin-left: 10px;
  margin-right: 10px;
}

.push_1, .pull_1,
.push_2, .pull_2,
.push_3, .pull_3,
.push_4, .pull_4,
.push_5, .pull_5,
.push_6, .pull_6,
.push_7, .pull_7,
.push_8, .pull_8,
.push_9, .pull_9,
.push_10, .pull_10,
.push_11, .pull_11,
.push_12, .pull_12,
.push_13, .pull_13,
.push_14, .pull_14,
.push_15, .pull_15 {
  position: relative;
}

.container_12 .grid_3,
.container_16 .grid_4 {
  width: 220px;
}

.container_12 .grid_6,
.container_16 .grid_8 {
  width: 460px;
}

.container_12 .grid_9,
.container_16 .grid_12 {
  width: 700px;
}

.container_12 .grid_12,
.container_16 .grid_16 {
  width: 940px;
}




.alpha {
  margin-left: 0;
}

.omega {
  margin-right: 0;
}




.container_12 .grid_1 {
  width: 60px;
}

.container_12 .grid_2 {
  width: 140px;
}

.container_12 .grid_4 {
  width: 300px;
}

.container_12 .grid_5 {
  width: 380px;
}

.container_12 .grid_7 {
  width: 540px;
}

.container_12 .grid_8 {
  width: 620px;
}

.container_12 .grid_10 {
  width: 780px;
}

.container_12 .grid_11 {
  width: 860px;
}




.container_16 .grid_1 {
  width: 40px;
}

.container_16 .grid_2 {
  width: 100px;
}

.container_16 .grid_3 {
  width: 160px;
}

.container_16 .grid_5 {
  width: 280px;
}

.container_16 .grid_6 {
  width: 340px;
}

.container_16 .grid_7 {
  width: 400px;
}

.container_16 .grid_9 {
  width: 520px;
}

.container_16 .grid_10 {
  width: 580px;
}

.container_16 .grid_11 {
  width: 640px;
}

.container_16 .grid_13 {
  width: 760px;
}

.container_16 .grid_14 {
  width: 820px;
}

.container_16 .grid_15 {
  width: 880px;
}




.container_12 .prefix_3,
.container_16 .prefix_4 {
  padding-left: 240px;
}

.container_12 .prefix_6,
.container_16 .prefix_8 {
  padding-left: 480px;
}

.container_12 .prefix_9,
.container_16 .prefix_12 {
  padding-left: 720px;
}




.container_12 .prefix_1 {
  padding-left: 80px;
}

.container_12 .prefix_2 {
  padding-left: 160px;
}

.container_12 .prefix_4 {
  padding-left: 320px;
}

.container_12 .prefix_5 {
  padding-left: 400px;
}

.container_12 .prefix_7 {
  padding-left: 560px;
}

.container_12 .prefix_8 {
  padding-left: 640px;
}

.container_12 .prefix_10 {
  padding-left: 800px;
}

.container_12 .prefix_11 {
  padding-left: 880px;
}




.container_16 .prefix_1 {
  padding-left: 60px;
}

.container_16 .prefix_2 {
  padding-left: 120px;
}

.container_16 .prefix_3 {
  padding-left: 180px;
}

.container_16 .prefix_5 {
  padding-left: 300px;
}

.container_16 .prefix_6 {
  padding-left: 360px;
}

.container_16 .prefix_7 {
  padding-left: 420px;
}

.container_16 .prefix_9 {
  padding-left: 540px;
}

.container_16 .prefix_10 {
  padding-left: 600px;
}

.container_16 .prefix_11 {
  padding-left: 660px;
}

.container_16 .prefix_13 {
  padding-left: 780px;
}

.container_16 .prefix_14 {
  padding-left: 840px;
}

.container_16 .prefix_15 {
  padding-left: 900px;
}




.container_12 .suffix_3,
.container_16 .suffix_4 {
  padding-right: 240px;
}

.container_12 .suffix_6,
.container_16 .suffix_8 {
  padding-right: 480px;
}

.container_12 .suffix_9,
.container_16 .suffix_12 {
  padding-right: 720px;
}




.container_12 .suffix_1 {
  padding-right: 80px;
}

.container_12 .suffix_2 {
  padding-right: 160px;
}

.container_12 .suffix_4 {
  padding-right: 320px;
}

.container_12 .suffix_5 {
  padding-right: 400px;
}

.container_12 .suffix_7 {
  padding-right: 560px;
}

.container_12 .suffix_8 {
  padding-right: 640px;
}

.container_12 .suffix_10 {
  padding-right: 800px;
}

.container_12 .suffix_11 {
  padding-right: 880px;
}




.container_16 .suffix_1 {
  padding-right: 60px;
}

.container_16 .suffix_2 {
  padding-right: 120px;
}

.container_16 .suffix_3 {
  padding-right: 180px;
}

.container_16 .suffix_5 {
  padding-right: 300px;
}

.container_16 .suffix_6 {
  padding-right: 360px;
}

.container_16 .suffix_7 {
  padding-right: 420px;
}

.container_16 .suffix_9 {
  padding-right: 540px;
}

.container_16 .suffix_10 {
  padding-right: 600px;
}

.container_16 .suffix_11 {
  padding-right: 660px;
}

.container_16 .suffix_13 {
  padding-right: 780px;
}

.container_16 .suffix_14 {
  padding-right: 840px;
}

.container_16 .suffix_15 {
  padding-right: 900px;
}




.container_12 .push_3,
.container_16 .push_4 {
  left: 240px;
}

.container_12 .push_6,
.container_16 .push_8 {
  left: 480px;
}

.container_12 .push_9,
.container_16 .push_12 {
  left: 720px;
}




.container_12 .push_1 {
  left: 80px;
}

.container_12 .push_2 {
  left: 160px;
}

.container_12 .push_4 {
  left: 320px;
}

.container_12 .push_5 {
  left: 400px;
}

.container_12 .push_7 {
  left: 560px;
}

.container_12 .push_8 {
  left: 640px;
}

.container_12 .push_10 {
  left: 800px;
}

.container_12 .push_11 {
  left: 880px;
}




.container_16 .push_1 {
  left: 60px;
}

.container_16 .push_2 {
  left: 120px;
}

.container_16 .push_3 {
  left: 180px;
}

.container_16 .push_5 {
  left: 300px;
}

.container_16 .push_6 {
  left: 360px;
}

.container_16 .push_7 {
  left: 420px;
}

.container_16 .push_9 {
  left: 540px;
}

.container_16 .push_10 {
  left: 600px;
}

.container_16 .push_11 {
  left: 660px;
}

.container_16 .push_13 {
  left: 780px;
}

.container_16 .push_14 {
  left: 840px;
}

.container_16 .push_15 {
  left: 900px;
}




.container_12 .pull_3,
.container_16 .pull_4 {
  left: -240px;
}

.container_12 .pull_6,
.container_16 .pull_8 {
  left: -480px;
}

.container_12 .pull_9,
.container_16 .pull_12 {
  left: -720px;
}




.container_12 .pull_1 {
  left: -80px;
}

.container_12 .pull_2 {
  left: -160px;
}

.container_12 .pull_4 {
  left: -320px;
}

.container_12 .pull_5 {
  left: -400px;
}

.container_12 .pull_7 {
  left: -560px;
}

.container_12 .pull_8 {
  left: -640px;
}

.container_12 .pull_10 {
  left: -800px;
}

.container_12 .pull_11 {
  left: -880px;
}




.container_16 .pull_1 {
  left: -60px;
}

.container_16 .pull_2 {
  left: -120px;
}

.container_16 .pull_3 {
  left: -180px;
}

.container_16 .pull_5 {
  left: -300px;
}

.container_16 .pull_6 {
  left: -360px;
}

.container_16 .pull_7 {
  left: -420px;
}

.container_16 .pull_9 {
  left: -540px;
}

.container_16 .pull_10 {
  left: -600px;
}

.container_16 .pull_11 {
  left: -660px;
}

.container_16 .pull_13 {
  left: -780px;
}

.container_16 .pull_14 {
  left: -840px;
}

.container_16 .pull_15 {
  left: -900px;
}






.clear {
  clear: both;
  display: block;
  overflow: hidden;
  visibility: hidden;
  width: 0;
  height: 0;
}



.clearfix:before,
.clearfix:after,
.container_12:before,
.container_12:after,
.container_16:before,
.container_16:after {
  content: '.';
  display: block;
  overflow: hidden;
  visibility: hidden;
  font-size: 0;
  line-height: 0;
  width: 0;
  height: 0;
}

.clearfix:after,
.container_12:after,
.container_16:after {
  clear: both;
}






.clearfix,
.container_12,
.container_16 {
  zoom: 1;
} 


a,
abbr,
acronym,
address,
applet,
article,
aside,
audio,
b,
big,
blockquote,
body,
canvas,
caption,
center,
cite,
code,
dd,
del,
details,
dfn,
dialog,
div,
dl,
dt,
em,
embed,
fieldset,
figcaption,
figure,
font,
footer,
form,
h1,
h2,
h3,
h4,
h5,
h6,
header,
hgroup,
hr,
html,
i,
iframe,
img,
ins,
kbd,
label,
legend,
li,
mark,
menu,
meter,
nav,
object,
ol,
output,
p,
pre,
progress,
q,
rp,
rt,
ruby,
s,
samp,
section,
small,
span,
strike,
strong,
sub,
summary,
sup,
table,
tbody,
td,
tfoot,
th,
thead,
time,
tr,
tt,
u,
ul,
var,
video,
xmp {
  border: 0;
  margin: 0;
  padding: 0;
  font-size: 100%;
}

html,
body {
  height: 100%;
}

article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
menu,
nav,
section {







  display: block;
}

b,
strong {





  font-weight: bold;
}

img {
  color: transparent;
  font-size: 0;
  vertical-align: middle;




  -ms-interpolation-mode: bicubic;
}

li {



  display: list-item;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}

th,
td,
caption {
  font-weight: normal;
  vertical-align: top;
  text-align: left;
}

q {
  quotes: none;
}

q:before,
q:after {
  content: '';
  content: none;
}

sub,
sup,
small {
  font-size: 75%;
}

sub,
sup {
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

svg {



  overflow: hidden;
} body {
  font-family:Calibri, Helvetica, sans-serif;
  font-size:14px;
  
}
  #iniciologos {
  border-top: 1px solid #A7A9AC;
  border-bottom: 1px solid #A7A9AC;
  

}  

#slider101 li img{ border-left: 1px solid #A7A9AC !important;}
#slider101 li img last{ border-left: 1px solid #A7A9AC !important;}
#slider101 { border-top: 1px solid #A7A9AC !important;}
#slider101 { border-bottom: 1px solid #A7A9AC !important;}
#prev101 {        left: -62px !important;       top: 16px !important;}#next107 { right: -49px !important;       top: 16px !important;}
#next101 { right: -49px !important;       top: 16px !important;}

  

.slide-title {
color:#333;
}
  
  
a:link {
  color: #333;
  text-decoration:underline;

}
a:visited {
  color: #333;
}
a:hover {
  color: #EC0623;
  text-decoration:underline;
  
}
a:active {
  color: #EC0623;
}

h1 {
   
  border-left:#EC0623 4px solid;
  font-size:20px;
  font-weight:normal;
  padding-left:10px;
  margin-bottom:20px;
  color:#414042;}

  
h2 { 
  font-size:16px;
  font-weight:bold;
  padding-bottom:5px;
  margin-bottom:10px;
  color:#414042;}

h4 {
  
  
  border-left:red 4px solid;
  font-size:15px;
  font-weight:normal;
  padding-left:10px;
  margin-top:20px;
  margin-bottom:20px;
  color:#000;}

.beige {
background:#EEECE1;
  height:155px;
}

#logo{
  
  }
  
#logo img{
  padding:10px 0px 10px 0px;
  text-align:center;
  }

#social{
  padding:0px 0px 0px 0px;
  text-align:right;
}
#social img{
padding-top:85px;
}

#search{
padding-top:85px;
}

#search .inputbox{
width:150px;
border:1px solid #ccc;
  padding:3px
}
#search .button{
width:30px;
border:0px;
  cursor:hand;
  cursor:pointer;
  background:#666;
  color:#fff;
  padding:2px;
    margin-left:5px;
}
  




#menu{
  background:#EBEBEC;
  height:30px;
  }



  
#menu a:link {
  color: #333;
  text-decoration:none;
}
#menu a:visited {
  color: #333;
  text-decoration:none;
}
#menu a:hover {
  color: #fff;
  text-decoration:none;
  
}
#menu a:active {
  color: #fff;
  text-decoration:none;
}

#slide{
  width:640px;
  
  }

#featureright{
  background:#fff;
  
  }

#bread{
  background:#F1F2F2;
  height:16px;
  padding:4px;
  font-size:11px;
  border:1px solid #ccc;
  }

#content{
  
  padding-right:20px;
  padding-top:20px;
  padding-bottom:20px;
  margin-bottom:20px;
  min-height:500px;
  }

#content ul{margin-left:20px;}

#right{
  border-left:1px solid #ccc;
  min-height:500px;
  padding-left:20px;
  padding-top:20px;
  }

#footerouter{
 background:#EEECE1;
  margin-top:-8px;
  color:#000;
  }

#footerleft{
    color:#000;
  font-weight:normal;
  font-size:11px;
    padding-bottom:20px;
  
  }

#footercenter{
      color:#000;
      font-weight:normal;
  font-size:11px;
  border-right:1px solid #ccc;
    border-left:1px solid #ccc;
    padding-left:20px;
      padding-right:20px;
        padding-bottom:20px;
  }

#footerright{
      color:#000;
    font-weight:normal;
  font-size:11px;
    padding-bottom:20px;
  }


.greybox {
background:#F2F2F2;
border:1px dotted #ccc;
min-height:110px;
margin-bottom:20px;
padding:5px;
}

.greybox:hover {
background:#fff;
}

.greybox img {
border:4px solid #fff;
}

.greybox:hover img {
border:4px solid #F2F2F2;
}

.tisch1 {
  height:115px;
  background:#A7A9AC;
padding:5px;}

.tisch2 {
  height:115px;
  background:#939598;
}

.haupttisch {
padding:5px;
color:#fff;
}


.decke{padding:10px;}

.obentisch{height:75px;}

.untentisch{text-align:right;
font-weight:normal;}



a.untentischlink:link{
color:#fff;
text-decoration:none;
    font-weight:normal;
}
a.untentischlink:hover{
color:blue;
text-decoration:none;
}
a.untentischlink:active{
color:#fff;
text-decoration:none;
}
a.untentischlink:visited{
color:#fff;
text-decoration:none;
}

.slide-desc-bg {
background:#fff;
}







.footersquare {
margin-left:15px;
}

.footersquare a:link {
color:#fff;
} 
.footersquare a:visited {
color:#fff;
} 
.footersquare a:hover {
color:#ccc;
 text-decoration:none;
} 

#slider100 {width:640px;}

#k2ModuleBox108{margin-left:-20px;}
#k2ModuleBox109{margin-left:-20px;}

#right .inputbox{
border:1px solid #333;
padding:4px;
width:200px;

}

#right .button {
margin-left:7px;
margin-bottom:-5px;
  cursor:hand;
  cursor:pointer;

}

#right h3 {
   
  border-left:#EC0623 4px solid;
  font-size:16px;
  font-weight:normal;
  padding-left:10px;
  margin-bottom:20px;
   margin-top:20px;
  color:#414042;}

  .latestnewsenhanced_107 .newshead .calendar.noimage {      
          background: none; 
         
          filter: none; 
          
          color: #3D3D3D;            
          border-top-right-radius: 0px;
          border-top-left-radius: 0px;
        }   

a.onecatlink {
  padding-left:25px;
  background:url('/templates/naumann/images/fugue/leermasicon.gif') no-repeat top left;}








    #wrapper {
      position: relative;
      margin: 0px auto 0px auto;
      border: none;
      
    }
    
    
    .pin {
      display: none;
    }
    
    
    .tooltip-up, .tooltip-down {
      position: absolute;
      background: url(http://www.scoomadesign.com/freiheit/templates/naumann/images/arrow-up-down.png);
      width: 11px;
      height: 15px;
    }
    
    .tooltip-down {
      background-position: 0 -52px;
    }
    
    .tooltip {
      font-size:10px;
      font-weight:normal;
      font-family:Tahoma, Geneva, sans-serif;
      display: none;
      width: 100px;
      cursor: help;
      position: absolute;
      top: 10px;
      left: 50%;
      z-index: 999;
      margin-left: -58px;
      margin-bottom: 10px;
      margin-top: 10px;
      padding:8px;
      color: #222;
      -moz-border-radius: 5px;
      -webkit-border-radius: 5px;
      border-radius: 5px;
      -moz-box-shadow: 0 3px 0 rgba(0,0,0,.7);
      -webkit-box-shadow: 0 3px 0 rgba(0,0,0,.7);
      box-shadow: 0 3px 0 rgba(0,0,0,.7);
      background: #fff1d3;
      background: -webkit-gradient(linear, left top, left bottom, from(#fff1d3), to(#ffdb90));
      background: -webkit-linear-gradient(top, #fff1d3, #ffdb90);
      background: -moz-linear-gradient(top, #fff1d3, #ffdb90);
      background: -ms-linear-gradient(top, #fff1d3, #ffdb90);
      background: -o-linear-gradient(top, #fff1d3, #ffdb90);
      background: linear-gradient(top, #fff1d3, #ffdb90);      
    }
    
    .tooltip::after {
      content: '';
      position: absolute;
      top: -10px;
      left: 50%;
      margin-left: -10px;
      border-bottom: 10px solid #fff1d3;
      border-left: 10px solid transparent;
      border-right :10px solid transparent;
    }
    
    .tooltip-down .tooltip {
      bottom: 12px;
      top: auto;
    }
    
    .tooltip-down .tooltip::after {
      bottom: -10px;
      top: auto;
      border-bottom: 0;
      border-top: 10px solid #ffdb90;
    }
    
    .tooltip h2 {
      font: 11px 'Tahoma', Arial;
      margin: 0 0 5px;
      font-weight:normal;  
    }
    
    .tooltip ul {
      margin: 0;
      padding: 0;
      list-style: none;
      font-weight:normal;  
    }    


.toptable {border:1px solid #ccc;}
.culmtable  {border:1px solid #ccc; padding:4px;}


.pagenav {margin:3px; padding:3px; background:#333; color:#fff; }
.k2Pagination a:link {color:#fff;}
.k2Pagination a:visited {color:#fff;}
.k2Pagination a:hover {color:#fff;}
.k2Pagination a:active {color:#fff;}
.pagination-prev {}
.pagination-start {} 