<?php function getFileMTime($filePath){
        $time = filemtime($filePath);
        $isDST     = (date('I', $time) == 1);
        $systemDST = (date('I') == 1);
        if ($isDST == false && $systemDST == true) $adjustment = 3600;
        else if ($isDST == true && $systemDST == false) $adjustment = -3600;
        else $adjustment = 0;
        return ($time + $adjustment);
}
if (function_exists('mb_convert_encoding') && function_exists('mb_detect_encoding')) {
	function file_get_contents_utf8($fn)
	{
		$content = file_get_contents($fn);
		return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
	}
} else {
	function file_get_contents_utf8($fn)
	{
		return file_get_contents($fn);
	}
}
header('Expires: '.gmdate('D, d M Y H:i:s', getFileMTime(__FILE__)+3600));
header('Content-type:  application/x-javascript; charset=UTF-8');
header('Cache-Control: public');
header('X-Content-Encoded-By: RokBooster');
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime(__FILE__))){
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', getFileMTime(__FILE__)).' GMT', true, 304);
        exit;
}
else {
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', getFileMTime(__FILE__)).' GMT', true, 200);
}
if (!get_cfg_var("zlib.output_compression") && true) ob_start ("ob_gzhandler"); else ob_start ();echo file_get_contents_utf8(dirname(__FILE__) . DIRECTORY_SEPARATOR . '2dbbb2352325101e4b4ff35108fd752c_data.php');