<?php die("Access Denied"); ?>#x#a:2:{s:6:"output";s:0:"";s:6:"result";O:8:"stdClass":52:{s:2:"id";s:3:"517";s:5:"title";s:32:"Las elecciones peruanas del 2016";s:5:"alias";s:32:"las-elecciones-peruanas-del-2016";s:5:"catid";s:1:"3";s:9:"published";s:1:"1";s:9:"introtext";s:303:"<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
";s:8:"fulltext";s:47018:"
<p>&nbsp;</p>
<p>Los elecciones peruanas son, como suele ser por estos lares, un momento de polarización en el que se ponen en duda los cimientos mismos sobre los que está construido el sistema republicano. La joven democracia peruana -decimos joven porque recién volvió en el 2001- ha experimentado tres elecciones y han tenido algunas características: las segundas vueltas han supuesto, al menos en teoría, una situación de inflexión frente al modelo económico de mercado que desde los 90 impera. Además, la presencia de movimientos políticos y de figuras 'tradicionales' fue fundamental. Las elecciones que se desarrollan mientras redactamos esta nota, sin embargo, han quebrado varios de esos paradigmas. Aquí analizaremos los aspectos más relevantes sobre lo que viene sucediendo en el Perú.</p>
<p>&nbsp;</p>
<p><strong>Los fantasmas antidemocráticos</strong></p>
<p>&nbsp;</p>
<p>Las distintas opiniones -dentro del Perú, pero más aún de medios extranjeros- acerca de la exclusión de dos candidatos -Julio Guzmán y César Acuña- le han dado al asunto una apariencia antidemocrática. Se ha hablado, pues, de un posible fraude y, sobre todo, de artimañas políticas del establishment para bloquear a quienes representarían amenazas.</p>
<p>&nbsp;</p>
<p>La salida de Guzmán y Acuña poco tuvo que ver con maniobras políticas y más con una ley electoral pésimamente elaborada que sanciona con la exclusión errores procedimentales que podrían ser subsanables sin necesidad de medidas desproporcionadas.</p>
<p>&nbsp;</p>
<p>La decisión de las autoridades electorales no fue antidemocrática. Tampoco fue impulsada por intereses políticos subyacentes. El jurado electoral aplicó la ley, más allá de que sea esta -y en esto estamos de acuerdo- absurda.</p>
<p>&nbsp;</p>
<p>Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado.</p>
<p>&nbsp;</p>
<p><strong>El baile de la izquierda</strong></p>
<p>&nbsp;</p>
<p>Una vez que las candidaturas de Guzmán y Acuña fueron eliminadas, los votos que -de acuerdo con las encuestas- los favorecían migraron hacia la izquierda. La pregunta es: ¿por qué este bolsón de votos (de más o menos 25%) fue a parar a las opciones que plantean más Estado y menos mercado?</p>
<p>&nbsp;</p>
<p>A pesar de que muchos se han conformado con la idea de que una nueva izquierda se ha empezado a consolidar en el Perú, nosotros no nos sentimos cómodos con esa única explicación. Creemos que los factores que permitieron el movimiento de columpio que fortaleció a las opciones de izquierda (Verónika Mendoza, Alfredo Barnechea y Gregorio Santos) tuvo que ver con que el elector peruano promedio es hoy más joven que en los procesos previos.</p>
<p>&nbsp;</p>
<p>Meses antes de los comicios, las encuestas mostraban que lo que la juventud buscaba en su candidato era un rostro nuevo en política. Así, la caída de Guzmán y de Acuña abrió la puerta a que las alternativas de izquierda, también caras nuevas, hereden el voto de los excluidos.</p>
<p>&nbsp;</p>
<p>Si bien las tres opciones que hemos taxonomizado como izquierdistas planteaban la sustitución del mercado por actividad estatal en más de un ámbito, las alternativas eran distintas: Mendoza representó a una izquierda similar a la del Socialismo del Siglo XXI. Barnechea planteó una alternativa más mesurada de socialdemocracia. Santos, finalmente, propuso un socialismo radical y anti minero.</p>
<p>&nbsp;</p>
<p><strong>La caída de Mendoza</strong></p>
<p>&nbsp;</p>
<p>Durante buena parte del proceso, Fujimori se mantuvo firme en el primer lugar, con un tercio del electorado a su favor. La cuestión era quién iba por el segundo.</p>
<p>El crecimiento de Verónika Mendoza fue paulatino y bastante parejo en comparación al de Alfredo Barnechea. No obstante, en días, el carisma de Mendoza y su buena capacidad para comunicar ideas la llevaron a tomar la ventaja.</p>
<p>&nbsp;</p>
<p>Una seguidilla de errores políticos de Barnechea lo dejó -prácticamente- fuera de juego algunos días antes a los comicios. Así, Mendoza y el candidato liberal Pedro Pablo Kuczynski -PPK- llegaron a lo que las encuestadoras llamaron "empate técnico" en el segundo lugar. Ahora: los resultados de la primera vuelta muestran una ventaja de más de dos puntos del candidato liberal sobre la alternativa socialista. ¿Qué sucedió?</p>
<p>&nbsp;</p>
<p>Confluyeron tres cuestiones que generaron un "voto estratégico". Es decir: el abandono de ciertas preferencias previas por un voto que asegure la ausencia de Mendoza en la segunda vuelta. El temor que despertó Mendoza en varios sectores del electorado tuvo que ver con su vinculación con el chavismo bolivariano y el recuerdo del colapso económico que trajo el estatismo en el Perú décadas atrás. Sin embargo, a este primer motivo, correctamente sustentado en la realidad, se sumó una guerra sucia que buscó -injustamente- vincular a Mendoza con movimientos terroristas. Decimos que es injusto porque, a pesar de ser el Frente Amplio un movimiento abiertamente socialista, existe una diferencia irrenunciable entre el socialismo y el terrorismo.</p>
<p>&nbsp;</p>
<p>Así, entre verdades y mentiras, Mendoza perdió fuerza. Al día siguiente de su fracaso electoral, la Bolsa de Valores de Lima cerró la jornada con un crecimiento sin precedentes en los últimos años.</p>
<p><strong><br /></strong></p>
<p><strong>PPKeiko y el divorcio entre libertades</strong></p>
<p><strong><br /></strong></p>
<p>La salida de Mendoza dejó un panorama incómodo para quienes dieron su apoyo a los candidatos de izquierda. Esta vez tendrían que elegir entre dos opciones afines al libre mercado.</p>
<p>&nbsp;</p>
<p>Antes de la primera vuelta, cuando las encuestas mostraban ya una disputa importante entre PPK y Mendoza por el segundo lugar, muchos peruanos percibían ya una identidad casi absoluta entre Fuijmori y Kuczynski. Esta identidad dio pie a un ingenioso apodo para Kuczynski: PPKeiko.</p>
<p>&nbsp;</p>
<p>Este apodo echa relevantes luces el imaginario popular del elector peruano. Utilizar el término PPKeiko para equiparar a PPK con Keiko Fujimori revela la dificultad que existe para diferenciar a dos candidatos que tienen discrepancias fundamentales, pero que representan a la derecha, entendida esta como la opción afín a la libertad económica. Lo que no parecen percibir los electores -y esto es lo relevante- es el factor de libertad social que marca una profunda diferencia entre ambos candidatos.</p>
<p>&nbsp;</p>
<p>Fujimori ha basado su campaña en un supuesto deslinde con el gobierno de su padre -el "nuevo fujimorismo"- y un compromiso de respetar el orden democrático y los derechos humanos, una serie de declaraciones anteriores -que el juicio de Alberto Fujimori fue injusto y la posibilidad de indultarlo, por ejemplo- han despertado un justo recelo en quienes se mantienen vigilantes con la candidata.</p>
<p>&nbsp;</p>
<p>Por otro lado, Fujimori -a diferencia de PPK- se ha manifestado numerosas veces en contra de medidas liberales en cuestiones sociales, tales como la despenalización del aborto en casos de violación, la legalización de la venta de marihuana y la unión civil entre parejas del mismo sexo -ni qué decir de la posibilidad de matrimonios homosexuales-.</p>
<p>&nbsp;</p>
<p>PPK, en cambio, ha propuesto legalizar la marihuana como una estrategia para combatir narcotráfico y, en cuanto a la unión civil entre parejas homosexuales, ha dicho que apoyará a que la gente "viva como quiera y en paz".</p>
<p>&nbsp;</p>
<p>En el Perú, pues, parece existir una dicotomía entre libertad económica y libertad social. La dictadura de Alberto Fujimori -que representó la primera apertura del mercado desde 1963- estigmatizó la idea de "la derecha" como una opción necesariamente contradictoria con la libertad civil, la democracia y los derechos humanos. Y esta dicotomía ha llevado incluso a un llamado al voto viciado por parte de un relevante sector de la izquierda en la segunda vuelta. Porque -según dicen- cuando se vota por PPK o por Keiko se vota, en realidad, por la misma alternativa.</p>
<div style="position: absolute; left: -40px; top: -25px; width: 1px; height: 1px; overflow: hidden;" data-mce-bogus="1" class="mcePaste" id="_mcePaste"><!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<p class="MsoNormal" style="line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Las elecciones peruanas del 2016</span></b></p>
<p class="MsoNormal" style="line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Mijael Garrido Lecca Palacios </span></b></p>
<p class="MsoNormal" style="line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Ariana Lira Delcore</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Lima</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">@MijaelGLP</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">@arianalirad</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Los elecciones peruanas son, como suele ser por estos lares, un momento de polarización en el que se ponen en duda los cimientos mismos sobre los que está construido el sistema republicano. La joven democracia peruana -decimos joven porque recién volvió en el 2001- ha experimentado tres elecciones y han tenido algunas características: las segundas vueltas han supuesto, al menos en teoría, una situación de inflexión frente al modelo económico de mercado que desde los 90 impera. Además, la presencia de movimientos políticos y de figuras ‘tradicionales’ fue fundamental. Las elecciones que se desarrollan mientras redactamos esta nota, sin embargo, han quebrado varios de esos paradigmas. Aquí analizaremos los aspectos más relevantes sobre lo que viene sucediendo en el Perú.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Los fantasmas antidemocráticos</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Las distintas opiniones -dentro del Perú, pero más aún de medios extranjeros- acerca de la exclusión de dos candidatos -Julio Guzmán y César Acuña- le han dado al asunto una apariencia antidemocrática. Se ha hablado, pues, de un posible fraude y, sobre todo, de artimañas políticas del <i style="mso-bidi-font-style: normal;">establishment</i> para bloquear a quienes representarían amenazas.</span></p>
<p class="MsoNormal" style="line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; mso-fareast-font-family: 'Times New Roman'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">La salida de Guzmán y Acuña poco tuvo que ver con maniobras políticas y más con una ley electoral pésimamente elaborada que sanciona con la exclusión errores procedimentales que podrían ser subsanables sin necesidad de medidas desproporcionadas.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">La decisión de las autoridades electorales no fue antidemocrática. Tampoco fue impulsada por intereses políticos subyacentes. El jurado electoral aplicó la ley, más allá de que sea esta -y en esto estamos de acuerdo- absurda.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">El baile de la izquierda</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Una vez que las candidaturas de Guzmán y Acuña fueron eliminadas, los votos que -de acuerdo con las encuestas- los favorecían migraron hacia la izquierda. La pregunta es: ¿por qué este bolsón de votos (de más o menos 25%) fue a parar a las opciones que plantean más Estado y menos mercado? </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">A pesar de que muchos se han conformado con la idea de que una nueva izquierda se ha empezado a consolidar en el Perú, nosotros no nos sentimos cómodos con esa única explicación. Creemos que los factores que permitieron el movimiento de columpio que fortaleció a las opciones de izquierda (Verónika Mendoza, Alfredo Barnechea y Gregorio Santos) tuvo que ver con que el elector peruano promedio es hoy más joven que en los procesos previos. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Meses antes de los comicios, las encuestas mostraban que lo que la juventud buscaba en su candidato era un rostro nuevo en política. Así, la caída de Guzmán y de Acuña abrió la puerta a que las alternativas de izquierda, también caras nuevas, hereden el voto de los excluidos.<span style="mso-spacerun: yes;">&nbsp; </span></span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Si bien las tres opciones que hemos taxonomizado como izquierdistas planteaban la sustitución del mercado por actividad estatal en más de un ámbito, las alternativas eran distintas: Mendoza representó a una izquierda similar a la del Socialismo del Siglo XXI. Barnechea planteó una alternativa más mesurada de socialdemocracia. Santos, finalmente, propuso un socialismo radical y anti minero.</span></p>
<p class="MsoNormal" style="line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">La caída de Mendoza</span></b></p>
<p class="MsoNormal" style="line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Durante buena parte del proceso, Fujimori se mantuvo firme en el primer lugar, con un tercio del electorado a su favor. La cuestión era quién iba por el segundo. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">El crecimiento de Verónika Mendoza fue paulatino y bastante parejo en comparación al de Alfredo Barnechea. No obstante, en días, el carisma de Mendoza y su buena capacidad para comunicar ideas la llevaron a tomar la ventaja. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Una seguidilla de errores políticos de Barnechea lo dejó -prácticamente- fuera de juego algunos días antes a los comicios. Así, Mendoza y el candidato liberal Pedro Pablo Kuczynski -PPK- llegaron a lo que las encuestadoras llamaron “empate técnico” en el segundo lugar. Ahora: los resultados de la primera vuelta muestran una ventaja de más de dos puntos del candidato liberal sobre la alternativa socialista. ¿Qué sucedió? </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Confluyeron tres cuestiones que generaron un “voto estratégico”. Es decir: el abandono de ciertas preferencias previas por un voto que asegure la ausencia de Mendoza en la segunda vuelta. El temor que despertó Mendoza en varios sectores del electorado tuvo que ver con su vinculación con el chavismo bolivariano y el recuerdo del colapso económico que trajo el estatismo en el Perú décadas atrás. Sin embargo, a este primer motivo, correctamente sustentado en la realidad, se sumó una guerra sucia que buscó -injustamente- vincular a Mendoza con movimientos terroristas. Decimos que es injusto porque, a pesar de ser el Frente Amplio un movimiento abiertamente socialista, existe una diferencia irrenunciable entre el socialismo y el terrorismo. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Así, entre verdades y mentiras, Mendoza perdió fuerza. Al día siguiente de su fracaso electoral, la Bolsa de Valores de Lima cerró la jornada con un crecimiento sin precedentes en los últimos años. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">PPKeiko y el divorcio entre libertades</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">La salida de Mendoza dejó un panorama incómodo para quienes dieron su apoyo a los candidatos de izquierda. Esta vez tendrían que elegir entre dos opciones afines al libre mercado.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Antes de la primera vuelta, cuando las encuestas mostraban ya una disputa importante entre PPK y Mendoza por el segundo lugar, muchos peruanos percibían ya una identidad casi absoluta entre Fuijmori y Kuczynski. Esta identidad dio pie a un ingenioso apodo para Kuczynski: PPKeiko. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Este apodo echa relevantes luces el imaginario popular del elector peruano. Utilizar el término PPKeiko para equiparar a PPK con Keiko Fujimori revela la dificultad que existe para diferenciar a dos candidatos que tienen discrepancias fundamentales, pero que representan a la derecha, entendida esta como la opción afín a la libertad económica. Lo que no parecen percibir los electores -y esto es lo relevante- es el factor de libertad social que marca una profunda diferencia entre ambos candidatos.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Fujimori ha basado su campaña en un supuesto deslinde con el gobierno de su padre -el “nuevo fujimorismo”- y un compromiso de respetar el orden democrático y los derechos humanos, una serie de declaraciones anteriores -que el juicio de Alberto Fujimori fue injusto y la posibilidad de indultarlo, por ejemplo- han despertado un justo recelo en quienes se mantienen vigilantes con la candidata. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Por otro lado, Fujimori -a diferencia de PPK- se ha manifestado numerosas veces en contra de medidas liberales en cuestiones sociales, tales como la despenalización del aborto en casos de violación, la legalización de la venta de marihuana y la unión civil entre parejas del mismo sexo -ni qué decir de la posibilidad de matrimonios homosexuales-.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">PPK, en cambio, ha propuesto legalizar la marihuana como una estrategia para combatir narcotráfico y, en cuanto a la unión civil entre parejas homosexuales, ha dicho que apoyará a que la gente “viva como quiera y en paz”.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">En el Perú, pues, parece existir una dicotomía entre libertad económica y libertad social. La dictadura de Alberto Fujimori -que representó la primera apertura del mercado desde 1963- estigmatizó la idea de “la derecha” como una opción necesariamente contradictoria con la libertad civil, la democracia y los derechos humanos. Y esta dicotomía ha llevado incluso a un llamado al voto viciado por parte de un relevante sector de la izquierda en la segunda vuelta. Porque -según dicen- cuando se vota por PPK o por Keiko se vota, en realidad, por la misma alternativa. </span></p>
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:TrackMoves/>
  <w:TrackFormatting/>
  <w:DoNotShowComments/>
  <w:DoNotShowPropertyChanges/>
  <w:HyphenationZone>21</w:HyphenationZone>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>ES-TRAD</w:LidThemeOther>
  <w:LidThemeAsian>JA</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]--><!--[if gte mso 10]>

<![endif]--></div>";s:5:"video";N;s:7:"gallery";N;s:12:"extra_fields";s:2:"[]";s:19:"extra_fields_search";s:0:"";s:7:"created";s:19:"2016-06-01 14:41:37";s:10:"created_by";s:3:"744";s:16:"created_by_alias";s:0:"";s:11:"checked_out";s:3:"744";s:16:"checked_out_time";s:19:"2016-06-01 14:54:09";s:8:"modified";s:19:"2016-06-01 14:54:09";s:11:"modified_by";s:3:"744";s:10:"publish_up";s:19:"2016-06-01 14:41:37";s:12:"publish_down";s:19:"0000-00-00 00:00:00";s:5:"trash";s:1:"0";s:6:"access";s:1:"1";s:8:"ordering";s:2:"85";s:8:"featured";s:1:"0";s:17:"featured_ordering";s:1:"0";s:13:"image_caption";s:0:"";s:13:"image_credits";s:0:"";s:13:"video_caption";s:0:"";s:13:"video_credits";s:0:"";s:4:"hits";i:0;s:6:"params";O:10:"JParameter":5:{s:7:" * _raw";s:4257:"{"enable_css":"1","jQueryHandling":"1.8.2","backendJQueryHandling":"local","userName":"1","userImage":"1","userDescription":"1","userURL":"1","userEmail":"0","userFeedLink":"1","userFeedIcon":"1","userItemCount":"10","userItemTitle":"1","userItemTitleLinked":"1","userItemDateCreated":"1","userItemImage":"1","userItemIntroText":"1","userItemCategory":"1","userItemTags":"1","userItemCommentsAnchor":"1","userItemReadMore":"1","userItemK2Plugins":"1","defaultUsersItemid":"","tagItemCount":"10","tagItemTitle":"1","tagItemTitleLinked":"1","tagItemDateCreated":"1","tagItemImage":"1","tagItemIntroText":"1","tagItemCategory":"1","tagItemReadMore":"1","tagItemExtraFields":"0","tagOrdering":"","tagFeedLink":"1","tagFeedIcon":"1","defaultTagsItemid":"","genericItemCount":"10","genericItemTitle":"1","genericItemTitleLinked":"1","genericItemDateCreated":"1","genericItemImage":"1","genericItemIntroText":"1","genericItemCategory":"1","genericItemReadMore":"1","genericItemExtraFields":"0","genericFeedLink":"1","genericFeedIcon":"1","feedLimit":"10","feedItemImage":"1","feedImgSize":"S","feedItemIntroText":"1","feedTextWordLimit":"","feedItemFullText":"1","feedItemTags":"0","feedItemVideo":"0","feedItemGallery":"0","feedItemAttachments":"0","feedBogusEmail":"","introTextCleanup":"0","introTextCleanupExcludeTags":"","introTextCleanupTagAttr":"","fullTextCleanup":"0","fullTextCleanupExcludeTags":"","fullTextCleanupTagAttr":"","xssFiltering":"0","linkPopupWidth":"900","linkPopupHeight":"600","imagesQuality":"100","itemImageXS":"100","itemImageS":"200","itemImageM":"400","itemImageL":"600","itemImageXL":"900","itemImageGeneric":"300","catImageWidth":"100","catImageDefault":"1","userImageWidth":"100","userImageDefault":"1","commenterImgWidth":"48","onlineImageEditor":"splashup","imageTimestamp":"0","imageMemoryLimit":"","socialButtonCode":"","twitterUsername":"","facebookImage":"Small","comments":"1","commentsOrdering":"DESC","commentsLimit":"10","commentsFormPosition":"below","commentsPublishing":"1","commentsReporting":"2","commentsReportRecipient":"","inlineCommentsModeration":"0","gravatar":"1","recaptcha":"0","commentsFormNotes":"1","commentsFormNotesText":"","frontendEditing":"1","showImageTab":"1","showImageGalleryTab":"1","showVideoTab":"1","showExtraFieldsTab":"1","showAttachmentsTab":"1","showK2Plugins":"1","sideBarDisplayFrontend":"0","mergeEditors":"1","sideBarDisplay":"1","attachmentsFolder":"","hideImportButton":"0","taggingSystem":"1","lockTags":"0","showTagFilter":"0","googleSearch":"0","googleSearchContainer":"k2Container","K2UserProfile":"1","redirect":"","adminSearch":"simple","cookieDomain":"","recaptcha_public_key":"","recaptcha_private_key":"","recaptcha_theme":"clean","recaptchaOnRegistration":"0","stopForumSpam":"0","stopForumSpamApiKey":"","showItemsCounterAdmin":"1","showChildCatItems":"1","disableCompactOrdering":"0","metaDescLimit":"150","SEFReplacements":"\u00c5\u00a0|S, \u00c5\u2019|O, \u00c5\u00bd|Z, \u00c5\u00a1|s, \u00c5\u201c|oe, \u00c5\u00be|z, \u00c5\u00b8|Y, \u00c2\u00a5|Y, \u00c2\u00b5|u, \u00c3\u20ac|A, \u00c3\ufffd|A, \u00c3\u201a|A, \u00c3\u0192|A, \u00c3\u201e|A, \u00c3\u2026|A, \u00c3\u2020|A, \u00c3\u2021|C, \u00c3\u02c6|E, \u00c3\u2030|E, \u00c3\u0160|E, \u00c3\u2039|E, \u00c3\u0152|I, \u00c3\ufffd|I, \u00c3\u017d|I, \u00c3\ufffd|I, \u00c3\ufffd|D, \u00c3\u2018|N, \u00c3\u2019|O, \u00c3\u201c|O, \u00c3\u201d|O, \u00c3\u2022|O, \u00c3\u2013|O, \u00c3\u02dc|O, \u00c3\u2122|U, \u00c3\u0161|U, \u00c3\u203a|U, \u00c3\u0153|U, \u00c3\ufffd|Y, \u00c3\u0178|s, \u00c3\u00a0|a, \u00c3\u00a1|a, \u00c3\u00a2|a, \u00c3\u00a3|a, \u00c3\u00a4|a, \u00c3\u00a5|a, \u00c3\u00a6|a, \u00c3\u00a7|c, \u00c3\u00a8|e, \u00c3\u00a9|e, \u00c3\u00aa|e, \u00c3\u00ab|e, \u00c3\u00ac|i, \u00c3\u00ad|i, \u00c3\u00ae|i, \u00c3\u00af|i, \u00c3\u00b0|o, \u00c3\u00b1|n, \u00c3\u00b2|o, \u00c3\u00b3|o, \u00c3\u00b4|o, \u00c3\u00b5|o, \u00c3\u00b6|o, \u00c3\u00b8|o, \u00c3\u00b9|u, \u00c3\u00ba|u, \u00c3\u00bb|u, \u00c3\u00bc|u, \u00c3\u00bd|y, \u00c3\u00bf|y, \u00c3\u0178|ss","sh404SefLabelCat":"","sh404SefLabelUser":"blog","sh404SefLabelItem":"2","sh404SefTitleAlias":"alias","sh404SefModK2ContentFeedAlias":"feed","sh404SefInsertItemId":"0","sh404SefInsertUniqueItemId":"0","cbIntegration":"0"}";s:7:" * _xml";N;s:12:" * _elements";a:0:{}s:15:" * _elementPath";a:1:{i:0;s:69:"/home4/relial2015/public_html/libraries/joomla/html/parameter/element";}s:7:" * data";O:8:"stdClass":239:{s:10:"enable_css";s:1:"1";s:14:"jQueryHandling";s:5:"1.8.2";s:21:"backendJQueryHandling";s:5:"local";s:8:"userName";s:1:"1";s:9:"userImage";s:1:"1";s:15:"userDescription";s:1:"1";s:7:"userURL";s:1:"1";s:9:"userEmail";s:1:"0";s:12:"userFeedLink";s:1:"1";s:12:"userFeedIcon";s:1:"1";s:13:"userItemCount";s:2:"10";s:13:"userItemTitle";s:1:"1";s:19:"userItemTitleLinked";s:1:"1";s:19:"userItemDateCreated";s:1:"1";s:13:"userItemImage";s:1:"1";s:17:"userItemIntroText";s:1:"1";s:16:"userItemCategory";s:1:"1";s:12:"userItemTags";s:1:"1";s:22:"userItemCommentsAnchor";s:1:"1";s:16:"userItemReadMore";s:1:"1";s:17:"userItemK2Plugins";s:1:"1";s:18:"defaultUsersItemid";s:0:"";s:12:"tagItemCount";s:2:"10";s:12:"tagItemTitle";s:1:"1";s:18:"tagItemTitleLinked";s:1:"1";s:18:"tagItemDateCreated";s:1:"1";s:12:"tagItemImage";s:1:"1";s:16:"tagItemIntroText";s:1:"1";s:15:"tagItemCategory";s:1:"1";s:15:"tagItemReadMore";s:1:"1";s:18:"tagItemExtraFields";s:1:"0";s:11:"tagOrdering";s:0:"";s:11:"tagFeedLink";s:1:"1";s:11:"tagFeedIcon";s:1:"1";s:17:"defaultTagsItemid";s:0:"";s:16:"genericItemCount";s:2:"10";s:16:"genericItemTitle";s:1:"1";s:22:"genericItemTitleLinked";s:1:"1";s:22:"genericItemDateCreated";s:1:"1";s:16:"genericItemImage";s:1:"1";s:20:"genericItemIntroText";s:1:"1";s:19:"genericItemCategory";s:1:"1";s:19:"genericItemReadMore";s:1:"1";s:22:"genericItemExtraFields";s:1:"0";s:15:"genericFeedLink";s:1:"1";s:15:"genericFeedIcon";s:1:"1";s:9:"feedLimit";s:2:"10";s:13:"feedItemImage";s:1:"1";s:11:"feedImgSize";s:1:"S";s:17:"feedItemIntroText";s:1:"1";s:17:"feedTextWordLimit";s:0:"";s:16:"feedItemFullText";s:1:"1";s:12:"feedItemTags";s:1:"0";s:13:"feedItemVideo";s:1:"0";s:15:"feedItemGallery";s:1:"0";s:19:"feedItemAttachments";s:1:"0";s:14:"feedBogusEmail";s:0:"";s:16:"introTextCleanup";s:1:"0";s:27:"introTextCleanupExcludeTags";s:0:"";s:23:"introTextCleanupTagAttr";s:0:"";s:15:"fullTextCleanup";s:1:"0";s:26:"fullTextCleanupExcludeTags";s:0:"";s:22:"fullTextCleanupTagAttr";s:0:"";s:12:"xssFiltering";s:1:"0";s:14:"linkPopupWidth";s:3:"900";s:15:"linkPopupHeight";s:3:"600";s:13:"imagesQuality";s:3:"100";s:11:"itemImageXS";s:3:"100";s:10:"itemImageS";s:3:"200";s:10:"itemImageM";s:3:"400";s:10:"itemImageL";s:3:"600";s:11:"itemImageXL";s:3:"900";s:16:"itemImageGeneric";s:3:"300";s:13:"catImageWidth";s:3:"100";s:15:"catImageDefault";s:1:"1";s:14:"userImageWidth";s:3:"100";s:16:"userImageDefault";s:1:"1";s:17:"commenterImgWidth";s:2:"48";s:17:"onlineImageEditor";s:8:"splashup";s:14:"imageTimestamp";s:1:"0";s:16:"imageMemoryLimit";s:0:"";s:16:"socialButtonCode";s:0:"";s:15:"twitterUsername";s:0:"";s:13:"facebookImage";s:5:"Small";s:8:"comments";s:1:"1";s:16:"commentsOrdering";s:4:"DESC";s:13:"commentsLimit";s:2:"10";s:20:"commentsFormPosition";s:5:"below";s:18:"commentsPublishing";s:1:"1";s:17:"commentsReporting";s:1:"2";s:23:"commentsReportRecipient";s:0:"";s:24:"inlineCommentsModeration";s:1:"0";s:8:"gravatar";s:1:"1";s:9:"recaptcha";s:1:"0";s:17:"commentsFormNotes";s:1:"1";s:21:"commentsFormNotesText";s:0:"";s:15:"frontendEditing";s:1:"1";s:12:"showImageTab";s:1:"1";s:19:"showImageGalleryTab";s:1:"1";s:12:"showVideoTab";s:1:"1";s:18:"showExtraFieldsTab";s:1:"1";s:18:"showAttachmentsTab";s:1:"1";s:13:"showK2Plugins";s:1:"1";s:22:"sideBarDisplayFrontend";s:1:"0";s:12:"mergeEditors";s:1:"1";s:14:"sideBarDisplay";s:1:"1";s:17:"attachmentsFolder";s:0:"";s:16:"hideImportButton";s:1:"0";s:13:"taggingSystem";s:1:"1";s:8:"lockTags";s:1:"0";s:13:"showTagFilter";s:1:"0";s:12:"googleSearch";s:1:"0";s:21:"googleSearchContainer";s:11:"k2Container";s:13:"K2UserProfile";s:1:"1";s:8:"redirect";s:0:"";s:11:"adminSearch";s:6:"simple";s:12:"cookieDomain";s:0:"";s:20:"recaptcha_public_key";s:0:"";s:21:"recaptcha_private_key";s:0:"";s:15:"recaptcha_theme";s:5:"clean";s:23:"recaptchaOnRegistration";s:1:"0";s:13:"stopForumSpam";s:1:"0";s:19:"stopForumSpamApiKey";s:0:"";s:21:"showItemsCounterAdmin";s:1:"1";s:17:"showChildCatItems";s:1:"1";s:22:"disableCompactOrdering";s:1:"0";s:13:"metaDescLimit";s:3:"150";s:15:"SEFReplacements";s:583:"Å |S, Å’|O, Å½|Z, Å¡|s, Å“|oe, Å¾|z, Å¸|Y, Â¥|Y, Âµ|u, Ã€|A, Ã�|A, Ã‚|A, Ãƒ|A, Ã„|A, Ã…|A, Ã†|A, Ã‡|C, Ãˆ|E, Ã‰|E, ÃŠ|E, Ã‹|E, ÃŒ|I, Ã�|I, ÃŽ|I, Ã�|I, Ã�|D, Ã‘|N, Ã’|O, Ã“|O, Ã”|O, Ã•|O, Ã–|O, Ã˜|O, Ã™|U, Ãš|U, Ã›|U, Ãœ|U, Ã�|Y, ÃŸ|s, Ã |a, Ã¡|a, Ã¢|a, Ã£|a, Ã¤|a, Ã¥|a, Ã¦|a, Ã§|c, Ã¨|e, Ã©|e, Ãª|e, Ã«|e, Ã¬|i, Ã­|i, Ã®|i, Ã¯|i, Ã°|o, Ã±|n, Ã²|o, Ã³|o, Ã´|o, Ãµ|o, Ã¶|o, Ã¸|o, Ã¹|u, Ãº|u, Ã»|u, Ã¼|u, Ã½|y, Ã¿|y, ÃŸ|ss";s:16:"sh404SefLabelCat";s:0:"";s:17:"sh404SefLabelUser";s:4:"blog";s:17:"sh404SefLabelItem";s:1:"2";s:18:"sh404SefTitleAlias";s:5:"alias";s:29:"sh404SefModK2ContentFeedAlias";s:4:"feed";s:20:"sh404SefInsertItemId";s:1:"0";s:26:"sh404SefInsertUniqueItemId";s:1:"0";s:13:"cbIntegration";s:1:"0";s:11:"inheritFrom";s:1:"0";s:17:"num_leading_items";s:1:"0";s:19:"num_leading_columns";s:1:"1";s:14:"leadingImgSize";s:5:"Large";s:17:"num_primary_items";s:1:"6";s:19:"num_primary_columns";s:1:"1";s:14:"primaryImgSize";s:6:"XSmall";s:19:"num_secondary_items";s:1:"0";s:21:"num_secondary_columns";s:1:"1";s:16:"secondaryImgSize";s:5:"Small";s:9:"num_links";s:1:"0";s:17:"num_links_columns";s:1:"1";s:12:"linksImgSize";s:6:"XSmall";s:14:"catCatalogMode";s:1:"1";s:16:"catFeaturedItems";s:1:"1";s:13:"catPagination";s:1:"2";s:20:"catPaginationResults";s:1:"1";s:8:"catTitle";s:1:"1";s:19:"catTitleItemCounter";s:1:"0";s:14:"catDescription";s:1:"1";s:8:"catImage";s:1:"0";s:11:"catFeedLink";s:1:"0";s:11:"catFeedIcon";s:1:"0";s:13:"subCategories";s:1:"1";s:13:"subCatColumns";s:1:"3";s:11:"subCatTitle";s:1:"1";s:22:"subCatTitleItemCounter";s:1:"0";s:17:"subCatDescription";s:1:"0";s:11:"subCatImage";s:1:"1";s:12:"catItemTitle";s:1:"1";s:18:"catItemTitleLinked";s:1:"1";s:21:"catItemFeaturedNotice";s:1:"0";s:13:"catItemAuthor";s:1:"0";s:18:"catItemDateCreated";s:1:"0";s:13:"catItemRating";s:1:"0";s:12:"catItemImage";s:1:"1";s:16:"catItemIntroText";s:1:"1";s:18:"catItemExtraFields";s:1:"0";s:11:"catItemHits";s:1:"0";s:15:"catItemCategory";s:1:"0";s:11:"catItemTags";s:1:"0";s:18:"catItemAttachments";s:1:"0";s:25:"catItemAttachmentsCounter";s:1:"0";s:12:"catItemVideo";s:1:"0";s:20:"catItemVideoAutoPlay";s:1:"0";s:19:"catItemImageGallery";s:1:"0";s:19:"catItemDateModified";s:1:"0";s:15:"catItemReadMore";s:1:"1";s:21:"catItemCommentsAnchor";s:1:"0";s:16:"catItemK2Plugins";s:1:"1";s:15:"itemDateCreated";s:1:"0";s:9:"itemTitle";s:1:"1";s:18:"itemFeaturedNotice";s:1:"0";s:10:"itemAuthor";s:1:"0";s:15:"itemFontResizer";s:1:"0";s:15:"itemPrintButton";s:1:"0";s:15:"itemEmailButton";s:1:"0";s:16:"itemSocialButton";s:1:"0";s:15:"itemVideoAnchor";s:1:"0";s:22:"itemImageGalleryAnchor";s:1:"0";s:18:"itemCommentsAnchor";s:1:"0";s:10:"itemRating";s:1:"0";s:9:"itemImage";s:1:"1";s:11:"itemImgSize";s:6:"XSmall";s:20:"itemImageMainCaption";s:1:"1";s:20:"itemImageMainCredits";s:1:"1";s:13:"itemIntroText";s:1:"1";s:12:"itemFullText";s:1:"1";s:15:"itemExtraFields";s:1:"0";s:16:"itemDateModified";s:1:"0";s:8:"itemHits";s:1:"0";s:12:"itemCategory";s:1:"0";s:8:"itemTags";s:1:"0";s:15:"itemAttachments";s:1:"1";s:22:"itemAttachmentsCounter";s:1:"1";s:9:"itemVideo";s:1:"1";s:17:"itemVideoAutoPlay";s:1:"0";s:16:"itemVideoCaption";s:1:"1";s:16:"itemVideoCredits";s:1:"1";s:16:"itemImageGallery";s:1:"1";s:14:"itemNavigation";s:1:"1";s:12:"itemComments";s:1:"0";s:17:"itemTwitterButton";s:1:"1";s:18:"itemFacebookButton";s:1:"1";s:23:"itemGooglePlusOneButton";s:1:"1";s:15:"itemAuthorBlock";s:1:"0";s:15:"itemAuthorImage";s:1:"0";s:21:"itemAuthorDescription";s:1:"0";s:13:"itemAuthorURL";s:1:"0";s:15:"itemAuthorEmail";s:1:"0";s:16:"itemAuthorLatest";s:1:"0";s:21:"itemAuthorLatestLimit";s:1:"5";s:11:"itemRelated";s:1:"0";s:16:"itemRelatedLimit";s:1:"5";s:16:"itemRelatedTitle";s:1:"0";s:19:"itemRelatedCategory";s:1:"0";s:20:"itemRelatedImageSize";s:1:"0";s:20:"itemRelatedIntrotext";s:1:"0";s:19:"itemRelatedFulltext";s:1:"0";s:17:"itemRelatedAuthor";s:1:"0";s:16:"itemRelatedMedia";s:1:"0";s:23:"itemRelatedImageGallery";s:1:"0";s:13:"itemK2Plugins";s:1:"1";}}s:8:"metadesc";s:0:"";s:8:"metadata";s:15:"robots=
author=";s:7:"metakey";s:0:"";s:7:"plugins";s:0:"";s:8:"language";s:1:"*";s:11:"lastChanged";s:19:"2016-06-01 14:54:09";s:12:"categoryname";s:10:"Actualidad";s:10:"categoryid";s:1:"3";s:13:"categoryalias";s:10:"actualidad";s:14:"categoryparams";s:2744:"{"inheritFrom":"1","theme":"","num_leading_items":"2","num_leading_columns":"1","leadingImgSize":"Large","num_primary_items":"4","num_primary_columns":"2","primaryImgSize":"Medium","num_secondary_items":"4","num_secondary_columns":"1","secondaryImgSize":"Small","num_links":"4","num_links_columns":"1","linksImgSize":"XSmall","catCatalogMode":"0","catFeaturedItems":"1","catOrdering":"","catPagination":"2","catPaginationResults":"1","catTitle":"1","catTitleItemCounter":"1","catDescription":"1","catImage":"1","catFeedLink":"1","catFeedIcon":"1","subCategories":"1","subCatColumns":"2","subCatOrdering":"","subCatTitle":"1","subCatTitleItemCounter":"1","subCatDescription":"1","subCatImage":"1","itemImageXS":"","itemImageS":"","itemImageM":"","itemImageL":"","itemImageXL":"","catItemTitle":"1","catItemTitleLinked":"1","catItemFeaturedNotice":"0","catItemAuthor":"1","catItemDateCreated":"1","catItemRating":"0","catItemImage":"1","catItemIntroText":"1","catItemIntroTextWordLimit":"","catItemExtraFields":"0","catItemHits":"0","catItemCategory":"1","catItemTags":"1","catItemAttachments":"0","catItemAttachmentsCounter":"0","catItemVideo":"0","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"0","catItemImageGallery":"0","catItemDateModified":"0","catItemReadMore":"1","catItemCommentsAnchor":"1","catItemK2Plugins":"1","itemDateCreated":"1","itemTitle":"1","itemFeaturedNotice":"1","itemAuthor":"1","itemFontResizer":"1","itemPrintButton":"1","itemEmailButton":"1","itemSocialButton":"1","itemVideoAnchor":"1","itemImageGalleryAnchor":"1","itemCommentsAnchor":"1","itemRating":"1","itemImage":"1","itemImgSize":"Large","itemImageMainCaption":"1","itemImageMainCredits":"1","itemIntroText":"1","itemFullText":"1","itemExtraFields":"1","itemDateModified":"1","itemHits":"1","itemCategory":"1","itemTags":"1","itemAttachments":"1","itemAttachmentsCounter":"1","itemVideo":"1","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"0","itemVideoCaption":"1","itemVideoCredits":"1","itemImageGallery":"1","itemNavigation":"1","itemComments":"1","itemTwitterButton":"1","itemFacebookButton":"1","itemGooglePlusOneButton":"1","itemAuthorBlock":"1","itemAuthorImage":"1","itemAuthorDescription":"1","itemAuthorURL":"1","itemAuthorEmail":"0","itemAuthorLatest":"1","itemAuthorLatestLimit":"5","itemRelated":"1","itemRelatedLimit":"5","itemRelatedTitle":"1","itemRelatedCategory":"0","itemRelatedImageSize":"0","itemRelatedIntrotext":"0","itemRelatedFulltext":"0","itemRelatedAuthor":"0","itemRelatedMedia":"0","itemRelatedImageGallery":"0","itemK2Plugins":"1","catMetaDesc":"","catMetaKey":"","catMetaRobots":"","catMetaAuthor":""}";s:8:"category";O:15:"TableK2Category":22:{s:2:"id";s:1:"3";s:4:"name";s:10:"Actualidad";s:5:"alias";s:10:"actualidad";s:11:"description";s:0:"";s:6:"parent";s:1:"1";s:16:"extraFieldsGroup";s:1:"0";s:9:"published";s:1:"1";s:5:"image";s:5:"3.jpg";s:6:"access";s:1:"1";s:8:"ordering";s:1:"2";s:6:"params";s:2744:"{"inheritFrom":"1","theme":"","num_leading_items":"2","num_leading_columns":"1","leadingImgSize":"Large","num_primary_items":"4","num_primary_columns":"2","primaryImgSize":"Medium","num_secondary_items":"4","num_secondary_columns":"1","secondaryImgSize":"Small","num_links":"4","num_links_columns":"1","linksImgSize":"XSmall","catCatalogMode":"0","catFeaturedItems":"1","catOrdering":"","catPagination":"2","catPaginationResults":"1","catTitle":"1","catTitleItemCounter":"1","catDescription":"1","catImage":"1","catFeedLink":"1","catFeedIcon":"1","subCategories":"1","subCatColumns":"2","subCatOrdering":"","subCatTitle":"1","subCatTitleItemCounter":"1","subCatDescription":"1","subCatImage":"1","itemImageXS":"","itemImageS":"","itemImageM":"","itemImageL":"","itemImageXL":"","catItemTitle":"1","catItemTitleLinked":"1","catItemFeaturedNotice":"0","catItemAuthor":"1","catItemDateCreated":"1","catItemRating":"0","catItemImage":"1","catItemIntroText":"1","catItemIntroTextWordLimit":"","catItemExtraFields":"0","catItemHits":"0","catItemCategory":"1","catItemTags":"1","catItemAttachments":"0","catItemAttachmentsCounter":"0","catItemVideo":"0","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"0","catItemImageGallery":"0","catItemDateModified":"0","catItemReadMore":"1","catItemCommentsAnchor":"1","catItemK2Plugins":"1","itemDateCreated":"1","itemTitle":"1","itemFeaturedNotice":"1","itemAuthor":"1","itemFontResizer":"1","itemPrintButton":"1","itemEmailButton":"1","itemSocialButton":"1","itemVideoAnchor":"1","itemImageGalleryAnchor":"1","itemCommentsAnchor":"1","itemRating":"1","itemImage":"1","itemImgSize":"Large","itemImageMainCaption":"1","itemImageMainCredits":"1","itemIntroText":"1","itemFullText":"1","itemExtraFields":"1","itemDateModified":"1","itemHits":"1","itemCategory":"1","itemTags":"1","itemAttachments":"1","itemAttachmentsCounter":"1","itemVideo":"1","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"0","itemVideoCaption":"1","itemVideoCredits":"1","itemImageGallery":"1","itemNavigation":"1","itemComments":"1","itemTwitterButton":"1","itemFacebookButton":"1","itemGooglePlusOneButton":"1","itemAuthorBlock":"1","itemAuthorImage":"1","itemAuthorDescription":"1","itemAuthorURL":"1","itemAuthorEmail":"0","itemAuthorLatest":"1","itemAuthorLatestLimit":"5","itemRelated":"1","itemRelatedLimit":"5","itemRelatedTitle":"1","itemRelatedCategory":"0","itemRelatedImageSize":"0","itemRelatedIntrotext":"0","itemRelatedFulltext":"0","itemRelatedAuthor":"0","itemRelatedMedia":"0","itemRelatedImageGallery":"0","itemK2Plugins":"1","catMetaDesc":"","catMetaKey":"","catMetaRobots":"","catMetaAuthor":""}";s:5:"trash";s:1:"0";s:7:"plugins";s:0:"";s:8:"language";s:1:"*";s:7:" * _tbl";s:16:"#__k2_categories";s:11:" * _tbl_key";s:2:"id";s:6:" * _db";O:15:"JDatabaseMySQLi":19:{s:4:"name";s:6:"mysqli";s:12:" * nameQuote";s:1:"`";s:11:" * nullDate";s:19:"0000-00-00 00:00:00";s:12:" * dbMinimum";s:5:"5.0.4";s:20:" JDatabase _database";s:17:"relial20_relialdb";s:13:" * connection";O:6:"mysqli":19:{s:13:"affected_rows";N;s:11:"client_info";N;s:14:"client_version";N;s:13:"connect_errno";N;s:13:"connect_error";N;s:5:"errno";N;s:5:"error";N;s:10:"error_list";N;s:11:"field_count";N;s:9:"host_info";N;s:4:"info";N;s:9:"insert_id";N;s:11:"server_info";N;s:14:"server_version";N;s:4:"stat";N;s:8:"sqlstate";N;s:16:"protocol_version";N;s:9:"thread_id";N;s:13:"warning_count";N;}s:8:" * count";i:0;s:9:" * cursor";b:0;s:8:" * debug";b:0;s:8:" * limit";i:0;s:6:" * log";a:0:{}s:9:" * offset";i:0;s:6:" * sql";s:69:"SELECT COUNT(*) FROM #__k2_comments WHERE itemID=517 AND published=1 ";s:14:" * tablePrefix";s:6:"yxvf5_";s:6:" * utf";b:1;s:11:" * errorNum";i:1146;s:11:" * errorMsg";s:133:"Table 'relial20_relialdb.yxvf5_k2_comments' doesn't exist SQL=SELECT COUNT(*) FROM yxvf5_k2_comments WHERE itemID=517 AND published=1";s:12:" * hasQuoted";b:0;s:9:" * quoted";a:0:{}}s:15:" * _trackAssets";b:0;s:9:" * _rules";N;s:10:" * _locked";b:0;s:10:" * _errors";a:0:{}s:4:"link";s:39:"/index.php/productos/archivo/actualidad";}s:4:"link";s:81:"/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016";s:9:"printLink";s:108:"/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016?tmpl=component&amp;print=1";s:11:"imageXSmall";s:61:"/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_XS.jpg";s:10:"imageSmall";s:60:"/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_S.jpg";s:11:"imageMedium";s:60:"/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_M.jpg";s:10:"imageLarge";s:60:"/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_L.jpg";s:11:"imageXLarge";s:61:"/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_XL.jpg";s:12:"imageGeneric";s:66:"/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_Generic.jpg";s:10:"cleanTitle";s:32:"Las elecciones peruanas del 2016";s:13:"numOfComments";N;}}