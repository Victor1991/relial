<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:51164:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId517"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Las elecciones peruanas del 2016
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_XS.jpg" alt="Las elecciones peruanas del 2016" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Los elecciones peruanas son, como suele ser por estos lares, un momento de polarización en el que se ponen en duda los cimientos mismos sobre los que está construido el sistema republicano. La joven democracia peruana -decimos joven porque recién volvió en el 2001- ha experimentado tres elecciones y han tenido algunas características: las segundas vueltas han supuesto, al menos en teoría, una situación de inflexión frente al modelo económico de mercado que desde los 90 impera. Además, la presencia de movimientos políticos y de figuras 'tradicionales' fue fundamental. Las elecciones que se desarrollan mientras redactamos esta nota, sin embargo, han quebrado varios de esos paradigmas. Aquí analizaremos los aspectos más relevantes sobre lo que viene sucediendo en el Perú.</p>
<p>&nbsp;</p>
<p><strong>Los fantasmas antidemocráticos</strong></p>
<p>&nbsp;</p>
<p>Las distintas opiniones -dentro del Perú, pero más aún de medios extranjeros- acerca de la exclusión de dos candidatos -Julio Guzmán y César Acuña- le han dado al asunto una apariencia antidemocrática. Se ha hablado, pues, de un posible fraude y, sobre todo, de artimañas políticas del establishment para bloquear a quienes representarían amenazas.</p>
<p>&nbsp;</p>
<p>La salida de Guzmán y Acuña poco tuvo que ver con maniobras políticas y más con una ley electoral pésimamente elaborada que sanciona con la exclusión errores procedimentales que podrían ser subsanables sin necesidad de medidas desproporcionadas.</p>
<p>&nbsp;</p>
<p>La decisión de las autoridades electorales no fue antidemocrática. Tampoco fue impulsada por intereses políticos subyacentes. El jurado electoral aplicó la ley, más allá de que sea esta -y en esto estamos de acuerdo- absurda.</p>
<p>&nbsp;</p>
<p>Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado.</p>
<p>&nbsp;</p>
<p><strong>El baile de la izquierda</strong></p>
<p>&nbsp;</p>
<p>Una vez que las candidaturas de Guzmán y Acuña fueron eliminadas, los votos que -de acuerdo con las encuestas- los favorecían migraron hacia la izquierda. La pregunta es: ¿por qué este bolsón de votos (de más o menos 25%) fue a parar a las opciones que plantean más Estado y menos mercado?</p>
<p>&nbsp;</p>
<p>A pesar de que muchos se han conformado con la idea de que una nueva izquierda se ha empezado a consolidar en el Perú, nosotros no nos sentimos cómodos con esa única explicación. Creemos que los factores que permitieron el movimiento de columpio que fortaleció a las opciones de izquierda (Verónika Mendoza, Alfredo Barnechea y Gregorio Santos) tuvo que ver con que el elector peruano promedio es hoy más joven que en los procesos previos.</p>
<p>&nbsp;</p>
<p>Meses antes de los comicios, las encuestas mostraban que lo que la juventud buscaba en su candidato era un rostro nuevo en política. Así, la caída de Guzmán y de Acuña abrió la puerta a que las alternativas de izquierda, también caras nuevas, hereden el voto de los excluidos.</p>
<p>&nbsp;</p>
<p>Si bien las tres opciones que hemos taxonomizado como izquierdistas planteaban la sustitución del mercado por actividad estatal en más de un ámbito, las alternativas eran distintas: Mendoza representó a una izquierda similar a la del Socialismo del Siglo XXI. Barnechea planteó una alternativa más mesurada de socialdemocracia. Santos, finalmente, propuso un socialismo radical y anti minero.</p>
<p>&nbsp;</p>
<p><strong>La caída de Mendoza</strong></p>
<p>&nbsp;</p>
<p>Durante buena parte del proceso, Fujimori se mantuvo firme en el primer lugar, con un tercio del electorado a su favor. La cuestión era quién iba por el segundo.</p>
<p>El crecimiento de Verónika Mendoza fue paulatino y bastante parejo en comparación al de Alfredo Barnechea. No obstante, en días, el carisma de Mendoza y su buena capacidad para comunicar ideas la llevaron a tomar la ventaja.</p>
<p>&nbsp;</p>
<p>Una seguidilla de errores políticos de Barnechea lo dejó -prácticamente- fuera de juego algunos días antes a los comicios. Así, Mendoza y el candidato liberal Pedro Pablo Kuczynski -PPK- llegaron a lo que las encuestadoras llamaron "empate técnico" en el segundo lugar. Ahora: los resultados de la primera vuelta muestran una ventaja de más de dos puntos del candidato liberal sobre la alternativa socialista. ¿Qué sucedió?</p>
<p>&nbsp;</p>
<p>Confluyeron tres cuestiones que generaron un "voto estratégico". Es decir: el abandono de ciertas preferencias previas por un voto que asegure la ausencia de Mendoza en la segunda vuelta. El temor que despertó Mendoza en varios sectores del electorado tuvo que ver con su vinculación con el chavismo bolivariano y el recuerdo del colapso económico que trajo el estatismo en el Perú décadas atrás. Sin embargo, a este primer motivo, correctamente sustentado en la realidad, se sumó una guerra sucia que buscó -injustamente- vincular a Mendoza con movimientos terroristas. Decimos que es injusto porque, a pesar de ser el Frente Amplio un movimiento abiertamente socialista, existe una diferencia irrenunciable entre el socialismo y el terrorismo.</p>
<p>&nbsp;</p>
<p>Así, entre verdades y mentiras, Mendoza perdió fuerza. Al día siguiente de su fracaso electoral, la Bolsa de Valores de Lima cerró la jornada con un crecimiento sin precedentes en los últimos años.</p>
<p><strong><br /></strong></p>
<p><strong>PPKeiko y el divorcio entre libertades</strong></p>
<p><strong><br /></strong></p>
<p>La salida de Mendoza dejó un panorama incómodo para quienes dieron su apoyo a los candidatos de izquierda. Esta vez tendrían que elegir entre dos opciones afines al libre mercado.</p>
<p>&nbsp;</p>
<p>Antes de la primera vuelta, cuando las encuestas mostraban ya una disputa importante entre PPK y Mendoza por el segundo lugar, muchos peruanos percibían ya una identidad casi absoluta entre Fuijmori y Kuczynski. Esta identidad dio pie a un ingenioso apodo para Kuczynski: PPKeiko.</p>
<p>&nbsp;</p>
<p>Este apodo echa relevantes luces el imaginario popular del elector peruano. Utilizar el término PPKeiko para equiparar a PPK con Keiko Fujimori revela la dificultad que existe para diferenciar a dos candidatos que tienen discrepancias fundamentales, pero que representan a la derecha, entendida esta como la opción afín a la libertad económica. Lo que no parecen percibir los electores -y esto es lo relevante- es el factor de libertad social que marca una profunda diferencia entre ambos candidatos.</p>
<p>&nbsp;</p>
<p>Fujimori ha basado su campaña en un supuesto deslinde con el gobierno de su padre -el "nuevo fujimorismo"- y un compromiso de respetar el orden democrático y los derechos humanos, una serie de declaraciones anteriores -que el juicio de Alberto Fujimori fue injusto y la posibilidad de indultarlo, por ejemplo- han despertado un justo recelo en quienes se mantienen vigilantes con la candidata.</p>
<p>&nbsp;</p>
<p>Por otro lado, Fujimori -a diferencia de PPK- se ha manifestado numerosas veces en contra de medidas liberales en cuestiones sociales, tales como la despenalización del aborto en casos de violación, la legalización de la venta de marihuana y la unión civil entre parejas del mismo sexo -ni qué decir de la posibilidad de matrimonios homosexuales-.</p>
<p>&nbsp;</p>
<p>PPK, en cambio, ha propuesto legalizar la marihuana como una estrategia para combatir narcotráfico y, en cuanto a la unión civil entre parejas homosexuales, ha dicho que apoyará a que la gente "viva como quiera y en paz".</p>
<p>&nbsp;</p>
<p>En el Perú, pues, parece existir una dicotomía entre libertad económica y libertad social. La dictadura de Alberto Fujimori -que representó la primera apertura del mercado desde 1963- estigmatizó la idea de "la derecha" como una opción necesariamente contradictoria con la libertad civil, la democracia y los derechos humanos. Y esta dicotomía ha llevado incluso a un llamado al voto viciado por parte de un relevante sector de la izquierda en la segunda vuelta. Porque -según dicen- cuando se vota por PPK o por Keiko se vota, en realidad, por la misma alternativa.</p>
<div style="position: absolute; left: -40px; top: -25px; width: 1px; height: 1px; overflow: hidden;" data-mce-bogus="1" class="mcePaste" id="_mcePaste"><!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<p class="MsoNormal" style="line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Las elecciones peruanas del 2016</span></b></p>
<p class="MsoNormal" style="line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Mijael Garrido Lecca Palacios </span></b></p>
<p class="MsoNormal" style="line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Ariana Lira Delcore</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Lima</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">@MijaelGLP</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">@arianalirad</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Los elecciones peruanas son, como suele ser por estos lares, un momento de polarización en el que se ponen en duda los cimientos mismos sobre los que está construido el sistema republicano. La joven democracia peruana -decimos joven porque recién volvió en el 2001- ha experimentado tres elecciones y han tenido algunas características: las segundas vueltas han supuesto, al menos en teoría, una situación de inflexión frente al modelo económico de mercado que desde los 90 impera. Además, la presencia de movimientos políticos y de figuras ‘tradicionales’ fue fundamental. Las elecciones que se desarrollan mientras redactamos esta nota, sin embargo, han quebrado varios de esos paradigmas. Aquí analizaremos los aspectos más relevantes sobre lo que viene sucediendo en el Perú.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Los fantasmas antidemocráticos</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Las distintas opiniones -dentro del Perú, pero más aún de medios extranjeros- acerca de la exclusión de dos candidatos -Julio Guzmán y César Acuña- le han dado al asunto una apariencia antidemocrática. Se ha hablado, pues, de un posible fraude y, sobre todo, de artimañas políticas del <i style="mso-bidi-font-style: normal;">establishment</i> para bloquear a quienes representarían amenazas.</span></p>
<p class="MsoNormal" style="line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; mso-fareast-font-family: 'Times New Roman'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">La salida de Guzmán y Acuña poco tuvo que ver con maniobras políticas y más con una ley electoral pésimamente elaborada que sanciona con la exclusión errores procedimentales que podrían ser subsanables sin necesidad de medidas desproporcionadas.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">La decisión de las autoridades electorales no fue antidemocrática. Tampoco fue impulsada por intereses políticos subyacentes. El jurado electoral aplicó la ley, más allá de que sea esta -y en esto estamos de acuerdo- absurda.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%; background: white;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif'; color: #222222;" lang="ES-TRAD">Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">El baile de la izquierda</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Una vez que las candidaturas de Guzmán y Acuña fueron eliminadas, los votos que -de acuerdo con las encuestas- los favorecían migraron hacia la izquierda. La pregunta es: ¿por qué este bolsón de votos (de más o menos 25%) fue a parar a las opciones que plantean más Estado y menos mercado? </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">A pesar de que muchos se han conformado con la idea de que una nueva izquierda se ha empezado a consolidar en el Perú, nosotros no nos sentimos cómodos con esa única explicación. Creemos que los factores que permitieron el movimiento de columpio que fortaleció a las opciones de izquierda (Verónika Mendoza, Alfredo Barnechea y Gregorio Santos) tuvo que ver con que el elector peruano promedio es hoy más joven que en los procesos previos. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Meses antes de los comicios, las encuestas mostraban que lo que la juventud buscaba en su candidato era un rostro nuevo en política. Así, la caída de Guzmán y de Acuña abrió la puerta a que las alternativas de izquierda, también caras nuevas, hereden el voto de los excluidos.<span style="mso-spacerun: yes;">&nbsp; </span></span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Si bien las tres opciones que hemos taxonomizado como izquierdistas planteaban la sustitución del mercado por actividad estatal en más de un ámbito, las alternativas eran distintas: Mendoza representó a una izquierda similar a la del Socialismo del Siglo XXI. Barnechea planteó una alternativa más mesurada de socialdemocracia. Santos, finalmente, propuso un socialismo radical y anti minero.</span></p>
<p class="MsoNormal" style="line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">La caída de Mendoza</span></b></p>
<p class="MsoNormal" style="line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Durante buena parte del proceso, Fujimori se mantuvo firme en el primer lugar, con un tercio del electorado a su favor. La cuestión era quién iba por el segundo. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">El crecimiento de Verónika Mendoza fue paulatino y bastante parejo en comparación al de Alfredo Barnechea. No obstante, en días, el carisma de Mendoza y su buena capacidad para comunicar ideas la llevaron a tomar la ventaja. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Una seguidilla de errores políticos de Barnechea lo dejó -prácticamente- fuera de juego algunos días antes a los comicios. Así, Mendoza y el candidato liberal Pedro Pablo Kuczynski -PPK- llegaron a lo que las encuestadoras llamaron “empate técnico” en el segundo lugar. Ahora: los resultados de la primera vuelta muestran una ventaja de más de dos puntos del candidato liberal sobre la alternativa socialista. ¿Qué sucedió? </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Confluyeron tres cuestiones que generaron un “voto estratégico”. Es decir: el abandono de ciertas preferencias previas por un voto que asegure la ausencia de Mendoza en la segunda vuelta. El temor que despertó Mendoza en varios sectores del electorado tuvo que ver con su vinculación con el chavismo bolivariano y el recuerdo del colapso económico que trajo el estatismo en el Perú décadas atrás. Sin embargo, a este primer motivo, correctamente sustentado en la realidad, se sumó una guerra sucia que buscó -injustamente- vincular a Mendoza con movimientos terroristas. Decimos que es injusto porque, a pesar de ser el Frente Amplio un movimiento abiertamente socialista, existe una diferencia irrenunciable entre el socialismo y el terrorismo. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Así, entre verdades y mentiras, Mendoza perdió fuerza. Al día siguiente de su fracaso electoral, la Bolsa de Valores de Lima cerró la jornada con un crecimiento sin precedentes en los últimos años. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><b style="mso-bidi-font-weight: normal;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">PPKeiko y el divorcio entre libertades</span></b></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">La salida de Mendoza dejó un panorama incómodo para quienes dieron su apoyo a los candidatos de izquierda. Esta vez tendrían que elegir entre dos opciones afines al libre mercado.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Antes de la primera vuelta, cuando las encuestas mostraban ya una disputa importante entre PPK y Mendoza por el segundo lugar, muchos peruanos percibían ya una identidad casi absoluta entre Fuijmori y Kuczynski. Esta identidad dio pie a un ingenioso apodo para Kuczynski: PPKeiko. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Este apodo echa relevantes luces el imaginario popular del elector peruano. Utilizar el término PPKeiko para equiparar a PPK con Keiko Fujimori revela la dificultad que existe para diferenciar a dos candidatos que tienen discrepancias fundamentales, pero que representan a la derecha, entendida esta como la opción afín a la libertad económica. Lo que no parecen percibir los electores -y esto es lo relevante- es el factor de libertad social que marca una profunda diferencia entre ambos candidatos.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Fujimori ha basado su campaña en un supuesto deslinde con el gobierno de su padre -el “nuevo fujimorismo”- y un compromiso de respetar el orden democrático y los derechos humanos, una serie de declaraciones anteriores -que el juicio de Alberto Fujimori fue injusto y la posibilidad de indultarlo, por ejemplo- han despertado un justo recelo en quienes se mantienen vigilantes con la candidata. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">Por otro lado, Fujimori -a diferencia de PPK- se ha manifestado numerosas veces en contra de medidas liberales en cuestiones sociales, tales como la despenalización del aborto en casos de violación, la legalización de la venta de marihuana y la unión civil entre parejas del mismo sexo -ni qué decir de la posibilidad de matrimonios homosexuales-.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">PPK, en cambio, ha propuesto legalizar la marihuana como una estrategia para combatir narcotráfico y, en cuanto a la unión civil entre parejas homosexuales, ha dicho que apoyará a que la gente “viva como quiera y en paz”.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; line-height: 150%;"><span style="font-size: 10.0pt; line-height: 150%; font-family: 'Arial','sans-serif';" lang="ES-TRAD">En el Perú, pues, parece existir una dicotomía entre libertad económica y libertad social. La dictadura de Alberto Fujimori -que representó la primera apertura del mercado desde 1963- estigmatizó la idea de “la derecha” como una opción necesariamente contradictoria con la libertad civil, la democracia y los derechos humanos. Y esta dicotomía ha llevado incluso a un llamado al voto viciado por parte de un relevante sector de la izquierda en la segunda vuelta. Porque -según dicen- cuando se vota por PPK o por Keiko se vota, en realidad, por la misma alternativa. </span></p>
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:TrackMoves/>
  <w:TrackFormatting/>
  <w:DoNotShowComments/>
  <w:DoNotShowPropertyChanges/>
  <w:HyphenationZone>21</w:HyphenationZone>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>ES-TRAD</w:LidThemeOther>
  <w:LidThemeAsian>JA</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]--><!--[if gte mso 10]>

<![endif]--></div>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
			&laquo; “No hay comida”: el fantasma del hambre se apodera de Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
			Olas de Cambio - tercer número de la Mirada Liberal &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016#startOfPageId517">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:41:"Las elecciones peruanas del 2016 - Relial";s:11:"description";s:154:"&quot;Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado&quot; &amp;nbsp; Lima, e...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:32:"Las elecciones peruanas del 2016";s:6:"og:url";s:98:"http://relial.org/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016";s:8:"og:title";s:41:"Las elecciones peruanas del 2016 - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_S.jpg";s:14:"og:description";s:166:"&amp;quot;Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado&amp;quot; &amp;amp;nbsp; Lima, e...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:32:"Las elecciones peruanas del 2016";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}