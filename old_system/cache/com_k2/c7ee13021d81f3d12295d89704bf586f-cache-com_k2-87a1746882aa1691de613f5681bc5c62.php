<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16477:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Opinión</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana">
	  		El porqué de la crisis venezolana	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana" title="El porqu&eacute; de la crisis venezolana">
		    	<img src="/media/k2/items/cache/8462850dbb62c4bd159ee1ad55df6950_XS.jpg" alt="El porqu&eacute; de la crisis venezolana" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Luis Pazos</p>
<p>&nbsp;</p>
<p>La inflación y la devaluación que sufren actualmente los venezolanos, de las más altas del mundo, son parecidas a las vividas en México en la década de los 80. La inflación mexicana anualizada llegó al 117% en marzo de 1983. Esa inflación de tres dígitos fue debida a un déficit presupuestal gubernamental del 15.8% del PIB en 1982.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/354-chile-¿para-qué-imitar-a-venezuela-cuando-se-puede-emular-a-suiza?">
	  		Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza?	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/354-chile-¿para-qué-imitar-a-venezuela-cuando-se-puede-emular-a-suiza?" title="Chile: &iquest;para qu&eacute; imitar a Venezuela cuando se puede emular a Suiza?">
		    	<img src="/media/k2/items/cache/7081cca2f9cd0c06f2cce9e93d01dda9_XS.jpg" alt="Chile: &iquest;para qu&eacute; imitar a Venezuela cuando se puede emular a Suiza?" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>La presidente chilena Michelle Bachelet quiere reducir la desigualdad. Me sospecho que se refiere a la desigualdad de resultados, que es la que mide el coeficiente Gini. Pero es posible que en su afán nivelador acabe desplumando a la gallina de los huevos de oro.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/354-chile-¿para-qué-imitar-a-venezuela-cuando-se-puede-emular-a-suiza?">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios">
	  		El secreto de los estados totalitarios	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios" title="El secreto de los estados totalitarios">
		    	<img src="/media/k2/items/cache/f9a9b7c9f33a923e5475b478a62125ae_XS.jpg" alt="El secreto de los estados totalitarios" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>¿Cuál es la pieza clave en la construcción de la jaula totalitaria? Sencillo: la eliminación real de la separación de poderes, aunque se mantenga la fantasía formal de que continúa existiendo.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú">
	  		Unión civil y libertad en el Perú	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú" title="Uni&oacute;n civil y libertad en el Per&uacute;">
		    	<img src="/media/k2/items/cache/f41bf091a4e18f2312495cc0e975d9f7_XS.jpg" alt="Uni&oacute;n civil y libertad en el Per&uacute;" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinón de Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>Antes de escribir sobre la propuesta de la unión civil entre personas de la misma preferencia sexual en el Perú, considero pertinente expresar, lo más claramente que pueda, mi posición sobre estos vínculos. Creo que el Estado no debe intervenir, de ningún modo, en la unión entre dos personas para convivir. El Estado no debe sancionar ni entrometerse en la relación privada más íntima de dos seres humanos, como no debe hacerlo cuando compran una casa, adquieren un seguro, o deciden en qué colegio deben estudiar sus hijos. Por ende, a mi entender, la tarea correcta que un liberal debería acometer al respecto es la de luchar por privatizar las uniones civiles entre dos personas, del mismo modo que debería combatir por la privatización de la educación pública o de los servicios médicos a cargo del Estado.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina">
	  		Dónde se vive mejor o peor en América Latina	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina" title="D&oacute;nde se vive mejor o peor en Am&eacute;rica Latina">
		    	<img src="/media/k2/items/cache/fe4bbe81600a40063594e597e00eb05b_XS.jpg" alt="D&oacute;nde se vive mejor o peor en Am&eacute;rica Latina" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>¿Cuál es el mejor vividero de América Latina? Costa Rica. ¿Cuál es el peor? Cuba. Y hay una gran distancia entre ambos países.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina">
	  		La verdad de Hilda Molina	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina" title="La verdad de Hilda Molina">
		    	<img src="/media/k2/items/cache/ee760d1c1474fde7f8f71a96b2452e29_XS.jpg" alt="La verdad de Hilda Molina" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Por Héctor Ñaupari*</p>
<p>&nbsp;</p>
<p>Hace unos días presentamos en Lima el libro Mi verdad de la doctora Hilda Molina, ante una multitudinaria audiencia, junto a los librepensadores Álvaro Vargas Llosa y Carlos Alberto Montaner, dilectos amigos, en una magnífica cobertura organizada por la notable Yesenia Álvarez y su Instituto Político para la Libertad.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/opinion?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/opinion?start=42" class="pagenav">Anterior</a></li><li><a title="4" href="/index.php/productos/archivo/opinion?start=18" class="pagenav">4</a></li><li><a title="5" href="/index.php/productos/archivo/opinion?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/opinion?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/opinion?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/opinion?start=42" class="pagenav">8</a></li><li><span class="pagenav">9</span></li><li><a title="10" href="/index.php/productos/archivo/opinion?start=54" class="pagenav">10</a></li><li><a title="11" href="/index.php/productos/archivo/opinion?start=60" class="pagenav">11</a></li><li><a title="12" href="/index.php/productos/archivo/opinion?start=66" class="pagenav">12</a></li><li><a title="13" href="/index.php/productos/archivo/opinion?start=72" class="pagenav">13</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/opinion?start=54" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/opinion?start=84" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 9 de 15	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:17:"Opinión - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:62:"http://relial.org/index.php/productos/archivo/opinion?start=48";s:8:"og:title";s:17:"Opinión - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:43:"http://relial.org/media/k2/categories/2.jpg";s:5:"image";s:43:"http://relial.org/media/k2/categories/2.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}}s:6:"module";a:0:{}}