<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8138:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId442"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/acaf69a47291143b36612c5b483fc572_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/acaf69a47291143b36612c5b483fc572_XS.jpg" alt="ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><span style="font-size: 10pt;">Carlos Amel Oliva Torres, líder del Frente Juvenil Unión Patriótica de Cuba (UNPACU), fue impedido hoy de tomar un vuelo con destino a Chile y con escala en Panamá, que salía a las 12:38 pm desde La Habana. El joven, en el marco del proyecto Aulas Abiertas, tenía planeado viajar para participar en el Seminario Internacional "De las Ideas a la Acción, Comenzando un Centro de Estudios" organizado por la organización chilena Libertad y Desarrollo (LyD).</span></p>
<p><span style="font-size: 10pt;">	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">Oliva Torres es un activista que desde hace algunos años lucha pacíficamente por el establecimiento de la democracia en su país. Él cuenta que fue en la Oficina de Migración del Aeropuerto Internacional José Martí (La Habana) donde se acercó un señor con el uniforme de la Aduana, tomó sus documentos y le pidió que lo acompañe. Este señor entró a una oficina y el líder de UNPACU esperó afuera. Después de unos minutos, una joven le informó, sin darle razón alguna, que no podría viajar.</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">Vía telefónica Oliva nos expresa que la negativa está relacionada con un evento anterior. Hace una semana, miembros de la Seguridad del Estado lo citaron en una estación de policía para proponerle que trabaje para ellos informándoles todos los detalles de organizaciones en el exterior que vienen apoyando a la oposición. "Yo me niego y ellos me dicen que no me van a dejar viajar. Yo les contesté que esto no representaba nada en mi determinación, que yo voy a seguir luchando y no voy a hacer ningún trato con ellos"- precisó el líder juvenil de UNPACU.</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">Además, Oliva asegura que hoy un oficial de la Policía Política, quien se hace llamar Julio Fonseca, se acercó a su casa en Santiago de Cuba para amenazar a su familia y en particular a su esposa, quien está embarazada. El Oficial buscaba interrogarle por la edición de un audiovisual. El activista de UNPACU, muy preocupado expresa que "responsabiliza a este Oficial de la Policía Política sobre cualquier cosa que le pase a su esposa y a su futuro hijo".</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">Carlos Amel Oliva, se encuentra en estos momentos preocupado por su situación y la amenza que existe contra él y su familia, pide a la Comunidad Internacional que lo ayuden a denunciar esta violación a su libertad personal y a su libertad de tránsito, derecho universalmente reconocido y el cual están violando actualmente las autoridades cubanas impidiéndole arbitrariamente salir de Cuba sin brindarle motivo legal alguno, pues este líder de la sociedad civil ha cumplido todos los requisitos de viaje y obtenido las visas respectivas para viajar de su país al exterior.</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">Yusmila Reyna, periodista independiente, desde su cuenta de Twitter en Cuba (@YusmilaReyna) alertó de la situación de Carlos Amel Oliva con los siguientes mensajes: "Impiden viajar a evento en Chile a lider juvenil ‪#‎UNPACU‬‬‬‬‬ Carlos Amel Oliva Torres" y "#Habana: Es tanto temor Dictadura q sin explicacion Aduana Aereopuerto impiden viajar @amelunpacu1"</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">El Instituto Político para la Libertad (IPL Perú) exhorta a la Comunidad Internacional a sumarse a defender la libertad personal y de tránsito de este ciudadano cubano, así como exigir a las autoridades cubanas a brindar la justificación legal del impedimento de viaje.</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">INSTITUTO POLÍTICO PARA LA LIBERTAD – IPL PERÚ</span></p>
<p><span style="font-size: 10pt;">www.iplperu.org</span></p>
<p><span style="font-size: 10pt;">@iplperu</span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/423-proyecto-de-declaración-de-relial-sobre-la-situación-de-los-derechos-humanos-en-nicaragüa">
			&laquo; Declaración de RELIAL sobre la situación de los Derechos Humanos en Nicaragua		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/446-relial-por-el-ex-presidente-flores">
			RELIAL por el ex presidente Flores &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/442-alerta-activista-carlos-amel-oliva-fue-impedido-de-salir-de-cuba#startOfPageId442">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:75:"ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba - Relial";s:11:"description";s:156:"Carlos Amel Oliva Torres, líder del Frente Juvenil Unión Patriótica de Cuba (UNPACU), fue impedido hoy de tomar un vuelo con destino a Chile y con esc...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:66:"ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba";s:6:"og:url";s:131:"http://relial.org/index.php/productos/archivo/comunicados/item/442-alerta-activista-carlos-amel-oliva-fue-impedido-de-salir-de-cuba";s:8:"og:title";s:75:"ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/acaf69a47291143b36612c5b483fc572_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/acaf69a47291143b36612c5b483fc572_S.jpg";s:14:"og:description";s:156:"Carlos Amel Oliva Torres, líder del Frente Juvenil Unión Patriótica de Cuba (UNPACU), fue impedido hoy de tomar un vuelo con destino a Chile y con esc...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:66:"ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}