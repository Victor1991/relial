<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7433:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId399"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La fuerte oposición que Santos enfrentará en su segundo tiempo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/5b98a51d844cf083418c7193dcee292b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/5b98a51d844cf083418c7193dcee292b_XS.jpg" alt="La fuerte oposici&oacute;n que Santos enfrentar&aacute; en su segundo tiempo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Andrés Molano</p>
<p>&nbsp;</p>
<p>Luego de los comicios del domingo, la política colombiana comenzó a rearmarse, quedando en evidencia el polarizado panorama que deberá enfrentar el reelecto Presidente Juan Manuel Santos en el Congreso. Porque desde la derecha tendrá al Centro Democrático, liderado por el ex Presidente Alvaro Uribe (2002-2010), y por el lado de la izquierda, estarán el Polo Democrático y la Alianza Verde.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Y es el tono que ha usado el ex mandatario, tras conocer los resultados electorales, el que dio luces de la dura oposición que dará en el Senado. Así, calificó la elección como "la más corrupta de la historia".</p>
<p>&nbsp;</p>
<p>Con este escenario, el segundo mandato parece muy distinto al que se inició en 2010, cuando Santos contaba con un 85% del Congreso a su favor. En la actualidad, ese respaldo no supera el 55%. "La llamada aplanadora de la Unidad Nacional no será garantía para impulsar un paquete de reformas anunciadas en el discurso de la noche del 15 de junio", señaló la revista Semana.</p>
<p>&nbsp;</p>
<p>El punto álgido de la oposición uribista -que, según el resultado de las presidenciales, cuenta con el respaldo del 45% del electorado, que fue lo obtenido por su abanderado Oscar Zuluaga- serán las negociaciones de paz con las Farc. Para el ex Presidente Uribe y sus adherentes, el actual gobierno ha realizado muchas concesiones, ya que los diálogos avanzan sin que exista un compromiso de cese al fuego por parte de las Farc. También se oponen a que los miembros de la guerrilla puedan tener representación en el Congreso.</p>
<p>&nbsp;</p>
<p>"El Presidente Santos tiene un margen amplio de gobernabilidad con relación al proceso de paz, pero tendrá que incorporar en la ecuación a una oposición distinta, que es de derecha. Entonces será necesario construir canales de comunicación", dijo a La Tercera el analista de la Universidad del Rosario, Andrés Molano. Según el diario El Espectador, el uribista Centro Democrático ya está trabajando para conformar la agenda de oposición. El senador electo Alfredo Rangel anunció que realizarán un debate sobre los diálogos de paz. "Tenemos que exigir que haya transparencia, porque no se conoce la situación real", dijo Rangel, quien abrió la posibilidad de que miembros del equipo negociador asistan en su calidad de funcionarios del gobierno. En este sentido, los analistas advierten que ahora está a prueba la capacidad de Uribe para llegar a acuerdos con otras fuerzas políticas, con el fin de conformar un bloque mayor de oposición. En ese sentido, la revista bogotana Semana señala que Uribe podría consolidar acuerdos con movimientos como el Partido Conservador y Opción Ciudadana. Los primeros fueron sus principales aliados en los ocho años de su gobierno.</p>
<p>&nbsp;</p>
<p>Respecto de la oposición que tendrá Santos por el lado de la izquierda, la ex candidata Clara López dijo en una entrevista al diario El Tiempo que "se dedicará a pedirles a los colombianos que refrenden los acuerdos de La Habana". Además, añadió que harán una oposición "analítica, no intransigente", en otros temas.</p>
<p>&nbsp;</p>
<p>Mientras, el vicepresidente de la Alianza Verde, Antonio Sanguino, dijo que si bien sus congresistas apoyaron a Santos por el proceso de paz, no asumirán compromisos con el gobierno y mantendrán su independencia.</p>
<p>&nbsp;</p>
<p>Foto: ICP Colombia</p>
<p>Fuente: La tercera</p>
<p>Fuente: ICP Colombia</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana">
			&laquo; Política local y participación ciudadana		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/400-política-local-y-participación-ciudadana">
			Política local y participación ciudadana &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/399-la-fuerte-oposición-que-santos-enfrentará-en-su-segundo-tiempo#startOfPageId399">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:73:"La fuerte oposición que Santos enfrentará en su segundo tiempo - Relial";s:11:"description";s:157:"En la opinión de Andrés Molano &amp;nbsp; Luego de los comicios del domingo, la política colombiana comenzó a rearmarse, quedando en evidencia el pola...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:64:"La fuerte oposición que Santos enfrentará en su segundo tiempo";s:6:"og:url";s:131:"http://www.relial.org/index.php/productos/archivo/opinion/item/399-la-fuerte-oposición-que-santos-enfrentará-en-su-segundo-tiempo";s:8:"og:title";s:73:"La fuerte oposición que Santos enfrentará en su segundo tiempo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/5b98a51d844cf083418c7193dcee292b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/5b98a51d844cf083418c7193dcee292b_S.jpg";s:14:"og:description";s:161:"En la opinión de Andrés Molano &amp;amp;nbsp; Luego de los comicios del domingo, la política colombiana comenzó a rearmarse, quedando en evidencia el pola...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:64:"La fuerte oposición que Santos enfrentará en su segundo tiempo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}