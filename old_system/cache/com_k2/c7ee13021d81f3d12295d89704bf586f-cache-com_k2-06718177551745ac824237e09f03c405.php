<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10083:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId225"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7a6409a35f8223f856dc99651cb33cb1_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7a6409a35f8223f856dc99651cb33cb1_XS.jpg" alt="Conferencia Internacional sobre &ldquo;El Acuerdo de Asociaci&oacute;n  entre Centro Am&eacute;rica y la Uni&oacute;n Europea&rdquo;, &rdquo;Los mercados  de la Aeron&aacute;utica Civil y de la Energ&iacute;a El&eacute;ctrica&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El pasado 5 de noviembre se realizó esta conferencia organizada exitosamente por la Fundación Friedrich Naumann para la Libertad y la Asociación Nacional de Fomento Económico de Costa Rica (ANFE). El objetivo fue discutir los alcances del Acuerdo de Asociación entre Centro América y la Unión Europea desde la perspectiva de algunos países centroamericanos y la necesidad de abrir los mercados aéreos y eléctricos en esta región.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Participaron destacados representantes de think tanks centroamericanos como David Casasola, Investigador Asociado del Centro de Investigaciones Económicas Nacionales de Guatemala, Surse Pierpoint, Presidente de la Fundación Libertad de Panamá y Juan Ricardo Fernández, Presidente de la Asociación de Consumidores Libres de Costa Rica. Luisa Pastor, Directora de la Organización de Comercio e Inversiones del Reino Unido de la Embajada Británica en San José, representó la participación de la Unión Europea y Marielos Alfaro, Diputada, del Partido Movimiento Libertario de Costa Rica miembro de la Comisión de Asuntos Internacionales de la Asamblea Legislativa de Costa Rica.<img style="float: right;" src="images/Foto_chica.jpg" alt="Foto chica" width="318" height="250" /></p>
<p>&nbsp;</p>
<p>El sector privado regional estuvo representado por Fernando García, Director Ejecutivo de la Asociación Nacional de Industriales de Honduras; Andrés Pozuelo, Presidente de Jack's de Centroamérica y el Ingeniero Mario Alvarado, Director Ejecutivo de la Asociación Costarricense de Productores de Electricidad, ACOPE. Ana Victoria Velázquez de la Comisión para Promover la Competencia representó al Gobierno de Costa Rica.</p>
<p><img style="float: left;" src="images/Foto2_chica.png" alt="Foto2 chica" width="330" height="250" /></p>
<p>&nbsp;</p>
<p>En el marco de la discusión del Acuerdo de Asociación con la Unión Europea, David Casasola opinó que tiene ventajas en el sentido de abrir nuevos mercados y una variedad de destinos importante para los bienes centroamericanos además de promover el intercambio de capital humano, sin embargo no es un acuerdo que traerá cambios drásticos de la trayectoria del desarrollo de los países. La Diputada Alfaro comentó que el acuerdo tiene ventajas pero no tantas como se cree pues no es un tratado comercial de libre intercambio de productos sino que mantiene por parte de Europa algunas medidas proteccionistas principalmente de productos agropecuarios. También dijo la parlamentaria que es muy importante que los países de centroamérica actualicen la modernización de la infraestructura logística para elevar el nivel y la calidad de la competitividad de tal manera que se pueda incrementar la inversión directa, la producción y la generación de empleo.</p>
<p>&nbsp;</p>
<p>Luisa Pastor de la Embajada de Inglaterra en San José, explicó que el tratado consta de 363 artículos divididos en 5 partes –Dialo go Político (Artículos 12-23), –Cooperación (Artículos 24- 76) –Comercio (Artículos 77-351), 21 anexos y12 declaraciones conjuntas. Según las proye cciones de la UE Centroamérica incrementará el comercio con el viejo continente, los salarios aumentarán en promedio, la pobreza se reducirá, se fortalecerá considerablemente la sociedad civil y se estimulará el uso de energías renovables y el desarrollo sostenible. Algunos de los expertos asistentes no están tan de acuerdo en esas proyecciones porque todavía algunos países por no decir todos, no han hecho la tarea de mejorar los servicios básicos como la educación, la salud y la infraestructura que son fundamentales para que las personas aprovechen los beneficios.</p>
<p>&nbsp;</p>
<p>Por su parte, con relación a la apertura de los mercados, todos coinciden en la necesidad de promover mercados abiertos a la competencia en los servicios de transporte aéreo y en la generación de energía eléctrica para bajar el costo de las tarifas, sobre todo, el alto costo de la electricidad en Costa Rica, por ejemplo, está incidiendo en una reducción del nivel de competitividad de la industria local, lo que incluso está provocando despidos de personal y hasta traslados de empresas a otros países quizás con mano de obra menos calificada que en Costa Rica pero con tarifas eléctricas más competitivas.</p>
<p><img src="images/foto3_chicaq.png" alt="foto3 chicaq" width="330" height="250" style="float: right;" /></p>
<p>&nbsp;</p>
<p>Los gobiernos centroamericanos y los sectores productivos tienen todavía que tomar decisiones muy importantes para mejorar la competitividad de los países, para mejorar la calificación de la mano de obra, la eficiencia en los métodos de producción y sobre todo en la eliminación de regulaciones y obstáculos que impiden un desarrollo más dinámico de la economía. La mayoría de los países de la región ocupan lugares negativos en el ranking mundial de libertad económica y es necesario mejorar esos indicadores para hacer posible que el libre comercio dé los resultados esperados. Si bien es cierto, los sindicatos han representado un obstáculo para implementar algunas reformas, esto no debería ser la razón para desacelerar la dinámica de las políticas orientadas a mejorar el ambiente de negocios, la atracción de inversiones, la generación de empleo y la mejoría radical en la calidad de la infraestructura.</p>
<p>&nbsp;</p>
<p>Los nuevos gobiernos que se elijan en la región a partir de este y el próximo año deben tener en su rango de prioridad la profundización de las reformas para mejorar la libertad económica.</p>
<p>&nbsp;</p>
<p>Se adjuntan los anexos de las conferencias proyectadas por los panelistas.</p>
<p>&nbsp;</p>
<p>fuente: Mario Brenes</p>
<p>Fotos: ANFE</p>
<p>Texto: Proporcionado amablemente por Mario Brenes</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/221-relial-visita-la-ufm">
			&laquo; RELIAL visita la UFM		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo">
			Dora de Ampuero, condecorada al Mérito Educativo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”#startOfPageId225">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:189:"Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica” - Relial";s:11:"description";s:156:"El pasado 5 de noviembre se realizó esta conferencia organizada exitosamente por la Fundación Friedrich Naumann para la Libertad y la Asociación Nacio...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:180:"Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica”";s:6:"og:url";s:247:"http://www.relial.org/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”";s:8:"og:title";s:189:"Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/7a6409a35f8223f856dc99651cb33cb1_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/7a6409a35f8223f856dc99651cb33cb1_S.jpg";s:14:"og:description";s:156:"El pasado 5 de noviembre se realizó esta conferencia organizada exitosamente por la Fundación Friedrich Naumann para la Libertad y la Asociación Nacio...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:180:"Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}