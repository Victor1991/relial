<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8963:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId244"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El fascismo y su irritante vitalidad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/d0d354668f69293e040aa69de3140c78_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/d0d354668f69293e040aa69de3140c78_XS.jpg" alt="El fascismo y su irritante vitalidad" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Umberto Eco</p>
<p>&nbsp;</p>
<p>Los seres humanos son animales que, hasta el hartazgo, hacen lo posible por despertar dudas en torno a su inteligencia. Esa concepción que presenta al hombre como una criatura superior, cuyo rango se ampararía fundamentalmente en la razón, admite refutaciones en todas partes del mundo. Un argumento que permite sustentar esta opinión es la reiteración del error. Acontece que, mientras otras especies no suelen cometer la misma equivocación, incontables semejantes se sienten a gusto con cualquier clase de reincidencia. En estos casos, ni siquiera las experiencias menos aguantables, tanto propias como ajenas, consiguen que la estupidez jamás se repita. Bajo ese entendimiento, la reproducción de una falta o un despropósito no es cuestionable, puesto que, aunque nada lo demuestre, pueden todavía cambiarse sus consecuencias. En política, por ejemplo, se tiene la creencia de que, si una ocurrencia engendró las peores monstruosidades, esto no conlleva su nulidad. Pueden exponerse datos acerca de masacres, suplicios, perversidades y demás oprobios; empero, nunca faltará quien apueste aún por insistir en ese desvarío. Acepto que es factible moderar algunas insensateces, evitando asociaciones inmediatas, mas ello no evita que notemos la misma imbecilidad de antaño.</p>
<p>&nbsp;</p>
<p>El fascismo puede concebirse como actitud, doctrina o movimiento. Partiendo de la última opción, esa palabra serviría para denominar al grupo de mortales que conquistaron el poder en Italia, ejerciéndolo durante algunas décadas del siglo XX. Según esta perspectiva, sólo en ese contexto puede hablarse de fascistas, aficionados, simpatizantes y, desde luego, Benito Mussolini, quien, antes de ser su líder, fue miembro del Partido Socialista. Cabe apuntar que, en esa misma época, encontramos una versión alemana, racista pero de similar vileza, encabezada por Adolf Hitler. Pese a lo anterior, quizá con el anhelo de reproducir una era sombría, hay actualmente facciones que se declaran fascistas. Todos los símbolos, rituales e idioteces partidarias se mantienen vivos gracias a esa gente que, si tuviese prerrogativas gubernamentales, no vacilaría en repetir hecatombes. No se ignora que, como lo sostuvo Jean-François Revel hace varios años, esos extremistas sean minoritarios, careciendo del poderío de su pandilla inspiradora. Es absurdo imaginar su reconquista de una parte importante del planeta. Sin embargo, no debemos mirar con indiferencia a los que ansían la pulverización de nuestro sosiego. El desdén por esos delirios puede incrementar su fuerza entre los ciudadanos.</p>
<p>&nbsp;</p>
<p>Por otro lado, tal como lo precisé, hallamos también una doctrina que puede considerarse fascista, cuya vigencia es desgraciadamente discutible. Sus principales postulados fueron expuestos por Mussolini en la Enciclopedia Traccani, el año 1932, ocasión en que se apeló a reflexiones de Giovanni Gentile. Existen asimismo discursos que, con claridad, revelan cuáles son los principios de esa ideología. Sin desconocer esta base, esa corriente puede ser identificada merced a un conjunto de caracteres que, en resumen, alimentan el totalitarismo. Al respecto, destaco el sometimiento irrestricto a los dictados del gobernante, la pretensión de regular toda nuestra vida, un nacionalismo extremo, el endiosamiento del Estado, la repulsión por lo moderno, así como una evidente aversión a la democracia. Salvo quienes sienten predilección por las mentiras del gobernante, debe reconocerse que aquellos rasgos pueden ser percibidos en muchos regímenes. El debate no versaría sobre su presencia; correspondería polemizar acerca del grado al cual hayan llegado las autoridades de un país. Ésa es la realidad que tiene como gestores a varios de nuestros contemporáneos. Ellos pugnan por llevar el estandarte del retroceso.</p>
<p>&nbsp;</p>
<p>Finalmente, cuando se habla de fascismo, es admisible pensar en una cuestión psicológica. Así, un individuo puede ser tildado de fascista si se inclina por los abusos, las imposiciones personales y, en suma, el gusto por la intolerancia. No revelo nada nuevo al denunciar que esas actitudes son comunes en más de una sociedad. Es un tema de índole cultural que debe ser afrontado con la mayor seriedad posible. Abrigando el deseo de tener una convivencia pacífica, en la cual nuestras relaciones no sean sometidas al tormento del conflicto causado por quienes definen al prójimo como medio para satisfacer sus antojos, es necesario cambiar esa situación. Cada uno es libre de amargar su existencia, al igual que adoptar gestos y razonamientos ilógicos. Lo que no se debe consentir es limitarse a contemplar esa patente manifestación de necedad, pues su vigencia nos perjudicará. El objetivo no es disciplinar al individuo para que venere un sistema, sino ayudarlo a evitarnos la repetición de martirios. En este afán, la crítica es un recurso que puede ser explotado con generosidad.</p>
<p>&nbsp;</p>
<p>Autor: Enrique Fernández García,&nbsp;Escritor, filósofo y abogado,</p>
<p><span id="cloak14181">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak14181').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy14181 = 'c&#97;&#105;d&#111;d&#101;lt&#105;&#101;mp&#111;' + '&#64;';
 addy14181 = addy14181 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak14181').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy14181 + '\'>' + addy14181+'<\/a>';
 //-->
 </script></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/244-el-fascismo-y-su-irritante-vitalidad#startOfPageId244">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:45:"El fascismo y su irritante vitalidad - Relial";s:11:"description";s:153:"Umberto Eco &amp;nbsp; Los seres humanos son animales que, hasta el hartazgo, hacen lo posible por despertar dudas en torno a su inteligencia. Esa con...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:36:"El fascismo y su irritante vitalidad";s:6:"og:url";s:84:"http://relial.org/index.php/biblioteca/item/244-el-fascismo-y-su-irritante-vitalidad";s:8:"og:title";s:45:"El fascismo y su irritante vitalidad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/d0d354668f69293e040aa69de3140c78_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/d0d354668f69293e040aa69de3140c78_S.jpg";s:14:"og:description";s:157:"Umberto Eco &amp;amp;nbsp; Los seres humanos son animales que, hasta el hartazgo, hacen lo posible por despertar dudas en torno a su inteligencia. Esa con...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:33:"Ensayos y reflexiones académicas";s:4:"link";s:76:"/index.php/biblioteca/itemlist/category/22-ensayos-y-reflexiones-académicas";}i:2;O:8:"stdClass":2:{s:4:"name";s:36:"El fascismo y su irritante vitalidad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}