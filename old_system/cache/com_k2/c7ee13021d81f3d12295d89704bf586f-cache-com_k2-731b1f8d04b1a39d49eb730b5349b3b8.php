<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11401:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId342"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Unidos para un éxito mayor con la “Alianza para Centro América”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/49d07d2f2b048709fab28e0845347114_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/49d07d2f2b048709fab28e0845347114_XS.jpg" alt="Unidos para un &eacute;xito mayor con la &ldquo;Alianza para Centro Am&eacute;rica&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de David Henneberger y Elisabeth Maigler</p>
<p>&nbsp;</p>
<p>Un hecho sin precedentes: los partidos liberales en Centro América se unen en una alianza estratégica con el objetivo de desarrollar y defender posicionamientos políticos conjuntos, así como de capacitar a sus líderes. La Alianza se apoyará en el expertise de un Consejo Consultivo cuyos miembros serán los think-tanks liberales más prestigiosos de la subregión. El lanzamiento de la Alianza para Centro América se llevó a cabo el 5 de abril de 2014 en San Pedro Sula.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Mientras que los representantes de otras corrientes políticas, especialmente de partidos socialistas, tanto en Centro América como más allá de la subregión, con frecuencia mantienen una excelente cooperación, armonizan sus políticas y con ello celebran sus triunfos, los liberales no han logrado ser competitivos. Sin embargo, en Centro América existen una serie de retos comunes que demandan respuestas liberales: la violencia y la problemática de los Derechos Humanos, las barreras al libre comercio, la regulación excesiva de los mercados y los dramáticos índices de pobreza, por nombrar sólo algunos.</p>
<p>&nbsp;</p>
<p><img style="float: left;" src="images/1.jpg" alt="1" width="200" height="112" />La Alianza para Centro América apunta a fortalecer la cooperación subregional de las fuerzas liberales respecto de propuestas de solución en torno a dichos temas, así como a ofrecer una plataforma de intercambio de mejores prácticas. También busca que los líderes de sus miembros participen en seminarios diseñados de acuerdo a las necesidades de los partidos políticos. Asimismo es objetivo de la Alianza impulsar de manera importante el trabajo de los partidos políticos en el ámbito nacional, ya que también en Centro América se carece gravemente de respuestas concretas derivadas de la ideología liberal que respondan a los problemas actuales de los ciudadanos y las empresas.</p>
<p>&nbsp;</p>
<p>Para la Fundación Friedrich Naumann para la Libertad la Alianza para Centro América representa un instrumento para retomar de manera importante el trabajo con sus grupos meta originales -los líderes y líderes en formación de sus contrapartes. Los principios liberales como la autorresponsabilidad y el "ownership" se verán fortalecidos en el sentido de que a futuro las contrapartes asumirán la responsabilidad de capacitar a sus bases, explicaba David Henneberger, Director de Proyectos para Centro América en el programa televisivo de análisis político "Análisis Nacional" del Canal 6 de Honduras.</p>
<p>&nbsp;</p>
<p>A fin de realizar una primera evaluación preliminar, antecedió al taller de fundación de la Alianza en San Pedro Sula una conferencia, en la cual los representantes de los partidos políticos analizaron las causas que llevaron a los partidos liberales de Centro América a obtener los desalentadores resultados en las recientes elecciones y en qué ámbitos son más evidentes los déficits de la programática actual de los partidos. También los líderes representantes de las juventudes tuvieron participación en un panel, a través del cual expusieron a los líderes miembro de los partidos políticos ahí presentes sus ideas respecto de lo que debiera ser la política liberal en el siglo XXI.</p>
<p>&nbsp;</p>
<p>Harry Panting, miembro del Consejo Central Ejecutivo del Partido Liberal de Honduras, remarcó que "el éxito de un partido político no reside en la designación a corto plazo de candidatos para las elecciones - que con frecuencia obedecen a intereses particulares, sino más bien en el posicionamiento programático del partido más allá de personas individuales". Esta favorable apreciación fue compartida también por los demás interlocutores.</p>
<p>&nbsp;<img src="images/2.jpg" alt="2" width="508" height="200" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p>&nbsp;</p>
<p>"Un partido político debe concentrarse en la formulación de propuestas políticas concretas en los sectores neurálgicos de la política y la economía, ofreciendo soluciones a los problemas fundamentales de cada uno de los países, a modo de ejemplo, para combatir la pobreza, incentivar el libre comercio o defender la libertad de opinión y de prensa como un derecho fundamental por excelencia", señaló el candidato presidencial por el partido Movimiento Libertario de Costa Rica, Otto Guevara, quien en febrero fuera reelecto diputado.</p>
<p>&nbsp;</p>
<p>Desde la perspectiva de Edgar Herrera, diputado del Parlamento Centroamericano por el Partido Patriota de Guatemala, deberán fortalecerse la ideología y la programática. Sólo así el Partido Patriota podría hacer frente a las numerosas alternativas populistas en Guatemala, ya que en los debates temáticos fundamentados éstas pierden su validez.</p>
<p>&nbsp;</p>
<p>Los participantes del panel coincidieron en un punto: muchos partidos liberales no involucran de manera suficiente a la generación joven, esto a pesar de que los nuevos rostros en la dirigencia de los partidos tienen un impacto en amplios grupos de la población -en promedio entre el 60 y 70% de la población en los Estados centroamericanos es menor a 30 años. Claudia Amaya de Honduras, Asdrúbal Vargas de Costa Rica y Lersy Gatica de Guatemala como representantes de las juventudes compartieron decididamente esta opinión.</p>
<p>&nbsp;</p>
<p><img style="float: left;" src="images/3.jpg" alt="3" width="369" height="200" /></p>
<p>Asdrúbal Vargas como Presidente del Instituto AMAGI -un think tank de jóvenes para jóvenes, provenientes principalmente de universidades tanto públicas como privadas- sin embargo enfatizó que los jóvenes no deberían sólo esperar las oportunidades quebrinde el partido matriz, sino que también deberían buscarlas y asumir una mayor responsabilidad. "Si queremos suscitar cambios en la sociedad, no podemos sólo criticar, debemos actuar con responsabilidad y tomar parte en la divulgación de los principios liberales, así como también comprometernos activamente para lograr su implementación". Además, hizo referencia al término confuso de "la juventud", el cual sugeriría que se trata de un grupo homogéneo. En absoluto, por lo que precisamente la presencia de organizaciones liberales en las universidades goza de tan alta importancia, a fin de no perder hoy a los líderes de mañana si estos se siente atraídos por ideologías opuestas.</p>
<p>&nbsp;</p>
<p>A través de la Alianza, la Fundación Friedrich Naumann en Centro América se enfocará en la realización de actividades subregionales con los líderes de sus partidos contraparte, gracias a las cuales podrá estructurar de manera aún más eficiente la cooperación. El lanzamiento de la Alianza fue acompañado de un amplio despliegue de medios hondureños, atrajo también la atención mediática más allá de Centro América.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;Fuente: David Henneberger es Director de Proyectos para Centro América y Elisabeth Maigler es Asistente de Proyectos para Centro América, ambos colaboradores de la Fundación Friedrich Naumann para la Libertad.</p>
<p>Foto: FNF México</p>
<p>Autor: David Henneberger es Director de Proyectos para Centro América y Elisabeth Maigler es Asistente de Proyectos para Centro América, ambos colaboradores de la Fundación Friedrich Naumann para la Libertad.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/341-la-oposición-venezolana-arrasa">
			&laquo; La oposición venezolana arrasa		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/348-cedice-libertad-y-el-apoyo-de-la-región">
			CEDICE Libertad y el apoyo de la región &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/342-unidos-para-un-éxito-mayor-con-la-“alianza-para-centro-américa”#startOfPageId342">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:78:"Unidos para un éxito mayor con la “Alianza para Centro América” - Relial";s:11:"description";s:155:"En la opinión de David Henneberger y Elisabeth Maigler &amp;nbsp; Un hecho sin precedentes: los partidos liberales en Centro América se unen en una al...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:69:"Unidos para un éxito mayor con la “Alianza para Centro América”";s:6:"og:url";s:151:"http://www.relial.org/index.php/productos/archivo/actualidad/item/342-unidos-para-un-Ã©xito-mayor-con-la-â€œalianza-para-centro-amÃ©ricaâ€";s:8:"og:title";s:78:"Unidos para un éxito mayor con la “Alianza para Centro América” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/49d07d2f2b048709fab28e0845347114_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/49d07d2f2b048709fab28e0845347114_S.jpg";s:14:"og:description";s:159:"En la opinión de David Henneberger y Elisabeth Maigler &amp;amp;nbsp; Un hecho sin precedentes: los partidos liberales en Centro América se unen en una al...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:69:"Unidos para un éxito mayor con la “Alianza para Centro América”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}