<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8535:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId499"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Recordando la lucha peruana contra el terrorismo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/45245952fc8c5c5e099a3e444bf8f32a_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/45245952fc8c5c5e099a3e444bf8f32a_XS.jpg" alt="Recordando la lucha peruana contra el terrorismo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Este 12 de setiembre se cumplirán 23 años desde que el Grupo Especial de Inteligencia (GEIN) capturó a Abimael Guzmán, líder supremo y sanguinario del grupo terrorista Sendero Luminoso. Junto con Guzmán cayó parte esencial de la cúpula de la organización y, con la captura, comenzó el fin de la lucha armada contra la democracia y el Estado que Sendero Luminoso había empezado en mayo de 1980. En estos 23 años el país ha, lentamente, intentado cicatrizar; sin embargo, en estos 23 años también hemos olvidado.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Quienes fuimos sólo niños en el 92 tenemos recuerdos borrosos de este señor que, metido en una jaula, levantaba el puño, gritaba y se señalaba la cabeza. Recordamos el sonido de alguna bomba y también recordamos nuestras primeras noches iluminadas con velas cuando se iba la luz. Los que nacieron después no tienen siquiera este empozado recuerdo. La muerte de decenas de miles de peruanos hace sólo 23 años parece ser una historia que se va escapando. Y mientras eso pasa, Sendero Luminoso se reorganiza.</p>
<p>&nbsp;</p>
<p>Los organismos de fachada de Sendero (Movadef y Fudepp, principalmente) vienen realizando un silencioso esfuerzo por difuminar la historia. Su trabajo busca relativizar la causalidad que siguió la violencia y pretende justificar con el contexto histórico la crueldad y el genocidio. El problema es que lo están logrando. Han vuelto a formar escuelas políticas y están inundando a cientos de jóvenes con las afiebradas ideas que nos llevaron al terror. Tenemos la obligación, como sociedad, de responder antes de que sea demasiado tarde.</p>
<p>&nbsp;</p>
<p>Los senderistas asumen a la guerra como una extensión de la política. Entienden, además, que la forma en que debe librarse la guerra contra el Perú es la táctica maoísta de la "guerra prolongada". Recordemos que las actividades de preparación para el inicio de la lucha armada senderista empezaron, por lo menos, en 1964. Estamos hablando de 16 años de mera acción política. Hoy, Sendero Luminoso está volviendo a nutrirse de eso que tanta falta les ha hecho: nueva sangre. Jóvenes militantes; carne de cañón.</p>
<p>&nbsp;</p>
<p>Lo que tenemos que hacer es recordar. Abrir ese cajón de recuerdos ensangrentados del que tanto nos hemos alejado y reflexionar, discutir y repasar. Tenemos que llamar la atención de quienes fueron demasiado jóvenes para almacenar memorias y tenemos que llamar a las memorias de quienes han preferido olvidar. Vivimos en un país en el que todavía deambulan miles de viudas, huérfanos y heridos. La violencia con la que fuimos atacados no puede ser olvidada. La memoria es nuestra principal arma contra el terror de Sendero.</p>
<p>&nbsp;</p>
<p>Cuando en abril de 1987 Chaim Herzog se conviritó en el primer Presidente de Israel en visitar Alemania luego del fin de la guerra y de la creación del Estado de Israel, Herzog visitó el viejo campo de concentración de Bergen-Belsen. Campo que él mismo había visto operativo mientras luchó como oficial del ejército británico. Luego de la visita Herzog dijo: "No traigo el perdón conmigo, ni tampoco el olvido. Los únicos que pueden perdonar están muertos, los vivos no tienen derecho a olvidar". En sus palabras no hay violencia; tampoco olvido.</p>
<p>&nbsp;</p>
<p>Propongo algo simple: propongo un apagón. Apaguemos todos nuestras luces por una hora el 12 de setiembre y recordemos el día de la victoria del Perú sobre el terror. No llenemos de política esto. Podemos tener miles de desacuerdos, pero tenemos que ser unánimes en condenar la barbarie senderista. Debemos cerrar filas como país frente al terror. Superemos, por favor, nuestras diferencias esta vez. Apaguemos todos las luces y tomémonos fotos con velas. Inundemos las redes sociales de memoria. Estamos a tiempo.</p>
<p>&nbsp;</p>
<p>Con un montón de fotos no vamos a detener a Sendero, pero sí podemos dar un primer paso hacia la unión. Que la gente se pregunte qué es eso del apagón. Que la gente recuerde que "apagón" es una palabra que llevamos todos tatuada en las tripas. Que la gente busque en Google qué fue Sendero Luminoso y que lo encuentre. Carguemos, al menos por un día, el dolor que las miles de víctimas que tuvieron la suerte -y la miseria- de sobrevivir. Seamos un país más corajudo frente al futuro y más claro en su memoria.</p>
<p>&nbsp;</p>
<p>Autor: Mijael Garrido-Lecca</p>
<p>Fuente: <a href="http://www.iplperu.org/archives/6978/">IPL Perú</a></p>
<p>Foto: IPL Perú </p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba?-esa-es-la-pregunta-cuba-en-su-nueva-etapa-análisis-foda">
			&laquo; ¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo">
			Las elecciones parlamentarias en Venezuela: ambiente previo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo#startOfPageId499">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:57:"Recordando la lucha peruana contra el terrorismo - Relial";s:11:"description";s:158:"Este 12 de setiembre se cumplirán 23 años desde que el Grupo Especial de Inteligencia (GEIN) capturó a Abimael Guzmán, líder supremo y sanguinario del...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:48:"Recordando la lucha peruana contra el terrorismo";s:6:"og:url";s:118:"http://www.relial.org/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo";s:8:"og:title";s:57:"Recordando la lucha peruana contra el terrorismo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/45245952fc8c5c5e099a3e444bf8f32a_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/45245952fc8c5c5e099a3e444bf8f32a_S.jpg";s:14:"og:description";s:158:"Este 12 de setiembre se cumplirán 23 años desde que el Grupo Especial de Inteligencia (GEIN) capturó a Abimael Guzmán, líder supremo y sanguinario del...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:48:"Recordando la lucha peruana contra el terrorismo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}