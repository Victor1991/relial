<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:23917:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId498"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/42a35505dabe860dcdeb51f92d5be768_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/42a35505dabe860dcdeb51f92d5be768_XS.jpg" alt="&iquest;Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: An&aacute;lisis FODA" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>18 agosto, 2015</p>
<p>Consejo Empresarial de América Latina</p>
<p>Conferencia anual: América Latina y el Comercio Internacional, San Juan, Puerto Rico, 14 de agosto de 2015</p>
<p>Por: Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Precisamente hoy, 14 de agosto de 2015, se iza la bandera norteamericana en La Habana, restableciéndose formalmente las relaciones diplomáticas entre Estados Unidos y Cuba, interrumpidas desde el 3 de enero de 1961, hace 54 años, cuando en Washington mandaba Ike Eisenhower y en La Habana Fidel Castro.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El presidente de Estados Unidos Barack Obama y su canciller John Kerry, entre otras muchas personalidades, han pedido el levantamiento del embargo económico impuesto a Cuba por John F. Kennedy en 1962 en medio de la Guerra Fría, y por La Habana han pasado recientemente alemanes, franceses, holandeses y españoles a tender puentes hacia la Isla.</p>
<p>&nbsp;</p>
<p>La percepción, pues, es que Cuba, el mayor de los trece mercados del Caribe y la isla más poblada, seguida muy de cerca por República Dominicana, se abre nuevamente al comercio y las relaciones internacionales.</p>
<p>&nbsp;</p>
<p>A los efectos de la brevedad y de la objetividad, dada la composición de este auditorio, voy a emplear la estructura de los llamados análisis FODA para contribuir a la decisión empresarial de invertir en ese mercado, abstenerse de hacerlo o esperar más claridad y mejores tiempos. No será mi función recomendar lo que se debe hacer, sino colocar ciertos datos y elementos básicos sobre la mesa de los decision makers. Ellos sabrán lo que conviene llevar a cabo.</p>
<p>&nbsp;</p>
<p>Los análisis FODA, como sabemos, enumeran las Fortalezas positivas, las Oportunidades presentes, las Debilidades que se perciben y las Amenazas que acechan.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Fortalezas</strong></p>
<p>&nbsp;</p>
<p>1. El deshielo anunciado simultáneamente por los presidentes Barack Obama y Raúl Castro el 17 de diciembre pasado, a lo que se agregan los diversos pasos dados por ambos gobiernos en la dirección de la reconciliación, han creado una atmósfera de triunfalismo sobre el futuro económico de Cuba cargada de buenas expectativas.</p>
<p>&nbsp;</p>
<p>2. Según las encuestas, el apoyo a las medidas de Obama es notablemente mayoritario en Estados Unidos y dentro de Cuba, pero existen razones para pensar que un porcentaje sustancial de los exiliados cubanos también las respaldan.</p>
<p>&nbsp;</p>
<p>3. Como parte de ese acercamiento, se espera que algunas o muchas empresas extranjeras exploren la posibilidad de invertir en la Isla. Hay un notable capital humano disponible en Cuba: más de ochocientos mil universitarios y una población generalmente sana e instruida.</p>
<p>&nbsp;</p>
<p>4. El gobierno cubano, tras 56 años de continuado ejercicio del poder, parece sólidamente en control de la situación, sin persona o grupo alguno que visiblemente rete su autoridad con posibilidades de tener éxito.</p>
<p>&nbsp;</p>
<p>5. Tras la elección de Hugo Chávez a la presidencia de Venezuela a fines de 1998, este país petrolero ha transferido a Cuba cantidades ingentes de recursos. El economista Carmelo Mesa-Lago calcula el monto anual de la ayuda en su momento más alto en 13,000 millones de dólares.</p>
<p>&nbsp;</p>
<p>6. Tras un cuarto de siglo de la desaparición del bloque comunista, el gobierno de los Castro, junto a Corea del Norte, ha demostrado ser la excepción a la regla: ha mantenido el control sin renunciar a los principios del marxismo-leninismo y del partido único, aunque tratando cautelosamente de modificar su aparato productivo.</p>
<p>&nbsp;</p>
<p>7. Dentro de ese espíritu de "reformas-dentro-del-sistema", en abril de 2010 Raúl Castro presentó lo que se conoce como Lineamientos, donde trazó las líneas maestras de su reforma del Estado, creando espacios para la inversión extranjera y los "cuentapropistas".</p>
<p>&nbsp;</p>
<p>8. Las instituciones de mayor peso o más visibles en la sociedad cubana, como las Fuerzas Armadas y la Asamblea del Poder Popular, parecen respaldar totalmente los pasos dados por el presidente, general Raúl Castro, en dirección del reencuentro con Estados Unidos.</p>
<p>&nbsp;</p>
<p>9. Algunas cancillerías de la Unión Europea, como la española, la alemana y la holandesa ven con buenos ojos lo que sucede, así como el Vaticano, que ha participado en las reuniones secretas para lograr el fin de los conflictos entre Cuba y Estados Unidos. El papa Francisco, ha anunciado su próxima visita a Cuba como forma de espaldarazo a los cambios.</p>
<p>&nbsp;</p>
<p>10. Cuba ejerce una enorme influencia en los países de la ALBA y el claro liderazgo de los inscritos dentro de lo que se conoce como las naciones del Socialismo del Siglo XXI: Venezuela, Bolivia, Ecuador y Nicaragua.</p>
<p>&nbsp;</p>
<p>11. A esta lista hay que agregar la simpatía y solidaridad que inspira Cuba en gobiernos como los de Argentina, Brasil, Uruguay, Chile y El Salvador. En suma, nunca la Isla ha estado más arropada internacionalmente.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Oportunidades</strong></p>
<p>&nbsp;</p>
<p>1. El país necesita reconstruir casi totalmente sus infraestructuras. Todo es obsoleto o está en ruinas: los acueductos y alcantarillados, las redes eléctricas, Internet, los puentes y caminos, las comunicaciones telefónicas, las casas y edificios.</p>
<p>&nbsp;</p>
<p>2. Hay un campo fértil para la producción local de alimentos. Cuba importa el 80% de los alimentos que consumen las personas y los animales. Antes de 1959 las proporciones eran al revés.</p>
<p>&nbsp;</p>
<p>3. La industria azucarera, que antes de la revolución fue la espina dorsal de la economía, con una producción de seis millones de toneladas anuales como promedio, se ha reducido a dos.</p>
<p>&nbsp;</p>
<p>4. El turismo es un campo muy prometedor. La Isla, muy bella y con decenas de playas, cuenta con al menos 14 puntos en las dos costas potencialmente transformables en sitios ideales para construir polos de atracción semejantes a Puerto Banús en España o Casa de Campo en La Romana, República Dominicana.</p>
<p>&nbsp;</p>
<p>5. Considerando el número de investigadores en el campo de la medicina, hoy totalmente subutilizados, el país, potencialmente, sería el perfecto asociado de las grandes industrias farmacéuticas internacionales, tanto para desarrollar medicamentos nuevos como para producirlos.</p>
<p>&nbsp;</p>
<p>6. Dado el prestigio (hoy algo disminuido) de su sistema sanitario, Cuba, potencialmente, es un sitio excelente para el turismo médico y para establecer nexos con los sistemas de seguro norteamericanos. La Habana está mucho más cerca de Key West que Miami de Orlando en Florida.</p>
<p>&nbsp;</p>
<p>7. En el momento en que realmente se normalicen las relaciones y se faciliten y multipliquen los viajes, Cuba se convertirá en un lugar ideal para jubilar a decenas de miles de cubano-americanos y norteamericanos que percibirán un promedio de US$ 1,800 dólares mensuales.</p>
<p>&nbsp;</p>
<p>8. Cuba recibe unos tres millones de turistas anuales y tiene once millones de habitantes. En pocos años podría recibir once millones de turistas y alcanzar la clasificación de paraíso. Unos cuantos developers audaces e inteligentes pudieran transformar la Isla de Pinos, al sur de Cuba, en la Mallorca del Caribe. Mallorca, en el Mediterráneo, recibe 20 millones de turistas todos los años. En Estados Unidos hay 300,000 yates de lujo que pudieran visitar Cuba y repostar en las marinas que existen y se creen.</p>
<p>&nbsp;</p>
<p>9. Si las relaciones entre Estados Unidos y Cuba continúan profundizándose, es posible prever un Tratado de Libre Comercio que le permita a la Isla exportar sin limitaciones hacia el más rico mercado del mundo.</p>
<p>&nbsp;</p>
<p>10. Por su posición geográfica, Cuba es un hub ideal para distribuir la carga marítima y aérea a Norte y Sudamérica.</p>
<p>&nbsp;</p>
<p>11. Los cubano-americanos, como sucedió con los taiwaneses con relación a China, podrían trasladar a Cuba su expertise en los negocios, sus relaciones y los capitales que han creado, lo que integraría a la Isla en el mundo comercial y empresarial de Estados Unidos.</p>
<p>&nbsp;</p>
<p>12. El país, como sucedió en la Europa post-comunista, sería el paraíso de las franquicias norteamericanas de comida rápida más conocidas: MacDonalds, Burger King, KFC, etcétera.</p>
<p>&nbsp;</p>
<p>13. Lo mismo ocurriría con las cadenas de tiendas por departamentos: Zara, Corte Inglés, Macys, Sears y el resto de los más conocidos establecimientos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Debilidades</strong></p>
<p>&nbsp;</p>
<p>1. El gobierno de Raúl Castro no quiere un cambio de régimen sino mejorar el modelo comunista iniciado en 1959. De acuerdo con lo declarado y publicado, el modelo previsto ni siquiera es el chino o vietnamita, sino un Capitalismo Militar de Estado, donde el gobierno se reserva el control de las 2500 empresas mayores del país, que explota directamente por medio del aparato militar o en sociedad con capitalistas extranjeros. Los cubanos no tendrán acceso a los medios de producción, sino a pequeñas empresas de servicio (paladares, peluquerías, etcétera).</p>
<p>&nbsp;</p>
<p>2. El gobierno de Raúl Castro no cree, como proclamaba Deng Xiaoping en 1992, que "enriquecerse es glorioso". Raúl y Fidel siguen pensando que el capitalismo es una expresión malvada de las peores tendencias humanas y desprecian a quienes lo practican. En Cuba, los emprendedores no son alentados, sino reprimidos. Los capitalistas puedan ser coyunturalmente necesarios, pero siempre bajo el estricto control de la policía política para evitar la contaminación ideológica del pueblo y los desmanes de que son capaces. El gobierno continúa sosteniendo la supremacía de la planificación centralizada sobre el mercado y las virtudes de la frugalidad y el no-consumo.</p>
<p>&nbsp;</p>
<p>3. Si un empresario quiere evaluar el riesgo-país antes de invertir en cualquier lugar, repasa la calificación de Moody o de Standard &amp; Poor. De acuerdo con estas empresas de evaluación de riesgo, Cuba comparece en la franja más peligrosa. Hay un larguísimo historial de morosidad o simplemente de incumplimiento de obligaciones y de refinanciamientos que luego han resultado inútiles. No han podido pagarlos debido a un grave problema de fondo: la sociedad cubana es muy improductiva por el sistema económico colectivista que le han impuesto y por la cuantía y las prioridades del gasto público decididas por el gobierno.</p>
<p>&nbsp;</p>
<p>4. El Estado cubano carece de reservas sustanciales y apenas genera ingresos suficientes para sobrevivir mes a mes. Esto determina que no cuenta con recursos propios para emprender las grandes obras públicas que necesita y requiere de préstamos internacionales para afrontarlas, lo cual se convierte en un grave problema porque no es un sujeto de crédito fiable.</p>
<p>&nbsp;</p>
<p>5. No existen en Cuba tribunales imparciales a los cuales acudir para dirimir una controversia de derecho civil y mucho menos penal. Hay una absoluta indefensión legal ante cualquier conflicto con el gobierno o con una empresa estatal. El gobierno, de acuerdo con sus necesidades, persigue, apresa o libera arbitrariamente a cualquier ciudadano, incluidos los empresarios extranjeros con los que ha entrado en pleito. O sea, lo contrario de un verdadero Estado de Derecho. De esa circunstancia pueden dar testimonio, entre decenas de personas, el panameño Simón Abadi, el chileno Max Marambio o el canadiense Sarkis Yacoubian.</p>
<p>&nbsp;</p>
<p>6. Los empresarios extranjeros radicados en Cuba viven bajo la "no-tan-discreta" vigilancia de los cuerpos de Seguridad, siempre a la paranoica búsqueda del mítico y siniestro agente de la CIA. Muchos de los funcionarios cubanos y simples vecinos o potenciales amigos no se atreven a vincularse a los empresarios extranjeros por temor a perder la confianza del gobierno, o se ven obligados a informar por escrito sobre cada contacto voluntario o involuntario sostenido con ellos.</p>
<p>&nbsp;</p>
<p>7. Cuba no ha resuelto el importantísimo problema de la moneda. Oficialmente, el dólar se cambia aproximadamente a la par del peso cubano. En el mercado paralelo es 24 pesos por 1 dólar. Los cubanos cobran en pesos, pero el gobierno les vende en dólares, lo que genera una gran inconformidad y una obvia sensación de injusticia.</p>
<p>&nbsp;</p>
<p>8. Los salarios son muy bajos (unos US$24 mensuales), lo que determina la inexistencia de un mercado potencial de 11 millones de consumidores. Los cubanos poseen el menor poder adquisitivo per cápita de Hispanoamérica.</p>
<p>&nbsp;</p>
<p>9. El sistema bancario es insuficiente y primitivo. Los depósitos en dólares –como ha sucedido en el pasado reciente—pueden ser congelados a discreción del gobierno.</p>
<p>&nbsp;</p>
<p>10. La burocracia es muy lenta e insegura. Los funcionarios no se atreven a tomar decisiones y las instrucciones que reciben a veces son contradictorias.</p>
<p>&nbsp;</p>
<p>11. La contratación de trabajadores se hace por medio de una compañía estatal que les cobra en dólares a los inversionistas y les paga en pesos a los cubanos a los que ha asignado a esas tareas. En la transacción, el Estado cubano se queda con el 94% del salario. Esa práctica contraviene los acuerdos internacionales firmados con la OIT.</p>
<p>&nbsp;</p>
<p>12. Durante más de medio siglo, los cubanos se han acostumbrado a robarle al Estado para sobrevivir, vendiendo en bolsa negra el producto de esas sustracciones. Estos hábitos se trasladan a la empresa privada, especialmente porque han vivido bajo una intensa campaña propagandística en contra de los capitalistas explotadores. Saben que el socialismo conduce al desastre, pero creen que el capitalismo es una actividad de lobos hambrientos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Amenazas</strong></p>
<p>&nbsp;</p>
<p>1. Una parte sustancial de la política de reconciliación entre Obama y Raúl Castro depende del levantamiento del embargo. Aunque existe una gran presión por parte de la Casa Blanca y encuestas que demuestran que la mayoría de los norteamericanos desea que se terminen las sanciones, no hay garantías de que se logrará durante el actual periodo presidencial. Tampoco se puede predecir qué ocurrirá con las relaciones entre los dos países si las próximas elecciones las gana un candidato republicano. Dos de los más prominentes, Jeb Bush y Marco Rubio, han criticado al presidente Obama por su nueva política cubana.</p>
<p>&nbsp;</p>
<p>2. La anulación de la ley Helms-Burton de 1996 es uno de los requisitos clave para que Cuba pueda acercarse al BID, el FMI, o al BM. Sin esos créditos, que requieren la afiliación de Cuba a estos organismos, la Isla no tendrá acceso a los préstamos para reconstruir las costosas infraestructuras que el país necesita. Es un proceso muy lento que tomará mucho tiempo.</p>
<p>&nbsp;</p>
<p>3. Cuba depende en gran medida de los subsidios de Venezuela y el chavismo pasa por un pésimo momento que pudiera desembocar en su destrucción y sustitución por un gobierno desfavorable a "los cubanos". La desaparición del chavismo en Venezuela sería una catástrofe económica y política para Cuba. Hoy la popularidad de Nicolás Maduro está por debajo de un 20%.</p>
<p>&nbsp;</p>
<p>4. La estabilidad política del régimen cubano hasta ahora ha dependido del control de Fidel y Raúl Castro. Fidel acaba de cumplir 89 años y Raúl en junio alcanzó los 84. Con ellos se liquida la generación de la revolución y se le da paso a unas personas más jóvenes, encabezadas por el heredero elegido por Raúl Castro, el ingeniero Miguel Díaz-Canel, de 55 años. ¿Qué va a pasar entonces? ¿Podrá el régimen transmitir la autoridad dentro del modelo comunista de partido único sin los caudillos que hasta ahora le han dado forma y sentido al gobierno? ¿Surgirán las ambiciones incontrolables de otros candidatos, presumiblemente militares jóvenes, decididos a tomar el mando? Nadie sabe. La historia cubana, sin embargo, ha sido sangrienta cuando los regímenes llegan a su agotamiento. Son reemplazados por la violencia.</p>
<p>&nbsp;</p>
<p>5. Será muy riesgoso invertir en propiedades confiscadas a sus legítimos dueños en los primeros años de la revolución. Tanto en Estados Unidos como en España existen organizaciones de perjudicados que están dispuestos a acudir a los tribunales en defensa de sus derechos y han contratado abogados para ello. Originalmente, las víctimas de las confiscaciones eran norteamericanas o españolas, pero con el tiempo se han agregado los cubano-americanos y los hispano-cubanos. Tanto en el Congreso y el Senado de Estados Unidos como en el Parlamento español hay legisladores de ese origen.</p>
<p>&nbsp;</p>
<p>6. Probablemente suceda como en el caso de Nicaragua. Los exiliados nicas, infinitamente menos numerosos y poderosos que los cubano-americanos, sin congresistas de ese origen en Washington, lograron condicionar la ayuda norteamericana a la solución de las confiscaciones que habían padecido durante la primera década sandinista. Lo predecible es que los estadounidenses perjudicados, junto a los cubano-americanos, logren paralizar cualquier ayuda o beneficio norteamericano destinado a Cuba hasta que se les devuelvan las propiedades que fueron suyas o se les indemnicen por ellas.</p>
<p>&nbsp;</p>
<p>7. Aunque en el sector turístico hay una posibilidad evidente de ganar dinero en Cuba, es difícil que la industria hotelera norteamericana, casi toda presente en la Bolsa, sujeta a la legislación norteamericana y a la presión sindical, se preste a colaborar con la contrainteligencia cubana facilitando las escuchas y las filmaciones clandestinas dentro de las instalaciones hoteleras, como hoy hacen Meliá y otras cadenas europeas, violando, incluso, la propia ley cubana que supuestamente defiende la inviolabilidad de la intimidad. Los abogados norteamericanos tienen muy claro el alto precio que tuvieron que pagar las empresas alemanas que se asociaron al nazismo, como sucedió con Volkswagen y Bayer.</p>
<p>&nbsp;</p>
<p>8. Comoquiera que no se trata de invertir en un país sometido a una dictadura, sino de asociarse a un gobierno dictatorial para explotar a los trabajadores y a las empresas, es muy posible que, cuando se presente la oportunidad, esos empresarios vinculados al gobierno cubano van a ser acusados ante los tribunales por la confiscación del 94% del salario mediante el cambio de dólares por peso y otras violaciones de las reglas de la Organización Internacional del Trabajo. Las reclamaciones de los trabajadores serán cuantiosas y ya hay bufetes norteamericanos dispuestos a interponerlas por un porcentaje de lo que en su momento se reciba.</p>
<p>&nbsp;</p>
<p>9. Por último, dos artículos de la Constitución cubana perfilan la naturaleza inflexiblemente comunista del régimen y se convierten en una permanente amenaza contra los inversionistas extranjeros. Artículo 5: El Partido comunista de Cuba, martiano y marxista-leninista, vanguardia organizada de la nación cubana, es la fuerza dirigente superior de la sociedad y del Estado. Artículo 62: Ninguna de las libertades reconocidas a los ciudadanos puede ser ejercida contra lo establecido en la Constitución y las leyes, ni contra la decisión del pueblo cubano de construir el socialismo y el comunismo. La infracción de este principio es punible.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Foto: El blog de Montaner</p>
<p>Texto: Texto proporcionado por el autor</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-éxito">
			&laquo; Chile, de espaldas al éxito		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo">
			Recordando la lucha peruana contra el terrorismo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba?-esa-es-la-pregunta-cuba-en-su-nueva-etapa-análisis-foda#startOfPageId498">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:101:"¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA - Relial";s:11:"description";s:155:"18 agosto, 2015 Consejo Empresarial de América Latina Conferencia anual: América Latina y el Comercio Internacional, San Juan, Puerto Rico, 14 de agos...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:92:"¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA";s:6:"og:url";s:98:"http://relial.org/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba";s:8:"og:title";s:101:"¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/42a35505dabe860dcdeb51f92d5be768_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/42a35505dabe860dcdeb51f92d5be768_S.jpg";s:14:"og:description";s:155:"18 agosto, 2015 Consejo Empresarial de América Latina Conferencia anual: América Latina y el Comercio Internacional, San Juan, Puerto Rico, 14 de agos...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:92:"¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}