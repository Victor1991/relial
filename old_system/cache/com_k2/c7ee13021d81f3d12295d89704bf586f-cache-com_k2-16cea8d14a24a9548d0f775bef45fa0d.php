<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8011:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId280"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Respuestas liberales para la región
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8c4e7ddecfb739ef5c33c55621b27630_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8c4e7ddecfb739ef5c33c55621b27630_XS.jpg" alt="Respuestas liberales para la regi&oacute;n" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Ricardo López Murphy, Presidente de la Red Liberal de América Latina (RELIAL), visitó México en ocasión de una serie de conferencias sobre la situación política en Sudamérica así como los modelos que rigen la realidad económica en la región.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Dando inicio a la gestión 2014, RELIAL y la Fundación Friedrich Naumann para la Libertad propiciaron encuentros académicos con jóvenes estudiantes de universidades públicas y privadas que permitan analizar los rasgos económicos que distinguen al liberalismo y, de esta manera, romper los mitos que por años la izquierda cimentó en las altas casas superiores de estudio.</p>
<p>&nbsp;</p>
<p>Nadie mejor que Ricardo López Murphy, ex ministro de defensa y de finanzas de la República Argentina, además de reconocido economista y pensador liberal para llevar estos temas al análisis y la reflexión.</p>
<p>&nbsp;</p>
<p>A través de ejemplos claros e ilustrativos, demostró porqué países que siguieron recetas de corte populista - tales como Argentina y Venezuela- están hoy padeciendo crisis, inflación, desabastecimiento, descrédito institucional, en comparación con países que al abrir sus economías estimulan al inversión internacional dando lugar al crecimiento y prosperidad.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img src="images/foto2.jpg" alt="" /></p>
<p>&nbsp;</p>
<p>Que el discurso liberal llegue a las universidades públicas no tiene precedentes; durante décadas, los programas de formación han preconizado recetas y proclamado la justicia social como estrictos y únicos modelos a seguir. Llevar intelectuales y líderes de opinión que resalten –y demuestren- las bondades del mercado, y estimulen a pensar en las virtudes de la libertad individual es auténticamente revolucionario.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="font-size: 10pt;"><strong>PRESENTACIÓN DE "LA MISERIA DEL INTERVENCIONISMO: 1929- 2008"</strong></span></p>
<p><span style="font-size: 10pt;"><strong><br /></strong></span></p>
<p>Entre otras actividades de reflexión académica, Axel Kaiser presentó su libro. Roberto Salinas, Arturo Damm y Ricardo López Murphy comentaron esta obra.</p>
<p>&nbsp;</p>
<p><img src="images/20140212_112439_1miseria.jpg" alt="" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Un principio del pensamiento liberal es la apertura a la diversidad. La presentación del libro de Kaiser propició precisamente eso: un exquisito debate entre los puntos que difieren a los economistas neoclásicos y austriacos.</p>
<p>&nbsp;</p>
<p>El rol de la banca central, los postulados de Milton Friedman son temas que encuentran matices y enriquecen aún más las posiciones liberales frente a políticas económicas. Para el auditorio –jóvenes mexicanos en plena formación- el intercambio de ideas entre estos expertos de alto nivel fue una lección y un pase para conocer autores que fecunden sus conocimientos de economía liberal.</p>
<p>&nbsp;</p>
<p>El libro de Axel Kaiser fue altamente valorado por los oradores, quienes resaltaron la meticulosidad del análisis y el excelente relato –riguroso y sistemático con que el autor explica los orígenes, causas y consecuencias de las crisis económicas de los últimos años.</p>
<p>&nbsp;</p>
<p>Esta presentación fue propiciada por Caminos de la Libertad y México Business Forum, ambas organizaciones de RELIAL, con el apoyo de la Fundación Friedrich Naumann para la libertad.</p>
<p>&nbsp;</p>
<p><span style="color: #808080; font-size: 8pt;">Texto: Silvia Mercado</span></p>
<p><span style="color: #808080; font-size: 8pt;">Fotos: Miguel Ángel Torres, Caminos de la Libertad</span></p>
<p><span style="font-size: 8pt;">&nbsp;</span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/269-apuleyo-el-sistema-de-partidos-podría-colapsar-si-no-renueva-dirigencia">
			&laquo; Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/289-cedice-libertad-ante-los-atentados-a-las-libertades-en-venezuela--un-gobierno-debe-garantizar-las-condiciones-de-respeto-reconocimiento-y-libertades-que-permita-el-concurso-de-todos-los-ciudadanos-en-la-solución-de-los-problemas-del-país">
			CEDICE Libertad  Ante los atentados a las Libertades en Venezuela . Un gobierno debe garantizar las condiciones de respeto, reconocimiento y libertades que permita el concurso de todos los ciudadanos en la solución de los problemas del país. &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/280-respuestas-liberales-para-la-región#startOfPageId280">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:45:"Respuestas liberales para la región - Relial";s:11:"description";s:159:"Ricardo López Murphy, Presidente de la Red Liberal de América Latina (RELIAL), visitó México en ocasión de una serie de conferencias sobre la situació...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:36:"Respuestas liberales para la región";s:6:"og:url";s:106:"http://www.relial.org/index.php/productos/archivo/actualidad/item/280-respuestas-liberales-para-la-región";s:8:"og:title";s:45:"Respuestas liberales para la región - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/8c4e7ddecfb739ef5c33c55621b27630_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/8c4e7ddecfb739ef5c33c55621b27630_S.jpg";s:14:"og:description";s:159:"Ricardo López Murphy, Presidente de la Red Liberal de América Latina (RELIAL), visitó México en ocasión de una serie de conferencias sobre la situació...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:36:"Respuestas liberales para la región";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}