<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12042:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId412"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Hacia un sistema de pensiones justo y equitativo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/34649abc993982b197aa9a6210af69ed_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/34649abc993982b197aa9a6210af69ed_XS.jpg" alt="Hacia un sistema de pensiones justo y equitativo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de José Joaquín Fernández. Miembro de la Mont Pelerin Society.</p>
<p>&nbsp;</p>
<p>Hace unos días, el vicepresidente de la República de Costa Rica y ministro de Hacienda, Helio Fallas, manifestó su intención de reformar el sistema de pensiones con cargo al Presupuesto Nacional en donde un 2,9% percibe pensiones superiores a los C2,6 millones mensuales. ¿Es esto justo o equitativo?</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Una pensión justa debe calcularse a partir de la suma de las cotizaciones (ahorro), más los rendimientos generados por dicho ahorro. Parafraseando la Biblia: "Te ganarás la pensión con el sudor de tu frente". Quien recibe una pensión que excede lo expuesto, está disfrutando de una pensión a costa del "sudor del prójimo"; lo cual es una injusticia y debe detenerse. Básicamente en el mundo existen dos sistemas de pensiones: Fondos de Capitalización Individual y Sistema de Reparto.</p>
<p>&nbsp;</p>
<p>En el sistema de Fondos de Capitalización Individual, la pensión se calcula a partir de la suma de las cotizaciones (ahorro) del afiliado, más los rendimientos de las inversiones generados por dicho ahorro. Estos fondos son propiedad de los cotizantes y, por tanto, su patrimonio es independiente y distinto del de las empresas o instituciones que los administran.</p>
<p>&nbsp;</p>
<p>Por otra parte, el sistema de pensiones de reparto nace con Otto von Bismarck (1815-1898), quien fuera el creador del Estado Benefactor en la década de 1880 al impulsar las siguientes tres leyes: Seguro contra Enfermedad (1883), Seguro contra Accidentes de Trabajo (1884) y Seguro contra la Invalidez y la Vejez (1889). Desde entonces, su sistema de seguridad social se ha extendido por todo el mundo. El régimen de pensiones de reparto se caracteriza por ser: 1) de cotización obligatoria y coercitiva; 2) de beneficio definido previamente; 3) las cotizaciones son administradas por el burócrata y; 4) los trabajadores de hoy pagan la pensión de quien está jubilado hoy (Esquema Ponzi).</p>
<p>&nbsp;</p>
<p>El sistema de pensiones de reparto tiene todas las desventajas que se puedan pensar. Al ser obligatorias y coercitivas se viola el principio de libertad de escoger a la persona que cotiza. Al tener un beneficio definido previamente se rompe el vínculo de justicia social donde la pensión es fruto de los ahorros del cotizante más los rendimientos generados. Debido a que las cotizaciones son administradas por el burócrata, -con carácter obligatorio y sin guardar relación con lo cotizado-, el burócrata no tiene incentivo alguno en administrar de la mejor manera posible las cotizaciones. Lo anterior garantiza bajos rendimientos y, en el peor de los casos, hasta rendimientos negativos. Sin embargo, lo peor del sistema de reparto es que sigue un esquema Ponzi. El esquema Ponzi se le llama también esquema piramidal en algunos países.</p>
<p>&nbsp;</p>
<p>Un esquema Ponzi es una operación de inversión que se considera fraudulenta en todo el mundo y que es penada con cárcel a quien la practique de manera privada. Esta inversión consiste en que el operador de la inversión paga a los inversionistas con el capital aportado por los nuevos inversionistas, en vez de pagar con el rendimiento generado por el operador. Para que el sistema Ponzi sea sostenible, la base de inversionistas debe ser creciente. Cuando se creó el sistema de reparto, la población estaba creciendo; es decir, el número de nuevos afiliados al sistema era menor al número de nuevos pensionados. Sin embargo, las tendencias poblacionales están cambiando y el envejecimiento de la población mundial hace que cada vez sean menos los afiliados que pagan por un pensionado. Lo anterior ha acelerado la insostenibilidad del sistema de pensiones de reparto en todo el mundo.</p>
<p>&nbsp;</p>
<p>El sistema de pensiones de reparto no le conviene a ningún ciudadano. El único que se beneficia es el político, el Gobierno y el burócrata porque en este esquema, el afiliado le está haciendo un préstamo forzoso al Gobierno, de largo plazo y a un interés por debajo del mercado, equivalente al monto de sus cotizaciones. A mi juicio, la única razón para que el esquema de reparto se haya extendido por todo el mundo es porque alimenta la avaricia fiscal del Gobierno bajo el subterfugio de la justicia social.</p>
<p>&nbsp;</p>
<p>En Costa Rica, tanto los regímenes de pensiones con cargo al Presupuesto Nacional como el de la Caja Costarricense de Seguro Social (CCSS), son esquemas de reparto y, por tanto, no siguen el criterio de justicia social aquí expuesto.</p>
<p>&nbsp;</p>
<p>Si deseamos pensiones altas, debemos garantizar que las administradoras de pensiones coloquen nuestros ahorros en actividades rentables con un nivel de riesgo razonable y en donde las comisiones por su administración sean lo más bajas posibles. Esto solo se puede lograr en un ambiente altamente competitivo con operadoras privadas. La libre competencia es el único guardián permanente que garantiza el mejor uso de los recursos para la satisfacción del consumidor. Las operadoras deben tener libertad en la selección de su portafolio. No se obtiene beneficio alguno si las operadoras de pensiones privadas son obligadas a "invertir" en bonos del Gobierno.</p>
<p>&nbsp;</p>
<p>Es un hecho, tanto teórico como empírico, que un burócrata (el Estado) no ha sido, no lo es, ni será nunca eficiente en el manejo de cualquier recurso. No hay nada que justifique el gobierno empresario y, por tanto, éste no debe administrar fondos de pensiones. Sobran los estudios sobre el uso ineficiente de los recursos por parte de los Gobiernos. Por el bien de los pensionados, los Gobiernos deben abandonar la administración de fondos de pensiones.</p>
<p>&nbsp;</p>
<p>A lo sumo, las únicas pensiones con cargo al Presupuesto Nacional deben ser para velar por una pensión mínima, pero digna, únicamente a aquellas personas en tercera edad de muy escasos recursos que por alguna razón de fuerza mayor no fueron capaces de cotizar o que lo hicieron de manera insuficiente. Este sistema solidario de pensiones se financiaría con los impuestos tradicionales; no hacen falta cargas sociales para esto.</p>
<p>&nbsp;</p>
<p>El régimen de pensiones chileno es considerado como uno, sino el más, exitoso del mundo. Este se basa en un sistema de capitalización individual en donde las operadoras privadas están sometidas a un régimen altamente competitivo. En el informe "Sistema Previsional Chileno: Resultados Favorables en Comparación Internacional", publicado en Junio del 2013 por Libertad y Desarrollo se concluye que: "Los fondos de pensiones de los trabajadores en Chile están siendo bien manejados por las administradoras. Incluso, Chile ocupa el primer lugar de la OCDE en términos de rentabilidad". El sistema de pensiones chileno, no sólo es rentable sino que es sostenible, a diferencia de los sistemas de reparto donde ninguno lo es.</p>
<p>&nbsp;</p>
<p>José Piñera, exministro del Trabajo y Previsión Social en Chile, afirma sobre el sistema de pensiones chileno: "el impacto de la reforma del sistema de pensiones ha ido más lejos que los impresionantes indicadores económicos. La privatización de las pensiones ha significado un cambio de paradigma, logrando nada menos que una redistribución radical del poder del estado hacia la sociedad civil y, al convertir a los trabajadores en propietarios individuales del capital del país, ha creado una atmósfera cultural y política más consistente con los postulados de una sociedad libre".</p>
<p>&nbsp;</p>
<p>Los Gobiernos siempre recetan las mismas soluciones al régimen de reparto: aumento de la edad de pensión, aumento en la cuota de cotización y reducción de beneficios. Si queremos pensiones sostenibles, altas, justas y equitativas, debemos eliminar (no reformar) nuestros regímenes de pensiones de reparto, tanto aquellos con cargo al Presupuesto Nacional como el de la CCSS, y sustituirlos por uno de capitalización individual al estilo chileno donde la Libertad sea el faro de la justicia social.</p>
<p>&nbsp;</p>
<p>Foto: ANFE</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Autor: José Joaquín Fernández</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/409-españa-bienvenido-mr-chávez">
			&laquo; España: Bienvenido, Mr. Chávez		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes">
			Defaults eran los de antes &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/412-hacia-un-sistema-de-pensiones-justo-y-equitativo#startOfPageId412">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:57:"Hacia un sistema de pensiones justo y equitativo - Relial";s:11:"description";s:159:"En la opinión de José Joaquín Fernández. Miembro de la Mont Pelerin Society. &amp;nbsp; Hace unos días, el vicepresidente de la República de Costa Ric...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:48:"Hacia un sistema de pensiones justo y equitativo";s:6:"og:url";s:115:"http://www.relial.org/index.php/productos/archivo/opinion/item/412-hacia-un-sistema-de-pensiones-justo-y-equitativo";s:8:"og:title";s:57:"Hacia un sistema de pensiones justo y equitativo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/34649abc993982b197aa9a6210af69ed_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/34649abc993982b197aa9a6210af69ed_S.jpg";s:14:"og:description";s:163:"En la opinión de José Joaquín Fernández. Miembro de la Mont Pelerin Society. &amp;amp;nbsp; Hace unos días, el vicepresidente de la República de Costa Ric...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:48:"Hacia un sistema de pensiones justo y equitativo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}