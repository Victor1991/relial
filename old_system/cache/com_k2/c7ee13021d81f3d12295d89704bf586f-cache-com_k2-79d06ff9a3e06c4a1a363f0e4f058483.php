<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16100:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId339"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/4b9f9da50cf2f358abdcd4a4321104f9_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/4b9f9da50cf2f358abdcd4a4321104f9_XS.jpg" alt="Lo que los Think Tanks pro Libre Mercado deben a los &ldquo;Chicago Boys&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>&nbsp;</p>
<p>"Naturalmente, los académicos y egresados de la Universidad de Chicago -conocidos por su erudición- jugarían un papel importante en el ambiente de los <em>Think Tanks"</em></p>
<p><strong>Por Alejandro A. Chafuen</strong></p>
<p><em>P<span style="font-size: 8pt;">ublicado originalmente en la revista FORBES</span></em></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><a href="http://www.forbes.com/sites/alejandrochafuen/2013/11/27/the-fisher-recipe-for-successful-think-tanks-2/">Antony Fisher,</a> el fundador del <a href="http://www.iea.org.uk/">Instituto de Asuntos Económicos</a> (IEA por sus siglas en inglés) y líder en la creación de varios Think Tanks, sostenía que para que estos centros de pensamiento logren un impacto duradero, deberían elaborar estudios de política pública en respuesta a un problema, presentando soluciones escritas en un lenguaje lo suficientemente riguroso como para ser utilizados en las universidades, pero, a su vez accesible para aquellos que no sean expertos. Por lo tanto, los Think Tanks pro libre mercado necesitaban competir con las universidades y hacer un mayor esfuerzo para influenciar el contenido de los cursos. Para lograr ese objetivo tuvieron que atraer y publicar las obras de los pensadores más brillantes.</p>
<p>&nbsp;</p>
<p>Naturalmente, los académicos y egresados de la Universidad de Chicago -conocidos por su erudición- jugarían un papel importante en el ambiente de los Think Tanks.</p>
<p>&nbsp;</p>
<p>En 1946, la <a href="http://www.fee.org/">Fundación para la Educación Económica</a> (FEE, por sus siglas en inglés) publicó "Roofs or Ceilings?: The Current Housing Problem" escrito por Milton Friedman y George Stigler; realizaron carreras prodigiosas en la Universidad de Chicago, y ambos ganaron el premio Nobel de Economía. El Instituto de Asuntos Económicos de Antony Fisher también publicó las contribuciones de Milton Friedman, en particular su libro "Unemployment versus Inflation?" (1975).</p>
<p>&nbsp;</p>
<p>Podemos analizar la influencia de la <a href="http://www.forbes.com/colleges/university-of-chicago/">Universidad de Chicago</a> en los Think Tanks pro libre mercado en el continente americano con el caso de Chile, ya que este país nos brinda una fascinante historia de transformación económica. La influencia de Chicago comenzó formalmente en 1955, a partir de un acuerdo entre la Agencia de los Estados Unidos para el Desarrollo Internacional (USAID), la Pontificia Universidad Católica de Chile y la misma Universidad de Chicago. En 1981, cuando Friedman y miembros de la <a href="http://www.montpelerin.org/">Sociedad Mont Pelerin </a> visitaron a Chile, había más de 40 chilenos realizando sus estudios de posgrado en Chicago. Gracias al intercambio de profesores y el retorno de los graduados al país, el Instituto de Economía, un centro investigación de la Universidad Católica, comenzó a acumular una inmensa cantidad de capital humano.</p>
<p>&nbsp;</p>
<p>Sin embargo no fue Friedman, sino Arnold Harberger, quien ayudó a plantar las semillas y desarrollar más relaciones en Chile. Más allá de la influencia de Chicago en el mundo académico universitario, también influyeron en sus estupendos Think Tanks. Cristián Larroulet, quien dirigió Libertad y Desarrollo (LyD) durante dos décadas, atrajo a numerosos graduados de Chicago para su equipo. Ahora, después de cuatro años como Jefe de Gabinete del ex presidente chileno Sebastián Piñera, Larroulet está regresando al mundo de los Think Tanks, pero esta vez con un nuevo grupo de expertos en la Universidad del Desarrollo. Joaquín Lavín, graduado de Chicago como Larroulet, y quien también ocupó posiciones de liderazgo en gobiernos locales y nacionales ha estado afiliado con ambas instituciones. El <a href="http://www.cepchile.cl/dms/lang_1/home.html">Centro de Estudios Públicos</a> también tuvo en sus filas a los "Chicago boys"; por ejemplo, Juan Andrés Fontaine, quien también fue ministro de economía. Juan Andres, que continua como investigador asocidado, es el hermano de Arturo Fontaine. Este último condujo a la organización a la prominencia que hoy disfruta. Su actual Director, Harald Beyer, obtuvo su doctorado en la UCLA donde los "Chicago boys" juegan un papel destacado.</p>
<p>&nbsp;</p>
<p>La influencia de Chicago también llegó a otros países de América del Sur. En Argentina, la Universidad Nacional de Tucumán (UNT) envió a varios estudiantes a Chicago. Arnold Harberger, una vez me dijo: "No sé cómo hacen... ¡me mandan genios!" Valeriano García, ex Decano de la UNT, inició el Think Tank <a href="http://www.fundaciondeltucuman.org.ar/">Fundación de Tucumán</a>. Cuando García llegó a ser Director del Banco Mundial, su antiguo Think Tank se unió con la <a href="http://www.unsta.edu.ar/">Universidad del Norte Santo Tomás de Aquino</a> para ofrecer un MBA. Otro gran Think Tank, el Centro de Estudios Macroeconómicos de la Argentina (CEMA), que tuvo figuras clave como Carlos Rodríguez y Roque Fernández, es ahora una universidad. En varias administraciones los "Chicago boys" ocuparon cargos ministeriales, incluyendo las carteras de Economía, Defensa, Servicios de Inteligencia y temporariamente hasta la Presidencia del Banco Central. Ricardo López Murphy es el graduado argentino más activo de la Universidad de Chicago en el mundo los Think Tanks. Después de servir como Ministro de Defensa tuvo un breve desempeño como Ministro de Economía. Actualmente es Presidente de la<a href="http://www.relial.org/"> Red Liberal de América Latina (RELIAL).</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="images/Three%20Chicago%202%20-%20Copy.jpg" alt="Three Chicago 2 - Copy" width="362" height="211" /></p>
<p style="text-align: center;"><span style="font-size: 8pt; color: #999999;">Tres "Chicago Boys." De izquierda a derecha: Francisco Rosende (Chile), Manuel Suarez Mier (México), Ricardo López Murphy (Argentina)</span></p>
<p>&nbsp;</p>
<p>A pesar del gran número de "Chicago boys" que trabajaron en Argentina, sus propuestas no lograron penetrar la cultura local. Friedman, quien accedió ir a Chile, México y Perú, nunca visitó a la Argentina. Si bien Friedman era amigo de la <a href="http://www.libertad.org.ar/">Fundación Libertad </a> (Rosario), una organización que atrajo a seis premios Nobel en economía, nunca aceptó su invitación. Su razón fue directa, sencilla y profunda: "Argentina tiene muchos buenos economistas que saben lo que hay que hacer; si el gobierno no adopta esas políticas es porque los argentinos y sus líderes no las quieren".</p>
<p>&nbsp;</p>
<p>Cruzando el Río de la Plata, en Uruguay, Ernesto Talvi, miembro del Think Tank <a href="http://www.ceres-uy.org/home/index.asp">CERES</a>, es uno de los analistas económicos más respetados de su país. Su Think Tank se convirtió en una fuente confiable de análisis e información económica. De hecho, en 2012, la institución <a href="http://www.brookings.edu/">Brookings</a> estableció una relación formal con CERES gracias a su excelente reputación.</p>
<p>&nbsp;</p>
<p>Chicago también influyó en las políticas económicas mexicanas. El Instituto Tecnológico Autónomo de México (<a href="http://www.itam.mx/">ITAM),</a> una de las principales universidades mexicanas, tiene por lo menos cinco profesores con títulos de posgrado de Chicago, quienes también dirigen centros de investigación reconocidos. Desde 1993 varios Chicago boys, incluyendo a Arnold Harberger, se reúnen en la convención anual de los<a href="http://www.forbes.com/sites/alejandrochafuen/2013/12/18/its-snowing-in-mexico-think-tanks-promoting-a-free-economy/"> Álamos</a> en México. Allí comparten ideas, intercambian experiencias, y debaten retos económicos con invitados extranjeros y locales. Uno de los habitués de estas reuniones es Manuel Suárez Mier, quien jugó un papel importante en la promoción y la aprobación del Tratado de Libre Comercio de América del Norte.</p>
<p>&nbsp;</p>
<p>Brasil tuvo menor participación de los Chicago boys. Uno de ellos es el fallecido Og Leme quien impulsó con entusiasmo el desarrollo del <a href="http://www.institutoliberal.org.br/">Instituto Liberal</a> de Río de Janeiro. Se incluyen también Paulo Rabello de Castro, Presidente del <a href="http://www.instituto-atlantico.org.br/">Instituto Atlántico</a>, y José Luiz Carvalho, miembro de la <a href="http://www.montpelerin.org/">Sociedad Mont Pelerin</a>. Sin embargo, los Think Tanks pro libre mercado en Brasil han sido más influenciados por teorías económicas austríacas, que por la Universidad de Chicago.</p>
<p>&nbsp;</p>
<p>Otros países también se vieron beneficiados por las ideas de Chicago. La visita de Friedman a Perú en la década de 1980, organizada por el Instituto <a href="http://www.ild.org.pe/">Libertad y Democracia</a>, tuvo tal impacto que los artículos en la prensa escrita llenaron cuatro volúmenes encuadernados. En El Salvador y Bolivia, alumnos de posgrado que estudiaron bajo los "Chicago boys" en Chile, desempeñaron roles importantes en Think Tanks y universidades privadas.</p>
<p>&nbsp;</p>
<p>¿Cuáles son las enseñanzas que destacan a los Chicago boys en el mundo de los Think Tanks pro libre mercado? Francisco Rosende, quien hace poco terminó un largo período como Decano de la Universidad Católica de Chile, resume los aportes y las contribuciones en seis principios:</p>
<p>&nbsp;</p>
<p>1) Los incentivos importan. En su papel como consumidores o productores, la gente responde a incentivos específicos.</p>
<p>2) El mercado es, en la mayoría de los casos, la forma más eficiente de asignar recursos.</p>
<p>3) Las economías pequeñas se benefician de una manera especial con una economía abierta.</p>
<p>4) La inflación es un fenómeno ocasionado por la política monetaria.</p>
<p>5) La inflación reduce la eficiencia, así que la estabilidad de precios es un buen objetivo político.</p>
<p>6) La gestión eficiente del sector público requiere una evaluación social sistemática y rigurosa de los proyectos públicos.</p>
<p>&nbsp;</p>
<p>En 1997, cuando el optimismo por las reformas pro libre mercado en América Latina estaba en su apogeo, el Premio Nobel Gary Becker escribió: "A pesar de que los sindicatos y los viejos emporios familiares -acostumbrados a privilegios especiales- se aferrarán a las viejas políticas mercantilistas, los cambios ya iniciados por los Chicago boys en Chile han impulsado una revolución económica y política en América Latina, y es muy poco probable que ésta se invierta. Sus maestros están orgullosos de esta gloria bien merecida."</p>
<p>&nbsp;</p>
<p>Aunque estamos viendo el regreso de algunas antiguas políticas, Rosende cree que al menos en Chile, el legado de Chicago sobrevivirá al nuevo gobierno socialista. A pesar de la reputación de centro-izquierda de ciertos gobiernos latinoamericanos, vemos que en Perú y México se está desarrollando un consenso similar en torno a los seis principios. La economía de los Chicago boys, tanto en sustancia como en estilo, tiene su límite. La vida se trata de algo más que de la maximización de costos y beneficios. Sin embargo, por su respeto hacia los aspectos científicos de la economía, su formación casi monacal y rigurosa, la promoción de Think Tanks y el creciente número de personas que disfrutan de los beneficios del mercado libre, tenemos una deuda de gratitud con los Chicago boys.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="font-size: 11px;"><br /></span></p>
<p>Nota publicada y traducida gracias a la gentileza&nbsp;</p>
<p><span style="font-family: arial, helvetica, sans-serif;"><strong>Alejandro A. Chafuen, Ph.D.</strong></span></p>
<p><span style="font-family: arial, helvetica, sans-serif;"><strong>Presidente de&nbsp;Atlas Economic Research Foundation</strong></span></p>
<p><a href="http://www.forbes.com/sites/alejandrochafuen/">http://www.forbes.com/sites/alejandrochafuen/</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/327-el-parlamento-europeo-sobre-la-situación-en-venezuela">
			&laquo; El Parlamento Europeo sobre la situación en Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/340-diarios-venezolanos-reciben-papel-enviado-por-andiarios">
			Diarios venezolanos reciben papel enviado por Andiarios &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/339-lo-que-los-think-tanks-pro-libre-mercado-deben-a-los-“chicago-boys”#startOfPageId339">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:80:"Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys” - Relial";s:11:"description";s:156:"&amp;nbsp; &quot;Naturalmente, los académicos y egresados de la Universidad de Chicago -conocidos por su erudición- jugarían un papel importante en el...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:71:"Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys”";s:6:"og:url";s:141:"http://www.relial.org/index.php/productos/archivo/actualidad/item/339-lo-que-los-think-tanks-pro-libre-mercado-deben-a-los-“chicago-boys”";s:8:"og:title";s:80:"Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/4b9f9da50cf2f358abdcd4a4321104f9_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/4b9f9da50cf2f358abdcd4a4321104f9_S.jpg";s:14:"og:description";s:164:"&amp;amp;nbsp; &amp;quot;Naturalmente, los académicos y egresados de la Universidad de Chicago -conocidos por su erudición- jugarían un papel importante en el...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:71:"Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}