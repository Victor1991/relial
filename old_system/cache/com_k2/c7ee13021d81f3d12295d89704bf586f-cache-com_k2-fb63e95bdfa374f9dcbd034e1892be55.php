<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8553:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId422"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Manuel una noche echó a andar rumbo al norte
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/00c636e00bee0ba03b364841363f738b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/00c636e00bee0ba03b364841363f738b_XS.jpg" alt="Manuel una noche ech&oacute; a andar rumbo al norte" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opiniòn de Carlos A. Montaner</p>
<p>&nbsp;</p>
<p>Vale la pena, aunque pone los pelos de punta, leer el informe sobre "Extorsiones y Secuestros por Rescate" de la empresa Hazelwood Street, dirigida por el abogado Bruce Kaplan. Se puede consultar por medio de internet. De esos sangrientos y millonarios negocios viven los más siniestros grupos terroristas del planeta: los narcotraficantes, las mafias étnicas, los mareros, y todo aquel que posee un arma de fuego, y carece de escrúpulos y de temor a una casi siempre inexistente justicia.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Empiezo la historia.</p>
<p>&nbsp;</p>
<p>Digamos que se llama Manuel. No quiero facilitar ningún dato que permita que lo agarren y deporten de Estados Unidos. Es hondureño, tiene 24 años, esposa, y una niñita pequeña y revoltosa. Manuel es laborioso como una hormiga. Trabaja de sol a sol como jardinero.</p>
<p>&nbsp;</p>
<p>Hace pocos años, en su país natal, lo visitaron unos mareros conocidos en el barrio por un sanguinario historial de violencia. Le dijeron que lo necesitaban en el grupo. Manuel es alto y fuerte, y tenía una camioneta con la que trabajaba. Lo querían para traficar con cocaína y para participar en labores de extorsión. La invitación era inapelable. Si se negaba, lo mataban a él, o a su hijita, o a su mujer, o mejor a los tres. El número de cadáveres nunca es un problema en ese torturado rincón del planeta.</p>
<p>&nbsp;</p>
<p>Manuel pidió unos días para pensarlo. No tenía sentido acudir a la policía. Probablemente, algunos de los uniformados eran cómplices de los pandilleros y éstos sabrían la fuente de la denuncia. Manuel era una persona honrada y desesperada. Incluso, era religioso. Su madre, cuando chico, le leía la Biblia por las noches y le quedó la costumbre de rezarle a la Virgen de Suyapa. No quería convertirse en un delincuente. Tampoco quería morir o que le mataran a su mujer o a su hijita.</p>
<p>&nbsp;</p>
<p>Finalmente, vendió la camioneta, le dieron cuatro mil dólares, contactó a un coyote y se los entregó. Después de mil peripecias, logró cruzar la frontera e instalarse en California. Hoy sostiene a su familia con su trabajo honrado. Sueña con que su hijita, que ya habla inglés, se convierta en una americana con todos los derechos. Quiere que sea dentista cuando crezca. Le han dicho que los dentistas ganan mucho dinero.</p>
<p>&nbsp;</p>
<p>Técnicamente, Manuel es un inmigrante ilegal. En realidad, es un escapado del terror. Hay que distinguir entre quien emigra en busca de un destino mejor —lo cual es perfectamente razonable—, y el que escapa de una sociedad brutal y sin ley para que no lo maten. El matiz es trágicamente importante.</p>
<p>&nbsp;</p>
<p>Lo que falla en América Latina es el Estado de derecho. Falla por la cúpula cuando los políticos y funcionarios roban impunemente. Falla cuando los legisladores se dejan sobornar y los jueces prevarican o venden sus sentencias. Falla cuando los mandos intermedios cobran coimas y nada se puede hacer por evitarlo. Falla en la base cuando los pandilleros hacen y deshacen sin que nadie los detenga.</p>
<p>&nbsp;</p>
<p>¿Qué mensaje recibe la sociedad venezolana cuando Nicolás Maduro, Diosdado Cabello, y todo el gobierno, dirigidos por la dictadura cubana, protegen a un general acusado de narcotráfico y de asociarse con bandas criminales para cometer toda clase de delitos?</p>
<p>&nbsp;</p>
<p>El mensaje es obvio: las leyes no sirven para nada. El discurso oficial es falso. Lo que importa es enriquecerse a cualquier costo.</p>
<p>&nbsp;</p>
<p>¿Por qué los bolivianos van a respetar la ley si le han oído decir a Evo Morales que él viola las normas y allí están los abogados para arreglarlo?</p>
<p>&nbsp;</p>
<p>¿Qué pensarán los brasileros de Dilma Rousseff, los argentinos de Cristina Fernández, los uruguayos de Pepe Mujica, los ecuatorianos de Rafael Correa y los nicas de Daniel Ortega, cuando ven a sus presidentes respaldando la inmundicia venezolana y riéndole las gracias a un demente que habla con los pájaros?</p>
<p>&nbsp;</p>
<p>Piensan que sus líderes, realmente, viven encharcados en el cinismo y la mentira. Piensan que son más educados, pero no mejores que los asaltantes de las favelas.</p>
<p>&nbsp;</p>
<p>Ahí está el origen del mal: la columna vertebral de las Repúblicas es el respeto a la ley y la capacidad del Estado para proteger a las personas. Esto se ha perdido en casi toda América Latina. Por eso, Manuel, desesperado, echó a andar rumbo al norte. Cuentan que lloraba por las noches.</p>
<p>&nbsp;</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: El blog de Montaner</p>
<p>Autor: Carlos Albero Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman">
			&laquo; El legado de Milton Friedman		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho">
			El populismo está en guerra con el estado de derecho &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte#startOfPageId422">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:54:"Manuel una noche echó a andar rumbo al norte - Relial";s:11:"description";s:153:"En la opiniòn de Carlos A. Montaner &amp;nbsp; Vale la pena, aunque pone los pelos de punta, leer el informe sobre &quot;Extorsiones y Secuestros por...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:45:"Manuel una noche echó a andar rumbo al norte";s:6:"og:url";s:112:"http://www.relial.org/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte";s:8:"og:title";s:54:"Manuel una noche echó a andar rumbo al norte - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/00c636e00bee0ba03b364841363f738b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/00c636e00bee0ba03b364841363f738b_S.jpg";s:14:"og:description";s:161:"En la opiniòn de Carlos A. Montaner &amp;amp;nbsp; Vale la pena, aunque pone los pelos de punta, leer el informe sobre &amp;quot;Extorsiones y Secuestros por...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:45:"Manuel una noche echó a andar rumbo al norte";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}