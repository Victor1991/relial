<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11196:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId404"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Recuerden que el socialismo es imposible
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/dca6745fdbb9da5b038270324f6ced2f_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/dca6745fdbb9da5b038270324f6ced2f_XS.jpg" alt="Recuerden que el socialismo es imposible" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Adrián Ravier</p>
<p>&nbsp;</p>
<p>Si algo tienen en común los partidarios del socialismo y la economía pura de mercado es su crítica a las inconsistencias del capitalismo intervenido. El intervencionismo que se viene aplicando, gobierno tras gobierno, sólo suma parches que atienden a cuestiones "urgentes", pero nunca resuelven los problemas de fondo, las cuestiones "importantes". Los socialistas, sin embargo, fallan en dos aspectos centrales: primero, en diferenciar el sistema capitalista "puro" -como lo han entendido y defendido Adam Smith y Friedrich Hayek-, del sistema capitalista "intervenido" -con los parches propuestos por John Maynard Keynes y Paul Samuelson-; segundo, en comprender que "el socialismo es imposible", como han demostrado Ludwig von Mises en su artículo de 1920 y su libro 1922, y Friedrich Hayek en distintos documentos de los años 1930 y 1940, con un argumento que continúa sin respuesta, pero que muestra su validez en el fracaso de las distintas formas de socialismo en toda Europa, y ya casi podemos decir en todo el mundo.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>En este artículo sólo podré concentrarme en este último punto, el que ha sido tratado ampliamente en un libro del catedrático español Jesús Huerta de Soto titulado "Socialismo, cálculo económico y función empresarial". El libro cuenta con más de 400 páginas, pero el lector puede acceder a una reseña que personalmente escribí sobre este debate, y que fuera publicado en la revista Cuadernos de Economía (Vol. 30, Nº 54), de la Universidad Nacional de Colombia. El argumento básico explica que en un mundo de incertidumbre y conocimiento disperso, la propiedad privada es necesaria para dar lugar a los precios, pues sólo ellos pueden permitir a los empresarios advertir de ganancias y pérdidas en sus proyectos de inversión, y con ello asignar con relativa eficiencia los recursos escasos. Más en limpio, si no tenemos propiedad privada de los medios de producción, no tenemos mercados para esos medios de producción. Sin mercados para esos bienes de producción, no habrá precios. Sin precios, los empresarios no pueden advertir si sus proyectos de inversión son rentables.</p>
<p>&nbsp;</p>
<p>Si algo funciona -aún en el capitalismo intervenido- es precisamente ese proceso de prueba y error, en donde los empresarios van probando distintas inversiones, y sólo cuando son rentables, los proyectos se mantienen. Ganancias y pérdidas contables representan una información en el mercado acerca de si estamos asignando bien o mal los recursos. Y vale recordar que esos resultados son consistentes con la soberanía del consumidor, donde gana el que sabe satisfacer las necesidades del consumidor, y pierde el que no logra la demanda de sus consumidores. El socialismo propone terminar con la propiedad privada, terminar con estas señales de mercado, terminar con la función empresarial y reemplazar todo ello por la propiedad pública de los medios de producción. Aquí se abren un abanico de opciones, pero nunca ha quedado claro qué es lo que en definitiva proponen los socialistas. Y el problema es que el propio Marx careció de una propuesta concreta de cómo funcionaría el socialismo.</p>
<p>&nbsp;</p>
<p>De un lado, se propone que el gobierno administre públicamente esos medios de producción, como de hecho ocurrió en Alemania Oriental, en Rusia o actualmente es en Cuba. Aquí los problemas son al menos dos. Primero, como señaló el Premio Nobel en Economía James M. Buchanan –recientemente fallecido- el gobierno puede no tener los mejores incentivos para administrar "solidariamente" estos recursos. Si asumimos que los individuos siempre persiguen su propio beneficio, ¿por qué vamos a suponer que las personas que lleguen al poder van a tender a interesarse por el "bien común"? Buchanan insistía en que lo más probable es que estas personas tiendan siempre a alejarse de ese "bien común" y persigan más bien su propio beneficio y de aquellos a quienes representan, o que han financiado sus campañas electorales. Cuando uno mira la Argentina, ¡cuánta razón tenía!</p>
<p>&nbsp;</p>
<p>El segundo problema fue mencionado por otro premio Nobel en Economía, en este caso, Friedrich Hayek. Si aceptamos que el problema económico consiste en advertir cuáles son los bienes y servicios que deben producirse, en qué cantidad y calidad y de qué manera distribuirlos, debemos comprender que ese "conocimiento" no es dado a nadie en particular. Los bienes y servicios que necesitamos producir son los que la gente quiere. Y ese conocimiento está disperso en la sociedad, en las preferencias individuales de cada sujeto, en la forma de bits de información que cada uno tiene en su propia mente. ¡Es información no revelada! Salvo que permitamos que la gente demande y comunique esa información a los empresarios a través de los precios, precisamente.</p>
<p>&nbsp;</p>
<p>Los socialistas del siglo XXI han dado un paso atrás. Ahora se hacen llamar "socialistas de mercado", y afortunadamente han dejado de sugerir la propiedad pública de los medios de producción. En realidad se han dado cuenta de que nada es mejor que permitir que la producción de bienes y servicios la lleve adelante el mercado, lo que se traduce en alimentos, ropa y todo tipo de bienes y servicios en calidad y bajos precios, lo que es resultado precisamente del proceso competitivo.</p>
<p>&nbsp;</p>
<p>La discusión ahora se resume al rol del Estado. El "socialista de mercado" o aquellos que buscan un mayor "Estado de bienestar" piden un Estado que, paradójicamente, "intervenga", que ofrezca "bienes públicos", que evite o minimice "externalidades negativas" y subsidie las "externalidades positivas". Que aplique "políticas antimonopólicas" y "redistribuya los ingresos" de manera conveniente. Lo que no han advertido aún es que ese Estado al repartir la torta se queda con una porción enorme de la renta para beneficio propio, lo que impide la reinversión de quienes la generan -creando potenciales puestos de trabajo- y dejando a las clases más desfavorecidas sin salida.</p>
<p>&nbsp;</p>
<p>Dirán algunos pocos socialistas que este "socialismo de mercado" no es socialismo. Yo estoy de acuerdo. Dirán otros socialistas que la propuesta ideal tampoco es la propiedad pública de los medios de producción, sino la propiedad "comunal" de los medios de producción. En este caso se trataría de pequeñas comunidades de personas que manejarían las "empresas", y nótese que estas comillas no son arbitrarias. En tal caso las preguntas sin respuesta son cuantiosas. ¿Cómo se distribuyen los ingresos de esta empresa? Se dirá, quizás, que se lo hará igualitariamente, según las horas trabajadas. ¿Ganará lo mismo un ingeniero que un obrero? ¿Qué incentivo tendrá el ingeniero para capacitarse si finamente sus ingresos serán iguales? ¿Qué incentivo tendrá un obrero para trabajar eficientemente si los otros obreros no lo hacen? "Conocimiento" e "incentivos" son los dos grandes problemas del socialismo. Dejemos el socialismo para otro mundo. ¡Y por favor, dejemos de destinar tinta a un debate acabado!</p>
<p>&nbsp;</p>
<p>Autor: Adrián Ravier</p>
<p>Fuente: <a href="http://opinion.infobae.com/adrian-ravier/2013/10/28/recuerden-que-el-socialismo-es-imposible/">Infobae</a></p>
<p>Foto: Infobae</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/402-el-buen-salvaje">
			&laquo; El buen salvaje		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/406-el-misterioso-caso-de-los-comunistas-incapaces-de-aprender">
			El misterioso caso de los comunistas incapaces de aprender &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/404-recuerden-que-el-socialismo-es-imposible#startOfPageId404">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:49:"Recuerden que el socialismo es imposible - Relial";s:11:"description";s:158:"En la opinión de Adrián Ravier &amp;nbsp; Si algo tienen en común los partidarios del socialismo y la economía pura de mercado es su crítica a las inc...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:40:"Recuerden que el socialismo es imposible";s:6:"og:url";s:107:"http://www.relial.org/index.php/productos/archivo/opinion/item/404-recuerden-que-el-socialismo-es-imposible";s:8:"og:title";s:49:"Recuerden que el socialismo es imposible - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/dca6745fdbb9da5b038270324f6ced2f_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/dca6745fdbb9da5b038270324f6ced2f_S.jpg";s:14:"og:description";s:162:"En la opinión de Adrián Ravier &amp;amp;nbsp; Si algo tienen en común los partidarios del socialismo y la economía pura de mercado es su crítica a las inc...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:40:"Recuerden que el socialismo es imposible";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}