<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10676:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId362"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El mundial de la Calidad Institucional
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/ba0166c7a50d96eb270097f3f911e08a_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/ba0166c7a50d96eb270097f3f911e08a_XS.jpg" alt="El mundial de la Calidad Institucional" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Tenemos mucha suerte.</p>
<p>&nbsp;</p>
<p>El mundial de Brasil 2014 es un mundial de fútbol. Se resuelve en el verde césped y son los jugadores, técnicos, tácticas y suerte los principales determinantes de los resultados. Los partidos se definen por cantidad de goles.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>En el mundial de la calidad institucional nos iría muy mal.</p>
<p>&nbsp;</p>
<p>Supongamos, por un momento, que en lugar de goles los partidos se disputaran por la posición en el Índice de Calidad Institucional que elabora la Fundación Libertad y Progreso, bajo la dirección de Martín Krause, y edita la Red Liberal de América Latina (RELIAL), cuya edición 2014 se encuentra aquí: <a href="index.php/biblioteca/item/360-indice-de-calidad-institucional-2014">http://goo.gl/g5uek0</a></p>
<p>&nbsp;</p>
<p>Allí se mide y compara las instituciones de cada país, y refleja claramente que instituciones fuertes, sanas, protectoras de los derechos individuales y de la propiedad se correlacionan directamente con los mejores indicadores de progreso económico y social.</p>
<p>&nbsp;</p>
<p>Si el mundial de Brasil 2014 fuera un Mundial de Calidad Institucional los resultados serían bien interesantes aunque lejos de generarnos euforia nos despertaría una fuerte preocupación.</p>
<p>&nbsp;</p>
<p>Si suponemos que una mejor posición en el Índice equivale a más goles de manera que los partidos los ganan quienes mejor calidad institucional tienen, llegaríamos a los siguientes resultados.</p>
<p>&nbsp;</p>
<p>En el Grupo A quedaría afuera Brasil en primera ronda pues su calidad institucional es menor a la de México y Croacia por muy poco.</p>
<p>&nbsp;</p>
<p>Grupo A</p>
<p><strong>Croacia 72</strong></p>
<p><strong>México 80</strong></p>
<p>Brasil 94</p>
<p>Camerún 165</p>
<p>&nbsp;</p>
<p>El Grupo B es el más disputado y podría llamarse "grupo de la muerte" pues tiene el mejor promedio en términos de calidad institucional. Australia y Holanda postulan como serios candidatos.</p>
<p>&nbsp;</p>
<p>Grupo B</p>
<p><strong>Australia 6</strong></p>
<p><strong>Holanda 8</strong></p>
<p>Chile 22</p>
<p>España 31</p>
<p>&nbsp;</p>
<p>El Grupo C muestra a Japón como amplio favorito, termina en el primer puesto del grupo y Grecia remonta del año anterior y le gana por muy poco a Colombia su lugar en cuartos.</p>
<p>&nbsp;</p>
<p>Grupo C</p>
<p><strong>Japón 19</strong></p>
<p><strong>Grecia 74</strong></p>
<p>Colombia 80</p>
<p>Costa de Marfil 170</p>
<p>&nbsp;</p>
<p>El grupo D, que en términos futbolísticos es el grupo de la muerte, se resuelve muy fácil a favor de Inglaterra que tiene uno de los mejores niveles de calidad institucional y muestra la primera sorpresa. Uruguay le gana el lugar a Italia.</p>
<p>&nbsp;</p>
<p>Grupo D</p>
<p><strong>Inglaterra 10</strong></p>
<p><strong>Uruguay 43</strong></p>
<p>Costa Rica 49</p>
<p>Italia 58</p>
<p>&nbsp;</p>
<p>El Grupo E no muestra sorpresas. Suiza es un actor estable en el Índice de Calidad Institucional.</p>
<p>&nbsp;</p>
<p>Grupo E</p>
<p><strong>Suiza 3</strong></p>
<p><strong>Francia 24</strong></p>
<p>Honduras 132</p>
<p>Ecuador 143</p>
<p>&nbsp;</p>
<p>Grupo F: Dios los cría y el bolillero los junta. Argentina comparte el grupo con peor promedio de calidad institucional. Casualmente o no, todos los países del Grupo F muestran scores superiores a 100 lejísimo de los primeros niveles y demostrativos de los resultados económicos y sociales de cada país. Bosnia y Argentina "se ganaron en buena ley" su lugar frente a los flojitos de instituciones Irán y Nigeria. Patético grupo.</p>
<p>&nbsp;</p>
<p>Grupo F</p>
<p><strong>Bosnia-Herzegovina 102</strong></p>
<p><strong>Argentina 134</strong></p>
<p>Nigeria 161</p>
<p>Irán 168</p>
<p>&nbsp;</p>
<p>Los Grupos G y H no muestran sorpresas.</p>
<p>&nbsp;</p>
<p>Grupo G</p>
<p><strong>Estados Unidos 11</strong></p>
<p><strong>Alemania 13</strong></p>
<p>Portugal 32</p>
<p>Ghana 79</p>
<p>&nbsp;</p>
<p>Grupo H</p>
<p><strong>Bélgica 19</strong></p>
<p><strong>Corea del Sur 27</strong></p>
<p>Rusia 135</p>
<p>Algeria 157 (Índice 2013)</p>
<p>&nbsp;</p>
<p>Una vez definidos los grupos vamos a los cruces de octavos y cuartos de final:</p>
<p>&nbsp;</p>
<p>Croacia (72) vs Holanda (8): Clasifica Holanda</p>
<p>&nbsp;</p>
<p>Japón (19) vs Uruguay (43): Clasifica Japón</p>
<p>&nbsp;</p>
<p>Suiza (3) vs Argentina (134: Se nos acabó la suerte. Nos aplastó Suiza. En términos de calidad institucional Argentina superó por fortuna la primera ronda pero nos golearon en octavos.</p>
<p>&nbsp;</p>
<p>EEUU (11) vs Corea del Sur (27): Clasifica USA</p>
<p>&nbsp;</p>
<p>Australia (6) vs México (80): Clasifica Australia</p>
<p>&nbsp;</p>
<p>Inglaterra (10) vs Grecia (74): Clasifica Inglaterra</p>
<p>&nbsp;</p>
<p>Bosnia (102) vs Francia (24): Apabulla Francia</p>
<p>&nbsp;</p>
<p>Bélgica (19) vs Alemania (13): Muy ajustado triunfo alemán</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Los cuartos de final: Holanda vs Japón, Suiza vs Estados Unidos, Australia vs Inglaterra, y Francia vs Alemania definen el cuadro final.</p>
<p>&nbsp;</p>
<p>En la semifinal Suiza (3) supera a Holanda (10). Australia (6) vencería a Alemania (13).</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>A la final los dos países con mejor calidad institucional.</p>
<p>Suiza será el campeón superando a Australia. Dos campeones históricos en materia de calidad institucional.</p>
<p>&nbsp;</p>
<p>Más allá de esta analogía el Mundial siempre es una oportunidad extraordinaria para nuestro país. No sólo por la pasión futbolera y la gesta deportiva, sino porque nuestros funcionarios van a estar viendo cada partido. Seguramente el Congreso y las oficinas del poder ejecutivo y toda la administración se paralizarán cada vez que el juez marque el inicio de cada uno de los partidos.</p>
<p>&nbsp;</p>
<p>Es una oportunidad para que nuestros funcionarios observen la globalización, la capacidad de superación de cada país, pensar que los abuelos de muchos jugadores alguna vez se enfrentaron en un campo de batalla mientras sus nietos ahora, disputan una pelota. Significa un enorme avance de la humanidad. Nuestros funcionarios podrían ver como cada uno de los países cuyas selecciones compiten superaron problemas, guerras, enfrentamientos, caos económicos y sociales.</p>
<p>&nbsp;</p>
<p>El mundial siempre es una oportunidad para ver el mundo, ese mundo al cual nos cerramos y no se vino abajo. Ese mundo tiene muchas enseñanzas para nosotros.</p>
<p>&nbsp;</p>
<p>Foto: Libertad y Progreso</p>
<p>Fuente: Fortuna Web</p>
<p>Autor: Gustavo Lazzari, economista y colaborador de Libertad y Progreso</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/361-ideas-y-pensamientos-de-valor-en-los-30-años-de-cedice-libertad">
			&laquo; Ideas y pensamientos de valor en los 30 años de CEDICE Libertad		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/378-iaf-curso-profiling-political-liberalism-as-an-effective-force-for-progress">
			IAF curso Profiling Political Liberalism as an Effective Force for Progress &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/362-el-mundial-de-la-calidad-institucional#startOfPageId362">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"El mundial de la Calidad Institucional - Relial";s:11:"description";s:157:"Tenemos mucha suerte. &amp;nbsp; El mundial de Brasil 2014 es un mundial de fútbol. Se resuelve en el verde césped y son los jugadores, técnicos, táct...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"El mundial de la Calidad Institucional";s:6:"og:url";s:105:"http://www.relial.org/index.php/productos/archivo/opinion/item/362-el-mundial-de-la-calidad-institucional";s:8:"og:title";s:47:"El mundial de la Calidad Institucional - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/ba0166c7a50d96eb270097f3f911e08a_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/ba0166c7a50d96eb270097f3f911e08a_S.jpg";s:14:"og:description";s:161:"Tenemos mucha suerte. &amp;amp;nbsp; El mundial de Brasil 2014 es un mundial de fútbol. Se resuelve en el verde césped y son los jugadores, técnicos, táct...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:38:"El mundial de la Calidad Institucional";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}