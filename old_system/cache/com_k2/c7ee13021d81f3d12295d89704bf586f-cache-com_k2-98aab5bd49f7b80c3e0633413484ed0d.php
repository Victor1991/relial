<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9327:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId405"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Analizan realidades y perspectivas del constitucionalismo hondureño
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/391d45802a606be64095bd7b66c67316_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/391d45802a606be64095bd7b66c67316_XS.jpg" alt="Analizan realidades y perspectivas del constitucionalismo hondure&ntilde;o" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La Fundación Friedrich Naumann y la Universidad de San Pedro Sula (USAP), instalaron ayer el Tercer Congreso Internacional de Derecho Constitucional, para analizar las realidades y perspectivas del constitucionalismo hondureño.</p>
<div>&nbsp;</div>
<div>Mary Eli Martínez, directora de la carrera de Derecho de ese centro universitario, dijo que el tema central es analizar la realidad del derecho constitucional en Honduras.</div>
<div>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>"Invitamos a expositores internacionales para discutir a la luz de lo que está pasando en otros países para ver qué retos y perspectivas hay en el país. Algunos temas que se darán a conocer son la independencia de los poderes en un Estado de Derecho constituido; el nuevo constitucionalismo latinoamericano; la democracia y el control de constitucionalidad", mencionó Martínez.</p>
<p>&nbsp;</p>
<p>En el evento expondrán tres conferencistas internacionales; el doctor Rubén Darío Cuellar, coordinador del observatorio de Derechos Humanos de la Fundación Nueva Democracia de Bolivia, el doctor Carlos Sabino de la Universidad Francisco Marroquín de Guatemala, y el doctor Guillermo Lousteau de Interamerican Institute for Democracy de Estados Unidos.</p>
<p>&nbsp;</p>
<p>Además habrá expositores nacionales entre ellos los abogados; Ramón Barrios, Oswaldo Ramos Soto, Omar Menjívar, Joaquín Mejía, quien es litigante en la Corte Penal Internacional.</p>
<p>&nbsp;</p>
<p>Para los catedráticos de la USAP estas conferencias son importantes porque consideran que están formando a los futuros abogados, congresistas, magistrados y jueces.</p>
<p>&nbsp;</p>
<p>"Queremos que los jóvenes asuman la responsabilidad y por eso los involucramos en estos temas que puedan reflexionar y formarse su criterio", manifestó Martínez.</p>
<p>&nbsp;</p>
<p>El congreso culmina el próximo jueves. Las exposiciones de los conferencistas se desarrollan de 3:30 a 8:30 de la noche.</p>
<p>&nbsp;</p>
<p>INDEPENDENCIA</p>
<p>Uno de los conferencistas internacionales, abogado Rubén Darío Cuéllar, quien es originario de Bolivia, expresó que quiere hacerle conocer al público la situación de la independencia de poderes en su país y la vigencia del Estado de Derecho.</p>
<p>&nbsp;</p>
<p>Cuéllar mencionó que desde hace unos años conduce un observatorio de derechos humanos que consiste en monitorear y sistematizar toda la información referida a las violaciones de derechos políticos de los ciudadanos.</p>
<p>&nbsp;</p>
<p>Explicó que dentro de ese campo hacen seguimiento a cuatro ejes temáticos entre los que están; la suspensión de autoridades ilegítimamente elegidas, persecución judicial por motivaciones políticas, discriminación de accesos a cargos públicos o de accesos a cualquier servicio y la suspensión de autoridades judiciales.</p>
<p>&nbsp;</p>
<p>"Acabamos de incorporar un eje de la persecución y la violencia contra las mujeres en función de cargos electivos porque en Bolivia la totalidad de mujeres que fueron elegidas en el año 2010 el 39 por ciento se ha visto obligada a renunciar por el ejercicio de violencia contra ellas y un 48 por ciento declara haber sido objeto de violencia emocional o física", dijo el constitucionalista.</p>
<p>&nbsp;</p>
<p>PODERES</p>
<p>Por su parte Rosbinda Sabillón, representante de la Fundación Friedrich Naumann en Honduras, señaló que esta actividad es para que el joven compare si en Honduras existe la separación de poderes y si existe un verdadero Estado de Derecho.</p>
<p>&nbsp;</p>
<p>"Si el Estado de Derecho no existe difícilmente podremos tener una economía de mercado sana y atraer al productor o al inversionista en Honduras", refirió Sabillón.</p>
<p>&nbsp;</p>
<p>David Vincent, quien es el director de América Central de la fundación, explicó que esta institución es una de las fundaciones políticas alemanas que fue fundada hace 60 años después de la Segunda Guerra Mundial.</p>
<p>&nbsp;</p>
<p>"La idea de crear la fundación es para apoyar al entendimiento de la democracia en el pueblo alemán por lo que años después comenzamos a trabajar en todo el mundo y estamos en 60 países para apoyar a las instituciones democráticas", finalizó Vincent.</p>
<p>&nbsp;</p>
<p>CONGRESO</p>
<p>El Tercer Congreso Internacional de Derecho Constitucional, tendrá una duración de tres días dese el 24, 25 y jueves 26 de junio a partir de las 3:30 de la tarde, y se realizará en el Auditorio Jorge Emilio Jaar de la USAP.</p>
<p>&nbsp;</p>
<p>FRASE</p>
<p>"Esta actividad es un tema de interés para toda la sociedad hondureña, y que todos los estudiantes están invitados, así mismo, toda persona particular que le interese el congreso Internacional de Derecho Constitucional": Mary Eli Martínez.La Fundación Friedrich Naumann y la Universidad de San Pedro Sula (USAP), instalaron ayer el Tercer Congreso Internacional de Derecho Constitucional, para analizar las realidades y perspectivas del constitucionalismo hondureño.</p>
<p>&nbsp;</p>
<p>Foto: Fundación Friedrich Naumann</p>
<p>Fuente: <a href="http://www.tiempo.hn/portada/noticias/analizan-realidades-y-perspectivas-del-constitucionalismo-hondureno">Tiempo Honduras</a></p>
<p>Autor:&nbsp;<a href="http://www.tiempo.hn/portada/noticias/analizan-realidades-y-perspectivas-del-constitucionalismo-hondureno">Tiempo Honduras</a></p>
</div>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/398-tiempo-de-canallas-fusilamiento-al-amanecer">
			&laquo; Tiempo de canallas, fusilamiento al amanecer		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/417-celebrando-a-milton-friedman-en-sus-102-años">
			Celebrando a Milton Friedman en sus 102 años &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/405-analizan-realidades-y-perspectivas-del-constitucionalismo-hondureño#startOfPageId405">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:77:"Analizan realidades y perspectivas del constitucionalismo hondureño - Relial";s:11:"description";s:153:"La Fundación Friedrich Naumann y la Universidad de San Pedro Sula (USAP), instalaron ayer el Tercer Congreso Internacional de Derecho Constitucional,...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:68:"Analizan realidades y perspectivas del constitucionalismo hondureño";s:6:"og:url";s:138:"http://www.relial.org/index.php/productos/archivo/actualidad/item/405-analizan-realidades-y-perspectivas-del-constitucionalismo-hondureño";s:8:"og:title";s:77:"Analizan realidades y perspectivas del constitucionalismo hondureño - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/391d45802a606be64095bd7b66c67316_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/391d45802a606be64095bd7b66c67316_S.jpg";s:14:"og:description";s:153:"La Fundación Friedrich Naumann y la Universidad de San Pedro Sula (USAP), instalaron ayer el Tercer Congreso Internacional de Derecho Constitucional,...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:68:"Analizan realidades y perspectivas del constitucionalismo hondureño";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}