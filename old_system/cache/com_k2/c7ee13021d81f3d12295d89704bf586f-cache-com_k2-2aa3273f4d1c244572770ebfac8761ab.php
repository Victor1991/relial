<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9714:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId472"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Muerte del fiscal Alberto Nisman despierta dudas e indignación en Argentina
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/4739b6c64144f72975550c5e8df1b948_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/4739b6c64144f72975550c5e8df1b948_XS.jpg" alt="Muerte del fiscal Alberto Nisman despierta dudas e indignaci&oacute;n en Argentina" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En el mayor atentado terrorista en la historia de la República Argentina, el 18 de julio de 1994 murieron 85 personas en la voladura de la AMIA (Asociación Mutual Israelita Argentina). El atentado, todavía sin esclarecer, se cree que fue mediante la explosión de un coche bomba (camioneta Trafic) detonado por un conductor suicida. Luego de varias pistas falsas e investigaciones que han sido confirmadas como equívocas, se fortaleció la teoría de que el atentado estaba vinculado a la organización terrorista Hezbollah y a varios funcionarios iraníes.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p><br />Desde 1996 el fiscal Alberto Nisman se dedicó a seguir la pista de la investigación que relacionaba al atentado con una venganza contra el gobierno de Carlos Menem por la interrupción de un acuerdo previo de transferencia de tecnología nuclear al gobierno iraní.</p>
<p>&nbsp;</p>
<p>Durante los últimos años del gobierno de Cristina Fernández de Kirchner, el Poder Ejecutivo argentino fue fuertemente cuestionado por lo que se comprendió como un acercamiento al gobierno de Irán. Sobre todo con la firma de un acuerdo aprobado por el oficialismo, donde se resolvía que los fiscales argentinos puedan interrogar a diferentes sospechosos en Irán. Este acuerdo (aprobado en el Congreso solo mediante los votos del partido gobernante) fue desestimado por la justicia como inconstitucional y fue interpretado por los medios de comunicación, sectores de la oposición y gran parte de la sociedad civil como un pacto de impunidad.</p>
<p>&nbsp;</p>
<p>Recientemente, el fiscal Alberto Nisman sacudió el escenario político cuando denunció a la presidente Cristina Fernández de Kirchner, al canciller Héctor Tímerman y a otros funcionarios de gobierno por la existencia de un pacto de impunidad con el gobierno iraní. La denuncia, que incluyó un embargo a la presidente por 200 millones de pesos, señalaba las supuestas pruebas que el Poder Ejecutivo, mediante diferentes enviados, había pactado impunidad por el atentado a la AMIA a cambio de un tratado comercial que intercambiaba carne y cereales argentinos por importaciones relacionadas al ámbito energético. Según Nisman el gobierno argentino tenía planeado conducir nuevamente la causa del atentado hacia una pista falsa, cerrar la sospecha sobre las vinculaciones con Irán y de esa manera celebrar el acuerdo comercial que supuestamente se había pactado entre funcionarios argentinos e iraníes.</p>
<p>&nbsp;</p>
<p>Estas supuestas evidencias, entre varios materiales, incluían escuchas telefónicas donde, según Nisman, los miembros del gobierno nacional, como el diputado Larroque, quedaban absolutamente expuestos. Estas pistas, según el fiscal fallecido, ocupaban más de 300 CD´s de información. Durante los últimos días el país estuvo en vilo y con todas las expectativas puestas en el lunes 19 de enero, ya que era el día en el que el fiscal se presentaría en el Congreso para mostrar nuevas evidencias, que según anticipó, revelarían la intención del gobierno en liberar a Irán de la responsabilidad del atentado.</p>
<p>&nbsp;</p>
<p>En los días previos a este lunes donde se realizaría el encuentro en el Congreso Nacional, el fiscal Nisman fue víctima de constantes ataques desde sectores del gobierno y los medios de comunicaciones estatales y afines al kirchnerismo. Buenos Aires hace varios días que amanecía con carteles de tono amenazante en defensa de la presidente con leyendas como "No lo intenten" o "Cristina somos todos" mientras que los voceros oficiales anunciaban que sectores de la justicia buscaban destituir a la presidente en una especie de "golpe de Estado judicial".</p>
<p>&nbsp;</p>
<p>La presentación en el Congreso, planeada para las 15:00 del lunes 19 de enero de 2015 jamás se realizó ya que el fiscal Alberto Nisman fue encontrado en su departamento del barrio de Puerto Madero con un disparo en la cabeza en la noche del domingo 18. La sensación general durante la madrugada del lunes en la calle y en los medios de comunicación fue que de alguna manera el gobierno había logrado deshacerse de su gran amenaza horas antes de su declaración en el Congreso.</p>
<p>&nbsp;</p>
<p>En relación a las contrapartes de la FNF, la diputada nacional Patricia Bullrich ha contado con gran notoriedad en las últimas horas ya que como titular de la Comisión de Derecho Penal fue una de las legisladoras que más estuvo en contacto con el fiscal antes de su muerte. Durante la madrugada manifestó que si bien debía ser prudente, que no había notado en el comportamiento del fiscal nada que suponga un posible suicidio horas antes de la presentación de las pruebas que había prometido. En este mismo sentido, Ricardo López Murphy manifestó que sin dudas se trata de un hecho de extrema gravedad institucional y que en lo personal le recordaba a esos "suicidios" que tenían lugar en la Unión Soviética cuando el régimen se deshacía de personas que le resultaran incómodas.</p>
<p>&nbsp;</p>
<p>Por su parte, la Fundación Libertad y Progreso convocó abiertamente a la ciudadanía a una marcha en repudio por la muerte del fiscal y su director, Agustín Etchebarne, manifestó públicamente que el hecho es la demostración de que Argentina se encuentra gobernada por una "mafia". El sentimiento durante la jornada de ayer en las redes sociales fue de total indignación y solo los medios oficialistas descuentan que se trató de un simple suicidio. Miles de personas salieron ayer a las calles en señal de protesta, se espera que las manifestaciones continúen.</p>
<p>&nbsp;</p>
<p>Autor: Marcelo Duclos</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: Fundación Friedrich Naumann para la Libertad</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/471-los-cinco-errores-de-obama-en-su-nueva-política-sobre-cuba">
			&laquo; Los cinco errores de Obama en su nueva política sobre Cuba		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/473-¿qué-esconde-la-alfombra-roja-de-la-celac?">
			¿Qué esconde la alfombra roja de la CELAC? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/472-muerte-del-fiscal-alberto-nisman-despierta-dudas-e-indignación-en-argentina#startOfPageId472">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:85:"Muerte del fiscal Alberto Nisman despierta dudas e indignación en Argentina - Relial";s:11:"description";s:154:"En el mayor atentado terrorista en la historia de la República Argentina, el 18 de julio de 1994 murieron 85 personas en la voladura de la AMIA (Asoci...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:76:"Muerte del fiscal Alberto Nisman despierta dudas e indignación en Argentina";s:6:"og:url";s:148:"http://www.relial.org/index.php/productos/archivo/actualidad/item/472-muerte-del-fiscal-alberto-nisman-despierta-dudas-e-indignaciÃ³n-en-argentina";s:8:"og:title";s:85:"Muerte del fiscal Alberto Nisman despierta dudas e indignación en Argentina - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/4739b6c64144f72975550c5e8df1b948_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/4739b6c64144f72975550c5e8df1b948_S.jpg";s:14:"og:description";s:154:"En el mayor atentado terrorista en la historia de la República Argentina, el 18 de julio de 1994 murieron 85 personas en la voladura de la AMIA (Asoci...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:76:"Muerte del fiscal Alberto Nisman despierta dudas e indignación en Argentina";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}