<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5906:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId480"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Se incrementa la represión económica en Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Se incrementa la represión económica en Venezuela</p>
<p>Comunicado - Caracas, 07 de febrero de 2015</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>1. El gobierno venezolano ha transformado una falacia en la justificación de la represión. La "guerra económica" se ha convertido en la excusa ideológica para reprimir empresarios, violar los derechos de propiedad, amedrentar dirigentes gremiales y crear un ambiente de miedo y de silencios obligados.</p>
<p>&nbsp;</p>
<p>2. Los resultados de la peor política económica posible -la del socialismo del Siglo XXI- se expresan en escasez e inflación. La gente sabe que el desabastecimiento y el alto costo de la vida son irreductibles sin el cambio de las políticas. Se protege frente a un situación peor y llenan las calles de colas que se realizan frente a supermercados, cadenas de farmacias y tiendas, sin importar que sean públicas o privadas.</p>
<p>&nbsp;</p>
<p>3. El gobierno insiste en practicar las causas del desastre: Control cambiario y controles de costos, precios, logística de distribución, e incluso define qué es lo que se debe producir, cuanto se debe producir y los precios. Somete así al sector privado a presiones insoportables desde el punto de vista de la dinámica económica.</p>
<p>&nbsp;</p>
<p>4. El gobierno se ha valido de una ley de costos y precios, que en su momento denunciamos como inconstitucional, para acusar a las empresas de boicot y obstaculización de la economía. Ha detenido a empresarios y expropiado empresas, sometiéndolos a expropiación, multas y penas de cárcel muy gravosas. No hay debido proceso, ni legítima defensa, ni garantías expropiatorias. El empresariado venezolano está sufriendo amedrentamiento, extorsión y persecución. Son los nuevos presos políticos del gobierno y el régimen lo hace con absoluta impunidad.</p>
<p>&nbsp;</p>
<p>5. Esta situación tiene que denunciarse: No hay Estado de Derecho. No hay Derechos de Propiedad. No hay Justicia. Estamos sufriendo la represión pura y dura y el daño acumulado de una ideología frustrada que arremete contra la realidad y busca culpables de sus propios fracasos. Apelamos a la solidaridad de la sensatez y de los valores libertarios para poner a la luz esta terrible situación.</p>
<p>&nbsp;</p>
<p>Los invitamos a ver en la pagina del programa&nbsp;<a href="http://paisdepropietarios.org/home/">www.paisdepropietarios.org</a> más información</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/478-en-relación-a-la-ley-para-permitir-la-fuerza-letal-con-manifestantes-en-venezuela">
			&laquo; En relación a la Ley para permitir la fuerza letal con manifestantes en Venezuela (inglés)		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/488-educación-y-libertad-garantías-para-una-sociedad-armónica">
			Educación y Libertad, garantías para una sociedad armónica &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/480-se-incrementa-la-represión-económica-en-venezuela#startOfPageId480">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:60:"Se incrementa la represión económica en Venezuela - Relial";s:11:"description";s:155:"Se incrementa la represión económica en Venezuela Comunicado - Caracas, 07 de febrero de 2015 &amp;nbsp; 1. El gobierno venezolano ha transformado una...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:51:"Se incrementa la represión económica en Venezuela";s:6:"og:url";s:125:"http://www.relial.org/index.php/productos/archivo/actualidad/item/480-se-incrementa-la-represiÃ³n-econÃ³mica-en-venezuela";s:8:"og:title";s:60:"Se incrementa la represión económica en Venezuela - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:159:"Se incrementa la represión económica en Venezuela Comunicado - Caracas, 07 de febrero de 2015 &amp;amp;nbsp; 1. El gobierno venezolano ha transformado una...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:51:"Se incrementa la represión económica en Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}