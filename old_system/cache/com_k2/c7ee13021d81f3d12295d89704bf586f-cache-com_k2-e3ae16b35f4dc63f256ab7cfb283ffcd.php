<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10655:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId466"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La Normalización
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/23e58ccd18e32cab182dbd6268a12868_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/23e58ccd18e32cab182dbd6268a12868_XS.jpg" alt="La Normalizaci&oacute;n" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Barack Obama ha comenzado la normalización de las relaciones con la dictadura cubana. Es lo que le pedía el cuerpo. En su discurso y en sus planteamientos ha ido mucho más allá de lo que se podía prever. Al fin y al cabo, como dijo en su alocución, él ni siquiera había nacido cuando el presidente John F. Kennedy decretó el embargo en 1961. Era un pleito que lo dejaba indiferente. Supongo que hasta lo aburría.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Para mí no hay duda de que se trata de un triunfo político total por parte de la dictadura cubana. En La Habana están eufóricos. Washington ha hecho una docena de concesiones unilaterales. Cuba, en cambio, se ha limitado a farfullar unas cuantas consignas.</p>
<p>&nbsp;</p>
<p>Es verdad que Raúl Castro ha puesto en libertad a medio centenar de presos políticos y ha liberado a Alan Gross a cambio de tres espías. Pero sólo este año ha detenido a más de dos mil opositores y ha aporreado a cientos de ellos, y muy especialmente a las sufridas "Damas de Blanco".</p>
<p>&nbsp;</p>
<p>En realidad, Obama no había cambiado antes la política cubana por razones electorales. Ese es el factor esencial en la esfera pública. Manda su majestad la urna. Esperó al término de las elecciones parciales de su segundo mandato –las últimas en las que participaría su partido durante su presidencia-- y a que el senado entrara en receso. Entonces actuó.</p>
<p>&nbsp;</p>
<p>Una de las pocas ventajas de ser un lame duck es que no se paga un precio electoral. Por lo menos no lo paga el presidente en funciones, aunque a lo mejor tiene que abonarlo el candidato de su partido en los comicios posteriores.</p>
<p>&nbsp;</p>
<p>Al Gore –por ejemplo—nunca le perdonó a Bill Clinton el tipo de solución que le dio al caso del niño balsero Elián González. Perdió Florida por 536 votos –los cubanos votaron mayoritaria y furiosamente en su contra-- y en ese estado se liquidaron sus sueños de llegar a la presidencia.</p>
<p>&nbsp;</p>
<p>Previamente al discurso de Obama y a su cambio de política, The New York Times había ablandado a la opinión pública con un bombardeo de siete editoriales consecutivos en los que solicitaba lo que inmediatamente se iba a conceder.</p>
<p>&nbsp;</p>
<p>No era la influencia de la prensa sobre la Casa Blanca. Era al revés: era la influencia de la Casa Blanca sobre la prensa para lograr objetivos políticos. En esos editoriales estaba la hoja de ruta del cambio de la política norteamericana con relación a Cuba. Ahora se entiende la campaña del NYT. No era buen periodismo. Eran buenas relaciones públicas.</p>
<p>&nbsp;</p>
<p>Los argumentos de Obama para revertir la estrategia política seguida por una decena de presidentes republicanos y demócratas previos fueron principalmente dos: primero, no ha dado resultados, y, segundo, Estados Unidos mantiene relaciones con países como China y Vietnam. Dos dictaduras nominalmente comunistas.</p>
<p>&nbsp;</p>
<p>En cuanto a los resultados del embargo contra el régimen cubano, no es eso lo que sostiene el gobierno de los Castro. La Habana afirma que el embargo, originado por la confiscación sin compensación de las propiedades norteamericanas en la Isla, les ha costado miles de millones de dólares.</p>
<p>&nbsp;</p>
<p>Por otra parte, lo cierto es que, desde que Kennedy puso en marcha el embargo, esa operación de castigo, si bien no sirvió para que Cuba compensara a los legítimos propietarios, ni para derrocar al régimen, fue útil para que ningún otro país latinoamericano se atreviera a confiscar sin pago empresas norteamericanas, mientras (alegan algunos estrategas) contribuyó a que la Isla se viera obligada a reducir sus fuerzas armadas a la mitad tras la debacle soviética en 1991.</p>
<p>&nbsp;</p>
<p>Es irrebatible que Estados Unidos tiene relaciones plenas con China y Vietnam, de donde Obama, como mucha gente, deduce que debía tener buenos vínculos con Cuba, pero la premisa es muy discutible y está basada en una visión pragmática de las relaciones internacionales en la que no intervienen los juicios morales.</p>
<p>&nbsp;</p>
<p>Si ése es el caso, ¿por qué no tener relaciones normales con Siria si las tienen con Arabia Saudita, que es otra tiranía islámica? ¿Por qué no tratar con indiferencia al Califato (ISIS) que ha surgido en un rincón de Siria y hoy hace metástasis por todo el Oriente medio? ¿Que Siria y el califato matan y atropellan? En China y en Vietnam también matan y atropellan. En rigor, desde la perspectiva estrictamente pragmática, ¿qué le importa a Estados Unidos que los talibanes sean una banda de asesinos si los muertos ocurren en una zona alejada del mundo?</p>
<p>&nbsp;</p>
<p>Hay una regla de oro de la ética que Obama ha olvidado: donde quiera que se pueda sostener la coherencia entre la conducta y los principios, hay que hacerlo. Uno puede entender que es sensato tener relaciones normales con China, un gigante demográfico y nuclear, porque las consecuencias de defender los principios puede llevarnos a la catástrofe. Lo mismo sucede con Arabia saudita y su maldito petróleo, pero en Cuba es diferente.</p>
<p>&nbsp;</p>
<p>En Cuba, Estados Unidos podía evitar la disonancia moral porque la Isla, violadora pertinaz de los derechos humanos, enemiga a muerte de Estados Unidos al extremo de pedirle a la URSS el exterminio nuclear preventivo del país vecino, que ya ha vertido el 20% de su población dentro del territorio norteamericano, no tiene la menor significación demográfica o económica y era posible casar coherentemente los valores y los comportamientos.</p>
<p>&nbsp;</p>
<p>Durante todo el siglo XX, con razón, muchos latinoamericanos criticaron a Estados Unidos por tener buenas relaciones con dictadores como Stroessner, Pinochet, Batista, Trujillo o Somoza. Entonces se decía que era una total hipocresía de Washington invocar los valores de la libertad y la democracia mientras tenía relaciones estrechas con los opresores de sus pueblos.</p>
<p>&nbsp;</p>
<p>Como consecuencia de ese reclamo, el 11 de septiembre del 2001, mientras ardían las Torres Gemelas, se firmó en Lima la Carta Democrática de la OEA, un documento impulsado por Estados Unidos en el que se perfilaban todos los rasgos que debían tener las naciones del continente para ser consideradas, realmente, democráticas.</p>
<p>&nbsp;</p>
<p>De cierta manera, esos eran los rasgos de la normalidad democrática. Obama, que cita el documento, acaba de traicionar su esencia. Ha normalizado las relaciones con Cuba, pero al precio de volver a la nefasta política de la indiferencia moral en América Latina. Esa disonancia es una desgracia.</p>
<p>&nbsp;</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/465-todos-con-maría-corina-machado">
			&laquo; Todos con María Corina Machado.		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/467-“todos-somos-americanos”-estados-unidos-y-cuba">
			“Todos somos americanos”: Estados Unidos y Cuba &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/466-la-normalización#startOfPageId466">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:26:"La Normalización - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Barack Obama ha comenzado la normalización de las relaciones con la dictadura cubana. Es lo que le...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:17:"La Normalización";s:6:"og:url";s:90:"http://www.relial.org/index.php/productos/archivo/opinion/item/466-la-normalizaciÃƒÂ³n";s:8:"og:title";s:26:"La Normalización - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/23e58ccd18e32cab182dbd6268a12868_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/23e58ccd18e32cab182dbd6268a12868_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Barack Obama ha comenzado la normalización de las relaciones con la dictadura cubana. Es lo que le...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:17:"La Normalización";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}