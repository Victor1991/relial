<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5983:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId382"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Venezuela: Análisis económico de un país en crisis
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/e561084e9220383c7204eb40f9bc13f1_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/e561084e9220383c7204eb40f9bc13f1_XS.jpg" alt="Venezuela: An&aacute;lisis econ&oacute;mico de un pa&iacute;s en crisis" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Durante los últimos quince años en Venezuela se adoptó un sistema político con alto intervencionismo estatal en todos los ámbitos de la vida en sociedad, que sus promotores denominaron "Socialismo del Siglo XXI". El orden institucional se tornó altamente discrecional para aplicar políticas populistas clientelares, ejerciendo un poder casi hegemónico por el uso de la fueza o la amenaza del uso de la fuerza. Las libertades políticas, económicas y civiles se han visto cada vez más restringidas, dado que el régimen ha interpretado, cambiado y acomodado la constitución y las leyes a los intereses del grupo gobernante. Esto ha provocado abuso de poder político, corrupción rampante, altos niveles de inseguridad ciudadana, escasez, inflación, restricción a los derechos de asociación y expresión, entre otros.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>El régimen autoritario chavista ha concentrado el poder a través del control de las instituciones políticas y la toma de la economía por controles de precios, cambiarios, expropiaciones, dirección del crédito y la explotación de la empresa estatal petrolera, entre otros. En noviembre del 2013 entró en vigencia una <em>Ley Habilitante</em> otorgando poder casi ilimitado y discrecional al Presidente de Venezuela para emitir decretos con fuerza de ley. En definitiva, la concentración de poder se ha alimentado de sofocar los derechos individuales de los ciudadanos. Los venezolanos iniciaron protestas desde el inicio de febrero del presente año que todavía siguen en pie. Lo que presenciamos es un país bajo una crisis económica, social y política sin precedentes en ese país. No se vislumbra una salida fácil ni rápida, lo que implicará mayor deterioro de la libertad y, consecuentemente, reducción en la calidad de vida de los venezolanos.</p>
<p>&nbsp;</p>
<p>Para más información y leer el reporte completo dar <a href="index.php/biblioteca/item/380-venezuela-an%C3%A1lisis-econ%C3%B3mico-de-un-pa%C3%ADs-en-crisis">clic aquí.</a></p>
<p>&nbsp;</p>
<p>Foto: CREES</p>
<p>Fuente: Texto proporcionado por el Centro Regional de Estrategias Económicas Sostenibles</p>
<p>Autores: Ernesto Selman y Rafael Fornet</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/379-sobre-la-libertad">
			&laquo; Sobre la libertad		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor">
			No más barreras a la creación de valor &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis#startOfPageId382">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:62:"Venezuela: Análisis económico de un país en crisis - Relial";s:11:"description";s:158:"Durante los últimos quince años en Venezuela se adoptó un sistema político con alto intervencionismo estatal en todos los ámbitos de la vida en socied...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:53:"Venezuela: Análisis económico de un país en crisis";s:6:"og:url";s:119:"http://www.relial.org/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis";s:8:"og:title";s:62:"Venezuela: Análisis económico de un país en crisis - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/e561084e9220383c7204eb40f9bc13f1_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/e561084e9220383c7204eb40f9bc13f1_S.jpg";s:14:"og:description";s:158:"Durante los últimos quince años en Venezuela se adoptó un sistema político con alto intervencionismo estatal en todos los ámbitos de la vida en socied...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:53:"Venezuela: Análisis económico de un país en crisis";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}