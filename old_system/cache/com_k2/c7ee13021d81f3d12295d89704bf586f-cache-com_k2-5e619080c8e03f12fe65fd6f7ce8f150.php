<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8039:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId424"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Alerta por los Derechos Humanos de activista LGBTI en Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/3119c7be2ab58173062c39c6b8c72ed7_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/3119c7be2ab58173062c39c6b8c72ed7_XS.jpg" alt="Alerta por los Derechos Humanos de activista LGBTI en Venezuela" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El Instituto Político Para la Libertad (IPL Perú), exige la liberación de Rosmit Mantilla, y alerta sobre la situación del joven Rosmit Mantilla, activista venezolano LGBTI detenido y preso injustamente por el Gobierno Venezolano.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El Instituto Político Para la Libertad, IPL-Perú, en su tradición de lucha y defensa irrestricta por la libertad y la democracia, pone de manifiesto a la ciudadanía peruana su profunda preocupación por la detención arbitraria por parte de las autoridades venezolanas del joven Rosmit Mantilla, dirigente estudiantil y activista LGBTI.</p>
<p>&nbsp;</p>
<p>MANTILLA, quien se encuentra injustamente privado de libertad en la sede del Servicio Bolivariano de Inteligencia Nacional (SEBIN) desde el día 02 de mayo del presente año.</p>
<p>&nbsp;</p>
<p>Mantilla, fundador de Pro inclusión, la primera organización de diversidad sexual integrada a la estructura de un partido político (Acción Democrática en Venezuela), fue detenido el pasado 2 de Mayo por funcionarios del Servicio Bolivariano de Inteligencia Nacional (SEBIN) quienes allanaron irregularmente su casa y lo inculparon con evidencias sembradas e inverosímiles. Han pasado desde ese entonces alrededor de 100 días en los que no hay visos para su liberación dado que el gobierno autoritario de Venezuela es cada vez más represivo con los que difieren con su pensamiento y con las líneas directrices del régimen; que ha sumido en una profunda crisis social, político y económica a la sociedad venezolana y que además tiene copadas todas las instituciones públicas y en donde, por esta situación, la justicia ha abandonado a muchos activistas.</p>
<p>&nbsp;</p>
<p>Además de esto, el gobierno no investiga los constantes abusos y amenazas contra la comunidad LGBTI, al contrario, muchos representantes del gobierno reproducen un discurso homofóbico, como el diputado Pedro Carreño que lanzó insultos homofóbicos contra el ex candidato presidencial Henrique Capriles en plena Asamblea Nacional hace poco tiempo.</p>
<p>&nbsp;</p>
<p>Rosmit Mantilla además de ser activista LGBTI, es sobre todo parte de esta generación de jóvenes que han salido a plantarle cara hace mucho tiempo a los abusos del gobierno venezolano, a las violaciones de los derechos humanos y a la errada forma de gobernar un país que tiene todo para ser próspero y libre pero que hoy solo es escenario de una terrible realidad que golpe al pueblo venezolano.</p>
<p>&nbsp;</p>
<p>Rosmit Mantilla representa a muchos jóvenes que sueñan con una Venezuela libre y democrática y que no han postergado sus ilusiones y esperanzas pese a todo. Rosmit fue uno de los impulsores para que se debata en la Asamblea Nacional el matrimonio igualitario.</p>
<p>&nbsp;</p>
<p>Por todo lo mencionado IPL Perú condena enérgicamente la injusta y arbitraria detención de Rosmit Mantilla por el solo de hecho de disentir con el régimen y de luchar por la libertad y la democracia en su país. Esta detención sienta un pésimo precedente de justicia en Venezuela, además de vulnerar y obstaculizar la lucha LGBTI en dicho país y en América Latina, donde en los últimos meses se ha avanzado sobremanera en esta materia en muchos países de la región.</p>
<p>&nbsp;</p>
<p>Por ello, exigimos la liberación inmediata de Rosmit Mantilla y la de otros muchos jóvenes y líderes políticos opositores apresados injustamente por el gobierno venezolano. Esperamos que todos ellos tengan un debido proceso acorde con la justicia y que lo lleven en libertad.</p>
<p>&nbsp;</p>
<p>Exhortamos además, a la comunidad política peruana y latinoamericana a rechazar estos actos antidemocráticos y a pronunciarse al respecto, además de velar y exigir el respeto por los derechos de los ciudadanos venezolanos más vulnerables en el gobierno de Nicolás Maduro.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: Instituto Político para la Libertad IPL Perú</p>
<p>Foto: IPL&nbsp;</p>
<p>Autor: IPL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/420-ricardo-lópez-murphy-“el-default-de-argentina-fue-un-fracaso-colectivo-inexplicable”">
			&laquo; Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/425-diálogo-“la-defensa-de-la-libertad-y-la-lucha-contra-el-racismo”">
			Diálogo “La defensa de la libertad y la lucha contra el racismo” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/424-alerta-por-los-derechos-humanos-de-activista-lgbti-en-venezuela#startOfPageId424">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:72:"Alerta por los Derechos Humanos de activista LGBTI en Venezuela - Relial";s:11:"description";s:157:"El Instituto Político Para la Libertad (IPL Perú), exige la liberación de Rosmit Mantilla, y alerta sobre la situación del joven Rosmit Mantilla, acti...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:63:"Alerta por los Derechos Humanos de activista LGBTI en Venezuela";s:6:"og:url";s:133:"http://www.relial.org/index.php/productos/archivo/actualidad/item/424-alerta-por-los-derechos-humanos-de-activista-lgbti-en-venezuela";s:8:"og:title";s:72:"Alerta por los Derechos Humanos de activista LGBTI en Venezuela - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/3119c7be2ab58173062c39c6b8c72ed7_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/3119c7be2ab58173062c39c6b8c72ed7_S.jpg";s:14:"og:description";s:157:"El Instituto Político Para la Libertad (IPL Perú), exige la liberación de Rosmit Mantilla, y alerta sobre la situación del joven Rosmit Mantilla, acti...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:63:"Alerta por los Derechos Humanos de activista LGBTI en Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}