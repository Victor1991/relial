<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6307:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId291"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Comunicado de Fundación Internacional para la Libertad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/63955aa9869cf7707ada1662dbfb31e2_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/63955aa9869cf7707ada1662dbfb31e2_XS.jpg" alt="Comunicado de Fundaci&oacute;n Internacional para la Libertad" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: center;"><strong>La Fundación Internacional para la Libertad,</strong></p>
<p style="text-align: center;">presidida por Mario Vargas Llosa, se solidariza con los jóvenes</p>
<p style="text-align: center;">Venezolanos y apoya su lucha por la Libertad</p>
<p>	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	&nbsp;</p>
<p>LIMA, 19 DE FEBRERO – Una vez más, los estudiantes venezolanos están dando una valerosa demostración de amor a la libertad que los espíritus libres del mundo no pueden ignorar. Se hace ética y políticamente necesaria una solidaridad expresa con quienes en estos días en Venezuela defienden los valores de nuestra cultura frente a un régimen autoritario, enemigo de la libertad y la democracia, que ha tenido un comportamiento brutal en sus procedimientos. Expreso un comunicado enviado por la Fundación Internacional para la Libertad, presidida por el premio nobel de Literatura Mario Vargas Llosa.</p>
<p>&nbsp;</p>
<p>Ciudadanos de varios países, unidos por el oficio de pensar y escribir, nos declaramos solidarios con los estudiantes venezolanos que al precio de sus vidas están demostrando que los valores básicos de la cultura de la libertad no se rendirán al acoso totalitario.</p>
<p>&nbsp;</p>
<p>MARIO VARGAS LLOSA, (PERÚ), CARLOS ALBERTO MONTANER (CUBA), PLINIO APULEYO MENDOZA (COLOMBIA), ALVARO VARGAS LLOSA (PERU), ENRIQUE GHERSI (PERU), GERARDO BONGIOVANNI (ARGENTINA), ARTURO FONTAINE (CHILE ), ALEJANDRO CHAFUEN (USA), ROCIO GUIJARRO (VENEZUELA), LORENZO B. DE QUIROS (ESPAÑA). CRISTIAN LARROULET (CHILE).</p>
<p>&nbsp;</p>
<p>Contacto: <span id="cloak56208">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak56208').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy56208 = 'gb&#111;ng&#105;&#111;v&#97;n&#105;' + '&#64;';
 addy56208 = addy56208 + 'l&#105;b&#101;rt&#97;d' + '&#46;' + '&#111;rg';
 document.getElementById('cloak56208').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy56208 + '\'>' + addy56208+'<\/a>';
 //-->
 </script>.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/288-cedice-libertad-ante-los-atentados-a-las-libertades-en-venezuela-repudiamos-la-violencia-y-la-represión-que-atentan-contra-la-paz-y-la-democracia-en-el-país">
			&laquo; CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/312-superar-el-limbo-de-gobernabilidad-en-bogotá">
			Superar el limbo de gobernabilidad en Bogotá &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/291-comunicado-de-fundación-internacional-para-la-libertad#startOfPageId291">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:64:"Comunicado de Fundación Internacional para la Libertad - Relial";s:11:"description";s:155:"La Fundación Internacional para la Libertad, presidida por Mario Vargas Llosa, se solidariza con los jóvenes Venezolanos y apoya su lucha por la Liber...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:55:"Comunicado de Fundación Internacional para la Libertad";s:6:"og:url";s:122:"http://relial.org/index.php/productos/archivo/comunicados/item/291-comunicado-de-fundación-internacional-para-la-libertad";s:8:"og:title";s:64:"Comunicado de Fundación Internacional para la Libertad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/63955aa9869cf7707ada1662dbfb31e2_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/63955aa9869cf7707ada1662dbfb31e2_S.jpg";s:14:"og:description";s:155:"La Fundación Internacional para la Libertad, presidida por Mario Vargas Llosa, se solidariza con los jóvenes Venezolanos y apoya su lucha por la Liber...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:55:"Comunicado de Fundación Internacional para la Libertad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}