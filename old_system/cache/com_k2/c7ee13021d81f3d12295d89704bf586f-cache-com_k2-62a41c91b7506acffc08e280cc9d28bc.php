<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8994:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId393"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La inmensa tarea de los reyes de España
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/15d406f06ce12f2ac57cb5137d1afc69_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/15d406f06ce12f2ac57cb5137d1afc69_XS.jpg" alt="La inmensa tarea de los reyes de Espa&ntilde;a" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Francisco Franco murió en 1975 seguro de que el futuro de España estaba "atado y bien atado". Nunca he creído la hipótesis de que el Caudillo preparó una transición post mórtem hacia la democracia. Franco era un hombre de orden y cuartel, melancólicamente convencido de que los "demonios familiares" del separatismo y la anarquía inevitablemente conducirían a los españoles a la catástrofe, a menos que una mano dura lo evitara.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Afortunadamente, Juan Carlos, el joven Borbón seleccionado, educado y designado por Franco para continuar su régimen autoritario al frente del Estado, tenía una idea diferente de España. Sabía que sólo podía o valía la pena reinar en una nación democrática en la que la Corona estuviera subordinada a la Constitución y al Parlamento, como era la norma en el norte de Europa occidental.</p>
<p>&nbsp;</p>
<p>El monarca no perdió tiempo. Con la ayuda de las Cortes, acertadamente reclutó a Adolfo Suárez como Presidente de Gobierno. Fue el negociador ideal para lograr un cambio que parecía imposible: a trancas y barrancas, porque no fue fácil, los franquistas se transformaron en demócratas, los socialistas abandonaron el marxismo, los comunistas renunciaron al leninismo, los vascos y catalanes silenciaron y aplazaron sus pulsiones nacionalistas, el ejército se subordinó a la jefatura de los civiles –salvo el limitado espasmo golpista de 1981–, la Iglesia Católica bendijo la metamorfosis, y todos admitieron la monarquía.</p>
<p>&nbsp;</p>
<p>Juan Carlos, heredero de una dinastía desacreditada ante los ojos de los españoles, dos veces derribada por una sociedad que no amaba ni respetaba a la familia real, a lo que se agregaba el origen espurio de su poder, arbitrariamente impuesto por Franco, los necesitaba a todos para poder reinar con legitimidad moral (tenía la política), pero todos necesitaban a Juan Carlos para ocupar cierto espacio en un orden democrático que surgió milagrosamente en apenas tres años.</p>
<p>&nbsp;</p>
<p>La transacción funcionó espléndidamente, al menos por un tiempo. Los españoles, como se ha dicho mil veces, no se hicieron monárquicos, pero sí juancarlistas. Casi todo el país le agradeció al Rey el establecimiento de la democracia y su actitud decidida cuando varios militares trataron de derribar el gobierno por la fuerza. El consenso general era que sin la tutela de Juan Carlos y su predicamento en las Fuerzas Armadas, el tránsito hacia la democracia se habría interrumpido.</p>
<p>&nbsp;</p>
<p>Esa primera transición duró 39 años. Algo más que el franquismo. En ese periodo, con aciertos y fallos, los grandes partidos gobernaron en el ámbito nacional o regional, solos o en coalición, y las instituciones funcionaron razonablemente bien. Sólo faltaba por ponerse a prueba la transmisión de la autoridad dentro de la monarquía.</p>
<p>&nbsp;</p>
<p>Acaba de suceder. Con la abdicación de Juan Carlos I y la asunción al trono de su hijo, quien reinará como Felipe VI junto a Letizia, la reina, se cierra el ciclo y comienza una segunda etapa en la que las prioridades generales son otras: propiciar la creación de empleo, lo que entraña generar el surgimiento de empresas; combatir la corrupción; enfrentarse constructivamente al separatismo vasco y catalán, si ello es posible; y revitalizar la monarquía, hoy muy devaluada por los escándalos económicos del yerno del rey, Iñaki Urdangarín, y por el comportamiento un tanto frívolo de Juan Carlos I, quien se marchó con una "amiga" a África a cazar elefantes en medio de una severa crisis económica.</p>
<p>&nbsp;</p>
<p>La inmensa tarea que Felipe y Letizia tienen por delante desde el día uno de su reinado, es convertir a los españoles de juancarlistas desengañados en monárquicos convencidos de la utilidad de una institución que los conecta con su vieja historia nacional y forma parte de las señas de identidad colectivas, como sucede en Holanda, Inglaterra o Escandinavia.</p>
<p>&nbsp;</p>
<p>Los dos tienen el talento, la formación, las virtudes y la simpatía que se necesitan para poder consolidar la monarquía, pero esa peculiar institución no se sostiene de manera autónoma, sino dentro de la estructura de un Estado que tiene que funcionar con probidad y eficiencia, para ganarse el respeto de una sociedad que necesariamente debe percibir que posee posibilidades de mejorar progresivamente su calidad de vida si hace los necesarios esfuerzos.</p>
<p>&nbsp;</p>
<p>El prestigio de Juan Carlos creció mientras España prosperaba y cayó en picado cuando la economía se hundió. Felipe y Letizia serán pronto los reyes de España. Están llenos de buenas intenciones, pero les tocará a Rajoy y a los que vengan detrás gobernar bien para que la monarquía se sostenga. En 1981 el rey salvó a la democracia. Ahora la democracia debe salvar a los reyes.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: El blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018">
			&laquo; Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana">
			Política local y participación ciudadana &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa#startOfPageId393">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:49:"La inmensa tarea de los reyes de España - Relial";s:11:"description";s:156:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Francisco Franco murió en 1975 seguro de que el futuro de España estaba &quot;atado y bien atado&q...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:40:"La inmensa tarea de los reyes de España";s:6:"og:url";s:107:"http://www.relial.org/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa";s:8:"og:title";s:49:"La inmensa tarea de los reyes de España - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/15d406f06ce12f2ac57cb5137d1afc69_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/15d406f06ce12f2ac57cb5137d1afc69_S.jpg";s:14:"og:description";s:168:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Francisco Franco murió en 1975 seguro de que el futuro de España estaba &amp;quot;atado y bien atado&amp;q...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:40:"La inmensa tarea de los reyes de España";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}