<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17328:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId471"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Los cinco errores de Obama en su nueva política sobre Cuba
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/0e8bfd1d071657cbc63f9ace1550f1f3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/0e8bfd1d071657cbc63f9ace1550f1f3_XS.jpg" alt="Los cinco errores de Obama en su nueva pol&iacute;tica sobre Cuba" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>La visita a Cuba el próximo 21 de enero de Roberta Jacobson, secretaria de Estado adjunta de EE UU para el Hemisferio Occidental, encaminada a retomar oficialmente el diálogo con la dictadura de los Castro, será problemática. La diplomática, siempre muy preocupada por los temas de derechos humanos, llega a la isla en una posición muy débil debido a que el presidente Barack Obama entregó previamente todas las bazas de negociación con que contaba Estados Unidos. La señora Jacobson tendrá en su contra, por lo menos, los cinco peores errores de Obama en su nueva política cubana.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Primer error</p>
<p>&nbsp;</p>
<p>Suponer que puso fin a una política que no había funcionado. Eso no es cierto. El propósito de liquidar al régimen comunista no existe desde 1964, cuando Lyndon Johnson terminó de un plumazo las operaciones subversivas contra Castro y puso en marcha una estrategia de "contención", en alguna medida similar a la utilizada frente a la URSS, basada en tres elementos primordiales: propaganda, restricciones a las relaciones económicas y aislamiento diplomático.</p>
<p>&nbsp;</p>
<p>Eran medidas de guerra fría contra un país que nunca ha dejado de combatir a Estados Unidos. Washington desde entonces no ha tratado seriamente de eliminar al castrismo. En la primera mitad de los noventa, cuando había desaparecido la URSS y el castrismo carecía de aliados, hubiera sido muy fácil ponerle fin a la dictadura cubana, pero a Bill Clinton no le interesaba erradicar el régimen vecino.</p>
<p>&nbsp;</p>
<p>Pudo hacerlo, con el apoyo o la indiferencia de aquella Rusia de Borís Yeltsin y su canciller Andréi Kozyrev, cuando Castro desató el "balserazo" en 1994. Pudo hacerlo después en 1996, cuando derribó las avionetas de Hermanos al Rescate y autorizó el asesinato de varios norteamericanos en aguas internacionales. Pero Clinton ni siquiera consideraba a Cuba un país enemigo y se limitó a firmar la Ley Helms-Burton [que establece que cualquier empresa no norteamericana que tenga tratos con Cuba puede sufrir represalias legales].</p>
<p>&nbsp;</p>
<p>Cuba le parecía un anacronismo histórico, un fenómeno de Parque Jurásico, pero no estaba interesado en eliminar a ese Gobierno de la faz de la tierra. Entonces prevalecía la idea de que se trataba de una tiranía decrépita que colapsaría con el tiempo. Era, pensaba, una verruga que se caería sola. No había que extirparla.</p>
<p>&nbsp;</p>
<p>Tal vez Obama debió decir que cancelaba unas medidas de guerra fría contra un país que había superado ese periodo de la historia, pero ¿cómo explicar que en julio de 2013 detuvieran en Panamá un barco clandestinamente cargado en Cuba con 250 toneladas de pertrechos de guerra? ¿Cómo reclasificar como "país normal" a una nación calificada como terrorista, aliada de las peores tiranías islamistas —Irán, la Libia de Gadafi—, que se confabula con Venezuela, Bolivia, Ecuador y Nicaragua para articular una gran campaña antinorteamericana, como en los peores tiempos de la Guerra Fría? ¿No continúan en Cuba, protegidos por las autoridades, decenas de delincuentes norteamericanos, políticos y comunes?</p>
<p>&nbsp;</p>
<p>Cuba no era un exenemigo. Mantenía intacta su virulencia antiamericana.</p>
<p>&nbsp;</p>
<p>Segundo error</p>
<p>&nbsp;</p>
<p>Cancelar la política de contención sin tener con qué sustituirla. Ni una visión estratégica que defina cuáles son los objetivos que se persiguen. Es obvio que lo que debiera interesarle a Estados Unidos es que en esa isla tan cercana a sus fronteras, y que tantos percances le ha causado, haya un Gobierno democrático, pacífico y políticamente estable, para que no se produzcan espasmos migratorios como los que ya han trasladado al 20% de la población cubana a territorio norteamericano. Costa Rica es un buen ejemplo de ese modelo de nación tranquila latinoamericana que describo.</p>
<p>&nbsp;</p>
<p>Asimismo, lo conveniente para todos, y especialmente para los cubanos, es que en Cuba haya una sociedad próspera, desarrollada y amistosa con la cual realizar muchas transacciones comerciales, mutuamente satisfactorias. La tonta "teoría de la dependencia", caracterizada y resumida en Las venas abiertas de América Latina, carece de sentido. Para Estados Unidos, lo preferible es una Cuba rica y sosegada, antes que una Cuba tumultuosa y empobrecida.</p>
<p>&nbsp;</p>
<p>¿Se consiguen esos objetivos democráticos y estabilizadores potenciando a una dinastía militar empeñada en el colectivismo, el partido único y la falta de derechos humanos? ¿Se logra fomentar una sociedad rica ignorando que Raúl y sus militares se han dividido el aparato productivo a la manera mafiosa de Rusia? ¿No es obvio que, al no crear instituciones de derecho capaces de absorber los cambios y transmitir la autoridad ordenada, pacífica y democráticamente, esa isla está abocada a nuevas confrontaciones y conflictos a medio plazo?</p>
<p>&nbsp;</p>
<p>Obama cree que ha resuelto un problema enmendando las relaciones con Raúl Castro. Falso: lo que ha hecho es aplazarlo. En el futuro próximo se presentarán otras crisis que arrastrarán a Estados Unidos. Así ha sido desde el siglo XIX. Es lo que ocurre cuando no se curan permanentemente las heridas.</p>
<p>&nbsp;</p>
<p>Tercer error</p>
<p>&nbsp;</p>
<p>El daño hecho a la oposición democrática. Tal vez es el más grave de todos. Durante décadas, el mensaje de los disidentes más acreditados a la dictadura fue muy claro: "Sentémonos a conversar y entre cubanos busquemos una salida democrática. El problema es entre nosotros, no entre Washington y La Habana".</p>
<p>&nbsp;</p>
<p>A ese planteamiento —que, con matices, fue el de Gustavo Arcos, de la Plataforma Democrática Cubana, y Oswaldo Payá— el régimen respondía con represión y acusaciones de que se trataba de una maniobra de la CIA. Pero ese desenlace, como en Europa del Este, como en el Chile de Pinochet, como en la Nicaragua de 1990, era el mejor para todos, incluido Estados Unidos, y era el camino obvio para cualquiera que heredara el poder de los Castro, ambos ya en su etapa final por razones biológicas.</p>
<p>&nbsp;</p>
<p>No obstante, para lograrlo, Washington debía mantenerse firme y remitir a la dictadura a la aduana opositora, cada vez que directa o indirectamente se insinuaba la posibilidad de la reconciliación. El problema era entre cubanos y debía solucionarse entre cubanos. Esto lo entendieron muy bien Bill Clinton y George W. Bush, los dos presidentes norteamericanos de la era postsoviética, y es lo que irresponsablemente acaba de invalidar Obama, eliminándole a la oposición toda posibilidad de ser un actor importante en la forja del destino de la isla.</p>
<p>&nbsp;</p>
<p>¿Para qué hacer reformas democráticas, dirán los herederos de Castro, si ya se nos acepta tal y como somos? ¿No declaró Roberta Jacobson, en nombre del Gobierno norteamericano, que no se hacían ninguna ilusión con respecto a que los Castro permitieran las libertades? A los 13 días exactos de anunciada la reconciliación, el 30 de diciembre de 2014, la policía política cubana detuvo o inmovilizó en sus casas a unas cuantas decenas de intelectuales y artistas que trataban de realizar una performance en la plaza de la Revolución. ¿Cuál es el incentivo que le queda a Washington para inducir el respeto a los derechos humanos, si ya ha hecho la mayor parte de las concesiones unilateralmente?</p>
<p>&nbsp;</p>
<p>Lo dijo con toda claridad el alto oficial de inteligencia Jesús Arboleya, diplomático y experto cubano en las relaciones con Estados Unidos y Canadá, respondiendo a una entrevista que le hicieran en El Nuevo Día de Puerto Rico el 30 de diciembre de 2014. El periódico le preguntó si temía a la nueva política de Obama: "¿Por qué, si antes, que tenían todo el poder para imponer sus valores, no les funcionó, les va a funcionar a partir de ahora?".</p>
<p>&nbsp;</p>
<p>La dictadura está eufórica. Siente que tiene carta abierta para aplastar a los demócratas sin pagar por ello el menor precio. Obama ha contribuido insensiblemente a debilitar a la oposición.</p>
<p>&nbsp;</p>
<p>Cuarto error</p>
<p>&nbsp;</p>
<p>De carácter moral. Desde la época de Jimmy Carter, en Estados Unidos se fue generando una doctrina democrática para América Latina. Se planteó la excepcionalidad de la región a los efectos de defender la democracia y la libertad.</p>
<p>&nbsp;</p>
<p>Estados Unidos, por razones estratégicas, o por realpolitik, podía no exigirle a China que tuviera un comportamiento democrático, pero de la misma manera que América Latina podía ser declarada región libre de armas nucleares, era factible declararla libre de dictaduras y de abusos contra los derechos humanos.</p>
<p>&nbsp;</p>
<p>Este espíritu culminó en la firma de la Carta Democrática Interamericana, suscrita por todos los países del hemisferio en Lima el 11 de septiembre de 2001, el mismo día del ataque de los islamistas a Nueva York y Washington. En el documento se describían los rasgos y comportamientos de las naciones aceptables para formar parte de la Organización de los Estados Americanos OEA. Cuba no cumplía con ninguno de esos requisitos. Era una despreciable dictadura calcada del modelo soviético-estalinista.</p>
<p>&nbsp;</p>
<p>De alguna manera, el texto de esa Carta, en la que trabajó arduamente Estados Unidos, ponía fin a la tradición vergonzosa de permanente componenda entre Washington y las peores dictaduras latinoamericanas a lo largo del siglo XX: Trujillo, Stroessner, Somoza, Batista y un largo etcétera. Ya no tendría validez el cínico dictum de "es un hijo de puta, pero es nuestro hijo de puta".</p>
<p>&nbsp;</p>
<p>Tras la reconciliación entre Obama y Raúl Castro, Estados Unidos vuelve a las andadas. Hace en casa el gran discurso de la libertad, pero lo desmiente en su conducta diplomática. Es verdad que eso es lo que deseaban muchos países latinoamericanos, pero no deja de ser una pena que en las relaciones interamericanas no haya espacio para las consideraciones morales. Estados Unidos ha sacrificado inútilmente su posición de líder ético y ha regresado al peor relativismo moral. Una gran pena.</p>
<p>&nbsp;</p>
<p>Quinto error</p>
<p>&nbsp;</p>
<p>De carácter legal. Estados Unidos es una república dirigida por los delegados de la sociedad seleccionados por medio de elecciones democráticas. Entre ellos, el presidente es el principal representante de la voluntad popular, pero no el único. Hay un poder legislativo que comparte muchas de las funciones con la Casa Blanca, y existe una Constitución, interpretada por el poder judicial, a la que todos deben atenerse. Como todos sabemos, la esencia de la república es la división de poderes para evitar la dictadura y para obligar a la dirigencia a buscar fórmulas de consenso.</p>
<p>&nbsp;</p>
<p>Es posible que las encuestas reflejen que una mayoría de la sociedad norteamericana apoya coyunturalmente la reconciliación con la dictadura cubana —como en 1939 la mayoría apoyaba la neutralidad frente a los nazis—, pero ese dato tiene una importancia relativa. Estados Unidos, insisto, es una república ajustada a derecho y es una democracia representativa. Eso es lo que cuenta y tiene muy poco que ver con las encuestas o con las decisiones asamblearias.</p>
<p>&nbsp;</p>
<p>Pues bien: es muy posible que una parte sustancial de los dos años de mandato que le quedan al presidente Obama tendrá que dedicarlos a defender en la Cámara y en el Senado por qué engañó a la opinión pública y por qué engañó a los otros poderes del Estado, diciéndoles, hasta la víspera del anuncio junto a Raúl Castro el 17 de diciembre de 2014, que no haría concesiones unilaterales a menos que la dictadura cubana diera pasos hacia la libertad y la apertura. No fue una maniobra diplomática silenciosa. Fue engañosa.</p>
<p>&nbsp;</p>
<p>En las dos Cámaras hay cinco congresistas y tres senadores cubanoamericanos, republicanos y demócratas que tienen una enorme experiencia en el tema. ¿No debió el presidente conversar previamente con ellos sobre su política cubana en busca de opiniones y consejos? ¿No existe la cordialidad cívica en la Casa Blanca? ¿Ni siquiera le merecía ese tratamiento el senador demócrata Bob Menéndez, presidente del Comité de Relaciones Exteriores del Senado?</p>
<p>&nbsp;</p>
<p>Es verdad que la política exterior es una prerrogativa de quien ocupe la presidencia, pero los legisladores tienen un claro papel que desempeñar en ese campo y todos sienten que el presidente los ha estafado. Algunos legisladores, además, suponen que el presidente violó la ley y tratarán de demostrarlo.</p>
<p>&nbsp;</p>
<p>Lo que Obama piensa que es parte de su legado —tener relaciones plenas y cordiales con una dictadura militar— tal vez se le convierta en una pesadilla. Por lo pronto, es un terrible error en el que no habían caído ninguno de los 10 presidentes que lo precedieron en el cargo. Por algo sería.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Fuente: www.elpais.com</p>
<p>Foto: El Blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/451-columnas-por-la-libertad-muros-a-derribar">
			&laquo; Columnas por la Libertad, muros a derribar		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/472-muerte-del-fiscal-alberto-nisman-despierta-dudas-e-indignación-en-argentina">
			Muerte del fiscal Alberto Nisman despierta dudas e indignación en Argentina &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/471-los-cinco-errores-de-obama-en-su-nueva-política-sobre-cuba#startOfPageId471">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:68:"Los cinco errores de Obama en su nueva política sobre Cuba - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; La visita a Cuba el próximo 21 de enero de Roberta Jacobson, secretaria de Estado adjunta de EE UU...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:59:"Los cinco errores de Obama en su nueva política sobre Cuba";s:6:"og:url";s:129:"http://www.relial.org/index.php/productos/archivo/actualidad/item/471-los-cinco-errores-de-obama-en-su-nueva-política-sobre-cuba";s:8:"og:title";s:68:"Los cinco errores de Obama en su nueva política sobre Cuba - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/0e8bfd1d071657cbc63f9ace1550f1f3_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/0e8bfd1d071657cbc63f9ace1550f1f3_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; La visita a Cuba el próximo 21 de enero de Roberta Jacobson, secretaria de Estado adjunta de EE UU...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:59:"Los cinco errores de Obama en su nueva política sobre Cuba";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}