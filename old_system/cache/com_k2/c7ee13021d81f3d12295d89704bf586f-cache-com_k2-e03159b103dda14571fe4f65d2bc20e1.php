<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7234:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId437"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	“Strengthening NGOs -Winning Support for Ideas and their Political Implementation”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/20e6d0dc79365d45f8620c21a172f633_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/20e6d0dc79365d45f8620c21a172f633_XS.jpg" alt="&ldquo;Strengthening NGOs -Winning Support for Ideas and their Political Implementation&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de María José Romano Boscarino,&nbsp;Representante de la Fundación Federalismo y Libertad (Tucumán, Argentina)</p>
<p>&nbsp;</p>
<p>Desde 1995, la Academia Internacional para el Liderazgo de la Fundación Friedrich Naumann localizada en Alemania, se ha convertido en un centro de formación y diálogo internacional único, sede de encuentro de representantes de organizaciones con las que trabaja, pertenecientes a diferentes países del mundo.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Como miembro de la Fundación Federalismo y Libertad de Argentina, tuve la oportunidad de participar de uno de los seminarios que se dictaron durante este año 2014, en Gummersbach. La temática del mismo se orientó a la transmisión de conocimientos sobre cómo fortalecer nuestras ONGs, ganar apoyo para nuestras ideas y su implementación política.</p>
<p>&nbsp;</p>
<p>El objetivo consistía en proveernos de una guía de pasos concretos contemplando la evaluación de nuestras metas, el refuerzo de nuestra imagen y marca, de la estructura interna, la definición de nuestros grupos de interés, la formulación de un plan de negocios y finalmente el desarrollo de un programa completo de comunicación para lograr el éxito de nuestros espacios.</p>
<p>&nbsp;</p>
<p>El curso se llevó a cabo en la Academia de la Fundación en Gummersbach y fue complementado con actividades realizadas en las ciudades de Aachen, Bruselas y Maastricht. El mismo, contó con disertantes y moderadores de primer nivel, con presentaciones didácticas y dinámicas que hicieron de cada jornada una experiencia diferente que pudimos aprovechar al máximo. Además, durante los viajes realizados participamos de reuniones con representantes de reconocidas ONGs de Alemania, Bélgica y Holanda y de charlas programadas en la sede de la Fundación Naumann en Bruselas y en el Parlamento Europeo.</p>
<p>&nbsp;</p>
<p>Fuimos 28 los representantes de ONGs de diferentes países del mundo los invitados a este seminario, lo que permitió que además de adquirir conocimientos teóricos y técnicos a través de cada jornada, pudiéramos enriquecernos con las experiencias, ideas y propuestas de jóvenes con realidades culturales, políticas, económicas y organizacionales completamente diferentes.</p>
<p>&nbsp;</p>
<p>Puedo afirmar en nombre de todos los participantes, que aquellas dos semanas intensas cargadas de ideas, trabajo, intercambio y desafíos fueron verdaderamente inolvidables. Retornamos a nuestros países con nuevas herramientas para mejorar la labor de nuestras organizaciones, transmitir efectivamente nuestras ideas y contribuir al progreso de nuestras sociedades, motivados e inspirados, con una energía adicional que nos hace sentir que el cambio es verdaderamente posible, que depende de nosotros, del trabajo en equipo, del compromiso, de la pasión que pongamos en cada iniciativa que tomemos y del propósito claro, ese sueño que nos guía.</p>
<p>&nbsp;</p>
<p>"Passion + Purpose = Believe"</p>
<p>&nbsp;</p>
<p>Simplemente gracias a la Fundación Friedrich Naumann y a todos los que contribuyeron al éxito de este seminario.</p>
<p>&nbsp;</p>
<p>Autor:&nbsp;María José&nbsp;Romano Boscarino</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: Fundación Federalismo y Libertad</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/434-seminario-“fortaleciendo-las-ongs”">
			&laquo; Seminario “Fortaleciendo las ONGs”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/443-argetina-¿quiénes-son-los-idiotas?">
			Argetina: ¿Quiénes son los idiotas? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/437-“strengthening-ngos-winning-support-for-ideas-and-their-political-implementation”#startOfPageId437">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:95:"“Strengthening NGOs -Winning Support for Ideas and their Political Implementation” - Relial";s:11:"description";s:158:"En la opinión de María José Romano Boscarino,&amp;nbsp;Representante de la Fundación Federalismo y Libertad (Tucumán, Argentina) &amp;nbsp; Desde 1995...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:86:"“Strengthening NGOs -Winning Support for Ideas and their Political Implementation”";s:6:"og:url";s:152:"http://www.relial.org/index.php/productos/archivo/opinion/item/437-“strengthening-ngos-winning-support-for-ideas-and-their-political-implementation”";s:8:"og:title";s:95:"“Strengthening NGOs -Winning Support for Ideas and their Political Implementation” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/20e6d0dc79365d45f8620c21a172f633_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/20e6d0dc79365d45f8620c21a172f633_S.jpg";s:14:"og:description";s:166:"En la opinión de María José Romano Boscarino,&amp;amp;nbsp;Representante de la Fundación Federalismo y Libertad (Tucumán, Argentina) &amp;amp;nbsp; Desde 1995...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:86:"“Strengthening NGOs -Winning Support for Ideas and their Political Implementation”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}