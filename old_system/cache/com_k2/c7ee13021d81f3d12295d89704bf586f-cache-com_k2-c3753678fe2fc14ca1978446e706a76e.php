<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7027:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId484"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	¿Por qué soy liberal?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7ae73efe4c50580e061849ef2d5b5ea4_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7ae73efe4c50580e061849ef2d5b5ea4_XS.jpg" alt="&iquest;Por qu&eacute; soy liberal?" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Rocio Guijarro</p>
<p>&nbsp;</p>
<p>¿Qué significa ser liberal? Primero, no tener miedo a decirlo.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>Además, significa libertad, responsabilidad individual, respeto a los derechos naturales del ser humano y a la propiedad privada. El liberalismo genuino está en perfecta armonía con la doctrina cristiana, no robar y no codiciar los bienes ajenos son mandamientos de la ley de Dios, que afirman la propiedad privada y constituye el fundamento básico del ser liberal.</p>
<p>&nbsp;</p>
<p>Como liberal creo en los postulados de la escuela austríaca de economía, capitaneada por Carl Menger y en la que convergieron Ludwig von Mises, Friedrich Von Hayek y muchos otros, quienes analizaron las implicaciones formales de la acción humana y esto se realiza porque la esencia de lo económico está implícita en la acción que el ser humano realiza en un sociedad siempre y cuando sea libre para crear, innovar aportar a la sociedad para lograr prosperidad con responsabilidad y ética.</p>
<p>&nbsp;</p>
<p>Soy liberal porque creo en el libre mercado, el Estado de Derecho y en un gobierno limitado, factores básicos para que una sociedad crezca, se desarrolle y los ciudadanos vivan con bienestar y con calidad de vida. Soy liberal y actúo como tal cívicamente, que conoce sus deberes y hace respetar sus derechos, creo en la igualdad de oportunidades ante la ley, y para ello debe haber también independencia y autonomía en los poderes del estado. Como madre he transmitido a mis hijos estos principios, para que sean buenos ciudadanos y obren en función de una mejor sociedad.</p>
<p>&nbsp;</p>
<p>Creo, además, en una moneda estable, que me permita tener acceso a una mejor calidad de vida, y no en la inestabilidad que hace difícil planificar, paraliza la inversión, genera desempleo y, lo más grave, entroniza el impuesto más perverso, que nos empobrece a todos: la inflación.</p>
<p>&nbsp;</p>
<p>Muchos atacan a quienes defendemos estos principios, utilizando una etiqueta que ni ellos mismos saben lo que significa: neoliberal. Pero, amigo lector, si es usted amante de la libertad; si está comprometido social y ambientalmente con su país; si cree en la democracia y promueve la paz; si está dispuesto a la acción y a la participación; si quiere una República que funcione con la menor intervención posible del Estado; si desea un equilibrio democrático de intereses entre los miembros de la sociedad; si considera que el empleo de la inteligencia, del amor al prójimo y de la capacidad para prevenir el futuro de la política, es más importante que el lobby de los buscadores de rentas; si cree en los objetivos humanos y en que éstos no se encuentran sólo en lo material, sino también y en mayor medida en el ámbito de lo moral, ético y cultural; y si quiere ser responsable y decidir por tanto de sus propios intereses como sea posible, entonces, amigo lector, usted también es liberal.</p>
<p>&nbsp;</p>
<p>El liberalismo es la doctrina de la libertad personal. Por eso es insustituible.</p>
<p>&nbsp;</p>
<p>Autor: Rocío Guijarro; Gerente de CEDICE Libertad</p>
<p>Foto: CEDICE</p>
<p>Texto: <a href="http://cedice.org.ve/por-que-soy-liberal-rocio-guijarro-saucedo/">www.cedice.org.ve</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/483-entre-los-comisarios-y-el-mercado">
			&laquo; Entre los comisarios y el mercado		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/486-el-gurú-de-obama-y-un-mundo-sin-cabeza">
			El gurú de Obama y un mundo sin cabeza &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/484-¿por-qué-soy-liberal?#startOfPageId484">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:32:"¿Por qué soy liberal? - Relial";s:11:"description";s:157:"En la opinión de Rocio Guijarro &amp;nbsp; ¿Qué significa ser liberal? Primero, no tener miedo a decirlo. &amp;nbsp; Además, significa libertad, respo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:23:"¿Por qué soy liberal?";s:6:"og:url";s:89:"http://www.relial.org/index.php/productos/archivo/opinion/item/484-¿por-qué-soy-liberal";s:8:"og:title";s:32:"¿Por qué soy liberal? - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/7ae73efe4c50580e061849ef2d5b5ea4_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/7ae73efe4c50580e061849ef2d5b5ea4_S.jpg";s:14:"og:description";s:165:"En la opinión de Rocio Guijarro &amp;amp;nbsp; ¿Qué significa ser liberal? Primero, no tener miedo a decirlo. &amp;amp;nbsp; Además, significa libertad, respo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:23:"¿Por qué soy liberal?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}