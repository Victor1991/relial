<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8751:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId283"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Venezuela llega a su hora decisiva
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/d382bd8ae87d9139df6458192532657c_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/d382bd8ae87d9139df6458192532657c_XS.jpg" alt="Venezuela llega a su hora decisiva" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Sabino</p>
<p>&nbsp;</p>
<p>Venezuela vive un momento de definiciones. Cansados de sufrir privaciones, la inflación, el desabastecimiento y la dictadura, los venezolanos han salido por fin a las calles para protestar contra un gobierno que — descaradamente — ejerce el poder sin restricciones. Han comenzado los estudiantes en varias ciudades del interior pero ya la ciudadanía, sin distinciones de ningún tipo, ha salido a manifestar contra un gobierno que es títere de los comunistas cubanos y que ejerce la represión con brutalidad y sin mesura.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Los males que sufren los venezolanos, gracias a la implantación de la variante chavista del socialismo, se han hecho ahora bien conocidos: una inflación que supera con creces la cifra oficial del 56%, un desabastecimiento de la mayoría de los productos de consumo diario, repuestos y equipos, unos medios de comunicación amordazados, poderes públicos que son una simple prolongación de la camarilla gobernante y un sistema electoral viciado, que da siempre la victoria al oficialismo, aunque sea por algunas décimas.</p>
<p>&nbsp;</p>
<p>La fuerte reacción del gobierno también es comprensible pues ¿qué futuro le espera a quienes han saqueado al país, han impuesto su dominio excluyendo todo diálogo y han roto las normas de la convivencia civilizada? Menos comprensible es la lenta y cautelosa reacción de gran parte de la oposición que, a mi juicio, confunde tres conceptos en el fondo diferentes, lo que en estas circunstancias podrían marcar la diferencia entre el triunfo y la derrota: no violencia, legalismo y vía electoral.</p>
<p>&nbsp;</p>
<p>Iniciar el camino de la violencia sería equivocado, peligroso y hasta suicida: el camino de la violencia arroja a los pueblos a abismos de los que resulta siempre muy difícil salir pues las heridas que deja en el tejido social cicatrizan solo muy lentamente. Pero es más, en las condiciones actuales, dejaría solo un tendal enorme de víctimas y casi seguramente llevaría al fracaso, pues el de Maduro es un gobierno armado hasta los dientes que no tiene ningún principio moral, que miente y asesina sin piedad alguna.</p>
<p>&nbsp;</p>
<p>El legalista tampoco parece el camino acertado: reclamar ante poderes públicos dóciles a los gobernantes y acudir a un poder judicial totalmente parcializado es solo dar legitimidad a un gobierno que no la tiene, por sus manipulaciones electorales y su vocación totalitaria. Lo mismo puede decirse de la vía electoral, probada ya decenas de veces, siempre infecunda ante las variadas formas de fraude y de condicionamiento que han realizado los chavistas desde hace más de una década.</p>
<p>&nbsp;</p>
<p>¿No queda ninguna alternativa entonces y está todo perdido para los venezolanos? Por supuesto que no, que siempre puede enfrentarse a un gobierno dictatorial con éxito sin tener que llegar a traspasar la frontera de la violencia. Una presencia en las calles tenaz y decidida, imaginativa y constante, puede derrotar a los dictadores de todo tipo que han ejercido el poder en nuestro mundo. Así lo han mostrado checos, húngaros y polacos hace más de dos décadas, y así ha ocurrido en varias naciones árabes, como Túnez o Egipto en la reciente primavera árabe; ahora mismo está ocurriendo en Ucrania.</p>
<p>&nbsp;</p>
<p>No se trata de acudir a las armas pero, entre la lucha armada frontal y la sumisión a las elecciones y al andamiaje formal de la dictadura queda en Venezuela un ancho margen que es el decisivo para ganar esta batalla, en que el país se juega su destino. Porque no nos engañemos, si estas protestas fracasan esperan a Venezuela largos años de una dictadura cada vez más opresiva, más empobrecedora, más semejante a la cubana.</p>
<p>&nbsp;</p>
<p>El problema es que, todavía, la mayoría de la oposición no ha comprendido que no puede esperar paciente y sumisamente a las próximas elecciones — que también perdería, en todo caso — y que de nada vale hacer llamamientos vacíos contra la violencia. Lo que se necesita no es apagar la llama de las protestas sino ejercer un efectivo liderazgo, encauzar la lucha, hacer valer los derechos sin pedir permiso ni aceptar lo que el régimen quiera imponer, denunciar el socialismo que ha llevado a los venezolanos a la miseria y proponer el camino de un retorno a los valores republicanos.</p>
<p>&nbsp;</p>
<p>¿Lo hará la oposición? ¿Surgirán líderes que sacudan el letargo y las costumbres de la vieja política? Es pronto para saberlo aunque, para darnos nuevas esperanzas, algunos líderes parecen ya haber comprendido que en estos días se está jugando — sin la menor duda — lo que será la Venezuela del futuro.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Sabino</p>
<p>Fuente: <a href="http://es.panampost.com/carlos-sabino/2014/02/17/protestas-llevan-a-venezuela-a-su-hora-decisiva/#sPhPGQLmCrDqWRKx.01">Panampost</a></p>
<p>Foto: Carlos Sabino</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/278-virtudes-republicanas-molestias-para-el-tirano">
			&laquo; Virtudes republicanas, molestias para el tirano		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/297-por-qué-perdió-rafael-correa">
			Por qué perdió Rafael Correa &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/283-venezuela-llega-a-su-hora-decisiva#startOfPageId283">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:43:"Venezuela llega a su hora decisiva - Relial";s:11:"description";s:155:"En la opinión de Carlos Sabino &amp;nbsp; Venezuela vive un momento de definiciones. Cansados de sufrir privaciones, la inflación, el desabastecimient...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:34:"Venezuela llega a su hora decisiva";s:6:"og:url";s:101:"http://www.relial.org/index.php/productos/archivo/opinion/item/283-venezuela-llega-a-su-hora-decisiva";s:8:"og:title";s:43:"Venezuela llega a su hora decisiva - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/d382bd8ae87d9139df6458192532657c_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/d382bd8ae87d9139df6458192532657c_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Sabino &amp;amp;nbsp; Venezuela vive un momento de definiciones. Cansados de sufrir privaciones, la inflación, el desabastecimient...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:34:"Venezuela llega a su hora decisiva";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}