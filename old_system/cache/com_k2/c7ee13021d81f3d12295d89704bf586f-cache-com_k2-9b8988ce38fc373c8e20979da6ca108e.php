<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16703:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Actualidad</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/506-choque-de-trenes-en-venezuela">
	  		Choque de trenes en Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/506-choque-de-trenes-en-venezuela" title="Choque de trenes en Venezuela">
		    	<img src="/media/k2/items/cache/a786836489dab4f04d53706ec376ba50_XS.jpg" alt="Choque de trenes en Venezuela" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Tras 17 años de régimen populista con corte autoritario, hay que ser muy cautelosos tras la aparente pacífica aceptación de los resultados electorales por parte del oficialismo chavista".&nbsp;</p>
<p>&nbsp;</p>
<p>Por Marcela Prieto, Directora de <a href="http://www.semana.com/opinion/articulo/elecciones-en-venezuela-un-choque-de-trenes-opinion-marcela-prieto/452905-3">Revista Semana</a> y Vicepresidente de RELIAL</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/506-choque-de-trenes-en-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonía-chavista">
	  		Venezuela: el fin de la hegemonía chavista	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonía-chavista" title="Venezuela: el fin de la hegemon&iacute;a chavista">
		    	<img src="/media/k2/items/cache/0060e62cf7c869b03300254ca743ee3c_XS.jpg" alt="Venezuela: el fin de la hegemon&iacute;a chavista" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong><span style="color: #000000;">"El cuadro político e institucional del país cambió radicalmente. Se acabó la hegemonía que el chavismo ejercía desde diciembre de 1998, cuando Hugo Chávez obtuvo la presidencia de la República de forma holgada. Por primera vez la alternativa democrática consigue un triunfo amplio e inobjetable..."</span></strong></p>
<p>&nbsp;</p>
<p>Trino Márquez, Director Académico de <a href="http://cedice.org.ve/">CEDICE Libertad</a></p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonía-chavista">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo">
	  		Las elecciones parlamentarias en Venezuela: ambiente previo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo" title="Las elecciones parlamentarias en Venezuela: ambiente previo">
		    	<img src="/media/k2/items/cache/6ef598b10d1539793d4ace8d8b7b613f_XS.jpg" alt="Las elecciones parlamentarias en Venezuela: ambiente previo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Las elecciones parlamentarias en Venezuela: ambiente previo</strong></p>
<p><strong>REPORTE RELIAL</strong></p>
<p>Trino Márquez, director académico <a href="http://cedice.org.ve/">CEDICE Libertad</a></p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo">
	  		Recordando la lucha peruana contra el terrorismo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo" title="Recordando la lucha peruana contra el terrorismo">
		    	<img src="/media/k2/items/cache/45245952fc8c5c5e099a3e444bf8f32a_XS.jpg" alt="Recordando la lucha peruana contra el terrorismo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Este 12 de setiembre se cumplirán 23 años desde que el Grupo Especial de Inteligencia (GEIN) capturó a Abimael Guzmán, líder supremo y sanguinario del grupo terrorista Sendero Luminoso. Junto con Guzmán cayó parte esencial de la cúpula de la organización y, con la captura, comenzó el fin de la lucha armada contra la democracia y el Estado que Sendero Luminoso había empezado en mayo de 1980. En estos 23 años el país ha, lentamente, intentado cicatrizar; sin embargo, en estos 23 años también hemos olvidado.</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba?-esa-es-la-pregunta-cuba-en-su-nueva-etapa-análisis-foda">
	  		¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba?-esa-es-la-pregunta-cuba-en-su-nueva-etapa-análisis-foda" title="&iquest;Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: An&aacute;lisis FODA">
		    	<img src="/media/k2/items/cache/42a35505dabe860dcdeb51f92d5be768_XS.jpg" alt="&iquest;Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: An&aacute;lisis FODA" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>18 agosto, 2015</p>
<p>Consejo Empresarial de América Latina</p>
<p>Conferencia anual: América Latina y el Comercio Internacional, San Juan, Puerto Rico, 14 de agosto de 2015</p>
<p>Por: Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Precisamente hoy, 14 de agosto de 2015, se iza la bandera norteamericana en La Habana, restableciéndose formalmente las relaciones diplomáticas entre Estados Unidos y Cuba, interrumpidas desde el 3 de enero de 1961, hace 54 años, cuando en Washington mandaba Ike Eisenhower y en La Habana Fidel Castro.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba?-esa-es-la-pregunta-cuba-en-su-nueva-etapa-análisis-foda">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-éxito">
	  		Chile, de espaldas al éxito	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-éxito" title="Chile, de espaldas al &eacute;xito">
		    	<img src="/media/k2/items/cache/b3a54ecc0915f9347c3f53fa31d161fe_XS.jpg" alt="Chile, de espaldas al &eacute;xito" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>¿Cuántas reformas se necesitan para destruir treinta años de progreso?</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-éxito">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/actualidad?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/actualidad?limitstart=0" class="pagenav">Anterior</a></li><li><a title="1" href="/index.php/productos/archivo/actualidad?limitstart=0" class="pagenav">1</a></li><li><span class="pagenav">2</span></li><li><a title="3" href="/index.php/productos/archivo/actualidad?start=12" class="pagenav">3</a></li><li><a title="4" href="/index.php/productos/archivo/actualidad?start=18" class="pagenav">4</a></li><li><a title="5" href="/index.php/productos/archivo/actualidad?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/actualidad?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/actualidad?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/actualidad?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/actualidad?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/actualidad?start=54" class="pagenav">10</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/actualidad?start=12" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 2 de 14	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:19:"Actualidad - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:64:"http://relial.org/index.php/productos/archivo/actualidad?start=6";s:8:"og:title";s:19:"Actualidad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:43:"http://relial.org/media/k2/categories/3.jpg";s:5:"image";s:43:"http://relial.org/media/k2/categories/3.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}}s:6:"module";a:0:{}}