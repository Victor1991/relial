<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12584:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId374"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL y su trabajo en Derechos Humanos
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/9ad74ebcc3d83e86bcc0098026ed5e9f_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/9ad74ebcc3d83e86bcc0098026ed5e9f_XS.jpg" alt="RELIAL y su trabajo en Derechos Humanos" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>&nbsp;</p>
<p>Derechos Humanos: ¿qué defendemos los liberales? Alrededor de esta pregunta, la Red Liberal de América Latina (RELIAL) y la Fundación Friedrich Naumann para la Libertad convocaron a reconocidos expertos y defensores de la libertad a debatir la situación de los derechos humanos en la región. Fruto de las jornadas de trabajo de este grupo de análisis, RELIAL concretó su proyecto: Postulado Liberal sobre Derechos Humanos, documento preliminar que será sometido a la aprobación del Congreso de la Red.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Cuernavaca, a menos una hora de la Ciudad de México, fue la sede de esta actividad que durante los días 26 y 27 de mayo acogió a un selecto grupo de intelectuales liberales comprometidos con el propósito de esta reunión.</p>
<p>&nbsp;</p>
<p>Ricardo López Murphy, Presidente de RELIAL, y Carlos Alberto Montaner, miembro de la Junta Honorífica de la Red, condujeron la mesa que entre sus participantes tuvo a Rubén Darío Cuellar -Coordinador del Observatorio de Derechos Humanos de la <a href="http://nuevademocracia.org.bo/">Fundación Nueva Democracia</a> (Bolivia)-, Emilio Palacio -en representación del <a href="http://www.ieep.org.ec/">Instituto Ecuatoriano de Economía Política</a> (Ecuador)- , Bertha Pantoja -Directora Ejecutiva de <a href="http://www.caminosdelalibertad.com/">Caminos de Libertad</a>-, Ramón Custodio -Ex Comisionado por los Derechos Humanos de Honduras-, en representación de los países de Centroamérica.</p>
<p>&nbsp;</p>
<p>Participó también Roberto González, abogado asociado de <a href="http://humanrightsfoundation.org/">Human Rights Foundation</a> (USA), organización afín a las ideas y principios de RELIAL. Como invitado especial se contó con la presencia de Luis de La Barreda, Fundador y Coordinador del programa Universitario de Derechos Humanos de la UNAM, México. Se lamentó la ausencia de Beatriz Borges de Venezuela, quien precisamente por la difícil situación que atraviesa su país tuvo que cancelar su viaje México de último momento.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Sobre los contenidos</strong></p>
<p><strong><br /></strong></p>
<p>Abordar la situación de los derechos humanos en América Latina ineludiblemente requirió repasar las actuales circunstancias que tocan a países como Ecuador y Bolivia. Desde tal punto de vista, Rubén Darío Cuellar y Emilio Palacio expusieron las diversas formas de represión y acoso de las que sus países son víctimas. La instrumentalización de la justicia con fines políticos, la falta de independencia de los poderes republicanos y las severas restricciones a la libertad de expresión, son las tres principales arbitrariedades que hoy laceran los derechos humanos y la libertad de la sociedad civil tanto en Bolivia como en Ecuador; realidad de la que nos dista Argentina.</p>
<p>&nbsp;</p>
<p>En México, los problemas aquejan en otros niveles, pues las víctimas no son necesariamente políticas, como expuso Bertha Pantoja; sin embargo el diagnóstico de los derechos humanos son un tema que aún exige atención y tratamiento específico tal y como explicó Luis De la Barreda. En lo que refiere a Centroamérica, Ramón Custodio hizo una síntesis de lo que ocurre en Centroamérica donde el contexto de violación de los derechos humanos ha cambiado gracias al crimen organizado y las bandas juveniles. Brindando un panorama general, Roberto González explicó la labor de Human Rights Foundation para la región y cómo dicha organización hace seguimiento a casos concretos, ante la falta de una respuesta efectiva de la OEA.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Sobre el Proyecto Postulado Liberal sobre Derechos Humanos</strong></p>
<p><strong><br /></strong></p>
<p>Para RELIAL y la <a href="http://www.la.fnst.org/">Fundación Friedrich Naumann para la Libertad,</a> los derechos humanos y el liberalismo son inseparables. Desde esta noción, el grupo de trabajo conceptualizó un documento preliminar en base a los principios y valores fundamentales que ciñen la misión y visión de la RELIAL: la defensa de la democracia liberal, de la libertad y la responsabilidad individual; el respeto a la propiedad privada; la promoción de un gobierno limitado; el impulso a la economía de mercado; la primacía del Estado de derecho y la defensa de la paz.</p>
<p>&nbsp;</p>
<p>La propuesta respeta y adopta los principales tratados internacionales tales como la <a href="http://www.un.org/es/documents/udhr/">Declaración Universal de Derechos Humanos (1948)</a>, la Carta de la <a href="http://www.oas.org/dil/esp/tratados_B-32_Convencion_Americana_sobre_Derechos_Humanos.htm">Convención Americana sobre Derechos Humanos (1969)</a>, el <a href="http://www2.ohchr.org/spanish/law/ccpr.htm">Pacto Pacto Internacional de los Derechos Políticos y Civiles (1976)</a>, así como la Declaración de Chapultepec (1194). De todos estos documentos, los liberales ponderamos la libertad de la persona como punto de partida.</p>
<p>&nbsp;</p>
<p>Como Red de organizaciones políticas y partidos políticos, la posición de RELIAL en esta materia es política. Defiende los derechos humanos y civiles de las personas en un marco general que escolta la Democracia y el Estado de Derecho. Para los liberales, es la aplicación y respeto por los derechos fundamentales lo que permite a cada persona la posibilidad de desarrollarse libremente.</p>
<p><strong><br /></strong></p>
<p><strong><br /></strong></p>
<p><strong>Compartiendo con periodistas</strong></p>
<p>&nbsp;<span style="font-size: 11px;">&nbsp;</span></p>
<p>Como parte de estas jornadas de trabajo, el miércoles 28 de mayo, la Fundación Friedrich Naumann para la Libertad convocó a un almuerzo con periodistas de distintos medios de comunicación. En este escenario, cada uno de los invitados internacionales tuvo la oportunidad de relatar la situación de su país y responder preguntas. Para México, lo sucede en la región en materia de derechos humanos es un foco de atención constante por las repercusiones en el tema migratorio.</p>
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>Cerramos con la conferencia: Camino de Servidumbre</strong></p>
<p><strong><br /></strong></p>
<p>Celebrando 70 años de la publicación de la obra de Hayek, Caminos de la Libertad, organización miembro de RELIAL, llevó a cabo una conferencia con el nombre del libro. Invitando a parte de los invitados internacionales a debatir la trascendencia del escrito en el actual escenario del siglo 21, ahora que el populismo y la planificación estatal, pareciera, han vuelto a sumir a las naciones latinoamericanas en servidumbre y, en algunos casos, en autoritarismos semejantes a la época en que fue escrito el libro (1940-1943), cuando el futuro la humanidad se debatía entre el totalitarismo y la libertad. Participaron como expositores Sergio Sarmiento, Carlos Alberto Montaner, Ricardo López Murphy, Emilio Palacio, Arturo Damm y José Torra.<em style="font-size: 11px;"></em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center;">&nbsp;<img src="images/ddhh%20wg.jpg" alt="ddhh wg" width="556" height="369" /><span style="font-size: 11px; text-align: left;">.</span></p>
<p><span style="font-size: 8pt;"><em><br /></em></span></p>
<p><span style="font-size: 10pt;"><em>Para ver más fotografías de todas estas actividades, visite nuestra galería, click<a href="index.php/multimedia/fotos/item/377-working-group-en-derechos-humanos-%C2%BFqu%C3%A9-defendemos-los-liberales?-fotos-1"> <strong>acá</strong></a></em></span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<div><span style="font-size: 10pt;">Texto: Silvia Mercado, Coordinadora del Proyecto FFN RELIAL.</span></div>
<p><span style="font-size: 10pt;">Fotos: &nbsp;Miguel Angel Torres, Asistente del Proyecto FFN RELIAL.&nbsp;</span></p>
<p>&nbsp;</p>
<p><span style="font-size: 10pt;"><em style="font-size: 11px; text-align: left;">Descargue el brochure de participantes entre otros contenidos haciendo click&nbsp;<strong><a href="index.php/biblioteca/item/375-participantes-en-working-group-en-derechos-humanos-¿que-defendemos-losliberales">acá</a></strong></em></span></p>
<p><span style="font-size: 8pt;"><em style="font-size: 11px; text-align: left;"><strong><br /></strong></em></span></p>
<p><span style="font-size: 8pt;"><strong>RELIAL, México DF</strong></span></p>
<p><em><strong><br /></strong></em></p>
<p><em><strong><br /></strong></em></p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/363-venezuela-fundación-hacer-lanza-campaña-de-solidaridad-internacional-con-la-sociedad-civil-venezolana">
			&laquo; Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/395-celebrating-the-2014-tax-freedom-day-tfd-in-porto-alegre-brazil-–-10th-edition">
			Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/374-relial-y-su-trabajo-en-derechos-humanos#startOfPageId374">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:48:"RELIAL y su trabajo en Derechos Humanos - Relial";s:11:"description";s:157:"&amp;nbsp; Derechos Humanos: ¿qué defendemos los liberales? Alrededor de esta pregunta, la Red Liberal de América Latina (RELIAL) y la Fundación Fried...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:39:"RELIAL y su trabajo en Derechos Humanos";s:6:"og:url";s:105:"http://relial.org/index.php/productos/archivo/actualidad/item/374-relial-y-su-trabajo-en-derechos-humanos";s:8:"og:title";s:48:"RELIAL y su trabajo en Derechos Humanos - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/9ad74ebcc3d83e86bcc0098026ed5e9f_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/9ad74ebcc3d83e86bcc0098026ed5e9f_S.jpg";s:14:"og:description";s:161:"&amp;amp;nbsp; Derechos Humanos: ¿qué defendemos los liberales? Alrededor de esta pregunta, la Red Liberal de América Latina (RELIAL) y la Fundación Fried...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:39:"RELIAL y su trabajo en Derechos Humanos";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}