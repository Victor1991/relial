<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8578:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId76"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La pasión por la igualdad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><img style="margin: 2px; border: 2px solid #cccccc; float: left;" alt="fpp mini" src="images/fotos/fpp%20mini.jpg" width="125" height="55" /><br style="clear: right;" />"El igualitarismo fáctico simplemente se opone al igualitarismo ético que promueve la máxima libertad individual, como una consecuencia lógica de la idea de que cada persona tiene el derecho sagrado a perseguir sus fines..."</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Por Axel Kaiser.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>"No tengo respeto alguno por la pasión de la igualdad, que a mi juicio no parece más que envidia idealizada". La cita es del escritor norteamericano Oliver Wendell Holmes y es particularmente útil como introducción al debate en torno a la igualdad. Pues no hay duda de que Holmes tenía razón cuando identificó la envidia como uno de los motores centrales del impulso igualitarista que habita en cada uno de nosotros. Este impulso se encuentra en el origen de esa común identificación entre la idea de igualdad material y la de justicia y es, probablemente, una reminiscencia de nuestro pasado tribal en que la libertad era totalmente inexistente y el individuo se entendía como parte de un todo orgánico dirigido por una autoridad dictatorial que velaba por el bien de la comunidad.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Karl Popper vio en ideologías colectivistas como el marxismo y el fascismo, y en la filosofía de pensadores como Hegel, Platón y Marx, un esfuerzo por volver a ese pasado tribal en que el individuo era aniquilado en función del colectivo. Con el tránsito hacia las sociedades abiertas y complejas, facilitado esencialmente por el comercio, las estructuras tribales fueron gradualmente deshaciéndose y el individuo cobrando cada vez mayor protagonismo. En Occidente, una fuerza central en el triunfo del individuo sobre el colectivo fue el cristianismo, una religión auténticamente individualista que planteó, como ninguna otra antes o después, la idea de que cada ser humano es único y titular de derechos superiores y anteriores al colectivo.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El mismo cristianismo hizo serios esfuerzos civilizadores en orden a limitar los efectos de la envidia. Y es que la envidia tiene el potencial destructivo suficiente para impedir el progreso o derrumbar un orden social. Cuando esta logra presentarse bajo un manto de moralidad -disfrazada de justicia por ejemplo-, y en la forma de una teoría intelectualmente respetable, usualmente termina dominando el clima de opinión intelectual, definiendo el curso de la evolución social hacia el conflicto de clases y el estatismo. Instalado ese ambiente, personas con las mejores intenciones y de intachable integridad caen seducidas por los ecos de cantos tribales que nos invitan a revivir nuestro pasado colectivista. Y puesto que es el libre actuar de los seres humanos lo que origina la desigualdad material, la demanda por mayor igualdad será inevitablemente una demanda por incrementar el poder y alcance de una autoridad central -el Estado-, con el fin de que este limite por la fuerza la libertad individual. Este igualitarismo, que podemos denominar fáctico, es así completamente incompatible con el principio cristiano de que cada ser humano es único y tiene un sagrado derecho a expresar su individualidad en todas las dimensiones posibles. Un Steve Jobs no puede existir en una sociedad que persigue la igualdad material -incluida la de oportunidades- como el fin supremo.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El igualitarismo fáctico simplemente se opone al igualitarismo ético que promueve la máxima libertad individual, como una consecuencia lógica de la idea de que cada persona tiene el derecho sagrado a perseguir sus fines. Más aún, el igualitarismo ético tiene justificación precisamente en el hecho de que los seres humanos somos diferentes. Si todos fuéramos exactamente iguales, no sería necesaria una regla que nos obligue a respetar los proyectos y la propiedad ajena, ya que todos tomaríamos exactamente las mismas decisiones, tendríamos lo mismo y jamás nos diferenciaríamos del resto.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Afortunadamente no hay dos seres humanos idénticos en el mundo, algo que en el mercado se expresa con mayor claridad que en cualquier otra esfera de la vida social. De ahí que los igualitaristas fácticos exijan restringirlo. Estos no quieren a los Steve Jobs. De lo que se trata para el igualitarista no es de que todos estén mejor -algo en cuyo logro el mercado, gracias a gente como Jobs, es insuperable-, sino de que unos no estén mucho mejor que otros. Para el igualitarista eso es justicia. Pero bien analizado el tema es difícil no concordar con Holmes y concluir que, cuando el igualitarista habla de justicia, más bien está idealizando la envidia.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;Fuente: El Mercurio, 14.05.2013, Publicado en <a href="http://www.fprogreso.org/index.html">Fundación Para el Progreso. </a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/75-prestigio-internacional-de-la-oposición">
			&laquo; Prestigio internacional de la oposición		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/277-venezuela-se-cae-de-maduro-que-el-pueblo-reclama-libertad">
			Venezuela: Se cae de Maduro que el pueblo reclama libertad &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/76-la-pasión-por-la-igualdad#startOfPageId76">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:35:"La pasión por la igualdad - Relial";s:11:"description";s:157:"&quot;El igualitarismo fáctico simplemente se opone al igualitarismo ético que promueve la máxima libertad individual, como una consecuencia lógica de...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:26:"La pasión por la igualdad";s:6:"og:url";s:94:"http://relial.org/index.php/productos/archivo/opinion/item/76-la-pasiÃƒÂ³n-por-la-igualdad";s:8:"og:title";s:35:"La pasión por la igualdad - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:161:"&amp;quot;El igualitarismo fáctico simplemente se opone al igualitarismo ético que promueve la máxima libertad individual, como una consecuencia lógica de...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:26:"La pasión por la igualdad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}