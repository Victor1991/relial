<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16593:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Actualidad</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
	  		Olas de Cambio - tercer número de la Mirada Liberal	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal" title="Olas de Cambio - tercer n&uacute;mero de la Mirada Liberal">
		    	<img src="/media/k2/items/cache/e791ab626e6785062374d45b25cc6e7f_XS.jpg" alt="Olas de Cambio - tercer n&uacute;mero de la Mirada Liberal" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>L<strong>a Mirada Liberal,</strong> revista de análisis y coyuntura política de América Latina, esta vez en su tercer número les ofrece artículos relacionadosa los procesos electorales de los dos últimos años.&nbsp;</p>
<p>&nbsp;</p>
<p>Quedan invitados a leer la reflexión de nuestros expertos <a href="images/Miradas/FNF%20RELIAL_MLiberal_OlasDC-digital_22jun-16.pdf" target="_blank"><strong>aquí</strong></a></p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
	  		Las elecciones peruanas del 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016" title="Las elecciones peruanas del 2016">
		    	<img src="/media/k2/items/cache/7aa3cdbe521e8339c01d4c47e738f898_XS.jpg" alt="Las elecciones peruanas del 2016" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
	  		“No hay comida”: el fantasma del hambre se apodera de Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela" title="&ldquo;No hay comida&rdquo;: el fantasma del hambre se apodera de Venezuela">
		    	<img src="/media/k2/items/cache/072519f74a95ea36f571d1e83f1c23bd_XS.jpg" alt="&ldquo;No hay comida&rdquo;: el fantasma del hambre se apodera de Venezuela" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
	  		Juntos en el compromiso por la libertad personal alrededor del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo" title="Juntos en el compromiso por la libertad personal alrededor del mundo">
		    	<img src="/media/k2/items/cache/605a5b56c8e1f29c51548653d6f1dfc8_XS.jpg" alt="Juntos en el compromiso por la libertad personal alrededor del mundo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideología socialista – una convicción política que compartió con su amigo colombiano, el Premio Nobel de Literatura, Gabriel García Márquez. Esa amistad se rompió, desde luego no sólo a causa de la ya memorable disputa que sostuvieron ambos y la cual dejó al colombiano un moretón (luego de este incidente hace 30 años ninguno de los contrincantes se volvió a pronunciar al respecto), sino porque también se transformó rápidamente de un entusiasta socialista a un convencido liberal. Cualquier tipo de intolerancia y política autoritaria son para él una atrocidad, sin importar bajo qué etiqueta política se cometan.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
	  		CEDICE Libertad en Venezuela entre los mejores think tanks del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo" title="CEDICE Libertad en Venezuela entre los mejores think tanks del mundo">
		    	<img src="/media/k2/items/cache/ca9456ad89fef6c66a71b99b32dfe05e_XS.jpg" alt="CEDICE Libertad en Venezuela entre los mejores think tanks del mundo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Cedice Libertad</strong> entre los primeros Centros de Estudios <em>(think tank</em>) de acuerdo al ranking que elabora la Universidad de Pensilvania</p>
<p><strong>Como difusor del pensamiento liberal y por su influencia en la opinión pública venezolana</strong></p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/509-un-diálogo-sobre-argentina">
	  		Un diálogo sobre Argentina	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/509-un-diálogo-sobre-argentina" title="Un di&aacute;logo sobre Argentina">
		    	<img src="/media/k2/items/cache/cfee1df0aef1bf88281266898fc4ff19_XS.jpg" alt="Un di&aacute;logo sobre Argentina" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Los efectos del Kirchner ismo en Argentina han sido terribles para el desarrollo económico. El triunfo de un candidato opositor abre la puerta a una gran cantidad de posibilidades. Un diálogo sobre Argentina es una exploración sobre los caminos hacia donde se puede ir y las posibilidades de revertir los daños del peronismo.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/509-un-diálogo-sobre-argentina">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><span class="pagenav">Inicio</span></li><li class="pagination-prev"><span class="pagenav">Anterior</span></li><li><span class="pagenav">1</span></li><li><a title="2" href="/index.php/productos/archivo/actualidad?start=6" class="pagenav">2</a></li><li><a title="3" href="/index.php/productos/archivo/actualidad?start=12" class="pagenav">3</a></li><li><a title="4" href="/index.php/productos/archivo/actualidad?start=18" class="pagenav">4</a></li><li><a title="5" href="/index.php/productos/archivo/actualidad?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/actualidad?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/actualidad?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/actualidad?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/actualidad?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/actualidad?start=54" class="pagenav">10</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/actualidad?start=6" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 1 de 14	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:19:"Actualidad - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:60:"http://www.relial.org/index.php/productos/archivo/actualidad";s:8:"og:title";s:19:"Actualidad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:47:"http://www.relial.org/media/k2/categories/3.jpg";s:5:"image";s:47:"http://www.relial.org/media/k2/categories/3.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}}s:6:"module";a:0:{}}