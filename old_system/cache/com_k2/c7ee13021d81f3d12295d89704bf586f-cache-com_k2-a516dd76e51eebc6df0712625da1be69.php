<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9783:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId506"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Choque de trenes en Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/a786836489dab4f04d53706ec376ba50_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/a786836489dab4f04d53706ec376ba50_XS.jpg" alt="Choque de trenes en Venezuela" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>"Tras 17 años de régimen populista con corte autoritario, hay que ser muy cautelosos tras la aparente pacífica aceptación de los resultados electorales por parte del oficialismo chavista".&nbsp;</p>
<p>&nbsp;</p>
<p>Por Marcela Prieto, Directora de <a href="http://www.semana.com/opinion/articulo/elecciones-en-venezuela-un-choque-de-trenes-opinion-marcela-prieto/452905-3">Revista Semana</a> y Vicepresidente de RELIAL</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Los resultados electorales en Venezuela del pasado domingo 6 de diciembre a favor de la oposición, son sin duda una buena noticia para el país, y una más para la región latinoamericana luego del triunfo de Macri en Argentina. Pero este es apenas el primer paso para una posible trasformación del vecino país.</p>
<p>&nbsp;</p>
<p>Tras 17 años de régimen populista con corte autoritario, hay que ser muy cautelosos tras la aparente pacífica aceptación de los resultados electorales por parte del oficialismo chavista. Es bueno que recordemos que Venezuela es un país con una larga trayectoria de golpes militares y con ello el papel de la Fuerzas Armadas ha sido importante en el marco institucional de éste país. Tan es así que no sería extraño afirmar que gran parte de la razón por la cual el régimen de Maduro aceptó la derrota electoral legislativa, fue por presión de la institucionalidad militar.</p>
<p>&nbsp;</p>
<p>Siempre le escuché decir a Gustavo Vasco Muñoz, quien fuera una figura influyente en la política colombiana a finales de los años 80 y embajador en Venezuela en uno de los momentos de mayor tensión con Colombia (por concepto de la famosa Fragata Caldas en el golfo), que serían las Fuerzas Armadas la única institución que prevalecería luego de la "ola chavista", o sería ésta la que en últimas ayudaría a definir muchas situaciones de tensión, y por qué no decirlo, de una posible transición en el país.</p>
<p>&nbsp;</p>
<p>Esto no quiere decir que de la noche a la mañana las Fuerzas Armadas venezolanas se hayan convertido en ángeles impolutos, pero sí es cierto que no tienen una buena relación con Nicolás Maduro. Su cúpula lo considera incompetente y responsable, en gran medida, de la crisis económica y social que se vive en el país hoy en día.</p>
<p>&nbsp;</p>
<p>Sumado a lo anterior, hay un aspecto sociológico que no debemos perder de vista en éste momento histórico y es la profunda polarización política y social que se vive en Venezuela. Si bien ganó la oposición y, por fortuna, contará con una mayoría calificada en el legislativo, los odios entre la oposición y el oficialismo son profundos, están cargados de emotividad y se basan en sentimientos de lucha de clases. Tan es así, que hoy día ya hay manifestaciones de adeptos al régimen frente a la Asamblea Nacional "jurando por la patria" que no dejarán entrar a los nuevos legisladores de la MUD. Dicen que "si sangre ha de caer, que haiga. Si muertos tiene que haber, que haya". Dicen que por nada del mundo permitirán que regrese la oligarquía al poder para que eche a perder todo lo logrado por la Revolución. Esta puede ser la antesala de un profundo y muy peligroso choque de trenes entre un poder Ejecutivo lleno de inseguridades y de nuevos enemigos internos, y un Legislativo, que podría llegar con ánimo revanchista.</p>
<p>&nbsp;</p>
<p>Esto nos invita a hacer dos reflexiones. Primero, recordar por qué el coronel Hugo Chávez llega al poder con el proyecto Socialismo del Siglo XXI, en un país inmensamente rico gracias a su abundancia petrolera, pero con partidos políticos corruptos, acumuladores de poder y privilegios y una élite profundamente rentista. Segundo, aspirar a que la oposición sepa administrar bien su mayoría en el legislativo. De no ser así, se podría propiciar una tensión peligrosa no solo en el ámbito gubernamental sino también en las calles, agudizando aún más la ya muy deteriorada seguridad ciudadana.</p>
<p>&nbsp;</p>
<p>Saber administrar el nuevo poder significa saber identificar bien los desafíos que tiene la MUD por delante y entender que el triunfo en el Legislativo es apenas el primer paso. Así las cosas, se deben coordinar y alinear las diferentes fuerzas opositoras, lo cual no será tarea fácil, pues allí hay diferentes visiones, intereses y aproximaciones sobre cómo debe ser el cambio.</p>
<p>&nbsp;</p>
<p>Sería conveniente llegar sin ese temido ánimo revanchista. Sería estratégico mostrar con el ejemplo que los cambios se pueden hacer pacíficamente bajando el tono y evitando un leguaje que invite al odio. Es crucial recordar siempre que el objetivo final es lograr el poder presidencial; y finalmente el mensaje que siempre deberán mandarle al pueblo venezolano, como si fuera su mantra, es que la MUD propenderá por un verdadero cambio, es decir: no seguir como van, pero tampoco regresarán al pasado.</p>
<p>&nbsp;</p>
<p>Los cambios se tendrán que hacer lentamente. Desmontar las políticas asistencialistas del chavismo no será tarea fácil porque dicho desmonte tendrá que venir acompañado de políticas públicas que garanticen estabilidad, servicios básicos satisfechos y una recuperación de la economía que garantice oportunidades para las nuevas generaciones. De lo contrario las reacciones violentas no se harán esperar.</p>
<p>&nbsp;</p>
<p>A pesar de todas estas advertencias, los primeros pasos hacia el cambio tendrán que ir dirigidos hacia la liberación de los presos políticos. Las primeras movidas de la nueva mayoría legislativa irán encaminadas hacia garantizar las libertades individuales, hacia el respeto al Estado de derecho y de los derechos fundamentales. Leopoldo López es hoy una figura emblemática, casi mística y, por ello, recaerá sobre sus hombros, junto a otros dirigentes políticos, la responsabilidad de liderar el cambio y así poder llegar a gobernar para todos los venezolanos sin excepción. Será necesario y estratégico minimizar el golpe del choque de trenes.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonía-chavista">
			&laquo; Venezuela: el fin de la hegemonía chavista		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/509-un-diálogo-sobre-argentina">
			Un diálogo sobre Argentina &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/506-choque-de-trenes-en-venezuela#startOfPageId506">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:38:"Choque de trenes en Venezuela - Relial";s:11:"description";s:157:"&quot;Tras 17 años de régimen populista con corte autoritario, hay que ser muy cautelosos tras la aparente pacífica aceptación de los resultados elect...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:29:"Choque de trenes en Venezuela";s:6:"og:url";s:95:"http://relial.org/index.php/productos/archivo/actualidad/item/506-choque-de-trenes-en-venezuela";s:8:"og:title";s:38:"Choque de trenes en Venezuela - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/a786836489dab4f04d53706ec376ba50_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/a786836489dab4f04d53706ec376ba50_S.jpg";s:14:"og:description";s:161:"&amp;quot;Tras 17 años de régimen populista con corte autoritario, hay que ser muy cautelosos tras la aparente pacífica aceptación de los resultados elect...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:29:"Choque de trenes en Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}