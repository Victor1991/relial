<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6575:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId355"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El porqué de la crisis venezolana
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8462850dbb62c4bd159ee1ad55df6950_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8462850dbb62c4bd159ee1ad55df6950_XS.jpg" alt="El porqu&eacute; de la crisis venezolana" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Luis Pazos</p>
<p>&nbsp;</p>
<p>La inflación y la devaluación que sufren actualmente los venezolanos, de las más altas del mundo, son parecidas a las vividas en México en la década de los 80. La inflación mexicana anualizada llegó al 117% en marzo de 1983. Esa inflación de tres dígitos fue debida a un déficit presupuestal gubernamental del 15.8% del PIB en 1982.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>La inflación fue exacerbada por controles de precios que produjeron escasez y mercado negro. El control de cambios en esos años generó especulación, dificultad para adquirir dólares y una mayor devaluación. Un aumento por decreto del 30% a los salarios con el Presidente López Portillo coadyuvó al aumento de los precios.</p>
<p>&nbsp;</p>
<p>Lo que pasó en México hace 30 años, sucede actualmente en la economía de Venezuela. El gasto público creciente, desordenado y populista del gobierno venezolano llevó a un déficit del 15% del Producto Interno Bruto, 8 puntos del cual se financió con emisión de dinero. El aumento del circulante es la causa principal del aumento general de precios.</p>
<p>&nbsp;</p>
<p>A marzo de este año, según datos del Banco Central de Venezuela, los precios anualizados aumentaron en un 59% y la canasta de alimentos un 79.2%. La mayoría de los productos básicos racionados y con precios controlados, presentan una inflación oficial por debajo de la real, la que sube más con el control de precios. El venezolano humilde y de clase media pierde horas en largas filas para comprar papel higiénico, leche y medicamentos, o los compra más caros en el mercado negro.</p>
<p>&nbsp;</p>
<p>Para compensar la inflación, el gobierno de Maduro decretó, a partir de mayo, un incremento de los salarios del 30% a empleados públicos y privados. Ese aumento implica una mayor emisión monetaria para enfrentar el pago de mayores salarios nominales a los empleados del gobierno, pues los reales seguirán bajando. La carrera precios-salarios, siempre la ganan los precios si no se frenan las causas estructurales de la inflación.</p>
<p>&nbsp;</p>
<p>La crisis venezolana no es causada por complots internacionales ni por la oposición, sino por políticas económicas equivocadas, cuyos resultados inflacionarios y devaluatorios se dan en México, Venezuela o cualquier otro país que aplique esas políticas.</p>
<p>&nbsp;</p>
<p>Autor: Luis Pazos | Profesor de Economía Política</p>
<p>Fuente: <a href="http://www.cedice.org.">www.cedice.org</a>.ve</p>
<p>Foto: CEDICE Libertad</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/354-chile-¿para-qué-imitar-a-venezuela-cuando-se-puede-emular-a-suiza?">
			&laquo; Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/358-participación-en-el-seminario-“profiling-political-liberalism-as-an-effective-force-for-progress-–a-global-future-workshop”">
			Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana#startOfPageId355">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:43:"El porqué de la crisis venezolana - Relial";s:11:"description";s:157:"En la opinión de Luis Pazos &amp;nbsp; La inflación y la devaluación que sufren actualmente los venezolanos, de las más altas del mundo, son parecidas...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:34:"El porqué de la crisis venezolana";s:6:"og:url";s:101:"http://www.relial.org/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana";s:8:"og:title";s:43:"El porqué de la crisis venezolana - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/8462850dbb62c4bd159ee1ad55df6950_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/8462850dbb62c4bd159ee1ad55df6950_S.jpg";s:14:"og:description";s:161:"En la opinión de Luis Pazos &amp;amp;nbsp; La inflación y la devaluación que sufren actualmente los venezolanos, de las más altas del mundo, son parecidas...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:34:"El porqué de la crisis venezolana";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}