<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12639:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId56"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Último combate contra la prensa libre
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/d3b3799d6611d677944f5f86a500beb3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/d3b3799d6611d677944f5f86a500beb3_XS.jpg" alt="&Uacute;ltimo combate contra la prensa libre" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El populismo, como expresión aggiornada del totalitarismo, va a contrapelo de los sistemas de democracia republicana basados en el principio de que la fragmentación del poder es una garantía para la libertad de la sociedad. El poder, en cambio, debe volver a ser único e indivisible para el populista, tal y como ocurría en las dictaduras del siglo pasado o, retrocediendo aún más en el tiempo, en las monarquías absolutas de antaño.	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>En su camino hacia la autocracia, al populismo no le basta con maniatar los poderes del Estado establecidos en la Constitución; es decir, no le es suficiente hacer del Congreso una escribanía y de la Justicia una sumisa sirvienta de los caprichos del Ejecutivo que aseguren, no obstante, una teatralización de institucionalidad. En efecto, el unicato populista se torna incompleto y tambaleante sin la completa subordinación de aquello que a menudo suele considerarse informalmente como el cuarto poder en las democracias modernas: los medios de comunicación.</p>
<p>&nbsp;</p>
<p>La prensa libre se convierte, en este orden de ideas, en un estorbo para el populista principalmente por dos razones. La primera, es que los medios de comunicación tienen el poder de poner en conocimiento de la sociedad civil determinada información que acaba por limitar indirectamente el poder del gobierno. Cumplen, así, una función disuasiva. La segunda, es que la prensa suele estropear los relatos épicos embusteros sobre los que el caudillo populista edifica aquello que Max Weber denominó "legitimidad carismática", cumpliendo una función desmitificadora. No olvidemos que el populismo, diferente del autoritarismo pero semejante al totalitarismo, no persigue la mera sumisión del pueblo como aquél, sino la entusiasta subordinación como éste.</p>
<p>&nbsp;</p>
<p>El gobierno kirchnerista es un actor relevante en el concierto de gobiernos populistas de la región. Su famoso "vamos por todos" es una expresión coloquial del intransigente populismo llevado hasta las últimas consecuencias por un modelo que pretende hacerse de la suma del poder público y privado del país a como de lugar. Es por ello que ese mismo "vamos por todo" no se agota en el dominio del Congreso y en la colonización de la Justicia, sino que se apresta a dar su batalla decisiva y final: la pulverización de la prensa no adicta, el último centro de poder del escenario nacional que no ha sido puesto de rodillas por el momento.</p>
<p>&nbsp;</p>
<p>Néstor Kirchner llegó a la presidencia de la Argentina pensando que podía aplicar a nivel nacional las mismas estratagemas que le dieron la suma del poder cuando gobernaba en el sur. Maniatar medios con los recursos de la pauta oficial y enriquecer amigos para que construyan usinas de opinión adicta al régimen son prácticas que caracterizaron la gestión del matrimonio Kirchner en Santa Cruz. Quienes vivieron aquellos años en la provincia patagónica recuerdan muy bien, por ejemplo, el control ejercido sobre La Opinión Austral (matutino popularmente conocido como "Lupinión" en referencia a Lupín, es decir, a Néstor), y el despliegue de diarios, revistas y radios que compraban las amistades de Kirchner, como Rudy Ulloa, dueño de El Periódico Austral, que se repartía gratuitamente en razón de un ejemplar cada ocho santacruceños adultos.</p>
<p>&nbsp;</p>
<p>Es evidente, sin embargo, que esta doble estrategia de "ahogar económicamente a quienes no dicen lo que yo quiero" y "subsidiar a quienes lamen los calcetines del poder" no ha tenido resultados contundentes a nivel nacional. Cinco años de ininterrumpida guerra simbólica entre los medios independientes y el kirchnerismo, que registra caídas significativas en la popularidad de éste y cada vez más consumidores de aquéllos, dan cuenta del fracaso estratégico del gobierno. Todo indica que los tiros están saliendo por la culata.</p>
<p>&nbsp;</p>
<p>En vano han sido los 11 millones anuales que recibe Diego Gvirtz por 6 7 8, su producto de comunicación goebbeliana por excelencia cuyo rating con suerte alcanza los magros 3 puntos. En vano han sido los cientos de millones destinados a minúsculos medios que prácticamente nadie consume pero que se mantienen en el mercado gracias a los subsidios gubernamentales. En vano han sido todos los recursos dirigidos a los medios de Sergio Szpolski, pues cada vez menos personas leen El Argentino, Tiempo Argentino y Revista Veintitrés. En vano ha sido la excursión de Cristóbal López por el mercado comunicacional desembolsando más de 40 millones de dólares en la compra de medios ya establecidos, pues cada vez menos gente consume C5N y Radio 10, desde que sus principales periodistas fueron echados por no desinformar. En vano han sido las insufribles cadenas nacionales de Cristina Kirchner, pues se ha comprobado que la gran mayoría de los argentinos cambia el canal en el preciso momento que el rostro de bisturí de la presidente aparece en el televisor.</p>
<p>&nbsp;</p>
<p>El kirchnerismo ha fracasado en manos del mercado, que refleja en última instancia las preferencias del pueblo a la hora de informarse. Es claro que el plan de formar un ejército de periodistas mercenarios y ahogar las voces contrarias con el uso discrecional de propaganda oficial ha fallado. Estamos asistiendo a un giro de ciento ochenta grados en la estrategia oficialista. Si lo que hasta ahora hemos vivido fue una suerte de "guerra de guerrillas" mediática, lo que se viene es una guerra clásica en la que el aniquilamiento directo e inmediato del enemigo constituye la lógica predominante. El tiempo apura, y cada vez resta menos para las elecciones legislativas que prometen ser desastrosas para el oficialismo.</p>
<p>&nbsp;</p>
<p>En las últimas horas han pasado al menos tres sucesos que hablan a las claras de este endurecimiento de la estrategia kirchnerista en su cruzada mediática.</p>
<p>&nbsp;</p>
<p>El viernes pasado, la Administración Federal de Ingresos Públicos (AFIP) allanó sorpresivamente los domicilios de los periodistas de TN Darío Lopreite y Sergio Lapegüe, sustrayéndoles de sus hogares numerosos documentos e información que guardaban en sus computadoras personales. Fue una clara señal para sus colegas, instrumentada a través de un órgano gubernamental utilizado para perseguir a disidentes que no en vano se conoce popularmente como "la Gestapo kirchnerista".</p>
<p>&nbsp;</p>
<p>Algunos días antes, los fiscales José Nebbia y Miguel Palazzani, designados ad hoc en una causa contra el Dr. Vicente Massot por la fiscal general Alejandra Gils Carbó (ferviente kirchnerista impulsora de los proyectos de reforma judicial), solicitaron la detención del director del diario bahiense La Nueva Provincia. El pedido se enmarca en una causa iniciada en septiembre del año pasado por la cobertura periodística del citado diario durante la guerra interna de los '70. Vale destacar que el canciller Héctor Timerman, director del vespertino La Tarde que tenía la misión de generar un clima propicio para el golpe de Estado del '76 y que hasta lo celebró en sus páginas, no es investigado por la Justicia.</p>
<p>&nbsp;</p>
<p>Vicente Massot es blanco del gobierno por ser referente no tanto de una oposición meramente política del kirchnerismo, cuanto de una oposición ideológica. Massot es doctor en Ciencia Política y es considerado uno de los pensadores más lúcidos de la Argentina. El kirchnerismo entiende a pie juntillas que en la conformación del "sentido común" del que hablaba Antonio Gramsci, los pensadores tienen un rol preponderante, y de ahí el ensañamiento con Massot.</p>
<p>&nbsp;</p>
<p>Finalmente, y como frutilla del postre, el domingo pasado se supo que los proyectos de los diputados kirchneristas Carlos Kunkel y Diana Conti para expropiar Papel Prensa generaron gran entusiasmo en Cristina Kirchner. Se supo, también, sobre la posibilidad de que muy pronto la Comisión Nacional de Valores (CNV) disponga la intervención del Grupo Clarín y desplace al directorio de la empresa, destruyéndolo por completo.</p>
<p>&nbsp;</p>
<p>La guerra que comenzó hace cinco años entre la prensa no adicta y el kirchnerismo, está llegando a su fin. Asistimos a momentos de definiciones cruciales, en las que no se descartan hechos de violencia (un vallado de contención ya ha sido colocado en la sede del "gran diario argentino"). Pero debe subrayarse lo siguiente: no se trata de defender al Grupo Clarín que, valga recordar, antes de 2008 fue un importante aliado de este gobierno populista. Se trata, más bien, de defender lo que nos queda de libertad de expresión frente al totalitarismo de quienes sin vacilar irán por todo hasta las últimas consecuencias.</p>
<p>&nbsp;</p>
<p>Autor: Agustín Laje</p>
<p>foto: www.laprensapopular.com</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/66-américa-latina-map">
			&laquo; ¿Dónde está America Latina?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/75-prestigio-internacional-de-la-oposición">
			Prestigio internacional de la oposición &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/56-último-combate-contra-la-prensa-libre#startOfPageId56">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"Último combate contra la prensa libre - Relial";s:11:"description";s:154:"El populismo, como expresión aggiornada del totalitarismo, va a contrapelo de los sistemas de democracia republicana basados en el principio de que la...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"Último combate contra la prensa libre";s:6:"og:url";s:104:"http://www.relial.org/index.php/productos/archivo/opinion/item/56-último-combate-contra-la-prensa-libre";s:8:"og:title";s:47:"Último combate contra la prensa libre - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/d3b3799d6611d677944f5f86a500beb3_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/d3b3799d6611d677944f5f86a500beb3_S.jpg";s:14:"og:description";s:154:"El populismo, como expresión aggiornada del totalitarismo, va a contrapelo de los sistemas de democracia republicana basados en el principio de que la...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:38:"Último combate contra la prensa libre";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}