<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9791:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId513"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Juntos en el compromiso por la libertad personal alrededor del mundo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/605a5b56c8e1f29c51548653d6f1dfc8_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/605a5b56c8e1f29c51548653d6f1dfc8_XS.jpg" alt="Juntos en el compromiso por la libertad personal alrededor del mundo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideología socialista – una convicción política que compartió con su amigo colombiano, el Premio Nobel de Literatura, Gabriel García Márquez. Esa amistad se rompió, desde luego no sólo a causa de la ya memorable disputa que sostuvieron ambos y la cual dejó al colombiano un moretón (luego de este incidente hace 30 años ninguno de los contrincantes se volvió a pronunciar al respecto), sino porque también se transformó rápidamente de un entusiasta socialista a un convencido liberal. Cualquier tipo de intolerancia y política autoritaria son para él una atrocidad, sin importar bajo qué etiqueta política se cometan.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p><strong>Compromiso político</strong></p>
<p>&nbsp;</p>
<p>No sólo en sus obras literarias Vargas Llosa ha descrito la realidad extrema, la violencia y la doble moral que impera en América Latina, sino también en el ámbito político se ha comprometido con la defensa de la dignidad y las posibilidades de desarrollo del individuo: en 1987 fue co- fundador del Movimiento Libertad y pronto se convirtió en presidente del partido, que abogó a favor de la economía de mercado, la protección de la propiedad privada y del Estado de Derecho. En 1990 Mario Vargas Llosa fue presentado como candidato de la coalición de centro-derecha FREDEMO. Contó con el respaldo de la Fundación Friedrich Naumann para la Libertad durante esa peligrosa época cuando el grupo terrorista Sendero Luminoso prácticamente acabó con la vida pública en Lima. Harald Klein, el entonces Director de Proyecto de la Fundación en el Perú, organizó capacitaciones para Vargas Llosa y su partido – con soldados fuertemente armados en la azotea, para proteger a los participantes de posibles ataques terroristas. Mientras que durante la primera vuelta electoral Vargas Llosa llevaba claramente la delantera con un 34%, en la segunda vuelta perdió ante el aún desconocido Ingeniero Agrónomo, Alberto Fujimori. Su programa económico, cuyo punto clave estaba basado en privatización, libre comercio, protección de la propiedad privada y recortes en el sector público, a fin de combatir el elevado endeudamiento estatal, despertó temor, sobre todo entre los peruanos más pobres. De haber ganado el literato Vargas Llosa las elecciones de 1990 en el Perú y no el agrónomo Fujimori, con seguridad el desarrollo político del país hubiera tomado otro rumbo.</p>
<p>&nbsp;</p>
<p>En 1991, cuando bajo el gobierno de Fujimori fue disuelto el parlamento -y también el partido de Vargas Llosa-, fundó en el marco del que entonces fuera proyecto de la Fundación Friedrich Naumann para la Libertad, el Instituto Ciudadano que se fijó como objetivo la formación de líderes jóvenes afines a las ideas liberales para ocupar puestos en la administración y la política. Un gran número de influyentes líderes que hoy día continúan activos en la política, la economía y la administración del Perú surgieron de este instituto.</p>
<p>&nbsp;</p>
<p><strong>Defensor del derecho a la libertad</strong></p>
<p>&nbsp;</p>
<p>El famoso y políticamente comprometido escritor polariza hasta hoy día el ámbito político. Aunque luego de su derrota electoral se retiró de la política partidista activa, continua encauzando su popularidad y prestigio hacia la defensa de los derechos políticos y personales de libertad. Para los activistas de izquierda esto le convierte en el clásico provocador. En 2008, cuando Vargas Llosa se presentó en Rosario, Argentina como orador principal del Congreso Anual de la Red Liberal para América Latina, RELIAL, red que cuenta con el amplio respaldo de la FNF, los autobuses que transportaban a los invitados de honor, entre los que se encontraban el ex presidente del gobierno español, José María Aznar y el Presidente de la FNF, Wolfgang Gerhardt, fueron atacados con bombas de pintura y piedras por simpatizantes radicales de los regímenes de Cuba y Venezuela.</p>
<p>&nbsp;</p>
<p>En 2009 durante su participación en el Congreso de RELIAL en Caracas, el entonces Jefe de Estado, Hugo Chávez propuso a los participantes debatir en cadena nacional, los intelectuales aceptaron el reto y designaron a Vargas Llosa para que representase al grupo, luego de una serie de justificaciones Chávez evadió toda confrontación directa. En su lugar, envió a militantes de su partido, quienes insultaban e intimidaban a los participantes, a ocupar la entrada del hotel donde se llevaron a cabo las conferencias. Estos hechos no impidieron que el ganador del Premio Nobel (2010) continuara defendiendo de manera consecuente los derechos de libertad en Venezuela. En 2014 cuando los estudiantes se manifestaron para exigir una mejor formación académica y el respeto a sus derechos de libertad, viajó nuevamente a Caracas y haciendo uso de su popularidad, se pronunció en los medios acerca de la desastrosa situación económica y política. En el evento participaron de igual manera representantes de la FNF y de RELIAL quienes exigieron el respeto de los derechos democráticos de libertad.</p>
<p>&nbsp;</p>
<p>El compromiso incondicional del Premio Nobel, quien tampoco teme al riesgo personal, fue reconocido por la FNF en el 2008 a través del Premio a la Libertad que le fue otorgado en la Paulskirche. Mario Vargas Llosa continúa siendo un incómodo demandante e incondicional abogado de la Libertad y de la autodeterminación.</p>
<p>&nbsp;</p>
<p>Autor: Birgit Lamm, Directora Regional para América Latina de la Fundación Friedrich Naumann</p>
<p>Fuente: <a href="http://www.la.fnst.org">www.la.fnst.org</a></p>
<p>Foto: Fundación Friedrich Naumann</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
			&laquo; CEDICE Libertad en Venezuela entre los mejores think tanks del mundo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
			“No hay comida”: el fantasma del hambre se apodera de Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo#startOfPageId513">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:77:"Juntos en el compromiso por la libertad personal alrededor del mundo - Relial";s:11:"description";s:155:"La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideolo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:68:"Juntos en el compromiso por la libertad personal alrededor del mundo";s:6:"og:url";s:134:"http://relial.org/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo";s:8:"og:title";s:77:"Juntos en el compromiso por la libertad personal alrededor del mundo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/605a5b56c8e1f29c51548653d6f1dfc8_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/605a5b56c8e1f29c51548653d6f1dfc8_S.jpg";s:14:"og:description";s:155:"La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideolo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:68:"Juntos en el compromiso por la libertad personal alrededor del mundo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}