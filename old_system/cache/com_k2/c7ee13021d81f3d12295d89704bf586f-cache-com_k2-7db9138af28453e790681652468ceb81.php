<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7356:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId209"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL dio la bienvenida a la Internacional Liberal
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/e02fde07d49ee258cc3f6d1b19207757_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/e02fde07d49ee258cc3f6d1b19207757_XS.jpg" alt="RELIAL dio la bienvenida a la Internacional Liberal" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="margin-bottom: 0pt; text-align: justify;">En La Antigua, ciudad patrimonio de la humanidad, representantes políticos que defienden el liberalismo se dieron cita en ocasión de 191va Reunión de Comité Ejecutivo de la Internacional Liberal (Ll). El Partido Patriota (PP), miembro pleno de la IL y también miembro pleno de RELIAL, fue el anfitrión oficial; la Fundación Friedrich Naumann para la Libertad y RELIAL coorganizaron y apoyaron este gran encuentro liberal.</p>
<p style="margin-bottom: 0pt; text-align: justify;">&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p style="margin-bottom: 0pt; text-align: justify;">&nbsp;</p>
<p style="margin-bottom: 0pt; text-align: justify;">&nbsp;</p>
<p style="margin-bottom: 0pt;"><span style="font-family: 'Agfa Rotis Sans Serif'; font-size: 11.5pt;"><img style="display: block; margin-left: auto; margin-right: auto;" src="images/fotos/212.JPG" alt="" /></span></p>
<p style="margin-bottom: 0pt; text-align: justify;">&nbsp;</p>
<p style="margin-bottom: 0pt; text-align: justify;"><span>La temática central tuvo que ver con El Estado de derecho y su rol en la lucha contra el crimen organizado. Sobre ello, políticos de América Latina y Europa discutieron propuestas y valoraron iniciativas que buscan abordar el tema desde la política pública y desde la sociedad civil. Roxana Baldetti, Vicepresidenta de la República de Guatemala, tomó la palabra e hizo notar que la gran víctima de la delincuencia organizada es la población femenina, en particular en aquellos países donde las instituciones son débiles; destacó que su gobierno está poniendo especial atención a esta situación.&nbsp;Entre otros temas, miembros de la IL y RELIAL intercambiaron criterios respecto a la situación de los derechos humanos en los distintos continentes, y sobre todo el contexto político de sus países.</span></p>
<p style="margin-bottom: 0pt; text-align: justify;"><span style="font-size: 11px;"><br /></span></p>
<p style="margin-bottom: 0pt; text-align: justify;"><span style="font-size: 11px;">Descargue el reporte completo haciendo click <a href="index.php/biblioteca/item/210-relial-reporte-ilec">acá</a></span></p>
<p style="margin-bottom: 0pt; text-align: justify;"><span style="font-size: 8pt;">Texto y fotos: Silvia Mercado/<span id="cloak33859">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak33859').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy33859 = 's&#105;lv&#105;&#97;.m&#101;rc&#97;d&#111;' + '&#64;';
 addy33859 = addy33859 + 'fnst' + '&#46;' + '&#111;rg';
 document.getElementById('cloak33859').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy33859 + '\'>' + addy33859+'<\/a>';
 //-->
 </script></span></p>
<p style="margin-bottom: 0pt; text-align: justify;"><span style="font-size: 11px;"><br /></span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="RELIAL_reporte__octubre_2013.pdf" href="/index.php/productos/archivo/actualidad/item/download/81_d846e36268b388e3d22c3f8d7e0f16e4">RELIAL_reporte__octubre_2013.pdf</a>
			    			    <span>(316 K2_DOWNLOADS)</span>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/116-“enseñamos-a-los-empresarios-a-decir-orgullosos-soy-liberal”">
			&laquo; “Enseñamos a los empresarios a decir orgullosos: soy liberal”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/218-partidos-políticos-de-relial-se-reunieron-en-guatemala">
			Partidos políticos de RELIAL se reunieron en Guatemala &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal#startOfPageId209">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:60:"RELIAL dio la bienvenida a la Internacional Liberal - Relial";s:11:"description";s:155:"En La Antigua, ciudad patrimonio de la humanidad, representantes políticos que defienden el liberalismo se dieron cita en ocasión de 191va Reunión de...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:51:"RELIAL dio la bienvenida a la Internacional Liberal";s:6:"og:url";s:121:"http://www.relial.org/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal";s:8:"og:title";s:60:"RELIAL dio la bienvenida a la Internacional Liberal - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/e02fde07d49ee258cc3f6d1b19207757_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/e02fde07d49ee258cc3f6d1b19207757_S.jpg";s:14:"og:description";s:155:"En La Antigua, ciudad patrimonio de la humanidad, representantes políticos que defienden el liberalismo se dieron cita en ocasión de 191va Reunión de...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:51:"RELIAL dio la bienvenida a la Internacional Liberal";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}