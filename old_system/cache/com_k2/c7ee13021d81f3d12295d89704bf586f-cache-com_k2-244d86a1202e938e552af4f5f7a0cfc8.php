<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17050:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId293"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: center;"><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Los Amigos de la Carta Democrática Interamericana expresan su rechazo ante los hechos ocurridos con ocasión de la manifestación pacífica convocada por organizaciones estudiantiles el 12 de febrero pasado en Venezuela. En este sentido, el grupo de Los Amigos lamenta la pérdida de vidas humana y los heridos, y hace pública su enérgica condena a la detención de más de 100 estudiantes, algunos de los cuales han denunciado que sufrieron atentados a su integridad física.</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">Especialmente preocupante es el allanamiento a la oficina de un partido político sin orden judicial, así como la detención del coordinador del partido político Voluntad Popular, Leopoldo López. Dicha situación configura una criminalización de la actividad política de los sectores de oposición, lo cual es inaceptable en una sociedad democrática.</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">Igualmente preocupantes son los obstáculos existentes para que los medios de comunicación informen sobre los acontecimientos, incluyendo las amenazas de penalidades que conduce a la autocensura, la eliminación del aire de un canal televisora internacional y la falta de papel para la prensa escrita.</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><strong><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">En virtud de estos graves hechos, el Grupo de Amigos de la Carta Democrática:</span></strong></p>
<p><strong><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></strong></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">1. Condenan la represión de las manifestaciones pacíficas y la detención arbitraria de estudiantes venezolanos;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">2. Condenan la detención arbitraria de dirigentes políticos;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">3. Requieren la inmediata liberación de todas las personas todavía detenidas, en los términos en que el derecho a la libertad personal está garantizado por la Constitución venezolana, el Pacto Internacional de Derechos Civiles y Políticos de Naciones Unidas, y otros instrumentos internacionales que obligan a Venezuela, sin perjuicio de que quienes hayan sido inculpados por hechos de violencia o vandalismo, sean sometidos a los procedimientos legales para determinar su responsabilidad, con las debidas garantías al debido proceso;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">4. Requieren una investigación independiente exhaustiva y transparente de los hechos de violencia así como de las denuncias de abusos contra estudiantes detenidos;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">5. Llaman al Gobierno de Venezuela a respetar y garantizar las condiciones necesarias para la realización de las actividades políticas por parte de la oposición y los sectores sociales que lo adversan, como está garantizado en la Carta Democrática Inter-Americana;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">6. Recuerdan a todos los venezolanos y venezolanas que el ejercicio de su derecho constitucional a protestar deber ser pacífico, tolerante y respetuoso del pluralismo de una sociedad democrática.</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">Urgimos a los líderes políticos buscar urgentemente soluciones y mecanismos que prevengan una escalada del conflicto en el país.</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><br /></span></p>
<p><strong><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></strong></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">El Grupo de Amigos de la Carta Democrática Interamericana lo integran ex Presidentes, ex Primeros Ministros, ex miembros de gabinete, expertos y promotores de los Derechos Humanos del hemisferio, que procuran incrementar el reconocimiento y cumplimiento de la Carta Democrática Interamericana, así como prevenir que tensiones políticas se conviertan en crisis que amenacen la estabilidad democrática. Los Amigos de la Carta Democrática visitan países para analizar conflictos políticos, animar a la ciudadanía y a los gobiernos a utilizar los instrumentos internacionales para proteger sus democracias y resolver conflictos constitucionales. Asimismo, formulan recomendaciones a la Organización de Estados Americanos para aplicar la Carta Democrática con un carácter preventivo y constructivo. El Centro Carter actúa como la secretaría de los Amigos de la Carta Democrática Interamericana.</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p>
<p style="text-align: center;"><strong><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;">Amigos de la Carta Democrática Interamericana</span></strong></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Diego Abente Brun</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Ministro de Justicia y Trabajo - Paraguay</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Cecilia Blondet</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Ministra Para La Promoción de la Mujer y el Desarrollo Humano - Perú</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Jorge Castañeda</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Ministro de Relaciones Exteriores - México&nbsp;</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Joe Clark</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Primer Ministro y ex Ministro de Relaciones Exteriores - Canadá </em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em><br /></em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">John Graham</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Presidente Emérito, Fundación Canadiense Para las Américas (FOCAL), Canadá </em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Osvaldo Hurtado</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Presidente de&nbsp; Ecuador</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em><br /></em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Torquato Jardim</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Magistrado del Tribunal Superior Electoral - Brasil</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">John Manley</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Ministro de Relaciones Exteriores -&nbsp; Canadá</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Barbara McDougall</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Secretaria de Estado párr Relaciones Exteriores -&nbsp; Canadá</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Andrés Pastrana</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Presidente de&nbsp; Colombia</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Sonia Picado</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Presidenta&nbsp; Instituto inter-americano de Derechos Humanos</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Sergio Ramírez</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex VICEPRESIDENTE de&nbsp; Nicaragua</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em><br /></em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Sir Ronald Sanders</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Miembro del Grupo de Personas Eminentes del Commonwealth 2010-2011</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Eduardo Stein</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex VICEPRESIDENTE de&nbsp;Guatemala </em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Alejandro Toledo</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Presidente de Perú</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Fernando Tuesta Soldevilla</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Ex Director Oficina Nacional de Procesos Electorales -&nbsp;Perú </em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Joaquín Villalobos</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><em>Fundador del Frente Farabundo Martí para la Liberación Nacional (FMLN), firmante de los Acuerdos de Paz de El Salvador es 19</em></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">-------------------------------------------------- -------------------------------------------------</span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks">
			&laquo; Think Tanks And The Power of Networks		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico">
			Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/293-declaración-de-los-amigos-de-la-carta-democrática-interamericana-sobre-la-situación-en-venezuela#startOfPageId293">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:107:"Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela - Relial";s:11:"description";s:157:"Los Amigos de la Carta Democrática Interamericana expresan su rechazo ante los hechos ocurridos con ocasión de la manifestación pacífica convocada por...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:98:"Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela";s:6:"og:url";s:165:"http://relial.org/index.php/productos/archivo/actualidad/item/293-declaración-de-los-amigos-de-la-carta-democrática-interamericana-sobre-la-situación-en-venezuela";s:8:"og:title";s:107:"Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:157:"Los Amigos de la Carta Democrática Interamericana expresan su rechazo ante los hechos ocurridos con ocasión de la manifestación pacífica convocada por...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:98:"Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}