<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6630:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId468"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Si no es ahora, ¿cuándo?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/08f61c52357dcc6d2503bfea790efe4d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/08f61c52357dcc6d2503bfea790efe4d_XS.jpg" alt="Si no es ahora, &iquest;cu&aacute;ndo?" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Yesenia Álvarez</p>
<p>&nbsp;</p>
<p>Las reacciones al nuevo régimen laboral juvenil dejan serias preocupaciones sobre lo incomprendido que es el camino hacia el desarrollo.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>En el Perú es apremiante la falta de empleo para los jóvenes que menos tienen. Con una tasa de desempleo juvenil mayor y una alta tasa de informalidad, el gobierno ha optado por impulsar un nuevo régimen que flexibiliza el mercado laboral para que aquellos que no cuentan con oportunidades se vuelvan competitivos. Cuesta creer que una ley de este tipo se dé en el gobierno de Humala y más aún que se haya comprendido la lógica de flexibilizar el mercado laboral. Triste que esta lógica no sea entendida por los héroes de las marchas.</p>
<p>&nbsp;</p>
<p>Así como es censurable que el gobierno no se haya tomado su tiempo en comunicar y consultar el sentir ciudadano, lo es también que quienes han salido a marchar lo hayan hecho sin leer la ley y sin reflexionar sobre cómo se forma el empleo. Puede haber buena intención, y siempre es defendible el derecho a protestar, pero cabe recordarles que están marchando de espaldas a quienes realmente necesitan una alternativa para salir de la pobreza.</p>
<p>&nbsp;</p>
<p>El propósito de la ley es bajar las barreras de quienes no pueden competir. Entonces, nada más excluyente que las demandas de las protestas que buscan que se mantengan esas barreras. Una protesta que perjudica a quienes busca proteger es paradójica, y una protesta que sacrifica el futuro de otros es hasta cierto punto inmoral. Un mínimo de moralidad exige informarse bien sobre esto: menos barreras, menos costos laborales, menos rigidez representan más opciones para conseguir empleo, mejores sueldos y mejor calidad de vida.</p>
<p>&nbsp;</p>
<p>Es claro que una sola ley no resuelve el problema de fondo. Lo óptimo es que tumbemos esas barreras para todos, pero, en un contexto laboral tan rígido y costoso, que vayamos flexibilizando para un sector menos favorecido y que, además, sea temporal es un primer paso en la dirección correcta.</p>
<p>&nbsp;</p>
<p>Luego, si no se desmonta la rigidez laboral, esta ley habrá significado muy poco. Ideológicamente es demasiado importante que esta ley prospere, porque, si no se apoya un cambio pequeño como este ahora, ¿cómo se esperan cambios más profundos en el futuro? Esta medida es un buen comienzo hacia la reforma laboral que nuestro país necesita. Si es aceptada, habrá ganado la visión que entiende por dónde va la senda del desarrollo. Si no, otra vez habremos sacrificado nuestro futuro.</p>
<p>&nbsp;</p>
<p>Autor: Yesenia Álvarez</p>
<p>Foto: IPL Perú</p>
<p>Fuente: <a href="http://peru21.pe/opinion/si-no-ahora-cuando-2208341">Peru 21</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/467-“todos-somos-americanos”-estados-unidos-y-cuba">
			&laquo; “Todos somos americanos”: Estados Unidos y Cuba		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/469-escribo-tu-nombre-libertad-el-atentado-de-parís-según-el-liberalismo">
			Escribo tu nombre, libertad: El atentado de París según el liberalismo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/468-si-no-es-ahora-¿cuándo?#startOfPageId468">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:35:"Si no es ahora, ¿cuándo? - Relial";s:11:"description";s:155:"En la opinión de Yesenia Álvarez &amp;nbsp; Las reacciones al nuevo régimen laboral juvenil dejan serias preocupaciones sobre lo incomprendido que es...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:26:"Si no es ahora, ¿cuándo?";s:6:"og:url";s:87:"http://relial.org/index.php/productos/archivo/opinion/item/468-si-no-es-ahora-¿cuándo";s:8:"og:title";s:35:"Si no es ahora, ¿cuándo? - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/08f61c52357dcc6d2503bfea790efe4d_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/08f61c52357dcc6d2503bfea790efe4d_S.jpg";s:14:"og:description";s:159:"En la opinión de Yesenia Álvarez &amp;amp;nbsp; Las reacciones al nuevo régimen laboral juvenil dejan serias preocupaciones sobre lo incomprendido que es...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:26:"Si no es ahora, ¿cuándo?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}