<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8297:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId263"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Los saqueadores
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/28232e87509dedeadc0cd2b6a94ea3f5_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/28232e87509dedeadc0cd2b6a94ea3f5_XS.jpg" alt="Los saqueadores" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La opinión de Sergio Sarmiento</p>
<p>&nbsp;</p>
<p>"Ya nos saquearon. México no se ha acabado. ¡No nos volverán a saquear!".</p>
<p>José López Portillo</p>
<p>&nbsp;</p>
<p>ZURICH, SUIZA.- Cuando a José López Portillo le estalló la crisis de 1982 no entendió que la economía simplemente le estaba cobrando los años de malas políticas de él y de su predecesor, Luis Echeverría.	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>No creía que la economía se le estaba desmoronando en las manos por sus errores. Era más fácil culpar a los sacadólares, a los especuladores, a los saqueadores, a los malos mexicanos, a los pillos que buscaban cobrarle facturas. La estatización de la banca el 1o. de septiembre de ese año fue su forma de castigar a quienes según él habían querido traicionarlo.</p>
<p>&nbsp;</p>
<p>La presidenta Cristina Fernández de Kirchner de Argentina no ha tocado en público el tema del desplome del peso argentino en los mercados cambiarios, pero su ministro de Economía, Axel Kicillof, se refirió a la situación como "un ataque especulativo muy fuerte sobre la divisa para atentar contra el proyecto económico".</p>
<p>&nbsp;</p>
<p>Por su parte, el presidente Nicolás Maduro de Venezuela, que apenas el 15 de enero se comprometió a no devaluar el bolívar (como lo hizo López Portillo el 5 de febrero de 1982 cuando dijo que defendería el peso como un perro), se ha visto ya obligado a aceptar una fuerte caída de la divisa en un cada vez más complejo sistema de tipos distintos que genera incertidumbre y corrupción. Una y otra vez Maduro ha culpado a los reaccionarios por los problemas económicos que sufre su país.</p>
<p>&nbsp;</p>
<p>La verdad es que los mercados en Venezuela y Argentina no están haciendo más que compensar los estragos causados por años de malas políticas económicas. Hugo Chávez y Nicolás Maduro en Venezuela así como Néstor Kirchner y Cristina Fernández en Argentina han aplicado políticas diseñadas para destruir la inversión productiva privada y aumentar el gasto público de manera inmoderada. El resultado de castigar la oferta y aumentar artificialmente la demanda generará siempre una mayor inflación. Si la inflación es reprimida con controles sobre los precios y el tipo de cambio, como han hecho los dos países, el estallido final simplemente será más fuerte. Los gobiernos de Argentina y Venezuela están repitiendo paso a paso el experimento populista mexicano de los años setenta y ochenta.</p>
<p>&nbsp;</p>
<p>Argentina tiene una inflación de más de 25 por ciento al año. Ocultar la información estadística no ha servido para aliviar el problema. Tampoco ha ayudado saquear los fondos de pensiones o las reservas internacionales para subsidiar el gasto gubernamental. De nada ha servido eliminar la libertad cambiaria.</p>
<p>&nbsp;</p>
<p>Personajes como Echeverría, López Portillo, Chávez, Maduro, Kirchner y Cristina Fernández siempre han justificado los resultados de su ignorancia económica con el argumento de que una conspiración se opone a sus buenas intencionadas políticas económicas. La historia nos dice lo contrario. La economía tiene reglas muy sencillas que si no se obedecen terminan por revertir cualquier política, y el gasto público excesivo aunado a impuestos confiscatorios y restricción de la producción siempre terminan en tragedia.</p>
<p>&nbsp;</p>
<p>Lo anterior no significa que no pueda haber un gasto público con propósitos sociales. Simplemente hay que financiarlo bien. Los países escandinavos han demostrado que el gasto público puede ayudar a reducir las diferencias sociales, siempre y cuando se pague sanamente. Esto es algo que no hicieron ni el México de López Portillo ni la Venezuela de Chávez ni la Argentina de Cristina. Esperemos que el actual gobierno mexicano lo entienda.</p>
<p>&nbsp;</p>
<p>BRASIL</p>
<p>&nbsp;</p>
<p>En Davos la presidenta brasileña Dilma Rousseff descartó que su país pueda ser afectado seriamente por la crisis argentina. "Son circunstancias muy distintas", dijo. Argentina, efectivamente, cuenta con sólo 29 mil millones de dólares en reservas, las cuales han venido cayendo rápidamente, mientras que Brasil tiene más de 360 mil millones. Brasil tuvo 68 mil millones de dólares en inversión extranjera directa en los nueve primeros meses de 2013. Argentina pierde capitales en lugar de atraerlos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: <a href="http://www.sergiosarmiento.com/index.php/columnas/reforma/615-los-saqueadores">Sergio Sarmiento</a></p>
<p>Foto: Caminos de la libertad&nbsp;</p>
<p>Autor: Sergio Sarmiento</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo">
			&laquo; Memoria del saqueo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/268-reeleccionismo-mágico-latinoamericano">
			Reeleccionismo mágico latinoamericano &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/263-los-saqueadores#startOfPageId263">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:24:"Los saqueadores - Relial";s:11:"description";s:159:"La opinión de Sergio Sarmiento &amp;nbsp; &quot;Ya nos saquearon. México no se ha acabado. ¡No nos volverán a saquear!&quot;. José López Portillo &amp...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:15:"Los saqueadores";s:6:"og:url";s:82:"http://www.relial.org/index.php/productos/archivo/opinion/item/263-los-saqueadores";s:8:"og:title";s:24:"Los saqueadores - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/28232e87509dedeadc0cd2b6a94ea3f5_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/28232e87509dedeadc0cd2b6a94ea3f5_S.jpg";s:14:"og:description";s:175:"La opinión de Sergio Sarmiento &amp;amp;nbsp; &amp;quot;Ya nos saquearon. México no se ha acabado. ¡No nos volverán a saquear!&amp;quot;. José López Portillo &amp;amp...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:15:"Los saqueadores";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}