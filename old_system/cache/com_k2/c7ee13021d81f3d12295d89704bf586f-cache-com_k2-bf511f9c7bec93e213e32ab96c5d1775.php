<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10872:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId521"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	EDUCAR -además del Estado- SÍ ES POSIBLE
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/271073fa434dfbedecc5cddef10cff3e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/271073fa434dfbedecc5cddef10cff3e_XS.jpg" alt="EDUCAR -adem&aacute;s del Estado- S&Iacute; ES POSIBLE" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>"La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una sociedad empoderada cuestiona y defiende sus derechos y libertades; no permite abusos ni se conforma con migajas".&nbsp;</p>
<p>&nbsp;</p>
<p>Un artículo de: Tatiana Macías Muentes, Ecuador.&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>"<em>Una educación libre es un sistema donde cualquiera pueda ofertar su particular programa de enseñanza y cualquiera pueda demandarlo sin censuras ni inquisiciones por parte de los nuevos guardianes de la ortodoxia educativa".</em> -Juan Ramón Rallo</p>
<p>&nbsp;</p>
<p>He tenido la posibilidad de asistir a más de un seminario alrededor del mundo y todos han significado una experiencia de crecimiento en varios sentidos. Sin embargo, hace aproximadamente un mes inició una actividad que me generaba bastantes expectativas y las superó con creces. El seminario "Education Policies - The Key to Enlightenment, Participation and Progress" en la IAF de la Fundación Friedrich Naumann -a través del Proyecto Relial- definitivamente me ha otorgado una nueva perspectiva y más que todo me ha llenado de esperanza.</p>
<p>&nbsp;</p>
<p>El contenido propuesto fue exquisito, ya que se cubrieron casi todos los aspectos relacionados con el campo educativo. Analizar temas como manejo de currícula, rol de los stakeholders, uso de la tecnología, entre otros, permitió que se baraje un sinnúmero de propuestas que desde mi único ángulo lamentablemente no hubiese podido obtener. Por ejemplo es curioso que un problema, como suele ser el financiamiento, pueda ser considerado como obstáculo en un país, mientras que otra nación que cuenta con amplio presupuesto, tampoco ha logrado una solución . En el caso de países que cuentan con alto nivel de fondos para el sector educativo, esto no ha sido una herramienta para un verdadero aprendizaje, mientras en Pakistán donde tienen un 2.1% del presupuesto nacional asignado consideran que esto es uno de los principales impedimentos para impulsar el desarrollo educativo.</p>
<p>&nbsp;</p>
<p>Como docente puedo aseverar que las técnicas de aprendizaje y metodologías de enseñanza empleadas fueron brillantes y acertadas. Manali Shah y Stefan Melnik , como facilitadores demostraron -con el ejemplo- qué es educar y que la nueva educación que esperamos y proponemos, es posible.</p>
<p>&nbsp;</p>
<p>Fue gratificante conocer de primera mano casos como los de las escuelas low-cost, presididas por Ekta Sodha, donde el rol del Estado es prescindible y con un pago mínimo equivalente a US$6-US$12 brindan un servicio educativo de alta calidad. Sodha Schools es uno de los tantos casos bajo esta modalidad que ha recopilado el profesor James Tooley y que demuestran que la sociedad civil tiene un papel protagónico en el desarrollo de una comunidad . Sin embargo, adicional a ello, fue bastante alentador aprender que al igual que estos casos de renombre y trascendencia mundial, existen iniciativas igualmente exitosas presididas por los mismos participantes del seminario.</p>
<p>&nbsp;</p>
<p>Uno de los proyectos que cabe resaltar es Karkhana en Nepal. Según la organización, son una compañía de educación. "Somos un programa multidisciplinario extracurricular con un enfoque único para el aprendizaje. Nuestros profesores - ingenieros, diseñadores, artistas, científicos - pretenden convertir el aula en un laboratorio para el descubrimiento. Queremos que nuestros estudiantes obtengan una idea clave: el mundo es maleable. Nuestros talleres están diseñados para enseñar conceptos de Ciencia, Tecnología, Ingeniería, Artes y Matemáticas (STEAM por sus siglas en inglés Science, Technology, Engineering, Arts and Mathematics), todo a la vez".</p>
<p>&nbsp;</p>
<p>Por otro lado, "Centre for civil society" de la India es una propuesta en la que se impulsa el cambio social a través de políticas públicas. Su trabajo en la educación y la formación política promueve la votación libre y la rendición de cuentas constante en todos los sectores públicos y privados. Según su visión, la CCS "prevé un mundo donde cada individuo maneje su vida bajo los principios de elección con respecto a temas del ámbito personal, económico, esferas políticas y todas las instituciones donde se aplique."</p>
<p>&nbsp;</p>
<p>Finalmente, en Filipinas el Pantawid Pamilyang Pilipino Program muestra cómo en situaciones extremas la contribución externa es beneficiosa. Este programa denominado las 4P´s por sus siglas, según su credo es "una medida de desarrollo humano del gobierno nacional, que otorga donaciones en efectivo condicionadas a los más pobres entre los pobres, para mejorar la salud, la nutrición y la educación de los niños 0-18 años de edad. Se sigue el modelo de los esquemas de transferencias monetarias condicionadas (TMC) en los países latinoamericanos y africanos, que han elevado a millones de personas en todo el mundo de la pobreza. El Departamento de Bienestar Social y Desarrollo (DSWD) es la principal agencia gubernamental de las 4P".</p>
<p>&nbsp;</p>
<p>Todos estos proyectos son una inyección de aliento que permite reconocer que siempre hay una alternativa para hacer las cosas mejor; que todo empieza con sentimiento de disconformidad que se transforma en idea y luego, gracias a la perseverancia y el deseo de cambiar el entorno, se vuelve tan efectivo como tangible. Estos sueños hechos realidad cambian no solamente el futuro de su líder o de quien lo emprende, sino de todos aquellos que tienen la oportunidad de ser parte de esta nueva corriente educativa.</p>
<p>&nbsp;</p>
<p>El intercambio de ideas con personas de múltiples regiones del mundo con quienes compartimos la misma insatisfacción con respecto al sistema educativo convencional y a quienes nos une el mismo deseo de una revolución incipiente, deja en nuestras manos responsabilidad intrínseca de hacer algo al respecto desde nuestras respectivas trincheras. Políticos, educadores, miembros de think thanks, estudiantes, entre otras vocaciones que tienen como punto común un mejor futuro, demostraron con su trabajo que incluso los pequeños esfuerzos siempre dan sus frutos; y he ahí la razón de mi esperanza.</p>
<p>&nbsp;</p>
<p>La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una sociedad empoderada cuestiona y defiende sus derechos y libertades; no permite abusos ni se conforma con migajas. Una sociedad de este tipo es peligrosa para quienes pretenden manipularla y se vuelve enemiga de sistemas donde se merma la iniciativa individual, no se defiende la vida o se irrespeta la propiedad.</p>
<p>&nbsp;</p>
<p>Afortunadamente este shot de optimismo lo compartimos de manera generalizada y confío que en un futuro no muy lejano tendremos más Sodha Schools, Karkhanas o CCS en diferentes rincones del planeta, tal vez con diferentes nombres o en diferentes modalidades pero siempre con el mismo horizonte: muchos más niños y jóvenes empoderados para aprender y libres para crecer.</p>
<p>&nbsp;</p>
<p>Fuente: autor.&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
			&laquo; Perú: retos de gestión para el próximo gobierno		</a>
		
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible#startOfPageId521">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:51:"EDUCAR -además del Estado- SÍ ES POSIBLE - Relial";s:11:"description";s:155:"&quot;La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una socieda...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:42:"EDUCAR -además del Estado- SÍ ES POSIBLE";s:6:"og:url";s:103:"http://relial.org/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible";s:8:"og:title";s:51:"EDUCAR -además del Estado- SÍ ES POSIBLE - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/271073fa434dfbedecc5cddef10cff3e_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/271073fa434dfbedecc5cddef10cff3e_S.jpg";s:14:"og:description";s:159:"&amp;quot;La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una socieda...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:42:"EDUCAR -además del Estado- SÍ ES POSIBLE";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}