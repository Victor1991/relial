<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10720:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId250"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Argentina: Saqueos en ¿un país con buena gente?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/e303e2027514497aaa0603a129a3eb42_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/e303e2027514497aaa0603a129a3eb42_XS.jpg" alt="Argentina: Saqueos en &iquest;un pa&iacute;s con buena gente?" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Profunda pena me produce ser testigo de los hechos que transcurren hoy en Argentina. El fin de ciclo llegó antes de lo que se pronosticaba y con él parece haber llegado el incendio de todo el país. Los saqueos, el silencio de nuestros dirigentes, la figura cuasi-ausente de Cristina Fernández de Kirchner y el acuartelamiento de la fuerzas de seguridad se han convertido en la postal diaria de los últimos días. El modelo no funciona -nunca funcionó- pero hoy se pueden palpar los resultados que realmente arrojó. Lamentablemente es tarde, por lo que necesitamos con urgencia que los cambios se produzcan y a partir de eso - y de una buena vez por todas - construir una república democrática real basada en el estado de derecho, las libertades individuales y la libre empresa y dejar de lado festejos populistas ridículos sobre una democracia que no sirve.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>En las últimas elecciones el oficialismo perdió en todas partes, sin embargo, discursivamente declararon ser la primera fuerza del país. El oficialismo venezolano de Maduro utilizó exactamente el mismo argumento cuando en las elecciones del 8 de Diciembre pasado perdió en las ciudades más importantes del país y hasta en el estado donde nació el ahora difunto Hugo Chávez. ¿Extraña casualidad?. Afortunadamente en Argentina el triunfo quedo únicamente en el discurso porque a todo el mundo le quedó muy claro que la gente no votó por este modelo de "inclusión", de "igualdad de oportunidades" y de "derechos para todos y todas", seguramente porque no los vio en su día a día, no los vio plasmados en la realidad y dicho desencanto se expresó en las urnas. Y como todo se demuestra en la realidad, pese a que finjamos y hasta manifestemos lo contrario, la verdadera crisis del modelo llegó y con él, el resultado de diez años de decadencia, de mentiras, de robos, de insultos y de atentados a los derechos individuales, a la Constitución Nacional y a la República.</p>
<p>&nbsp;</p>
<p>2000 saqueos en 14 provincias y pérdidas de casi 600 millones de pesos diarios han sido el saldo de estos últimos días. Argentina está al borde de un enfrentamiento generalizado donde, por un lado se encuentra la gente trabajadora, emprendedora, que luchó toda su vida para dejarle un futuro a sus hijos y quienes creen que el esfuerzo es la condición para crecer; y por el otro, los mantenidos por el Estado, la gente que piensa que debe seguir siendo mantenida a cambio de nada, los que justifican los saqueos con la malversación que el gobierno hace de nuestros impuestos y a los que las netbook de "conectar igualdad" no parecen conformarlos, por lo que consideran justo robar televisores LCD, acondicionadores de aire, zapatillas, ropa, bebidas alcohólicas y lo que sea que surja. El derecho a la vida y a la propiedad privada, parecen ser cosa del pasado en Argentina. Los propietarios de los locales y la gente que por miedo ha permanecido encerrada en sus casas, se han tenido que armar a la fuerza para lograr proteger lo que les ha llevado años construir y para salvaguardar a sus seres queridos. La postal más común de Diciembre de 2013 en Argentina es un grupo de saqueadores esperando y corriendo para robar un comercio y gente armada al frente de su negocio cuidando que no les roben sus pertenencias porque el Estado no le brinda lo más básico y esencial: seguridad.</p>
<p>&nbsp;</p>
<p>El caso específico del acuartelamiento de la Policía en Córdoba fue ni más ni menos que un ajuste de cuentas político desde la Nación cuando el Gobernador no se encontraba en el país. Cabe entonces recordar los incendios intencionales que tuvieron lugar en las sierras hace unos meses atrás y que dejaron a la provincia en llamas. Pero, como somos la Provincia Kastigada estamos acostumbrados a solucionar nuestros problemas sin ayuda de nadie y a través del diálogo y de la unión de todas las fuerzas políticas y religiosas, se pudo llegar a un acuerdo. Pareciera que para la Nación los cordobeses somos ciudadanos argentinos tan sólo a la hora de pagar nuestros impuestos, ya que desde la Casa Rosada no se dignaron a enviarnos las fuerzas de Gendarmería a tiempo. Pero el drama cordobés terminó siendo tan sólo el comienzo de una situación delincuencial que se extendió a buena parte del país. Dinero para guardar en las cajas fuertes de nuestros mandatarios parece haber mucho, pero para solventar una policía que nos proteja, que prevenga los delitos y que no se corrompa con dinero fácil del narcotráfico, no parece haber. El gobierno nacional se lava las manos diciendo que para eso están los policías provinciales, pero no tiene forma de justificar el no envío de gendarmería, como si los habitantes de esta supuesta República Federal no pagáramos impuestos nacionales y nos fuésemos, primero, como lo dice la Constitución Nacional hijos de esta patria y después de las provincias. El discurso del gobierno nacional cambia por completo a la hora de hablar de la coparticipación de los recursos provinciales.</p>
<p>&nbsp;</p>
<p>Cristina Fernández de Kirchner se ha limitado a hablar de los saqueos como parte de una "estrategia conspirativa" contra su gobierno y a repetir que "no se trata de una casualidad". Mientras tanto la situación en Argentina es lamentable, un gobierno virtualmente acéfalo y las fuerzas de seguridad acuarteladas. El Estado no puede brindar seguridad, una de sus funciones fundamentales, no hay derecho a la vida ni a la propiedad, hay crisis civil no por hambre ni por necesidades sino porque la igualdad y la inclusión en términos reales no existen y los "derechos para todos y todas" se gastaron entre otras cosas, en subsidios para los terroristas de la época del gobierno militar.</p>
<p>&nbsp;</p>
<p>Hoy necesitamos con urgencia un cambio profundo en la manera de entender la democracia y debemos actuar en consecuencia. Tenemos que abrir los ojos y aceptar que estamos ante una crisis muchísimo más profunda que la de 2001. Es particularmente triste ver como el gobierno nacional festeja los 30 años de democracia con pitos y flautas y a la vuelta de la esquina los hechos demuestran el escenario contrario. Salgamos, alcemos nuestras voces y construyamos, con total sinceridad, lo que realmente queremos, una república democrática donde nosotros y nuestros seres queridos podamos vivir libres, seguros y en paz.</p>
<p>&nbsp;</p>
<p style="text-align: justify;">Autor: Daniela E. Rodríguez, &nbsp;Directora de Programas de la Fundación HACER en Argentina&nbsp;</p>
<p style="text-align: justify;"><span style="text-align: left;">Foto: HACER</span></p>
<p style="text-align: justify;"><span style="text-align: left;">Fuente:&nbsp;</span><a style="text-align: left;" href="http://www.hacer.org/" target="_blank"><span style="font-size: 9pt; line-height: 115%; color: #3a6999; border: 1pt none windowtext; padding: 0cm;">HACER</span></a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste">
			&laquo; Hay una camino, pero no es éste		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo">
			Memoria del saqueo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente?#startOfPageId250">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:58:"Argentina: Saqueos en ¿un país con buena gente? - Relial";s:11:"description";s:155:"Profunda pena me produce ser testigo de los hechos que transcurren hoy en Argentina. El fin de ciclo llegó antes de lo que se pronosticaba y con él pa...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:49:"Argentina: Saqueos en ¿un país con buena gente?";s:6:"og:url";s:114:"http://www.relial.org/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente";s:8:"og:title";s:58:"Argentina: Saqueos en ¿un país con buena gente? - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/e303e2027514497aaa0603a129a3eb42_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/e303e2027514497aaa0603a129a3eb42_S.jpg";s:14:"og:description";s:155:"Profunda pena me produce ser testigo de los hechos que transcurren hoy en Argentina. El fin de ciclo llegó antes de lo que se pronosticaba y con él pa...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:49:"Argentina: Saqueos en ¿un país con buena gente?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}