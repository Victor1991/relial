<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10834:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId292"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Think Tanks And The Power of Networks
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/dbe05350458c15fa6c802fb686391131_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/dbe05350458c15fa6c802fb686391131_XS.jpg" alt="Think Tanks And The Power of Networks" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>The output of think tanks usually falls into four categories: published research, educational programs, advocacy campaigns, and "do-tank" products. "Do-tanks" provide services, such as pro bono legal defense, or management and technical support.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Working through networks has become essential for achieving some think tank goals. Developing, nurturing and mobilizing networks became another task for governments, businesses, and NGOs. Networking became so important for some organizations that a few even changed their names. The National Foundation for Teaching Entrepreneurship, is now the Network for Teaching Entrepreneurship. The Atlas Economic Research Foundation, although retaining its legal name, is branding itself more as the "Atlas Network."</p>
<p>&nbsp;</p>
<p>Depending on their focus and goals, networks face different challenges. Some networks, such as Al-Qaeda and its superstructure in hostile governments, need as much secrecy as possible. So do those who combat them. Others, such as the non-ideological European Network of Political Foundations, which includes 69 political party affiliated organizations can afford almost total transparency. Market-oriented networks have different degrees of transparency, needs, and sponsors.</p>
<p>&nbsp;</p>
<p>The Atlas Network works to strengthen the worldwide freedom movement by identifying, training, and supporting individuals with the potential to found and develop effective independent organizations. It connects more than 400 think tanks in over 80 countries. It was founded by British entrepreneur Antony Fisher in 1981. It seeks to win the long-term policy battles by helping intellectual entrepreneurs create credible institutes—well-managed and independent of vested interests—that use sound business practices to advance sound public policy ideas.</p>
<p>&nbsp;</p>
<p>State Policy Network (SPN) started as a membership organization and then was restructured to work in a similar manner as Atlas but with the focus on U.S. state-focused think tanks, and covering all areas of policy, not just political economy. It was founded in 1992 by Tom Roe who, like Fisher, was a businessman enamored with the free society. It works to improve the practical effectiveness of state-focused think tanks. Like Atlas, SPN seeks to strengthen the institutional capacity of think tanks. Membership in SPN, over 100 regular and associate members, is voluntary and by invitation only, and more formal than Atlas.</p>
<p>&nbsp;</p>
<p>Atlas and SPN are not endowed, they do not accept government money, and their budgets, approximately $8 and $7 million respectively, comes mostly from foundations and individuals. In the most recent financials, Atlas has reported less than 1 percent of its income coming from public corporations, and SPN just 3 percent (the industry average is 10 percent of corporate funding). Atlas is ahead in social media reach, and SPN beats Atlas in budget growth and in number of donors (approximately 3000 vs 1000 donors).</p>
<p>&nbsp;</p>
<p>Two of the largest market-oriented networks in Europe are the European Ideas Network (EIN) and the Stockholm Network (London). EIN is affiliated with the Popular Party group which has the largest representation in the EU parliament. The Stockholm Network operates as a private corporation. EIN works with over 250 think tanks and foundations and 3000 individuals in Europe and across the globe. The Stockholm Network lists over 120 groups. A large number of EIN members are listed as part of the Stockholm and Atlas Networks. EIN receives funding from the European Union budget and the parliament. Total operations run over $2 million per year. The Stockholm Network works with more privacy but judged by its annual reports, it is safe to assume that most of its funding comes from corporations. The Center for European Studies, list 29 members, and it also acts as a "do-tank" helping in the preparation of programs and policy documents.</p>
<p>&nbsp;</p>
<p>RELIAL, with approximately 40 members and over 12 associate organizations, is one of the strongest networks in Latin America. Its mission is to become a "belligerent and efficient liberal network that helps convert Latin America into a region characterized by liberal democracies and prosperous societies, committed to the principles of liberty, individual responsibility, respect for private property, a market economy, and the primacy of rule of law and peace, in order to achieve rising living standards in the region." Its current president is economist Ricardo López Murphy, former Minister of Defense of Argentina. RELIAL was developed and sponsored by the Friedrich Naumann Foundation (Germany).</p>
<p>&nbsp;</p>
<p>Leading think tanks also have formal networking efforts. The Fraser Institute (Canada), has its Economic Freedom Network, devoted to promoting economic freedom around the world. The members help Fraser disseminate its Economic Freedom of the World report and provide advice to its authors. The network has member institutes in over 80 nations and all are part of the less formal, but more extended, Atlas Network. FAES in Spain excels at networking, they have a European network, with 29 members, as well as a network of close to 700 young Latin Americans who passed through their training programs. They have used their networks to elaborate and distribute and promote policy proposals with regional value.</p>
<p>&nbsp;</p>
<p>Heritage Foundation also has network-type services. It hosts an annual Heritage Resource Bank meeting which attracts several hundred think tanks giving them an opportunity to network and strategize. Think Tanks in Europe and Latin America have replicated the format and conduct smaller versions of these networking events.</p>
<p>&nbsp;</p>
<p>How effective are these networks in helping win battles for freedom? We have few independent studies, but in the region I am most familiar with, the Americas, a study by Karin Fischer &amp; Dieter Plehwe sponsored by the Friedrich Ebert Foundation and published in "State of Nature" an online journal credited several networks mentioned in this piece for helping stop the tide toward a more socialist Latin America. It concludes: "It is most important to realize that the various think tanks are not stand alone operations. Due to the embeddedness of each of the organizations in a comprehensive network, the total is larger than the sum of the individual parts." From the news we receive each day from Latin America, I hope they become even more effective.</p>
<p>&nbsp;</p>
<p>Foto: Forbes</p>
<p>Fuente: <a href="http://www.forbes.com">www.forbes.com</a></p>
<p>Autor:&nbsp;<a class="exit_trigger_set" href="http://www.forbes.com/sites/alejandrochafuen/">Alejandro Chafuen</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/290-declaración-sobre-la-situación-en-venezuela-federación-internacional-de-juventudes-liberales">
			&laquo; Declaración sobre la situación en Venezuela (Federación Internacional de Juventudes Liberales)		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/293-declaración-de-los-amigos-de-la-carta-democrática-interamericana-sobre-la-situación-en-venezuela">
			Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks#startOfPageId292">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:46:"Think Tanks And The Power of Networks - Relial";s:11:"description";s:153:"The output of think tanks usually falls into four categories: published research, educational programs, advocacy campaigns, and &quot;do-tank&quot; pr...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:37:"Think Tanks And The Power of Networks";s:6:"og:url";s:103:"http://relial.org/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks";s:8:"og:title";s:46:"Think Tanks And The Power of Networks - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/dbe05350458c15fa6c802fb686391131_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/dbe05350458c15fa6c802fb686391131_S.jpg";s:14:"og:description";s:161:"The output of think tanks usually falls into four categories: published research, educational programs, advocacy campaigns, and &amp;quot;do-tank&amp;quot; pr...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:37:"Think Tanks And The Power of Networks";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}