<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:18863:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId394"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Política local y participación ciudadana
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/34570ba96cc3a4daee50221a47a4e2ec_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/34570ba96cc3a4daee50221a47a4e2ec_XS.jpg" alt="Pol&iacute;tica local y participaci&oacute;n ciudadana" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Eugenio Guerrero</p>
<p>&nbsp;</p>
<p>INTRODUCCIÓN</p>
<p>&nbsp;</p>
<p>El presente reporte recoge la capacitación obtenida en la academia de formación Theodor-Heuss-Akademie de la Fundación Friedrich Naumann para la Libertad (FNFF), Gummersbach, que tuvo lugar del 4 al 16 de mayo.. El programa cargado de diversas y extraordinarias experiencias desde una perspectiva de la libertad en las políticas locales y el planteamiento democrático con gobiernos limitados. El tema tratado a lo largo y profundo del programa estuvo basado en la Política Local y la Participación ciudadana. Doce días de aprendizajes interesantes en las dinámicas grupales, un intercambio cultural sumamente grandioso con personas muy inteligentes que defienden la liberad, de alrededor de 20 países, totalmente distintos en culturas y tradiciones pero juntos con una meta: La Libertad.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>A través de este reporte deseo exponer brevemente lo aprendido con base a la estructura del programa, y cómo todo este conocimiento ponerlo en práctica en mi país y cómo usaré tales conocimientos a favor de la institución que tan amablemente me postuló a tan prestigioso programa.</p>
<p>&nbsp;</p>
<p>OBJETIVOS DEL PROGRAMA</p>
<p>&nbsp;</p>
<p>- El objetivo principal del programa fue el desarrollo de capacidades y la formación de los distintos mecanismos que en la Política Local logran dinamizar la interacción ciudadana por medio de múltiples canales de comunicación y participación para la toma de decisiones relacionadas a las problemáticas locales: desde el presupuesto y asignación de los recursos, hasta las tasas impositivas, construcción, ordenanzas, elecciones, entre otros.</p>
<p>&nbsp;</p>
<p>- Todo el contenido bajo los valores liberales dentro de la política local.</p>
<p>&nbsp;</p>
<p>- Conocer distintas instancias de poder, líderes políticos, políticos en ejercicio y establecer conversaciones para intercambio de ideas y respuestas a interrogantes de cómo funcionan los gobiernos locales en Alemania.</p>
<p>&nbsp;</p>
<p>- El intercambio cultural y de realidades entre los diversos participantes organizados en grupos por región y también de manera mixta, con el fin de reconocer puntos en común, debatir ideas con respecto a las funciones de los poderes locales y sus límites a la hora de ejercer su influencia en la ciudadanía.</p>
<p>&nbsp;</p>
<p>- Mesas de discusión sobre los distintos planteamientos locales y propuestas desde diferentes grupos e individuos que actúan dentro de la sociedad: sociedad civil, partidos políticos, organizaciones no gubernamentales, Think Tank y organizaciones gremiales de distintos orígenes y temáticas a tratar.</p>
<p>&nbsp;</p>
<p>- Breve historia de Alemania y los aspectos fundamentales de su política. Un recorrido histórico en como tratan de limitar el poder y garantizar la libertad, el respeto a los derechos de propiedad y la no obstaculización de la libre iniciativa.</p>
<p>&nbsp;</p>
<p>- Proporcionar herramientas de comunicación para la expresión pública con diversos actores sociales y técnicas para el debate relacionados a temas ideológicos vinculados con las políticas públicas desde distintas perspectivas.</p>
<p>&nbsp;</p>
<p>- Fortalecer una red mundial en defensa de los principios liberales desde los distintos países para perseguir el camino a la libertad y la prosperidad.</p>
<p>&nbsp;</p>
<p>LO APRENDIDO</p>
<p>&nbsp;</p>
<p>Para mí y, supongo, para muchos liberales tiende a ser nuevo especialmente en América Latina, tocar el tema del ejercicio del poder; buscando limitar al gobierno, nos acostumbramos a no tener en cuenta qué ocurriría si liberales auténticos aspiren y lleguen al poder: qué haríamos, cómo lo haríamos, de qué manera lo que aprendemos diariamente de los distintos autores lo pondríamos a práctica de tener la oportunidad.</p>
<p>&nbsp;</p>
<p>Es nuevo porque por lo menos en mi país, de cultura socialista, intervencionista y populista, jamás ha existido un partido liberal realmente. Intentos históricos que no llegaban a nada y cuando se logró, no articulaban los principios que hoy día defendemos desde las distintas posturas y escuelas dentro del liberalismo.</p>
<p>&nbsp;</p>
<p>Y si algo aprendí es que existe una realidad político-partidista en ejercicio que puede servirnos como ejemplo para aspirar, asesorar o coadyuvar a aquellas personas que defendiendo similares ideales, se tomen su tiempo y descubran su vocación en la política partidista y se lancen triunfantes en períodos electorales con grandes o pequeños equipos buscando influir y cambiar las cosas para mantener la libertad, vital para la pervivencia de nuestra civilización.</p>
<p>&nbsp;</p>
<p>Esta realidad está, por lo menos, en Alemania con el FDP (Partido Liberal Alemán). Con una larga tradición en la diatriba política por el poder y plasmar los principios liberales, han desarrollado una vasta experiencia en la participación electoral, la aplicación de reformar y presentar unos principios que, si a no todos le gusta por la cultura antiliberal de las últimas década en el viejo continente, se mantenido y convencido con éstos sin decaer en la defensa de los mismo.</p>
<p>&nbsp;</p>
<p>Interesante fue enterarse de cómo se manejan los gobiernos locales:</p>
<p>&nbsp;</p>
<p>- Los Estados son Federados</p>
<p>&nbsp;</p>
<p>- La autonomía de los gobiernos locales se mantienen a distancia del poder central porque hay votos directos (a diferencia de los gobernadores) en el proceso de elección de los Alcaldes y Concejales.</p>
<p>&nbsp;</p>
<p>- Competencias limitadas y transparencia en el manejo de los recursos.</p>
<p>&nbsp;</p>
<p>- Mecanismos de consulta y discusión pública como especies de cabildos abiertos para la discusión de quienes deseen participar en la toma de decisiones colectiva.</p>
<p>&nbsp;</p>
<p>- Se han desarrollado consejos locales de participación ciudadana donde los gremios, grupos de presión y sociedad civil organizada tienen voz importante en determinar los resultados en las discusiones públicas (no en todos los estados).</p>
<p>&nbsp;</p>
<p>Pude también aprender con la Dra. Monika Ballin (Consejera de la ciudad de Leverkusen) el paradigma hacia la Nueva Gestión Pública.</p>
<p>&nbsp;</p>
<p>Los principales puntos de esta propuesta son:</p>
<p>&nbsp;</p>
<p>- NPM - Una visión General de la Nueva Gestión Pública.</p>
<p>&nbsp;</p>
<p>- Estado pequeño</p>
<p>&nbsp;</p>
<p>- La separación de los niveles de toma de decisiones</p>
<p>&nbsp;</p>
<p>- Nueva actitud frente al servicio (más relacionado a clientes privados que pagan y exigen calidad de su servicio).</p>
<p>&nbsp;</p>
<p>- Nuevo Modelo de Control.</p>
<p>&nbsp;</p>
<p>Los valores que deben estar inmersos dentro de cualquier Gobierno liberal: Responsabilidad individual, justicia, elección libre, tolerancia, diversidad, competencia, economía de libre mercado, transparencia, propiedad privada, descentralización y estado subsidiario.</p>
<p>&nbsp;</p>
<p>Las dinámicas grupales –muy numerosas- nos enseñaron las realidades locales en los distintos países, aunque el trabajo más intenso estuvo cuando las reuniones eran regionales. En el caso de Latinoamérica en el trabajo en equipo pudimos aprender las distintas realidades y problemáticas locales en Venezuela, México, Honduras, Brasil y Argentina. Las similitudes y diferencias y cómo las diferentes organizaciones y partidos proponen cambios: desde la descentralización hasta la autonomía real en las administraciones pública locales. Sin duda, quien sufre la peor situación en esta área es Venezuela con el proyecto comunal.</p>
<p>&nbsp;</p>
<p>Importante también fue lograr articular estrategias en grupo de cómo hacer que Gobierno local, sociedad civil y partidos políticos tengan un espacio donde el respeto y la deliberación sana puedan manifestarse.</p>
<p>&nbsp;</p>
<p>Tomar en cuenta que el elemento de la participación y su promoción a través de la apertura y la comunicación pueden mejorar los procesos de gobernabilidad y estabilidad local en la transparencia del ejercicio del poder local.</p>
<p>&nbsp;</p>
<p>La necesidad de planificar estrategias con base a casos de estudios específicos según realidades. Desarrollar encuestas, estudios de opinión, análisis costo beneficios, deliberación publica para la comunicación de la propuesta final posterior a los pasos iniciales y así lograr un mayor nivel no sólo de apoyo, también de participación local.</p>
<p>&nbsp;</p>
<p>Tuvimos la oportunidad de asistir al Congreso Nacional del FDP, donde pude presenciar los valores liberales del partido defendidos cabalmente en el discurso de Christian Lindner (presidente del FDP), cuál es la visión de Europa que tiene el partido y de qué manera se puede garantizar mayor libertad y prosperidad a través del respeto a las instituciones del país.</p>
<p>&nbsp;</p>
<p>Nos reunimos con los jóvenes liberales del FDP. En una pequeña reunión pudimos interactuar con su representante nacional e indagar en el funcionamiento del partido, su organización, estructura y de qué manera proponen los cambios dentro y fuera del partido por medio de los valores liberales.</p>
<p>&nbsp;</p>
<p>Asistimos al parlamento de Sajonia donde tuve el honor de asistir a una reunión con el diputado Bejamin Karabinski (Diputado del parlamento estatal de Sajonia y consejero municipal de Freiberg). Tratamos temas desde la legislación local al funcionamiento de la estructura del partido en la promoción de la participación local y la interacción política entre el Estado y la política local de Sajonia.</p>
<p>&nbsp;</p>
<p>También logramos reunirnos con Torsten Herbst (Diputado y secretario general del FDP en Sajonia).</p>
<p>&nbsp;</p>
<p>Desarrollamos una actividad grupal enfocada en dos puntos importantes los cuales son:</p>
<p>&nbsp;</p>
<p>- La necesidad de un enfoque estratégico</p>
<p>&nbsp;</p>
<p>- Elementos e ideas estratégicas: la importancia de los resultados de los gobiernos locales para el cambio de las preferencias electorales.</p>
<p>&nbsp;</p>
<p>En todo el programa, presentábamos exposiciones realizadas en equipo y proporcionábamos informes sobre el estatus del trabajo y el desarrollo de las diferentes estrategias como actores políticos de partido o intrigantes de organizaciones no gubernamentales (Think tank y grupos de presión).</p>
<p>&nbsp;</p>
<p>Posteriormente vimos un contenido interesante sobre las elecciones locales en el Estado de Renania del Norte – Westfalia. Temas tratados: Sistema electoral, candidatos, campañas y medios.</p>
<p>&nbsp;</p>
<p>Finalmente recibimos un taller de debate competitivo y técnicas para estructuración del discurso, comunicación a nivel político y prácticas grabadas, también ensayos de entrevistas periodista-entrevistado y entrevistado-periodista, donde pudimos hacer críticas sobre nuestros errores, observarlos en general y desarrollar opiniones para la mejora considerable y superación de diversos obstáculos a la hora de comunicar de manera efectiva.</p>
<p>&nbsp;</p>
<p>Pudimos disfrutar de diversos vídeos donde expertos en comunicación política dictaban cursos sobre cuál sería el orden más acertado y adecuado a la hora de conquistar terreno político y la venta de ideas para influir en la sociedad.</p>
<p>&nbsp;</p>
<p>¿CÓMO LO APLICARÍA EN MI PAÍS DESDE LA INSTITUCIÓN A DONDE PERTENEZCO?</p>
<p>&nbsp;</p>
<p>Dentro del conflicto actual y evidente en Venezuela resulta casi imposible porque la libertad no es compatible con la coerción y la represión. Pero si sería posible en un contexto más democrático donde las propuestas y procesos de reforma se puedan realizar de manera pacífica y consensuada. Pero aun así trataremos de transmitir todo lo aprendido a través de gobiernos locales que aun están en manos de demócratas.</p>
<p>&nbsp;</p>
<p>Primero: he pensado que con respecto a estos tipos de organizaciones locales efectivas en el manejo del poder limitado con transparencia, hay que comunicarlo a los diferentes actores de la sociedad. Que sepan que existen, que es posible hacer política desde distintos modos con mejores resultados sin afectar considerablemente la libertad de las personas.</p>
<p>&nbsp;</p>
<p>Segundo: en el plano de la formación de políticas públicas en CEDICE Libertad y CEDICE Joven, sería de gran interés exponerlo a los diversos líderes que en política partidista participan y aspiran llegar al poder para cambiar las cosas.</p>
<p>&nbsp;</p>
<p>Tercero: profundizar mi formación en el paradigma de la Nueva Gestión Pública que, hasta los momentos, es la que más coincide con una democracia de gobierno limitado; llevarlo a la mesa de formación y desarrollar un programa que coincida con las políticas públicas que CEDICE ha venido desarrollando desde hace más de 30 años.</p>
<p>&nbsp;</p>
<p>Cuarto: el país necesita de un cambio completo en el sistema político, una democracia limitada basada en la descentralización y limitar el control del presidente manipulando a las mayorías. Por eso que un grupo de jóvenes y yo no descartamos proponer un sistema como el alemán de elecciones para contener el poder.</p>
<p>&nbsp;</p>
<p>Quinto: Diseñar estos mecanismos locales aprendidos en el programa para fortalecer la formación y participación ciudadana. En la Venezuela actual la mayoría de las personas no conocen sus derechos y deberes; aún menos que radio de acción tienen para frenar o no ciertas aplicación de políticas locales, y aunque no existan actualmente no descartamos luchar por la aplicación de éstas en algún momento y que sea la sociedad civil organizada y no organizada (como corresponde) que no permita la expansión del poder en detrimento de sus libertades y las de otros.</p>
<p>&nbsp;</p>
<p>Sexto: diseminar los valores morales, políticos y filosóficos de un gobierno liberal en política local.</p>
<p>&nbsp;</p>
<p>Séptimo: establecer alianzas con los Alcaldes y Concejales cercanos a CEDICE e influidos por la institución, para desarrollar políticas públicas. Actualmente nos encontramos cerca de grupos de jóvenes políticos y políticos en general con los cuales a través de los programas de formación de la institución, estamos influyendo con estas ideas para generar cambios en el modelo y método en que desarrollan las políticas públicas.</p>
<p>&nbsp;</p>
<p>Agradezco una vez mas esta experiencia que deseamos estén seguros que será una oportunidad para apoyar a Concejales, Alcaldes, ciudadanos que trabajen por una sociedad mas transparente y de participación activa en la sociedad como lo hacemos desde CEDICE Libertad.</p>
<p>&nbsp;</p>
<p>Foto: CEDICE</p>
<p>Autor: Eugenio Guerrero</p>
<p>Fuente: Texto proporcionado por el autor</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa">
			&laquo; La inmensa tarea de los reyes de España		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/399-la-fuerte-oposición-que-santos-enfrentará-en-su-segundo-tiempo">
			La fuerte oposición que Santos enfrentará en su segundo tiempo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana#startOfPageId394">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:51:"Política local y participación ciudadana - Relial";s:11:"description";s:157:"En la opinión de Eugenio Guerrero &amp;nbsp; INTRODUCCIÓN &amp;nbsp; El presente reporte recoge la capacitación obtenida en la academia de formación T...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:42:"Política local y participación ciudadana";s:6:"og:url";s:109:"http://www.relial.org/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana";s:8:"og:title";s:51:"Política local y participación ciudadana - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/34570ba96cc3a4daee50221a47a4e2ec_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/34570ba96cc3a4daee50221a47a4e2ec_S.jpg";s:14:"og:description";s:165:"En la opinión de Eugenio Guerrero &amp;amp;nbsp; INTRODUCCIÓN &amp;amp;nbsp; El presente reporte recoge la capacitación obtenida en la academia de formación T...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:42:"Política local y participación ciudadana";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}