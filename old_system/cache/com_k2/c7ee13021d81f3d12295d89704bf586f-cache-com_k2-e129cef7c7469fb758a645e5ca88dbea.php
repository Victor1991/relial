<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8833:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId122"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El gobernante de las dos caras
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/97aa066dcc42404e7602768333af5659_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/97aa066dcc42404e7602768333af5659_XS.jpg" alt="El gobernante de las dos caras" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Rafael Correa, el presidente de los ecuatorianos, es un personaje contradictorio hasta bordear la esquizofrenia. Tiene, por lo menos, dos caras. Veamos.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>A veces utiliza un lenguaje de izquierda y se proclama partidario del socialismo radical, pero otras es un católico conservador, adversario del matrimonio gay, que se emociona conversando con el papa Francisco.</p>
<p>&nbsp;</p>
<p>Se presenta como un demócrata, pero sostiene una visión retorcida de los valores de la libertad y opina que Fidel Castro no es un dictador, que Gadaffi es una figura injustamente "maltratada", y que el tiranuelo antisemita Ahmadineyad, un peligroso guerrerista que amenaza con ahogar a los israelíes en el mar, o destruirlos con armas atómicas, es un venerable personaje, aliado de su país, quien, naturalmente, considera al ecuatoriano como su "solidario hermano y amigo".</p>
<p>&nbsp;</p>
<p>Correa, que da lecciones de economía al Banco Central Europeo, y asegura ser un gobernante que favorece al ser humano antes que al capital, renuncia al ambientalismo de sus primeros tiempos, se enfrenta a las comunidades indígenas, opta por un modelo rabiosamente extractivo, y propone una ley para la explotación del subsuelo que les da grandes ventajas a las empresas mineras.</p>
<p>&nbsp;</p>
<p>No obstante, mientras, por una parte, el gobierno de Correa con esa nueva ley de minería parece invitar a las empresas y capitales extranjeros a invertir en el país, por la otra, es incapaz de llegar a un acuerdo con la compañía minera canadiense Kinross –notable por sus programas sociales dentro de la llamada "responsabilidad social corporativa"--, la cual prefiere abandonar Ecuador en agosto próximo ante la falta de seguridad jurídica que sufren las compañías extranjeras (y nacionales).</p>
<p>&nbsp;</p>
<p>Correa, es muy sensible frente al lenguaje crítico de la prensa, pero una fundación ecuatoriana contó (y luego un parlamentario de oposición reportó) 171 insultos y agravios vertidos contra sus adversarios en sus conferencias de prensa y alocuciones radiales.</p>
<p>&nbsp;</p>
<p>Utiliza palabras impropias de un presidente, como "perro", "ladilla", "ladrón", "cara de estreñido". A la periodista Sandra Ochoa la llamó públicamente "gordita horrorosa", sin la menor consideración por su género o porque la señora estaba haciendo su labor de hacer preguntas incómodas.</p>
<p>&nbsp;</p>
<p>Correa, como muestra de su respeto a la ley asegura que no hay ningún periodista preso, pero su gobierno se ocupa de perseguir hasta la exclusión a profesionales como Emilio Palacio, quien debió exiliarse por temor a ser encarcelado, Carlos Vera, Carlos Jijón, Jorge Ortiz o José Hernández, por sólo mencionar a algunos de los más prestigiosos. No los encarcela, pero trata de someterlos por hambre. Eso no lo hace un político realmente demócrata.</p>
<p>&nbsp;</p>
<p>Ahora mismo, Jaime Mantilla, director del diario Hoy y presidente de la Sociedad Interamericana de Prensa, está bajo un fuerte ataque que incluye presiones económicas y campañas de descrédito conocidas como "asesinatos de la reputación" para obligarlo a desdecirse o a rectificar una información que a sus reporteros les parece correcta.</p>
<p>&nbsp;</p>
<p>Esas campañas, del más claro estilo goebbeliano, sin ningún respeto por la verdad y la decencia, las orquestan desde la Secretaría de Comunicaciones de la Presidencia, verdadero Ministerio de la Verdad. (A mí me acusaron calumniosamente de fomentar un ridículo e inexistente golpe militar por haber presentado cortésmente al expresidente Lucio Gutiérrez en una conferencia dada en Miami, invitado por el Interamerican Institute for Democracy).</p>
<p>&nbsp;</p>
<p>En fin, ¿cómo puede definirse este contradictorio personaje? A mi juicio, es un autócrata emocionalmente inmaduro e intelectualmente incompetente, que no comprende que los gobernantes demócratas realmente exitosos, creadores de riqueza y de estabilidad, se colocan bajo la autoridad de la ley, buscan consensos, practican la cordialidad cívica con sus adversarios, respetan la separación de poderes y no se dedican a perseguir a la prensa.</p>
<p>&nbsp;</p>
<p>Esos buenos estadistas entienden que la función de los periodistas es juzgar la conducta de los políticos y funcionarios, y no al revés. Saben que esa prensa crítica, por incómoda que resulte, y a pesar de los excesos que a veces comete, desempeña el papel fundamental de levantar auditorías, descubrir corruptelas, denunciar negligencias y señalar costosas estupideces que deben costear los trabajadores con sus impuestos. Gracias a ella los gobiernos son mejores.</p>
<p>&nbsp;</p>
<p>Sólo hay un dato que redime a Correa y genera alguna esperanza: ha asegurado que no volverá a aspirar a la presidencia. Ojalá que cumpla su promesa.</p>
<p>&nbsp;</p>
<p>Una colaboración de Carlos Alberto Montaner, miembro de la Junta Honorífica de RELIAL.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/119-si-la-libertad-de-prensa-no-existiese-habría-que-inventarla">
			&laquo; Si la libertad de prensa no existiese, habría que inventarla		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/211-cuba-y-sus-dos-monedas">
			Cuba y sus dos monedas &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/122-el-gobernante-de-las-dos-caras#startOfPageId122">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:39:"El gobernante de las dos caras - Relial";s:11:"description";s:154:"La opinión de Carlos Alberto Montaner &amp;nbsp; Rafael Correa, el presidente de los ecuatorianos, es un personaje contradictorio hasta bordear la esq...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:30:"El gobernante de las dos caras";s:6:"og:url";s:97:"http://www.relial.org/index.php/productos/archivo/opinion/item/122-el-gobernante-de-las-dos-caras";s:8:"og:title";s:39:"El gobernante de las dos caras - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/97aa066dcc42404e7602768333af5659_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/97aa066dcc42404e7602768333af5659_S.jpg";s:14:"og:description";s:158:"La opinión de Carlos Alberto Montaner &amp;amp;nbsp; Rafael Correa, el presidente de los ecuatorianos, es un personaje contradictorio hasta bordear la esq...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:30:"El gobernante de las dos caras";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}