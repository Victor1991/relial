<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9412:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId419"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El legado de Milton Friedman
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/390c9d1de2a80a844d0e01ba21c1192e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/390c9d1de2a80a844d0e01ba21c1192e_XS.jpg" alt="El legado de Milton Friedman" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Óscar Álvarez Araya</p>
<p>&nbsp;</p>
<p>El 31 de julio del 2014 se cumplen los 102 años del nacimiento de Milton Friedman, economista, estadístico e intelectual estadounidense, Padre del Monetarismo Moderno y Premio Nobel de Economía en 1976. De origen judío austro húngaro, había nacido el 31 de julio de 1912 en Brooklyn, Nueva York.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Se le considera el líder de la Escuela monetarista de Chicago que enfatiza la importancia de la cantidad de dinero como el instrumento de la política gubernamental y como el determinante de los ciclos económicos y la inflación.</p>
<p>&nbsp;</p>
<p>Entre sus libros destacaron Capitalismo y libertad (1962), muy famoso y controversial, Una historia monetaria de Estados Unidos (1963), Un marco teórico para el análisis monetarista (1971), Libertad de elección (1980), Dinero y desarrollo económico (1973) y Teoría de los precios (1976).</p>
<p>&nbsp;</p>
<p>Fue profesor e investigador en las universidades de Chicago, Columbia y Stanford. En 1996 junto con su esposa Rose crearon la Fundación Friedman y también entre ambos publicaron en 1998 las Memorias. Se graduó de Master de la Universidad de Chicago y de Ph. D. en la Universidad de Columbia en 1946. Y recibió numerosos doctorados honorarios en universidades de Estados Unidos, Japón, Israel y Guatemala. Fue Presidente de la Asociación Americana de Economía y de la Sociedad Mont Pelerin.</p>
<p>&nbsp;</p>
<p>Para sus múltiples seguidores en diferentes puntos del planeta fue un liberal, un paladín de la libertad individual y gran defensor de la libertad económica y el libre mercado. Tanto en Chile como en China y en realidad en todas partes adónde pronunció conferencias sostuvo que la liberalización económica era indispensable y que tarde o temprano conduciría a la democratización política. Adversó las teorías de Lord John Maynard Keynes y se basó en el liberalismo clásico de Adam Smith. Siempre dijo que: "La economía social de mercado es la única solución".</p>
<p>&nbsp;</p>
<p>Criticó el gran tamaño del sector público en los países occidentales y se opuso a las políticas de fijación de precios y al proteccionismo económico. Favoreció la apertura de la economía afirmando que las fuerzas del mercado libre son más eficientes para la asignación de recursos y para fomentar el crecimiento económico que la intervención del gobierno sobre la economía. Pero aclarando que la buena política económica favorece y fortalece la democracia y la libertad. Y además aseveró que "la existencia de un mercado libre no elimina la necesidad de un gobierno... que es esencial para determinar las reglas del juego y como árbitro para aplicar las reglas que se decidan".</p>
<p>&nbsp;</p>
<p>Friedman fue el pionero en la elaboración de los rankings de libertad económica por país y se opuso al servicio militar obligatorio. En 1988 fue distinguido con la Medalla de la libertad de los Estados Unidos.</p>
<p>&nbsp;</p>
<p>Generalmente se le considera el pensador liberal más influyente del siglo XX y principios del siglo XXI. Sus ideas fueron aplicadas por los gobiernos de Margaret Thatcher en el Reino Unido y de Ronald Reagan en los Estados Unidos pero su influencia fue mucho más allá y gobiernos socialdemócratas, socialistas y hasta comunistas se apropiaron de algunas de sus recomendaciones y políticas públicas. Su ideario llegó a tener peso incluso en China y Rusia. Los que tratan de desacreditarlo se concentran en criticar su visita al General Pinochet y su influencia en las políticas económicas en ese periodo de la historia chilena pero curiosamente nunca le objetaron sus visitas a China y a otros países con sistemas políticos no precisamente democráticos.</p>
<p>&nbsp;</p>
<p>Sus postulados son la base de muchas de las políticas y prácticas del Fondo Monetario Internacional, el Banco Mundial, la Organización Mundial del Comercio y otras entidades internacionales. Para los gobiernos y movimientos del Foro de Sao Paolo, Friedman sería el arquetipo del "neo liberal", algo que se debe criticar siempre en campaña electoral y luego hay que tomar en cuenta cuando se llega al gobierno.</p>
<p>&nbsp;</p>
<p>Friedman puso a Hong Kong como ejemplo de economía de libre mercado: Dijo que: "Si quieren ver capitalismo en acción, vayan a Hong Kong".</p>
<p>&nbsp;</p>
<p>Se pronunció a favor del libre comercio pues consideraba que favorece la entrada de ideas y mercancías y constituye el instrumento más poderoso para crear sociedades libres. Sobre la crisis financiera global del 2008 dijo que: "El mercado no ha fallado, está reaccionando a décadas de intervenciones políticas que lo distorsionaron".</p>
<p>&nbsp;</p>
<p>Para Friedman la inflación es un fenómeno negativo creado por los Bancos Centrales que expanden la masa monetaria.</p>
<p>&nbsp;</p>
<p>Favoreció un gobierno limitado a lo suyo, es decir la libertad, la protección de los derechos de propiedad, la justicia, la seguridad y la paz.</p>
<p>&nbsp;</p>
<p>La formación económica en nuestras universidades sería más balanceada y completa si además de Keynes y Marx se estudiara también el legado de Milton Friedman. Sin caer por cierto en el pensamiento único y en el culto a la Escuela de Chicago o a alguna otra escuela de economía.</p>
<p>&nbsp;</p>
<p>Friedman falleció a los 94 años de un ataque al corazón en San Francisco, Estados Unidos, el 16 de noviembre de 2006. Ave libertas morituri te salutant!</p>
<p>&nbsp;</p>
<p>Autor: Óscar Álvarez</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: ANFE</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes">
			&laquo; Defaults eran los de antes		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte">
			Manuel una noche echó a andar rumbo al norte &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman#startOfPageId419">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:37:"El legado de Milton Friedman - Relial";s:11:"description";s:158:"En la opinión de Óscar Álvarez Araya &amp;nbsp; El 31 de julio del 2014 se cumplen los 102 años del nacimiento de Milton Friedman, economista, estadís...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:28:"El legado de Milton Friedman";s:6:"og:url";s:95:"http://www.relial.org/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman";s:8:"og:title";s:37:"El legado de Milton Friedman - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/390c9d1de2a80a844d0e01ba21c1192e_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/390c9d1de2a80a844d0e01ba21c1192e_S.jpg";s:14:"og:description";s:162:"En la opinión de Óscar Álvarez Araya &amp;amp;nbsp; El 31 de julio del 2014 se cumplen los 102 años del nacimiento de Milton Friedman, economista, estadís...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:28:"El legado de Milton Friedman";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}