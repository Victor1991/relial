<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9799:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId348"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	CEDICE Libertad y el apoyo de la región
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/de2df791682f079f8397226a3ff38bc7_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/de2df791682f079f8397226a3ff38bc7_XS.jpg" alt="CEDICE Libertad y el apoyo de la regi&oacute;n" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">CEDICE demuestra, una vez más, ser el ejemplo de institucionalidad, entrega y compromiso por los derechos, la vida y la propiedad de los ciudadanos. Celebra tres décadas que dejan huella en generaciones y alumbra el sendero que guía hacia la libertad.</span></div>
<div><span style="font-size: 10pt;">&nbsp;</span></div>
<div><span style="font-size: 10pt;">Por Silvia Mercado*</span></div>
<div>&nbsp;</div>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<div>&nbsp;</div>
<div>&nbsp;</div>
<div><span style="font-size: 12pt;">Qué mejor homenaje a la libertad que luchar por ella. El Centro de Divulgación del Conocimiento Económico para la Libertad (CEDICE) cumplió 30 años de intenso trabajo por las libertades individuales, la iniciativa privada, gobierno limitado y búsqueda de la paz.</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">Una celebración particularmente emotiva y emblemática por las difíciles circunstancias que atraviesa Venezuela, país que hoy por hoy está en el centro de la atención mundial debido a la violencia, acoso y maltrato que sufre su sociedad civil, tanto económica como moralmente.</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">Sin embargo, pese al clima hostil notoriamente propiciado por el gobierno, distintas organizaciones de América Latina decidieron acompañar a CEDICE Libertad en este encuentro que pasará a la historia.</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">Poner el hombro y escoltar a Venezuela fue la consigna que convocó a personalidades del mundo intelectual, expertos, académicos, jóvenes y ciudadanos defensores de la democracia a participar de este evento aniversario cuyo nombre sintetiza la visión por la que hoy luchan los venezolanos: El Futuro es la Libertad.</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">CEDICE demuestra, una vez más, ser el ejemplo de institucionalidad, entrega y compromiso por los derechos, la vida y la propiedad de los ciudadanos. Celebra tres décadas que dejan huella en generaciones y alumbra el sendero que guía hacia la libertad.</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">La Red Liberal de América Latina (RELIAL), con la representación de sus distintas organizaciones miembros, y la Fundación Friedrich Naumann para Libertad fueron parte de este acontecimiento. En distintos paneles, think tanks de RELIAL abordaron ejes estratégicos para el desarrollo y progreso de la región como el tema energético, la integración regional y los modelos económicos que distinguen a los países que optaron por la apertura de sus mercados versus los que se adhirieron a recetas populistas, intervencionistas y de mayor presencia del Estado en la vida de los ciudadanos.&nbsp;</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div><span style="font-size: 12pt;">Este encuentro fue una valiosa y oportuna ocasión para ponderar el trabajo de CEDICE Libertad, así como el esfuerzo de todas las personas que llevan adelante esta institución.</span></div>
<div><span style="font-size: 12pt;">&nbsp;</span></div>
<div>&nbsp;</div>
<div>
<table style="background-color: #f2f2f2; border: 0px solid #ec0623;" border="0">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
</td>
<td>
<p><span style="font-size: 12pt;"><br /></span></p>
<p><span style="font-size: 12pt;">&nbsp; &nbsp;<strong>Venezuela en el foco de la Internacional Liberal</strong></span></p>
</td>
<td>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p><span style="font-size: 12pt;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
</td>
<td>
<p><span style="font-size: 12pt;">&nbsp;</span></p>
<p><span style="font-size: 12pt;">Celebrando su 58 Congreso, en Rotterdam, Países Bajos, la Internacional Liberal (IL) también llamó la atención alrededor del difícil momento que toca a Venezuela. &nbsp;</span></p>
<p><span style="font-size: 12pt;"><br /></span></p>
<p><span style="font-size: 12pt;">Con este propósito la IL –Confederación de Partidos Políticos Liberales del mundo- convocó al Presidente de RELIAL, Doctor Ricardo López Murphy, y voceros venezolanos -representados por el Doctor Gustavo Villasmil y el Diputado Eduardo Gómez Sigala-, a exponer la situación en detalle: vulneración de los derechos humanos y enajenación del Estado de Derecho en Venezuela, ante parlamentarios y políticos de todos los continentes.</span></p>
<p><span style="font-size: 12pt;">&nbsp;</span></p>
<p><span style="font-size: 12pt;">Asimismo, la Fundación Friedrich Naumann para la Libertad, a través de su Oficina para Asuntos Internacionales en Bruselas, citó a diplomáticos de alto nivel a una reunión abierta denominada “Venezuela: ¿oportunidad para el cambio?” con el objetivo de escuchar las demandas venezolanas y poner en altavoz alternativas y propuestas que den lugar a la salida pacífica de la actual crisis que atraviesa este &nbsp;país latinoamericano.</span></p>
<p><span style="font-size: 12pt;">&nbsp;</span></p>
</td>
<td>
<p><span size="3" style="font-size: medium;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p>&nbsp;</p>
</td>
<td>
<p>&nbsp;</p>
</td>
<td></td>
</tr>
</tbody>
</table>
</div>
<div><strong><span style="color: #000000;"><br /></span></strong></div>
<div><em><span style="color: #000000;"><br /></span></em></div>
<div><span style="font-size: 10pt;"><em><span style="color: #000000;">*Silvia Mercado es Coordinadora del Proyecto RELIAL para la Fundación Naumann para la Libertad</span></em></span></div>
<div>&nbsp;</div>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/342-unidos-para-un-éxito-mayor-con-la-“alianza-para-centro-américa”">
			&laquo; Unidos para un éxito mayor con la “Alianza para Centro América”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/356-fortuño-presenta-plan-desarrollo-caribe">
			Fortuño presenta plan desarrollo Caribe &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/348-cedice-libertad-y-el-apoyo-de-la-región#startOfPageId348">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:49:"CEDICE Libertad y el apoyo de la región - Relial";s:11:"description";s:154:"&amp;nbsp; CEDICE demuestra, una vez más, ser el ejemplo de institucionalidad, entrega y compromiso por los derechos, la vida y la propiedad de los ci...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:40:"CEDICE Libertad y el apoyo de la región";s:6:"og:url";s:106:"http://relial.org/index.php/productos/archivo/actualidad/item/348-cedice-libertad-y-el-apoyo-de-la-región";s:8:"og:title";s:49:"CEDICE Libertad y el apoyo de la región - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/de2df791682f079f8397226a3ff38bc7_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/de2df791682f079f8397226a3ff38bc7_S.jpg";s:14:"og:description";s:158:"&amp;amp;nbsp; CEDICE demuestra, una vez más, ser el ejemplo de institucionalidad, entrega y compromiso por los derechos, la vida y la propiedad de los ci...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:40:"CEDICE Libertad y el apoyo de la región";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}