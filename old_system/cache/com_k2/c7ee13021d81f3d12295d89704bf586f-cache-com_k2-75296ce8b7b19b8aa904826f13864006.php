<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8074:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId237"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL en atención y solidaridad con Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/84ac056b57dd032fcf18a346d4a81feb_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/84ac056b57dd032fcf18a346d4a81feb_XS.jpg" alt="RELIAL en atenci&oacute;n y solidaridad con Venezuela" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>&nbsp;</p>
<p>RELIAL en atención y solidaridad con Venezuela</p>
<p><strong><span style="font-size: 11px;">Comunidado de VENTE Venezuela/&nbsp;</span><em style="font-size: 11px;">Movimiento político de ciudadanos libres</em></strong></p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><span style="font-size: 11px;">Estimados amigos:</span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p>Nuevamente requerimos de su consecuente atención, para informarles sobre un grave hecho de persecución política contra la dirigencia opositora ocurrido la madrugada de este sábado 23 de noviembre en vísperas de la Protesta Nacional convocada por todas las fuerzas políticas.</p>
<p>&nbsp;</p>
<p>El régimen, actuando de manera ilegal, arbitraria, ilegítima y violatoria de los tratados y convenios internacionales de protección de derechos humanos y las leyes internas, detuvo de Alejandro Silva, Coordinador de Giras de Henrique Capriles Radonski, dirigente de Primero Justicia, quien se encuentra a la orden de la Dirección de Inteligencia Militar, sin que haya sido incoada causa alguna en su contra, ni presentado ante un juez y mucho menos habérsele permitido el derecho a la defensa.</p>
<p>&nbsp;</p>
<p>Anexamos el mensaje que Henrique Capriles Radonski envió a la comunidad internacional alertando de los graves hechos, que forman parte del esquema de destrucción de la Democracia emprendido por el régimen de Nicolás Maduro y cuya presión mas la solidaridad, comprensión y apoyo que a lo largo de estos años han tenido con la Venezuela democrática y que desde el exterior se manifestó hoy, Alejandro Silva, hace poco minutos fue liberado.</p>
<p>&nbsp;</p>
<p>Hace unas semanas, les pedimos mantenerse alertas ante cualquier acción contra Henrique Capriles Radonski, María Corina Machado, y Leopoldo López; hoy les reiteramos que la amenaza que se cierne sobre nuestros líderes, su entorno y los ciudadanos venezolanos adversos al régimen, es real y veremos acciones como las que contra Alejandro Silva se ejecutaron como parte de las nuevas medidas de la Ley Habilitante, lo que se debe ser conocido y denunciado con fuerza más allá de nuestras fronteras.</p>
<p>&nbsp;</p>
<p>Seguros estamos de contar con su apoyo, solidaridad y compromiso.</p>
<p>&nbsp;</p>
<p style="text-align: center;"><strong>Carta de Henrique Capriles a la Comunidad Internacional</strong></p>
<p>&nbsp;</p>
<p>Caracas, 23 de noviembre de 2013</p>
<p>Me permito dirigirme a Usted para denunciar formalmente la detención arbitraria e ilegal del Coordinador Nacional de Giras del Comando Simón Bolívar y militante del partido Primero Justicia, Alejandro Silva Rodríguez.</p>
<p>&nbsp;</p>
<p>En horas de la madrugada de este sábado 23 de noviembre de 2013, doce funcionarios armados, vestidos de civil, sin identificación ni orden de aprehensión irrumpieron en la habitación del Hotel Eurobuilding Caracas, donde se hospedaba Alejandro Silva. Durante el procedimiento se incumplieron los mínimos protocolos de protección de derechos humanos y fue secuestrado el jefe de seguridad del Hotel Eurobuilding Caracas.</p>
<p>&nbsp;</p>
<p>En vista de lo anterior le manifestamos nuestra profunda preocupación por los hechos de violencia política verbal y física que de manera sistemática es promovida por Nicolás Maduro y su gobierno.</p>
<p>&nbsp;</p>
<p>Resulta alarmante la violencia política que nuestro país sufre a diario, pero sin duda no puede sino sobresaltarnos que las hostilidades políticas se promuevan y aplaudan en la sede de los poderes públicos nacionales.</p>
<p>&nbsp;</p>
<p>En virtud de lo anterior, apreciamos que esta denuncia sea llevada a la consideración de su gobierno a fin de que se conozcan estos hechos y se activen los mecanismos necesarios para que cese este tipo de agresiones impropias de la naturaleza democrática.</p>
<p>&nbsp;</p>
<p>Nos despedimos de usted agradeciéndole la atención que pudiera prestarle a la situación planteada y ratificándole nuestro profundo compromiso con los valores democráticos.</p>
<p>&nbsp;</p>
<p style="text-align: center;"><strong>HENRIQUE CAPRILES RADONSKI</strong></p>
<p style="text-align: center;"><strong>Líder de la Unidad Democrática</strong></p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos">
			&laquo; Ya no nos podemos conformar con menos		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/238-metiéndose-en-honduras">
			Metiéndose en Honduras &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/237-relial-en-atención-y-solidaridad-con-venezuela#startOfPageId237">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:56:"RELIAL en atención y solidaridad con Venezuela - Relial";s:11:"description";s:154:"&amp;nbsp; RELIAL en atención y solidaridad con Venezuela Comunidado de VENTE Venezuela/&amp;nbsp;Movimiento político de ciudadanos libres &amp;nbsp;...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:47:"RELIAL en atención y solidaridad con Venezuela";s:6:"og:url";s:117:"http://www.relial.org/index.php/productos/archivo/actualidad/item/237-relial-en-atención-y-solidaridad-con-venezuela";s:8:"og:title";s:56:"RELIAL en atención y solidaridad con Venezuela - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/84ac056b57dd032fcf18a346d4a81feb_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/84ac056b57dd032fcf18a346d4a81feb_S.jpg";s:14:"og:description";s:166:"&amp;amp;nbsp; RELIAL en atención y solidaridad con Venezuela Comunidado de VENTE Venezuela/&amp;amp;nbsp;Movimiento político de ciudadanos libres &amp;amp;nbsp;...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:47:"RELIAL en atención y solidaridad con Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}