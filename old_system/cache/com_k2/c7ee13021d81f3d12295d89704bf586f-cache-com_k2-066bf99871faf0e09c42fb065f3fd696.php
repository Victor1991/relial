<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8330:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId297"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Por qué perdió Rafael Correa
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/157c6c6cd616f458d56a6caf427711f8_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/157c6c6cd616f458d56a6caf427711f8_XS.jpg" alt="Por qu&eacute; perdi&oacute; Rafael Correa" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Volvió a suceder. La derrota del presidente ecuatoriano Rafael Correa en las elecciones municipales del 23 de febrero no es un caso aislado. Es posible que el Socialismo del Siglo XXI, sus vecinos ideológicos, y el circuito del ALBA estén de capa caída.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Hay una cierta fatiga con el lenguaje tontiloco del chavismo. El péndulo se mueve en la otra dirección. El espectáculo venezolano, con los sangrientos atropellos de Maduro contra estudiantes desarmados, es demasiado repugnante.</p>
<p>&nbsp;</p>
<p>Antes le sucedió a Cristina Fernández en Argentina, a Manuel Zelaya en Honduras (quien sacrificó a su mujer Xiomara Castro en las elecciones), a José María Villalta en Costa Rica, a López Obrador en México y a Aníbal Carrillo en Paraguay. Ese polvoriento discurso estatista, hecho de quejas y confrontaciones, ya no suele convencer, aunque todavía conserva su atractivo en algunos parajes indiferentes ante la experiencia.</p>
<p>&nbsp;</p>
<p>Son síntomas típicos de las sociedades con tendencias autodestructivas que practican alegremente la extraña costumbre de hacerse el harakiri. Es muy probable, por ejemplo, que una variante extrema del chavismo triunfe en El Salvador, donde el comunista Salvador Sánchez Cerén, ex guerrillero de línea dura, encabeza las encuestas para los comicios del próximo 9 de marzo, lo que augura una época de conflictos, turbulencias y retroceso económico en el país más pequeño de América Latina.</p>
<p>&nbsp;</p>
<p>En todo caso, Correa, el gobernante que más tiempo ha ocupado la casa presidencial de manera continuada en la historia de Ecuador, y el que más ha hecho crecer el gasto público aprovechándose de la bonanza petrolera, perdió 9 de las 10 ciudades más pobladas del país y la mayor parte de las prefecturas, como allá se les llama a las provincias. Entre las ciudades están Quito, la capital; Guayaquil, el corazón económico; y Cuenca, la tercera gran urbe del país. Eso es un mazazo electoral.</p>
<p>&nbsp;</p>
<p>¿Por qué Correa perdió esas elecciones, al margen de la tendencia latinoamericana actual a desplazar al chavismo de las casas de gobierno? Casi todo el mundo le reconoce que ha hecho infraestructuras importantes, que se ha esforzado por mejorar la educación, y que ha tenido el coraje de enfrentarse al sindicato de maestros, a los ambientalistas y a los indigenistas cuando le ha tocado defender el interés general de los ecuatorianos. Eso no lo discuten.</p>
<p>&nbsp;</p>
<p>El problema es su carácter autoritario, su incapacidad para encajar las críticas, su trato áspero con quienes le contradicen, incluida una joven periodista que le hizo una pregunta incómoda en una rueda de prensa y la humilló públicamente llamándola "gordita horrorosa". ¿Qué manera es ésa de tratar a una dama?</p>
<p>&nbsp;</p>
<p>Correa debe tener unos niveles estratosféricos de cortisol, la hormona del berrinche, del mal genio. (¿Por qué no le examinan las suprarrenales a ese hombre? A lo mejor es una cuestión sencilla de botica). Como Salvador Dalí, que todos los días se levantaba muy feliz de ser Salvador Dalí, Rafael Correa amanece tremendamente satisfecho de ser quién es, y no puede admitir que un caricaturista le gaste una broma o un articulista, con razón o sin ella, lo critique.</p>
<p>&nbsp;</p>
<p>En lugar de comportarse como un servidor público, seleccionado para cumplir y hacer cumplir las leyes, como corresponde a un ordenamiento republicano, Correa se jacta públicamente de desobedecer las reglas del Consejo Electoral y del Parlamento, porque le parecen "obsoletas". ¿Por qué el ciudadano de a pie tiene que someterse a las leyes y el presidente está exento de esa obligación?</p>
<p>&nbsp;</p>
<p>Ya Correa explicó que, como había sido elegido Presidente, era, al mismo tiempo, el jefe del Poder Judicial y del Legislativo, de toda la nación. O sea, el déspota ilustrado, dueño de las instituciones, el tirano benévolo de la razón y el orden, que imponía su buen juicio en beneficio del pueblo, como aquellos monarcas del antiguo régimen felizmente desplazados por la democracia liberal tras las revoluciones del siglo XIX.</p>
<p>&nbsp;</p>
<p>Correa terminará su mandato en el 2017. Si no rectifica acabará siendo tremendamente impopular. Ya se le ve la oreja al lobo. Sería una pena.</p>
<p>&nbsp;</p>
<p>Fuente: Texto proporcionado amablemente por Carlos Alberto Montaner</p>
<p>Foto: Blog de Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/283-venezuela-llega-a-su-hora-decisiva">
			&laquo; Venezuela llega a su hora decisiva		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/302-el-legado-de-hugo-chávez">
			El legado de Hugo Chávez &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/297-por-qué-perdió-rafael-correa#startOfPageId297">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:39:"Por qué perdió Rafael Correa - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Volvió a suceder. La derrota del presidente ecuatoriano Rafael Correa en las elecciones municipale...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:30:"Por qué perdió Rafael Correa";s:6:"og:url";s:97:"http://www.relial.org/index.php/productos/archivo/opinion/item/297-por-qué-perdió-rafael-correa";s:8:"og:title";s:39:"Por qué perdió Rafael Correa - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/157c6c6cd616f458d56a6caf427711f8_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/157c6c6cd616f458d56a6caf427711f8_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Volvió a suceder. La derrota del presidente ecuatoriano Rafael Correa en las elecciones municipale...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:30:"Por qué perdió Rafael Correa";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}