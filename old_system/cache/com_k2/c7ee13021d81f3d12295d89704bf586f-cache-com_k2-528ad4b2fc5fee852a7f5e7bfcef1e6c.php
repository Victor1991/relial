<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10133:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId259"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	CELAC: Una infamia para la democracia en la región
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/a07bb170c4a36161aa1f8f4859c19794_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/a07bb170c4a36161aa1f8f4859c19794_XS.jpg" alt="CELAC: Una infamia para la democracia en la regi&oacute;n" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La opinión de Yesenia Álvarez</p>
<p>&nbsp;</p>
<p>Los inicios del 2014 son sombríos para la democracia en la región. Que Cuba, la dictadura más longeva del continente, sea la anfitriona del conjunto de treintaitrés países que conforman la Comunidad de Estados Americanos y Caribeños (CELAC), nos alerta del poco o nulo compromiso que nuestros gobiernos tienen con los principios democráticos, el Estado de derecho y las libertades.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>El hecho no parece preocupar o motivar ninguna protesta de indignados y por el contrario se guarda silencio. La hipocresía internacional viene desde enero del 2013 en que Raúl Castro fue aplaudido e investido como presidente de la CELAC, por gobernantes que se jactan de ser demócratas. Claro está que este organismo es una contradicción y que ha trastocado lo qué es la democracia representativa, ¿cómo es posible que Cuba siendo una dictadura tenga la autoridad moral para presidir pro témpore una organización que supuestamente ha sido creada para promover la promoción, defensa y protección del Estado de Derecho, del orden democrático, de los Derechos Humanos y las libertades fundamentales? ¿Cómo es posible que Cuba sea la anfitriona de una cita que evaluará los avances y desafíos de la región sobre el proceso de integración de Latinoamérica y el Caribe, y el seguimiento a la Declaración de Caracas (2011), y de Santiago (2012), ambas inspiradas en la pretensión común de construir sociedades justas, democráticas y libres?</p>
<p>&nbsp;</p>
<p>Cuba viola constantemente todos los principios democráticos, los Castro se han perpetuado en el poder por más de cincuenta años con un sistema de partido y pensamiento único, sin elecciones libres, sin libertad de expresión, con una férrea persecución política a los opositores, despreciando impunemente todos los valores de la libertad. Y junto a la instalación de un exitoso aparato represivo ha devastado los proyectos de vida de los cubanos empobreciéndolos moral y económicamente.</p>
<p>&nbsp;</p>
<p>Como sabemos, la CELAC, es un organismo continental que se constituyó en el 2011 en Venezuela excluyendo a Canadá y Estados Unidos intencionalmente, y fue promovida por Hugo Chávez como alternativa a la Organización de Estados Americanos (OEA), y hasta el momento ningún gobierno latinoamericano ha alzado su voz de protesta o cuestionado la incorporación y permanencia de una dictadura como la cubana en la CELAC.</p>
<p>&nbsp;</p>
<p>Y el cinismo de CELAC no tiene límites, en su misma Declaración Especial sobre la Defensa de la Democracia de diciembre del 2011 acordaron una cláusula de compromiso que los obliga a adoptar acciones concretas concertadas de cooperación cuando existe una amenaza de ruptura o alteración del orden democrático. En la misma declaración, con grandilocuencia expresan que buscarán el pronunciamiento de la comunidad latinoamericana y caribeña para la defensa y preservación de la institucionalidad democrática, y que contribuirán en la restitución del proceso político institucional democrático y del Estado de Derecho a la brevedad posible.</p>
<p>&nbsp;</p>
<p>Una burla perversa de este esperpento de organismo continental, pues pone a una dictadura a custodiar que no se altere el orden democrático de la región. De ser CELAC una organización seria y consecuente con sus principios, el primer país en el que debe tomar medidas concretas y concertadas para restituir el proceso político institucional democrático y el Estado de Derecho a la brevedad posible, debe ser en la misma Cuba. Es el país que más lo necesita. Según su Declaración Especial sobre la Defensa de la Democracia los treintaitrés gobernantes deben ya estar buscando el pronunciamiento de la comunidad latinoamericana y caribeña para sancionar al gobierno de Cuba y pedir que haya una restitución del orden democrático.</p>
<p>&nbsp;</p>
<p>Sin embargo, la infamia promovida por este organismo continúa, este 27 y 28 de enero los miembros de CELAC se reúnen en La Habana, y nadie parece tomar la iniciativa en denunciar a Cuba por las flagrantes violaciones a los derechos humanos o exigirle que se sujete a los compromisos democráticos que supuestamente infunden a la misma conformación de la CELAC. Ya se iniciaron las reuniones y otro hecho desconcertante para los demócratas es que además se ha confirmado la asistencia del Secretario General de la ONU, Ban Ki-moon, y el de la Organización de Estados Americanos (OEA), José Miguel Insulza. Este desconcierto puede convertirse en una situación alentadora si tan destacados representantes internacionales tienen a bien escuchar o reunirse con la disidencia.</p>
<p>&nbsp;</p>
<p>Mientras tanto en las calles de Cuba, el gobierno sigue haciendo lo que mejor sabe hacer: reprimir. Ya se han reportado y denunciado varios arrestos, detenciones temporales, amenazas y paraderos desconocidos de opositores cubanos, en las últimas horas. La dictadura lo hace para evitar reuniones paralelas, pues en el contexto de la Cumbre de la CELAC reunida en La Habana, y en vista del atropello que significa para la democracia, la disidencia está organizando pacíficamente y al mismo tiempo un Foro Democrático en Relaciones Internacionales y Derechos Humanos denominado "La declaración especial de defensa de la democracia de la CELAC y su incompatibilidad con el sistema de partido único de Cuba". El título del foro habla por sí mismo y la valentía de los organizadores es admirable.</p>
<p>&nbsp;</p>
<p>En la otra vereda, lamentable y vergonzosamente, los gobiernos latinoamericanos empiezan el 2014 reverenciando a una dictadura y dándole la espalda a la democracia, vaciándola de contenido, haciendo de ella cualquier cosa, transgrediendo la dignidad de un pueblo víctima de unos tiranos. Es hoy, sin duda, uno de esos momentos en que se hace necesariamente impostergable la expresión de solidaridad de los ciudadanos demócratas de la región frente a la venia y complicidad de nuestros gobernantes con una tiranía.</p>
<p>&nbsp;</p>
<p>&nbsp;Fuente: colaboración de la autora</p>
<p><strong>*Yesenia Álvarez es Directora del Instituto Político para la Libertad<a href="http://www.iplperu.com/"> IPL&nbsp;</a></strong></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/257-la-celac-contra-la-carta-democrática-interamericana">
			&laquo; La CELAC contra la Carta Democrática Interamericana		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/260-liberales-y-liberales">
			Liberales y liberales &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/259-celac-una-infamia-para-la-democracia-en-la-región#startOfPageId259">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:60:"CELAC: Una infamia para la democracia en la región - Relial";s:11:"description";s:158:"La opinión de Yesenia Álvarez &amp;nbsp; Los inicios del 2014 son sombríos para la democracia en la región. Que Cuba, la dictadura más longeva del con...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:51:"CELAC: Una infamia para la democracia en la región";s:6:"og:url";s:116:"http://relial.org/index.php/productos/archivo/actualidad/item/259-celac-una-infamia-para-la-democracia-en-la-región";s:8:"og:title";s:60:"CELAC: Una infamia para la democracia en la región - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/a07bb170c4a36161aa1f8f4859c19794_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/a07bb170c4a36161aa1f8f4859c19794_S.jpg";s:14:"og:description";s:162:"La opinión de Yesenia Álvarez &amp;amp;nbsp; Los inicios del 2014 son sombríos para la democracia en la región. Que Cuba, la dictadura más longeva del con...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:51:"CELAC: Una infamia para la democracia en la región";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}