<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7501:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId234"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Otro desvarío del gobierno venezolano
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/e7b279be6a862d254f0e7cc4dde2874e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/e7b279be6a862d254f0e7cc4dde2874e_XS.jpg" alt="Otro desvar&iacute;o del gobierno venezolano" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Las autoridades venezolanas interpretan como felicidad cuando el estado regala viviendas, electrodomésticos, efectivo y otra serie de cosas. Tan sólo faltaba plasmarlo en un decreto creando ese viceministerio. Sin embargo, me parece un irrespeto enorme que aquel logro tan inusitado y descollante, se circunscriba a un simple viceministerio y no a un ministerio. No se merece esa bajada de piso. Es de esperar que ahora, con este reconocimiento del estado de felicidad que se vive en Venezuela bajo el gobierno chavista y el de su sucesor Maduro, abundarán el papel higiénico, la leche, el azúcar, el café, el aceite y la margarina, que hoy los "mentirosos" opositores dicen que escasea en ese país y que es, por tanto, causa de infelicidad. Les aseguro que esa inflación ignominiosa del 50% anual, que también "inventaron" esos mismos mentirosos, se quedará muy corta.</p>
<p>&nbsp;</p>
<p>Los gobernantes venezolanos creen que el estado es el que da la felicidad a los ciudadanos. Llamados al "bien común" o al "bien público", son las razones para actuar "por el bien del gobernado". Pero existen tantas ideas diferentes acerca de lo que es la felicidad, como seres humanos hay. Lo que importa es ver cómo una sociedad logra que esos individuos puedan buscar su felicidad. Es errada la creencia de esos gobernantes venezolanos, al igual que la de otros tantos similares en la historia humana, que la sociedad de alguna forma es un ente capaz de experimentar la felicidad. Los individuos son los únicos que pueden definir su felicidad, la cual ellos buscan. Si lo que prima en una sociedad es que esa búsqueda de felicidad la defina un gobierno o un gobernante y no por cada uno de esos individuos, se estará en presencia de un gobierno despótico, absoluto. En una nación de siervos el rey tirano decidía cuál era la felicidad de sus súbditos. Algo parecido sucede hoy en Venezuela.</p>
<p>&nbsp;</p>
<p>En una sociedad lo esencial es que se posibilite que los individuos puedan buscar la felicidad, según su propia concepción, de una manera simultánea y sin que haya conflicto. Se logra tan sólo cuando los individuos poseen derechos en un orden social. En los hechos y en la moral, el bien público se da cuando existe protección y preservación de la propiedad –que incluye la vida, la libertad y todas las cosas que una persona posee. El bien común lo promueve un gobierno si protege los derechos de los individuos. Sólo así es posible que en un orden social los individuos puedan buscar su propia felicidad, mediante el intercambio voluntario de toda índole. En él todos ganan, pues, de no ser así, no participarían de él. Por eso se requiere que la gente pueda actuar en libertad. Que lo haga según sus propios juicios, sin que haya compulsión o fuerza por la cual se les imponen sus decisiones.</p>
<p>&nbsp;</p>
<p>El gobierno venezolano no promueve la protección y preservación de los derechos de los individuos, sino que define la felicidad y que, como lo atestigua el nombre de su viceministerio "de la felicidad suprema", no es más que una sarta de regalías y transferencias estatales. Ese gobierno recuerda a un soberano omnímodo quien goza de derechos absolutos e inalienables y al que no le interesa la vigencia de los derechos de los individuos. La arrogancia de ese gobierno -de creerse el supremo dador de la felicidad de los ciudadanos- es la novedosa representación de un viejo absolutismo, que ahora corroe la política latinoamericana.</p>
<p>&nbsp;</p>
<p>Aunque fingen o lo ocultan, hay seguidores domésticos de las políticas disparatadas de los gobernantes de la Venezuela de hoy. Si pretenden electoralmente llegar al poder, tienen la obligación moral de ser honestos con la ciudadanía acerca del rumbo que quieren imponerle al país. Difícilmente las personas podremos buscar la felicidad o el bienestar propio, si de hecho no se garantizan nuestros derechos. No es aceptable que nos sea impuesto un criterio de felicidad para todos y cada uno de nosotros, cuando en última instancia eso no es más que la creencia coercitiva particular, de algún pretendiente iluminado de turno y quien aspira a gobernarnos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>fuente: ANFE</p>
<p>ator: Jorge Corrales Quesada</p>
<p>foto: ANFE</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/234-otro-desvarío-del-gobierno-venezolano#startOfPageId234">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"Otro desvarío del gobierno venezolano - Relial";s:11:"description";s:154:"Las autoridades venezolanas interpretan como felicidad cuando el estado regala viviendas, electrodomésticos, efectivo y otra serie de cosas. Tan sólo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"Otro desvarío del gobierno venezolano";s:6:"og:url";s:90:"http://www.relial.org/index.php/biblioteca/item/234-otro-desvarío-del-gobierno-venezolano";s:8:"og:title";s:47:"Otro desvarío del gobierno venezolano - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/e7b279be6a862d254f0e7cc4dde2874e_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/e7b279be6a862d254f0e7cc4dde2874e_S.jpg";s:14:"og:description";s:154:"Las autoridades venezolanas interpretan como felicidad cuando el estado regala viviendas, electrodomésticos, efectivo y otra serie de cosas. Tan sólo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:33:"Ensayos y reflexiones académicas";s:4:"link";s:76:"/index.php/biblioteca/itemlist/category/22-ensayos-y-reflexiones-académicas";}i:2;O:8:"stdClass":2:{s:4:"name";s:38:"Otro desvarío del gobierno venezolano";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}