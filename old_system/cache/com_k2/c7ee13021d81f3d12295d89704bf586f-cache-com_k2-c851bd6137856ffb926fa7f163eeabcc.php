<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:18746:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId398"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Tiempo de canallas, fusilamiento al amanecer
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/bab2d5d59ac444db8043a4f3e32c9f0e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/bab2d5d59ac444db8043a4f3e32c9f0e_XS.jpg" alt="Tiempo de canallas, fusilamiento al amanecer" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>(El Nuevo Herald) Publicamos el primer capítulo de 'Tiempo de canallas', un 'thriller' político que es la última novela de Carlos Alberto Montaner, la cual estará en el mercado alrededor del 24 de junio. La obra, aunque basada en hechos ocurridos en la primera mitad del siglo XX, es de ficción. La publica el sello mexicano SUMA, de Santillana-Prisa, perteneciente a Random House.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>"Esta vez sí me fusilarán al amanecer", pensó Rafael Mallo mientras lo trasladaban, como cada mes, al despacho-celda de los interrogatorios. No sintió miedo. En el trayecto fijó la vista en el calendario polvoriento que colgaba de la pared. Lo hacía siempre. Bajo la foto aérea de Barcelona comparecía la fecha: 21 de noviembre de 1947. Llevaba siete años encerrado en esa prisión. El terror se le había agotado de tanto experimentarlo. Ya ni siquiera sudaba. Le vino a la memoria, eso sí, incontrolablemente, el estribillo de una canción muchas veces coreada entre batallas durante la Guerra Civil española: Anda jaleo, jaleo/ ya se acabó el alboroto/ y vamos al tiroteo. ¿Irían al tiroteo? ¿Moriría de inmediato tras la descarga del pelotón de fusilamiento o tendría que esperar a que el tiro de gracia le hiciera estallar el cerebro?</p>
<p>&nbsp;</p>
<p>Es verdad que, desde hacía años, no lo torturaban ni lo insultaban. Lo amenazaban, sí, abundantemente, aunque cada vez menos, pero no lo golpeaban ni lo trataban de intimidar, aunque habitualmente le recordaban que cualquier día podían ejecutar la sentencia de muerte a la que había sido condenado, y a la que Francisco Franco, con total indiferencia, le había puesto el fatal "Enterado". Ni siquiera era necesario solicitar una renovación del documento. Técnicamente, estaba muerto y al pie de la sepultura desde hacía siete años.</p>
<p>&nbsp;</p>
<p>Había llegado al castillo de Montjuich en Barcelona a fines de 1940, después del líder nacionalista catalán Lluís Companys, su inesperado compañero de infortunios. A los dos, que no se conocían, los habían apresado las tropas alemanas de ocupación situadas en Francia y los habían entregado a la policía española en la ciudad fronteriza de Irún. Con poco tiempo de diferencia, ambos habían pasado por los tenebrosos calabozos de la Dirección General de Seguridad en la madrileña Puerta del Sol, donde los habían torturado y vejado, y luego los remitieron en tren, esposados y vigilados, a Barcelona, al viejo cuartel-fortaleza de Montjuich, que tantos crímenes y tanta gloria había conocido a lo largo de su centenaria historia. Allí serían juzgados y fusilados.</p>
<p>&nbsp;</p>
<p>Cuando mataron a Companys y a otros cientos de prisioneros, el hecho le generó una ambigua sensación de felicidad por mantenerse vivo y de vergüenza por no seguir el destino de tantos republicanos de izquierda. ¿Por qué no lo habían fusilado? Nunca se lo aclararon. Lo condenaron a muerte, pero no ejecutaron la sentencia. Probablemente aplazaron el cumplimiento de la orden judicial por una conjunción de razones: su pasaporte cubano, su condición de colaborador del POUM durante la guerra —un grupo político acusado de trotskista, muy perseguido y diezmado por los soviéticos, lo que convertía a sus miembros, lateralmente, en "enemigos del enemigo"—, el hecho de que en la década de los treinta había sido un notable poeta surrealista rodeado de amigos prestigiosos en el ámbito internacional (todavía reverberaba el asesinato de García Lorca), el fin de la Segunda Guerra Mundial dos años antes, en 1945, y, por qué no decirlo, la curiosidad.</p>
<p>&nbsp;</p>
<p>En efecto, la curiosidad del importante comisario gallego Alberto Casteleiro. Casteleiro era un tipo gordo, calvo, sonriente, irónico, afectado por una paranoia crónica y por una vistosa "cojera bamboleante" (así la calificaba él mismo), producto de un balazo que recibió en la pierna izquierda durante la batalla del Ebro; la herida se saldó y soldó con cinco centímetros menos medidos desde la cadera al tobillo. Así, cojo y astuto, al comisario lo habían destacado en Montjuich para interrogar a los prisioneros condenados a muerte con el objeto de exprimirles hasta la última gota de información, y siempre había pensado que Rafael Mallo era una persona demasiado singular, que probablemente ocultaba algo. Esa arbitraria sospecha, cuyo origen no era capaz de precisar, había contribuido a salvarle la vida a Mallo.</p>
<p>&nbsp;</p>
<p>Los interrogatorios estaban basados en una rutina burocrática que podía convertirse en un pesado ejercicio de memoria a medio camino entre la literatura y el sadismo. Todos los meses, el detenido debía entregar una minuciosa autobiografía con los detalles de su vida, las personas a las que había conocido, los estudios que había hecho, las mujeres a las que había amado, los pensamientos que entonces albergaba ("el contexto ideológico y emocional es clave", solía decirle el comisario), los episodios principales de su existencia y los más nimios. Nada debía dejarse fuera.</p>
<p>&nbsp;</p>
<p>El compromiso era que escribiera todos los días (menos los domingos, día de oración decretado por Casteleiro, hombre muy religioso). El texto, escrito a mano y con buena letra (le proporcionaban pluma, tintero y secante), debía entregarse en unos amarillentos cuadernos rayados aportados por el comisario, de manera que, a fines de cada mes, el relato alcanzaba cierto volumen. La información, para que fluyera, debía estar ordenada cronológicamente y dividida en epígrafes porque todo, decía Casteleiro, podía ser importante (y porque el comisario era un neurótico que necesitaba conocer los pormenores de la vida de sus prisioneros). Las libretas se archivaban cuidadosamente en una de las bodegas del castillo junto a otros miles de expedientes y documentos.</p>
<p>&nbsp;</p>
<p>La conversación mensual entre el preso y el policía seguía siempre el mismo patrón. Casteleiro le anunciaba que probablemente esa tarde sería la última en que se encontrarían, porque tal vez decidiera fusilarlo al amanecer del próximo día si no quedaba convencido de la veracidad de lo que iba a oír, invitaba a Mallo a sentarse, le ofrecía un cigarrillo, le pedía a un guardia que les trajera café, sacaba los últimos dos cuadernillos de la inmensa cartera negra que siempre portaba, y discutía meticulosamente con el recluso las diferencias y los datos que había encontrado en la nueva redacción. Incluso, si tenían tiempo jugaban una partida de ajedrez que con frecuencia ganaba el prisionero. De esos contactos, era posible que entre ambos hubiera surgido algo parecido a una relación amistosa o, al menos, cordial.</p>
<p>&nbsp;</p>
<p>Casi siempre, e indefectiblemente cuando perdía, Casteleiro se despedía sin aclararle si había decidido o no que lo fusilaran al amanecer. Era el dueño de su vida y disfrutaba su papel dejando sin revelar cuál había sido su decisión. Pero esa vez comenzó la conversación de un modo muy extraño y en un tono inusual que Mallo no consiguió descifrar.</p>
<p>&nbsp;</p>
<p>—Señor Mallo, creo que éste será mi último interrogatorio —dijo mirando fijamente a los ojos de su prisionero.</p>
<p>&nbsp;</p>
<p>Rafael no sabía si era un juego intimidatorio o si, efectivamente, sería liquidado a la salida del sol. En realidad, ya estaba acostumbrado a la idea, tras siete años en el pabellón de la muerte, cerca del foso de Santa Eulalia, donde se oían las órdenes impartidas al pelotón de fusilamiento y los posteriores disparos, de manera que optó por encogerse de hombros.</p>
<p>&nbsp;</p>
<p>—Comisario Casteleiro, haga lo que le ordenen. Si llegó la hora de llevarme al foso, no se preocupe por mí. Eso sí, le ruego que no envíe al capellán a consolarme. La idea del más allá, de una vida después de la muerte, me parece una fantasía pueril.</p>
<p>&nbsp;</p>
<p>El comisario movió la cabeza con el gesto universal de "este personaje es incorregible", pero enseguida pensó que su ateísmo militante era más respetable, dadas las circunstancias, que adoptar una posición oportunista. Hubiera sido despreciable una conversión de última hora. Se puso de pie y se acercó al ventanuco de su despacho. Extrajo un cigarrillo Gitanes de la cajetilla (el tabaco francés era casi la única concesión al hedonismo que estaba dispuesto a permitirse) y comenzó a explicarle la más sorprendente de las historias:</p>
<p>&nbsp;</p>
<p>—No, señor Mallo. No voy a ordenar su fusilamiento. Ayer recibí una orden, para mí, inexplicable.</p>
<p>&nbsp;</p>
<p>—¿En qué consiste esa orden? —preguntó Mallo intrigado. —Me han pedido que lo ayude a fugarse.</p>
<p>&nbsp;</p>
<p>El prisionero, incrédulo, abrió sus ojos azules hasta el límite que le permitían las órbitas.</p>
<p>&nbsp;</p>
<p>—¿No irá a aplicarme la ley de fuga? —preguntó con un gesto</p>
<p>&nbsp;</p>
<p>que, simultáneamente, descartaba esa posibilidad.</p>
<p>&nbsp;</p>
<p>—¿Para qué matarlo ilegalmente si puedo hacerlo con todas las de la ley? Simplemente, alguien en el Gobierno, y tiene que ser una persona muy importante, está interesado en que usted quede en libertad. El propio generalísimo Franco debe estar al tanto. En España nadie se atreve a hacer algo así sin contar con El Pardo. Es la primera vez que me piden una cosa tan extraña.</p>
<p>&nbsp;</p>
<p>Mallo advirtió, otra vez, que Casteleiro no podía pronunciar el nombre de Franco sin hacerlo preceder por su rango de generalísimo.</p>
<p>&nbsp;</p>
<p>—¿Y por qué, en ese caso, no me indultan?</p>
<p>&nbsp;</p>
<p>—Eso mismo pregunté yo —respondió Casteleiro—, pero me dijeron que no habría indulto ni explicación, sino fuga.</p>
<p>&nbsp;</p>
<p>—¿Y cómo me voy a fugar de Montjuich? Todo el mundo sabe que esta es una prisión prácticamente inexpugnable.</p>
<p>&nbsp;</p>
<p>—Es un plan de la Brigada Político-Social. Usted va a salir de la prisión en una furgoneta blindada, como si fuera a un trámite en los juzgados, pero en el trayecto unos hombres armados, vestidos de Guardia Civil, van a interceptar el vehículo y se lo llevarán.</p>
<p>&nbsp;</p>
<p>Mallo se quedó callado por unos segundos y, antes de responder, involuntariamente se le asomó una sonrisa muy triste.</p>
<p>&nbsp;</p>
<p>—¿A dónde me llevarán? La operación se parece mucho a la supuesta historia de cómo la Gestapo liberó a Andrés Nin de mano de sus captores comunistas.</p>
<p>&nbsp;</p>
<p>—Usted sabe que eso es mentira. Se lo he leído muchas veces en los informes autobiográficos que nos ha escrito. A Nin lo mataron los soviéticos.</p>
<p>&nbsp;</p>
<p>Mallo se quedó en silencio por un buen rato. Luego agregó:</p>
<p>&nbsp;</p>
<p>—Sí. Era una mentira increíble. No sé por qué dijeron una cosa tan estúpida. Supongo que formaba parte de la batalla entre los estalinistas y los trotskistas. Nin era un trotskista. La Guerra Civil sacó lo peor y lo mejor de la conducta de los españoles.</p>
<p>&nbsp;</p>
<p>—Así fue —sentenció Casteleiro—. Pero el enfrentamiento entre estalinistas y trotskistas no era una pelea entre buenos y malos, sino entre diversos grados de maldad.</p>
<p>&nbsp;</p>
<p>Mallo cambió súbitamente el curso de la conversación. No quería, otra vez, enfrascarse en una discusión ideológica con Casteleiro, un tipo fanático que creía en los ángeles y estaba convencido de que Franco, el generalísimo Franco, era una especie de santo laico que había salvado a Occidente de la dominación rusa.</p>
<p>&nbsp;</p>
<p>—Pero ¿para qué van a hacer una operación tan aparatosa? No entiendo la lógica.</p>
<p>&nbsp;</p>
<p>—También hice esa pregunta y por toda respuesta me dijeron que el objetivo era muy simple: que la historia la recogiera la prensa. Hasta me dieron la nota de la agencia EFE que publicarán todos los diarios del Movimiento. No puedo dejarle copia, pero sí estoy autorizado a leérsela.</p>
<p>&nbsp;</p>
<p>—Por favor, hágalo —casi imploró el prisionero.</p>
<p>&nbsp;</p>
<p>—Con gusto: "En la mañana de ayer martes, el delincuente político Rafael Mallo, un bandido hispanocubano, autodenominado 'poeta surrealista', exmiembro del Partido Obrero de Unificación Marxista, el llamado POUM, fundado por los trotskistas Andrés Nin y Joaquín Maurín, protagonizó una espectacular fuga en un falso retén colocado por subversivos en el kilómetro 15 de la carretera a Barcelona. Tres supuestos guardias civiles se lo llevaron a punta de pistola. Mallo estaba condenado a muerte desde 1940 por los crímenes cometidos durante la Guerra Civil, y desde entonces guardaba prisión en el castillo de Montjuich, en la sección conocida como el 'Tubo de la Risa'. Se espera que la policía logre recapturarlo en las próximas horas, pero hasta el momento no hay ninguna pista concreta sobre su paradero".</p>
<p>&nbsp;</p>
<p>—Muy bien. Muy imaginativo, ¿pero cómo saber que no voy a morir en medio de una confusa balacera?</p>
<p>&nbsp;</p>
<p>—Ésa es mi responsabilidad. Ya le he dicho que si la idea era matarlo, bastaba con fusilarlo dentro de la más estricta legalidad. Alguien lo quiere vivo. Me han pedido que dirija la operación e, incluso, que pase el bochorno de explicarle a la prensa que fuimos sorprendidos por los subversivos.</p>
<p>&nbsp;</p>
<p>—Pero, ¿por qué todo este enredo?</p>
<p>&nbsp;</p>
<p>—No tengo la menor idea. Supongo que se lo dirán a usted los mismos que simularán rescatarlo. Tras el "rescate" debo trasladarme a la Seo de Urgel. Ahí lo esperaré en un coche civil y con una muda de ropa para llevarlo sano y salvo hasta Andorra, donde lo recogerá alguien, que no sé quién es, porque no me lo han dicho.</p>
<p>&nbsp;</p>
<p>Mallo, como conocía a su carcelero, ya no tenía la menor duda de que Casteleiro le estaba diciendo todo lo que sabía.</p>
<p>&nbsp;</p>
<p>—¿Cuándo se llevará a cabo la operación?</p>
<p>&nbsp;</p>
<p>—Hoy mismo. Ahora. Comenzaremos en unos minutos. No quieren que se despida de sus amigos presos para que no cometa ninguna indiscreción. Ellos también van a ser sorprendidos con la noticia.</p>
<p>&nbsp;</p>
<p>—De acuerdo.</p>
<p>&nbsp;</p>
<p>—Magnífico. Manos a la obra.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: El blog de Montaner</p>
<p>Autor: El Nuevo Herald</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/396-colombia-da-voto-de-confianza-a-santos-y-al-proceso-de-paz">
			&laquo; Colombia da voto de confianza a Santos y al proceso de paz		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/405-analizan-realidades-y-perspectivas-del-constitucionalismo-hondureño">
			Analizan realidades y perspectivas del constitucionalismo hondureño &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/398-tiempo-de-canallas-fusilamiento-al-amanecer#startOfPageId398">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:53:"Tiempo de canallas, fusilamiento al amanecer - Relial";s:11:"description";s:155:"(El Nuevo Herald) Publicamos el primer capítulo de &#039;Tiempo de canallas&#039;, un &#039;thriller&#039; político que es la última novela de Carlos...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:44:"Tiempo de canallas, fusilamiento al amanecer";s:6:"og:url";s:109:"http://relial.org/index.php/productos/archivo/actualidad/item/398-tiempo-de-canallas-fusilamiento-al-amanecer";s:8:"og:title";s:53:"Tiempo de canallas, fusilamiento al amanecer - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/bab2d5d59ac444db8043a4f3e32c9f0e_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/bab2d5d59ac444db8043a4f3e32c9f0e_S.jpg";s:14:"og:description";s:171:"(El Nuevo Herald) Publicamos el primer capítulo de &amp;#039;Tiempo de canallas&amp;#039;, un &amp;#039;thriller&amp;#039; político que es la última novela de Carlos...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:44:"Tiempo de canallas, fusilamiento al amanecer";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}