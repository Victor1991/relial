<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8402:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId311"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Rusia y la nueva Guerra Fría
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/60959e8d8c34f5c00b9627dfd768f462_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/60959e8d8c34f5c00b9627dfd768f462_XS.jpg" alt="Rusia y la nueva Guerra Fr&iacute;a" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Vladimir Putin está experimentando la gloria del héroe victorioso. Ésa es una sensación muy poderosa que genera cierta adicción y tiende a repetirse. Más del 70% de los rusos apoyan la reconquista de Crimea. Retrospectivamente, juzgan con benevolencia la sujeción por la fuerza de Chechenia y el zarpazo dado a Georgia en el 2008 por los territorios de Osetia del Sur y Abjasia.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>¿Renace la Guerra Fría? En modo alguno. La Guerra Fría fue un episodio del siglo XX impulsado por una ideología universal de conquista, el marxismo-leninismo, que no era nacionalista sino internacionalista, utopía basada en la superstición de la lucha de clases y en el supuesto parentesco que vinculaba a todos los trabajadores del planeta frente a los capitalistas opresores que poseían los medios de producción.</p>
<p>&nbsp;</p>
<p>El marxismo-leninismo, sustento de la URSS, era una disparatada construcción artificial, intensamente racional que, tras arruinar a medio planeta y dejar cien millones de cadáveres sobre el terreno, acabó por implosionar como consecuencia de sus propias deficiencias y contradicciones cuando Gorbachov intentó rectificar los "errores". No eran errores en la ejecución del proyecto. La teoría completa carecía de sentido. No se podía rectificar. Había que sustituirla. Esa fue la ingrata y gloriosa tarea que le tocó a Boris Yeltsin.</p>
<p>&nbsp;</p>
<p>Esto que hoy ocurre es más emocional y ancla en unas actitudes anteriores al marxismo-leninismo. Dicho en un lenguaje metafórico: el marxismo-leninismo congregaba a las personas tras ideas equivocadas. Era un mal de la cabeza. El nacionalismo las junta tras emociones compartidas. Es un mal del corazón.</p>
<p>&nbsp;</p>
<p>(En realidad el nacionalismo también es un mal de la cabeza, en la medida en que la sensación física de amar a la patria, ese estremecimiento que provocan los himnos y las banderas, es la consecuencia de la acción de ciertos neurotransmisores encaminados a unificar a las tribus para fortalecerlas y lograr su supervivencia, pero esta aseveración, probablemente cierta, mas nunca confirmada, nos llevaría a un debate diferente que merece otra columna).</p>
<p>&nbsp;</p>
<p>En fin, los rusos, con Putin en el puente de mando, tratan de reeditar la gloria del viejo imperio construido por los zares. ¿Hay que tratar de frenar este espasmo imperial? Yo creo que sí. El nacionalismo, en pequeñas dosis, además de ser inevitable contribuye al mejoramiento colectivo, pero, cuando se exacerba, como demostró Hitler, puede ser letal.</p>
<p>&nbsp;</p>
<p>En Rusia, la mayor nación del planeta, que duplica el tamaño de Estados Unidos, Canadá, China o Brasil, los otros gigantes del mundo, vuelven a oírse los peligrosos argumentos del "espacio vital" o del supuesto derecho que tienen los Estados a proteger a las personas pertenecientes a la misma etnia radicadas en diferentes países. (Argumento que en nuestros días muy bien pudiera esgrimir México para invadir el sur de Estados Unidos ante los atropellos contra mexicanos indocumentados de personajes como Joe Arpaio, alguacil de Maricopa en Arizona).</p>
<p>&nbsp;</p>
<p>¿Qué puede hacer Estados Unidos ante la actitud de Rusia? Primero, entender que el Moscú poscomunista no es, por definición, antioccidental. Ya no busca el dominio del mundo sino restablecer la grandeza de Rusia y su rol de potencia internacional. Algo así sucedía en el siglo XIX, cuando Rusia unas veces se aliaba a algunas potencias europeas y otras reñía con ellas.</p>
<p>&nbsp;</p>
<p>Segundo, mantener afilada y creíble a la OTAN. El razonamiento de Stalin tras la Segunda Guerra vuelve a tener vigencia en Rusia. Entonces el padrecito Stalin pensaba que la seguridad de la URSS dependía del establecimiento de una zona de protección en el Este de Europa que comenzaba en los países Bálticos y no concluía hasta Bulgaria. Hoy casi toda esa zona pertenece a la Unión Europea y está protegida por la OTAN. Para el sostenimiento de la paz es vital que se mantenga esa protección.</p>
<p>&nbsp;</p>
<p>La OTAN fue un instrumento militar surgido durante la Guerra Fría que impidió el estallido de un tercer conflicto contra la URSS. Ahora servirá de elemento disuasorio ante la nueva-vieja Rusia. Por el bien de todos es muy útil mantenerlo aceitado. Los romanos tenían razón: si quieres la paz debes prepararte para la guerra. Si vis pacem, para bellum.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: Texto proporcionado por Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/310-méxico-insurgente">
			&laquo; México insurgente		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/313-¿es-la-desigualdad-un-problema?">
			¿Es la desigualdad un problema? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/311-rusia-y-la-nueva-guerra-fría#startOfPageId311">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:38:"Rusia y la nueva Guerra Fría - Relial";s:11:"description";s:158:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Vladimir Putin está experimentando la gloria del héroe victorioso. Ésa es una sensación muy podero...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:29:"Rusia y la nueva Guerra Fría";s:6:"og:url";s:96:"http://www.relial.org/index.php/productos/archivo/opinion/item/311-rusia-y-la-nueva-guerra-fría";s:8:"og:title";s:38:"Rusia y la nueva Guerra Fría - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/60959e8d8c34f5c00b9627dfd768f462_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/60959e8d8c34f5c00b9627dfd768f462_S.jpg";s:14:"og:description";s:162:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Vladimir Putin está experimentando la gloria del héroe victorioso. Ésa es una sensación muy podero...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:29:"Rusia y la nueva Guerra Fría";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}