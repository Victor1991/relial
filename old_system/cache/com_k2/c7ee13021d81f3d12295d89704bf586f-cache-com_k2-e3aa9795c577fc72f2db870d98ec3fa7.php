<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8396:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId242"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Honduras o el fin del chavismo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/fedea746cd0ecb257a1249d3a2a80bb1_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/fedea746cd0ecb257a1249d3a2a80bb1_XS.jpg" alt="Honduras o el fin del chavismo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Las mejores encuestas lo advirtieron una semana antes. Juan Orlando Hernández, al frente del Partido Nacional, le sacaría entre 5 y 6 puntos de ventaja a Xiomara Castro, la mujer de Mel Zelaya, cabeza nominal del partido Libre. Y así fue: votó un 60% del censo electoral y JOH obtuvo el 35% de los sufragios. Xiomara Castro, como testaferro de su marido, recibió el 29%.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>Los gritos de Mel son inútiles. Puede armar una permanente protesta pública, como Andrés Manuel López Obrador en México, pero el joven abogado Juan Orlando Hernández es ya el presidente electo de Honduras. Lo certificó el Tribunal Superior Electoral y le dieron el visto bueno la OEA, el Parlamento Europeo y el Centro Carter. Lo inteligente sería que Zelaya admitiera su derrota.</p>
<p>&nbsp;</p>
<p>¿Por qué no lo hace? Afirman que Mel, electo diputado por su Olancho natal, está tratando de cambiar su pacífica aceptación de los comicios por la presidencia del Congreso. Si es cierto, no creo que lo logre. De los 128 diputados de la cámara –no hay senado– el partido oficial (Nacional) cuenta con 47, los zelayistas (Libre) 39, los liberales 26, Salvador Nasralla (PAC) 13 y otras 3 formaciones uno per cápita.</p>
<p>&nbsp;</p>
<p>El pacto "natural" en el Congreso pudiera ser entre los nacionalistas de Juan Orlando Hernández y los liberales de Mauricio Villeda. Al fin y al cabo, esas dos formaciones, acompañadas por el Poder Judicial, acordaron desalojar del poder a Zelaya en junio del 2009, cuando Mel trató, torpemente, de llevar su país al bando chavista. Entonces, 111 de los 128 diputados –liberales y nacionalistas– votaron su destitución. Acaso vuelvan a coincidir.</p>
<p>&nbsp;</p>
<p>Estas elecciones son mucho más importantes de lo que parecen. Finalmente, las preferencias políticas de los hondureños han podido contarse sin tapujos y se demostró que el chavismo, liderado por Zelaya, nunca alcanzó el 30% de respaldo popular. Era una loca y temeraria imposición tratar de arrastrar a los hondureños a un modelo político y económico rechazado por el 70% de la sociedad.</p>
<p>&nbsp;</p>
<p>Como ahora se ha visto, la destitución de Zelaya no sólo respondió al ordenamiento constitucional. También expresaba la voluntad de una mayoría que no quería participar en la fallida aventura autoritaria del Socialismo del Siglo XXI.</p>
<p>&nbsp;</p>
<p>Los hondureños, de algún modo, se adelantaron a su tiempo. El escandaloso mundillo de la ALBA está de capa caída tras la muerte de Hugo Chávez y el inocultable desastre venezolano. Resultó muy significativo que Daniel Ortega fuera una de las primeras voces que reconocieron el triunfo de Juan Orlando Hernández.</p>
<p>&nbsp;</p>
<p>Por otra parte, Nicolás Maduro, Evo Morales, Rafael Correa y Raúl Castro se han mantenido en silencio, lo mismo que el chavismo vegetariano de la periferia democrática: Cristina Fernández de Kirchner, Dilma Rousseff y José Mujica. No han tenido la cortesía de felicitar pública y vivamente a JOH, pero tampoco se han sumado al coro de los deslegitimadores. Eso es moral de derrota.</p>
<p>&nbsp;</p>
<p>Probablemente, el resultado de las elecciones hondureñas sea un ensayo general de lo que sucederá en Venezuela en la consulta del 8 de diciembre próximo. No son comicios presidenciales, sino municipales. Supuestamente, lo que está en juego son 335 alcaldías con sus aproximadamente dos mil quinientos concejales, pero, en verdad, se trata de una prueba del liderazgo y respaldo que posee Nicolás Maduro.</p>
<p>&nbsp;</p>
<p>Si los venezolanos logran impedir las trampas, votan masivamente y consiguen que el gobierno no vulnere la voluntad popular –tres condiciones esenciales–, ocurrirá lo que anticipa el encuestador Alfredo Keller: el chavismo perderá por una decena de puntos y en casi todas las ciudades importantes se instalará la oposición.</p>
<p>&nbsp;</p>
<p>Esa situación multiplicará sustancialmente la debilidad de Nicolás Maduro, un personaje nada respetado y poco querido por la sociedad, incluidos los chavistas cansados de un patético señor que habla con los pájaros, duerme junto a los restos de Chávez y no deja de hacer y decir tonterías. Pero la oposición, si obtiene la victoria, no puede dejar arrebatársela. Sería el fin de cualquier esperanza de salir de esa pesadilla pacíficamente.</p>
<p>&nbsp;</p>
<p>fuente: <a href="http://www.elblogdemontaner.com">www.elblogdemontaner.com</a></p>
<p>foto: El blog de Montaner</p>
<p>autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde">
			&laquo; JFK y Castro se encuentran medio siglo más tarde		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela">
			La grandeza de Mandela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo#startOfPageId242">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:39:"Honduras o el fin del chavismo - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Las mejores encuestas lo advirtieron una semana antes. Juan Orlando Hernández, al frente del Parti...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:30:"Honduras o el fin del chavismo";s:6:"og:url";s:97:"http://www.relial.org/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo";s:8:"og:title";s:39:"Honduras o el fin del chavismo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/fedea746cd0ecb257a1249d3a2a80bb1_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/fedea746cd0ecb257a1249d3a2a80bb1_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Las mejores encuestas lo advirtieron una semana antes. Juan Orlando Hernández, al frente del Parti...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:30:"Honduras o el fin del chavismo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}