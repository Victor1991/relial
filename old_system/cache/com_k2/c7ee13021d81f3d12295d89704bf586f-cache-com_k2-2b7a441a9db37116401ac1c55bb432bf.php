<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8818:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId269"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/d65085d854a39feae81a41eb458281c5_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/d65085d854a39feae81a41eb458281c5_XS.jpg" alt="Apuleyo: el sistema de partidos podr&iacute;a colapsar si no renueva dirigencia" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El escritor y diplomático colombiano Plinio Apuleyo Mendoza consideró que el sistema de partidos del país puede colapsar si no se renuevan las dirigencias con mayor participación de los jóvenes.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>Al dictar una conferencia en la que participó el presidente del Partido Revolucionario Dominicano (PRD), el secretario general del PRSC, Ramón Rogelio Genao y otros dirigentes de los tres principales partidos del país, el diplomático colombiano destacó que en el padrón electoral de República Dominicana los jóvenes ocupan el 45%, lo que indica que los partidos deben dar participación a ese segmento poblacional.</p>
<p>&nbsp;</p>
<p>Se mostró sorprendido tras la pregunta de una estudiante de la PUCMM sobre las pocas oportunidades que brindan los partidos locales a los jóvenes.</p>
<p>&nbsp;</p>
<p>"Es un error, la gente joven comienza a sentirse que se queda marginada, si solamente asisten a la política los que se comprometen a jugar con esas vieja normas clientelistas. Puede colapsar el sistema de partidos aca, claro que puede colapsar", dijo Apuleyo Mendoza.</p>
<p>&nbsp;</p>
<p>El periodista colombiano fue el orador de la conferencia sobre el "Auge y caída de la partidocracia tradicional latinoamericana", que tuvo lugar en el auditorio A, y fue organizada por la Asociación de Cronistas Políticos de la República Dominicana (ACP), el Centro de Análisis para Políticas Públicas (CAPP) que dirige el diputado Víctor Bisonó, y la Escuela de Derecho de la Pontificia Universidad Católica Madre y Maestra (PUCMM).</p>
<p>&nbsp;</p>
<p>Dijo que en Latinoamérica, el desprestigio de los partidos políticos tradicionales, el clientelismo y la corrupción están socavando al sistema de partidos, por lo que estimó peligroso la perdida de la esencia en la forma de hacer política, ya que se están inclinando a los electores hacia opciones terceristas y populistas.</p>
<p>&nbsp;</p>
<p>Apuleyo Mendoza deploró que el dinero se haya convertido en influencia electoral, por lo que en muchos países del área la población no quiere saber nada de los políticos.</p>
<p>&nbsp;</p>
<p>"Se acabó la fe en los partidos y surgió otra cosa, vote por mí, vote por el, y yo le doy esto. Recuerdo que un cuñado galanista echó unos discursos y un campesino le dijo que era muy bonito el discurso, pero le preguntó ¿y pá yo que?, puntualizó Apuleyo Mendoza.</p>
<p>&nbsp;</p>
<p>Presidente de la ACP</p>
<p>El presidente de la ACP, el periodista Alberto Caminero, manifestó que el país requiere de líderes políticos responsables, que hagan causa común con el anhelo de sus militantes y simpatizantes y se conviertan en organizaciones que preparen a sus jóvenes y mujeres en las escuelas de cuadros.</p>
<p>&nbsp;</p>
<p>"Queremos vivir en verdadera democracia, donde líderes sociales y comunitarios estén motivados a ir a los partidos de su preferencia, porque valoran la verdadera participación de sus miembros, expuso Caminero.</p>
<p>&nbsp;</p>
<p>Sostuvo que en los últimos años los partidos han experimentado un deterioro preocupante, debido a las insatisfacciones de la población sobre las políticas de los gobiernos durante los cuatrenios que les ha tocado gobernar el país, y se ha ampliado la brecha entre ricos y pobres.</p>
<p>&nbsp;</p>
<p>Sobre Apuleyo</p>
<p>Apuleyo fue coautor con Gabriel García Márquez del libro "El olor de la guayaba" y del "Manual del perfecto idiota latinoamericano" con Carlos Montaner y Álvaro Vargas Llosa.</p>
<p>&nbsp;</p>
<p>Entre sus obras se destacan "Aquellos tiempos con Gabo", "Los retos del poder", "El desafío neoliberal", "Entre dos aguas", "El desertor, años de fuego", "La llama y el hielo", entre otras.</p>
<p>&nbsp;</p>
<p>El Periodista y escritor se encuentra en el país tras la invitación de la ACP, que preside Alberto Caminero, de la que también forman parte de la directiva los periodistas Héctor Marte Pérez, Abel Guzmán y Rosa Alcántara.</p>
<p>&nbsp;</p>
<p>Miguel Vargas</p>
<p>El presidente del Partido Revolucionario Dominicano (PRD) Miguel Vargas, estuvo presente en la exposición del diplomático y escritor colombiano.</p>
<p>&nbsp;</p>
<p>Estuvo acompañado del ex jefe de las fuerzas armadas José Miguel Soto Jiménez y del diputado Víctor Gómez Casanova, entre otros dirigentes del PRD.</p>
<p>&nbsp;</p>
<p>La ACP también envió invitación al presidente Danilo Medina, a los presidentes de las dos cámaras del Congreso, Reinaldo Pared Pérez y Abel Martínez, a los presidentes del PLD, Leonel Fernández; del PRSC, Carlos Morales Troncoso; y al presidente de la Junta Central Electoral, Roberto Rosario, pero ninguno de estos estuvo presente.</p>
<p>&nbsp;</p>
<p>fuente: <a href="http://www.elcaribe.com">www.elcaribe.com</a></p>
<p>autor: CAPP</p>
<p>foto: CAPP</p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/267-crisis-en-américa-latina-por-factores-institucionales">
			&laquo; Crisis en América Latina por factores institucionales		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/280-respuestas-liberales-para-la-región">
			Respuestas liberales para la región &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/269-apuleyo-el-sistema-de-partidos-podría-colapsar-si-no-renueva-dirigencia#startOfPageId269">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:82:"Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia - Relial";s:11:"description";s:156:"El escritor y diplomático colombiano Plinio Apuleyo Mendoza consideró que el sistema de partidos del país puede colapsar si no se renuevan las dirigen...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:73:"Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia";s:6:"og:url";s:142:"http://www.relial.org/index.php/productos/archivo/actualidad/item/269-apuleyo-el-sistema-de-partidos-podría-colapsar-si-no-renueva-dirigencia";s:8:"og:title";s:82:"Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/d65085d854a39feae81a41eb458281c5_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/d65085d854a39feae81a41eb458281c5_S.jpg";s:14:"og:description";s:156:"El escritor y diplomático colombiano Plinio Apuleyo Mendoza consideró que el sistema de partidos del país puede colapsar si no se renuevan las dirigen...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:73:"Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}