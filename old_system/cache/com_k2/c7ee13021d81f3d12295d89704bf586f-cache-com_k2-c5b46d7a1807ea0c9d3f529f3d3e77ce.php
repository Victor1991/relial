<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12886:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId307"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La libertad en las calles
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/753a82091bdf93df272697e1f26229c2_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/753a82091bdf93df272697e1f26229c2_XS.jpg" alt="La libertad en las calles" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Mario Vargas Llosa</p>
<p>&nbsp;</p>
<p>PIEDRA DE TOQUE. Venezuela ya no es un país democrático y la gran movilización popular es para que haya todavía elecciones de verdad en ese país y no rituales operaciones circenses como son las de Cuba</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Hace ya cuatro semanas que los estudiantes venezolanos comenzaron a protestar en las calles de las principales ciudades del país contra el Gobierno de Nicolás Maduro y, pese a la dura represión —20 muertos y más de 300 heridos reconocidos hasta ahora por el régimen, y cerca de un millar de detenidos, entre ellos Leopoldo López, uno de los principales líderes de la oposición—, la movilización popular sigue en pie. Ha sembrado Venezuela de "Trincheras de la Libertad" en las que, además de universitarios y escolares, hay ahora obreros, amas de casa, empleados, profesionales, una ola popular que parece incluso haber desbordado a la Mesa de la Unidad Democrática (MUD), la organización sombrilla de todos los partidos y grupos políticos gracias a los cuales Venezuela no se ha convertido todavía en una segunda Cuba.</p>
<p>&nbsp;</p>
<p>Pero que esas son las intenciones del sucesor del comandante Hugo Chávez es evidente. Todos los pasos que ha dado en el año que lleva en el poder que le legó su predecesor son inequívocos. El más notorio, la asfixia sistemática de la libertad de expresión. El único canal de televisión independiente que sobrevivía —Globovisión— fue sometido a un acoso tal por el Gobierno, que sus dueños debieron venderlo a empresarios adictos, que lo han alineado ahora con el chavismo. El control de las estaciones de radio es casi absoluto y las que todavía se atreven a decir la verdad sobre la catastrófica situación económica y social del país tienen los días contados. Lo mismo ocurre con la prensa independiente, a quien el Gobierno va eliminando poco a poco mediante el sistema de privarla de papel.</p>
<p>&nbsp;</p>
<p>Sin embargo, aunque el pueblo venezolano ya casi no pueda ver, oír ni leer una información libre, vive en carne propia la descarnada y trágica situación a la que los desvaríos ideológicos del régimen —las nacionalizaciones, el intervencionismo sistemático en la vida económica, el hostigamiento a la empresa privada, la burocratización cancerosa— han llevado a Venezuela y esta realidad no se oculta con demagogia. La inflación es la más alta de América Latina y la criminalidad una de las más altas del mundo. La carestía y el desabastecimiento han vaciado los anaqueles de los almacenes y la imposición de precios oficiales para todos los productos básicos ha creado un mercado negro que multiplica la corrupción a extremos de vértigo. Solo la nomenclatura conserva altos niveles de vida, mientras la clase media se encoge cada día más y los sectores populares son golpeados de una manera inmisericorde que el régimen trata de paliar con medidas populistas —estatismo, colectivismo, repartos de dádivas— y mucha, mucha propaganda acusando a la "derecha", el "fascismo" y el "imperialismo norteamericano" del desbarajuste y caída en picado de los niveles de vida del pueblo venezolano.</p>
<p>&nbsp;</p>
<p>La palabra favorita de Maduro es "¡fascista!", que endilga a quienes se oponen a su régimen</p>
<p>&nbsp;</p>
<p>El historiador mexicano Enrique Krauze recordaba hace algunos días el fantástico dispendio que ha hecho el régimen chavista, en los 15 años que lleva en el poder, de los 800.000 millones de dólares que ingresaron al país en este periodo gracias al petróleo (las reservas petroleras de Venezuela son las más grandes del mundo). Buena parte de ese irresponsable derroche ha servido para garantizar la supervivencia económica de Cuba y para subvencionar o sobornar a esos Gobiernos que, como el nicaragüense del comandante Ortega, el argentino de la señora Kirchner o el boliviano de Evo Morales, se han apresurado en estos días a solidarizarse con Nicolás Maduro y a condenar la protesta de los estudiantes "fascistas" venezolanos.</p>
<p>&nbsp;</p>
<p>La prostitución de las palabras, como lo señaló Orwell, es la primera proeza de todo Gobierno de vocación totalitaria. Nicolás Maduro no es un hombre de ideas, como advierte de inmediato quien lo oye hablar; los lugares comunes embrollan sus discursos, que él pronuncia siempre rugiendo, como si el ruido pudiera suplir la falta de razones, y su palabra favorita parece ser "¡fascista!", que endilga sin ton ni son a todos los que critican y se oponen al régimen que ha llevado a uno de los países potencialmente más ricos del mundo a la pavorosa situación en que se encuentra. ¿Sabe el señor Maduro lo que fascismo significa? ¿No se lo enseñaron en las escuelas cubanas donde recibió su formación política? Fascismo significa un régimen vertical y caudillista, que elimina toda forma de oposición y, mediante la violencia, anula o extermina las voces disidentes; un régimen invasor de todos los dominios de la vida de los ciudadanos, desde el económico hasta el cultural y, principalmente, claro está, el político; un régimen donde los pistoleros y matones aseguran mediante el terror la unanimidad del miedo y el silencio y una frenética demagogia a través de los medios tratando de convencer al pueblo día y noche de que vive en el mejor de los mundos. Es decir, el fascismo es lo que va viviendo cada día más el infeliz pueblo venezolano, lo que representa el chavismo en su esencia, ese trasfondo ideológico en el que, como explicó tan bien Jean-François Revel, todos los totalitarismos —fascismo, leninismo, estalinismo, castrismo, maoísmo, chavismo— se funden y confunden.</p>
<p>&nbsp;</p>
<p>Es contra esta trágica decadencia y la amenaza de un endurecimiento todavía peor del régimen —una segunda Cuba— que se han levantado los estudiantes venezolanos, arrastrando con ellos a sectores muy diversos de la sociedad. Su lucha es para impedir que la noche totalitaria caiga del todo sobre la tierra de Simón Bolívar y ya no haya vuelta atrás. Leo, esta mañana, un artículo de Joaquín Villalobos en EL PAÍS (Cómo enfrentarse al chavismo), desaconsejando a la oposición venezolana la acción directa que ha emprendido y recomendándole que espere, más bien, que crezcan sus fuerzas para poder ganar las próximas elecciones. Sorprende la ingenuidad del exguerrillero convertido (en buena hora) a la cultura democrática. ¿Quién garantiza que habrá futuras elecciones dignas de ese nombre en Venezuela? ¿Lo fueron las últimas, en las condiciones de desventaja absoluta para la oposición en que se dieron, con un poder electoral sometido al régimen, una prensa sofocada y un control obsceno de los recuentos por los testaferros del Gobierno? Desde luego que la oposición pacífica es lo ideal, en democracia. Pero Venezuela ya no es un país democrático, está mucho más cerca de una dictadura como la cubana que de lo que son, hoy en día, países como México, Chile o Perú. La gran movilización popular que hoy día vive Venezuela es, precisamente, para que, en el futuro, haya todavía elecciones de verdad en ese país y no sean esas rituales operaciones circenses como eran las de la Unión Soviética o son todavía las de Cuba, donde los electores votan por candidatos únicos, que ganan, oh sorpresa, siempre, por el 99% de los votos.</p>
<p>&nbsp;</p>
<p>Es triste, aunque no sorprende, la soledad de los valientes venezolanos que luchan por su país</p>
<p>&nbsp;</p>
<p>Lo que es triste, aunque no sorprendente, es la soledad en que los valientes venezolanos que ocupan las "Trincheras de la Libertad" están luchando por salvar a su país, y a toda América Latina, de una nueva satrapía comunista, sin recibir el apoyo que merecen de los países democráticos o de esa inútil y apolillada OEA (Organización de Estados Americanos), en cuya carta principista, vaya vergüenza, figura velar por la legalidad y la libertad de los países que la integran. Naturalmente, qué otra cosa se puede esperar de Gobiernos cuyos presidentes comparecieron, prácticamente todos, en La Habana, a celebrar la Cumbre de la Comunidad de Estados Latinoamericanos y Caribeños (CELAC) y a rendir un homenaje a Fidel Castro, momia viviente y símbolo animado de la dictadura más longeva de la historia de América Latina.</p>
<p>&nbsp;</p>
<p>Sin embargo, este lamentable espectáculo no debe desmoralizarnos a quienes creemos que, pese a tantos indicios en contrario, la cultura de la libertad ha echado raíces en el continente latinoamericano y no volverá a ser erradicada en el futuro inmediato, como tantas veces en el pasado. Los pueblos en nuestros países suelen ser mejores que sus Gobiernos. Ahí están para demostrarlo los venezolanos, como los ucranios ayer, jugándose la vida en nombre de todos nosotros, para impedir que en la tierra de la que salieron los libertadores de América del Sur desaparezcan los últimos resquicios de libertad que todavía quedan. Tarde o temprano, triunfarán.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: El país</p>
<p>Autor: Mario Vargas Llosa</p>
<p>Foto: El país</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/304-un-apunte-sobre-bitcoin">
			&laquo; Un apunte sobre Bitcoin		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/309-la-oportunidad-perdida">
			La oportunidad perdida &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/307-la-libertad-en-las-calles#startOfPageId307">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:34:"La libertad en las calles - Relial";s:11:"description";s:157:"En la opinión de Mario Vargas Llosa &amp;nbsp; PIEDRA DE TOQUE. Venezuela ya no es un país democrático y la gran movilización popular es para que haya...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:25:"La libertad en las calles";s:6:"og:url";s:92:"http://www.relial.org/index.php/productos/archivo/opinion/item/307-la-libertad-en-las-calles";s:8:"og:title";s:34:"La libertad en las calles - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/753a82091bdf93df272697e1f26229c2_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/753a82091bdf93df272697e1f26229c2_S.jpg";s:14:"og:description";s:161:"En la opinión de Mario Vargas Llosa &amp;amp;nbsp; PIEDRA DE TOQUE. Venezuela ya no es un país democrático y la gran movilización popular es para que haya...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:25:"La libertad en las calles";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}