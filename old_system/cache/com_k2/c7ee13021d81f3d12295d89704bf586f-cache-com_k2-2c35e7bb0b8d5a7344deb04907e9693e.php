<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6524:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId425"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Diálogo “La defensa de la libertad y la lucha contra el racismo”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/6cdb4ac6ccf86fc9922b1b1ecf5faa0d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/6cdb4ac6ccf86fc9922b1b1ecf5faa0d_XS.jpg" alt="Di&aacute;logo &ldquo;La defensa de la libertad y la lucha contra el racismo&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Diálogo "La defensa de la libertad y la lucha contra el racismo"</p>
<p>&nbsp;</p>
<p>El Instituto Político para la libertad (IPL) organizó un diálogo con el tema "La defensa de la libertad y la lucha contra el racismo".</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>La productiva conversación, tuvo como invitado a Marco Antonio Ramírez, presidente de la Red Peruana de Jóvenes Afrodescendientes: Ashanti Perú, quien expuso sobre la labor que realiza para luchar contra el racismo en nuestro país y sus esfuerzos por visibilizar a los afrodescendientes en nuestra sociedad.</p>
<p>&nbsp;</p>
<p>Intelectuales liberales como Yesenia Alvarez, Paul Laurent, Edwin Zarco y jóvenes promotores de la libertad como Alfredo Li, Jose Landeo, Edson Villantoy, entre otros, aportaron con sus ideas en este debate en el marco de una agenda de IPL para proponer ideas para luchar contra el racismo desde la filosofía de la libertad.</p>
<p>&nbsp;</p>
<p>Estos diálogos son preparatorios para nuestro programa anual "Universidad de la Libertad", que esta vez se centrará en "La libertad y los derechos humanos" porque este año ha sido adverso para las libertades individuales en el continente tal como lo que está sucediendo en Venezuela y Cuba, por ejemplo.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Dialogue "The defense of freedom and the fight against racism"</p>
<p>&nbsp;</p>
<p>The Political Institute for Freedom (IPL) organized a dialogue with the topic "The defense of freedom and the fight against racism."</p>
<p>&nbsp;</p>
<p>The productive conversation, was a guest Marco Antonio Ramirez, president of the Peruvian Youth Network of African Descent: Ashanti Peru, who spoke about the work being done to figth against racism in our country and their efforts to make visible people of African descent in our society.</p>
<p>&nbsp;</p>
<p>Liberal intellectuals like Yesenia Alvarez, Paul Laurent, Edwin Zarco and young promoters of freedom as Alfredo Li, Edson Villantoy, among others, contributed their ideas to this debate in the context of an agenda of IPL to propose ideas for fighting against racism from the philosophy of freedom.</p>
<p>&nbsp;</p>
<p>These dialogues are preparatory to our anual program "Freedom University", this time focusing on "Freedom and human rights" because this year has been adverse to the individual freedoms on the continent such as what is happening in Venezuela and Cuba, for example.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: Instituto Político para la Libertad - IPL Perú</p>
<p>Foto: IPL</p>
<p>Autor: IPL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/424-alerta-por-los-derechos-humanos-de-activista-lgbti-en-venezuela">
			&laquo; Alerta por los Derechos Humanos de activista LGBTI en Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/426-asociación-de-jóvenes-argentinos-liberales">
			Asociación de Jóvenes Argentinos Liberales &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/425-diálogo-“la-defensa-de-la-libertad-y-la-lucha-contra-el-racismo”#startOfPageId425">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:78:"Diálogo “La defensa de la libertad y la lucha contra el racismo” - Relial";s:11:"description";s:157:"Diálogo &quot;La defensa de la libertad y la lucha contra el racismo&quot; &amp;nbsp; El Instituto Político para la libertad (IPL) organizó un diálogo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:69:"Diálogo “La defensa de la libertad y la lucha contra el racismo”";s:6:"og:url";s:216:"http://www.relial.org/index.php/productos/archivo/actualidad/item/425-diÃƒÆ’Ã‚Â¡logo-ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œla-defensa-de-la-libertad-y-la-lucha-contra-el-racismoÃƒÂ¢Ã¢â€šÂ¬Ã‚Â";s:8:"og:title";s:78:"Diálogo “La defensa de la libertad y la lucha contra el racismo” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/6cdb4ac6ccf86fc9922b1b1ecf5faa0d_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/6cdb4ac6ccf86fc9922b1b1ecf5faa0d_S.jpg";s:14:"og:description";s:169:"Diálogo &amp;quot;La defensa de la libertad y la lucha contra el racismo&amp;quot; &amp;amp;nbsp; El Instituto Político para la libertad (IPL) organizó un diálogo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:69:"Diálogo “La defensa de la libertad y la lucha contra el racismo”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}