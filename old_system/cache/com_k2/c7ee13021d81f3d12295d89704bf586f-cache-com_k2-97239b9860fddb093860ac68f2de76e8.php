<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7056:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId478"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	En relación a la Ley para permitir la fuerza letal con manifestantes en Venezuela (inglés)
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/937f929d420ab20a5c9d96a1ab9e2021_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/937f929d420ab20a5c9d96a1ab9e2021_XS.jpg" alt="En relaci&oacute;n a la Ley para permitir la fuerza letal con manifestantes en Venezuela (ingl&eacute;s)" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Carta del Vicepresidente de la Internacional Liberal, el Sr. Markus Löning, al Embajador de Venezuela ante las Naciones Unidas en Ginebra, en relación con la reciente ley para permitir que la fuerza letal contra los manifestantes en el país.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><span style="color: #000000;">His Excellency Mr. Jorge Valero</span></p>
<p><span style="color: #000000;">Ambassador Extraordinary and Plenipotentiary</span></p>
<p><span style="color: #000000;">Permanent Mission of the Bolivarian Republic of Venezuela to the UN</span></p>
<p><span style="color: #000000;">Chemin François-Lehmann 18A</span></p>
<p><span style="color: #000000;">Grand-Saconnex, 1218</span></p>
<p><span style="color: #000000;">Geneva</span></p>
<p><span style="color: #000000;"><br /></span></p>
<p><span style="color: #000000;">London, 3</span> February 2015</p>
<p>&nbsp;</p>
<p>Your Excellency,</p>
<p>It is with great dismay and concern that we, Liberal International - the world federation of liberal</p>
<p>political parties - learn of the decision made by the government of the Bolivarian Republic of</p>
<p>Venezuela to adopt military tactics that permit the use of potentially lethal force against civilians</p>
<p>during public gatherings and demonstrations.</p>
<p>At the heart of every functioning democracy in the society of states lies the right to freedom of speech,</p>
<p>including the right of citizens to participate in public demonstrations. This right is clearly enshrined in</p>
<p>and protected by the Universal Declaration of Human Rights, to which the Bolivarian Republic of</p>
<p>Venezuela is a signatory. We expect that Venezuela will honour the international legal obligations to</p>
<p>which it has committed itself and protect its citizens who choose to exercise their rights. Further, we</p>
<p>call upon the Venezuelan government to immediately repeal the proposed measure and permit and</p>
<p>protect peaceful domestic demonstrations as is expected of any government.</p>
<p>Liberal International is vocal at the United Nations Human Rights Council on a regular basis and,</p>
<p>through its consultative status, defends the rights of people around the world to pursue freedom and</p>
<p>democracy. In this capacity Liberal International will continue to speak out for the basic rights of the</p>
<p>Venezuelan people.</p>
<p>Kind Regards,</p>
<p>&nbsp;</p>
<p>&nbsp;<strong>Markus Loening</strong></p>
<p>&nbsp;Vice-President, Liberal International</p>
<p>&nbsp;Chair, Liberal International Human Rights Committee</p>
<p>&nbsp;</p>
<p><span style="color: #0000ff;">Por favor, siga este enlace, donde encontrará un artículo completo en la página web LI. Se adjunta una copia de la carta. Si usted cree que sería de uso, por favor circular esta carta a los miembros de la prensa en Venezuela y más allá: http://www.liberal-international.org/site/VP_Loening_confronts_Venezuelan_Ambassador_over_proposal_for_lethal_force_against_protesters.html</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/477-2014-global-go-to-think-tanks">
			&laquo; 2014 Global Go To Think Tanks		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/480-se-incrementa-la-represión-económica-en-venezuela">
			Se incrementa la represión económica en Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/478-en-relación-a-la-ley-para-permitir-la-fuerza-letal-con-manifestantes-en-venezuela#startOfPageId478">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:101:"En relación a la Ley para permitir la fuerza letal con manifestantes en Venezuela (inglés) - Relial";s:11:"description";s:155:"Carta del Vicepresidente de la Internacional Liberal, el Sr. Markus Löning, al Embajador de Venezuela ante las Naciones Unidas en Ginebra, en relación...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:92:"En relación a la Ley para permitir la fuerza letal con manifestantes en Venezuela (inglés)";s:6:"og:url";s:154:"http://www.relial.org/index.php/productos/archivo/actualidad/item/478-en-relaciÃ³n-a-la-ley-para-permitir-la-fuerza-letal-con-manifestantes-en-venezuela";s:8:"og:title";s:101:"En relación a la Ley para permitir la fuerza letal con manifestantes en Venezuela (inglés) - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/937f929d420ab20a5c9d96a1ab9e2021_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/937f929d420ab20a5c9d96a1ab9e2021_S.jpg";s:14:"og:description";s:155:"Carta del Vicepresidente de la Internacional Liberal, el Sr. Markus Löning, al Embajador de Venezuela ante las Naciones Unidas en Ginebra, en relación...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:92:"En relación a la Ley para permitir la fuerza letal con manifestantes en Venezuela (inglés)";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}