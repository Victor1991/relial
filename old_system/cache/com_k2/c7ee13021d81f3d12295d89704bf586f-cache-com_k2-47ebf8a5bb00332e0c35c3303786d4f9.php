<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:13079:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId396"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Colombia da voto de confianza a Santos y al proceso de paz
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/18bdd04e288fc232234be2fb5ea8bf38_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/18bdd04e288fc232234be2fb5ea8bf38_XS.jpg" alt="Colombia da voto de confianza a Santos y al proceso de paz" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><strong>El presidente logró imponerse a su contrincante Iván Zuluaga en la segunda vuelta de las presidenciales. El resultado se interpreta como un importante espaldarazo al proceso de paz con la guerrilla.</strong></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Juan Manuel Santos seguirá siendo presidente de Colombia, luego de que este domingo la mayoría de los colombianos decidiera darle la oportunidad de un nuevo período al frente del gobierno.</p>
<p>&nbsp;</p>
<p>También se trata de un voto de confianza al proceso de paz con las guerilleras Fuerzas Armadas Revolucionarias de Colombia (FARC) que Santos inició en noviembre de 2012.</p>
<p>&nbsp;</p>
<p>Con el 99,97% de las mesas de votación escrutadas, el mandatario obtuvo el 50,94% de los votos, por el 45,01% de su rival, Oscar Iván Zuluaga.</p>
<p>&nbsp;</p>
<p>Y, junto a las alianzas de última hora, una mayor participación en las urnas parece haber decidido la contienda, pues esta vez cinco de cada diez colombianos elegibles para votar (47,98%) lo hicieron, mientras que en la primera vuelta de las elecciones la abstención superó el 60%.</p>
<p>&nbsp;</p>
<p>"Colombianos de muy diferentes opciones, incluyendo muchos que no simpatizaban con mi gobierno, se movilizaron por una causa, la causa de la paz", dijo Santos durante la noche del domingo, en su discurso de celebración.</p>
<p>&nbsp;</p>
<p>"Se movilizaron a sabiendas que la historia tiene sus momentos y que este es el momento de la paz, de terminar ese largo y cruento conflicto", agregó.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>UN DESAFÍO CLARO</p>
<p>&nbsp;</p>
<p>Elegido luego de una campaña que tuvo ese tema como su eje central Santos ahora está obligado a culminar satisfactoriamente las negociaciones con las FARC en La Habana y avanzar lo más rápidamente posible en las conversaciones con el ELN, la segunda guerrilla del país.</p>
<p>&nbsp;</p>
<p>Pero, al mismo tiempo, también tendrá que garantizar que cualquier acuerdo con los rebeldes que se alcance en la mesa de negociación sea además aceptable para la mayoría de la población.</p>
<p>&nbsp;</p>
<p>Y encontrar un balance entre las aspiraciones de los guerrilleros y las demandas de los ciudadanos –que tendrán que aprobar cualquier eventual acuerdo en un referéndum– supondrá un importante desafío para el presidente reelecto.</p>
<p>&nbsp;</p>
<p>"Hay que recordar que según las encuestas el 73% de la población dice que no está de acuerdo con que la FARC participe en política y el 83% que deben pagar cárcel todos", le dijo a BBC Mundo Jorge Restrepo, director del Centro de Recursos para el Análisis de Conflictos (CERAC).</p>
<p>&nbsp;</p>
<p>"Así que el principal reto de Santos será culminar el proceso de paz y sobre todo poder llevarlo de manera exitosa a las urnas", agregó.</p>
<p>&nbsp;</p>
<p>Según Restrepo, para su campaña Zuluaga intentó interpretar ese sentir de la mayoría de los colombianos, que le respondieron con el 45% de los sufragios.</p>
<p>&nbsp;</p>
<p>Y la presencia de una fuerte bancada de oposición en el congreso -encabezada por el expresidente Álvaro Uribe, el principal valedor de Zuluaga- tampoco facilitará las cosas en el camino de la negociación.</p>
<p>&nbsp;</p>
<p>El mandatario reelecto seguramente encontrará ahí un importante obstáculo a la hora de intentar aprobar las reformas necesarias para la implementación de un eventual acuerdo con la guerrilla.</p>
<p>&nbsp;</p>
<p>"Si llega a un acuerdo con las FARC que en concepto del uribismo tuviera un alto grado de impunidad, la inseguridad política y jurídica del proceso (de paz) será altísima", advirtió Rafael Guarín, una analista cercano al expresidente Uribe.</p>
<p>&nbsp;</p>
<p>Y Guarín también recordó que el acuerdo no sólo tiene que satisfacer a los colombianos, sino también cumplir con las demandas del Derecho Internacional a la hora de garantizar que los crímenes de lesa humanidad y otras violaciones flagrantes de los derechos humanos no queden en la impunidad.</p>
<p>&nbsp;</p>
<p>Pero en su discurso celebratorio Santos dijo haber recibido el mensaje de las urnas: "Esta no será una paz con impunidad, está será una paz justa", prometió.</p>
<p>&nbsp;</p>
<p>Y también le pidió a las FARC y al ELN escuchar el mensaje que les mandaban los votantes: que ya había llegado la hora para poner fin al conflicto, con seriedad y decisión.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>UN PARLAMENTO DIFERENTE</p>
<p>&nbsp;</p>
<p>La presencia en el parlamento de un importante bancada de oposición de derecha –beligerante y disciplinada como seguramente será la que encabezará Uribe– también marcará una importante diferencia con respecto al primer mandato de Santos. Y muy probablemente tendrá consecuencias más allá del proceso de paz.</p>
<p>&nbsp;</p>
<p>"Si bien Santos va a tener mayoría, no va a ser tan grande como la que tuvo en su período anterior", le dijo a BBC Mundo Marcela Prieto, directora del Instituto de Ciencias Políticas, un centro de pensamiento basado en Bogotá.</p>
<p>&nbsp;</p>
<p>"Va a tener la oposición del Centro Democrático y además alguna parcial del (izquierdista) Polo Democrático y alguna parcial del Partido Conservador. Y ellos se pueden unir en temas estratégicos y convertirse en una piedra en el camino", explicó Prieto.</p>
<p>&nbsp;</p>
<p>Aunque para la analista la existencia de una mayor oposición en el parlamento no es necesariamente mala para la democracia ni el país.</p>
<p>&nbsp;</p>
<p>"No es normal tener el 85% de la coalición gubernamental en el congreso. Y se necesita debate legislativo. Mi único temor es que el uribismo se atraviese como una vaca muerta en el camino a todo lo que presente el gobierno solo por simplemente ser oposición", le dijo a BBC Mundo.</p>
<p>&nbsp;</p>
<p>Aunque Restrepo espera que la tradición política colombiana de concertación termine imponiéndose en la mayoría de los temas no vinculados al proceso de paz.</p>
<p>&nbsp;</p>
<p>"Yo me atrevo a decir que Colombia es un país que se reconcilia muy rápidamente después de una contienda electoral, que no traslada a la labor de gobierno la oposición fuerte y retrechera de la campaña", le dijo el director del CERAC a BBC Mundo.</p>
<p>&nbsp;</p>
<p>"Colombia no es un país de gobiernos radicales y de oposición extrema", aseguró el analista. Pero no sin antes reconocer el riesgo de que se intente construir una más solida mayoría parlamentaria "sobre la base del clientelismo, lo que resultaría muy costoso para el país".</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>¿GIRO A LA IZQUIERDA?</p>
<p>&nbsp;</p>
<p>El estado de la economía, de hecho, también es una de las preocupaciones de Restrepo, quien cree que el manejo de la misma podría convertirse también en uno de los grandes desafíos del segundo gobierno de Santos.</p>
<p>&nbsp;</p>
<p>"Creo que ya estamos viendo señales fuertes de que el precio de los productos básicos se están cayendo y van a empezar a golpear la reducción de la capacidad exportadora de Colombia", le dijo a BBC Mundo.</p>
<p>&nbsp;</p>
<p>Y Restrepo no descarta que en ese, y otros temas, Santos pueda moverse un poco más hacia la izquierda. Pero no significativamente. Y con una importante salvedad</p>
<p>&nbsp;</p>
<p>"Creo que no va a ser un gobierno progresista en materia de iniciativa parlamentaria, dado el peso que el Centro Democrático va a tener en el congreso, pero en materia del ejecutivo sí va a serlo, porque básicamente quienes lo acompañaron en la reelección fueron los partidos de centro izquierda," explicó el analista.</p>
<p>&nbsp;</p>
<p>Y Prieto opina algo parecido: "La reelección siempre hace que tu primer mandato sea muy complaciente, y también hemos visto eso como característica de la personalidad y el talante de Santos", le dijo a BBC Mundo.</p>
<p>&nbsp;</p>
<p>"Pero en el segundo puede arriesgar más capital político. Aunque tendrá que hacer un balance muy bueno porque efectivamente va a tener una situación difícil en el congreso", agregó.</p>
<p>&nbsp;</p>
<p>Y ahí también están los compromisos adquiridos durante la cerrada campaña para lograr la reelección.</p>
<p>&nbsp;</p>
<p>"Puede que Santos llegue bastante amarrado con compromisos con la izquierda, aunque al mismo tiempos ellos han sido claros en decir que la suya era una alianza por la paz y que en el resto de temas seguirán haciendo oposición", dijo la directora del Instituto de Ciencia Política.</p>
<p>&nbsp;</p>
<p>En su discurso final, Santos le agradeció por nombre a casi todos los líderes de izquierda que lo apoyaron.</p>
<p>&nbsp;</p>
<p>Pero tal vez habrá que esperara hasta el nombramiento del nuevo gabinete de gobierno para hacerse una mejor idea de cuánto y en qué sentido podría cambiar el presidente reelecto, quien tendrá un nueva oportunidad para convertir su sueño de pasar a la historia como el presidente de la paz.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Foto: ICP Colombia</p>
<p>Fueten: ICP Colombia</p>
<p>Autor: Marcela Prieto Botero</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/395-celebrating-the-2014-tax-freedom-day-tfd-in-porto-alegre-brazil-–-10th-edition">
			&laquo; Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/398-tiempo-de-canallas-fusilamiento-al-amanecer">
			Tiempo de canallas, fusilamiento al amanecer &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/396-colombia-da-voto-de-confianza-a-santos-y-al-proceso-de-paz#startOfPageId396">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:67:"Colombia da voto de confianza a Santos y al proceso de paz - Relial";s:11:"description";s:155:"El presidente logró imponerse a su contrincante Iván Zuluaga en la segunda vuelta de las presidenciales. El resultado se interpreta como un importante...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:58:"Colombia da voto de confianza a Santos y al proceso de paz";s:6:"og:url";s:128:"http://www.relial.org/index.php/productos/archivo/actualidad/item/396-colombia-da-voto-de-confianza-a-santos-y-al-proceso-de-paz";s:8:"og:title";s:67:"Colombia da voto de confianza a Santos y al proceso de paz - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/18bdd04e288fc232234be2fb5ea8bf38_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/18bdd04e288fc232234be2fb5ea8bf38_S.jpg";s:14:"og:description";s:155:"El presidente logró imponerse a su contrincante Iván Zuluaga en la segunda vuelta de las presidenciales. El resultado se interpreta como un importante...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:58:"Colombia da voto de confianza a Santos y al proceso de paz";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}