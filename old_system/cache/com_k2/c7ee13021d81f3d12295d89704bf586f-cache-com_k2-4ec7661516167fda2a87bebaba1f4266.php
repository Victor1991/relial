<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17007:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId447"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Declaración de RELIAL sobre Derechos Humanos
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/be76d1a55ee5ffb3b2dc895570c95b36_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/be76d1a55ee5ffb3b2dc895570c95b36_XS.jpg" alt="Declaraci&oacute;n de RELIAL sobre Derechos Humanos" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><span style="font-size: 10pt;"><span size="2">El evento en conmemoración a los 10 años de RELIAL &nbsp;-una década traducida en compromiso, entrega y salvaguardia de la democracia liberal, la responsabilidad individual, la primacía del Estado de derecho y el impulso a la economía de mercado- fue, además un acontecimiento particularmente simbólico pues, en el marco de un ejercicio pluralista y democrático, aprobamos la Declaración de RELIAL sobre los Derechos Humanos</span>.</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	<br /></span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">Frente a la crítica situación por la que atraviesa la región en materia de derechos humanos, durante 2014, la Red Liberal de América Latina, RELIAL, impulsó diversos esfuerzos en defensa y promoción de este tema.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Siendo una prioridad en la agenda de la Red, en mayo del presente año, se llevó a cabo un primer grupo de trabajo concentrado en debatir ¿qué defendemos los liberales? y desarrollar un proyecto que postule, en detalle, un posicionamiento liberal respecto a la aplicación y respeto por los derechos fundamentales que permiten a cada persona la posibilidad de desarrollarse libremente.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Del grupo de trabajo, posteriores análisis y revisiones, resultó un documento preliminar "Declaración de RELIAL sobre los Derechos Humanos", el cual fue sometido a la observación de los miembros plenos de la Red para su posterior aprobación, por votación, en lo que fue el Congreso y Aniversario de RELIAL en Panamá.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">De esta manera, el evento en conmemoración a los 10 años de RELIAL &nbsp;-una década traducida en compromiso, entrega y salvaguardia de la democracia liberal, la responsabilidad individual, la primacía del Estado de derecho y el impulso a la economía de mercado- fue, además un acontecimiento particularmente simbólico pues, en el marco de un ejercicio pluralista y democrático, aprobamos la Declaración de RELIAL sobre los Derechos Humanos.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p style="text-align: center;"><span style="font-size: 10pt;">&nbsp;</span></p>
<p style="text-align: center;"><span style="font-size: 10pt;">"<strong>Declaración de RELIAL sobre los Derechos Humanos"</strong></span></p>
<p style="text-align: center;"><span style="font-size: 10pt;">Aprobada durante el Congreso y Aniversario de RELIAL</span></p>
<p style="text-align: center;"><span style="font-size: 10pt;">16 de noviembre de 2014, Ciudad de Panamá</span></p>
<p><strong><span style="font-size: 10pt;">Los principios deben convertirse en acciones</span></strong></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">La Red Liberal de América Latina surgió hace una década para promocionar la libertad en la región de acuerdo con nuestra visión moral de la persona humana y del rol de la sociedad civil y del Estado.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">En consecuencia, estamos decididos a promover varios instrumentos para la defensa de los derechos humanos, basados en los siete principios aprobados como señas de identidad de nuestra coalición de organizaciones liberales y las consideraciones sobre la cohesión social propia de una sociedad avanzada.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Defensa de la democracia liberal. El modelo de Estado en el que creemos hoy está bajo ataque. Los gobiernos autoritarios y los del llamado Socialismo del Siglo XXI niegan las virtudes de la democracia liberal, con su respeto por la separación de poderes, por las libertades, el pluralismo, la rendición de cuentas, la alternancia pacífica en el poder, y el resto de los atributos que caracterizan a las 25 naciones más prósperas y defensoras de los derechos humanos.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Creemos que una referencia idónea para fortalecer la democracia liberal y combatir a quienes la adversan es la Carta Democrática de la Organización de Estados Americanos (OEA) suscrita por todos los países miembros en Lima el 11 de septiembre de 2001.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Libertad y responsabilidad individual. Basados en la tradición clásica occidental, en el Bill of Rights británico, en la declaración de Independencia de Estados Unidos, y en la Declaración de los Derechos del Hombre y del Ciudadano proclamada por la Revolución Francesa, concordamos en que los seres humanos poseemos unos derechos naturales inalienables e imprescriptibles.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Simultáneamente, admitimos que derechos y deberes son las dos caras de la misma moneda. De la misma manera que aceptamos la existencia de derechos naturales, de donde provienen nuestros derechos humanos contemporáneos, admitimos que también existen deberes naturales que nos comprometen a luchar por la libertad&nbsp; de nuestros semejantes a procurar su propio bienestar, a tratar de conservar la paz y la concordia, y a practicar la tolerancia y el respeto por las ideas ajenas y la integridad de todas las personas, incluidas las que nos adversan.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Respeto a la propiedad privada. Sostenemos que poseer y disponer libremente de la riqueza creada por los seres humanos, no sólo es un derecho fundamental de las personas, sino que constituye el elemento básico fundamental de su prosperidad. Basta contrastar el desarrollo, las libertades y los derechos humanos de las sociedades donde prevalecen la propiedad y la iniciativa privadas con el de las sociedades colectivistas en las que el Estado posee los medios de producción –las dos Alemanias, las dos Coreas—para confirmar la superioridad total de las primeras.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Para defender vigorosamente este principio nos acogemos a la Declaración Universal de Derechos Humanos aprobada por la ONU en 1948, cuyo epígrafe 17 está conformado por dos partes claras y rotundas: 1) Toda persona tiene derecho a la propiedad, individual y colectivamente. 2) Nadie será privado arbitrariamente de su propiedad.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Promoción de un gobierno limitado. &nbsp;Concebimos un Estado administrado por un gobierno limitado constitucionalmente, siguiendo la tradición de la democracia liberal surgida tras el fin del Antiguo Régimen, la cual se propuso terminar con siglos de despotismo y colocando a los funcionarios del Estado bajo la autoridad de la ley y al servicio y respeto de los derechos individuales.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Este modelo de gobierno de atribuciones limitadas está definido en la mencionada Carta Democrática de la OEA y en los ideales fundacionales del movimiento de los derechos humanos, particularmente en aquellos que están reflejados en la Declaración Universal de Derechos Humanos (1948), la Convención Americana sobre Derechos Humanos (1969) y el Pacto Internacional de los Derechos Políticos y Civiles (1976).</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Impulso a la economía de mercado. Entendemos que la libre concurrencia en el mercado de los agentes económicos –productores, compradores, comerciantes—es la forma más eficiente de asignar recursos, compatible con la libertad y autonomía de los ciudadanos.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Rechazamos la planificación centralizada, por empobrecedora y violatoria de los derechos y libertades individuales, usualmente realizada por los burócratas y comisarios estatales, o la aberración mayor, el Estado-empresario, foco permanente de monopolios corruptos, ineficientes y clientelistas.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Primacía del Estado de Derecho. Estamos persuadidos de que la convivencia armónica, la solución de los inevitables conflictos y la prosperidad, aunque no se consigan de inmediato, se alcanzan mejor y de forma permanente dentro de las instituciones, que por la acción espasmódica de la violencia y del accionar caudillista.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Es nuestra intención coadyuvar todas las instancias que nos permite el Derecho Internacional así como a la opinión pública, para denunciar las violaciones de la ley, las imposiciones arbitrarias y la utilización de la fuerza en lugar de la razón.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Defensa de la paz. Creemos que lograr la paz es un objetivo loable, entendida no solamente como la ausencia de la guerra sino como la construcción de una sociedad que rechaza toda forma de violencia. Rechazamos la absurda hipótesis de que la guerra puede energizar la economía y favorecernos. La guerra es muerte y destrucción de bienes materiales. No hay forma de reemplazar a las personas muertas durante los conflictos, y los recursos empleados en reconstruir los bienes destruidos podían emplearse de una manera mucho más eficiente y razonable en crear nuevos bienes.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Es nuestro propósito denunciar cualquier intento de recurrir a la fuerza para solucionar los conflictos entre Estados o de los gobiernos contra ciudadanos desarmados que se manifiestan pacíficamente y dentro del Estado de derecho.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">La cohesión social y la lucha contra la pobreza. Como liberales, lucharemos para que todas las personas tengan las mismas oportunidades de perseguir los fines individuales que se propongan. Sabemos, además, que hay tres factores esenciales en la lucha contra la pobreza y la desigualdad: la educación, la práctica de una ciudadanía efectiva y la ética del trabajo, del ahorro y del esfuerzo propio. Mientras mejor educación tengan los ciudadanos, mejor será el tejido institucional, social y empresarial que sean capaces de crear, y mientras más valor le agreguen a la producción de bienes y servicios, mayores serán sus ingresos y, por lo tanto, menor será la desigualdad y la heterogeneidad de los niveles de vida como lo prueban la historia y la experiencia comparada.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">La razón de la concordia. Como liberales defendemos la concordia, la convivencia pacífica, la tolerancia y la actitud afable y respetuosa entre los ciudadanos de una comunidad, como requisitos para construir una sociedad democrática. Repudiamos el uso del gobierno para propagar el odio, el antagonismo, el enfrentamiento y la discriminación, que hace inviable la libertad y el desarrollo de las potencialidades de las personas.</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Libertad de expresión. Reconocemos la libertad de expresión –en todas sus formas y manifestaciones- como un derecho fundamental para que la persona pueda defender sus demás derechos. El primer objetivo de todos los gobiernos autoritarios es acabar con la libertad de expresión; la solución no radica en el control del estado de la libertad de expresión o los medios de comunicación sino en la crítica de los ciudadanos. Nos oponemos a que los estados dicten leyes que limiten la libertad de expresión. En su lugar, promovemos la creación de espacios para la crítica constructiva de los ciudadanos a los medios de comunicación.&nbsp; Adherimos a la Declaración de Chapultepec (1994)&nbsp; e instamos a los gobiernos a ratificar la misma.</span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="Declaraciyn_de_RELIAL_en_Derechos_Humanos_aprobada_en_PanamyA.pdf" href="/index.php/productos/archivo/comunicados/item/download/113_4e2931fb2a6a9f471253451899b6e802">Declaraciyn_de_RELIAL_en_Derechos_Humanos_aprobada_en_PanamyA.pdf</a>
			    			    <span>(449 K2_DOWNLOADS)</span>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/446-relial-por-el-ex-presidente-flores">
			&laquo; RELIAL por el ex presidente Flores		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/482-libertad-para-los-presos-políticos">
			LIBERTAD PARA LOS PRESOS POLÍTICOS &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/447-declaración-de-relial-sobre-derechos-humanos#startOfPageId447">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:54:"Declaración de RELIAL sobre Derechos Humanos - Relial";s:11:"description";s:155:"El evento en conmemoración a los 10 años de RELIAL &amp;nbsp;-una década traducida en compromiso, entrega y salvaguardia de la democracia liberal, la...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:45:"Declaración de RELIAL sobre Derechos Humanos";s:6:"og:url";s:122:"http://www.relial.org/index.php/productos/archivo/comunicados/item/447-declaraciÃƒÂ³n-de-relial-sobre-derechos-humanos";s:8:"og:title";s:54:"Declaración de RELIAL sobre Derechos Humanos - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/be76d1a55ee5ffb3b2dc895570c95b36_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/be76d1a55ee5ffb3b2dc895570c95b36_S.jpg";s:14:"og:description";s:159:"El evento en conmemoración a los 10 años de RELIAL &amp;amp;nbsp;-una década traducida en compromiso, entrega y salvaguardia de la democracia liberal, la...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:45:"Declaración de RELIAL sobre Derechos Humanos";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}