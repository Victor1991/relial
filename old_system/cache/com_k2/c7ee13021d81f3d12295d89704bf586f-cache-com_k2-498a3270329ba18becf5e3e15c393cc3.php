<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:19016:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId392"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/5ef17ed4d733d4dc3519b291889643fe_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/5ef17ed4d733d4dc3519b291889643fe_XS.jpg" alt="Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Análisis de los resultados de la primera vuelta de las elecciones presidenciales en Colombia, de las perspectivas abiertas para la segunda vuelta el próximo 15 de junio y del talante de los dos candidatos en contienda.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p><strong><br /></strong></p>
<p><strong>De la primera a la segunda vuelta</strong></p>
<p>&nbsp;</p>
<p>El próximo domingo 15 de junio se definirá, en una segunda vuelta, quién ejercerá la Presidencia de Colombia para el periodo 2014-2018. Los dos candidatos en contienda son, por un lado, el candidato-presidente Juan Manuel Santos (que se presenta a nombre de una coalición de Unidad Nacional liderada originalmente por el Partido Liberal, el Partido Social de Unidad Nacional y el Partido Cambio Radical); y por el otro, el ex ministro de Hacienda Oscar Iván Zuluaga, por el Partido Centro Democrático cuyo promotor y líder es el ex presidente Alvaro Uribe Vélez (quien gobernó entre 2002 y 2010, y quien se ha convertido en una de las figuras más prominentes de la oposición al actual gobierno de Juan Manuel Santos no obstante haber impulsado y respaldado su candidatura hace 4 años).</p>
<p>&nbsp;</p>
<p>Hace apenas un par de meses, pocos hubieran apostado por el triunfo de Oscar Iván Zuluaga en la primera vuelta. El candidato por el Centro Democrático ocupaba un modesto puesto en las encuestas, que a su vez registraban un liderazgo sostenido —aunque para nada contundente— del candidato-presidente Juan Manuel Santos era prácticamente indiscutido. Al lado de Zuluaga, y aún más lejos del candidato presidente, las demás candidaturas* parecían no despegar tampoco.</p>
<p>&nbsp;</p>
<p>Sin embargo, faltando menos de quince días para la elección, el candidato Zuluaga, aupado por el apoyo y la enorme popularidad del ex presidente Uribe Vélez, empezó a figurar en el primer lugar de los sondeos. Ello coincidió con un estancamiento de Santos y el significativo repunte de los otros candidatos.</p>
<p>&nbsp;</p>
<p>Estas tendencias fueron confirmadas en los comicios del 25 de mayo, tras unas agitadas semanas de debate electoral en el que no faltaron los escándalos y la guerra sucia entre las campañas de Santos y Zuluaga. De los casi 33 millones de ciudadanos habilitados para votar lo hizo tan sólo el 40% (una de las abstenciones más altas de tiempos recientes), quizá como muestra de hastío frente al tono que adquirió al final la campaña. En todo caso, Zuluaga logró hacerse con el primer puesto (29,25%) y le sacó al candidato-presidente Santos (25,69%) una ventaja de casi medio millón de votos. Los resultados obtenidos por el Partido Conservador y el Polo Democrático (cada con algo más del 15%) los posicionaron como fuerzas políticas clave de cara a la segunda vuelta, en la que también entrarán a jugar un papel importante los votantes del Partido Verde (8,28%), y acaso también los abstencionistas, dado el estrecho margen que separó a Santos y Zuluaga y que se mantiene —al modo de un empate técnico— en las encuestas realizadas desde entonces.</p>
<p>&nbsp;</p>
<p>Gráfico 1. Resultados de la primera vuelta, 25 de mayo de 2014</p>
<p><img src="images/Resultados25Mayo_chica.jpg" alt="Resultados25Mayo chica" width="448" height="264" /></p>
<p>&nbsp;</p>
<p>Fuente: Registraduría Nacional del Estado Civil, Colombia. Último boletín disponible en www.registraduria.gov.co</p>
<p><strong><br /></strong></p>
<p><strong><br /></strong></p>
<p><strong>Aspectos relevantes de la primera vuelta</strong></p>
<p>&nbsp;</p>
<p>• La significativa abstención. Como ya se señaló, la abstención registrada (60%) es superior al menos en 10 puntos al registro histórico (que desde 1991 ha oscilado entre 41% y 54%). Este incremento puede reflejar un creciente desencanto de la ciudadanía frente al sistema político en general, frente a la agenda que ofrecen los candidatos, frente al liderazgo que éstos representan o frente a la forma en que se desarrolló la campaña. En cualquier caso, es un indicador preocupante para una democracia como la colombiana, que aunque relativamente consolidada, es todavía muy superficial.</p>
<p>&nbsp;</p>
<p>• La estrecha diferencia entre los candidatos que pasan a segunda vuelta. Este resultado reñido —que podría repetirse en la segunda vuelta— refleja una creciente polarización política en Colombia alrededor del tema de las negociaciones con las Farc que vienen realizándose en La Habana (Cuba). Mientras que Santos ha proyectado toda su campaña en defensa de la posibilidad de un acuerdo de paz, Zuluaga ha enarbolado las banderas más críticas frente al proceso y llegó a anunciar —aunque luego matizara ambiguamente su posición— que de resultar elegido suspendería inmediatamente los diálogos. Esta polarización tendrá un enorme impacto, cualquiera el candidato finalmente elegido Presidente, en la estrategia que deba adoptar en cuanto al proceso con las Farc, en la continuación e implementación de la negociación o en su suspensión o terminación.</p>
<p>&nbsp;</p>
<p>• Un complicado escenario para el candidato presidente Santos. No obstante haber pasado a la segunda vuelta, Santos puede considerarse uno de los grandes perdedores de la primera. En efecto, teniendo en cuenta su condición de presidente en ejercicio (incumbent), y que su plataforma se basa en la promesa de lograr la paz al cabo de varias décadas de conflicto armado interno, el balance resulta desalentador. El Gobierno Santos no ha logrado comunicar a la ciudadanía un mensaje contundente sobre las perspectivas de alcanzar la paz en Colombia, ni sobre las ventajas de la solución negociada, no obstante logros innegables no sólo en el avance sin precedentes de la negociación sino en la adopción de políticas públicas orientadas a preparar al país para el posconflicto (por ejemplo en cuanto tiene que ver con la reparación de las víctimas del conflicto armado).</p>
<p>&nbsp;</p>
<p>• La paz, gran diferenciador. Finalmente, los resultados parecen confirmar que el tema de la paz es el que marca el contraste más notorio en el actual debate político colombiano. Los candidatos podían clasificarse y diferenciarse en función de su posición frente al actual proceso de paz con las Farc en La Habana. Y finalmente, este ha sido uno de los elementos que mayor peso ha jugado a definir los alineamientos y las convergencias posteriores a la primera vuelta.</p>
<p><strong><br /></strong></p>
<p><strong><br /></strong></p>
<p><strong>La segunda vuelta en perspectiva</strong></p>
<p>&nbsp;</p>
<p>• El "techo" de atracción de la intención de voto. Santos parece tener un mayor potencial para atraer votos nuevos, procedentes de más diversos sectores del espectro político (tanto de derecha como de izquierda, y de los llamados "independientes"). Sin embargo, no hay que olvidar que Colombia es un país tradicionalmente de derecha; y que últimamente una derecha cada vez más radical parece haberse ido consolidando sobre la base de la crítica al proceso de paz con las Farc y la denuncia de la inminente amenaza de que Colombia caiga presa del "castro-chavismo**" .</p>
<p>&nbsp;</p>
<p>• Convergencias políticas. Como era de esperarse, un importante sector del Partido Conservador, encabezado por la candidata Ramírez, adhirió explícitamente a Zuluaga para la segunda vuelta, en una jugada que le ha servido a Zuluaga para presentar un discurso ligeramente más moderado (en la forma, aunque no en el fondo) sobre el proceso de paz. Otro sector de ese mismo partido prefirió unirse a la campaña de Santos (orientado sobre todo por expectativas puramente burocráticas). Entre tanto, tanto el Polo Democrático como el Partido Verde prefirieron dejar a sus votantes "en libertad de elegir", pero varios de sus dirigentes —incluyendo la ex candidata presidencial del Polo Democrático— han anunciado su voto por Santos como un voto en apoyo al proceso de paz, sin que ello implique un apoyo más amplio a su programa de gobierno.</p>
<p>&nbsp;</p>
<p>• La cantera abstencionista. Por último, ambos candidatos podrían intentar obtener votos entre los abstencionistas no habituales, es decir, entre aquellos ciudadanos que usualmente vota pero que por alguna razón no lo hizo el 25 de mayo. Sin embargo, esta parece ser una veta difícil de explotar. El hecho de que ese fin de semana sea el primer partido de Colombia en la Copa Mundial de Fútbol y se celebre el día del padre, puede llegar a tener un impacto importante en la afluencia de votantes.</p>
<p>&nbsp;</p>
<p>Gráfico 2. La tarjeta electoral que se usará el 15 de junio en la segunda vuelta.</p>
<p><img src="images/Tarjeton2davuelta_chica.jpg" alt="Tarjeton2davuelta chica" width="448" height="318" /></p>
<p>Fuente: Registraduría Nacional del Estado Civil, Colombia.</p>
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>Factores que definirán la segunda vuelta</strong></p>
<p>&nbsp;</p>
<p>• Los posibles avances o retrocesos en el proceso de paz no deben ser subestimados. Pese a que un segmento de la opinión pública considera que este tema no es prioritario en la agenda nacional, se ha convertido en el factor diferenciador entre ambos candidatos. El anuncio de un acuerdo sobre los principios rectores para la discusión del tema de víctimas, hecho el 7 de junio, constituye un avance histórico en la búsqueda de la terminación negociada del conflicto armado; y se produce al tiempo que el candidato Zuluaga advierte que no considera vinculante nada de lo ya acordado en La Habana y que por lo tanto procederá a revisar integralmente el proceso en caso de resultar elegido —lo cual implica, de algún modo, poner en entredicho cuanto se ha logrado hasta ahora.</p>
<p>&nbsp;</p>
<p>• Otro elemento a tener en cuenta es la intensidad de la confrontación personal entre los candidatos. La primera vuelta fue precedida de una seguidilla de episodios de guerra sucia, propaganda negativa y denuncias de una campaña contra la otra. Además de hacerle un inmenso daño a la democracia, nuevos enfrentamientos de este tipo podrían generar reacciones adversas ante un proceso electoral que ya se encuentra enturbiado por la polarización.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Conclusión: Dos talantes diferentes para un gobierno lleno de desafíos</strong></p>
<p>&nbsp;</p>
<p>No será fácil gobernar Colombia durante los próximos cuatro años, para ninguno de los dos candidatos, cualquiera que de ellos resulte elegido. La democracia colombiana se enfrentará, sin lugar a dudas, a excepcionales desafíos que pondrán a prueba las instituciones, el Estado de Derecho, la separación y equilibrio de poderes; y naturalmente a los retos particulares relacionados con el conflicto armado interno, con su terminación y el paso al posconflicto, o con su eventual recrudecimiento.</p>
<p>&nbsp;</p>
<p>Santos ha demostrado ser un hombre de talante liberal. Ha impulsado la internacionalización de la economía colombiana (impulsando por ejemplo la Alianza del Pacífico), un nuevo modelo de relaciones internacionales (más cosmopolita, menos confrontacional que el de su predecesor), y además ha restablecido una relación más armónica con los demás poderes del Estado.</p>
<p>&nbsp;</p>
<p>Zuluaga quizá sea liberal en lo económico, tanto como Santos. Pero no lo es en lo político, ni en su concepción de la escena internacional, ni en su idea del régimen de derechos y libertades, y suscribe, como su mentor Uribe, la idea de que existe un "Estado de Opinión" superior al Estado de Derecho. Las reservas de su movimiento político frente al proceso de paz de La Habana no son reservas frente al proceso como tal, sino frente a la negociación como forma de terminar el conflicto. La idea de que una derrota militar de la guerrilla no sólo es posible sino deseable y preferible a cualquier otra forma de terminación del conflicto (cuya existencia además niega expresamente), está profundamente arraigada entre sus partidarios, y pesará enormemente en abordaje de estos asuntos.</p>
<p>&nbsp;</p>
<p>Cualquiera de los dos encontrará un país polarizado, un Congreso dividido, y un clima político enrarecido como consecuencia de la forma en que se ha desarrollado la campaña. Quizá el proceso de paz con las Farc tenga ya la inercia suficiente para mantenerse sin importar el resultado; pero es evidente que un triunfo de Zuluaga y del uribismo supondría un punto de quiebre con implicaciones muy serias y difíciles de prever. Y en cualquier caso, el Presidente que resulte elegido tendrá que liderar un proceso complejo, en el cual el país responda las preguntas hoy por hoy fundamentales: ¿qué tipo de paz quiere? ¿qué tanto está dispuesto a asumir los costos del posconflicto? ¿qué tipo de institucionalidad quiere construir para mantener la paz luego de terminado el conflicto, evitar la reproducción de la violencia y profundizar la democracia?</p>
<p>&nbsp;</p>
<p>En el fondo, Colombia tomará una decisión cuyo impacto se prolongará más allá del próximo cuatrenio. Tal vez el 15 de junio Colombia esté eligiendo su destino por la próxima década o incluso más todavía.</p>
<p>&nbsp;</p>
<p><span style="font-size: 8pt;">*<span style="line-height: 115%; font-family: 'Times New Roman', serif;">Martha Lucía Ramírez (Partido Conservador, de centro derecha), Clara López (Polo Democrático, de izquierda), y Enrique Peñalosa (Partido Verde, de una identidad ideológica poco definida).</span></span></p>
<p><span style="font-size: 8pt;"><span style="line-height: 115%; font-family: 'Times New Roman', serif;">**</span><span style="font-family: 'Times New Roman', serif;">La derecha más radical, liderada por el ex presidente Uribe denomina así la posibilidad de que en Colombia se implante un régimen afín al “Socialismo del siglo XXI” liderado en su momento por el presidente Venezolano Hugo Chávez e inspirado en el Castrismo cubano.</span></span></p>
<p><span style="font-size: 8pt;"><span style="font-family: 'Times New Roman', serif;"><br /></span></span></p>
<p><span style="font-size: 8pt;"><span style="font-family: 'Times New Roman', serif;"><br /></span></span></p>
<p><span style="font-family: 'times new roman', times;">Autor: &nbsp;<strong style="text-align: justify;">Andrés MOLANO-ROJAS,&nbsp;</strong><span style="text-align: justify;">Profesor de Relaciones Internacionales y Ciencia Política de la Universidad del Rosario.&nbsp; Director Académico del Observatorio de Política y Estrategia en América Latina (OPEAL) del Instituto de Ciencia Política “Hernán Echavarría Olózaga”.&nbsp; Bogotá, Colombia.</span></span></p>
<p><span style="font-family: 'times new roman', times; text-align: justify;">Fuente: Texto proporcionado por el autor</span></p>
<p><span style="font-family: 'times new roman', times; text-align: justify;">Foto: Instituto de Ciencia Política "Hernán Echavarría Olózaga". Bogotá, &nbsp;Colombia</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor">
			&laquo; No más barreras a la creación de valor		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa">
			La inmensa tarea de los reyes de España &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018#startOfPageId392">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:88:"Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018 - Relial";s:11:"description";s:154:"Análisis de los resultados de la primera vuelta de las elecciones presidenciales en Colombia, de las perspectivas abiertas para la segunda vuelta el p...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:79:"Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018";s:6:"og:url";s:143:"http://www.relial.org/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018";s:8:"og:title";s:88:"Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018 - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/5ef17ed4d733d4dc3519b291889643fe_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/5ef17ed4d733d4dc3519b291889643fe_S.jpg";s:14:"og:description";s:154:"Análisis de los resultados de la primera vuelta de las elecciones presidenciales en Colombia, de las perspectivas abiertas para la segunda vuelta el p...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:79:"Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}