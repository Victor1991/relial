<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6373:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId33"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	CATILINARIAS
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f7a0a54c92471ac4480e727e4ccf93df_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f7a0a54c92471ac4480e727e4ccf93df_XS.jpg" alt="CATILINARIAS" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>	  </div>
	  	  
		<div class="clr"></div>

	  	  <!-- Item extra fields -->
	  <div class="itemExtraFields">
	  	<h3>Info</h3>
	  	<ul>
									<li class="even typeTextfield group1">
								<span class="itemExtraFieldsLabel">Titulo:</span>
				<span class="itemExtraFieldsValue">CATILINARIAS</span>
							</li>
												<li class="odd typeTextfield group1">
								<span class="itemExtraFieldsLabel">Autor(es):</span>
				<span class="itemExtraFieldsValue">Hugo Nelson Vera</span>
							</li>
												<li class="even typeTextfield group1">
								<span class="itemExtraFieldsLabel">Editorial:</span>
				<span class="itemExtraFieldsValue">FFN- RELIAL</span>
							</li>
												<li class="odd typeTextfield group1">
								<span class="itemExtraFieldsLabel">Fecha de publicación:</span>
				<span class="itemExtraFieldsValue">Sept 2011</span>
							</li>
												<li class="odd typeTextfield group1">
								<span class="itemExtraFieldsLabel">Idioma:</span>
				<span class="itemExtraFieldsValue">Español</span>
							</li>
												<li class="even typeTextfield group1">
								<span class="itemExtraFieldsLabel">Descripción:</span>
				<span class="itemExtraFieldsValue">La suma de la importancia de los valores y derechos de los seres humanos como tales, dados por su esencia, encuentra cabida y plena redundancia en los escritos del Abogado Hugo Vera Ojeda. Encuentro palpable en cada una de las redacciones la libertad en todos sus aspectos como principal eje de la vida de las personas en cada artículo se demuestra que este valor es primordial para el desarrollo pleno de cada ser humano en particular y de las sociedades en general. Los valores del liberalismo pretenden que los miembros de una sociedad estén más comprometidos con su propia realidad y que utilicen la racionalidad para obtener el bien común y así construir una sociedad sólida y basada en el respeto de todos sus integrantes. Las líneas expresadas por el autor denotan la problemática que abarca todos los ámbitos de la vida cotidiana, por lo que mediante el análisis de las situaciones ilustradas por el mismo, el lector podrá hacer una reflexión acabada del panorama que se nos presenta y de esa manera, desde el punto de vista particular de cada quien, y desde el lugar que cada uno ocupa en la sociedad, se podrán llevar a cabo tareas que busquen y pretendan subsanar las carencia que atañen a nuestra sociedad y así, con un trabajo mancomunado mejorar cada espacio de la sociedad en particular y en consecuencia, el bienestar podrá ser generalizado y a nivel global. Por el trabajo realizado, tarea llevada a cabo con el máximo interés por mejorar y revalorizar al ser humano y a la compleja sociedad en que vivimos, cabe felicitar y alentar al Abogado Hugo Vera Ojeda a seguir adelante con esta tan ardua misión asumida.</span>
							</li>
									</ul>
	    <div class="clr"></div>
	  </div>
	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="CATILINARIAS_HUGO_VERA_OJEDA.pdf" href="/index.php/biblioteca/item/download/15_86c1795eadf6c4879f3793760584d71e">CATILINARIAS_HUGO_VERA_OJEDA.pdf</a>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/33-catilinarias#startOfPageId33">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:21:"CATILINARIAS - Relial";s:11:"description";s:1:" ";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:12:"CATILINARIAS";s:6:"og:url";s:63:"http://www.relial.org/index.php/biblioteca/item/33-catilinarias";s:8:"og:title";s:21:"CATILINARIAS - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f7a0a54c92471ac4480e727e4ccf93df_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f7a0a54c92471ac4480e727e4ccf93df_S.jpg";s:14:"og:description";s:1:" ";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:20:"Políticas Liberales";s:4:"link";s:63:"/index.php/biblioteca/itemlist/category/13-políticas-liberales";}i:2;O:8:"stdClass":2:{s:4:"name";s:12:"CATILINARIAS";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}