<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8281:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId449"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Sobre el Congreso y Aniversario de RELIAL
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/da89514e409822180ac867ab6712269d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/da89514e409822180ac867ab6712269d_XS.jpg" alt="Sobre el Congreso y Aniversario de RELIAL" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de María Fernanda Valencia</p>
<p>&nbsp;</p>
<p>Del 14 al 17 de noviembre, Panamá fue sede de la celebración del congreso anual y décimo aniversario de la Red Liberal de América Latina (Relial), y la Fundación Libertad tuvo el honor de dar la bienvenida y ser anfitrión de tan importante evento.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El congreso contó con la participación de partidos políticos y centros de pensamiento liberal de diversos países y con personalidades, como Gesine Meissner, diputada del Partido Liberal, en el Parlamento Europeo; Juli Minoves, presidente de la Internacional Liberal; Ricardo López Murphy, presidente de la Red Liberal de América Latina y exministro de Economía de Argentina; Carlos Alberto Montaner, escritor y periodista cubano; y Otto Guevara, diputado de la Asamblea de Costa Rica, entre otros.</p>
<p>&nbsp;</p>
<p>Se abordaron diversos temas que hoy día afectan a los países de Latinoamérica, como el populismo, abusos de poder, violación de los derechos humanos, corrupción inminente y cómo encarar estos escenarios. Se llegó a la conclusión de que en un Estado menos intervencionista, el impulso de una economía de mercado y su libre concurrencia podrán remediar dichos problemas.</p>
<p>&nbsp;</p>
<p>Durante el evento se hizo hincapié en que uno de los objetivos es lograr la eliminación de la pobreza, mediante la educación, la apertura de los mercados y, sobre todo, la ética del esfuerzo propio. No obstante, estos se ven amenazados por las medidas populistas que han implementado diversos mandatarios de la región. En Brasil, por ejemplo, cada vez crece más la política de subsidios, una de ellas es el programa Bolsa Familia, que ha causado que "muchos ciudadanos prefieran no trabajar para no perderlo", explicó Ricardo Gomes, director del Instituto de Estudos Empresariais.</p>
<p>&nbsp;</p>
<p>En la entrevista que se le hizo al exfiscal de la Corte Penal Internacional, Luis Moreno Ocampo, se destacó que la corrupción, el mercantilismo y los sobornos son los agentes que destruyen a la economía de mercado y que es de imperiosa la necesidad un gobierno limitado, puesto que los individuos son conscientes de la perversidad con la que operan y otorgan privilegios.</p>
<p>&nbsp;</p>
<p>Según Otto Guevara, Costa Rica ha avanzado en materia de libertades, por las influencias que ha tenido el Partido Liberal en ese país, pues ya se han discutido en la Asamblea temas como la reducción del excesivo gasto público, la apertura del mercado eléctrico y la dolarización.</p>
<p>&nbsp;</p>
<p>Durante la jornada no se pudo dejar a un lado la conmemoración de los 25 años de la caída del Muro de Berlín, también llamado "El Muro de la Vergüenza", cuyo objetivo no era más que evitar que quienes habitaban en la Alemania del Este, escaparan hacia la Alemania Occidental, huyendo de las crueldades del socialismo. En la Alemania del Este, las personas hacían enormes filas para buscar alimentos básicos, por lo general siempre escasos; hoy día este escenario tristemente se repite en países como Venezuela, donde escasean los productos de primera necesidad. Lo anterior nos lleva a concluir que aún existen estos "muros" en Latinoamérica y que es importante reconocerlos y enfrentarlos con ideas acordes a los principios liberales que han hecho prósperas a naciones como Hong Kong, Singapur o Nueva Zelanda.</p>
<p>&nbsp;</p>
<p>Pese a que en el mundo aún hay muros y barreras, existen también estructuras como el Canal de Panamá, que abre sus puertas y, por ello, constituye un símbolo de libertad. El Canal, contrario al muro, representa un paraíso para los liberales.</p>
<p>&nbsp;</p>
<p>Es un compromiso para todos aquellos que creen con fervor en la libertad individual y en el respeto a la propiedad, seguir promoviendo las iniciativas que nos permitan conformar un estado de derecho, y de esto surge grandes interrogantes: ¿Habrá cabida en Panamá para un partido político liberal? ¿Habrá en algún momento un partido político que condene prácticas populistas como los subsidios?</p>
<p>&nbsp;</p>
<p>La respuesta concreta aún no la tenemos, sin embargo, no podemos descartar que una organización política de este tipo brindaría más opciones a los panameños, frente a la política tradicional que hoy conocemos.</p>
<p>&nbsp;</p>
<p>Autor: María Fernanda Valencia</p>
<p>Fuente: <a href="http://www.prensa.com">www.prensa.com</a></p>
<p>Foto: Fundación Libertad Panamá</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/445-el-terremoto-brasilero">
			&laquo; El Terremoto Brasilero		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/457-bachelet-de-espaldas-al-chile-moderno">
			Bachelet de espaldas al Chile moderno &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/449-sobre-el-congreso-y-aniversario-de-relial-en-panamá#startOfPageId449">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:50:"Sobre el Congreso y Aniversario de RELIAL - Relial";s:11:"description";s:158:"En la opinión de María Fernanda Valencia &amp;nbsp; Del 14 al 17 de noviembre, Panamá fue sede de la celebración del congreso anual y décimo aniversar...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:41:"Sobre el Congreso y Aniversario de RELIAL";s:6:"og:url";s:91:"http://relial.org/index.php/productos/archivo/opinion/item/449-no-trabajar-para-no-perderlo";s:8:"og:title";s:50:"Sobre el Congreso y Aniversario de RELIAL - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/da89514e409822180ac867ab6712269d_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/da89514e409822180ac867ab6712269d_S.jpg";s:14:"og:description";s:162:"En la opinión de María Fernanda Valencia &amp;amp;nbsp; Del 14 al 17 de noviembre, Panamá fue sede de la celebración del congreso anual y décimo aniversar...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:41:"Sobre el Congreso y Aniversario de RELIAL";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}