<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10985:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId229"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Ya no nos podemos conformar con menos
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7e64c4d2a4a242251ffdaa790b21fa01_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7e64c4d2a4a242251ffdaa790b21fa01_XS.jpg" alt="Ya no nos podemos conformar con menos" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>"Ya no nos podemos conformar con menos"</p>
<p>&nbsp;</p>
<p>Comentario sobre el seminario from Government to Good Governance</p>
<p>&nbsp;</p>
<p>Por: Carolina Gómez, cuya participación en el seminario fue posible gracias al apoyo de Relial al Instituto de Ciencia Política (ICP) y a la Revista Perspectiva.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>El seminario sobre Buena Gobernanza que ofrece la Academia Internacional para el Liderazgo de la Fundación Friedrich Naumann ha sido una experiencia única para quienes hemos tenido la fortuna de asistir en su primera edición, que tuvo lugar entre el 20 de octubre y el 1 de noviembre de 2013, en Gummersbach, Alemania.</p>
<p><img style="float: right;" src="images/foto2.png" alt="" /></p>
<p>&nbsp;</p>
<p>Cuenta con todos los ingredientes que un programa intensivo sobre la materia debe tener: conferencias magistrales con todos los criterios teóricos necesarios para una completa comprensión de los conceptos, visitas de interés relacionado como el Ministerio de Cooperación Internacional y antigua sede de la Cancillería, la sede del Parlamento de Sajonia, y el encuentro con Torsten Herbst, secretario del Partido Democrático Liberal de Sajonia, entre otras. Además, visitas de interés cultural para comprender mejor el contexto del país anfitrión, trabajos en grupo y espacios para conocer y relacionarse con los demás participantes, líderes liberales provenientes de distintos países de todo el mundo.</p>
<p>&nbsp;</p>
<p>Y es precisamente el contexto lo que refuerza la calidad de lo aprendido. Me cuesta trabajo pensar en un ambiente más propicio que la Academia y un país con ejemplos más modernos y completos en Buena Gobernanza. Si bien en casi todos los continentes encontramos iniciativas exitosas a partir de la aplicación de soluciones novedosas, suelen ser referencias muy puntuales. Alemania está bien posicionada en todos los indicadores de Buena Gobernanza disponibles actualmente, y como dijo mi compañero Shahir George de Egipto: "Nos enseña el camino de lo que podemos aspirar como naciones... Ya no nos podemos conformar con menos".</p>
<p>&nbsp;</p>
<p>Un buen espejo para mirar, comparar y obtener ideas útiles para mejorar nuestras respectivas realidades.</p>
<p>&nbsp;</p>
<p>En conclusión fue una experiencia increíble. Con gusto volvería a la que llaman "la cárcel más bonita del mundo".</p>
<p>&nbsp;</p>
<p>PD. El equipo de Latinoamérica (conformado por 4 representantes de México, Brasil, Argentina y Colombia) ganó el primer puesto en el concurso sobre soluciones prácticas para mejorar la gobernanza en nuestros países.</p>
<p>&nbsp;</p>
<p>Algunas fotografías en la siguiente página.</p>
<p>&nbsp;</p>
<p><span style="font-size: 12pt; font-family: Cambria, serif;"><br /><img style="float: left;" src="images/foto1.png" alt="foto1" width="205" height="250" /></span><span style="font-size: 10pt;"><span style="font-family: Cambria, serif;">Hugo Neto de Brasil, Pablo Benitez de Argentina, Carolina Gómez de Colombia y Oscar Márquez de México recibiendo el reconocimiento de manos de Katja Manuela Egger (moderadora del seminario) por </span><span style="font-family: Cambria, serif;">el primer puesto en el concurso sobre soluciones prácticas para mejorar la gobernanza en nuestros países.</span></span></p>
<p>&nbsp;</p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px;"><br /></span></p>
<p style="text-align: right;"><span style="font-size: 11px;">Visita al Parlamento de Sajonia<img style="float: right;" src="images/foto4.png" alt="" /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;"><br /></span></p>
<p><img style="float: left;" src="images/foto3.png" alt="" /></p>
<p><span style="font-size: 10pt;">&nbsp;<span style="text-indent: 35.4pt;">Encuentro con Torsten Herbst,</span></span></p>
<p><span style="text-indent: 35.4pt; font-size: 10pt;">secretario del Partido Democrático</span></p>
<p><span style="text-indent: 35.4pt; font-size: 10pt;">Liberal de Sajonia</span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;"><br /></span></p>
<p><span style="font-size: 11px; text-indent: 35.4pt;">fuente: Carolina Gomez exbecaria por parte de ICP</span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;">fotos: Propiedad de Carolina Gomez</span></p>
<p><span style="text-indent: 35.4pt; font-size: 11px;">texto: Carolina Gomez&nbsp;</span></p>
<p style="margin-left: 70.8pt; text-indent: 35.4pt;">&nbsp;</p>
<p style="margin-left: 70.8pt; text-indent: 35.4pt;">&nbsp;</p>
<p style="margin-left: 70.8pt; text-indent: 35.4pt;">&nbsp;</p>
<p style="margin-left: 70.8pt; text-indent: 35.4pt;">&nbsp;</p>
<p style="margin-left: 70.8pt; text-indent: 35.4pt;">&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo">
			&laquo; Dora de Ampuero, condecorada al Mérito Educativo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/237-relial-en-atención-y-solidaridad-con-venezuela">
			RELIAL en atención y solidaridad con Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos#startOfPageId229">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:46:"Ya no nos podemos conformar con menos - Relial";s:11:"description";s:153:"&quot;Ya no nos podemos conformar con menos&quot; &amp;nbsp; Comentario sobre el seminario from Government to Good Governance &amp;nbsp; Por: Carolina...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:37:"Ya no nos podemos conformar con menos";s:6:"og:url";s:103:"http://relial.org/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos";s:8:"og:title";s:46:"Ya no nos podemos conformar con menos - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/7e64c4d2a4a242251ffdaa790b21fa01_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/7e64c4d2a4a242251ffdaa790b21fa01_S.jpg";s:14:"og:description";s:169:"&amp;quot;Ya no nos podemos conformar con menos&amp;quot; &amp;amp;nbsp; Comentario sobre el seminario from Government to Good Governance &amp;amp;nbsp; Por: Carolina...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:37:"Ya no nos podemos conformar con menos";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}