<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8399:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId119"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Si la libertad de prensa no existiese, habría que inventarla
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/47e29f9fe96a1771642fb05ac8a8fd00_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/47e29f9fe96a1771642fb05ac8a8fd00_XS.jpg" alt="Si la libertad de prensa no existiese, habr&iacute;a que inventarla" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: justify;">En la opinión de Rodolfo Hernández</p>
<p style="text-align: justify;">El ser humano siempre ha tenido una aspiración válida: ser libre. Pero hechos los hombres, hechas las normas. Su manifestación más antigua la vemos reflejada en los diez mandamientos.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Hace casi 3.300 años, el Creador de todas las cosas había puesto pesos y contrapesos, porque Él, que se había arrepentido de crear al hombre por su mal comportamiento (Génesis) sabía que a la libre, el ser humano, en su apetito insaciable, es capaz de cualquier cosa.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p style="text-align: justify;">Mucha agua ha corrido desde entonces y nadie podría dudar de que, en la mayoría de los casos, quienes han dictado las normas para evitar el libertinaje, lo han hecho henchidos de buena fe. Lamentablemente, no siempre ha sido así.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">En Costa Rica tenemos dos ejemplos recientes de cómo no se deben hacer las cosas: en primer lugar, el abortado proyecto enviado a la Asamblea Legislativa para cobijar con inmunidad perpetua a los miembros de los supremos poderes y, en segundo lugar, la llamada "ley mordaza".</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Ambas son iniciativas típicas de una tiranía en democracia, inconcebibles al nivel del siglo XXI.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Los que tienen derecho a pensar mal en busca de acertar, podrían considerar que una está ligada con la otra: primero me blindo, luego peco; después, aquí paz y después gloria. Pero a este país ya no se le mete un diez con hueco. Y está demostrado que, a la fuerza, ni un purgante.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Algunos señores diputados y este Gobierno parece que se olvidaron –espero que definitivamente– del adefesio que pretendía el blindaje a favor de la impunidad.</p>
<p style="text-align: justify;">&nbsp;</p>
<p><img src="images/rsz_libertad_de_presnsa2.jpg" style="margin: 15px; border: 1px solid #000000; float: right;" width="129" height="120" /></p>
<div><br />
<p style="text-align: justify;">Hace solo unas horas, los llamados padres de la patria aprobaron la esperada reforma a la conocida como "ley mordaza", una iniciativa que, en el fondo, pretendía meter a la cárcel a los periodistas investigadores, a los medios de comunicación libres e independientes, y acallar, al mismo tiempo, a una ciudadanía sedienta de buenos actos, de integridad en la función pública y privada de rectitud a toda prueba.</p>
<p><span style="text-align: justify;"><br />Estoy contento y deseo compartirlo. Me siento feliz de que esto haya ocurrido. Costa Rica lo celebra como un triunfo de la democracia; como debe ser.</span>&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Hago mías las palabras del benemérito de la patria don José María Castro Madriz, pronunciadas cuando, siendo presidente de la República, gobernaba un terreno movido por pasiones e intereses personales y de familia: "La libertad de la prensa es un derecho consagrado por la ley, y como tal debo respetarlo, cualesquiera que sean las consecuencias que de su ejercicio para mí resulten. Quizás su acción en estos momentos no sea favorable para mi gobierno, desde luego que contra él se esgrimen con no disimulada furia sus armas; pero esa libertad es una de las que más habrán de aprovecharle".</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Y agrego de mi cosecha: Si la libertad de prensa no existiese, habría que inventarla. Una democracia sin fiscales acuciosos es una democracia abortada.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Publicado: 29.04.2013</p>
<p style="text-align: justify;">Autor:&nbsp;<span style="color: #000000;">Dr. Rodolfo Hernández</span></p>
<p style="text-align: justify;"><span style="color: #000000;">fuente:&nbsp;<a href="http://porunaprensamashumanayobjetiva.blogspot.mx/">http://porunaprensamashumanayobjetiva.blogspot.mx/</a></span></p>
<p style="text-align: justify;"><span style="color: #000000;">foto: www.10minutos.com.uy</span></p>
<p style="text-align: justify;"><span style="color: #cacaca; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: small; line-height: 18px; background-color: #000000;"><br /></span></p>
</div>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/113-la-puerta-de-los-monstruos">
			&laquo; La puerta de los monstruos		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/122-el-gobernante-de-las-dos-caras">
			El gobernante de las dos caras &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/119-si-la-libertad-de-prensa-no-existiese-habría-que-inventarla#startOfPageId119">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:70:"Si la libertad de prensa no existiese, habría que inventarla - Relial";s:11:"description";s:157:"En la opinión de Rodolfo Hernández El ser humano siempre ha tenido una aspiración válida: ser libre. Pero hechos los hombres, hechas las normas. Su ma...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:61:"Si la libertad de prensa no existiese, habría que inventarla";s:6:"og:url";s:167:"http://www.relial.org/index.php/productos/archivo/opinion/item/119-si-la-libertad-de-prensa-no-existiese-habrÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â­a-que-inventarla";s:8:"og:title";s:70:"Si la libertad de prensa no existiese, habría que inventarla - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/47e29f9fe96a1771642fb05ac8a8fd00_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/47e29f9fe96a1771642fb05ac8a8fd00_S.jpg";s:14:"og:description";s:157:"En la opinión de Rodolfo Hernández El ser humano siempre ha tenido una aspiración válida: ser libre. Pero hechos los hombres, hechas las normas. Su ma...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:61:"Si la libertad de prensa no existiese, habría que inventarla";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}