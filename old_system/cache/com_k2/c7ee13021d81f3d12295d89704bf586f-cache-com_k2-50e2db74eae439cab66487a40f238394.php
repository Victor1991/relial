<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9057:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId379"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Sobre la libertad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/eb9b7452cdc806568d2312ea5614a301_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/eb9b7452cdc806568d2312ea5614a301_XS.jpg" alt="Sobre la libertad" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>"La libertad entonces se opone a la idea de poder entendido como la capacidad que tiene una persona o grupo de personas de imponer a otros su voluntad por la fuerza..."</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Resulta imposible entender el gran debate que se está dando hoy en Chile sin hacerse cargo de sus fundamentos ideológicos, y más específicamente sobre la idea de libertad que anima a quienes promueven una mayor intervención y control del Estado sobre la vida de los chilenos.</p>
<p>&nbsp;</p>
<p>La tradición intelectual liberal de origen anglosajón, que inspiró en buena medida el sistema económico que existe en Chile desde la década de los 70, entiende la libertad como el derecho que asiste a cada persona a perseguir sus fines disponiendo de lo que le es propio. Ser libre en la tradición liberal clásica consiste en no encontrarse sujeto a una voluntad ajena y en no verse privado de su propiedad para satisfacer fines o necesidades ajenas. La libertad en este contexto es un concepto social, pues se refiere siempre a una relación entre personas. Usted es libre mientras otro no le imponga por la fuerza un curso de acción que no ha elegido. Obligarlo a no fumar, a hacer ejercicio, a no consumir drogas, y cosas por el estilo, implica, por tanto, una agresión a su libertad. La libertad, entonces, se opone a la idea de poder entendido como la capacidad que tiene una persona o grupo de personas de imponer a otros su voluntad por la fuerza.</p>
<p>&nbsp;</p>
<p>El Estado, que, como diría Max Weber, es esa agrupación de personas que reclaman con éxito para sí el legítimo ejercicio de la violencia sobre los demás miembros de la comunidad, se convierte para los liberales en el garante de la libertad en la medida en que protege a cada individuo en sus derechos fundamentales. Esa es la justificación de que detente el monopolio de la fuerza. Pero, al mismo tiempo, es también la principal fuente de amenaza a nuestra libertad, pues al concentrar el monopolio de la violencia, el grupo que controla el Estado puede fácilmente obligarnos a seguir cursos de acción que no son los que nos hemos dado y, aun más, puede disponer de nuestras vidas y propiedad sin que podamos resistirnos. Los campos de concentración, las guerras y abusos sistemáticos han sido por regla general obra de los estados, o, mejor dicho, de las personas que controlan la maquinaria estatal. De ahí que sea fundamental contar con límites al poder político, función que suelen cumplir las constituciones y, por cierto, también el mercado como espacio que permite una activa oposición y fiscalización de los gobernantes. Y es que, como dijo Trotsky, en un país en que el Estado controla todas las fuentes de trabajo, el disenso significa la muerte por inanición.</p>
<p>&nbsp;</p>
<p>Pero hay otra visión de libertad que cree que esta consiste en tener el poder efectivo de cumplir los fines que una persona se ha propuesto. La libertad aquí ya no es un concepto estrictamente social, pues no consiste en estar libre de la coacción de otro, sino estar exento de sujeción a necesidades de materiales. Los pobres, en esta lógica, no pueden ser libres realmente, y están determinados para siempre a permanecer en la condición de pobres, salvo que los gobernantes los saquen de ahí. Si bien es cierto esta idea es históricamente falsa, sus implicancias filosóficas son aún más preocupantes. Pues el Estado -es decir, quienes gobiernan- necesariamente debe convertirse en satisfactor de necesidades ajenas si han de promover la libertad, lo cual conduce inevitablemente a la redistribución de riqueza mediante la coacción estatal. Así, de pasar a garantizar los derechos de las personas protegiéndolas de la agresión de terceros, el poder político se convierte en el agresor de esos derechos bajo el argumento de asegurar la libertad y bienestar. Para esta concepción que entiende la libertad como poder efectivo de alcanzar un fin que alguien se ha propuesto, el Estado no se limita a proteger la libertad, sino que es la fuente creadora de la libertad. De ahí que los proyectos de izquierda se hayan siempre fundado en la idea de que la libertad del hombre se consigue haciendo crecer el poder que los gobernantes tienen sobre los individuos. Esta tradición, como se advierte, asume que las personas son incapaces de elegir su destino sin la tutela del gobernante, el que supuestamente sabe mejor que los ciudadanos cuál es su bien. Como diría el ministro Eyzaguirre, la mayoría de la gente no es lo suficientemente inteligente para elegir el colegio de sus hijos, por lo tanto, la autoridad debe elegirlo por ellos e imponerles por la fuerza esa elección.</p>
<p>&nbsp;</p>
<p>Los liberales, en cambio, confiamos en los individuos y su capacidad de elegir y creemos en la vieja advertencia de Henry David Thoreau, quien afirmara con gran lucidez que si él supiera con absoluta certeza que una persona se dirige hacia su casa con el expreso propósito de hacerle el bien, correría por salvar su vida.</p>
<p>&nbsp;</p>
<p>Autor: Axel Kaiser</p>
<p>Fuente: <a href="http://www.elmercurio.com/blogs/2014/06/03/22369/Sobre-la-libertad.aspx">El mercurio</a></p>
<p>Foto: Fundación para el Progreso</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/378-iaf-curso-profiling-political-liberalism-as-an-effective-force-for-progress">
			&laquo; IAF curso Profiling Political Liberalism as an Effective Force for Progress		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis">
			Venezuela: Análisis económico de un país en crisis &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/379-sobre-la-libertad#startOfPageId379">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:26:"Sobre la libertad - Relial";s:11:"description";s:153:"&quot;La libertad entonces se opone a la idea de poder entendido como la capacidad que tiene una persona o grupo de personas de imponer a otros su vol...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:17:"Sobre la libertad";s:6:"og:url";s:84:"http://www.relial.org/index.php/productos/archivo/opinion/item/379-sobre-la-libertad";s:8:"og:title";s:26:"Sobre la libertad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/eb9b7452cdc806568d2312ea5614a301_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/eb9b7452cdc806568d2312ea5614a301_S.jpg";s:14:"og:description";s:157:"&amp;quot;La libertad entonces se opone a la idea de poder entendido como la capacidad que tiene una persona o grupo de personas de imponer a otros su vol...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:17:"Sobre la libertad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}