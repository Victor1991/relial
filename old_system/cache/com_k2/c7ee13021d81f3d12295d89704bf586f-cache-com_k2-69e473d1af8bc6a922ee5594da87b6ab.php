<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8739:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId445"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El Terremoto Brasilero
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/319a6b58175c8348e5b537a311344d30_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/319a6b58175c8348e5b537a311344d30_XS.jpg" alt="El Terremoto Brasilero" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Aécio Neves, economista, como la presidente, pero una generación más joven y mucho más carismático, puede derrotar a Dilma Rousseff en el ballotage del 26 de octubre. Al menos dos encuestadoras (Instituto Veritá y el Instituto Paraná de Pesquisas) le dan prácticamente 10 puntos de ventaja al candidato del Partido Social Demócrata Brasilero.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Serán menos. Esa diferencia puede reducirse sustancialmente, e incluso desaparecer, en la medida en que se intensifiquen los ataques del Partido de los Trabajadores, con Lula da Silva a la cabeza del pelotón de fusileros. Neves hoy goza la ventaja de cuatro días de gloria publicitaria positiva tras los sorprendentes resultados de la primera vuelta, así que tendrá que defenderse y atacar para poder prevalecer el día de las elecciones.</p>
<p>&nbsp;</p>
<p>¿Por qué la popularidad de Rousseff ha caído en picado? Por una combinación de tres factores:</p>
<p>&nbsp;</p>
<p>· La economía. El país entra en recesión. El aparato productivo no crece y las exportaciones disminuyen debido al enfriamiento de la economía China. Todo era una un espejismo. Brasil no estaba haciendo bien su trabajo. Eran los chinos. Bastaba que China redujera uno o dos puntos su crecimiento para que se estancara el de Brasil. Con apenas $12,100 dólares de PIB per cápita anual, el país creaba menos riqueza por habitante que otras seis naciones latinoamericanas, incluidos los vecinos Argentina, Uruguay y Chile. Es verdad que, por su volumen, es la 8va economía del mundo, pero, por su per cápita es la 105, y por su crecimiento la 137. La productividad brasilera es el 50% de la mexicana y el 18% de la norteamericana. Una birria, debido al proteccionismo y a la enorme burocracia. El país ocupa el lugar número 100 en el Índice de Libertad Económica, entre Gabón y Benín, dos atrasados países africanos. No en balde los pobres resultados.</p>
<p>&nbsp;</p>
<p>· La corrupción. La percepción general es que los gobiernos del Partido de los Trabajadores han sido los más corruptos de la historia reciente de Brasil. Cuando comenzó Lula, según Transparencia Internacional, estaba en el lugar 69 del planeta. En la medición más reciente de deslizó al 72. El último escándalo involucra a la (ex) prestigiosa Petrobrás. La empresa es otra alcantarilla. Según las revelaciones de Pablo Roberto Costa, Petrobrás le entregaba al PT el 3% de todos los contratos. Esa es una inmensa cantidad de dinero. El conflicto es mayor que el mensalao que sacudió al gobierno de Lula, y culminó con la condena de una veintena de funcionarios del gobierno, incluida su mano derecha, José Dirceu, Ministro de la Presidencia, hombre formado por los servicios de inteligencia de Cuba.</p>
<p>&nbsp;</p>
<p>· El tercermundismo. Los brasileros, no obstante la disparidad entre el sur desarrollado y el nordeste pobre, la Belindia –combinación de Bélgica y la India—que describió el economista Edmar Lisboa, siempre han jugado la carta occidental. Fue el único país de América Latina que participó en la Primera Guerra frente alemanes y austriacos, aunque de manera modesta. En la Segunda, sin embargo, envió 30,000 hombres a pelear junto a Estados Unidos por la conquista de Italia. A muchos brasileros no les gusta la estrecha relación del PT con Irán, Rusia, Cuba o el chavismo, así como el patrocinio del Foro de Sao Paulo, una especie de Internacional radical antioccidental, antimercado y antiamericana. No entienden muy bien por qué enquistarse en el Mercosur o en BRICS, cuando al país le iría mucho mejor trenzando alianzas abiertas con el Primer Mundo.</p>
<p>&nbsp;</p>
<p>Pero hay más en el terreno internacional: si Neves ganara las elecciones, su victoria sería una pésima señal para los países del llamado Socialismo del Siglo XXI y una clara advertencia de que se agota esa estridente tendencia ideológica neopopulista que ya arruinó a Venezuela.</p>
<p>&nbsp;</p>
<p>Probablemente, por ejemplo, influiría en los comicios de Uruguay, impulsando la candidatura de Luis Lacalle Pou, joven y enérgico político de centroderecha, frente a Tabaré Vázquez, un expresidente de 74 años que encabeza la fórmula del Frente Amplio, donde militan comunistas y tupamaros, desgastado personaje mucho menos atractivo para la nueva generación de votantes uruguayos decididos a buscar un cambio.</p>
<p>&nbsp;</p>
<p>La ola también llegaría a la Argentina en las elecciones del año próximo y contribuiría a barrer al kirchnerismo, así como a Bolivia, donde Evo Morales perderá un aliado al que no le importaba que la cocaína de su país llegara por toneladas a Brasil.</p>
<p>&nbsp;</p>
<p>Será un verdadero terremoto. Pero antes Aécio Neves tiene que ganar.</p>
<p>&nbsp;</p>
<p>Autor:&nbsp;Carlos Alberto Montaner</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: El Blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/443-argetina-¿quiénes-son-los-idiotas?">
			&laquo; Argetina: ¿Quiénes son los idiotas?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/449-sobre-el-congreso-y-aniversario-de-relial-en-panamá">
			Sobre el Congreso y Aniversario de RELIAL &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/445-el-terremoto-brasilero#startOfPageId445">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:31:"El Terremoto Brasilero - Relial";s:11:"description";s:159:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Aécio Neves, economista, como la presidente, pero una generación más joven y mucho más carismático...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:22:"El Terremoto Brasilero";s:6:"og:url";s:89:"http://www.relial.org/index.php/productos/archivo/opinion/item/445-el-terremoto-brasilero";s:8:"og:title";s:31:"El Terremoto Brasilero - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/319a6b58175c8348e5b537a311344d30_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/319a6b58175c8348e5b537a311344d30_S.jpg";s:14:"og:description";s:163:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Aécio Neves, economista, como la presidente, pero una generación más joven y mucho más carismático...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:22:"El Terremoto Brasilero";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}