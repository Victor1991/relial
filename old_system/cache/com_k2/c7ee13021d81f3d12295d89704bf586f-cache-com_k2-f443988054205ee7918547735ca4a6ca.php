<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6757:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId446"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL por el ex presidente Flores
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/ce742d950ca63a98d59ecec5eba0da2e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/ce742d950ca63a98d59ecec5eba0da2e_XS.jpg" alt="RELIAL por el ex presidente Flores" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: center;"><strong>CARTA ABIERTA</strong></p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">Congreso de la Red Liberal de América Latina</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">Ciudad de Panamá, domingo 16 de noviembre de 2014</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Esta carta es un pedido al gobierno salvadoreño por las condiciones extremas en que se encuentra detenido el ex presidente de ese país, Francisco Flores.</p>
<p style="text-align: left;">&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p style="text-align: left;">&nbsp;Nadie debe situarse por encima de la ley, pero en las verdaderas democracias existe el principio de presunción de inocencia. Nadie es culpable hasta que un tribunal imparcial falla en esa dirección.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Francisco Flores sostiene que es inocente de las acusaciones de corrupción que le imputan y tiene derecho a un juicio justo e imparcial, proceso al que él se ha sometido voluntariamente.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">El ex presidente Flores padece una grave enfermedad circulatoria y tenemos información de que su vida corre peligro si lo mantienen, como hasta ahora, aislado en una mínima celda, prácticamente sin ventilación, sin las mínimas facilidades sanitarias, algo contrario a las normas elementales de reclusión de prisioneros, y en definitiva, opuesto a la decencia.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Ante esta situación, respetuosamente, les pedimos a las autoridades salvadoreñas que contemplen la posibilidad de permitirle al ex presidente Flores cumpla prisión domiciliaria durante el tiempo en que se celebre el juicio al que está sometido.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Resulta congruente este pedido en relación a un mandatario que llegó al poder por el voto mayoritario de sus compatriotas.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Atentamente,</p>
<p style="text-align: center;"><strong>Ricardo López Murphy</strong></p>
<p style="text-align: center;">Presidente de la Red Liberal de América Latina<br />RELIAL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="RELIAL_carta_abierta_16_de_noviembre_de_2014.pdf" href="/index.php/productos/archivo/comunicados/item/download/112_da5f968c2241ba713c99a51c97e0e1ea">RELIAL_carta_abierta_16_de_noviembre_de_2014.pdf</a>
			    			    <span>(505 K2_DOWNLOADS)</span>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/442-alerta-activista-carlos-amel-oliva-fue-impedido-de-salir-de-cuba">
			&laquo; ALERTA: Activista Carlos Amel Oliva, fue impedido de salir de Cuba		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/447-declaración-de-relial-sobre-derechos-humanos">
			Declaración de RELIAL sobre Derechos Humanos &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/446-relial-por-el-ex-presidente-flores#startOfPageId446">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:43:"RELIAL por el ex presidente Flores - Relial";s:11:"description";s:155:"CARTA ABIERTA &amp;nbsp; &amp;nbsp; &amp;nbsp; Congreso de la Red Liberal de América Latina &amp;nbsp; Ciudad de Panamá, domingo 16 de noviembre de 20...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:34:"RELIAL por el ex presidente Flores";s:6:"og:url";s:105:"http://www.relial.org/index.php/productos/archivo/comunicados/item/446-relial-por-el-ex-presidente-flores";s:8:"og:title";s:43:"RELIAL por el ex presidente Flores - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/ce742d950ca63a98d59ecec5eba0da2e_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/ce742d950ca63a98d59ecec5eba0da2e_S.jpg";s:14:"og:description";s:171:"CARTA ABIERTA &amp;amp;nbsp; &amp;amp;nbsp; &amp;amp;nbsp; Congreso de la Red Liberal de América Latina &amp;amp;nbsp; Ciudad de Panamá, domingo 16 de noviembre de 20...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:34:"RELIAL por el ex presidente Flores";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}