<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9269:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId483"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Entre los comisarios y el mercado
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8835fed4de3847a26822c5cc338a0cab_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8835fed4de3847a26822c5cc338a0cab_XS.jpg" alt="Entre los comisarios y el mercado" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de <strong>Carlos Alberto Montaner</strong></p>
<p>&nbsp;</p>
<p><strong>Parece que una parte sustancial de los artistas e intelectuales españoles, incluidos los medios académicos, va a votar por PODEMOS, la formación política neocomunista que ha irrumpido con fuerza en la escena política.</strong></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>No me extraña. La intelligentsia latinoamericana y española, como regla general, suele ser estatista. A eso le llaman ser de izquierda. Los escritores, artistas plásticos, músicos, cineastas, actores, autores dramáticos, y, especialmente, los catedráticos y estudiantes de Ciencias Sociales y de Humanidades (antropólogos, sociólogos, arqueólogos, filósofos, teólogos, pedagogos, periodistas, etc.), se sitúan a la izquierda del espectro político. Se colocan, con variable intensidad, en el campo del estatismo.</p>
<p>&nbsp;</p>
<p>Pero no todos. Por la otra punta de este fenómeno, en general, una buena parte de las facultades de ingeniería, arquitectura, medicina, odontología, informática, Ciencias Empresariales, y tal vez la mitad de los economistas y abogados, tanto profesores como alumnos, mantienen una actitud diferente.</p>
<p>&nbsp;</p>
<p>Entre estos profesionales y aspirantes a serlo abunda un mayor porcentaje de personas que pudiéramos llamar liberales, en el sentido que se le da a ese término en América Latina y Europa. Confían mucho más en el esfuerzo individual, se inscriben en el espacio político del centroderecha, y desconfían de la gestión del Estado porque la experiencia les ha demostrado que suele ser desastrosa.</p>
<p>&nbsp;</p>
<p>La izquierda está convencida de que le corresponde al Estado, administrado por gobiernos populistas, producir ciertos bienes o gestionar directamente una gran cantidad de servicios para el pretendido beneficio del "pueblo", lo que inevitablemente significa la adjudicación y el manejo de un alto porcentaje de la riqueza que la sociedad produce.</p>
<p>&nbsp;</p>
<p>La derecha, persuadida de que ése es el camino más corto al aumento de la corrupción, al clientelismo, al descalabro económico y al surgimiento de atropellos contra los individuos, defiende que los bienes se produzcan o los servicios se brinden dentro del ámbito privado. Serán mejores, alega, y resultarán más económicos.</p>
<p>&nbsp;</p>
<p>¿Por qué esa marcada inclinación populista de la intelligentsia? Sospecho que se trata de una fatal consecuencia del mercado. El vasto campo de los intelectuales y artistas ofrece una mercancía que, independientemente de su calidad, salvo algunas excepciones, difícilmente puede sostenerse motu proprio entre los consumidores. La inmensa mayoría depende fatalmente de cátedras universitarias, subsidios, becas o premios que suelen ser abonados por medio de los presupuestos oficiales. Son "cazadores de rentas".</p>
<p>&nbsp;</p>
<p>En cambio, los profesionales que suministran algún servicio demandado por la sociedad, pese al riesgo que ello entraña, confían mucho más en el mercado que en la seguridad de colocarse bajo el ala protectora del Estado y recibir un salario mensual o alguna suerte de prebenda.</p>
<p>&nbsp;</p>
<p>A esa intelligentsia estatista que rechaza el mercado con un despreciativo aire de superioridad, le gusta autopercibirse como solidaria y generosa, pero, aunque algunos o muchos de sus miembros tengan esos rasgos, en realidad se trata de un grupo que, como es frecuente, defiende sus intereses individuales y busca la protección de un patrón que le garantice la seguridad económica, divulgación y cierta fama profesional.</p>
<p>&nbsp;</p>
<p>Claro, eso tiene un costo. En general, las dictaduras ilustradas, es decir, las que poseen un corpus ideológico que define sus presupuestos y objetivos –comunistas y fascistas en primer lugar--, son las que con más habilidad crean instituciones y mecanismos dedicados a controlar a la intelligentsia.</p>
<p>&nbsp;</p>
<p>Lo hacen mediante un sistema claramente conductista de refuerzos positivos y negativos, administrado por inflexibles comisarios culturales que manejan (en Cuba utilizan el verbo "atender") los gremios en los que colocan a los periodistas, escritores, artistas plásticos y otros intelectuales para servirse de ellos.</p>
<p>&nbsp;</p>
<p>Esos gremios son jaulas sin barrotes en las que estabulan a la intelligentsia para vigilarla y organizarla de manera que, dócilmente, los intelectuales firmen documentos, y aprendan y repitan consignas que le sean útiles al régimen para construir y sostener su relato. Si asumen los dogmas de la secta y colaboran en estas tareas, se les remunera generosamente y se les llena de premios y lisonjas. Si se oponen, se les castiga y desacredita.</p>
<p>&nbsp;</p>
<p>En cambio, en los regímenes democráticos realmente libres, regidos por la economía abierta, la intelligentsia no está sujeta al látigo de los comisarios, sino a las preferencias del mercado, lo que, con frecuencia, resulta económicamente perjudicial y riesgoso para estos intelectuales y artistas.</p>
<p>&nbsp;</p>
<p>¿Es preferible el comisario o el mercado? Los comisarios son despreciables policías del pensamiento que exigen un insoportable sometimiento. El mercado –la libre preferencia de la sociedad—no tiene corazón y los artistas e intelectuales pueden naufragar, pero hay libertad. El mercado es mil veces mejor.</p>
<p>&nbsp;</p>
<p>El autor, Carlos Alberto Montaner, es Miembro de la Junta Honorífica de RELIAL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/479-el-récord-de-probreza-en-venezuela">
			&laquo; El récord de probreza en Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/484-¿por-qué-soy-liberal?">
			¿Por qué soy liberal? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/483-entre-los-comisarios-y-el-mercado#startOfPageId483">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:42:"Entre los comisarios y el mercado - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Parece que una parte sustancial de los artistas e intelectuales españoles, incluidos los medios ac...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:33:"Entre los comisarios y el mercado";s:6:"og:url";s:100:"http://www.relial.org/index.php/productos/archivo/opinion/item/483-entre-los-comisarios-y-el-mercado";s:8:"og:title";s:42:"Entre los comisarios y el mercado - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/8835fed4de3847a26822c5cc338a0cab_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/8835fed4de3847a26822c5cc338a0cab_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Parece que una parte sustancial de los artistas e intelectuales españoles, incluidos los medios ac...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:33:"Entre los comisarios y el mercado";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}