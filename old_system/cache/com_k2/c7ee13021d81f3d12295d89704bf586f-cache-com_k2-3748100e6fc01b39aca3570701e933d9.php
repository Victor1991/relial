<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17608:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Actualidad</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo">
	  		Dora de Ampuero, condecorada al Mérito Educativo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo" title="Dora de Ampuero, condecorada al M&eacute;rito Educativo">
		    	<img src="/media/k2/items/cache/82558bd755d4bf64f8b1324b360ed554_XS.jpg" alt="Dora de Ampuero, condecorada al M&eacute;rito Educativo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<div>&nbsp;</div>
<div>La Red Liberal de América Latina (RELIAL) felicita a Dora de Ampuero, Directora Ejecutiva del Instituto Ecuatoriano de Economía Política (IEEP), quien recibió la Condecoración al Mérito Educativo de parte de la Cámara de Comercio de Quito, Ecuador. Este merecido reconocimiento da cuenta del impacto del intenso trabajo de nuestra querida Dora. Celebramos que la sociedad ecuatoriana valore y ponga en alto el nombre de una luchadora liberal ejemplar.</div>
<div>&nbsp;</div>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”">
	  		Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica”	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”" title="Conferencia Internacional sobre &ldquo;El Acuerdo de Asociaci&oacute;n  entre Centro Am&eacute;rica y la Uni&oacute;n Europea&rdquo;, &rdquo;Los mercados  de la Aeron&aacute;utica Civil y de la Energ&iacute;a El&eacute;ctrica&rdquo;">
		    	<img src="/media/k2/items/cache/7a6409a35f8223f856dc99651cb33cb1_XS.jpg" alt="Conferencia Internacional sobre &ldquo;El Acuerdo de Asociaci&oacute;n  entre Centro Am&eacute;rica y la Uni&oacute;n Europea&rdquo;, &rdquo;Los mercados  de la Aeron&aacute;utica Civil y de la Energ&iacute;a El&eacute;ctrica&rdquo;" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>El pasado 5 de noviembre se realizó esta conferencia organizada exitosamente por la Fundación Friedrich Naumann para la Libertad y la Asociación Nacional de Fomento Económico de Costa Rica (ANFE). El objetivo fue discutir los alcances del Acuerdo de Asociación entre Centro América y la Unión Europea desde la perspectiva de algunos países centroamericanos y la necesidad de abrir los mercados aéreos y eléctricos en esta región.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/221-relial-visita-la-ufm">
	  		RELIAL visita la UFM	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/221-relial-visita-la-ufm" title="RELIAL visita la UFM">
		    	<img src="/media/k2/items/cache/63ae8dd535459e6ddaa9950601158f8d_XS.jpg" alt="RELIAL visita la UFM" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Si hay un lugar que inspira, estimula y logra que hasta el último detalle simbolice libertad, este lugar es la Universidad Francisco Marroquín (UFM). Una institución que -a través del compromiso con los valores, la ética y la responsabilidad como principios- se ha ganado el prestigio y reconocimiento de la comunidad académica en general y de los liberales de América Latina en particular.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/221-relial-visita-la-ufm">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/219-encuentro-clave-entre-directivos-il-–-relial">
	  		Encuentro clave entre Directivos IL – RELIAL	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/219-encuentro-clave-entre-directivos-il-–-relial" title="Encuentro clave entre Directivos IL &ndash; RELIAL">
		    	<img src="/media/k2/items/cache/cef8e7ed10ffa07bd8535ec35e230013_XS.jpg" alt="Encuentro clave entre Directivos IL &ndash; RELIAL" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La Mesa Directiva de RELIAL y la Comité Ejecutivo de la IL se reunieron para intercambiar puntos de vista y criterios que puedan fortalecer a ambas redes. Lideraron la reunión Hans van Baalen, Presidente de la Internacional Liberal, y Ricardo López Murphy, Presidente de la Red Liberal de América Latina.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/219-encuentro-clave-entre-directivos-il-–-relial">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/218-partidos-políticos-de-relial-se-reunieron-en-guatemala">
	  		Partidos políticos de RELIAL se reunieron en Guatemala	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/218-partidos-políticos-de-relial-se-reunieron-en-guatemala" title="Partidos pol&iacute;ticos de RELIAL se reunieron en Guatemala">
		    	<img src="/media/k2/items/cache/aa5045f13216477abf2a0e16a08acd59_XS.jpg" alt="Partidos pol&iacute;ticos de RELIAL se reunieron en Guatemala" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Dando continuidad a los esfuerzos por mejorar y dinamizar la relación entre los partidos políticos y los think tanks de RELIAL, se llevó a cabo un encuentro con los partidos miembros de la red, quienes se dieron cita en Antigua para ser parte de la reunión de Comité Ejecutivo de la Internacional Liberal.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/218-partidos-políticos-de-relial-se-reunieron-en-guatemala">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal">
	  		RELIAL dio la bienvenida a la Internacional Liberal	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal" title="RELIAL dio la bienvenida a la Internacional Liberal">
		    	<img src="/media/k2/items/cache/e02fde07d49ee258cc3f6d1b19207757_XS.jpg" alt="RELIAL dio la bienvenida a la Internacional Liberal" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p style="margin-bottom: 0pt; text-align: justify;">En La Antigua, ciudad patrimonio de la humanidad, representantes políticos que defienden el liberalismo se dieron cita en ocasión de 191va Reunión de Comité Ejecutivo de la Internacional Liberal (Ll). El Partido Patriota (PP), miembro pleno de la IL y también miembro pleno de RELIAL, fue el anfitrión oficial; la Fundación Friedrich Naumann para la Libertad y RELIAL coorganizaron y apoyaron este gran encuentro liberal.</p>
<p style="margin-bottom: 0pt; text-align: justify;">&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/actualidad?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/actualidad?start=66" class="pagenav">Anterior</a></li><li><a title="5" href="/index.php/productos/archivo/actualidad?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/actualidad?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/actualidad?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/actualidad?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/actualidad?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/actualidad?start=54" class="pagenav">10</a></li><li><a title="11" href="/index.php/productos/archivo/actualidad?start=60" class="pagenav">11</a></li><li><a title="12" href="/index.php/productos/archivo/actualidad?start=66" class="pagenav">12</a></li><li><span class="pagenav">13</span></li><li><a title="14" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">14</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 13 de 14	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:19:"Actualidad - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:69:"http://www.relial.org/index.php/productos/archivo/actualidad?start=72";s:8:"og:title";s:19:"Actualidad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:47:"http://www.relial.org/media/k2/categories/3.jpg";s:5:"image";s:47:"http://www.relial.org/media/k2/categories/3.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}}s:6:"module";a:0:{}}