<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7075:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId228"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Dora de Ampuero, condecorada al Mérito Educativo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/82558bd755d4bf64f8b1324b360ed554_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/82558bd755d4bf64f8b1324b360ed554_XS.jpg" alt="Dora de Ampuero, condecorada al M&eacute;rito Educativo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<div>&nbsp;</div>
<div>La Red Liberal de América Latina (RELIAL) felicita a Dora de Ampuero, Directora Ejecutiva del Instituto Ecuatoriano de Economía Política (IEEP), quien recibió la Condecoración al Mérito Educativo de parte de la Cámara de Comercio de Quito, Ecuador. Este merecido reconocimiento da cuenta del impacto del intenso trabajo de nuestra querida Dora. Celebramos que la sociedad ecuatoriana valore y ponga en alto el nombre de una luchadora liberal ejemplar.</div>
<div>&nbsp;</div>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<div>&nbsp;</div>
<div>Destacamos el texto:&nbsp;</div>
<div>&nbsp;</div>
<div>La Cámara de Comercio de Quito,&nbsp;</div>
<div>&nbsp;</div>
<div>CONSIDERANDO</div>
<div>•<span class="Apple-tab-span" style="white-space: pre;"> </span>Que la doctora Dora de Ampuero ha apoyado ampliamente al país al haber fundado el Instituto Ecuatoriano de Economía Política, primer centro de estudios privado, independiente y sin fines de lucro, dedicado a la promoción de las ideas liberales clásicas, libertad individual; mercados libres, gobierno limitado, propiedad privada y Estado de Derecho.</div>
<div>•<span class="Apple-tab-span" style="white-space: pre;"> </span>Que con su trabajo ha logrado constituirse en el principal referente, para estudiantes, académicos, periodistas y empresarios de los temas económicos y políticos del país.</div>
<div>•<span class="Apple-tab-span" style="white-space: pre;"> </span>Que es la autora de varios libros, artículos y ensayos y de las series Ideas de Libertad y Evolución y Cambio, en las que ha dado espacio a un sinnúmero de profesionales e intelectuales ecuatorianos.</div>
<div>•<span class="Apple-tab-span" style="white-space: pre;"> </span>Que por su incansable desarrollo personal y profesional es un modelo de transparencia, integridad moral, acción e iniciativa, que deben seguir las futuras generaciones.</div>
<div>•<span class="Apple-tab-span" style="white-space: pre;"> </span>Que es deber de la Cámara de Comercio de Quito reconocer los méritos y las virtudes de las personas que con visión y compromiso trabajan por un mejor país.&nbsp;</div>
<div>&nbsp;</div>
<div>ACUERDA:</div>
<div>1.<span class="Apple-tab-span" style="white-space: pre;"> </span>Otorgar a la doctora Dora de Ampuero la condecoración al Mérito Educativo “Federico González Suarez” como homenaje de reconocimiento a su inestimable contribución a favor de la economía, las políticas públicas y la divulgación de las ideas de libertad en el Ecuador conforme a las disposiciones del Reglamento de Condecoraciones y Estímulos de la institución.&nbsp;</div>
<div>2.<span class="Apple-tab-span" style="white-space: pre;"> </span>Hacer la entrega de una presea y el acuerdo que le acredite esta distinción.&nbsp;</div>
<div>&nbsp;</div>
<div>Dado en Quito a los 18 días de noviembre de 2013.</div>
<div>&nbsp;</div>
<div>BLASCO PENAHERRERA SOLAH</div>
<div>PRESIDENTE.</div>
<div>&nbsp;</div>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”">
			&laquo; Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos">
			Ya no nos podemos conformar con menos &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo#startOfPageId228">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:58:"Dora de Ampuero, condecorada al Mérito Educativo - Relial";s:11:"description";s:156:"&amp;nbsp; La Red Liberal de América Latina (RELIAL) felicita a Dora de Ampuero, Directora Ejecutiva del Instituto Ecuatoriano de Economía Política (I...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:49:"Dora de Ampuero, condecorada al Mérito Educativo";s:6:"og:url";s:118:"http://www.relial.org/index.php/productos/archivo/actualidad/item/228-dora-de-ampuero-condecorada-al-mérito-educativo";s:8:"og:title";s:58:"Dora de Ampuero, condecorada al Mérito Educativo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/82558bd755d4bf64f8b1324b360ed554_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/82558bd755d4bf64f8b1324b360ed554_S.jpg";s:14:"og:description";s:160:"&amp;amp;nbsp; La Red Liberal de América Latina (RELIAL) felicita a Dora de Ampuero, Directora Ejecutiva del Instituto Ecuatoriano de Economía Política (I...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:49:"Dora de Ampuero, condecorada al Mérito Educativo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}