<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11081:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId239"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	&quot;Lo que no se mide, no se puede mejorar&quot; Indices para la Libertad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/78a38d90a5f5af5857b8e93fa4dd5a84_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/78a38d90a5f5af5857b8e93fa4dd5a84_XS.jpg" alt="&amp;quot;Lo que no se mide, no se puede mejorar&amp;quot; Indices para la Libertad" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: justify;">Lo que no se mide no se puede mejorar. Asumiendo esta frase como premisa, la Red Liberal de América Latina (RELIAL), la <a href="http://www.la.fnst.org/">Fundación Friedrich Naumann para la Libertad</a> (México) y <a href="http://www.caminosdelalibertad.com/">Caminos de la Libertad</a> propiciaron la presentación de tres índices claves para el desarrollo, crecimiento y prosperidad: <a href="index.php/biblioteca/item/235-indice-de-libertad-econ%C3%B3mica-en-el-mundo-2013">Índice de Libertad Económica</a>, <a href="index.php/biblioteca/item/236-%C3%ADndice-internacional-de-derechos-de-propiedad-reporte-2013">Índice de Derechos Propiedad</a> e <a href="index.php/biblioteca/item/57-indice-de-calidad-institucional-2013">Índice de Calidad Institucional</a>.</p>
<p style="text-align: justify;">&nbsp;</p>
<p>Por <a href="https://twitter.com/silviamercadoa?utm_source=fb&amp;utm_medium=fb&amp;utm_campaign=silviamercadoa&amp;utm_content=405110772356759552">Silvia Mercado</a> (RELIAL)</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p style="text-align: justify;">Que los tres Índices se discutan en un mismo escenario permitió entrelazar puntos coincidentes. Por ejemplo, la solidez de los marcos jurídicos y esencialmente el Estado de derecho es un común denominador y la base para una relación lógica: la calidad de las instituciones depende del respeto de las normas, de las normas la preservación de los derechos de propiedad, de salvaguardar la propiedad privada, la libertad económica, y de la libertad económica, la libertad individual.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Es esta lectura dinámica e integral de la persona, la economía y la sociedad, la que da pie a que hallemos respuesta a los desafíos para superar la pobreza; es esta la ruta que ayudará a disminuir la brecha entre los países que están peor de los que están mejor. O, dicho de otro modo, a encontrar las alternativas para que los países menos libres adopten las prácticas de los más libres para estar mejor.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">&nbsp;</p>
<table style="background-color: #eeece1;" border="0">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
</td>
<td></td>
<td>
<p>&nbsp;</p>
<p style="text-align: justify;">&nbsp; &nbsp; &nbsp;<strong> &nbsp;</strong> El Índice de Libertad Económica es una iniciativa del Instituto Fraser de Canadá, y el Índice de Derechos de Propiedad de la Alianza de Derechos de Propiedad (Property Rights Alliance, PRA). Ambas organizaciones, a nivel mundial, analizan datos, recolectan información que posteriormente cada país reinterpreta según sus condiciones. Para esta presentación, los académicos a cargo fueron Roberto Salinas e Isaac Katz respectivamente. El Índice de Calidad Institucional es un trabajo de Martín Krause. Sergio Sarmiento, connotado periodista y líder de opinión en México y América Latina, condujo el panel.<span style="font-size: 11px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></p>
</td>
<td>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p style="text-align: justify;">&nbsp;</p>
<div style="text-align: justify;"><span style="font-size: 11px;">&nbsp;</span><span style="font-size: 11px;">&nbsp;</span></div>
<div style="text-align: justify;">¿La libertad individual inherente a la libertad económica? &nbsp;La respuesta es clara, la libertad económica es el conjunto de las libertades claves: elección personal, el intercambio voluntario, la libre competencia y la garantía de la propiedad privada. Para medir si estas libertades se consiguen y se respetan, el Índice trabaja 42 variables en el marco de cinco grandes áreas: Tamaño del Estado; Sistema legal y derechos de propiedad; Solidez monetaria; Libertad de comercio internacional; y Regulación. Dice el Informe que “casi, sin excepción, estos estudios han encontrado que los países con instituciones y políticas consistentes con la libertad económica tienen mayores tasas de inversión, un crecimiento económico más acelerado, mayores niveles de ingresos y una rápida reducción de la pobreza”.&nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;">¿Por qué medir los derechos de propiedad? Sencillamente porque es necesario saber si estos derechos están siendo protegidos en el mundo, si están siendo respetados en los tribunales y porque es vital que se respete la libertad que tiene cada individuo de poseer y, asimismo, de hacer el uso que vea conveniente de sus bienes. Contar con información clara respecto a los derechos de propiedad es otorgar herramientas para las personas y las empresas defiendan sus patrimonios, que no es otra cosa que el logro del trabajo y el esfuerzo. &nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;">Finalmente: ¿Por qué comparar la calidad institucional de un país respecto de otro? Porque solo midiendo, comparando, tomando parámetros es posible ver y analizar la calidad de vida de sus personas. La calidad institucional mide el papel de las instituciones, las cuales se entienden como las normas y reglas que enmarcan las acciones de los individuos en la sociedad. ¿Qué si no la calidad de vida de cada individuo es lo que debiera importar a los hacedores de políticas públicas? Desde esta perspectiva es importante evaluar la protección de los derechos individuales, el funcionamiento de la justicia, el funcionamiento de la democracia, las trabas existentes para el funcionamiento de los mercados y el desarrollo de actividades emprendedoras y las condiciones de competitividad que ofrecen los distintos países. Del mismo modo, observar la rendición de cuentas de los gobernantes a los ciudadanos, la libertad de expresión y la existencia de mayor o menor corrupción como un indicador de la calidad institucional.&nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;">En conclusión, es grato manifestar que fomentar la proyección de estos Índices es parte de la misión de la Fundación Friedrich Naumann para la Libertad, la Red Liberal de América Latina y Caminos de la Libertad, organizaciones que comparten principios y velan por la salud de las economías, democracias e instituciones de la región.&nbsp;</div>
<div style="text-align: justify;">&nbsp;</div>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/238-metiéndose-en-honduras">
			&laquo; Metiéndose en Honduras		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia">
			Un peligroso espiral de violencia &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/239-lo-que-no-se-mide-no-se-puede-mejorar-indices-para-la-libertad#startOfPageId239">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:74:""Lo que no se mide, no se puede mejorar" Indices para la Libertad - Relial";s:11:"description";s:155:"Lo que no se mide no se puede mejorar. Asumiendo esta frase como premisa, la Red Liberal de América Latina (RELIAL), la Fundación Friedrich Naumann pa...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:75:"&quot;Lo que no se mide, no se puede mejorar&quot; Indices para la Libertad";s:6:"og:url";s:132:"http://www.relial.org/index.php/productos/archivo/actualidad/item/239-lo-que-no-se-mide-no-se-puede-mejorar-indices-para-la-libertad";s:8:"og:title";s:84:"&quot;Lo que no se mide, no se puede mejorar&quot; Indices para la Libertad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/78a38d90a5f5af5857b8e93fa4dd5a84_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/78a38d90a5f5af5857b8e93fa4dd5a84_S.jpg";s:14:"og:description";s:155:"Lo que no se mide no se puede mejorar. Asumiendo esta frase como premisa, la Red Liberal de América Latina (RELIAL), la Fundación Friedrich Naumann pa...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:65:""Lo que no se mide, no se puede mejorar" Indices para la Libertad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}