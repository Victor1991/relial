<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8842:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId115"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Xi Jinping en las Américas
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/45e8f4939bc3bd36e4b87ab1e324d227_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/45e8f4939bc3bd36e4b87ab1e324d227_XS.jpg" alt="Xi Jinping en las Am&eacute;ricas" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La opinión de Oscar Alvarez</p>
<p style="text-align: justify;">El presidente de China, Xi Jinping, escogió a Costa Rica, Trinidad y Tobago, México y Estados Unidos como los destinos de su primera visita a América desde que asumiera funciones presidenciales en marzo de este año, en vez de aliados tradicionales como Cuba y Venezuela.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Su gira americana comenzó en Puerto España, la capital de Trinidad y Tobago, país del Caribe y luego continuó en San José, Costa Rica, el único país de Centroamérica que tiene relaciones diplomáticas con China, establecidas desde el 1 de junio del 2007 por el gobierno de Oscar Arias. Costa Rica es además el actual presidente pro témpore del Sistema de Integración Centroamericana (SICA). Los demás miembros del SICA reconocen a Taiwán. Nicaragua tuvo en el pasado relaciones diplomáticas con China pero ahora las mantiene con Taiwán y el gobierno hondureño de Porfirio Lobo ha estado intentando, sin lograrlo, cambiar su relación de Taipei a Pekín. Es difícil hacerlo cuando existe una tregua diplomática tácita entre China y Taiwán, según la cual entre ambos no se disputan los aliados políticos.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">La visita del mandatario chino empezó en los países pequeños de Centroamérica y el Caribe y luego continuó en los grandes, es decir México y Estados Unidos. China, la segunda economía global, ve a la América Latina como una fuente de materias primas y un gran mercado para sus productos industrializados.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">La visita de Xi Jinping a Costa Rica coincidió con el sexto aniversario del establecimiento de las relaciones diplomáticas entre dos países muy diferentes en casi todo, desde el tamaño hasta la cultura, la política exterior, el sistema político y económico, entre otros factores. Sin embargo, la relación entre ambas partes ha sido significativa porque China se convirtió en la primera fuente de cooperación internacional y en el segundo socio comercial para Costa Rica. Por su parte, para China Costa Rica es su aliado en Centroamérica. Desde el 2007 el país ha recibido $159 millones en cooperación china, incluyendo los $83 millones de donación para construir el nuevo Estadio Nacional y otras sumas para Radio Patrullas. Como producto de la relación se negoció y aprobó un Tratado de Libre Comercio Costa Rica-China que está vigente desde agosto del 2011 y se ha traducido en un incremento del comercio global en un 30.5%, llegando a representar $6,200 millones en el 2012.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">La visita de Estado de Xi lógicamente incluye una cita binacional entre Xi y la presidenta Chinchilla que se tradujo en la firma de numerosos acuerdos sobre materias de infraestructura, comercio, cultura, deportes, energía, seguridad, entre otros. Algunos de los temas centrales fueron la ampliación de la Ruta 32 en Limón con un crédito blando chino de $400 millones, la construcción de plantas solares y el seguimiento de proyectos como la construcción de la Escuela Nacional de Polícía y la Refinería China con RECOPE, el monopolio estatal de los combustibles. Algunos comentaristas alegan que la adjudicación de la Ruta 32 o de la refinería a China debió empezar con una licitación y un proceso más transparente y ajustado a las leyes.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">La visita a Costa Rica se produjo a pocas semanas de distancia de la del presidente estadounidense Barack Obama al país centroamericano, lo que generó especulaciones sobre una posible competencia entre la diplomacia de las dos grandes potencias para su proyección en América.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Después de su visita a Costa Rica, Xi partió para la ciudad de México a fin de seguir las huellas de la reciente visita de Obama y reunirse con el presidente Enrique Peña Nieto.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Después de pasar por países aliados de Estados Unidos, finalmente viaja hacia California, donde el 7 y 8 de junio se realizará la Cumbre entre Obama y Xi como Jefes de Estado del G2, integrado por las 2 mayores economías del planeta.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Autor: Oscar Alvarez. Politólogo, ex embajador de Costa Rica.</p>
<p style="text-align: justify;">fuente: <a href="http://www.firmaspress.com">www.firmaspress.com</a></p>
<p style="text-align: justify;">foto: ANFE, Asociacion Nacional de Fomento Económico.</p>
<p style="text-align: left;">Read more here: http://www.elnuevoherald.com/2013/06/06/1492970/oscar-alvarez-araya-xi-jinping.html#storylink=cpy</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/114-tax-freedom-day-2013-en-rio-grande-do-sul-novena-edición">
			&laquo; Tax Freedom Day 2013 en Rio Grande do Sul - novena. edición		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/116-“enseñamos-a-los-empresarios-a-decir-orgullosos-soy-liberal”">
			“Enseñamos a los empresarios a decir orgullosos: soy liberal” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/115-xi-jinping-en-las-américas#startOfPageId115">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:36:"Xi Jinping en las Américas - Relial";s:11:"description";s:155:"La opinión de Oscar Alvarez El presidente de China, Xi Jinping, escogió a Costa Rica, Trinidad y Tobago, México y Estados Unidos como los destinos de...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:27:"Xi Jinping en las Américas";s:6:"og:url";s:97:"http://www.relial.org/index.php/productos/archivo/actualidad/item/115-xi-jinping-en-las-américas";s:8:"og:title";s:36:"Xi Jinping en las Américas - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/45e8f4939bc3bd36e4b87ab1e324d227_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/45e8f4939bc3bd36e4b87ab1e324d227_S.jpg";s:14:"og:description";s:155:"La opinión de Oscar Alvarez El presidente de China, Xi Jinping, escogió a Costa Rica, Trinidad y Tobago, México y Estados Unidos como los destinos de...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:27:"Xi Jinping en las Américas";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}