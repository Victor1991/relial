<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5390:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId232"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Entrevista con Jamen Robinson, coautor del libro &quot;Why nations fail&quot;
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/e1e1ad60f07c4aa3ccbcb2973e9d7007_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/e1e1ad60f07c4aa3ccbcb2973e9d7007_XS.jpg" alt="Entrevista con Jamen Robinson, coautor del libro &amp;quot;Why nations fail&amp;quot;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Sé que hay una literatura en las áreas de Ciencia Política y&nbsp;Economía sobre Venezuela que tiende a mirar la realidad de este&nbsp;país a través del lente único del petróleo, pero para mí Venezuela<br />luce como cualquier otro país latinoamericano. Tiene más riqueza&nbsp;y mejores carreteras, pero no veo los recursos naturales creando&nbsp;un tipo completamente diferente de sociedad. El problema<br />fundamental está en las instituciones.</p>
<p>&nbsp;</p>
<p>Podría ser que los recursos naturales interactúen con&nbsp;determinados tipos de problemas institucionales, y eso haga que las&nbsp;dificultades sean peores de lo que hubieran sido. Las instituciones<br />son cruciales para la creación de gobiernos funcionales, para la inversión, la generación de oportunidades económicas y la innovación. La maldición que puedan generar los recursos naturales está condicionada al tipo de instituciones que se tienen.</p>
<p>&nbsp;</p>
<p>Economías basadas en la extracción de recursos&nbsp;naturales suelen asociarse a países «atrazados» y&nbsp;economías industriales a países «avanzados», sin&nbsp;embargo ¿podría decirse que no es determinante si una&nbsp;economía es extractiva o industrial, pues el elemento&nbsp;clave es la presencia de instituciones fuertes?</p>
<p>...</p>
<p>&nbsp;</p>
<p>Para leer el texto completo, descargar el PDF contenido en este artículo o bien visita la <a href="http://www.revistaperspectiva.com/">revista Perspectiva</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: right;">Fuente: ICP Colombia</p>
<p style="text-align: right;">Texto: Revista Perspectiva&nbsp;</p>
<p style="text-align: right;">Foto: ICP Colombia</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="James_Robison_entrevista_1.pdf" href="/index.php/biblioteca/item/download/84_ee4ec49d6c1d5709b734986a660bd6fa">James_Robison_entrevista_1.pdf</a>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/232-entrevista-con-james-robinson-coautor-del-libro-why-nations-fail#startOfPageId232">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:76:"Entrevista con Jamen Robinson, coautor del libro "Why nations fail" - Relial";s:11:"description";s:157:"Sé que hay una literatura en las áreas de Ciencia Política y&amp;nbsp;Economía sobre Venezuela que tiende a mirar la realidad de este&amp;nbsp;país a...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:77:"Entrevista con Jamen Robinson, coautor del libro &quot;Why nations fail&quot;";s:6:"og:url";s:116:"http://www.relial.org/index.php/biblioteca/item/232-entrevista-con-james-robinson-coautor-del-libro-why-nations-fail";s:8:"og:title";s:86:"Entrevista con Jamen Robinson, coautor del libro &quot;Why nations fail&quot; - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/e1e1ad60f07c4aa3ccbcb2973e9d7007_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/e1e1ad60f07c4aa3ccbcb2973e9d7007_S.jpg";s:14:"og:description";s:165:"Sé que hay una literatura en las áreas de Ciencia Política y&amp;amp;nbsp;Economía sobre Venezuela que tiende a mirar la realidad de este&amp;amp;nbsp;país a...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:33:"Ensayos y reflexiones académicas";s:4:"link";s:76:"/index.php/biblioteca/itemlist/category/22-ensayos-y-reflexiones-académicas";}i:2;O:8:"stdClass":2:{s:4:"name";s:67:"Entrevista con Jamen Robinson, coautor del libro "Why nations fail"";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}