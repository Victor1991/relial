<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9145:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId359"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El gran debate
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/58b053c805beeea9e04dde1724076741_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/58b053c805beeea9e04dde1724076741_XS.jpg" alt="El gran debate" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Hace exactamente 70 años el economista austriaco Friedrich Hayek publicó Camino de servidumbre. El libro, un best seller en su tiempo, conserva (casi) toda su vigencia en esta América Latina nuestra que no aprende de sus errores ni olvida sus peores comportamientos. Tres décadas después de publicar su obra más conocida, la academia sueca le otorgó el Premio Nobel de Economía en 1974.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>¿Qué dijo Hayek en su famoso libro? Algo muy importante: que la planificación centralizada por el Estado va en contra de las libertades y del progreso. Nos empobrece espiritual y materialmente.</p>
<p>&nbsp;</p>
<p>¿Por qué? En esencia, aunque no lo explicó Hayek de esa manera, porque la libertad es el ejercicio pleno de la facultad que tenemos de tomar decisiones y construir con ellas nuestras vidas de acuerdo con nuestros valores, intereses y querencias.</p>
<p>&nbsp;</p>
<p>Cuando el Estado decide por nosotros lo que supuestamente nos conviene, además de empobrecernos, nos genera un profundo malestar. Ese tipo de Estado deja de ser un conjunto de instituciones a nuestro servicio y bajo nuestras órdenes, y pasa a convertirse en nuestro amo y señor. Nos somete a la más vil servidumbre.</p>
<p>&nbsp;</p>
<p>Sucedió en Cuba, como ha ocurrido siempre en los Estados totalitarios, cuando el gobierno estableció los libros que debíamos leer y los que debían ser destruidos. Cuando unos revolucionarios iluminados decidieron las verdades que ya habían sido establecidas y hasta el modo en que nos debíamos ganar la vida.</p>
<p>&nbsp;</p>
<p>Incluso, escogieron las personas a las que debíamos querer o detestar, como ocurrió cuando se dio la orden de interrumpir los lazos con los "gusanos" que habían abandonado el país y se rompieron parejas, y padres, hijos y hermanos dejaron de hablarse. O cuando se persiguió a los homosexuales porque el Estado, cruelmente, había hecho metástasis a la zona afectiva y había decidido controlar las emociones de las personas para hacerlas felices y obligatoriamente "normales" mediante la reeducación que se lograba maltratándolas en los campos de caña.</p>
<p>&nbsp;</p>
<p>Al margen de lo que Hayek escribió en Camino de servidumbre, hay un elemento esencial que mantiene la vigencia de la obra siete décadas después de haberse publicado. Del texto se desprende el rol que debe desempeñar el Estado en su relación con la sociedad, y, sobre todo, el que no debe jugar porque todos acabamos perjudicados.</p>
<p>&nbsp;</p>
<p>No es verdad que el Estado, una entelequia manejada por personas, como todas, que tienen sus intereses, preferencias y clientelas políticas, es capaz de definir el "bien común" y actuar eficientemente y con sentido de la justicia. Lo demostró otro Premio Nobel de economía de la misma cuerda de Hayek, James M. Buchanan, con sus estudios sobre la "elección pública".</p>
<p>&nbsp;</p>
<p>No es verdad que el Estado debe elegir "ganadores" y "perdedores" o asumir la función de repartidor de bienes para igualar los resultados del trabajo. Suele hacerlo mal, distorsiona y reduce el proceso de creación de riquezas y demoniza los logros económicos como si fueran actos vergonzosos.</p>
<p>&nbsp;</p>
<p>Entre las decisiones sesgadas de los funcionarios convertidos en comisarios, supuestamente transformados en píos agentes de una improbable justicia social, y el mercado, conformado por las decisiones libres de millones de personas, el mejor resultado, el que suele conducir al desarrollo y eleva el nivel de vida de toda la sociedad, es el que se deriva del mercado que es, sin duda, una expresión de la libertad.</p>
<p>&nbsp;</p>
<p>Al principio de esta nota subrayé que Camino de servidumbre conserva casi toda su vigencia. ¿En qué falla? Tal vez en suponer que el socialismo conduce inevitablemente al totalitarismo. No siempre es cierto. Los socialistas inteligentes aprenden de la experiencia y pueden rectificar.</p>
<p>&nbsp;</p>
<p>Lo hicieron los suecos ante la crisis económica de los años noventa provocada por los excesos del Estado de Bienestar. Termino con un párrafo de Mauricio Rojas, un chileno del socialismo carnívoro que llegó a Suecia exiliado tras el golpe de Pinochet, allí adquirió un doctorado en economía, evolucionó intelectual y emocionalmente, y llegó a ser miembro del parlamento sueco representando al partido de los liberales.</p>
<p>&nbsp;</p>
<p>Dice Rojas, hoy de regreso en Chile, muy preocupado por las medidas que está tomando la señora Bachelet:</p>
<p>&nbsp;</p>
<p>"Sería muy lamentable emprender un camino, el del gran Estado-patrón, que otros han tenido que desandar. Se puede construir un Estado del bienestar distinto, que una la fuerza creativa de la competencia, la diversidad y el capitalismo con un profundo compromiso solidario, pero para ello no hay que dejarse llevar por las consignas de quienes creen tener la razón por el simple hecho de gritar más alto".</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Fuente: Texto proporcionado amablemente por el autor</p>
<p>Foto: El blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/358-participación-en-el-seminario-“profiling-political-liberalism-as-an-effective-force-for-progress-–a-global-future-workshop”">
			&laquo; Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/361-ideas-y-pensamientos-de-valor-en-los-30-años-de-cedice-libertad">
			Ideas y pensamientos de valor en los 30 años de CEDICE Libertad &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/359-el-gran-debate#startOfPageId359">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:23:"El gran debate - Relial";s:11:"description";s:156:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Hace exactamente 70 años el economista austriaco Friedrich Hayek publicó Camino de servidumbre. El...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:14:"El gran debate";s:6:"og:url";s:125:"http://www.relial.org/index.php/productos/archivo/opinion/item/359-el-gran-debate/index.php/relial/mision-vision-y-principios";s:8:"og:title";s:23:"El gran debate - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/58b053c805beeea9e04dde1724076741_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/58b053c805beeea9e04dde1724076741_S.jpg";s:14:"og:description";s:160:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Hace exactamente 70 años el economista austriaco Friedrich Hayek publicó Camino de servidumbre. El...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:14:"El gran debate";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}