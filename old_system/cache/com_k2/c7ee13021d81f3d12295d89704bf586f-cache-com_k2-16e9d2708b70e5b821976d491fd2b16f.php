<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4268:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId60"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	¿La derrota del Liberalismo en Venezuela?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item text -->
	  <div class="itemFullText">
	  	<p>Presentación de Rocío Guijarro en el foro "Los Avances y El Futuro de la libertad en América Latina" Organizado por la Fundación Libertad el 21 de febrero de 2013.&nbsp;Rocío Guijarro es gerente del Centro de Divulgación del Conocimiento Económico Para La Libertad "CEDICE Libertad", instancia de parte del Comité Ejecutivo de la Fundación Internacional Para La Libertad (FIL), Miembro del Patronato de la Fundación Iberoamérica - Europa (FIE), Miembro de la Junta Directiva de la Red Liberal de América Latina RELIAL, Miembro del Comité de Redacción de la revista Perspectiva del Instituto de Ciencia Política, Colombia y de la Revista Procesos de Mercado de Madrid, Miembro del Consejo de Redacción de Estrategia y Negocios del Diario El Nacional, Venezuela.</p>	  </div>
	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
  
  
  
	
  
	<div class="clr"></div>

    <!-- Item video -->
  <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

  <div class="itemVideoBlock">
  	<h3>K2_MEDIA</h3>

				<span class="itemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:500px;" class="avPlayerContainer">
		<div id="AVPlayerID_9ed89410_1233577685" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/OgxDz3JIxH4?rel=0&amp;fs=1&amp;wmode=transparent" width="500" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		
	  	  <span class="itemVideoCaption">OgxDz3JIxH4</span>
	  
	  	  <span class="itemVideoCredits">Fundación Libertad Panamá</span>
	  
	  <div class="clr"></div>
  </div>
  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/multimedia/videos/item/60-¿la-derrota-del-liberalismo-en-venezuela?#startOfPageId60">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:51:"¿La derrota del Liberalismo en Venezuela? - Relial";s:11:"description";s:157:"Presentación de Rocío Guijarro en el foro &quot;Los Avances y El Futuro de la libertad en América Latina&quot; Organizado por la Fundación Libertad el...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:42:"¿La derrota del Liberalismo en Venezuela?";s:6:"og:url";s:99:"http://www.relial.org/index.php/multimedia/videos/item/60-¿la-derrota-del-liberalismo-en-venezuela";s:8:"og:title";s:51:"¿La derrota del Liberalismo en Venezuela? - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:165:"Presentación de Rocío Guijarro en el foro &amp;quot;Los Avances y El Futuro de la libertad en América Latina&amp;quot; Organizado por la Fundación Libertad el...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:3:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:72:"/plugins/content/jw_allvideos/jw_allvideos/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:11:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:78:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/mediaplayer/jwplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:79:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/silverlight.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:77:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/wmvplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:86:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/quicktimeplayer/AC_QuickTime.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Multimedia";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:6:"Videos";s:4:"link";s:20:"index.php?Itemid=137";}i:2;O:8:"stdClass":2:{s:4:"name";s:42:"¿La derrota del Liberalismo en Venezuela?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}