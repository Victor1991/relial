<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12460:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId304"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Un apunte sobre Bitcoin
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/79e08f32fa8a036f84441baab7b7a7ff_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/79e08f32fa8a036f84441baab7b7a7ff_XS.jpg" alt="Un apunte sobre Bitcoin" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Alberto Benegas Lynch</p>
<p>&nbsp;</p>
<p>Debido al hartazgo de la manipulación gubernamental del dinero y del consecuente empobrecimiento de la gente que ve succionado una y otra vez el fruto de su trabajo, apareció en escena un instrumento virtual (no-material) denominado Bitcoin.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>Se venía meditando sobre esta posibilidad desde 1998 (principalmente Satashi Nakamoto y Nick Szabo) y se lanzó al mercado en 2009 al efecto de posibilitar transferencias directas vía Internet en base a llaves de encriptado y concebido de modo que la cantidad total no pueda superar 21 millones de unidades (hoy son 10 millones que aceptan treinta sitios en la web) subdividibles hasta la 100 millonésima parte. Su cotización ha variado grandemente en una y otra dirección, lo cual ha incentivado a repetidos arbitrajes, lo cual ocurre en al ámbito de cualquier cotización independientemente de la opinión económica de quienes se embarcan en las respectivas transacciones.</p>
<p>&nbsp;</p>
<p>Ya sabemos las cuatro funciones del dinero que se enseñan a cualquier colegial: unidad de cuenta, medio generalizado de intercambio, depósito de valor y cancelación de deudas. El asunto es si eso lo puede cumplir el Bitcoin puesto que no encaja en el teorema de la regresión monetaria.</p>
<p>&nbsp;</p>
<p>Recuerdo que este último teorema alude a que para que un bien se utilice como dinero antes que ello debe haberse valorado como de uso no monetario que es precisamente lo que le otorga el sentido para hacer de medio común de intercambio. La sal, el ganado, el tabaco, las sedas, el oro, la plata y otros bienes se usaron en el cambio indirecto porque se les otorgaba valor como mercancías para alimento, usos industriales etc.</p>
<p>&nbsp;</p>
<p>Las casas de depósito -luego denominadas bancos- entregaban recibos por la mercancía depositada (billetes) en el contexto de reserva total o el free banking (dicho sea al pasar, esta es la genuina convertibilidad y no el canje entre papeles moneda inconvertibles). Solo con el advenimiento de la banca central y el consecuente curso forzoso irrumpió el billete con un patrón en la práctica virtual y el sistema bancario de reserva fraccional junto al consiguiente efecto multiplicador en la creación de dinero secundario. Esta moneda fiat se acepta porque deriva de lo anterior, la cual desaparecería ni bien se abrogue el curso legal y la gente pueda elegir los activos monetarios de su agrado (hoy eso sucede pero principalmente entre dineros fiat).</p>
<p>&nbsp;</p>
<p>La inflación se debe exclusivamente a la expansión de la base monetaria debida a causas exógenas al mercado, es decir, debida a razones políticas y los efectos de la inflación se traducen en la alteración en los precios relativos que, a su vez, implican derroche de capital y, como resultado, la disminución en los salarios en términos reales. Por su parte, la deflación es causada por una contracción exógena que se traduce en los mismos resultados solo que la espiral de precios opera en sentido contrario.</p>
<p>&nbsp;</p>
<p>No hay tal cosa como "las expectativas" como causa de la inflación monetaria puesto que si éstas no son convalidadas por la mencionada expansión, no hay posibilidad de incrementar precios. Los costos de un producto clave como el petróleo tampoco son inflacionarios ya que en ausencia de expansión monetaria o se consume menos petróleo o se reduce el consumo de otros bienes.</p>
<p>&nbsp;</p>
<p>Entonces, la inflación no es "el aumento generalizado de precios" sino, tal como queda dicho, la expansión monetaria debido a causas exógenas y su efecto consiste en la distorsión en los precios relativos. Si fuera el aumento generalizado de precios, como el salario es también un precio, resultarían irrelevantes esos incrementos aunque fueran el cincuenta por ciento mensual. Es la distorsión en los precios relativos lo que genera el desequilibrio entre precios e ingresos por una parte y, por otra, el desequilibrio entre los diversos precios entre si. Finalmente, como se ha hecho notar en repetidas ocasiones, el banco central solo puede operar en una de tres direcciones (expandir, contraer o dejar igual la base monetaria) las cuales inexorablemente alteran los precios relativos con todas las consecuencia malsanas apuntadas (dejar inalterada la base será deflacionaria o inflacionaria según sea la demanda de dinero, y si coincidiera con lo que hubiera hecho la gente carece de sentido la intervención monetaria)</p>
<p>&nbsp;</p>
<p>Pues bien, observo que los colegas más destacados en materia monetaria del momento que he consultado (sobre todo franceses y estadounidenses) son más bien escépticos con el Bitcoin, especialmente en conexión al antedicho teorema de la regresión monetaria y, por consiguiente, escépticos con la posibilidad que pueda cumplir con las referidas cuatro funciones del dinero. Además, el hecho de que no pueda aumentarse el stock de Bitcoin quita la posibilidad por parte de los usuarios de que se incremente el dinero debido a fenómenos endógenos al mercado como consecuencia de la respectiva apreciación en su poder adquisitivo, lo cual eventualmente indicaría que debe producirse más de la divisa en cuestión.</p>
<p>&nbsp;</p>
<p>Por otro lado, conjeturo que nadie arreglará un televisor si se anticipa que se le pagará con Bitcoins. Por mi parte, yo no trabajaría una jornada para recibir dinero virtual y tampoco vendería mi casa si como contrapartida me entregaran anotaciones digitales. En fin, mis preferencias no necesariamente son compartidas por otros: no me gustaba para nada el canto, la música y el baile de Michael Jackson pero a muchísima gente les deleitaba.</p>
<p>&nbsp;</p>
<p>Estas elucubraciones en modo alguno significan descartar la posibilidad de que en el futuro cambie lo insinuado en éstas líneas respecto al Bitcoin (que por ahora es de carácter especulativo en el sentido más extendido de la expresión debido a la fluctuación en la cotización y solo muy marginalmente para transacciones). Tengamos presente que el conocimiento es siempre de carácter provisorio sujeto a refutación. Al fin y al cabo, el dinero es lo que la gente estima es dinero aunque no parece viable el patrón aire, es decir, en este contexto, la elección de algo sin valor previo.</p>
<p>&nbsp;</p>
<p>En todo caso, esta nota sobre el Bitcoin constituye un buen pretexto para pensar en voz alta y preguntar a los lectores más que concluir sobre su futuro, sobre todo si es pactado libre y voluntariamente. Hay que seguir meditando sobre la manera de liberarse de las garras de los aparatos estatales que sistemáticamente y en todos lados han arrasado con el poder adquisitivo del dinero desde que se arrogaron el monopolio de la acuñación, momento en el que limaban las monedas y decretaban el curso forzoso indefectiblemente devaluando la unidad monetaria.</p>
<p>&nbsp;</p>
<p>La causa de los problemas monetarios radica en la obsesión por mantener atada la moneda al monopolio de la fuerza, tal como lo pone de manifiesto la nutrida bibliografía que apunta en esa dirección. Como se ha consignado, el eje central consiste no solo en abrogar el curso forzoso sino en liquidar la banca central como responsable de dañar severamente los patrimonios de la gente y, consecuentemente, toda la coordinación social.</p>
<p>&nbsp;</p>
<p>Tengamos en cuenta que no es serio proponer que el dinero gubernamental "compita" con el sector de la banca privada puesto que los privilegios oficiales inexorablemente anulan la competencia (y si no hay privilegios no tiene sentido mantener el sistema) ni tampoco aplicar la noción de soberanía a la moneda, lo cual es tan insensato como aplicarla a las manzanas. En ese nuevo contexto, cuales han de ser los activos monetarios de mayor aceptación dependerá de las características de la demanda de dinero, lo cual eventualmente puede incluir al Bitcoin (a pesar de mis reparos y dudas). En un sistema libre, las auditorías privadas en competencia se ocuparán del resto, de la misma manera que lo hacían en el antes aludido free-banking donde, además, las compañías de seguros aseguraban los depósitos al efecto que los clientes se anoticien que cuando las primas subían mostraban riesgo en el banco en cuestión respecto a la ratio depósitos-reservas.</p>
<p>&nbsp;</p>
<p>Autor: Alberto Benegas Lynch</p>
<p>Texto: Libertad y Progreso</p>
<p>Foto: Libertad y Progreso</p>
<p>&nbsp;</p>
<p>Para ver una explicación de Foundation of Economic Education "Bitcoin y las monedas alternativas" <a href="http://www.youtube.com/watch?v=ll2PHbA7Xlc" target="_blank">clic aquí</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/303-milton-friedman-y-la-verdad-histórica">
			&laquo; Milton Friedman y la verdad histórica		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/307-la-libertad-en-las-calles">
			La libertad en las calles &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/304-un-apunte-sobre-bitcoin#startOfPageId304">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:32:"Un apunte sobre Bitcoin - Relial";s:11:"description";s:154:"En la opinión de Alberto Benegas Lynch &amp;nbsp; Debido al hartazgo de la manipulación gubernamental del dinero y del consecuente empobrecimiento de...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:23:"Un apunte sobre Bitcoin";s:6:"og:url";s:90:"http://www.relial.org/index.php/productos/archivo/opinion/item/304-un-apunte-sobre-bitcoin";s:8:"og:title";s:32:"Un apunte sobre Bitcoin - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/79e08f32fa8a036f84441baab7b7a7ff_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/79e08f32fa8a036f84441baab7b7a7ff_S.jpg";s:14:"og:description";s:158:"En la opinión de Alberto Benegas Lynch &amp;amp;nbsp; Debido al hartazgo de la manipulación gubernamental del dinero y del consecuente empobrecimiento de...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:23:"Un apunte sobre Bitcoin";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}