<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8527:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId226"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	JFK y Castro se encuentran medio siglo más tarde
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/27b4275cdf67fac8ef7af010ec180724_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/27b4275cdf67fac8ef7af010ec180724_XS.jpg" alt="JFK y Castro se encuentran medio siglo m&aacute;s tarde" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Se alborotó el avispero a los 50 años del asesinato de John F. Kennedy. El Secretario de Estado John Kerry no descarta que Fidel Castro o los soviéticos estuvieran detrás de esa muerte. Lo acaba de afirmar a media lengua. No cree, como medio país, la tesis oficial de que Lee Harvey Oswald era un loco suelto que actuó por su cuenta y riesgo.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Kerry no es el primer funcionario norteamericano de alto rango que tiene esa sospecha. El presidente Lyndon Johnson, sucesor de Kennedy, pensaba lo mismo. Joseph Califano, Secretario del Ejército en esa época, coincidía con su presidente. Winston Scott, el jefe de la CIA en México, país al que Oswald acudió poco antes del crimen y se entrevistó con diplomáticos cubanos y soviéticos, sostenía algo similar.</p>
<p>&nbsp;</p>
<p>No disputaban que Oswald hubiera disparado. Era su rifle, eran sus huellas digitales y lo prueban los exámenes balísticos. Incluso, casi todos, aunque con dudas, aceptaban que fue el único tirador, pero algunos suponían que el asesino había sido dirigido hacia su objetivo por la mano cubana. (O, al menos, como barrunta Brian Latell, alto oficial de la CIA en su libro Castro´s Secrets: Cuban Intelligence, the CIA, and the Assassination of John F. Kennedy, La Habana conocía lo que iba a suceder).</p>
<p>&nbsp;</p>
<p>Castro tenía razones para alentar la muerte de Kennedy. Sabía que el presidente norteamericano estaba tratando de asesinarlo. Y lo sabía –según Latell—porque uno de los presuntos magnicidas, el Comandante Rolando Cubela, era un doble agente. Lo sabía, también, porque alguno de los gángsters detenidos en Cuba le había contado a sus captores que la Mafia había sido cooptada, nada menos que por Bobby Kennedy, para liquidar a Fidel.</p>
<p>&nbsp;</p>
<p>El gobierno cubano niega su vinculación al crimen y ha puesto en circulación otras hipótesis improbables a manera de cortina de humo. Fidel Castro insinúa que fue Lyndon Johnson. Pero su aparato de desinformación afirma que fueron los exiliados cubanos. Concretamente, Herminio Díaz, un antiguo compañero de Fidel Castro en una violenta organización gangsteril llamada Unión Insurreccional Revolucionaria (UIR) de los años cuarenta, y Eladio del Valle, un excongresista cubano.</p>
<p>&nbsp;</p>
<p>Ninguno de los dos podía defenderse de la acusación porque habían sido asesinados. Herminio, cuando desembarcaba clandestinamente en Cuba. Del Valle apareció muerto de un tiro en el pecho y un machetazo en la cabeza. Su muerte ocurrió en Miami. Nunca se supo quiénes lo ejecutaron, pero las investigaciones apuntaban a un trabajo de la inteligencia cubana.</p>
<p>&nbsp;</p>
<p>Hay cuatro fuentes que no cuentan todo lo que saben. La primera es Estados Unidos. Washington mantiene censuradas cientos de páginas relacionadas con el viaje de Oswald a México y sus relaciones con los servicios cubanos. ¿Por qué? Una hipótesis es que el crimen se hubiera podido evitar si se hubiera transmitido correctamente todo lo que sabía la estación de la CIA sobre las relaciones de Oswald con al aparato castrista. Ocultan un caso terrible de negligencia.</p>
<p>&nbsp;</p>
<p>La segunda es La Habana y, especialmente, el oficial de inteligencia Fabián Escalante –hoy general--, quien, aparentemente, estaba en Dallas el día del asesinato. Escalante, además, pudiera aclarar las relaciones (¿íntimas?) entre Silvia Tirado de Durán, empleada del consulado cubano en México, y Oswald. También, la participación del asesino en una fiesta "cubana" en DF, como relatara la escritora mexicana Elena Garro, presente en el baile.</p>
<p>&nbsp;</p>
<p>La tercera es Moscú. La inteligencia soviética sabe mucho sobre Oswald. No es lógico que la URSS hubiera utilizado a una persona con la biografía de este personaje para matar al presidente norteamericano, dado que inmediatamente hubiera despertado sospechas, pero es muy significativo que Oswald se hubiera reunido en México con Oleg Nechiporenko, un agente de inteligencia de quien se afirma que no era extraño a estas siniestras tareas.</p>
<p>&nbsp;</p>
<p>Pero acaso el testimonio más importante es el de la Mafia. ¿Por qué Jack Ruby, un hampón de poca monta, decide ejecutar a Oswald "para ahorrarle a Jacqueline Kennedy la pena de participar en un juicio doloroso"? Conmovedor, pero impropio de un endurecido gangstercillo. Oswald había negado ser el autor del asesinato y en ese momento todo era muy confuso. ¿Trataba Ruby de borrar otras huellas?</p>
<p>&nbsp;</p>
<p>Cuando se cumplan 100 años de la muerte de JFK tal vez sepamos un poco más. O nunca.</p>
<p>&nbsp;</p>
<p>Fuente: Texto proporcionado por Carlos Alberto Montaner</p>
<p>Foto: Blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/211-cuba-y-sus-dos-monedas">
			&laquo; Cuba y sus dos monedas		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo">
			Honduras o el fin del chavismo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde#startOfPageId226">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:58:"JFK y Castro se encuentran medio siglo más tarde - Relial";s:11:"description";s:156:"Se alborotó el avispero a los 50 años del asesinato de John F. Kennedy. El Secretario de Estado John Kerry no descarta que Fidel Castro o los soviétic...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:49:"JFK y Castro se encuentran medio siglo más tarde";s:6:"og:url";s:116:"http://www.relial.org/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde";s:8:"og:title";s:58:"JFK y Castro se encuentran medio siglo más tarde - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/27b4275cdf67fac8ef7af010ec180724_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/27b4275cdf67fac8ef7af010ec180724_S.jpg";s:14:"og:description";s:156:"Se alborotó el avispero a los 50 años del asesinato de John F. Kennedy. El Secretario de Estado John Kerry no descarta que Fidel Castro o los soviétic...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:49:"JFK y Castro se encuentran medio siglo más tarde";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}