<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8190:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId248"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Hay una camino, pero no es éste
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/0bfc0ce99892772fc285e10ee3943d9a_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/0bfc0ce99892772fc285e10ee3943d9a_XS.jpg" alt="Hay una camino, pero no es &eacute;ste" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Miguel Velarde</p>
<p>&nbsp;</p>
<p>Basta de buscar victorias donde no existen. Dada la coyuntura y la grave crisis por la que atraviesa el país, los resultados de las elecciones municipales del domingo no fueron buenos para la oposición. La estrategia fue convertir esa contienda electoral en un plebiscito que demostrase que somos mayoría y que Nicolás Maduro no ganó las elecciones del 14 de abril; fracasamos en ese intento.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Los problemas comenzaron incluso antes del 8 de diciembre, cuando los números no daban y se empezaron a buscar culpables incluso en vísperas de la derrota. Pudimos observar a actores y sectores de oposición atacando con mayor vehemencia a quienes decidieron no votar que al régimen que enfrentamos. Eso es injusto, porque si alguien cumplió estos últimos 15 años fue el ciudadano que votó, marchó y hasta fue a paro cuando así se le pidió. Pero no solamente eso, sino que también es erróneo, porque el nivel de participación de la última elección fue mayor al promedio histórico en elecciones municipales.</p>
<p>&nbsp;</p>
<p>Tampoco podemos olvidar que el 14 de abril la gente una vez más estuvo a la altura de las circunstancias y acudió a las urnas masivamente. Fue el liderazgo político el que en esa ocasión falló. Es por eso que creemos que la dirigencia opositora debería estar más preocupada encontrando soluciones para defender sus victorias que chantajeando a quienes no quieren votar. Los políticos deben comprender que el voto no se mendiga, el voto se gana.</p>
<p>&nbsp;</p>
<p>Anoche, luego de que se conocieran los resultados de la jornada electoral, vimos con preocupación como ciertos sectores opositores celebraban eufóricamente la victoria en algunos estados. Imaginamos que ellos no leyeron lo que está planteado en el Plan de la Patria ni tampoco hicieron un diagnóstico correcto de lo que enfrentaremos en el 2014. No vivimos en condiciones normales y eso demanda que nos demos un baño de realismo. La lucha de los alcaldes recién electos no será por una buena gestión sino por su propia supervivencia.</p>
<p>&nbsp;</p>
<p>El cálculo político de algunos líderes y cierto sector de la oposición nos puede costar los próximos 20 años en Venezuela. Si ellos no son capaces de decir la verdad desde su actual posición, no queremos imaginar como serían en el gobierno. La pregunta que tantos nos hacemos es: ¿Cómo se puede ser "prudente" con un régimen que busca destruirlo todo? La coyuntura demanda riesgos y el que no esté dispuesto a asumirlos, debe hacerse a un lado y que pase el siguiente.</p>
<p>&nbsp;</p>
<p>Ha llegado la hora de una profunda reflexión. La Unidad como pilar debe preservarse e incluso la MUD como concepto puede rescatarse. Pero necesita una urgente renovación de su dirigencia y un cambio de estrategia. Carlos Blanco planteó ayer con gran claridad las "cinco R's" que se necesitan en la Unidad: Reflexión, Revisión, Rectificación, Renovación y Reforma.</p>
<p>&nbsp;</p>
<p>Necesitamos una nueva dirigencia que no busque ser alternativa, sino que quiera ser gobierno. La MUD debe dejar de ser un ente electoral y convertirse en uno político que aglutine a todos los sectores de oposición bajo el liderazgo legítimo de líderes electos por la gente y no solo por algunos partidos.</p>
<p>&nbsp;</p>
<p>Se cierra un ciclo, comienza otro.</p>
<p>&nbsp;</p>
<p>Hay un camino, pero no es éste.</p>
<p>&nbsp;</p>
<p>Fuente:Texto proporcionado amablemente por Miguel Velarde, exbecario RELIAL</p>
<p>Foto: Guayoyo en letras</p>
<p>Autor: Miguel Velarde, Editor en Jefe de Guayoyo en letras y exbecario de RELIAL</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @MiguelVelarde</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span id="cloak37459">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak37459').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy37459 = 'mv&#101;l&#97;rd&#101;' + '&#64;';
 addy37459 = addy37459 + 'g&#117;&#97;y&#111;y&#111;&#101;nl&#101;tr&#97;s' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak37459').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy37459 + '\'>' + addy37459+'<\/a>';
 //-->
 </script></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela">
			&laquo; La grandeza de Mandela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente?">
			Argentina: Saqueos en ¿un país con buena gente? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste#startOfPageId248">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:41:"Hay una camino, pero no es éste - Relial";s:11:"description";s:155:"En la opinión de Miguel Velarde &amp;nbsp; Basta de buscar victorias donde no existen. Dada la coyuntura y la grave crisis por la que atraviesa el paí...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:32:"Hay una camino, pero no es éste";s:6:"og:url";s:98:"http://www.relial.org/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste";s:8:"og:title";s:41:"Hay una camino, pero no es éste - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/0bfc0ce99892772fc285e10ee3943d9a_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/0bfc0ce99892772fc285e10ee3943d9a_S.jpg";s:14:"og:description";s:159:"En la opinión de Miguel Velarde &amp;amp;nbsp; Basta de buscar victorias donde no existen. Dada la coyuntura y la grave crisis por la que atraviesa el paí...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:32:"Hay una camino, pero no es éste";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}