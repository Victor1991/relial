<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8312:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId211"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Cuba y sus dos monedas
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/18cb4412b3fd96d4c2c15944894f7ea5_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/18cb4412b3fd96d4c2c15944894f7ea5_XS.jpg" alt="Cuba y sus dos monedas" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Cuba y las dos monedas</p>
<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>El gobierno de Raúl Castro ha declarado su intención de terminar gradualmente con la dualidad monetaria. Estupendo. Mientras más rápido desaparezca esa cruel anomalía, tanto mejor. La estafa, comenzada en 1994, ha durado demasiado.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>En la Isla hay dos monedas. Una es el peso o CUT, carente de valor adquisitivo, con el que les pagan a los trabajadores. La otra es el CUC, o peso convertible, equivalente (más o menos) al dólar, en el que les venden a precios internacionales todo lo que es deseable comprar.</p>
<p>&nbsp;</p>
<p>Pese a que, oficialmente, el peso regular y el convertible tienen el mismo valor, en realidad los CUC se cambian por 24 CUT. Razón por la que el salario promedio de los cubanos sea uno de los más bajos del planeta. Oscila entre 10 y 20 dólares al mes.</p>
<p>&nbsp;</p>
<p>Sin embargo, el fin de la dualidad monetaria no acabará con los quebrantos económicos de la Isla. Todo lo que conseguirá es hacer más transparente el desastre. Mientras más se sincere la economía más obvias serán sus falencias. Entendámoslo: esa detestable trampa no es el problema. Es sólo el reflejo de un gravísimo mar de fondo: la improductividad tremenda del sistema.</p>
<p>&nbsp;</p>
<p>La moneda cubana es la expresión fiel de su economía. Es una birria, porque el colectivismo planificado por los comisarios, basado en las supersticiones del marxismo-leninismo, provoca que la producción y la productividad de los cubanos sean bajísimas. ("Es el sistema, estúpido", diría James Carville).</p>
<p>&nbsp;</p>
<p>Mientras existió el patrón oro, cualquier moneda que tuviera el respaldo de ese metal y admitiera la libre convertibilidad, como sucedía con el peso cubano hasta el triunfo de la revolución, era respetable. Cuando se abandonó el patrón oro, las monedas sólo quedaron amparadas por la solvencia, la estabilidad y el carácter predecible de la sociedad que las imprimía.</p>
<p>&nbsp;</p>
<p>De ahí la despreciable insignificancia del peso cubano. De ahí, también, por la otra punta, la supremacía del dólar americano, pero también, en menor medida, del euro, el yen o la libra esterlina. Incluso, del franco suizo, pese a los escasos ocho millones de habitantes radicados en el diminuto país. La imponente productividad de la nación alpina y la fortaleza de sus instituciones convierten al franco suizo en una moneda-refugio ante cualquier turbulencia económica internacional. Cada vez que tiemblan las rodillas los expertos compran francos suizos.</p>
<p>&nbsp;</p>
<p>¿Qué puede hacer Raúl Castro para, realmente, enderezar la economía cubana? Sin duda, enterrar ese disparatado modo de producir y organizar la sociedad. El sistema no es enmendable. Gorbachov, que también trató de salvar el comunismo, acabó por admitir que no era posible, como sucedió en prácticamente toda Europa oriental.</p>
<p>&nbsp;</p>
<p>¿Por qué Raúl Castro no lo hace? Supongo que, al menos, por tres razones: por confusas convicciones ideológicas que no ha conseguido sacudirse; por aferrarse al poder; y (la de más entidad), por ser emocionalmente incapaz de aceptar que se ha pasado ochenta años defendiendo ideas equivocadas. Debe ser muy duro admitir que la obra de toda la vida es un perfecto disparate que ha generado un daño inmenso.</p>
<p>&nbsp;</p>
<p>Por supuesto, el fin del comunismo entrañaría la liquidación política de la casta dominante en Cuba, pero si Raúl Castro quisiera, realmente, que ese pobre país comenzara a producir como Dios manda, y los cubanos pudieran vivir decentemente, como asegura que son sus intenciones, no le quedaría más remedio que renunciar totalmente al error colectivista, admitir las libertades democráticas, y regresar a la existencia de la propiedad privada como principal agente económico y al mercado como forma de asignar recursos, aunque tenga que liquidar el frondoso berenjenal en el que su hermano Fidel, irresponsablemente, internó a los cubanos.</p>
<p>&nbsp;</p>
<p>Mientras los fundamentos del comunismo persistan, aunque hoy estén mitigados por algunas reformas laterales, da más o menos igual que haya una moneda o cuatro. El país seguirá patas arriba y los cubanos continuarán desesperados tratando de huir. El mal está en otra parte. A ver si se entera.</p>
<p>&nbsp;</p>
<p>fuente: texto proporcionado amablemente por Carlos Alberto Montaner</p>
<p>foto: Blog de Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/122-el-gobernante-de-las-dos-caras">
			&laquo; El gobernante de las dos caras		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde">
			JFK y Castro se encuentran medio siglo más tarde &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/211-cuba-y-sus-dos-monedas#startOfPageId211">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:31:"Cuba y sus dos monedas - Relial";s:11:"description";s:156:"Cuba y las dos monedas En la opinión de Carlos Alberto Montaner &amp;nbsp; El gobierno de Raúl Castro ha declarado su intención de terminar gradualmen...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:22:"Cuba y sus dos monedas";s:6:"og:url";s:89:"http://www.relial.org/index.php/productos/archivo/opinion/item/211-cuba-y-sus-dos-monedas";s:8:"og:title";s:31:"Cuba y sus dos monedas - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/18cb4412b3fd96d4c2c15944894f7ea5_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/18cb4412b3fd96d4c2c15944894f7ea5_S.jpg";s:14:"og:description";s:160:"Cuba y las dos monedas En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; El gobierno de Raúl Castro ha declarado su intención de terminar gradualmen...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:22:"Cuba y sus dos monedas";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}