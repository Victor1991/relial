<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9419:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId358"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/3b77d3f73b59742412f393cd0d264b14_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/3b77d3f73b59742412f393cd0d264b14_XS.jpg" alt="Participaci&oacute;n en el seminario &ldquo;Profiling Political Liberalism as an Effective Force for Progress &ndash;A Global Future Workshop&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Entre los días 27 de abril y el sábado 3 de mayo de 2014, tuvimos la oportunidad de participar en el seminario "Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop" en la International Academy for Leadership (IAF) de la Friedrich Naumann Foundation en Gummersbach, Alemania. El seminario fue una oportunidad única de poder compartir con individuos con pensamiento similar acerca de cómo cambiar las sociedades en que vivimos para obtener mayor progreso económico. Tanto los conferencistas como los participantes aportaron a enriquecer el debate y a brindar perspectivas diferentes y más profundas sobre los problemas analizados. La gran mayoría de las exposiciones aportaron al enriquecimiento de la experiencia, así como las discusiones en clase y dentro de los grupos mismos.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Fueron varias las intervenciones que en lo personal realizaron el mayor aporte a afianzar mis ideas liberales y a tener mejores argumentos para la defensa de un orden liberal como sustento del progreso económico. Los aportes del Dr. Terence Kealy fueron iluminadores, tanto en el rol de las academias, la intervención del Estado y, sobre las ideas erradas (aún entre muchos liberales) sobre las patentes y derechos de propiedad. Como "keynote lecture", su cátedra llenó mis expectativas y entiendo que sentó un buen tono para el resto de la actividades. Continuando con la idea del rol de los derechos de propiedad, Steffen Hentrich realizó un trabajo magistral al momento de proporcionar fundamentos claves para entender este tema. Sin lugar a dudas, estas dos exposiciones se complementaron muy bien y tuvieron la fuerza de transmitir ideas que, por lo menos a mi, me convencieron sobre la poca justificación del Estado en ese sentido. Si alguna vez pudiera influir sobre la política de derechos de propiedad de mi país, sabría mejor cuál es el rol que me toca jugar para avanzar una agenda de promoción del conocimiento y, por tanto, del desarrollo económico.</p>
<p>&nbsp;</p>
<p>Como el conocimiento y la educación van muy de la mano, fue importante la introducción del Dr. Kealy en ese sentido. Y, aunque fue una lástima no haber podido contar con la presencia de James Tooley, Ekta Sodha hizo un gran trabajo presentando el caso de los emprendedores dedicados a la educación, así como el uso de métodos novedosos para motivar más a los niños. La presentación de la Sra. Sodha proporcionó un mejor entendimiento de cómo, sin la necesidad de la intervención estatal, es posible desarrollar un sistema educativo de calidad que pueda responder a las necesidades los padres, agentes muchas veces dejado a un lado por los sistemas públicos. Fue una charla muy interesante y sus ejemplos ya los he empleado en diferentes conversaciones que he tenido desde mi regreso de Alemania al país. En particular, los usé durante este pasado fin de semana en la primera versión de la Universidad para la Libertad y el Progreso Económico (ULPE), organizada por CREES y que contó con la presencia de 49 dominicanos de la sociedad civil, partidos políticos, sector privado y personalidades de los medios de comunicación.</p>
<p>&nbsp;</p>
<p>Otra presentación que pude citar en más de una ocasión durante la ULPE, fue la realizada por Ronald Bailey. Había leído los artículos del Sr. Bailey desde 1995 en la revista Reason, pero no había tenido la oportunidad de escucharlo en una charla. Sus contribuciones fueron interesantísimas para advertirnos sobre el "principio de precaución" y cómo su adopción puede retrasar el avance tecnológico y, por tanto, detener el nivel de progreso económico que lleva la humanidad. El resto de la presentación proporcionó muy buena información con relación a nuevas tecnologías que nos beneficiarán en el futuro.</p>
<p>&nbsp;</p>
<p>La charla de Ricardo López Murphy fue muy buena por su perspectiva de una persona que ha participado en la administración pública y en la política. Sin dudas, fue provechosa desde el punto de vista de quienes trabajamos en la formulación de propuestas de políticas públicas. Las que estuvieron relacionadas con el rol del gobierno y sus agencias de seguridad sobre la información pública, proporcionaron una perspectiva del poder que tiene el Estado sobre la información de los ciudadanos. Una perspectiva que alerta sobre la constante vigilancia que sobre las autoridades debemos tener quienes pertenecemos a la sociedad civil y aquellos quienes estamos comprometidos con la libertad.</p>
<p>&nbsp;</p>
<p>Considero que iniciativas como estas ayudan a afianzar mejor las posiciones liberales de los participantes y a construir lazos con personas de otros países. Fue una oportunidad muy valiosa, que aunque tenía muchas actividades durante el día, tuvo un buen balance. Aunque tuvimos trabajos de grupo, se valoró más el tiempo de discusión con entre los participantes y con los mismos expositores. Sugiero que mantengan la dinámica igual en ese sentido. Aunque hubo poco tiempo de conocer la cultura alemana de primera mano, el viaje a Düsseldorf fue algo muy positivo en ese sentido. En lo particular, también lo valoro muy positivamente.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Miguel A. Collado Di Franco. Economista Senior, CREES, República Dominicana</p>
<p>Foto: CREES</p>
<p>Fuente: Texto proporcionado por el autor</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana">
			&laquo; El porqué de la crisis venezolana		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/359-el-gran-debate">
			El gran debate &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/358-participación-en-el-seminario-“profiling-political-liberalism-as-an-effective-force-for-progress-–a-global-future-workshop”#startOfPageId358">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:139:"Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop” - Relial";s:11:"description";s:155:"Entre los días 27 de abril y el sábado 3 de mayo de 2014, tuvimos la oportunidad de participar en el seminario &quot;Profiling Political Liberalism as...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:130:"Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop”";s:6:"og:url";s:197:"http://www.relial.org/index.php/productos/archivo/opinion/item/358-participación-en-el-seminario-“profiling-political-liberalism-as-an-effective-force-for-progress-–a-global-future-workshop”";s:8:"og:title";s:139:"Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/3b77d3f73b59742412f393cd0d264b14_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/3b77d3f73b59742412f393cd0d264b14_S.jpg";s:14:"og:description";s:159:"Entre los días 27 de abril y el sábado 3 de mayo de 2014, tuvimos la oportunidad de participar en el seminario &amp;quot;Profiling Political Liberalism as...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:130:"Participación en el seminario “Profiling Political Liberalism as an Effective Force for Progress –A Global Future Workshop”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}