<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8561:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId309"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La oportunidad perdida
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/a8bfce1b35b25b98e185785e8a683747_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/a8bfce1b35b25b98e185785e8a683747_XS.jpg" alt="La oportunidad perdida" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Ricardo López Murphy</p>
<p>&nbsp;</p>
<p>El lunes 5 de marzo de 2001, el presidente, Fernando de la Rúa, me designó ministro de Economía, Infraestructura y Vivienda. La situación en ese momento era compleja, difícil e incierta, pero creía entonces −y sigo creyendo ahora− que, por medio de una gestión apropiada, que requería un firme ajuste coyuntural, era posible reencauzar la marcha del proceso económico hacia una dirección promisoria.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Quince días más tarde, el mismo presidente le había retirado el respaldo al plan que habíamos presentado y debí dejar el cargo junto a una gran parte del equipo −integrado por profesionales de elevadísimas calificaciones técnicas y morales− que me había acompañado. De la Rúa entendió que las resistencias que nuestro programa suscitaba en la coalición de gobierno tornaban inconveniente mi permanencia en el ministerio.</p>
<p>&nbsp;</p>
<p>La complicaciones coyunturales se derivaban de la combinación de tres factores que era improbable que se produjeran en forma simultánea pero que, azarosamente, coincidieron: 1) la crisis de las economías del sudeste asiático, que provocaron una baja a menos de la mitad del precio promedio de las commodities exportadas por la Argentina; 2) la crisis rusa, que derivó en el cierre de los mercados de capitales para los países emergentes; 3) la fuerte devaluación de Brasil.</p>
<p>&nbsp;</p>
<p>A esos tres imponderables se sumaron dos componentes locales: 4) un sistema monetario-cambiario −la convertibilidad− que era sólido para enfrentar dificultades que estuvieran originadas en mecanismos monetarios pero, por las mismas razones, era menos apto para soportar adversidades provenientes de los procesos económicos reales, como una baja de precios de las commodities o una brutal devaluación de nuestro gigantesco vecino; 5) como herencia del gobierno anterior, un nivel de endeudamiento del orden del 45% del PBI, excesivo para un régimen de convertibilidad, que requiere un espacio fiscal holgado.</p>
<p>&nbsp;</p>
<p>Mi análisis era que la conjunción de los tres factores externos desfavorables era un escenario que se revertiría en un lapso de quince a veinte meses. Era impensable que tuviéramos tanta adversidad durante mucho tiempo. El problema era cómo superar la coyuntura hasta tanto el escenario internacional se tornara más amigable.</p>
<p>&nbsp;</p>
<p>La disciplina fiscal futura, proveniente de una reforma del sector público y de la maduración de la reforma previsional, que hacía que pagáramos dos sistemas jubilatorios simultáneos, por la propia reducción de los afiliados al reparto y la autofinanciación del sistema de capitalización, permitiría rebajar adicionalmente el déficit en 2% del PBI, lo cual durante la expansión reduciría el peso de la deuda pública.</p>
<p>&nbsp;</p>
<p>La clave del programa era la austeridad, cuando se debe aplicar, que es en la prosperidad.</p>
<p>&nbsp;</p>
<p>Se ha cuestionado mucho el "ajuste salvaje" que nuestro plan supuestamente incluía. Ese fue el factor político que determinó que el programa no haya sido aplicado. Aquí cabe una explicación precisa.</p>
<p>&nbsp;</p>
<p>Dado que teníamos un problema coyuntural que nos obligaba a hacer un ajuste, establecimos que ese recorte sería aplicado a los gastos ligados a la demagogia política.</p>
<p>&nbsp;</p>
<p>A menos que el Congreso determinara que no estaba dispuesto a ajustar por el lado de los subsidios políticos, entonces sí, automáticamente la poda se trasladaba a los gastos de la Universidad. El recorte del presupuesto universitario se concentró en el rectorado, que era el comité de Shuberoff y la Coordinadora. No se afectaba a las facultades. No había forma de no hacer un ajuste si queríamos evitar la crisis. La prueba es que tratar de evitar y postergar la corrección indujo la crisis que sobrevino diez meses más tarde.</p>
<p>&nbsp;</p>
<p>Finalmente, la coalición de gobierno no aceptó la reducción de gastos políticos. La realidad es que estábamos en problemas y teníamos que superar la situación en la que nos encontrábamos. Nunca propusimos −como también se dijo− rebajar jubilaciones ni salarios en ese programa. Otra gran mentira divulgada por el relato kirchnerista.</p>
<p>&nbsp;</p>
<p>Lamentablemente, la historia terminó dándonos la razón: corralito, default, pesificación asimétrica y devaluación, con más de 50% del país por debajo de la línea de pobreza. Perdimos la batalla, pero mantuvimos nuestras banderas en alto.</p>
<p>&nbsp;</p>
<p>Autor: Ricardo López Murphy*Ex ministro de Defensa y Economía. Fue candidato a presidente en 2003 y 2007.</p>
<p>Foto: perfil.com</p>
<p>Fuente: <a href="http://www.perfil.com/contenidos/2014/03/16/noticia_0060.html">www.perfil.com</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/307-la-libertad-en-las-calles">
			&laquo; La libertad en las calles		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/310-méxico-insurgente">
			México insurgente &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/309-la-oportunidad-perdida#startOfPageId309">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:31:"La oportunidad perdida - Relial";s:11:"description";s:158:"En la opinión de Ricardo López Murphy &amp;nbsp; El lunes 5 de marzo de 2001, el presidente, Fernando de la Rúa, me designó ministro de Economía, Infr...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:22:"La oportunidad perdida";s:6:"og:url";s:89:"http://www.relial.org/index.php/productos/archivo/opinion/item/309-la-oportunidad-perdida";s:8:"og:title";s:31:"La oportunidad perdida - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/a8bfce1b35b25b98e185785e8a683747_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/a8bfce1b35b25b98e185785e8a683747_S.jpg";s:14:"og:description";s:162:"En la opinión de Ricardo López Murphy &amp;amp;nbsp; El lunes 5 de marzo de 2001, el presidente, Fernando de la Rúa, me designó ministro de Economía, Infr...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:22:"La oportunidad perdida";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}