<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16059:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId420"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/91b1b90c684fd8e5c2ec1b7418ca380f_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/91b1b90c684fd8e5c2ec1b7418ca380f_XS.jpg" alt="Ricardo L&oacute;pez Murphy: &ldquo;El default de Argentina fue un fracaso colectivo inexplicable&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>&nbsp;</p>
<p>Entrevista a Ricardo López Murphy, Presidente de RELIAL</p>
<p>"El núcleo de la amenaza era el default. Argentina tenía tres características que es muy importante entender para ver la irracionalidad de la situación en donde estamos".</p>
<p>Por Belén Marty.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>&nbsp;Fue candidato a presidente de Argentina en 2003 y 2007, y a jefe de Gobierno de Buenos Aires en 2011. También fue ministro de Economía, ministro de Defensa y ministro de Infraestructura y Vivienda durante la presidencia de Fernando de la Rúa (1999-2001). Actualmente preside la Red Liberal de América Latina (RELIAL) y la Fundación Cívico Republicana (FCR). Su nombre es Ricardo López Murphy y, además de político y profesor universitario, es un hombre sencillo.</p>
<p>&nbsp;</p>
<p>El exministro me recibió en su oficina de la avenida Córdoba el jueves. La altísima humedad fue el segundo tema más recurrente de las conversaciones del jueves. El primero fue, sin lugar a dudas, el default y el futuro de la economía argentina.</p>
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>Empecemos por el contexto local. ¿Qué opina usted de cómo se manejó la Argentina con respecto a la negociación con los fondos buitre?</strong></p>
<p>&nbsp;</p>
<p>En el contencioso que hubo entre los fondos que se ocupan de deuda en mora —yo nunca les digo buitre, porque si ellos son buitre nosotros somos carroña— yo entiendo que es un slang que se usa en Argentina pero implica para nosotros temas muy delicados...</p>
<p>&nbsp;</p>
<p>Estos fondos se habían puesto como clave en toda su estrategia hacernos caer en default. Esa era la más grande amenaza que ponían sobre Argentina, y era una amenaza complicada, puesto que si caíamos en default, ellos también pierden porque van a la cola del default. Era como si hubiesen dicho acá "vuela el avión" si no hacen caso a mi demanda. Y la verdad que eso fue lo que ocurrió; para la Argentina es una tragedia.</p>
<p>&nbsp;</p>
<p>El núcleo de la amenaza era el default. Argentina tenía tres características que es muy importante entender para ver la irracionalidad de la situación en donde estamos.</p>
<p>&nbsp;</p>
<p>Argentina tiene una baja deuda pública, no por austeridad sino porque nadie nos presta, y eso es un factor importante. En general los países que defaultean son aquellos que tienen una enorme deuda pública.</p>
<p>&nbsp;</p>
<p>También, la deuda argentina tenía un bajo servicio anual. Los costos de intereses de la deuda argentina eran de menos de 1%. Ningún país defaultea con una deuda pública chica y bajo servicio.</p>
<p>&nbsp;</p>
<p>Forzarnos a defaultear es hacer un daño a un país que ya tiene un problema enorme en vinculación con el mercado de capitales.</p>
<p>&nbsp;</p>
<p>Yo creo que esto ha sido mal manejado y terminamos en este desastre. Y este desastre es triple. Esto agrava los problemas de Argentina, que es quien más va a sufrir. Por un lado es un mal resultado para el juez. La idea del juez era resolver la controversia. Yo creo que a lo mejor si se hubiese podido correr la resolución a enero, hubiese sido mas fácil superar un tema que en el acierto, en el error, traumatiza al gobierno argentino: la cláusula RUFO (Rights Upon Future Offers). Yo diría "hay que tener cuidado" porque no es un tema menor.</p>
<p>&nbsp;</p>
<p>Tercero, es un muy mal resultado para los holdouts, porque es como los terroristas que se matan ellos mismos. ¿Qué queda después de volar esto? Si hay default y no se paga, perdemos todos.</p>
<p>&nbsp;</p>
<p>Fue un fracaso colectivo casi inexplicable. La paradoja de Argentina es que ha hecho dos cosas increíbles: Tuvo el default más grande de la historia y el default más pequeño de la historia.</p>
<p>&nbsp;</p>
<p>En 2002 tuvimos un default inmenso y ahí teníamos un problema que la deuda era más grave y teníamos condiciones externas muy adversas. Ahora tenemos buenas condiciones y una deuda pequeña. Argentina es el único país del mundo que logra el default en estas condiciones.</p>
<p>&nbsp;</p>
<p>Yo creo que el Gobierno argentino ha subestimado este problema, ha usado una retórica inconducente de consumo doméstico que no ayuda a resolver problema y me parece que no percibe de la misma manera que yo lo hago, los inmensos costos que tiene el default.</p>
<p>&nbsp;</p>
<p>El Gobierno tiene una visión alegre sobre eso. Uno sabe como entra al default pero uno no sabe como sale de él.</p>
<p>&nbsp;</p>
<p>En este proceso todos aprendimos. Yo creo que los que entraron aprendieron que a los holdouts le fue mucho mejor que a los holdins.</p>
<p>&nbsp;</p>
<p>¿Tu volverías a entrar sabiendo cómo les fue a los que pleitaron? ¿O pleitarías también? Ahora, si todos pleitean, el tema no tiene solución. Es decir, para resolver estos problemas inventaron las leyes de quiebra, las sociedades de responsabilidad limitada.</p>
<p>&nbsp;</p>
<p>Hay en el mundo mecanismos para resolver estos problemas pero no los hay para los deudores soberanos, por eso es tan complejo el tema, por eso todo el mundo es tan prudente en su retórica. Nadie habla sin el abogado al lado. Lo que ayer hizo el ministro Kicillof de hablar después de negociar cinco horas bajo un estrés extraordinario no lo hace nadie porque es muy difícil controlar las emociones en esa circunstancia.</p>
<p>&nbsp;</p>
<p>Y es muy difícil que uno diga cosas que después no tienen arreglo.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>¿Resolverá algo el hecho de que Argentina vaya al tribunal de la Haya a protestar?</strong></p>
<p><strong><br /></strong></p>
<p>Podrás ir a la Haya, protestar, pero eso no va a solucionar el problema. Solo puede darles alivio y gratificarlos emocionalmente. Es como un luto, como un proceso de luto del fracaso. Eso no arregla el problema del default ni el acceso nuestro al mercado de capitales.</p>
<p><strong><br /></strong></p>
<p><strong><br /></strong></p>
<p><strong>En el PanAm Post tenemos muchos lectores venezolanos, ¿cómo ve usted la situación económica de ese país?</strong></p>
<p><strong><br /></strong></p>
<p>Venezuela no está en default. Argentina está en una categoría más baja. El único país latino que está en default en mi conocimiento es Cuba. Estamos en una situación extremadamente crítica. Venezuela no ha dejado de pagar el servicio de su deuda, nosotros sí.</p>
<p>&nbsp;</p>
<p>En el caso de Venezuela, su deuda es significativa, en nuestro caso no. Lo que es increíble es que Argentina tenga un problema con una deuda tan pequeña.</p>
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>El gasto público del Gobierno es un tema recurrente de la crítica opositora. ¿Cómo resolvería el problema del alto gasto público?</strong></p>
<p>&nbsp;</p>
<p>Para ponerlo en perspectiva, el gasto que era 30 puntos del producto [interno bruto] paso a ser 50. Y va a haber que volver a un nivel razonable. Va a haber que ir para abajo. Eso se hará gradualmente si hay recursos y mas enérgicamente si no los hay. A veces uno corrige porque no hay remedio. No siempre es por inteligencia o lucidez.</p>
<p>&nbsp;</p>
<p>No es algo que uno elige. Esa idea de que uno puede hacer o no va a depender de los recursos que tengas.</p>
<p>&nbsp;</p>
<p>El gasto público exagerado e ineficiente lo que hace es bajar el nivel de vida porque baja la creación de puestos de trabajo, baja la productividad, entonces uno puede empobrecerse terriblemente.</p>
<p>&nbsp;</p>
<p>El caso de Venezuela es el caso más claro, es un país inmensamente rico —mucho más que la Argentina— y vive mucho más pobremente porque ha desbordado el sector público y ha quebrado todas las actividades rentables en el país. Eso termina mal.</p>
<p>&nbsp;</p>
<p>Uno puede corregir inteligentemente o porque tuvo una terrible crisis. Yo creo que Maduro y compañía van a corregir ahora no porque se han vuelto lúcidos sino porque no tienen más espacio, la situación se les ha hecho inmanejable.</p>
<p><strong><br /></strong></p>
<p><strong><br /></strong></p>
<p><strong>¿Cómo resumiría brevemente la década kirchnerista en Argentina?</strong></p>
<p>&nbsp;</p>
<p>Una década desperdiciada con un enorme costo para las generaciones más jóvenes, un lastre muy grande para nuestro futuro y a mi me produce una inmensa tristeza.</p>
<p>&nbsp;</p>
<p>Me genera esto por lo que perdimos, por lo que desaprovechamos, por las consecuencias que van a quedar, por haber hecho tanto daño a nuestra cultura y a nuestro sistema de pensamiento.</p>
<p>&nbsp;</p>
<p>Se ha machacado tanto con una retórica disparatada que los argentinos nos hemos deseducado, hemos perdido nuestra capacidad de entender nuestro dilema por eso nos pasan cosas patológicas como el caso de la deuda.</p>
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>¿Qué opina de la polémica secretaría de Pensamiento Nacional que creó el gobierno semanas atrás?</strong></p>
<p><strong><br /></strong></p>
<p>La idea de que uno pueda imponer un pensamiento único desde el Estado endiosando a sus líderes es el paradigma del totalitarismo. Se relaciona con hacer del Estado el centro de nuestra vida y endiosar al líder de la experiencia estalisnista, maoísta, la experiencia fascista. Se subordinan los derechos individuales y las libertades al Estado. Esto es lo que define al colectivismo que para mí es la pesadilla del siglo XX.</p>
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>¿Dónde está parado el liberalismo en la región?</strong></p>
<p>&nbsp;</p>
<p>Yo lo veo en una enérgica lucha ideológica, conceptual y cultural. La batalla más importante es la batalla por las ideas. Ojalá maduremos en esta batalla para poder librar una en el campo electoral.</p>
<p>&nbsp;</p>
<p>Pero el primer plano precede al segundo. Los valores de la libertad, los éxitos de la libertad, el tipo de sociedad que ha construido la libertad, me parece que no tienen comparación. Cuando uno lo mira desde un ángulo racional es casi imposible creer que alguien elija el otro camino.</p>
<p>&nbsp;</p>
<p>En este sentido hemos cumplido en resistir la ola colectivista y totalitaria y pasar vigorosamente a la ofensiva. Nadie nos ve ahora callados.</p>
<p>&nbsp;</p>
<p>Han surgido jóvenes líderes que con un fervor extraordinario en sociedades donde la agresión a la libertad es tremenda están dispuestos a jugarse la vida en las calles. Yo eso no lo había visto en mi juventud. Los que venimos hace años batallando por estas ideas hoy sentimos una compañía inmensa. Y eso a mi me llena de placer.</p>
<p>&nbsp;</p>
<p>Parte de esto es un proceso educativo, de la difusión de las ideas. Esa batalla cultural no ha concluido, al contrario, la estamos librando. Desgraciadamente, hay mucha gente que vive del esperpento totalitario.</p>
<p>&nbsp;</p>
<p>Yo diría que una de las cosas que mas me han impactado es la percepción del éxito y del crecimiento de estas ideas en la literatura, en el debate público, en los jóvenes.</p>
<p>&nbsp;</p>
<p>Te lo voy a decir con emoción: nada me gustaría más que vivir para verlo. Lo que ha sido casi una cátedra, se convierta en un foro, y que el foro se convierta en una legión de jóvenes que defiendan estas ideas. A esto le he dedicado mi vida, así que imagínate lo que me gustaría que estas ideas prevalecieran.</p>
<p>&nbsp;</p>
<p>En este sentido somos invencibles, porque nos tienen que matar para quebrarnos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Entrevista realizada por <strong>Belén Marty,&nbsp;</strong>Periodista y conductora de un programa de radio. Porteña de nacimiento, vivió en Guatemala, Estados Unidos, Emiratos Árabes Unidos y Jordania. Ferviente defensora de la libertad. Síguela en Twitter: @BelenMarty.</p>
<p>Para&gt; <a href="http://panampost.com/">PANAMPOST</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/417-celebrando-a-milton-friedman-en-sus-102-años">
			&laquo; Celebrando a Milton Friedman en sus 102 años		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/424-alerta-por-los-derechos-humanos-de-activista-lgbti-en-venezuela">
			Alerta por los Derechos Humanos de activista LGBTI en Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/420-ricardo-lópez-murphy-“el-default-de-argentina-fue-un-fracaso-colectivo-inexplicable”#startOfPageId420">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:99:"Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable” - Relial";s:11:"description";s:156:"&amp;nbsp; Entrevista a Ricardo López Murphy, Presidente de RELIAL &quot;El núcleo de la amenaza era el default. Argentina tenía tres características...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:90:"Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable”";s:6:"og:url";s:155:"http://relial.org/index.php/productos/archivo/actualidad/item/420-ricardo-lópez-murphy-“el-default-de-argentina-fue-un-fracaso-colectivo-inexplicable”";s:8:"og:title";s:99:"Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/91b1b90c684fd8e5c2ec1b7418ca380f_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/91b1b90c684fd8e5c2ec1b7418ca380f_S.jpg";s:14:"og:description";s:164:"&amp;amp;nbsp; Entrevista a Ricardo López Murphy, Presidente de RELIAL &amp;quot;El núcleo de la amenaza era el default. Argentina tenía tres características...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:90:"Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}