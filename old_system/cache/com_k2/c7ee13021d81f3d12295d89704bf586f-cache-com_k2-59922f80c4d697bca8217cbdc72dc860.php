<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10951:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId346"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Unión civil y libertad en el Perú
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f41bf091a4e18f2312495cc0e975d9f7_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f41bf091a4e18f2312495cc0e975d9f7_XS.jpg" alt="Uni&oacute;n civil y libertad en el Per&uacute;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinón de Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>Antes de escribir sobre la propuesta de la unión civil entre personas de la misma preferencia sexual en el Perú, considero pertinente expresar, lo más claramente que pueda, mi posición sobre estos vínculos. Creo que el Estado no debe intervenir, de ningún modo, en la unión entre dos personas para convivir. El Estado no debe sancionar ni entrometerse en la relación privada más íntima de dos seres humanos, como no debe hacerlo cuando compran una casa, adquieren un seguro, o deciden en qué colegio deben estudiar sus hijos. Por ende, a mi entender, la tarea correcta que un liberal debería acometer al respecto es la de luchar por privatizar las uniones civiles entre dos personas, del mismo modo que debería combatir por la privatización de la educación pública o de los servicios médicos a cargo del Estado.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Por supuesto, la unión entre dos personas de la misma preferencia sexual se incluye dentro de los vínculos en los que el Estado no debería intervenir. Desde una perspectiva liberal o libertaria, la vida más íntima de ninguna persona puede ser objeto de una pesquisa, reconocimiento o sanción por parte del Estado o los particulares. Nunca. Eso comprende, por cierto, a los que hacen pública su preferencia sexual; peor todavía, sin ser consultados por ella. Los que creemos en la libertad, respetamos su privacidad y no estamos interesados en saberla.</p>
<p>&nbsp;</p>
<p>Ahora bien, las uniones privadas a las que aludo pueden ser entre dos personas, o varias; entre personas de la misma o distinta preferencia sexual; entre personas que hayan transformado su apariencia por la de una mujer, o un varón; por un tiempo o por toda la vida; para que una mantenga al otro, o viceversa; un varón y una mujer (o varias mujeres) unidos por toda su existencia según la religión que profesan; y un larguísimo etcétera, sin que a nadie interese o importe los términos de tales vínculos más que a los involucrados.</p>
<p>&nbsp;</p>
<p>Esa perspectiva que acabo de describir someramente no se encuentra por ningún lado en la propuesta que se promueve por estos días en el Perú. Tal como está planteada, la iniciativa busca que el Estado sancione un vínculo de convivencia adicional al del matrimonio y el concubinato, con privilegios exclusivos para un tipo específico de preferencia sexual. En consecuencia, no es una defensa a favor de la libertad; es a favor de la coerción. Si, como los activistas de esta proposición sostienen, "nadie, ni siquiera aquello llamado 'Estado' o 'Iglesia' puede imponerte formas de vida" prescribir una nueva forma de vida –más preciso decir, de convivencia mutua– entre dos personas por la vía coercitiva del Estado, que legisla y ordena mandatos y cumplimientos, es exactamente lo contrario a la libertad que dicen enarbolar. Por otra parte, las iglesias (en plural) amparan, y es su derecho, un modo de convivencia acorde con sus respectivas doctrinas. Lo que desafía al sentido común es convertir esa defensa legítima en una imposición a las personas, cuando la decisión de formar parte de una de muchas religiones es del libre albedrío individual, pudiendo uno renunciar en cualquier momento a ellas, y siempre, cuestionarlas, como a lo largo de milenios de historia. La contradicción es evidente.</p>
<p>&nbsp;</p>
<p>Se sostiene que "la aprobación del proyecto no va a perjudicar los derechos de los heterosexuales". Es claro que no se ha leído la iniciativa que se defiende, misma que propone que el reconocimiento de la unión civil sea inmediato, en tanto que, en el caso de los convivientes o concubinos en el Perú, deben esperar hasta dos años para ser reconocidos. Discriminación evidente contra quienes han elegido una forma de convivencia, sobre todo con la parte más desprotegida de dicha relación: es decir, las mujeres. Asimismo, el planteamiento otorga una serie de prerrogativas de orden tributario, pensionario y patrimonial instantáneos, y que, en el caso del concubinato, requieren de una larga probanza. De esta suerte, la propuesta no instituye tampoco la igualdad, sino por el contrario, un régimen de discriminación positiva y de privilegio a favor de una minoría de personas en razón de su preferencia sexual en el Perú. Entonces, esta propuesta sí daña los derechos de otros, como las convivientes peruanas, que ahora deben padecer –de aprobarse así– una situación de privilegio que las discrimina, además de la cotidiana violencia doméstica de sus parejas y de una situación de desamparo cuando son abandonadas, hechos que no conmueven ni llaman a la acción a los activistas del proyecto de ley.</p>
<p>&nbsp;</p>
<p>Finalmente, los liberales de todas las tendencias nos caracterizamos por no ser deterministas. Ninguna situación ocurre de modo inevitable ni definitivo (léase, determinado) por las condiciones que le preceden, la causan o coexisten con ella. En el caso que nos concierne, las preferencias sexuales no son inevitables ni definitivas; más todavía, al estar directamente relacionadas con el placer y el amor, cambian constantemente. El liberalismo –a diferencia de las demás ideologías– considera que los seres humanos somos complejos, contradictorios, cambiantes, únicos, extraordinarios, e irrepetibles. En consecuencia, combate sin desmayo a quienes quieren clasificar (o determinar) a las personas sólo y a rajatabla por su raza, su color de piel, su idioma, su "clase", y, por cierto, su preferencia particular en el acto amatorio.</p>
<p>&nbsp;</p>
<p>Se ha visto, para concluir, que esta iniciativa es defendida con ardor por representantes de la izquierda peruana. Y eso es correcto, coherente y consistente con dicha tendencia. Los socialistas siempre han luchado por la intervención del Estado en la vida íntima de las personas; por imponer relaciones de privilegio entre sectores de la sociedad, clasificándolos según su ideología; siempre han creído que el ser humano está determinado por sus condiciones previas o presentes y no puede salir de ellas. Lo sorprendente es que algunos liberales lo hagan. Por supuesto, tendrá más aceptación por la izquierda el liberal que defiende la unión civil antes que la privatización de la educación pública o la drástica reducción del Estado. Y es su opción. Pero es una opción que no comparto. La historia reciente nos está enseñando que la izquierda, como antes los conservadores, aprovechan a los liberales para facilitar la realización de sus programas en la sociedad. Ésta, y nosotros, perdimos en grado sumo. Los invoco a no cometer el mismo error. Todavía están a tiempo.</p>
<p>&nbsp;</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Autor:&nbsp;Héctor Ñaupari,&nbsp;<span style="font-size: 8pt; line-height: 115%; font-family: Arial, sans-serif;">Ex presidente de la Red Liberal de América Latina (RELIAL). Autor de <i>Sentido liberal, el sendero urgente de la libertad</i>, <i>La nueva senda de la libertad</i> y <i>Libertad para todos</i>, entre otras publicaciones.</span></p>
<p>Foto: IEAH</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina">
			&laquo; Dónde se vive mejor o peor en América Latina		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios">
			El secreto de los estados totalitarios &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú#startOfPageId346">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:44:"Unión civil y libertad en el Perú - Relial";s:11:"description";s:157:"En la opinón de Héctor Ñaupari &amp;nbsp; Antes de escribir sobre la propuesta de la unión civil entre personas de la misma preferencia sexual en el P...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:35:"Unión civil y libertad en el Perú";s:6:"og:url";s:102:"http://www.relial.org/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú";s:8:"og:title";s:44:"Unión civil y libertad en el Perú - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f41bf091a4e18f2312495cc0e975d9f7_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f41bf091a4e18f2312495cc0e975d9f7_S.jpg";s:14:"og:description";s:161:"En la opinón de Héctor Ñaupari &amp;amp;nbsp; Antes de escribir sobre la propuesta de la unión civil entre personas de la misma preferencia sexual en el P...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:35:"Unión civil y libertad en el Perú";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}