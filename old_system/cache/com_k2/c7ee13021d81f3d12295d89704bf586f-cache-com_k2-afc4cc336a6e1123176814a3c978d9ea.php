<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9665:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId512"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El líder lilght, el caudillo malo y el profeta enardecido
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/37e331b7a1b39f090b1249a069a513d3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/37e331b7a1b39f090b1249a069a513d3_XS.jpg" alt="El l&iacute;der lilght, el caudillo malo y el profeta enardecido" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El 1 de marzo será el Super martes. Doce estados norteamericanos realizarán sus primarias en una atmósfera de suspense que tiene mucho de pasión irracional. Será un "duelo al sol", pero a tres pistolas, como se vio en el debate de la noche del jueves, ganado claramente por Marco Rubio en el O.K. Corral de CNN y Telemundo. Quien salga victorioso el Super martes poseerá una altísima probabilidad de ser el candidato de su partido.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>En los años sesentas, Sergio Leone, director de cine italiano, llevó a la cumbre el spaghetti western con una trilogía de películas que, de paso, dieron a conocer a Clint Eastwood. Uno de aquellos filmes, el más famoso, se conoció como El bueno, el malo y el feo.</p>
<p>&nbsp;</p>
<p>Marco Rubio es el bueno. Tiene una cara juvenil de muchacho bondadoso que lleva la abuela a la terapia los sábados por la mañana. Es un líder light. Alguien que es seguido por un tipo de adhesión en la que no entra la fe incondicional. Es gentil y risueño. Tiene en su haber una hermosa familia y una impresionante cadena de triunfos políticos, pero lo acusan de ser un peso ligero. Probablemente lo sea, aunque sospecho que ese rasgo no es un problema serio. Lamentablemente, la mayor parte de los políticos son pesos ligeros. También lo acusan de hablar español y ser bicultural (como a Mitt Romney le imputaban hablar francés, como si ese raro conocimiento en un país monolingüe fuera un oscuro delito).</p>
<p>&nbsp;</p>
<p>Lo verdaderamente grave en Rubio es su falta de empatía con la tragedia de los indocumentados, y muy especialmente la de los jóvenes dreamers. Para alguien que ha convertido en un ritornello la idea del "sueño americano", esa dureza, real o impostada, contra jóvenes traídos por sus padres a Estados Unidos cuando eran unos niños, personas que son cultural, emocional e intelectualmente norteamericanas, aunque no lo sean legalmente, es una penosa contradicción. Si realmente lo cree, tiene muy poco corazón. Si lo dice para contentar a la derecha republicana, tiene muy poca espina dorsal.</p>
<p>&nbsp;</p>
<p>Donald Trump es el malo. Le encanta serlo. Es un bully y por eso mucha gente lo sigue. Los bullies arrastran a cierto tipo de ciudadanos. Mussolini o Hitler eran bullies. Trump no es un líder, sino un caudillo. A los caudillos se les obedece incondicionalmente porque Dios te libre de no hacerlo. Su gesticulación es la de una persona siempre colérica a punto de propinarte una bofetada o de ordenarles a los guardaespaldas que te den una paliza. Pone cara de malo, eleva el mentón, cierra los ojitos y saca el pecho porque le gusta intimidar. Su castigo comienza con los gestos. Se convirtió en una celebridad cesanteando ejecutivos en un programa de televisión llamado "El Aprendiz" (The Apprentice). Su inconfundible consigna de batalla era gritarle al concursante: ¡you're fired!</p>
<p>&nbsp;</p>
<p>Trump es un populista de derechas. Es autoritario, nacionalista, y proteccionista, como todos ellos. Hay mucho de racismo en su ideología. Algunos de sus partidarios difunden una vergonzosa consigna supremacista: "vote por Trump, no por los dos cubanos". Los "cubanos" son Rubio y Cruz, dos estadounidenses hijos de cubanos. Por la misma regla racista se podría decir "no vote por el alemán Trump", pero sería igualmente injusto. Trump desciende de alemanes, pero es tan gringo como el pie de manzana.</p>
<p>&nbsp;</p>
<p>Para Trump, el esplendor norteamericano se consigue por medio de opacar el de los mexicanos, japoneses, chinos y surcoreanos. Si lo dejan, hará una enorme muralla en la frontera sur. Está decidido a que la paguen los mexicanos, incluso Vicente Fox, que se niega vehementemente. Quiere exportar a manos llenas, pero impedir las importaciones porque no cree en la libre elección de los consumidores. En su hipotético gobierno todo el que lo contradiga será castigado al grito de you're fired! Si pudiera, los fusilaba al amanecer. Menos mal que no puede.</p>
<p>&nbsp;</p>
<p>Ted Cruz es el feo. Es un hombre inteligente lleno de certezas. Esa combinación entre un IQ muy alto y unas convicciones muy firmes suele transformarse en una repelente inflexibilidad. Por eso es el feo. No conoce la duda ni le preocupa el ridículo. Puede hablar sin ton ni son durante dos días en el senado para tratar de boicotear inútilmente una legislación. Su personalidad se ha fosilizado en un sistema binario de blancos y negros. No caben los grises. Está seguro de que la Biblia es el libro que contiene todo lo bueno y noble que debe preservarse en el terreno espiritual. Y está seguro, también, de que la Constitución de 1787, con las enmiendas y el Bill of Rights, es la única fuente de las virtudes ciudadanas. Por eso puede ser durísimo con los pobres inmigrantes. Estos tipos han mentido. Es verdad que lo han hecho para sobrevivir, pero han mentido. Han pecado. Han violado las leyes. Viven escondidos. Las mujeres pro-choice y los matrimonios gays se quemarán para siempre en el infierno. Cruz no conoce la compasión, sino las reglas. Hay que castigarlos y extirparlos del país. No es un líder light, como Rubio, ni un caudillo, como Trump. Es un enardecido profeta, como Jeremías, que se inspira todos los días en el Libro de las lamentaciones.</p>
<p>&nbsp;</p>
<p>¿Quién ganará el martes? ¿El líder light, el caudillo malo o el profeta enardecido? Según las encuestas, Donal Trump, el caudillo bully, malo como un forúnculo en la abertura del recto, encabeza el pelotón. Mala cosa. Como diría Ted Cruz: "Dios nos coja confesados". Ya se sabe que no es el fin del combate electoral, pero es una batalla muy importante.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: El Blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/507-la-pata-podrida">
			&laquo; La pata podrida		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
			Perú: retos de gestión para el próximo gobierno &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido#startOfPageId512">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:67:"El líder lilght, el caudillo malo y el profeta enardecido - Relial";s:11:"description";s:157:"El 1 de marzo será el Super martes. Doce estados norteamericanos realizarán sus primarias en una atmósfera de suspense que tiene mucho de pasión irrac...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:58:"El líder lilght, el caudillo malo y el profeta enardecido";s:6:"og:url";s:126:"http://www.relial.org/index.php/productos/archivo/opinion/item/512-el-lÃ­der-lilght-el-caudillo-malo-y-el-profeta-enardecido";s:8:"og:title";s:67:"El líder lilght, el caudillo malo y el profeta enardecido - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/37e331b7a1b39f090b1249a069a513d3_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/37e331b7a1b39f090b1249a069a513d3_S.jpg";s:14:"og:description";s:157:"El 1 de marzo será el Super martes. Doce estados norteamericanos realizarán sus primarias en una atmósfera de suspense que tiene mucho de pasión irrac...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:58:"El líder lilght, el caudillo malo y el profeta enardecido";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}