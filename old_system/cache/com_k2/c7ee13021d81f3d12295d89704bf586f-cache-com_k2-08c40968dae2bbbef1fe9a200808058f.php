<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17420:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Opinión</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo">
	  		Memoria del saqueo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo" title="Memoria del saqueo">
		    	<img src="/media/k2/items/cache/8ee107fb8e11fa27c5eb0c84c03d7dff_XS.jpg" alt="Memoria del saqueo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p style="text-align: justify;" align="center"><span style="line-height: 115%;">En la opinión de José Guillermo Godoy,&nbsp;Las jornadas del 9, 10 y 11 de diciembre en Tucumán (Argentina).&nbsp;</span></p>
<p style="text-align: justify;" align="center"><span style="text-align: left;">A comienzos del siglo XX, Tucumán tenía los índices de crecimiento económico más alto de Argentina, que por entonces ostentaba los niveles de desarrollo más altos del mundo, en claro contraste con sus pares en el continente. Pero, en el devenir del siglo XX el signo de la decadencia dominó el ritmo de la escena. Argentina se latinoamericanizó y Tucumán se argentinizó. La irrupción del kirchnerismo marca una primera década del siglo XXI que mirada a la distancia nos revela una atmosfera estancada y muerta. En la actualidad, en el caótico paisaje político y social que nos desvela, parecen ganar terreno la inconformidad y la desesperanza ante un destino social que se percibe como inevitable.</span></p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente?">
	  		Argentina: Saqueos en ¿un país con buena gente?	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente?" title="Argentina: Saqueos en &iquest;un pa&iacute;s con buena gente?">
		    	<img src="/media/k2/items/cache/e303e2027514497aaa0603a129a3eb42_XS.jpg" alt="Argentina: Saqueos en &iquest;un pa&iacute;s con buena gente?" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Profunda pena me produce ser testigo de los hechos que transcurren hoy en Argentina. El fin de ciclo llegó antes de lo que se pronosticaba y con él parece haber llegado el incendio de todo el país. Los saqueos, el silencio de nuestros dirigentes, la figura cuasi-ausente de Cristina Fernández de Kirchner y el acuartelamiento de la fuerzas de seguridad se han convertido en la postal diaria de los últimos días. El modelo no funciona -nunca funcionó- pero hoy se pueden palpar los resultados que realmente arrojó. Lamentablemente es tarde, por lo que necesitamos con urgencia que los cambios se produzcan y a partir de eso - y de una buena vez por todas - construir una república democrática real basada en el estado de derecho, las libertades individuales y la libre empresa y dejar de lado festejos populistas ridículos sobre una democracia que no sirve.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente?">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste">
	  		Hay una camino, pero no es éste	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste" title="Hay una camino, pero no es &eacute;ste">
		    	<img src="/media/k2/items/cache/0bfc0ce99892772fc285e10ee3943d9a_XS.jpg" alt="Hay una camino, pero no es &eacute;ste" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Miguel Velarde</p>
<p>&nbsp;</p>
<p>Basta de buscar victorias donde no existen. Dada la coyuntura y la grave crisis por la que atraviesa el país, los resultados de las elecciones municipales del domingo no fueron buenos para la oposición. La estrategia fue convertir esa contienda electoral en un plebiscito que demostrase que somos mayoría y que Nicolás Maduro no ganó las elecciones del 14 de abril; fracasamos en ese intento.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela">
	  		La grandeza de Mandela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela" title="La grandeza de Mandela">
		    	<img src="/media/k2/items/cache/8012f255a337782bffaadea968723f36_XS.jpg" alt="La grandeza de Mandela" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Nelson Mandela, una de las figuras más nobles y admirables del siglo XX, fue amigo de Fidel Castro. ¿Y qué? David Rockefeller también presumía de los mismos lazos. Carlos Andrés Pérez, hasta poco antes de exiliarse, pensaba que Fidel era amigo suyo. Al fin y al cabo, cuando Hugo Chávez, en 1992, trató de derrocarlo a tiros, y dejó decenas de muertos en las calles de Caracas, Fidel le mandó un mensaje de solidaridad a CAP y condenó la acción fascista del teniente coronel.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo">
	  		Honduras o el fin del chavismo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo" title="Honduras o el fin del chavismo">
		    	<img src="/media/k2/items/cache/fedea746cd0ecb257a1249d3a2a80bb1_XS.jpg" alt="Honduras o el fin del chavismo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Las mejores encuestas lo advirtieron una semana antes. Juan Orlando Hernández, al frente del Partido Nacional, le sacaría entre 5 y 6 puntos de ventaja a Xiomara Castro, la mujer de Mel Zelaya, cabeza nominal del partido Libre. Y así fue: votó un 60% del censo electoral y JOH obtuvo el 35% de los sufragios. Xiomara Castro, como testaferro de su marido, recibió el 29%.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde">
	  		JFK y Castro se encuentran medio siglo más tarde	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde" title="JFK y Castro se encuentran medio siglo m&aacute;s tarde">
		    	<img src="/media/k2/items/cache/27b4275cdf67fac8ef7af010ec180724_XS.jpg" alt="JFK y Castro se encuentran medio siglo m&aacute;s tarde" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Se alborotó el avispero a los 50 años del asesinato de John F. Kennedy. El Secretario de Estado John Kerry no descarta que Fidel Castro o los soviéticos estuvieran detrás de esa muerte. Lo acaba de afirmar a media lengua. No cree, como medio país, la tesis oficial de que Lee Harvey Oswald era un loco suelto que actuó por su cuenta y riesgo.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/226-jfk-y-castro-se-encuentran-medio-siglo-más-tarde">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/opinion?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/opinion?start=66" class="pagenav">Anterior</a></li><li><a title="6" href="/index.php/productos/archivo/opinion?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/opinion?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/opinion?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/opinion?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/opinion?start=54" class="pagenav">10</a></li><li><a title="11" href="/index.php/productos/archivo/opinion?start=60" class="pagenav">11</a></li><li><a title="12" href="/index.php/productos/archivo/opinion?start=66" class="pagenav">12</a></li><li><span class="pagenav">13</span></li><li><a title="14" href="/index.php/productos/archivo/opinion?start=78" class="pagenav">14</a></li><li><a title="15" href="/index.php/productos/archivo/opinion?start=84" class="pagenav">15</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/opinion?start=78" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/opinion?start=84" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 13 de 15	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:17:"Opinión - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:66:"http://www.relial.org/index.php/productos/archivo/opinion?start=72";s:8:"og:title";s:17:"Opinión - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:47:"http://www.relial.org/media/k2/categories/2.jpg";s:5:"image";s:47:"http://www.relial.org/media/k2/categories/2.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}}s:6:"module";a:0:{}}