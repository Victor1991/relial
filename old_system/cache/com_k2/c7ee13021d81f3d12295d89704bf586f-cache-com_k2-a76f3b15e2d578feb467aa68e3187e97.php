<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7919:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId290"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Declaración sobre la situación en Venezuela (Federación Internacional de Juventudes Liberales)
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/6c16f95e5837b7a15cc22a32eb72fad8_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/6c16f95e5837b7a15cc22a32eb72fad8_XS.jpg" alt="Declaraci&oacute;n sobre la situaci&oacute;n en Venezuela (Federaci&oacute;n Internacional de Juventudes Liberales)" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: center;">DECLARACIÓN DE IFLRY -</p>
<p>&nbsp;</p>
<p>La Federación Internacional de Juventudes Liberales (IFLRY) está profundamente preocupada con los informes internacionales que surgen de Venezuela en los últimos días. Las manifestaciones pacíficas que comenzaron el 12 de febrero en varias ciudades de Venezuela se han convertido en enfrentamientos violentos y han resultado en muertes innecesarias, detenciones y arrestos por parte del gobierno venezolano.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>La IFLRY condena firmemente la represión de las autoridades del gobierno contra manifestantes pacíficos, en particular los estudiantes, que quieren destacar: restricciones a las libertades democráticas, las desigualdades sociales y económicas, y los altos niveles de criminalidad. El derecho de protestar y la libertad de expresión son derechos humanos fundamentales.</p>
<p>&nbsp;</p>
<p>La IFLRY se ve aún más perturbada por el presunto bloqueo de Twitter y otras formas de medios de comunicación social. Como los medios de comunicación de propiedad estatal y privada en Venezuela están sujetos a la censura y la intimidación, muchos venezolanos dependen de la Internet y las redes sociales para mantenerse informados. Cualquier intento por parte de las autoridades para limitar el acceso a la información debe ser severamente reprendido.</p>
<p>&nbsp;</p>
<p>Los jóvenes liberales de todo el mundo hacen un llamamiento para la liberación de los activistas cívicos, los líderes estudiantiles y los dirigentes de la oposición detenidos sin el debido proceso judicial. La IFLRY cree que el gobierno venezolano ha actuado con dureza y con un nivel de fuerza que no se ajusta a las normas democráticas. La IFLRY pide a sus asociados de todo el mundo para presionar en favor de una solución pacífica y democrática, y seguirá poniendo de relieve cualquier abuso denunciado.</p>
<p>&nbsp;</p>
<p>Para comentarios y preguntas: www.iflry.org – <span id="cloak68604">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak68604').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy68604 = '&#111;ff&#105;c&#101;' + '&#64;';
 addy68604 = addy68604 + '&#105;flry' + '&#46;' + '&#111;rg';
 document.getElementById('cloak68604').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy68604 + '\'>' + addy68604+'<\/a>';
 //-->
 </script></p>
<p>La Federación Internacional de Juventudes Liberales (IFLRY) es la mayor organización internacional de jóvenes liberales. Consiste en una unión internacional de mas de 100 juventudes nacionales, generalmente, pero no siempre, afiliadas con partidos políticos miembros de la Internacional Liberal. La IFLRY continúa la tradición de otras dos organizaciones que la precedieron. La primera fue la Federación Mundial de Juventudes Liberales y Radicales (WFLRY), fundada en 1947. La WFLRY era también una organización internacional, pero la mayor parte de los miembros estaba activa en Europa. Ello llevó a la creación, en 1969, de la Federación Europea de Juventudes Liberales y Radicales (EFLRY). La WLFRY fue disuelta en 1978 y la EFLRY cambió su nombre por IFLRY, Federación Internacional de Juventudes Liberales y Radicales. En 2001 la organización cambió su nombre por IFLRY - Federación Internacional de Juventudes Liberales.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/289-cedice-libertad-ante-los-atentados-a-las-libertades-en-venezuela--un-gobierno-debe-garantizar-las-condiciones-de-respeto-reconocimiento-y-libertades-que-permita-el-concurso-de-todos-los-ciudadanos-en-la-solución-de-los-problemas-del-país">
			&laquo; CEDICE Libertad  Ante los atentados a las Libertades en Venezuela . Un gobierno debe garantizar las condiciones de respeto, reconocimiento y libertades que permita el concurso de todos los ciudadanos en la solución de los problemas del país.		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks">
			Think Tanks And The Power of Networks &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/290-declaración-sobre-la-situación-en-venezuela-federación-internacional-de-juventudes-liberales#startOfPageId290">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:106:"Declaración sobre la situación en Venezuela (Federación Internacional de Juventudes Liberales) - Relial";s:11:"description";s:156:"DECLARACIÓN DE IFLRY - &amp;nbsp; La Federación Internacional de Juventudes Liberales (IFLRY) está profundamente preocupada con los informes internaci...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:97:"Declaración sobre la situación en Venezuela (Federación Internacional de Juventudes Liberales)";s:6:"og:url";s:165:"http://www.relial.org/index.php/productos/archivo/actualidad/item/290-declaración-sobre-la-situación-en-venezuela-federación-internacional-de-juventudes-liberales";s:8:"og:title";s:106:"Declaración sobre la situación en Venezuela (Federación Internacional de Juventudes Liberales) - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/6c16f95e5837b7a15cc22a32eb72fad8_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/6c16f95e5837b7a15cc22a32eb72fad8_S.jpg";s:14:"og:description";s:160:"DECLARACIÓN DE IFLRY - &amp;amp;nbsp; La Federación Internacional de Juventudes Liberales (IFLRY) está profundamente preocupada con los informes internaci...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:97:"Declaración sobre la situación en Venezuela (Federación Internacional de Juventudes Liberales)";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}