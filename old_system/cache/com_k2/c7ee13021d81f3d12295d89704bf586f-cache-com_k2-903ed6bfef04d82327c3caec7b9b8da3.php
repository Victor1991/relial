<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9524:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId378"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	IAF curso Profiling Political Liberalism as an Effective Force for Progress
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/24051f1a73d7ccf80aa0e781a6178da9_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/24051f1a73d7ccf80aa0e781a6178da9_XS.jpg" alt="IAF curso Profiling Political Liberalism as an Effective Force for Progress" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><strong>Contenido del programa</strong></p>
<p>&nbsp;</p>
<p>El objetivo general del taller fue debatir acerca de cuál debería ser la respuesta de los lideres de think tanks y de políticos liberales ante el desafío que implica el rápido avance de la tecnología y la innovación que, de acuerdo con muchos, ha llegado a formar una sociedad basada en el la transmisión del conocimiento de manera cada vez más rápida y efectiva.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Puntualmente se debatieron temas como las políticas migratorias de puertas abiertas, la pertinencia o no de mantener derechos de propiedad intelectual y en qué áreas, el "principio de precaución" y su impacto en la innovación, privacidad y vigilancia en el ciber-espacio, el potencial de los emprendimientos privados las nuevas tecnologías en el campo de la educación.</p>
<p>&nbsp;</p>
<p>Puntos positivos:</p>
<p>• Relevancia y actualidad: El programa en su generalidad ha sido muy relevante en relación a los cambios que se están dando actualmente en el mundo.</p>
<p>&nbsp;</p>
<p>• Expositores: En general los expositores mostraron un gran conocimiento de los temas que se debatieron.</p>
<p>&nbsp;</p>
<p>• Metodología de trabajo: La metodología preparada por los moderadores para las sesiones de trabajos de grupo ayudó a que sea posible combinar las habilidades de cada participante (investigación, política, filosofía) para poder entregar resultados no solo principistas sino también prácticos.</p>
<p>&nbsp;</p>
<p>Puntos a mejorar:</p>
<p>• Aplicabilidad de los temas a la realidad de países en desarrollo: La evidencia no es conclusiva acerca de la aplicabilidad o no de los derechos de propiedad intelectual. Durante el curso recibimos la visión de sólo el lado que opina que efectivamente los derechos de propiedad intelectual desincentivan la innovación. Sin embargo, en países en desarrollo, como la mayoría de los países de América Latina, donde los derechos de propiedad en sí mismo son cuestionables, hablar de la aplicabilidad de los derechos de propiedad intelectual forma parte de un debate todavía no explorado.</p>
<p>&nbsp;</p>
<p>• Tiempo en las plenarias: Los debates en las plenarias tomaban mucho tiempo debido a que algunos participantes no respetaban el tiempo para intervenir y por momentos se volvía un dialogo entre dos posturas sin tomar en consideración el resto, por falta de tiempo</p>
<p>&nbsp;</p>
<p>Puntos para darle seguimiento</p>
<p>Educación privada de calidad para los pobres: El tema que creo podría ser muy explotado en mi país y en la región es el del fenómeno que se da en varios países en desarrollo como India y algunos de África, que es la educación privada de calidad para los pobres. Actualmente en la región se debate acerca de la calidad de la educación, su gratuidad o no, y los resultados en términos de un capital humano verdaderamente capacitado para el mercado laboral, y lo que verdaderamente demandan las empresas, a la par que se cuestiona el hecho que el sistema educativo, al ser un monopolio controlado por el Estado (la educación fiscal) y que regula fuertemente la educación privada, podría estar generando un capital humano preparado solo para ser trabajadores, y no para emprendedores.</p>
<p>&nbsp;</p>
<p><strong>Compañeros de seminario</strong></p>
<p>El grupo en general era muy diverso, no solo por el país de procedencia, sino también por su bagaje profesional. En su totalidad todos dominaban muy bien los temas que se debatían, brindaban ejemplos concretos de sus países en base a sus propias realidades.</p>
<p>&nbsp;</p>
<p>Para este seminario, la delegación de América Latina era la más numerosa. De los 24 participantes, 8 éramos de América Latina (30%), con lo que muchos coincidimos en nuestras apreciaciones.</p>
<p>&nbsp;</p>
<p><strong>Propuesta personal</strong></p>
<p>Creo que esta actividad podría muy bien ser aplicada en mi país. Bolivia está viviendo muchos cambios desde la nueva Constitución Política del Estado aprobada en el año 2009. El Estado es un actor relevante y el más importante en la actividad económica, y políticamente la institucionalidad está cada vez más cuestionada en el sentido que se duda cada vez mas de la imparcialidad del sistema judicial, de la policía, la percepción de elevada corrupción no ha cambiado, y las reformas estructurales en materia económica que Bolivia necesita no ha sido llevadas a cabo debido a que el Estado descansa sobre los recursos que llegan de la exportación de gas natural a Brasil y Argentina, los mismo que son de propiedad exclusiva del Estado, ya que toda la cadena de hidrocarburos en Bolivia está nacionalizada, así como otras como la generación eléctrica, y el transporte aéreo.</p>
<p>&nbsp;</p>
<p>Lo aprendido en el Seminario lo pienso aplicar en mi institución realizando un ciclo de charlas y conferencia con líderes intelectuales del país acerca de la relevancia de la educación privada, la calidad de la educación pública y privada, y el empoderamiento de los pobres a través de un elevado capital humano.</p>
<p>&nbsp;</p>
<p><strong>Comentarios</strong></p>
<p>Agradecido por la oportunidad de ser parte de este Seminario, e ingresar al grupo de ex alumnos de la Academia para la Libertad de la Fundación Friedrich Naumann.</p>
<p>&nbsp;</p>
<p>La organización estuvo impecable, de principio a fin, y me llamó la atención el alto nivel de la calidad humana de los organizadores y de los moderadores.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Sergio Daga</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: RELIAL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/362-el-mundial-de-la-calidad-institucional">
			&laquo; El mundial de la Calidad Institucional		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/379-sobre-la-libertad">
			Sobre la libertad &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/378-iaf-curso-profiling-political-liberalism-as-an-effective-force-for-progress#startOfPageId378">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:84:"IAF curso Profiling Political Liberalism as an Effective Force for Progress - Relial";s:11:"description";s:155:"Contenido del programa &amp;nbsp; El objetivo general del taller fue debatir acerca de cuál debería ser la respuesta de los lideres de think tanks y d...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:75:"IAF curso Profiling Political Liberalism as an Effective Force for Progress";s:6:"og:url";s:142:"http://www.relial.org/index.php/productos/archivo/opinion/item/378-iaf-curso-profiling-political-liberalism-as-an-effective-force-for-progress";s:8:"og:title";s:84:"IAF curso Profiling Political Liberalism as an Effective Force for Progress - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/24051f1a73d7ccf80aa0e781a6178da9_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/24051f1a73d7ccf80aa0e781a6178da9_S.jpg";s:14:"og:description";s:159:"Contenido del programa &amp;amp;nbsp; El objetivo general del taller fue debatir acerca de cuál debería ser la respuesta de los lideres de think tanks y d...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:75:"IAF curso Profiling Political Liberalism as an Effective Force for Progress";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}