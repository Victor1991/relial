<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7357:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId504"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Terrorismo y terrorista
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/84bd8bd55711fc1be7650ea3945868c3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/84bd8bd55711fc1be7650ea3945868c3_XS.jpg" alt="Terrorismo y terrorista" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Nos ha indignado profundamente a los peruanos que la zozobra, muerte y destrucción que causaron grupos sanguinarios, como el Movimiento Revolucionario Túpac Amaru (MRTA) en nuestro país, haya sido confundida con activismopor algunos medios estadounidenses.	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Aunque algunos repitieron la noticia sin contrastar las fuentes y se han rectificado, es inaceptable la 'nota informativa' que dio lugar a ello, la del periodista Frank Bajak de Associated Press (AP), que con una narrativa condescendiente con el terrorismo desdibuja la historia que sufrió nuestro país en manos emerretistas. La nota de Bajak sí va entre comillas porque dista mucho de la verdad y de ser profesional, y trata de presentar como información periodística su opinión sesgada del tema. Bajak evita llamar terroristas a los emerretistas y los llama "rebeldes de izquierda", y a una condenada por terrorismo como la estadounidense Lori Berenson la llama "activista".</p>
<p>&nbsp;</p>
<p>Sea intencional o por impericia, confundir activismo con terrorismo es una ofensa para las víctimas de nuestro país. Un activista busca cambios dentro de la misma democracia mediante los medios que el Estado de derecho tiene y en base al respeto de los derechos humanos, siendo el respeto a la vida el primerísimo. Todo esto lo desprecia un terrorista.</p>
<p>&nbsp;</p>
<p>En el Perú, en las décadas de 1980 y 1990, el MRTA, al que Bajak llama rebeldes de izquierda, se dedicó a sembrar el temor en nuestra población usando armamento y explosivos. Así son responsables de muchos asesinatos selectivos, secuestros y asaltos que dejaban víctimas mortales entre civiles y fuerzas del orden. Los emerretistas secuestraban periodistas y tomaban medios de comunicación para transmitir sus mensajes. Del MRTA son los terroristas que en diciembre de 1996 asaltaron la residencia del embajador japonés y tomaron de rehenes a los invitados. Ellos mantuvieron como rehenes a 72 personas durante 126 días, lo que terminó con uno de los rehenes, dos efectivos militares y los terroristas muertos. El MRTA financiaba sus gastos mediante secuestros a empresarios, manteniéndolos en espacios reducidos e insalubres que llamaron "cárceles del pueblo". El MRTA también es responsable de la masacre de ocho personas homosexuales ocurrida en 1989 en la ciudad de Tarapoto. Todos estos no son actos de rebeldía, son actos sangrientos de terrorismo documentados por muchas fuentes, entre ellas la Comisión de la Verdad y Reconciliación (CVR).</p>
<p>&nbsp;</p>
<p>Berenson fue miembro de esta organización terrorista y fue capturada en 1995, junto a altos mandos del MRTA, en una vivienda que era utilizada como base emerretista. Antes de ser capturada ingresó a nuestro Parlamento haciéndose pasar por periodista como parte de un plan para tomarlo. Hace unos días fue expulsada de nuestro país tras ser juzgada y cumplir una condena de veinte años no por activismo, sino por colaborar con el terrorismo.</p>
<p>&nbsp;</p>
<p>Hoy, cuando el terrorismo golpea sólidas democracias y son millones de personas en el mundo las que entienden y se solidarizan con rechazar estos actos como repudiables, con mayor razón no debemos permitir que desdibujen la historia de terror en la que nos hundió el MRTA. Que nuestra indignación nos lleve no solo a condenar con nuestra memoria por siempre a Berenson y al MRTA, sino a emprender una verdadera tarea educativa frente a la narrativa manchada con sangre.</p>
<p>&nbsp;</p>
<p>Foto: IPL Perú</p>
<p>Texto: <a href="http://elcomercio.pe/opinion/colaboradores/terrorismo-y-terrorista-yesenia-alvarez-noticia-1861855">elcomercio.pe</a></p>
<p>Autor: Yesenia Alvarez</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/492-con-ayudita-no-vale">
			&laquo; Con ayudita no vale		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/507-la-pata-podrida">
			La pata podrida &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/504-terrorismo-y-terrorista#startOfPageId504">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:32:"Terrorismo y terrorista - Relial";s:11:"description";s:154:"Nos ha indignado profundamente a los peruanos que la zozobra, muerte y destrucción que causaron grupos sanguinarios, como el Movimiento Revolucionario...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:23:"Terrorismo y terrorista";s:6:"og:url";s:86:"http://relial.org/index.php/productos/archivo/opinion/item/504-terrorismo-y-terrorista";s:8:"og:title";s:32:"Terrorismo y terrorista - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/84bd8bd55711fc1be7650ea3945868c3_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/84bd8bd55711fc1be7650ea3945868c3_S.jpg";s:14:"og:description";s:154:"Nos ha indignado profundamente a los peruanos que la zozobra, muerte y destrucción que causaron grupos sanguinarios, como el Movimiento Revolucionario...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:23:"Terrorismo y terrorista";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}