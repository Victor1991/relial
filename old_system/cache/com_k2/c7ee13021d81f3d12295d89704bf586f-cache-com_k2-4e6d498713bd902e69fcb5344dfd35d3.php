<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9232:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId443"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Argetina: ¿Quiénes son los idiotas?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/9c2fe6cb8c357cf6d57c8926869c1003_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/9c2fe6cb8c357cf6d57c8926869c1003_XS.jpg" alt="Argetina: &iquest;Qui&eacute;nes son los idiotas?" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>¿Quién ha dicho que hay una crisis inusual en Argentina? Es la misma de siempre. Gasto público excesivo, corrupción galopante, Estado prebendario, clientelismo, incumplimiento de las obligaciones, capitalismo de compadreo, inflación, desabastecimiento, cambio negro de dólares (que aquí, no sé por qué, se llama dólar blue y se prohíbe, pero se tolera, como sucede con la prostitución). El oficial está a 8. El blue, a 15. El pronóstico es que aumentará ese diferencial en la medida en que se prolongue la incertidumbre y se vaya instalando el pánico.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>¿Por qué, cada cierto tiempo, como si fuera una extraña maldición recurrente, Argentina, pese a su legendaria riqueza natural, se precipita en el caos? Quienes conocemos América Latina palmo a palmo sabemos que la concentración de talento en este país es la mayor de la región. Son los latinoamericanos mejor educados y más informados. Tuvieron casi ochenta años espléndidos, de 1853 a 1930, periodo en el que crearon una mayoritaria y asombrosamente resistente clase media. No obstante, con altibajos, el país, que fue una de las naciones más prósperas del planeta, comenzó lentamente a involucionar.</p>
<p>&nbsp;</p>
<p>En la década de los ochenta el ensayista estadounidense Larry Harrison publicó un libro titulado El subdesarrollo es un estado de la mente. Afirmaba y, a mi juicio, probaba, que alcanzar la prosperidad o vivir en la miseria era el resultado de las creencias, actitudes y valores que suscribían las personas. Había culturas orientadas a crear riquezas y otras que las destruían. Mariano Grondona, un notable pensador argentino, tiene un libro magnífico en el que estudia a fondo esta cuestión (Las condiciones culturales del desarrollo económico).</p>
<p>&nbsp;</p>
<p>Los argentinos, esencialmente producto de la influencia fascista, encarnada a la criolla en Juan Domingo Perón, attaché militar en la Italia de Benito Mussolini antes de hacerse con el poder y con la historia del país, echaron por la borda las enseñanzas de Juan Bautista Alberdi y de Domingo F. Sarmiento, dos políticos y pensadores liberales de la segunda mitad del siglo XIX, sustituyéndolas por el credo peronista.</p>
<p>&nbsp;</p>
<p>No entendieron que la prosperidad y el crecimiento económico dentro de las democracias liberales eran la consecuencia de la primacía de los derechos individuales, el gobierno limitado, la separación real de los poderes, el respeto por la propiedad privada, el imperio de la ley, el buen funcionamiento de las instituciones, la rendición de cuentas por parte de las autoridades, la existencia del mercado y de la meritocracia, clima en el que se generaba y conservaba la riqueza. (Así, con diversas variantes, se comportan los 25 países más prósperos del planeta).</p>
<p>&nbsp;</p>
<p>Era el modelo republicano de gobierno y lo desmantelaron. De manera creciente, los argentinos pensaron, y la influencia les llegó desde todos los ámbitos con diferente intensidad —fascismo, militares nacionalistas, comunismo, keynesianismo, cepalianismo—, que le correspondía al Estado la función de dirigir la economía y distribuir la riqueza creada, sin percatarse de que los gobiernos son tremendamente ineficientes e injustos, totalmente incapaces de llevar a cabo esas tareas con un mínimo de éxito.</p>
<p>&nbsp;</p>
<p>Me lo explicó el analista Esteban Lijalad con una sencilla información extraída de las encuestas periódicas que él hace para una firma publicitaria en el gran Buenos Aires, pulmón y cerebro de la nación. Cuando se les pregunta a los argentinos si prefieren que el Estado intervenga en todos los sectores de la economía, el 53% responde que sí. Cuando se les pregunta si debe intervenir en algún sector, el porcentaje baja al 35. Los que prefieren que no intervenga en ningún sector, se reducen al 9%. Los que no saben o quieren responder son muy pocos.</p>
<p>&nbsp;</p>
<p>No importa la experiencia terrible del Estado-empresario. (Por ejemplo, una década para conseguir una línea telefónica, mil coimas para mantenerla funcionando). Para la mayoría de los argentinos, la empresa privada es malvada. Enriquecerse es censurable. El individuo es sospechoso por su egoísmo.</p>
<p>&nbsp;</p>
<p>La solución de todos los males vendrá del altruista Estado que multiplicará milagrosamente, y luego repartirá, los panes, los peces y el delicioso vino Malbec. Lo esencial no es la realidad, sino la ideología, las distorsionadas percepciones y el relato de un amable "ogro filantrópico" que dispensa favores a los necesitados.</p>
<p>&nbsp;</p>
<p>En esta oportunidad fui a la Argentina con Álvaro Vargas Llosa, a presentar otro libro que habíamos escrito junto a Plinio Apuleyo Mendoza, publicado por Planeta, precisamente sobre este tema: Últimas noticias del nuevo idiota iberoamericano. Había un jugoso capítulo sobre Argentina. Era la tercera entrega de una saga que comenzó hace casi 20 años con el Manual del perfecto idiota latinoamericano.</p>
<p>&nbsp;</p>
<p>Está visto que los idiotas somos nosotros. No acabamos de entender que el mal parece que no tiene cura.</p>
<p>&nbsp;</p>
<p>Foto: Blog de Montaner</p>
<p>Fuente:&nbsp;Blog de Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/437-“strengthening-ngos-winning-support-for-ideas-and-their-political-implementation”">
			&laquo; “Strengthening NGOs -Winning Support for Ideas and their Political Implementation”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/445-el-terremoto-brasilero">
			El Terremoto Brasilero &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/443-argetina-¿quiénes-son-los-idiotas?#startOfPageId443">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:46:"Argetina: ¿Quiénes son los idiotas? - Relial";s:11:"description";s:157:"En la opinión de Carlos Alberto Montaner &amp;nbsp; ¿Quién ha dicho que hay una crisis inusual en Argentina? Es la misma de siempre. Gasto público exc...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:37:"Argetina: ¿Quiénes son los idiotas?";s:6:"og:url";s:102:"http://www.relial.org/index.php/productos/archivo/opinion/item/443-argetina-¿quiénes-son-los-idiotas";s:8:"og:title";s:46:"Argetina: ¿Quiénes son los idiotas? - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/9c2fe6cb8c357cf6d57c8926869c1003_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/9c2fe6cb8c357cf6d57c8926869c1003_S.jpg";s:14:"og:description";s:161:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; ¿Quién ha dicho que hay una crisis inusual en Argentina? Es la misma de siempre. Gasto público exc...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:37:"Argetina: ¿Quiénes son los idiotas?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}