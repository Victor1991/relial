<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8867:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId486"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El gurú de Obama y un mundo sin cabeza
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/051a22dde3b372e5c058fbc303756df4_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/051a22dde3b372e5c058fbc303756df4_XS.jpg" alt="El gur&uacute; de Obama y un mundo sin cabeza" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Me equivoqué. Escribí que, por primera vez en su historia, la diplomacia norteamericana carecía de un marco de referencia. Me lo indicaban, falsamente, las concesiones gratuitas a la dictadura cubana, el pésimo preacuerdo con Irán, la condescendencia con la dictadura birmana, la tolerancia con los desmanes de Chávez y Maduro, y la manera contradictoria con que habían manejado, por ejemplo, las crisis de Ucrania, Honduras o Egipto.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Craso error. Me lo señaló el historiador Diego Trinidad. Existe un marco de referencia, por ahora vagamente utilizado. Se titula How Enemies Become Friends: The Sources of Stable Peace (Cómo los enemigos se convierten en amigos: la fuente de una paz estable), escrito por Charles A. Kupchan, profesor de Georgetown University y miembro del Consejo Nacional de Seguridad que sirve directamente a la Casa Blanca.</p>
<p>&nbsp;</p>
<p>La obra, editada por Princeton University Press, puede adquirirse muy fácilmente por Amazon. Me ahorro una descripción detallada del contenido, lleno de alusiones históricas eruditas, pero vale la pena resumir su tesis central porque es muy sencilla: la manera de transformar a los enemigos en amigos y de sostener la paz es hacerles grandes concesiones unilaterales, no exigir ni esperar nada a cambio, cancelar toda conducta hostil, y no tratar de cambiar la naturaleza de esos gobiernos adversarios.</p>
<p>&nbsp;</p>
<p>Es el entierro de la tradicional lógica diplomática que prescribe zanahorias para los amigos y aliados, palos para los enemigos y nada para los indiferentes. Es el fin de la diplomacia activa, desarrollada tras la terminación de la Segunda Guerra, encaminada a tratar de convertir el mundo en un lugar pacífico y próspero, dominado por regímenes democráticos en el que se respeten la economía de mercado, los derechos humanos y las libertades.</p>
<p>&nbsp;</p>
<p>Es una mezcla de buenismo y neoaislacionismo. Es el fin, también, de la idea de que Estados Unidos, como potencia hegemónica en el terreno económico y militar, asume la responsabilidad de encabezar el castigo a los países agresores, intentar dotar al planeta de estabilidad y de promover el buen gobierno, definido éste como la administración de sociedades pacíficas, democráticas, productivas y abiertas al comercio internacional.</p>
<p>&nbsp;</p>
<p>Naturalmente, los regímenes de Cuba, Venezuela e Irán seguramente verán con un enorme agrado que Estados Unidos renuncie a tratar de frenarlos, porque eso les deja el campo libre, pero ello no modificará esencialmente la percepción que estas naciones tienen del gobierno y del sistema económico de libre empresa que exhibe la sociedad norteamericana.</p>
<p>&nbsp;</p>
<p>Al fin y al cabo, La Habana y Caracas no son enemigos étnicos de Estados Unidos, sino adversarios ideológicos de las democracias liberales y del sistema de libre empresa que esta nación encabeza. Si Estados Unidos fuera una nación comunista o participara de la visión de los países del llamado Socialismo del Siglo XXI, inmediatamente cesaría el antiamericanismo.</p>
<p>&nbsp;</p>
<p>Hay que recordar que Fidel Castro y Hugo Chávez no escogieron el antiamericanismo o el comunismo –Venezuela va en ese camino—por reacción a la política de Washington, sino (como Fidel Castro ha aclarado mil veces) por creer en las virtudes del colectivismo, de la planificación centralizada y del control social ejercido por un partido único. Son antiamericanos a fuer de ser procomunistas.</p>
<p>&nbsp;</p>
<p>De alguna manera, irónicamente, esta nueva forma de encarar la diplomacia (que a mi me parece disparatada) adoptada por Estados Unidos no es la consecuencia de la debilidad, sino del éxito. Producen una quinta parte de lo que genera el mundo con menos del 5% de la población del planeta y tienen unas fuerzas armadas imbatibles que consumen más de 600 mil millones de dólares al año. Eso les confiere una peligrosa sensación de invulnerabilidad.</p>
<p>&nbsp;</p>
<p>Con esos elementos a su favor, Obama cree que puede darse el lujo de ignorar a amigos y enemigos. ¿Podrá hacerlo? Lo dudo. La visión internacional norteamericana a partir de F.D. Roosevelt y los acuerdos de Bretton Woods de 1944, todavía con el ejército alemán sobre las armas, está concebida para que Washington asuma la responsabilidad de liderar el llamado "mundo libre" hasta lograr la derrota de los enemigos de la democracia.</p>
<p>&nbsp;</p>
<p>Esa tradición, que ya tiene más de 70 años, y que ha visto el triunfo de Occidente en la Guerra Fría, ha generado toda una burocracia (hoy desconcertada) dedicada a ejecutar medidas de gobierno para lograr esos objetivos. La inercia de estos organismos pesa mucho y a Obama sólo le quedan menos de un par de años en la Casa Blanca. No creo, afortunadamente, que logre imponer sus ideas, que son, parece, las de Kupchan. Un mundo sin cabeza es mucho más peligroso.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Foro: El blog de Montaner</p>
<p>Fuente: El Blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/484-¿por-qué-soy-liberal?">
			&laquo; ¿Por qué soy liberal?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/492-con-ayudita-no-vale">
			Con ayudita no vale &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/486-el-gurú-de-obama-y-un-mundo-sin-cabeza#startOfPageId486">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:48:"El gurú de Obama y un mundo sin cabeza - Relial";s:11:"description";s:157:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Me equivoqué. Escribí que, por primera vez en su historia, la diplomacia norteamericana carecía de...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:39:"El gurú de Obama y un mundo sin cabeza";s:6:"og:url";s:102:"http://relial.org/index.php/productos/archivo/opinion/item/486-el-gurú-de-obama-y-un-mundo-sin-cabeza";s:8:"og:title";s:48:"El gurú de Obama y un mundo sin cabeza - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/051a22dde3b372e5c058fbc303756df4_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/051a22dde3b372e5c058fbc303756df4_S.jpg";s:14:"og:description";s:161:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Me equivoqué. Escribí que, por primera vez en su historia, la diplomacia norteamericana carecía de...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:39:"El gurú de Obama y un mundo sin cabeza";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}