<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11844:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId516"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	“No hay comida”: el fantasma del hambre se apodera de Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/072519f74a95ea36f571d1e83f1c23bd_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/072519f74a95ea36f571d1e83f1c23bd_XS.jpg" alt="&ldquo;No hay comida&rdquo;: el fantasma del hambre se apodera de Venezuela" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Venezuela se encuentra plenamente destrozada, ya todos lo sabemos, sin embargo nadie hace ni dice nada. Simplemente se leen las noticias, ningún mandatario latinoamericano levanta la voz, los hermanos venezolanos mueren de hambre y padecen las consecuencias del Comunismo chavista.</p>
<p>&nbsp;</p>
<p>En dicho país impera el "hambrunismo", un concepto propio utilizado para hacer referencia a las consecuencias de la ideología comunista, socialista, marxista o como usted prefiera llamarle. Ideologías que siempre, absolutamente siempre, tuvieron como resultado hambre, muerte y pobreza extrema.</p>
<p>&nbsp;</p>
<p>Lea más: <a href="http://es.panampost.com/sabrina-martin/2016/05/03/cazan-gatos-perros-y-palomas-para-matar-el-hambre-en-venezuela/">En Venezuela cazan gatos, perros y palomas para matar el hambre</a></p>
<p>&nbsp;</p>
<p>Este terremoto que ya lleva casi dos décadas agitando al país, lo ha dejado en las ruinas. A continuación, veremos algunos números y datos que nos permitirán comprender un poco mejor la realidad que se vive por allí:</p>
<p>&nbsp;</p>
<ul>
<li>En Venezuela se proyecta una inflación de 700% para este año. Tiene la inflación más elevada del mundo.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Según la Confederación Venezolana de Industrias, en estas décadas de chavismo, aproximadamente más de 8.000 empresas han cerrado sus puertas en el país.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Más de 70% de los venezolanos cree que Nicolás Maduro debe abandonar el poder.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Según el Observatorio Venezolano de Conflictividad Social (OVCS), entre enero y abril de este año hubo 2.138 protestas y más de 170 saqueos. Hay un promedio de 18 protestas por día.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Venezuela tiene una de las tasas de homicidios más altas del mundo: 28.000 homicidios en 2015, 76 muertes violentas por día, 3 muertes por hora. Uno de cada cinco homicidios en América Latina lo sufre un venezolano.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Caracas es la ciudad más peligrosa del planeta.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>En este país, donde otrora los más pobres podían tomarse su propio whiskey, hoy no se consigue ni agua.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>El agua del grifo está sucia, huele mal y sabe peor. Lo dice esta argentina que lo experimentó en carne propia.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Según una encuesta de Encovi, al 87% de los venezolanos no les alcanza el dinero para comprar comida.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Según la Federación Nacional de Ganaderos, en 2015 los venezolanos redujeron el consumo de carne de res en 42% con respecto al año 2012, el consumo más bajo en 55 años.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>El 90% de los ciudadanos dice comprar menos alimentos ante la escasez.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Según la encuestadora Datanálisis, hay desabastecimiento de alimentos básicos en un 80% de los supermercados y 40% de los hogares.</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>Mientras en América Latina la desnutrición infantil es de 5% según la FAO, en Venezuela, la Fundación Bengoa encontró un 9% para 2015 (las últimas cifras reportaron 2,9% en 2011).</li>
</ul>
<p>&nbsp;</p>
<ul>
<li>La red de médicos de hospitales públicos, Médicos por la Salud, afirma que el 44% de los quirófanos no están funcionando, y el 94% de los laboratorios no tienen insumos para llevar a cabo su labor.</li>
</ul>
<p>&nbsp;</p>
<p>El pueblo venezolano carece de medicamentos, electricidad, agua y alimentos. Por su parte, abunda el crimen, así como los homicidios y la violencia callejera.</p>
<p>&nbsp;</p>
<p>Comúnmente, cuando tenemos hambre o nos faltan algunas cosas básicas en la nevera, tomamos las llaves de la casa, algo de dinero y salimos al mercado para volver en unos minutos y continuar con nuestras vidas. Bueno, en Venezuela no.</p>
<p>&nbsp;</p>
<p>Los venezolanos hacen fila inclusive durante la madrugada, mientras la tensión y el enojo caminan por las calles de todo el país. Al aparecer camiones con comida, la gente se empuja, se amontona y corre para conseguir algo de alimento, mientras los saqueos ya forman parte del paisaje cotidiano. La gente ha perdido la paciencia.</p>
<p>&nbsp;</p>
<p>Hacer filas interminables en distancia y tiempo para conseguir algo de alimento o productos básicos, forma parte del día a día de los venezolanos.</p>
<p>&nbsp;</p>
<p>Cada vez son más los ciudadanos que deben pasar por alto algunas comidas del día, y el simple hecho de comer, algo que nosotros vemos tan común y corriente, se ha convertido en un lujo para una buena parte de la población.</p>
<p>&nbsp;</p>
<p>Algunos de los productos que escasean y se encuentran regulados con controles de precios son: aceite, granos, azúcar, jugos, café, víveres diarios, pollo, carne de res, carne de cerdo, leche, champú, jabón para la ropa, maíz, harina de maíz, pasta dental, pescados, desodorante, pañales para bebés, papel higiénico, cloro, máquinas de afeitar, detergente, entre otros tantos bienes.</p>
<p>&nbsp;</p>
<p>La realidad que los chavistas no quieren decir, es que el control de precios es la principal causa de aquella escasez. Y si hoy hay hambre en Venezuela, la culpa es del Gobierno dictatorial.</p>
<p>&nbsp;</p>
<p>"Necesitamos la comida. ¿Hasta cuándo? Ya es demasiado, un niño no puede aguantar hambre, los niños necesitan alimentarse para crecer bien", expresa una mujer en llanto, mientras carga a su pequeño hijo en plena cola de mercado.</p>
<p>&nbsp;</p>
<p>En el país con las mayores reservas petroleras, ¿cómo es posible que escaseen los alimentos, la electricidad, el agua, las medicinas y otros tantos productos básicos? La respuesta es simple: corrupción, mala administración y Comunismo.</p>
<p>&nbsp;</p>
<p>Hay que ser bastante brutos para creer que el control de precios va a derivar en algo bueno. Cuando una entidad gubernamental que se cree la máxima conocedora de las necesidades, gustos y vidas de los ciudadanos, decide imponer precios a los productos, sólo logrará hacer desaparecer aquellos bienes y la escasez comenzará a imperar.</p>
<p>&nbsp;</p>
<p>El control de precios es una política que ha fracasado siempre, en cualquier lugar donde se la ha intentado experimentar.</p>
<p>&nbsp;</p>
<p><strong>Con las medicinas también</strong></p>
<p>&nbsp;</p>
<p>Lo mismo sucede con los medicamentos. Las personas que padecen enfermedades como hipertensión, gastritis, insuficiencias respiratorias, diabetes o infecciones, al ser recetados con algún medicamento, intentan conseguirlo en alguna farmacia, pero les dicen que ya no quedan más. Mueren en sus casas por algo que podría haber tenido cura de no ser por la inoperancia del chavismo imperante.</p>
<p>&nbsp;</p>
<p>Ni siquiera un dolor de cabeza puede ser calmado, el paracetamol tampoco se consigue.</p>
<p>&nbsp;</p>
<p>"Aquí la mayoría de los pacientes que caen en paro fallecen, porque no hay cómo atenderlos", expresan en el Hospital Universitario de Caracas, uno de los centros médicos más importantes del país.</p>
<p>&nbsp;</p>
<p>Los recién nacidos mueren, los ancianos mueren, los enfermos mueren, los estudiantes son asesinados, y los ciudadanos que no mueren padecen el martirio del día a día o, simplemente, deben poner un pie en el Aeropuerto Internacional de Maiquetía para buscar un futuro mejor, ya que el chavismo les ha arrebatado todo.</p>
<p>&nbsp;</p>
<p>A modo de conclusión, podemos afirmar que Venezuela ha vuelto a la prehistoria.</p>
<p>&nbsp;</p>
<p>Fuente: Panampost</p>
<p>Autor: Antonella Marty</p>
<p>Foto: PanamPost</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
			&laquo; Juntos en el compromiso por la libertad personal alrededor del mundo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
			Las elecciones peruanas del 2016 &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela#startOfPageId516">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:76:"“No hay comida”: el fantasma del hambre se apodera de Venezuela - Relial";s:11:"description";s:157:"¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo &a...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:67:"“No hay comida”: el fantasma del hambre se apodera de Venezuela";s:6:"og:url";s:144:"http://www.relial.org/index.php/productos/archivo/actualidad/item/516-â€œno-hay-comidaâ€-el-fantasma-del-hambre-se-apodera-de-venezuela";s:8:"og:title";s:76:"“No hay comida”: el fantasma del hambre se apodera de Venezuela - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/072519f74a95ea36f571d1e83f1c23bd_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/072519f74a95ea36f571d1e83f1c23bd_S.jpg";s:14:"og:description";s:161:"¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo &amp;a...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:67:"“No hay comida”: el fantasma del hambre se apodera de Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}