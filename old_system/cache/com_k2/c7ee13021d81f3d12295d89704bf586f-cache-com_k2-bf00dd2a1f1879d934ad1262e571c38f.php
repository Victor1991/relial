<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4915:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId51"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Documental Completo: Los nietos de la Revolución Cubana
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/eb6c7c01c4e98e1f2578f9959463b973_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/eb6c7c01c4e98e1f2578f9959463b973_XS.jpg" alt="Documental Completo: Los nietos de la Revoluci&oacute;n Cubana" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item text -->
	  <div class="itemFullText">
	  	<p>Plasma la frustración actual de una generación de jóvenes que rechaza los dogmas revolucionarios y se siente condenada a la marginalidad por el sistema.</p>
<p>&nbsp;</p>
<p>Dirigido por el cubano-estadounidense Carlos Montaner, el documental de una hora de duración, grabado de forma clandestina en la isla, desprende a través de los testimonios recogidos, un pesimismo ácido y una insatisfacción permanente hacia el régimen cubano.</p>
<p>&nbsp;</p>
<p>De la visión que los jóvenes entrevistados dan de la vida cubana se concluye que "no existe una vinculación emocional de éstos con el proceso histórico de la revolución", señaló el creador, hijo del periodista y escritor cubano exiliado Carlos Alberto Montaner.</p>
<p>&nbsp;</p>
<p>El documental ofrece numerosos testimonios directos de jóvenes, la mayor parte menores de 35 años, muchos de ellos desmotivados y dominados por una suerte de apatía respecto de la posibilidad de lograr un mejor futuro. Entre los jóvenes entrevistados figuran las ...</p>	  </div>
	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
  
  
  
	
  
	<div class="clr"></div>

    <!-- Item video -->
  <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

  <div class="itemVideoBlock">
  	<h3>K2_MEDIA</h3>

				<span class="itemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:500px;" class="avPlayerContainer">
		<div id="AVPlayerID_d0f317a2_1903166981" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/yxRVj3HWQog?rel=0&amp;fs=1&amp;wmode=transparent" width="500" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		
	  
	  
	  <div class="clr"></div>
  </div>
  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/multimedia/videos/item/51-documental-completo-los-nietos-de-la-revolución-cubana#startOfPageId51">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:65:"Documental Completo: Los nietos de la Revolución Cubana - Relial";s:11:"description";s:156:"Plasma la frustración actual de una generación de jóvenes que rechaza los dogmas revolucionarios y se siente condenada a la marginalidad por el sistem...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:56:"Documental Completo: Los nietos de la Revolución Cubana";s:6:"og:url";s:113:"http://www.relial.org/index.php/multimedia/videos/item/51-documental-completo-los-nietos-de-la-revolución-cubana";s:8:"og:title";s:65:"Documental Completo: Los nietos de la Revolución Cubana - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/eb6c7c01c4e98e1f2578f9959463b973_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/eb6c7c01c4e98e1f2578f9959463b973_S.jpg";s:14:"og:description";s:156:"Plasma la frustración actual de una generación de jóvenes que rechaza los dogmas revolucionarios y se siente condenada a la marginalidad por el sistem...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:3:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:72:"/plugins/content/jw_allvideos/jw_allvideos/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:11:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:78:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/mediaplayer/jwplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:79:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/silverlight.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:77:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/wmvplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:86:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/quicktimeplayer/AC_QuickTime.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Multimedia";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:6:"Videos";s:4:"link";s:20:"index.php?Itemid=137";}i:2;O:8:"stdClass":2:{s:4:"name";s:56:"Documental Completo: Los nietos de la Revolución Cubana";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}