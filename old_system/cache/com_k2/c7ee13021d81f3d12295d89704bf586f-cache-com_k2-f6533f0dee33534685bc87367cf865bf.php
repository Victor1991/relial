<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:14834:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId260"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Liberales y liberales
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/665e3353c5a0a1298b58f0408e39e998_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/665e3353c5a0a1298b58f0408e39e998_XS.jpg" alt="Liberales y liberales" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La opinión de Mario Vargas Llosa</p>
<p>&nbsp;</p>
<p>"Hay ciertas ideas básicas que definen a un liberal. Que la libertad, valor supremo, es una e indivisible y que ella debe operar en todos los campos para garantizar el verdadero progreso. La libertad política, económica, social, cultural, son una sola y todas ellas hacen avanzar la justicia, la riqueza, los derechos humanos, las oportunidades y la coexistencia pacífica en una sociedad. Si en uno solo de esos campos la libertad se eclipsa, en todos los otros se encuentra amenazada.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Como los seres humanos, las palabras cambian de contenido según el tiempo y el lugar. Seguir sus transformaciones es instructivo, aunque, a veces, como ocurre con el vocablo "liberal", semejante averiguación puede extraviarnos en un laberinto de dudas.</p>
<p>&nbsp;</p>
<p>En el Quijote y la literatura de su época la palabra aparece varias veces. ¿Qué quiere decir allí? Hombre de espíritu abierto, bien educado, tolerante, comunicativo; en suma, una persona con la que se puede simpatizar. En ella no hay connotaciones políticas ni religiosas, sólo éticas y cívicas en el sentido más ancho de ambas palabras.</p>
<p>&nbsp;</p>
<p>A fines del siglo XVIII este vocablo cambia de naturaleza y adquiere matices que tienen que ver con las ideas sobre la libertad y el mercado de los pensadores británicos y franceses de la Ilustración (Stuart Mill, Locke, Hume, Adam Smith, Voltaire). Los liberales combaten la esclavitud y el intervencionismo del Estado, defienden la propiedad privada, el comercio libre, la competencia, el individualismo y se declaran enemigos de los dogmas y el absolutismo.</p>
<p>&nbsp;</p>
<p>En el siglo XIX un liberal es sobre todo un librepensador: defiende el Estado laico, quiere separar la Iglesia del Estado, emancipar a la sociedad del oscurantismo religioso. Sus diferencias con los conservadores y los regímenes autoritarios generan a menudo guerras civiles y revoluciones. El liberal de entonces es lo que hoy llamaríamos un progresista, defensor de los derechos humanos (desde la Revolución Francesa se les conocía como los Derechos del Hombre) y la democracia.</p>
<p>&nbsp;</p>
<p>Con la aparición del marxismo y la difusión de las ideas socialistas, el liberalismo va siendo desplazado de la vanguardia a una retaguardia, por defender un sistema económico y político —el capitalismo— que el socialismo y el comunismo quieren abolir en nombre de una justicia social que identifican con el colectivismo y el estatismo. (No en todas partes ocurre esta transformación de la palabra liberal. En Estados Unidos un liberal es todavía un radical, un socialdemócrata o un socialista a secas). La conversión de la vertiente comunista del socialismo al autoritarismo empuja al socialismo democrático al centro político y lo acerca —sin juntarlo— al liberalismo.</p>
<p>&nbsp;</p>
<p>En nuestros días, liberal y liberalismo quieren decir, según las culturas y los países, cosas distintas y a veces contradictorias. El partido del tiranuelo nicaragüense Somoza se llamaba liberal y así se denomina, en Austria, un partido neofascista. <strong>La confusión es tan extrema que regímenes dictatoriales como los de Pinochet en Chile y de Fujimori en Perú son llamados a veces "liberales" o "neoliberales" porque privatizaron algunas empresas y abrieron mercados</strong>. De esta desnaturalización de lo que es la doctrina liberal no son del todo inocentes algunos liberales convencidos de que el liberalismo es una doctrina esencialmente económica, que gira en torno del mercado como una panacea mágica para la resolución de todos los problemas sociales. Esos logaritmos vivientes llegan a formas extremas de dogmatismo y están dispuestos a hacer tales concesiones en el campo político a la extrema derecha y al neofascismo que han contribuido a desprestigiar las ideas liberales y a que se las vea como una máscara de la reacción y la explotación.</p>
<p>&nbsp;</p>
<p>Dicho esto, es verdad que algunos gobiernos conservadores, como los de Ronald Reagan en Estados Unidos y Margaret Thatcher en el Reino Unido, llevaron a cabo reformas económicas y sociales de inequívoca raíz liberal, impulsando la cultura de la libertad de manera extraordinaria, aunque en otros campos la hicieran retroceder. Lo mismo podría decirse de algunos gobiernos socialistas, como el de Felipe González en España o el de José Mujica en Uruguay, que, en la esfera de los derechos humanos, han hecho progresar a sus países reduciendo injusticias inveteradas y creando oportunidades para los ciudadanos de menores ingresos.</p>
<p>&nbsp;</p>
<p>Una de las características del liberalismo en nuestros días es que se le encuentra en los lugares menos pensados y a veces brilla por su ausencia donde ciertos ingenuos creen que está. A las personas y partidos hay que juzgarlos no por lo que dicen y predican sino por lo que hacen. En el debate que hay en estos días en el Perú sobre la concentración de los medios de prensa, algunos valedores de la adquisición por el grupo El Comercio de la mayoría de las acciones de Epensa, que le confiere casi el 80% del mercado de la prensa, son periodistas que callaron o aplaudieron cuando la dictadura de Fujimori y Montesinos cometía sus crímenes más abominables y manipulaba toda la información, comprando a dueños y redactores de diarios o intimidándolos. ¿Cómo tomaríamos en serio a esos novísimos catecúmenos de la libertad? <strong>Un filósofo y economista liberal de la llamada escuela austríaca, Ludwig von Mises, se oponía a que hubiera partidos políticos liberales, porque, a su juicio, el liberalismo debía ser una cultura que irrigara a un arco muy amplio de formaciones y movimientos que, aunque tuvieran importantes discrepancias, compartieran un denominador común sobre ciertos principios liberales básicos.</strong></p>
<p>&nbsp;</p>
<p>Algo de eso ocurre desde hace buen tiempo en las democracias más avanzadas, donde, con diferencias más de matiz que de esencia, entre democristianos y socialdemócratas y socialistas, liberales y conservadores, republicanos y demócratas, hay unos consensos que dan estabilidad a las instituciones y continuidad a las políticas sociales y económicas, un sistema que sólo se ve amenazado por sus extremos, el neofascismo del Frente Nacional en Francia, por ejemplo, o La Liga Lombarda en Italia, y grupos y grupúsculos ultra comunistas y anarquistas.</p>
<p>&nbsp;</p>
<p>En América Latina este proceso se da de manera más pausada y con más riesgo de retroceso que en otras partes del mundo, por lo débil que es todavía la cultura democrática, que sólo tiene tradición en países como Chile, Uruguay y Costa Rica, en tanto que en los demás es más bien precaria. Pero ha comenzado a suceder y la mejor prueba de ello es que las dictaduras militares prácticamente se han extinguido y de los movimientos armados revolucionarios sobrevive a duras penas las FARC colombianas, con un apoyo popular decreciente. Es verdad que hay gobiernos populistas y demagógicos, aparte del anacronismo que es Cuba, pero Venezuela, por ejemplo, que aspiraba a ser el gran fermento del socialismo revolucionario latinoamericano, vive una crisis económica, política y social tan profunda, con el desplome de su moneda, la carestía demencial —todo falta, la comida, el agua, hasta el papel higiénico— y las iniquidades de la delincuencia, que difícilmente podría ser ahora el modelo continental en que quería convertirla el comandante Chávez.</p>
<p>&nbsp;</p>
<p><strong>Hay ciertas ideas básicas que definen a un liberal. Que la libertad, valor supremo, es una e indivisible y que ella debe operar en todos los campos para garantizar el verdadero progreso. La libertad política, económica, social, cultural, son una sola y todas ellas hacen avanzar la justicia, la riqueza, los derechos humanos, las oportunidades y la coexistencia pacífica en una sociedad. Si en uno solo de esos campos la libertad se eclipsa, en todos los otros se encuentra amenazada.</strong></p>
<p>&nbsp;</p>
<p>Los liberales creen que el Estado pequeño es más eficiente que el que crece demasiado, y que, cuando esto último ocurre, no sólo la economía se resiente, también el conjunto de las libertades públicas. Creen asimismo que la función del Estado no es producir riqueza, sino que esta función la lleva a cabo mejor la sociedad civil, en un régimen de mercado libre, en que se prohíben los privilegios y se respeta la propiedad privada. La seguridad, el orden público, la legalidad, la educación y la salud competen al Estado, desde luego, pero no de manera monopólica sino en estrecha colaboración con la sociedad civil.</p>
<p>&nbsp;</p>
<p>Estas y otras convicciones generales de un liberal tienen, a la hora de su aplicación, fórmulas y matices muy diversos relacionados con el nivel de desarrollo de una sociedad, de su cultura y sus tradiciones. No hay fórmulas rígidas y recetas únicas para ponerlas en práctica. Forzar reformas liberales de manera abrupta, sin consenso, puede provocar frustración, desórdenes y crisis políticas que pongan en peligro el sistema democrático. Este es tan esencial al pensamiento liberal como el de la libertad económica y el respeto a los derechos humanos. Por eso, la difícil tolerancia —para quienes, como nosotros, españoles y latinoamericanos, tenemos una tradición dogmática e intransigente tan fuerte— debería ser la virtud más apreciada entre los liberales. Tolerancia quiere decir, simplemente, aceptar la posibilidad del error en las convicciones propias y de verdad en las ajenas.</p>
<p>&nbsp;</p>
<p>Es natural, por eso, que haya entre los liberales discrepancias, y a veces muy serias, sobre temas como el aborto, los matrimonios gay, la descriminalización de las drogas y otros. Sobre ninguno de estos temas existe una verdad revelada liberal, porque para los liberales no hay verdades reveladas. La verdad es, como estableció Karl Popper, siempre provisional, sólo válida mientras no surja otra que la califique o refute. Los congresos y encuentros liberales suelen ser, a menudo, parecidos a los de los trotskistas (cuando el trotskismo existía): batallas intelectuales en defensa de ideas contrapuestas. Algunos ven en ello un rasgo de inoperancia e irrealismo. Yo creo que esas controversias entre lo que Isaías Berlin llamaba "las verdades contradictorias" han hecho que el liberalismo siga siendo la doctrina que más ha contribuido a mejorar la coexistencia social, haciendo avanzar la libertad humana.</p>
<p>&nbsp;</p>
<p>© Derechos mundiales de prensa en todas las lenguas reservados a Ediciones EL PAÍS, SL, 2014.</p>
<p>© Mario Vargas Llosa, 2014.</p>
<p>&nbsp;</p>
<p>Mario Vargas Llosa es miembro de la Junta Honorífca de RELIAL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/259-celac-una-infamia-para-la-democracia-en-la-región">
			&laquo; CELAC: Una infamia para la democracia en la región		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/262-cedice-libertad-ocupa-el-puesto-no-1-entre-los-centros-de-estudios-de-libre-mercado-más-influyentes-de-américa-latina">
			CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/260-liberales-y-liberales#startOfPageId260">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:30:"Liberales y liberales - Relial";s:11:"description";s:155:"La opinión de Mario Vargas Llosa &amp;nbsp; &quot;Hay ciertas ideas básicas que definen a un liberal. Que la libertad, valor supremo, es una e indivis...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:21:"Liberales y liberales";s:6:"og:url";s:91:"http://www.relial.org/index.php/productos/archivo/actualidad/item/260-liberales-y-liberales";s:8:"og:title";s:30:"Liberales y liberales - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/665e3353c5a0a1298b58f0408e39e998_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/665e3353c5a0a1298b58f0408e39e998_S.jpg";s:14:"og:description";s:163:"La opinión de Mario Vargas Llosa &amp;amp;nbsp; &amp;quot;Hay ciertas ideas básicas que definen a un liberal. Que la libertad, valor supremo, es una e indivis...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:21:"Liberales y liberales";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}