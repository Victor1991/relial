<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12521:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId252"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	It&#039;s Snowing In Mexico: Think Tanks Promoting A Free Economy
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/5fd45095a868b007b05013834dba7a95_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/5fd45095a868b007b05013834dba7a95_XS.jpg" alt="It&amp;#039;s Snowing In Mexico: Think Tanks Promoting A Free Economy" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>The battle to move public policy in the direction of economic freedom is not easy. Martin Anderson, of the Hoover Institution, used the analogy of a glacier. Snow, a soft element, begins to accumulate and slowly it transforms into ice. The pressure increases as new layers of snow bury and compress the previous layers, leading to the final "snap."</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>In this analogy, think tanks are idea "snow machines" and several have been very active in Mexico. During the last two decades, many who work for these think tanks, together with some of the world's best economists, gather every year in the picturesque Mexican town of Los Alamos. Arriving in small planes, long drives, or a combination of both, they arrive to network and charge their intellectual batteries.</p>
<p>&nbsp;</p>
<p>The patios, water fountains and spacious colonial lobbies lend themselves to friendly discussions. Los Alamos was one of Nobel Laureate Milton Friedman's favorite retreats. One key builder of this conference is Arnold Harberger, the famed UCLA economist, who has educated dozens of Latin American and Mexican economists. Clay LaForce, former dean of the business school at UCLA and former trustee of the Eli Lilly Company, is also instrumental in the conference. Roberto Salinas, of Mexico's Business Forum and adjunct scholar at the Cato Institute, has been handling the logistics for this program and is a major driving force of efforts for free markets.</p>
<p>&nbsp;</p>
<p>The Los Alamos conference provides a wonderful opportunity to network once a year and the Friedrich Naumann Foundation in Germany promotes such efforts throughout the year. The foundation moved their Latin American headquarters to Mexico in 2003. Victor Becerra coordinates the foundation's Mexico Project, and Sylvia Mercado, through RELIAL, a Latin American network nurtured by Naumann, connects Mexican policy leaders with experts from the Americas.</p>
<p>&nbsp;</p>
<p>The future of the recent reforms announced in Mexico will be influenced by the work of several dynamic groups, academics and other disseminators of ideas. What follows is a list of organizations and people to watch.</p>
<p>&nbsp;</p>
<p>Centro de Investigación para el Desarollo A.C (CIDAC) in Mexico is the first market-oriented think tank that is becoming a lasting institution. During much of its history, it was identified with its founder, economist Luis Rubio. Thanks to the consistency, continuity and quality of its efforts it is growing way beyond its founder. CIDAC was preceded by a research institute founded in 1980 closely connected with the banking industry. In 1982, after the banks were nationalized, its major players decided to build an endowment and launch CIDAC to continue producing independent research. It took its present form in 1984. The endowment now covers a small fraction of its operation so CIDAC is more entrepreneurial than ever.</p>
<p>&nbsp;</p>
<p>The CIDAC team has developed research and communication products that are influencing the national discussion on economic policies including industrial, labor and criminal justice policy. During the last year, CIDAC has played an important role releasing relevant publications, organizing events and promoting the reforms in the energy sector. Veronica Baz, who runs the organization, has assembled a team of extremely talented young collaborators which helps explain its growing presence in social media. They have over 21,000 Twitter followers, better than many U.S. think tanks. Luis Rubio remains as chairman of the board and maintains key connections with the U.S. (he is a board member of Forbes). CIDAC's trustees are relevant players in the academic, policy and business world.</p>
<p>&nbsp;</p>
<p>CIDAC has also served as a training camp for intellectual entrepreneurs. Edna Jaime, a former executive director of CIDAC, is now head of Mexico Evalúa. Founded in 2009, it focuses on measuring and monitoring the impact of public policy in Mexico. Their most recent work analyzes the importance of transparency, security and sound fiscal policy. Even though the group is still young, they have been able to attract sufficient backing from private sources. Jaime is an articulate and telegenic power in the Mexican policy world. With a team of approximately 10 people, Mexico Evalúa will continue to make valuable contributions to Mexico's future.</p>
<p>&nbsp;</p>
<p>Juan Pardinas, a former researcher at CIDAC with a doctorate from the London School of Economics, is the general director of IMCO, The Mexican Institute for Competitiveness. One of its main intellectual assets is Alejandro Hope who focuses on crime and homeland security. He conducts research in areas where Mexico needs urgent reform and improvement. Another unique product of IMCO is its comparative analysis of municipal governments.</p>
<p>&nbsp;</p>
<p>As in other countries, intellectual ­­­­­­­­entrepreneurs working in the academy complement, complete and collaborate with think tanks. I can't mention them all, but some of the talented professors and writers favorable to a free economy include: Isa­­­­­ac Katz at ITAM; Arturo Damm Arnal at the Universidad Panamericana; José Antonio Aguilar, editor of Liberty in Mexico and Carlos Elizondo, both of CIDE (Centro de Investigación y Docencia Económicas); legal scholar Juan Javier del Granado at UNAM; Jesús Silva Herzog Márquez, coming from a family of economists; and Carlos Muñoz Piña, an expert on environmental issues affiliated with the Instituto Global para la Sostenibilidad.</p>
<p>&nbsp;</p>
<p>Several Mexicans are also helping with their foreign connections. In the area of trade, Luz Maria de La Mora, collaborates with the International Center for Trade and Sustainable Development. Herminio Blanco and Manuel Suárez Mier (both with graduate degrees from the University of Chicago), as well as Luis de la Calle, affiliated with the Mexico Institute of the Wilson Center, and Federico Reyes with Transparency International, continue to be relevant with contacts in Washington and beyond.</p>
<p>&nbsp;</p>
<p>Most, if not all, of the above publish regular columns in the main newspapers, or in market-oriented websites such as Asuntos Capitales. Some, like Macario Schettino, have their own popular blogs and others, like Santos Mercado Reyes, a former Maoist-activist-turned-freedom-champion, conduct unceasing educational activities.</p>
<p>&nbsp;</p>
<p>I leave for the end the most youthful group. In 2008, when they were in their early 20s, Armando Regil and his sister Claudia, founded a new think tank: Instituto de Pensamiento Estratégico Ágora, A.C. (IPEA). Like traditional think tanks, IPEA focused on educational programs and generating public policy research. But they also became a "do tank." One of IPEA's most impressive initiatives, now with its own legal standing, is its One Million Youngsters for Mexico campaign. It is an independent and nonpartisan citizen initiative summoning Mexican youth to work together in transforming the country. It has a presence in 20 cities working in partnership with various organizations, universities, academics and other specialists. They already have over 24,000 Twitter followers. Unlike other youth movements, they pay special attention to responsibility as an essential element for flourishing in liberty and inspiring their peers to seek a noble purpose. Armando's leadership and courage, coupled with his dynamism, open mind and professionalism, helped him attract a board of directors with some of Mexico's top business players. Major companies such as Google and the Financial Times have added their support to IPEA. Former presidents and prime ministers from Europe, such as the late Margaret Thatcher, and José María Aznar, and from the Americas, such as Alvaro Uribe (Colombia) and Felipe Calderón (México), among many others, have devoted part of their valuable time to help promote IPEA's goals.</p>
<p>&nbsp;</p>
<p>In addition to the efforts above, the Regils are now helping organize the Los Alamos conference. The old meet the young. Both working together to unleash Mexico's energy potential.</p>
<p>&nbsp;</p>
<p>With the dedication of the think tanks, and the hundreds of hard working intellectuals, it is quite clear that the snow of change is falling in Mexico. And there doesn't seem to be any sign of it letting up.</p>
<p>&nbsp;</p>
<p>Autor: Alejandro Chaufen</p>
<p>Fuente: <a href="http://www.forbes.com/sites/alejandrochafuen/2013/12/18/its-snowing-in-mexico-think-tanks-promoting-a-free-economy/">Forbes</a></p>
<p>Foto: Forbes</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica">
			&laquo; Argentina, entre los países con menor libertad económica		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/255-human-rights-watch">
			Human Rights Watch &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/252-its-snowing-in-mexico-think-tanks-promoting-a-free-economy#startOfPageId252">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:69:"It's Snowing In Mexico: Think Tanks Promoting A Free Economy - Relial";s:11:"description";s:153:"The battle to move public policy in the direction of economic freedom is not easy. Martin Anderson, of the Hoover Institution, used the analogy of a g...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:65:"It&#039;s Snowing In Mexico: Think Tanks Promoting A Free Economy";s:6:"og:url";s:128:"http://www.relial.org/index.php/productos/archivo/actualidad/item/252-its-snowing-in-mexico-think-tanks-promoting-a-free-economy";s:8:"og:title";s:74:"It&#039;s Snowing In Mexico: Think Tanks Promoting A Free Economy - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/5fd45095a868b007b05013834dba7a95_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/5fd45095a868b007b05013834dba7a95_S.jpg";s:14:"og:description";s:153:"The battle to move public policy in the direction of economic freedom is not easy. Martin Anderson, of the Hoover Institution, used the analogy of a g...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:60:"It's Snowing In Mexico: Think Tanks Promoting A Free Economy";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}