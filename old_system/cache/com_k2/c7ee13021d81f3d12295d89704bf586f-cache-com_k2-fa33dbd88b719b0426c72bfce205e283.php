<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8843:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId233"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Impacto del alto costo de nuestra energía
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8e5f062e9750688c028aaa3058da9ec4_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8e5f062e9750688c028aaa3058da9ec4_XS.jpg" alt="Impacto del alto costo de nuestra energ&iacute;a" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Esta transformación del mercado se reflejará en un declive del poder monopólico de la OPEP, cuyas restricciones usuales de la oferta internacional, orientadas a elevar artificialmente el precio de los combustibles, se verán limitadas por el poder competitivo de la energía proveniente de América del Norte.</p>
<p>&nbsp;</p>
<p>Una energía más barata y abundante facilitará la repatriación de inversiones de los Estados Unidos hacia su país de origen. Esa inversión se había alejado, atraída fundamentalmente por una mano de obra más barata en otras naciones y, además, porque en cierto momento el precio de la energía en otras naciones no era tan diferente del costo que tenía en los Estados Unidos. Con esta revolución energética en proceso en los Estados Unidos, a pesar del poco apoyo y casi aversión de Obama hacia esa industria, se abren enormes posibilidades para que la producción industrial de esa nación tenga un renacer, de poder verse fortalecida de nuevo y de estar en capacidad de competir eficientemente en los mercados internacionales.</p>
<p>&nbsp;</p>
<p>Mientras tanto, en Costa Rica se observa un crecimiento paulatino del costo de la energía, resultado de un inmovilismo en la inversión potencial que se podría dar en el país, pero también porque el sector energético estatal muestra claras deficiencias económicas, resultado del proteccionismo en que se le ha tenido. Es lamentable que el cuerpo político legislativo de los últimos tiempos haya hecho todo lo posible, con notorias excepciones, para impedir que el mercado energético nacional pueda mejorar con el impulso productivo que le podría brindar el sector privado. En esta demora ha sido partícipe el gobierno actual, con su característica ineficiencia, al rechazar legislación que facilitaría el desarrollo del sector y pretender suplirla por otra que conserva el statu quo. Pero también han sido responsables algunas fracciones legislativas, empeñadas en mantener un populismo que termina por obstaculizar cualquier posibilidad de progreso en el país. Esa conducta la han observado las fracciones del PAC, del Frente Amplio, parte de la del PUSC, así como una fracturada mitad de la fracción liberacionista.</p>
<p>&nbsp;</p>
<p>De esa forma, en la Asamblea no se ha podido aprobar una nueva legislación que permita aumentar nuestra capacidad de oferta, principalmente logrando los aportes de los sectores privados. En la Asamblea tan sólo se desea más de lo mismo: que el Estado, en sus diferentes variedades y matices, continúe con el monopolio de hecho de la producción de energía. Esto es, que se prosiga con un modelo energético que a todas luces está haciendo aguas... y no por culpa de un neoliberalismo mítico, al cual el populismo barato, en vez de asumir los resultados de sus propias acciones, escoge achacarle todos los males de este mundo.</p>
<p>&nbsp;</p>
<p>Sabemos cómo es que actúan usualmente los políticos, especialmente en épocas previas a elecciones y ante la acosadora angurria financiera del momento. Creen en aprobar legislación que medio abra la posibilidad para algunos, casi que escogidos a dedo, de participar en el mercado de generación eléctrica. Consideran que así resuelven el problema de producción de energía, pero sin que se alborote el cotarro ni el coto de caza. Pero en verdad, el nudo gordiano que hay que cortar radica en la actual falta de competencia de privados con el estado, así como entre aquellos, de forma que se facilite el logro de la mayor cantidad posible y más barata de energía, a fin de suplir económicamente las necesidades del país.</p>
<p>&nbsp;</p>
<p>En vez de aprobar una ley moderna sobre el sector energético del país, basada en la competencia y la producción privada, lo que posiblemente se tendrá es un mamotreto más. Con él algunos pocos podrán medrar, pero se impedirá al país disponer del vigor necesario que impone la lucha competitiva, pues quedarán por fuera muchos otros entrantes potenciales. Con esa ley no se tendrá el aumento requerido en nuestra producción doméstica de energía, sino que será tan sólo un parche más a un sistema defectuoso e incapaz de generar lo que el país necesita: más energía y más barata para todos los usuarios y consumidores.</p>
<p>&nbsp;</p>
<p>Ante lo expuesto, posiblemente nuestra energía proseguirá siendo costosa y por ello no sólo veremos cómo empresas nacionales migrarán hacia otros países, para beneficiarse de su producción más barata de energía, sino que también observaremos cómo gradualmente disminuirá la inversión extranjera en el país. Ya varias empresas nacionales han trasladado sus operaciones o parte de ella a naciones del área que disponen de energía más barata (no vale la pena mencionarlas, pues bien las conocemos). Pero es indispensable tener presente que la gran fórmula nacional de atracción de capitales internacionales, con base en una mano de obra que fuera tanto relativamente barata como lo suficientemente calificada, no será capaz de compensar el creciente aumento del precio de nuestra energía, así como la fuerte reducción de esos costos en los Estados Unidos y en Norte América, en general. Ya sea que, por las razones expuestas, empresas nacionales se instalen en otros países, como que deje de entrar inversión extranjera al país, un resultado lamentable de ello será la caída en el empleo, así como la reducción de los ingresos familiares de los costarricenses.</p>
<p>&nbsp;</p>
<p>fuente: ANFE, Costa Rica</p>
<p>texto: Jorge Corrales Quesada</p>
<p>foto: ANFE &nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/233-impacto-del-alto-costo-de-nuestra-energía#startOfPageId233">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:51:"Impacto del alto costo de nuestra energía - Relial";s:11:"description";s:156:"Esta transformación del mercado se reflejará en un declive del poder monopólico de la OPEP, cuyas restricciones usuales de la oferta internacional, or...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:42:"Impacto del alto costo de nuestra energía";s:6:"og:url";s:94:"http://www.relial.org/index.php/biblioteca/item/233-impacto-del-alto-costo-de-nuestra-energía";s:8:"og:title";s:51:"Impacto del alto costo de nuestra energía - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/8e5f062e9750688c028aaa3058da9ec4_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/8e5f062e9750688c028aaa3058da9ec4_S.jpg";s:14:"og:description";s:156:"Esta transformación del mercado se reflejará en un declive del poder monopólico de la OPEP, cuyas restricciones usuales de la oferta internacional, or...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:33:"Ensayos y reflexiones académicas";s:4:"link";s:76:"/index.php/biblioteca/itemlist/category/22-ensayos-y-reflexiones-académicas";}i:2;O:8:"stdClass":2:{s:4:"name";s:42:"Impacto del alto costo de nuestra energía";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}