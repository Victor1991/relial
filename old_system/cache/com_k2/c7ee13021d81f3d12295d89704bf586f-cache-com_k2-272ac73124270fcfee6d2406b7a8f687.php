<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6628:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId357"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Impacto en los medios del Indice de Calidad Institucional
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/218fa54275e0e31c37b4e5091d9112ba_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/218fa54275e0e31c37b4e5091d9112ba_XS.jpg" alt="Impacto en los medios del Indice de Calidad Institucional" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El pasado 5 de mayo Libertad y Progreso presentó en el canal Agrositio el Índice de Calidad Institucional 2014 (#ICI2014). El programa fue visto en vivo por cientos de espectadores desde Argentina, Venezuela, Estados Unidos, Perú, Austria, Honduras, Chile, Guatemala, España, Irlanda y México, y recibió decenas de comentarios durante su transmisión, varios de los cuales fueron respondidos en el momento por los tres panelistas de lujo que lo presentaron.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>En los días siguientes, diversos medios de prensa se fueron haciendo eco de los resultados del #ICI2014. Destacamos las notas publicadas por:</p>
<p>&nbsp;</p>
<p>INFOBAE.- "La Argentina continúa cayendo en un índice de calidad institucional"</p>
<p>&nbsp;</p>
<p>Replicado por JujuyOnline y Mendoza Online.</p>
<p>&nbsp;</p>
<p>PERFIL.COM y FORTUNAWEB.- "Calidad Institucional: Argentina cayó 41 lugares en 8 años"</p>
<p>&nbsp;</p>
<p>LA NACIÓN.- "Caída en calidad institucional" (Sección Qué Pasa, Tapa de Economía, edición impresa)</p>
<p>- "Una democracia autoritaria, sin calidad ni consensos" (Editorial, edición impresa)</p>
<p>&nbsp;</p>
<p>ÁMBITO FINANCIERO.- "La Argentina perdió 41 lugares en ranking de calidad institucional"</p>
<p>Replicado por Diario Sobre Diarios.</p>
<p>&nbsp;</p>
<p>TERRA.- "Desde 2007 la Argentina cayó 41 lugares en calidad institucional, según la Fundación Libertad y Progreso"</p>
<p>Replicado por La Verdad Online.</p>
<p>&nbsp;</p>
<p>RADIO EL MUNDO.- "Desde la fundación Libertad y Progreso difunden el Índice de Calidad Institucional" (entrevista a Martín Krause)</p>
<p>&nbsp;</p>
<p>RADIO PEHUAJÓ.- Entrevista a Agustín Etchebarne sobre el #ICI2014.</p>
<p>&nbsp;</p>
<p>RADIO PALERMO.- Entrevista a Agustín Etchebarne sobre el #ICI2014.</p>
<p>&nbsp;</p>
<p>FORTUNAWEB.- "Argentina avanza en calidad institucional... en sentido contrario"</p>
<p>&nbsp;</p>
<p>LA GACETA.- Mención al ICI 2014</p>
<p>&nbsp;</p>
<p>AGROSITIO.- "Argentina cayó 41 lugares en el Índice de Calidad Institucional desde 2007"</p>
<p>&nbsp;</p>
<p>INFOCAMPO.- "Argentina cayó 41 lugares en el Índice de Calidad Institucional desde 2007"</p>
<p>&nbsp;</p>
<p>TIEMPO PYME.- "La Argentina sigue cayendo en "calidad institucional""</p>
<p>&nbsp;</p>
<p>SIN MORDAZA.- "Calidad institucional: Argentina sigue cayendo"</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Foto: Libertad y Progreso</p>
<p>Fuente: Libertad y Progreso</p>
<p>Autor: <a href="http://www.libertadyprogresonline.org/2014/05/14/impacto-en-los-medios-del-indice-de-calidad-institucional-2014/">Libertad y Progreso online</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/356-fortuño-presenta-plan-desarrollo-caribe">
			&laquo; Fortuño presenta plan desarrollo Caribe		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/363-venezuela-fundación-hacer-lanza-campaña-de-solidaridad-internacional-con-la-sociedad-civil-venezolana">
			Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/357-impacto-en-los-medios-del-indice-de-calidad-institucional#startOfPageId357">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:66:"Impacto en los medios del Indice de Calidad Institucional - Relial";s:11:"description";s:155:"El pasado 5 de mayo Libertad y Progreso presentó en el canal Agrositio el Índice de Calidad Institucional 2014 (#ICI2014). El programa fue visto en vi...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:57:"Impacto en los medios del Indice de Calidad Institucional";s:6:"og:url";s:123:"http://relial.org/index.php/productos/archivo/actualidad/item/357-impacto-en-los-medios-del-indice-de-calidad-institucional";s:8:"og:title";s:66:"Impacto en los medios del Indice de Calidad Institucional - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/218fa54275e0e31c37b4e5091d9112ba_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/218fa54275e0e31c37b4e5091d9112ba_S.jpg";s:14:"og:description";s:155:"El pasado 5 de mayo Libertad y Progreso presentó en el canal Agrositio el Índice de Calidad Institucional 2014 (#ICI2014). El programa fue visto en vi...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:57:"Impacto en los medios del Indice de Calidad Institucional";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}