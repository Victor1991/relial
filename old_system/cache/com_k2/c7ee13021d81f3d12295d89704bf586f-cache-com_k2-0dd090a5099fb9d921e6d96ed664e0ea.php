<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7931:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId417"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Celebrando a Milton Friedman en sus 102 años
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/45ff2190802f9793d44160c4e551925c_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/45ff2190802f9793d44160c4e551925c_XS.jpg" alt="Celebrando a Milton Friedman en sus 102 a&ntilde;os" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>HACER celebra 102 años de Milton Friedman:</p>
<p>&nbsp;</p>
<p>Hoy, 31 de Julio de 2014, la Fundación HACER celebra 102 años de las ideas de Milton Friedman organizando 15 eventos en 8 países del continente americano.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Gracias al apoyo de la Friedman Foundation for Educational Choice, la Fundación HACER celebrará la vida de Friedman y su legado para la libertad en Argentina (Buenos Aires, Rosario, Mar del Plata &amp; Tucumán), Bolivia, Colombia, Costa Rica, México (Distrito Federal &amp; Ciudad Juárez), Paraguay, Perú y Uruguay:</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-21/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Rosario, Argentina</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-26/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Buenos Aires, Argentina</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-23/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Tucumán, Argentina</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-24/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Mar del Plata, Argentina</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-25/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Bogotá, Colombia</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-20/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Tarija, Bolivia</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-17/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – México DF, México</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-18/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Ciudad Juárez, México</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-22/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Asunción, Paraguay</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-27/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Lima, Perú</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-29/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Iquitos, Perú</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-30/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Iquitos, Perú</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-16/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – San José, Costa Rica</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-19/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Montevideo, Uruguay</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="http://www.hacer.org/el-legado-de-milton-friedman-para-la-libertad-2014-28/">Julio 31, 2014 El Legado de Milton Friedman para la Libertad 2014 – Punta del Este, Uruguay</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: HACER</p>
<p>Foto: HACER</p>
<p>Autor: HACER</p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/405-analizan-realidades-y-perspectivas-del-constitucionalismo-hondureño">
			&laquo; Analizan realidades y perspectivas del constitucionalismo hondureño		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/420-ricardo-lópez-murphy-“el-default-de-argentina-fue-un-fracaso-colectivo-inexplicable”">
			Ricardo López Murphy: “El default de Argentina fue un fracaso colectivo inexplicable” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/417-celebrando-a-milton-friedman-en-sus-102-años#startOfPageId417">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:54:"Celebrando a Milton Friedman en sus 102 años - Relial";s:11:"description";s:156:"HACER celebra 102 años de Milton Friedman: &amp;nbsp; Hoy, 31 de Julio de 2014, la Fundación HACER celebra 102 años de las ideas de Milton Friedman or...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:45:"Celebrando a Milton Friedman en sus 102 años";s:6:"og:url";s:111:"http://relial.org/index.php/productos/archivo/actualidad/item/417-celebrando-a-milton-friedman-en-sus-102-años";s:8:"og:title";s:54:"Celebrando a Milton Friedman en sus 102 años - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/45ff2190802f9793d44160c4e551925c_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/45ff2190802f9793d44160c4e551925c_S.jpg";s:14:"og:description";s:160:"HACER celebra 102 años de Milton Friedman: &amp;amp;nbsp; Hoy, 31 de Julio de 2014, la Fundación HACER celebra 102 años de las ideas de Milton Friedman or...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:45:"Celebrando a Milton Friedman en sus 102 años";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}