<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9407:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId389"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	No más barreras a la creación de valor
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/bebada99aaa9847746eea59472544575_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/bebada99aaa9847746eea59472544575_XS.jpg" alt="No m&aacute;s barreras a la creaci&oacute;n de valor" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Emprendedor es aquella persona dispuesta a asumir los riesgos necesarios para obtener las ganancias deseadas a través de desarrollar un negocio, idea, bien o servicio. Lamentablemente el estatismo, burocracias, leyes complicadas, altos impuestos y un estado de derecho endeble han servido como barreras para destruir valor en vez de crearlo; dichos problemas que los liberales han criticado por décadas.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Las diversas realidades de los veinticinco países que participaron en el seminario* , India, Brasil, Argentina, Ucrania, Filipinas, Marruecos, Nepal, Costa Rica, Senegal, Zimbabue, Bruma, Jordania, Pakistán, Tanzania, Kazajstán, Honduras, Vietnam, Indonesia, Ecuador, República de Kosovo, Kirguistán, México, Malasia y Bután, mostraron que la falta de un Estado de derecho funcional determina que las fallas del mercado se vean acrecentadas en vez de solucionadas. La tiranía de una minoría, estando en el poder, consecuente a un débil Estado de derecho atiende que las libertades individuales sean violadas, como es el caso de Vietnam, país en el que la propiedad privada es un concepto ajeno a la realidad, ya que el Estado únicamente otorga a los individuos el uso de la tierra, pero el individuo no es el propietario.</p>
<p>&nbsp;</p>
<p>La coacción, utilizada en diversas formas por parte del estado, no permite la creación de valor. Coacción no sólo representada por los altos impuestos, sino también por la complicidad de las leyes y extensos requerimientos al momento de abrir una empresa, realizar algún trámite, obtener información fidedigna o cumplir con los impuestos, medidas que desincentiva a los emprendedores. Asimismo las barreras de entrada, a ciertas industrias, logran retrasar la formación competitiva de nuevos actores y, por lo tanto, el desarrollo de emprendedores capaces de crear ideas que tengan repercusión sobre esas industrias protegidas o sobre las nuevas industrias circundantes. El capitalismo mafioso o capitalismo de amigos representa un gran problema para la sociedad, ya que mantiene precios altos, consume recursos, a través de subsidios en su mayoría, que se pudieran aprovechar en otras industrias o sectores y no permite a los emprendedores a desarrollar nuevas estrategias, productos o servicios más eficientes.</p>
<p>&nbsp;</p>
<p>¿Queremos industrias protegidas? ¿Queremos barreras para la creación de empresas? ¿Queremos empleo?</p>
<p>&nbsp;</p>
<p>El ciclo de vida de los emprendedores se puede reducir a dos grupos de personas: Los recién graduados y personas que han trabajado en una compañía por muchos años. Los dos tipos de emprendedores se topan con barreras y limitaciones; conocimiento de las leyes, dinero y experiencia para los primeros y legal framework y complejidad de las leyes para los otros. Pero sabemos que para promover el emprendedurismo no es suficiente una sola reforma, sino que son diferentes reformas que mejoren la coordinación y planear el desarrollo del país.</p>
<p>&nbsp;</p>
<p>La tarea es respetar y fortalecer el estado de derecho, respetar y defender la propiedad privada y derechos de propiedad, lo que propiciaría un ambiente óptimo para los inversionistas y emprendedores. Se ha demostrados en diversas publicaciones, como el Doing Business, que al fortalecer el estado de derecho se crea y percibe un ambiente institucional favorable para el desarrollo de emprendedores que, asignando adecuadamente los recursos escasos, logran la creación de valor y aumentar la calidad de vida de la sociedad.</p>
<p>&nbsp;</p>
<p>La idea es simple, dentro de cualquier contexto, al facilitar el entendimiento de las leyes y reducir la complejidad de las mismas, las empresas tienen mayor tiempo para asignar sus recursos a sus actividades operativas y gastan menos recursos al enfrentarse a las trabas regulatorias. <em>"Si las leyes y regulaciones se pueden hacer cumplir ante un tribunal de justicia, si fuera necesario, el emprendedor tendrá más confianza para arriesgarse a hacer negocios desconocidos"**</em> . Al asignar la concentración de los recursos a las operaciones de la empresa, dentro de cualquier sociedad libre, tendrán mayor dinero*** lo que proporciona acceso universal cualquier bien o servicio concebible lo que desencadenará un crecimiento social integro.</p>
<p>&nbsp;</p>
<p>Por lo tanto, creo que el emprendedurismo debería de ser el objetivo de toda sociedad. Al generar un ambiente institucional se favorecería la competencia, reducirían los costos en la economía, protegerían los derechos fundamentales, se reduciría la corrupción; dicho de otra manera, se lograría una sociedad libre.</p>
<p>&nbsp;</p>
<p><span style="font-size: 8pt; font-family: arial, helvetica, sans-serif;"><em>*Vias políticas para la aperture de mercados y promoción del emprendedurismo, del 30 de marzo al 11 de abril de 2014, Gummersbach and Hamburg, Germany.</em></span></p>
<p><span style="font-size: 8pt; font-family: arial, helvetica, sans-serif;"><em>**&nbsp;Banco Mundial. 2014. Doing Business en México 2014. Washington, D.C</em></span></p>
<p><span style="font-size: 8pt; font-family: arial, helvetica, sans-serif;"><em>***Se debe entender el concepto acerca de la obtención de dinero como un desarrollo productivo y no como un concepto de gasto público.</em></span></p>
<p>&nbsp;</p>
<p>Autor: Miguel Torres Gil</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: RELIAL, Red Liberal de América Latina</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis">
			&laquo; Venezuela: Análisis económico de un país en crisis		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018">
			Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018 &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor#startOfPageId389">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:49:"No más barreras a la creación de valor - Relial";s:11:"description";s:154:"Emprendedor es aquella persona dispuesta a asumir los riesgos necesarios para obtener las ganancias deseadas a través de desarrollar un negocio, idea,...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:40:"No más barreras a la creación de valor";s:6:"og:url";s:107:"http://www.relial.org/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor";s:8:"og:title";s:49:"No más barreras a la creación de valor - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/bebada99aaa9847746eea59472544575_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/bebada99aaa9847746eea59472544575_S.jpg";s:14:"og:description";s:154:"Emprendedor es aquella persona dispuesta a asumir los riesgos necesarios para obtener las ganancias deseadas a través de desarrollar un negocio, idea,...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:40:"No más barreras a la creación de valor";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}