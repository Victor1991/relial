<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7185:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId221"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL visita la UFM
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/63ae8dd535459e6ddaa9950601158f8d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/63ae8dd535459e6ddaa9950601158f8d_XS.jpg" alt="RELIAL visita la UFM" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Si hay un lugar que inspira, estimula y logra que hasta el último detalle simbolice libertad, este lugar es la Universidad Francisco Marroquín (UFM). Una institución que -a través del compromiso con los valores, la ética y la responsabilidad como principios- se ha ganado el prestigio y reconocimiento de la comunidad académica en general y de los liberales de América Latina en particular.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Con tal agrado, la Red Liberal de América Latina (RELIAL), tuvo el gusto de visitar la UFM. Luego de un recorrido por el campus, el rector Gabriel Calzada, recibió a representantes de la Mesa Directiva de RELIAL, a la cabeza de su presidente Ricardo López Murphy; así se propició una grata reunión de acercamiento institucional de la que participó también la nueva directora regional de la Fundación Friedrich Naumann para la Libertad, Birgit Lamm.</p>
<p>&nbsp;</p>
<p>En nombre de RELIAL y Fundación Friedrich Naumann para la Libertad, nuestro sincero agradecimiento a la UFM por el recibimiento y la posibilidad de habernos permitido realizar nuestras reuniones en la Casa Popenoe, inmueble histórico en la ciudad de La Antigua.</p>
<p>&nbsp;</p>
<p>Visitar la UFM, más que un placer, es un compromiso con la ideas y con el ejemplo; pasear el campus, conocer sus innovaciones, ser partícipe de sus clases, es una inyección de entusiasmo para todos los que hemos hecho del liberalismo nuestra bandera.</p>
<p>&nbsp;</p>
<p>&nbsp;<span style="font-size: 11px;">&nbsp;</span></p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="images/UFM/mini.jpg" alt="" /></p>
<p style="text-align: center;"><span style="color: #999999;">&nbsp;<span style="font-size: 11px;">Birgit Lamm, Silvia Mercado, Ricardo López Murphy, Gabriel Calzada, Rocío Guijarro, Eduardo Montealegre, Gerardo Bongovanni</span></span></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="images/mini2.jpg" alt="" /></p>
<p style="text-align: center;">&nbsp;<span style="font-size: 11px;">&nbsp;<span style="color: #999999;">Ricardo López Murphy, Birgit Lamm, Rocío Guijarro, Luis Figueroa y Héctor Ñaupari</span></span></p>
<p style="text-align: center;"><span style="font-size: 11px;"><br /></span></p>
<p style="text-align: justify;">Más fotos, <a href="index.php/multimedia/fotos/item/215-relial-visita-la-ufm">acá</a></p>
<p style="text-align: justify;">Texto: Silvia Mercado / <span id="cloak18168">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak18168').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy18168 = 's&#105;lv&#105;&#97;.m&#101;rc&#97;d&#111;' + '&#64;';
 addy18168 = addy18168 + 'fnst' + '&#46;' + '&#111;rg';
 document.getElementById('cloak18168').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy18168 + '\'>' + addy18168+'<\/a>';
 //-->
 </script>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/219-encuentro-clave-entre-directivos-il-–-relial">
			&laquo; Encuentro clave entre Directivos IL – RELIAL		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/225-conferencia-internacional-sobre-“el-acuerdo-de-asociación-entre-centro-américa-y-la-unión-europea”-”los-mercados-de-la-aeronáutica-civil-y-de-la-energía-eléctrica”">
			Conferencia Internacional sobre “El Acuerdo de Asociación  entre Centro América y la Unión Europea”, ”Los mercados  de la Aeronáutica Civil y de la Energía Eléctrica” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/221-relial-visita-la-ufm#startOfPageId221">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:29:"RELIAL visita la UFM - Relial";s:11:"description";s:154:"Si hay un lugar que inspira, estimula y logra que hasta el último detalle simbolice libertad, este lugar es la Universidad Francisco Marroquín (UFM)....";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:20:"RELIAL visita la UFM";s:6:"og:url";s:90:"http://www.relial.org/index.php/productos/archivo/actualidad/item/221-relial-visita-la-ufm";s:8:"og:title";s:29:"RELIAL visita la UFM - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/63ae8dd535459e6ddaa9950601158f8d_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/63ae8dd535459e6ddaa9950601158f8d_S.jpg";s:14:"og:description";s:154:"Si hay un lugar que inspira, estimula y logra que hasta el último detalle simbolice libertad, este lugar es la Universidad Francisco Marroquín (UFM)....";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:20:"RELIAL visita la UFM";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}