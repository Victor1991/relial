<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11120:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId467"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	“Todos somos americanos”: Estados Unidos y Cuba
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<div>En la opinión de Ángel Soto, Profesor de la Universidad de los Andes, Chile</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Falta para ver si el discurso de Obama pasará a la historia como el que abrió las puertas a la normalización de las relaciones y de la democracia en Cuba, o si es uno más de un largo relato que ha tenido un solo perjudicado: el pueblo cubano.</div>
<div>&nbsp;</div>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<div>&nbsp;</div>
<div>"Todos somos americanos”, fueron las palabras que este miércoles 17 de diciembre pronunció en español el Presidente de los Estados Unidos, Barack Obama, en un discurso considerado por muchos como “histórico”.</div>
<div>&nbsp;</div>
<div>Calificación un tanto apresurada y que quizás responde a un deseo mediático, a una ansiedad universal porque “ocurra algo”, más que a un acontecimiento efectivo que verdaderamente trasciende en el tiempo. Aún queda mucho camino por recorrer, y el discurso de 15 minutos en televisión más bien dejó interrogantes que respuestas, sembrando la incógnita gatopardiana si acaso no es un cambio para que todo permanezca igual, o al menos hasta que los hermanos Castro se mantengan en el poder.</div>
<div>&nbsp;</div>
<div>Ciertamente que todos nos alegramos que se avance en el diálogo entre ambos países, y aprendemos que a veces es conveniente tener relaciones con países “no democráticos” para no quedar aislados y dar una lucha con mayor legitimidad.</div>
<div>&nbsp;</div>
<div>Asimismo, se confirma que, tras 50 años, la política norteamericana hacia la isla ha sido un fracaso y las sanciones económicas del bloqueo no tuvieron el efecto deseado. Al contrario, le sirvieron a Castro de chivo expiatorio para explicar internamente el fracaso de sus políticas.</div>
<div>&nbsp;</div>
<div>También nos alegramos que tanto Alan Gross, el preso estadounidense, como los cubanos Gerardo Hernández, Ramón Labañino y Antonio Guerrero, puedan regresar a sus países a reunirse con sus familias, especialmente tras haber sufrido –en ambos casos- detenciones calificadas como arbitrarias por el Grupo de Naciones Unidas.</div>
<div>&nbsp;</div>
<div>Pero, ¿qué ganó Estados Unidos en esta negociación? y ¿cuál es la razón del momento para hacerlo? ¿Por qué Cuba ha de querer acercarse a los Estados Unidos?</div>
<div>&nbsp;</div>
<div>No era necesario un anuncio de este tipo para un intercambio de prisioneros, y tampoco para mantener conversaciones de alto nivel. Por tanto, corresponde mirar otro camino, que en el caso de Obama, debe considerar como factor fundamental que éste no se está jugando la reelección, y para varios expertos, hay algo más que el cumplimiento de una promesa de campaña, sino que es el deseo de “pasar a la historia” como el Presidente norteamericano que abrió el camino a la normalización de las relaciones entre ambos países. ¿Ego personal?, o -como se sugiere- a pesar de su desconocimiento de la región, el Presidente norteamericano estaría pensando en cooptar el mercado latino con conferencias una vez terminado su mandato, dado que Clinton y Blair tienen cooptado el “mercado de universidades europeas y asiáticas”. Aún se recuerdan sus palabras al inicio de su primer mandato, que en temas latinoamericanos señaló que se acabó el tiempo de hablar y comenzó el de escuchar.</div>
<div>&nbsp;</div>
<div>En el caso de Cuba, puede preguntarse si no es acaso una más de las manifestaciones de oportunismo que han caracterizado a los Castro. Fue el propio Huber Matos el que endosó ese pragmatismo a Fidel, y que en la actualidad, sin la ayuda soviética y con una crisis profunda en Venezuela, les haya llevado a la conclusión de que, con una China y un Irán con problemas internos, sea necesario acercarse a los Estados Unidos para seguir sobreviviendo.</div>
<div>&nbsp;</div>
<div>Al menos es parte de lo que piensa Jon Perdue, de TFAS en Washington DC, para quien el régimen de Castro ha estado sobreviviendo durante la última década gracias al petróleo venezolano. “Hoy, con los precios del petróleo cayendo en picada y el régimen venezolano perdiendo su capacidad para continuar subsidiando a Cuba, lo último que los EE.UU. debiera hacer es lanzar un salvavidas de ayuda al régimen de Castro mediante la apertura de recursos bancarios y las relaciones comerciales que sólo les servirá a los Castro para seguir oprimiendo el pueblo cubano”. ¿Qué habría pasado si no recibe este salvavidas?</div>
<div>&nbsp;</div>
<div>Quizás no sirve a los intereses de Estados Unidos empujar a Cuba hacia el colapso, pero ¿qué hará Cuba para que el secretario de Estado John Kerry pueda eliminar al país caribeño de la lista de países que Estados Unidos considera patrocinan el terrorismo?</div>
<div>&nbsp;</div>
<div>¿Qué ganó la política norteamericana? Ya en el 2009 Obama había levantado las restricciones a los viajes familiares y envíos de remesas a Cuba. Cierto, ahora se ampliarán, pero desde entonces se venían pidiendo al gobierno cubano cambios reales que evidentemente no han existido. Incluso más, en este “histórico discurso” de Obama incluso hay pasajes en los que -podría decirse- el hombre más poderoso del mundo casi peca de ingenuidad al afirmar que “una política de compromiso” puede ser más eficaz que una de aislamiento, cuestión que sería cierta si al frente tuviera a un interlocutor con historial de cumplimiento. No vaya a ser que ambas políticas resulten un fracaso.</div>
<div>&nbsp;</div>
<div>Obama, como buen basquetbolista, dejó boteando la pelota y se la pasó al nuevo Congreso, de mayoría republicana, que asumirá el 2015, pues dos de los aspectos centrales de esta “nueva relación” deben ser aprobados por el Senado. El primero, nombrar un embajador. Cuestión que requiere de la mayoría del Senado, y ya el senador Marco Rubio anunció que se opondrá sea quien sea el candidato. Y en segundo lugar, la eliminación del bloqueo. Elemento central de las relaciones entre ambos países, especialmente si consideramos que el propio Raúl Castro, en su alocución a los cubanos señaló –y solicitó a Obama- que “esto no quiere decir que lo principal se haya resuelto”, pues debe terminarse con el “bloqueo económico, comercial y financiero que provoca enormes daños económicos y humanos”.</div>
<div>&nbsp;</div>
<div>Tal como afirmó el analista norteamericano Joseph Humire, Executive Director del Center for a Secure Free Society en Washington DC, Cuba, a pesar de su pobreza económica, ha tenido (y tiene) mucho poder e influencia en Latinoamérica y este episodio la legitima aún más. En su opinión, cualquier negociación implica conciliar intereses comunes, y en este caso no es claro qué es lo que ganó la política de EE.UU. &nbsp;¿Es parte de un plan para cerrar Guantánamo el próximo año? Incluso más, señala Humire, hasta aparece casi contradictorio, pues hace poco la Casa Blanca anunció sanciones a Venezuela y ahora se relaja con Cuba. Una decisión que &nbsp;obedecería a la visión personal de Obama, más que a la política de estado norteamericana.</div>
<div>&nbsp;</div>
<div>Sí, “todos somos americanos”, y tal como afirmó en su editorial The New York Times, la decisión de Estados Unidos y Cuba de restablecer sus lazos diplomáticos cierra “uno de los capítulos más equivocados” de la política exterior estadounidense. Pero aún falta para ver si este discurso pasará a la historia como el que abrió las puertas de la normalización de las relaciones y de la democracia en Cuba, o si es uno más de un largo relato que ha tenido un solo perjudicado: el pueblo cubano.</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Texto proporcionado por el autor.</div>
<div>&nbsp;</div>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/466-la-normalización">
			&laquo; La Normalización		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/468-si-no-es-ahora-¿cuándo?">
			Si no es ahora, ¿cuándo? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/467-“todos-somos-americanos”-estados-unidos-y-cuba#startOfPageId467">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:60:"“Todos somos americanos”: Estados Unidos y Cuba - Relial";s:11:"description";s:156:"En la opinión de Ángel Soto, Profesor de la Universidad de los Andes, Chile &amp;nbsp; &amp;nbsp; Falta para ver si el discurso de Obama pasará a la h...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:51:"“Todos somos americanos”: Estados Unidos y Cuba";s:6:"og:url";s:125:"http://www.relial.org/index.php/productos/archivo/opinion/item/467-â€œtodos-somos-americanosâ€-estados-unidos-y-cuba";s:8:"og:title";s:60:"“Todos somos americanos”: Estados Unidos y Cuba - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:164:"En la opinión de Ángel Soto, Profesor de la Universidad de los Andes, Chile &amp;amp;nbsp; &amp;amp;nbsp; Falta para ver si el discurso de Obama pasará a la h...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:51:"“Todos somos americanos”: Estados Unidos y Cuba";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}