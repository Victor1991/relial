<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9513:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId492"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Con ayudita no vale
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/a01253be6ea05e1e8a72b1a2a0636467_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/a01253be6ea05e1e8a72b1a2a0636467_XS.jpg" alt="Con ayudita no vale" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">Por Yesenia Álvarez, Presidente del Instituto Político para la Libertad IPL Perú</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;"><br /></span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">"Las mujeres no debemos caer en la trampa de lo políticamente correcto y apoyar estas medidas por el solo hecho de ser mujeres. En mi opinión, las cuotas y la alternancia son una afrenta al mérito y una ofensa para la mujeres talentosas y calificadas que vienen ganando espacios en la política, ya que no serán elegidas por sus habilidades o eficiencia sino por su género".</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;"><br /></span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">La mejor prueba de que la cuota electoral de género no ha funcionado como se esperaba es que van casi 18 años desde que esta idea se introdujo en la normativa de nuestro país y no ha logrado mejorar sustancialmente la participación política de las mujeres en el Congreso. Esto último no me lo he inventado yo, lo señala el actual Ministerio de la Mujer y Poblaciones Vulnerables, al que le preocupa que en las tres últimas elecciones al Congreso de la República la cuota de género no haya sido alcanzada y que además en las últimas elecciones del 2011 el porcentaje de mujeres elegidas se redujo al 21,5% respecto del 29% obtenido en el 2006. Todo esto siendo la mujeres el mayor porcentaje del electorado.</span></p>
<p><span style="font-family: Verdana, sans-serif; font-size: 8.5pt;"><br /></span></p>
<p><span style="font-family: Verdana, sans-serif; font-size: 8.5pt;">Después de haber defendido las cuotas, el ministerio, en el documento "Realidades y desafíos de la participación política de las mujeres", concluye que la realidad ha demostrado que la cuota de género no es suficiente para avanzar en una participación equilibrada entre hombres y mujeres. Y entonces esperaríamos que se pierda la confianza en este tipo de medidas y se vaya a las raíces de por qué las mujeres no participan abrumadoramente en política, y por qué no son de la preferencia de los votantes. Pero no, saltan a una idea descabellada, ya que plantean la alternancia de género, según la cual los partidos políticos tendrán que alternar un hombre y una mujer en sus listas de candidatos al Congreso. Es decir, con los años se demuestra que la medida fracasa, y el Estado miope resuelve que hay que ir a una versión más dura de la misma medida. Eso es absurdo.</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">El documento, con mucha corrección política, incluso nos plantea una falsa dicotomía: ¿Alternancia o exclusión? ¿De qué lado estamos? No hay salida; si criticamos la alternancia, estamos excluyendo. Esto denota la ceguera para enfrentar el problema real, y ver otras alternativas de solución acordes con el principio de igualdad ante la ley y con una cultura inclusiva que no sea impuesta por el Estado, ni a costa de otros, sino una que esté en las venas de los ciudadanos.</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">Las cuotas y su versión de alternancia de género son dañinas, se fundan en la idea de que para evitar discriminar o cometer una injusticia se puede discriminar así se cometa una injusticia. Desconoce que desde el Estado no se deben hacer distinciones ni privilegiar a unos ciudadanos sobre otros. La alternancia es más perversa porque plantea que ya no basta la igualdad en la postulación, la cual se defendía como temporal, sino que hay que ir más allá y cuestionar lo que el pueblo elegirá: los resultados.</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">Las mujeres no debemos caer en la trampa de lo políticamente correcto y apoyar estas medidas por el solo hecho de ser mujeres. En mi opinión, las cuotas y la alternancia son una afrenta al mérito y una ofensa para la mujeres talentosas y calificadas que vienen ganando espacios en la política, ya que no serán elegidas por sus habilidades o eficiencia sino por su género.</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">Las mujeres no necesitamos ayudita. Me sorprende que tantas mujeres destacadas la defiendan. Si vamos por la ayudita, como nos advierte el estadounidense Thomas Sowell, "el curanderismo social sustituirá los esfuerzos reales para afrontar problemas reales que pueden destrozar una sociedad".</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">Foto: <span style="font-family: Verdana, sans-serif; font-size: 11.3333330154419px;">Instituto&nbsp;</span>&nbsp;Político para la Libertad, IPL Perú</span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;"><span style="font-family: Verdana, sans-serif; font-size: 11.3333330154419px;">Texto: Yesenia Álvarez</span></span></p>
<p><span style="font-size: 8.5pt; font-family: Verdana, sans-serif;">Autor: <a href="http://elcomercio.pe/%20">Texto publicado originalmente en el Diario EL Comercio&nbsp;el día 20 de mayo de 2015.</a></span></p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/486-el-gurú-de-obama-y-un-mundo-sin-cabeza">
			&laquo; El gurú de Obama y un mundo sin cabeza		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/504-terrorismo-y-terrorista">
			Terrorismo y terrorista &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/492-con-ayudita-no-vale#startOfPageId492">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:28:"Con ayudita no vale - Relial";s:11:"description";s:157:"Por Yesenia Álvarez, Presidente del Instituto Político para la Libertad IPL Perú &quot;Las mujeres no debemos caer en la trampa de lo políticamente co...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:19:"Con ayudita no vale";s:6:"og:url";s:82:"http://relial.org/index.php/productos/archivo/opinion/item/492-con-ayudita-no-vale";s:8:"og:title";s:28:"Con ayudita no vale - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/a01253be6ea05e1e8a72b1a2a0636467_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/a01253be6ea05e1e8a72b1a2a0636467_S.jpg";s:14:"og:description";s:161:"Por Yesenia Álvarez, Presidente del Instituto Político para la Libertad IPL Perú &amp;quot;Las mujeres no debemos caer en la trampa de lo políticamente co...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:19:"Con ayudita no vale";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}