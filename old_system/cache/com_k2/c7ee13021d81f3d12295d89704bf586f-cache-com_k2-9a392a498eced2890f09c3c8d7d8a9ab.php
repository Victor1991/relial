<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:64738:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
	


		<!-- Item list -->
	<div class="itemList">

				<!-- Leading items -->
		<div id="itemListLeading">
			
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
	  		EDUCAR -además del Estado- SÍ ES POSIBLE	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una sociedad empoderada cuestiona y defiende sus derechos y libertades; no permite abusos ni se conforma con migajas".&nbsp;</p>
<p>&nbsp;</p>
<p>Un artículo de: Tatiana Macías Muentes, Ecuador.&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
	  		RELIAL alerta sobre democracia en Nicaragua	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Miembros de la Red Liberal de América Latina (RELIAL)</strong>, ante la decisión del Tribunal Electoral de Nicaragua de despojar a 28 diputados electos por el pueblo nicaragüense en el año 2011 de sus credenciales como miembros del Poder Legislativo de ese país, manifestamos nuestra indignación por estos hechos que violentan la decisión soberana de un pueblo que fue a las urnas y eligió a sus representantes ante el parlamento y preocupación por las actuaciones de las instituciones nicaragüenses que han orquestado la desaparición de la oposición con el propósito de dejar como único candidato presidencial al señor Daniel Ortega para el proceso electoral de noviembre del 2016.</p>
<p>&nbsp;</p>
<p>Reconocemos que la democracia y los derechos de los ciudadanos en Nicaragua han recibido un fuerte ataque y nos sumamos a aquellas personas de todo el mundo que hoy levantan la voz ante este golpe dado a los legisladores separados de sus cargos solo por representar los intereses de la población que no piensa como este gobierno.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
	  		Olas de Cambio - tercer número de la Mirada Liberal	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>L<strong>a Mirada Liberal,</strong> revista de análisis y coyuntura política de América Latina, esta vez en su tercer número les ofrece artículos relacionadosa los procesos electorales de los dos últimos años.&nbsp;</p>
<p>&nbsp;</p>
<p>Quedan invitados a leer la reflexión de nuestros expertos <a href="images/Miradas/FNF%20RELIAL_MLiberal_OlasDC-digital_22jun-16.pdf" target="_blank"><strong>aquí</strong></a></p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
	  		Las elecciones peruanas del 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
	  		“No hay comida”: el fantasma del hambre se apodera de Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
	  		Indice de Calidad Institucional 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

    <div class="catItemLinks">

		
		
	  
	  	  <!-- Item attachments -->
	  <div class="catItemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="catItemAttachments">
		    		    <li>
			    <a title="ICI2016_Espayol.pdf" href="/index.php/component/k2/item/download/122_ac0bf3fcea2394ef3ff239466c19332c">
			    	ICI2016_Espayol.pdf			    </a>
			    		    </li>
		    		    <li>
			    <a title="ICI2016_Ingles.pdf" href="/index.php/component/k2/item/download/123_23c18d349bf61d52c0ec72faa0f3598a">
			    	ICI2016_Ingles.pdf			    </a>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
	  		Perú: retos de gestión para el próximo gobierno	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Si se gobierna con buenos gestores podríamos estar al inicio de una verdadera transformación".</p>
<p>&nbsp;</p>
<p>En la opinión de Daniel Córdova,</p>
<p>Presidente del<a href="http://www.invertir.org.pe/"> Instituto Invertir</a>, Perú</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
	  		Juntos en el compromiso por la libertad personal alrededor del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideología socialista – una convicción política que compartió con su amigo colombiano, el Premio Nobel de Literatura, Gabriel García Márquez. Esa amistad se rompió, desde luego no sólo a causa de la ya memorable disputa que sostuvieron ambos y la cual dejó al colombiano un moretón (luego de este incidente hace 30 años ninguno de los contrincantes se volvió a pronunciar al respecto), sino porque también se transformó rápidamente de un entusiasta socialista a un convencido liberal. Cualquier tipo de intolerancia y política autoritaria son para él una atrocidad, sin importar bajo qué etiqueta política se cometan.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
	  		El líder lilght, el caudillo malo y el profeta enardecido	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>El 1 de marzo será el Super martes. Doce estados norteamericanos realizarán sus primarias en una atmósfera de suspense que tiene mucho de pasión irracional. Será un "duelo al sol", pero a tres pistolas, como se vio en el debate de la noche del jueves, ganado claramente por Marco Rubio en el O.K. Corral de CNN y Telemundo. Quien salga victorioso el Super martes poseerá una altísima probabilidad de ser el candidato de su partido.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
	  		CEDICE Libertad en Venezuela entre los mejores think tanks del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Cedice Libertad</strong> entre los primeros Centros de Estudios <em>(think tank</em>) de acuerdo al ranking que elabora la Universidad de Pensilvania</p>
<p><strong>Como difusor del pensamiento liberal y por su influencia en la opinión pública venezolana</strong></p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
	  		EDUCAR -además del Estado- SÍ ES POSIBLE	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una sociedad empoderada cuestiona y defiende sus derechos y libertades; no permite abusos ni se conforma con migajas".&nbsp;</p>
<p>&nbsp;</p>
<p>Un artículo de: Tatiana Macías Muentes, Ecuador.&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
	  		RELIAL alerta sobre democracia en Nicaragua	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Miembros de la Red Liberal de América Latina (RELIAL)</strong>, ante la decisión del Tribunal Electoral de Nicaragua de despojar a 28 diputados electos por el pueblo nicaragüense en el año 2011 de sus credenciales como miembros del Poder Legislativo de ese país, manifestamos nuestra indignación por estos hechos que violentan la decisión soberana de un pueblo que fue a las urnas y eligió a sus representantes ante el parlamento y preocupación por las actuaciones de las instituciones nicaragüenses que han orquestado la desaparición de la oposición con el propósito de dejar como único candidato presidencial al señor Daniel Ortega para el proceso electoral de noviembre del 2016.</p>
<p>&nbsp;</p>
<p>Reconocemos que la democracia y los derechos de los ciudadanos en Nicaragua han recibido un fuerte ataque y nos sumamos a aquellas personas de todo el mundo que hoy levantan la voz ante este golpe dado a los legisladores separados de sus cargos solo por representar los intereses de la población que no piensa como este gobierno.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
	  		Olas de Cambio - tercer número de la Mirada Liberal	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>L<strong>a Mirada Liberal,</strong> revista de análisis y coyuntura política de América Latina, esta vez en su tercer número les ofrece artículos relacionadosa los procesos electorales de los dos últimos años.&nbsp;</p>
<p>&nbsp;</p>
<p>Quedan invitados a leer la reflexión de nuestros expertos <a href="images/Miradas/FNF%20RELIAL_MLiberal_OlasDC-digital_22jun-16.pdf" target="_blank"><strong>aquí</strong></a></p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
	  		Las elecciones peruanas del 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
	  		“No hay comida”: el fantasma del hambre se apodera de Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
	  		Indice de Calidad Institucional 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

    <div class="catItemLinks">

		
		
	  
	  	  <!-- Item attachments -->
	  <div class="catItemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="catItemAttachments">
		    		    <li>
			    <a title="ICI2016_Espayol.pdf" href="/index.php/component/k2/item/download/122_ac0bf3fcea2394ef3ff239466c19332c">
			    	ICI2016_Espayol.pdf			    </a>
			    		    </li>
		    		    <li>
			    <a title="ICI2016_Ingles.pdf" href="/index.php/component/k2/item/download/123_23c18d349bf61d52c0ec72faa0f3598a">
			    	ICI2016_Ingles.pdf			    </a>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
	  		Perú: retos de gestión para el próximo gobierno	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Si se gobierna con buenos gestores podríamos estar al inicio de una verdadera transformación".</p>
<p>&nbsp;</p>
<p>En la opinión de Daniel Córdova,</p>
<p>Presidente del<a href="http://www.invertir.org.pe/"> Instituto Invertir</a>, Perú</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
	  		Juntos en el compromiso por la libertad personal alrededor del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideología socialista – una convicción política que compartió con su amigo colombiano, el Premio Nobel de Literatura, Gabriel García Márquez. Esa amistad se rompió, desde luego no sólo a causa de la ya memorable disputa que sostuvieron ambos y la cual dejó al colombiano un moretón (luego de este incidente hace 30 años ninguno de los contrincantes se volvió a pronunciar al respecto), sino porque también se transformó rápidamente de un entusiasta socialista a un convencido liberal. Cualquier tipo de intolerancia y política autoritaria son para él una atrocidad, sin importar bajo qué etiqueta política se cometan.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
	  		El líder lilght, el caudillo malo y el profeta enardecido	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>El 1 de marzo será el Super martes. Doce estados norteamericanos realizarán sus primarias en una atmósfera de suspense que tiene mucho de pasión irracional. Será un "duelo al sol", pero a tres pistolas, como se vio en el debate de la noche del jueves, ganado claramente por Marco Rubio en el O.K. Corral de CNN y Telemundo. Quien salga victorioso el Super martes poseerá una altísima probabilidad de ser el candidato de su partido.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
	  		CEDICE Libertad en Venezuela entre los mejores think tanks del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Cedice Libertad</strong> entre los primeros Centros de Estudios <em>(think tank</em>) de acuerdo al ranking que elabora la Universidad de Pensilvania</p>
<p><strong>Como difusor del pensamiento liberal y por su influencia en la opinión pública venezolana</strong></p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
				<!-- Secondary items -->
		<div id="itemListSecondary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
	  		EDUCAR -además del Estado- SÍ ES POSIBLE	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"La educación es un sector que considero como la verdadera herramienta del desarrollo de una nación y por eso destino mi vida a ella. Una sociedad empoderada cuestiona y defiende sus derechos y libertades; no permite abusos ni se conforma con migajas".&nbsp;</p>
<p>&nbsp;</p>
<p>Un artículo de: Tatiana Macías Muentes, Ecuador.&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
	  		RELIAL alerta sobre democracia en Nicaragua	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Miembros de la Red Liberal de América Latina (RELIAL)</strong>, ante la decisión del Tribunal Electoral de Nicaragua de despojar a 28 diputados electos por el pueblo nicaragüense en el año 2011 de sus credenciales como miembros del Poder Legislativo de ese país, manifestamos nuestra indignación por estos hechos que violentan la decisión soberana de un pueblo que fue a las urnas y eligió a sus representantes ante el parlamento y preocupación por las actuaciones de las instituciones nicaragüenses que han orquestado la desaparición de la oposición con el propósito de dejar como único candidato presidencial al señor Daniel Ortega para el proceso electoral de noviembre del 2016.</p>
<p>&nbsp;</p>
<p>Reconocemos que la democracia y los derechos de los ciudadanos en Nicaragua han recibido un fuerte ataque y nos sumamos a aquellas personas de todo el mundo que hoy levantan la voz ante este golpe dado a los legisladores separados de sus cargos solo por representar los intereses de la población que no piensa como este gobierno.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
	  		Olas de Cambio - tercer número de la Mirada Liberal	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>L<strong>a Mirada Liberal,</strong> revista de análisis y coyuntura política de América Latina, esta vez en su tercer número les ofrece artículos relacionadosa los procesos electorales de los dos últimos años.&nbsp;</p>
<p>&nbsp;</p>
<p>Quedan invitados a leer la reflexión de nuestros expertos <a href="images/Miradas/FNF%20RELIAL_MLiberal_OlasDC-digital_22jun-16.pdf" target="_blank"><strong>aquí</strong></a></p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
	  		Las elecciones peruanas del 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Es esencial despejar el panorama electoral de los fantasmas antidemocráticos con los que algunos medios lo han sazonado"</p>
<p>&nbsp;</p>
<p>Lima, especial para RELIAL</p>
<p>Autores:</p>
<p>Mijael Garrido Lecca Palacios @MijaelGLP</p>
<p>Ariana Lira Delcore @arianalirad</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
	  		“No hay comida”: el fantasma del hambre se apodera de Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>¿Cómo es posible que escaseen los alimentos, la electricidad, el agua y las medicinas? La respuesta es simple: corrupción, mala gestión y Comunismo</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
	  		Indice de Calidad Institucional 2016	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

    <div class="catItemLinks">

		
		
	  
	  	  <!-- Item attachments -->
	  <div class="catItemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="catItemAttachments">
		    		    <li>
			    <a title="ICI2016_Espayol.pdf" href="/index.php/component/k2/item/download/122_ac0bf3fcea2394ef3ff239466c19332c">
			    	ICI2016_Espayol.pdf			    </a>
			    		    </li>
		    		    <li>
			    <a title="ICI2016_Ingles.pdf" href="/index.php/component/k2/item/download/123_23c18d349bf61d52c0ec72faa0f3598a">
			    	ICI2016_Ingles.pdf			    </a>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
	  		Perú: retos de gestión para el próximo gobierno	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Si se gobierna con buenos gestores podríamos estar al inicio de una verdadera transformación".</p>
<p>&nbsp;</p>
<p>En la opinión de Daniel Córdova,</p>
<p>Presidente del<a href="http://www.invertir.org.pe/"> Instituto Invertir</a>, Perú</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
	  		Juntos en el compromiso por la libertad personal alrededor del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La experiencia de pobreza, violencia y dictaduras en América Latina le convirtieron en un entusiasta partidario de la Revolución Cubana y de su ideología socialista – una convicción política que compartió con su amigo colombiano, el Premio Nobel de Literatura, Gabriel García Márquez. Esa amistad se rompió, desde luego no sólo a causa de la ya memorable disputa que sostuvieron ambos y la cual dejó al colombiano un moretón (luego de este incidente hace 30 años ninguno de los contrincantes se volvió a pronunciar al respecto), sino porque también se transformó rápidamente de un entusiasta socialista a un convencido liberal. Cualquier tipo de intolerancia y política autoritaria son para él una atrocidad, sin importar bajo qué etiqueta política se cometan.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
	  		El líder lilght, el caudillo malo y el profeta enardecido	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>El 1 de marzo será el Super martes. Doce estados norteamericanos realizarán sus primarias en una atmósfera de suspense que tiene mucho de pasión irracional. Será un "duelo al sol", pero a tres pistolas, como se vio en el debate de la noche del jueves, ganado claramente por Marco Rubio en el O.K. Corral de CNN y Telemundo. Quien salga victorioso el Super martes poseerá una altísima probabilidad de ser el candidato de su partido.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView group">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
	  		CEDICE Libertad en Venezuela entre los mejores think tanks del mundo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Cedice Libertad</strong> entre los primeros Centros de Estudios <em>(think tank</em>) de acuerdo al ranking que elabora la Universidad de Pensilvania</p>
<p><strong>Como difusor del pensamiento liberal y por su influencia en la opinión pública venezolana</strong></p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
				<!-- Link items -->
		<div id="itemListLinks">
			<h4>K2_MORE</h4>
			
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
	  		EDUCAR -además del Estado- SÍ ES POSIBLE	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
	  		RELIAL alerta sobre democracia en Nicaragua	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/actualidad/item/519-olas-de-cambio-tercer-número-de-la-mirada-liberal">
	  		Olas de Cambio - tercer número de la Mirada Liberal	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/actualidad/item/517-las-elecciones-peruanas-del-2016">
	  		Las elecciones peruanas del 2016	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/actualidad/item/516-“no-hay-comida”-el-fantasma-del-hambre-se-apodera-de-venezuela">
	  		“No hay comida”: el fantasma del hambre se apodera de Venezuela	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/component/k2/item/515-indice-de-calidad-institucional-2016">
	  		Indice de Calidad Institucional 2016	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno">
	  		Perú: retos de gestión para el próximo gobierno	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
	  		Juntos en el compromiso por la libertad personal alrededor del mundo	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
	  		El líder lilght, el caudillo malo y el profeta enardecido	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
						
			
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout (links) -->
<div class="catItemView group">
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
	  				<a href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo">
	  		CEDICE Libertad en Venezuela entre los mejores think tanks del mundo	  	</a>
	  		  </h3>
	  
	  	  
	  </div>
<!-- End K2 Item Layout (links) -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
				<div class="clr"></div>
			</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:15:"Relial - Relial";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:53:"http://www.relial.org/?option=com_k2&view=itemlist&am";s:8:"og:title";s:15:"Relial - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:0:"";}}s:5:"links";a:2:{s:57:"/index.php/component/k2/itemlist?format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:58:"/index.php/component/k2/itemlist?format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}