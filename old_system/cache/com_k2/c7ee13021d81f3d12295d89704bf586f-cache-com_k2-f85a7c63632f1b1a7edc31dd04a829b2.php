<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12859:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId491"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL en el World Economic Forum
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/161bb2cd9d87d4fb2583e55eca6a3af4_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/161bb2cd9d87d4fb2583e55eca6a3af4_XS.jpg" alt="RELIAL en el World Economic Forum" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Por Agustín Etchebarne</p>
<p>La semana pasada se realizó el Foro Económico Mundial para América Latina (World Economic Forum), en Riviera Maya, México, con el objetivo de debatir y formular estrategias para el mejoramiento integral de la región. Estuvieron presentes en el Foro los presidentes de México, Panamá y Haití; el Premio Nobel de Economía Joseph Stiglitz, además de unos 700 empresarios, ministros, secretarios y académicos de la región y del resto del mundo. </p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Quienes tuvimos el privilegio de asistir, en mi caso apoyado por la Fundación Naumann y RELIAL, pudimos observar la división entre dos Latinoaméricas.</p>
<p>&nbsp;</p>
<p>Un primer grupo de países que está abierto al mundo e intentando cerrar la brecha con los países más ricos y desarrollados del planeta. Allí, encontramos a países como el anfitrión, México, que desde hace 21 años integra el Tratado de Libre Comercio con EE.UU. y Canadá (o TLCAN como se conoce allí). La progresiva integración con la América del Norte le permitió multiplicar por 7 sus exportaciones y diversificarlas al punto tal que hoy tres cuartas partes se componen de manufacturas. Como ejemplo, el mes de abril sus exportaciones de autos aumentaron un 22% interanual. México no se quedó solo con el TLCAN, sino que hace tres años consolidó la Alianza del Pacífico con Chile, Colombia y Perú. Estos países comercian con 0% de arancel el 93% de los productos y tienen un cronograma para ir reduciendo los aranceles en el resto durante la próxima década. Pero además, se habló mucho del avance en la integración en un tratado de inversiones sobre inversiones, comercio y regulaciones expandido hacia el Pacífico, a través de la Alianza Transpacífica (conocida por su sigla en inglés TPP), que se amplía a 12 países: Australia, Brunei, Canadá, Chile, Japón, Malasia, México, Nueva Zelanda, Perú, Singapur, EE.UU, y Vietnam.</p>
<p>&nbsp;</p>
<p>Se habló también de la brecha que existe entre los países más desarrollados del grupo y los que todavía están más rezagados. Allí podemos ver, que no todos fueron logros. El TLCAN sirvió para que México reduzca demasiado lentamente la brecha que lo distancia de los EE.UU.. Para explicar la demora se mencionaron aspectos que faltan mejorar, como la libre movilidad de las personas (ese es un muro que falta derribar), y de los capitales. Muchos destacaron el logro de haber superado en casi todos los países los golpes militares y la profundización de la democracia, pero inmediatamente agregaban la necesidad de profundizar las reformas estructurales que empezaron en los 90. El presidente de México, Peña Nieto, describió largamente el programa de 11 reformas estructurales que está en marcha y que destacaron varios de sus ministros, aunque algunos opositores mencionan que todavía les falta mucho para mejorar aún a nivel de las instituciones. Y en ese sentido y otros sentidos, varios oradores destacaron a Chile y no a México, como el modelo a seguir (esto a pesar de que justo en esa semana se desató la crisis política que derivó en el pedido de renuncia a todo el gabinete de Michel Bachelet).</p>
<p>&nbsp;</p>
<p>En cuanto a las reformas estructurales necesarias, los paneles que más me inspiraron fueron los que se dedicaron al tema de la educación y la brecha de habilidades entre los países más desarrollados y los latinoamericanos. Por ejemplo, China produce 600.000 nuevos ingenieros y científicos cada año, frente a unos 100.000 de México, y apenas 30.000 en Brasil.</p>
<p>&nbsp;</p>
<p>En ese sentido, se debatió la mejor manera de acelerar la capacitación y la educación en los países latinoamericanos. Se reconoció que como decía Mandela es "el arma más poderosa para cambiar el mundo". La reforma educativa en México establece que el propósito de la educación es tener ciudadanos libres. La nueva ley reivindica el rol de los maestros, y construyó un sistema de evaluación autónomo independiente. Pero sobre todo, estableció tres objetivos:</p>
<p>&nbsp;</p>
<p>a) Aprender a aprender. Hoy se sabe que no es el maestro quien debe impartir los conocimientos. El conocimiento se duplica cada año y cada vez más rápido, y está disperso en el mundo pero está alcance de cualquiera que tenga acceso a Internet.</p>
<p>&nbsp;</p>
<p>b) Aprender a convivir. Es decir, junto con la libertad es necesario aprender la responsabilidad y reconocer los derechos del prójimo como los propios.</p>
<p>&nbsp;</p>
<p>c) Aprender a hacer. Los conocimientos no deben ser abstractos sino que deben ser útiles para que cada uno logre ser libre e independiente y poder producir lo necesario para poder autofinanciarse y cubrir las necesidades de la familia.</p>
<p>&nbsp;</p>
<p>Otro tema muy destacado fue la cooperación público-privada en la educación. Así, en Enseña por América (Teach for América) se describió cómo esa ONG recluta a los mejores egresados de las universidades para que enseñen en escuelas públicas (con el modelo Charter) en EE.UU., durante un año, y cómo esos egresados con esa experiencia obtienen luego excelentes remuneraciones cuando se reintegran al mercado empresarial, generando una situación ganar-ganar. Se describieron también otros exitosos modelos de cooperación (partnership) como el del Rotary que fue útil para eliminar la polio en casi todo el mundo, y que ahora se aplica a mejorar la educación en Brasil. O el presidente de General Motors describió cómo las universidades pueden aliarse con las empresas para mejorar la educación y hacerla más efectiva para las necesidades futuras de las empresas de un determinado sector como, en su caso, es el automotriz.</p>
<p>&nbsp;</p>
<p>El segundo grupo está integrado por países que pretenden cerrarse sobre sí mismos y que tienen un dramático problema institucional. Me tocó integrar un país con varios representantes de este tipo de países, Cuba, Venezuela, Brasil y Colombia (que en realidad estaba como país que todavía tiene problemas para terminar el conflicto con la guerrilla). En este grupo el principal problema no es de izquierdas o derechas sino que todavía se debaten entre populismo proteccionista y estatista vs. Instituciones Republicanas. Así, está representado por el conflictivo Mercosur, con Argentina, Brasil, Uruguay y Paraguay y a dónde se sumó Venezuela. Es sintomático, que los países del primer grupo todavía crecen, a pesar del impacto de la caída de los commodities; mientras que en este segundo grupo Venezuela tiene una muy fuerte recesión y tiene la más alta inflación del mundo, Argentina tiene la segunda inflación más alta del mundo y también está en recesión, y Brasil también está en recesión y con inflación en ascenso, aunque en menor grado. Y los tres países tienen una gran crisis de corrupción. Uruguay y Paraguay están atrapados en este paquete, aunque seguramente deseosos de que el Mercosur cambie de estrategia y busque integrarse al mundo.</p>
<p>&nbsp;</p>
<p>Aprovechamos para presentar el índice de Calidad Institucional, elaborado por Martín Krause para Libertad y Progreso, con apoyo de la Fundación Naumann y Relial. Allí, podemos observar un ranking de 193 países, según el promedio de ocho indicadores, cuatro referidos a instituciones políticas y cuatro a instituciones de mercado. Venezuela y Cuba se ubican en las últimas posiciones, 184 y 173 respectivamente; mientras que Argentina está en el puesto 137, siendo el país que más cayó en la última década (56 posiciones). En cambio, Chile se encuentra en el puesto 22, muy cerca de EE.UU. y Canadá; Uruguay algo más abajo en el puesto 43. Brasil y México están en una posición intermedia 96 y 88 respectivamente.</p>
<p>&nbsp;</p>
<p>Dicho esto, podemos terminar con dos notas positivas: Primero que Argentina se encuentra en un proceso electoral que va a terminar este año con un nuevo presidente, que sea cualquiera de los dos principales candidatos, será más moderado y amigable que la presidente saliente; y en el caso de que sea Mauricio Macri, será una enorme oportunidad de cambio. El segundo hecho positivo, es que los cambios realizados en Brasil, debido a la crisis institucional, han llevado al gobierno a un equipo más amigable con las instituciones republicanas. Esto le permitió decir a Daniel Marteleto Godinho, Ministro de Desarrollo, industria y Comercio Exterior, que ya es una decisión tomada, la integración de Brasil al mundo y que comenzará con acelerar las negociaciones para un tratado de Libre Comercio con Europa. Las dificultades de este tratado es que el Mercosur requiere que todo tratado arancelario sea negociado en conjunto por todos los países, pero este escollo posiblemente pueda ser superado con la ayuda del próximo gobierno en Argentina.</p>
<p>&nbsp;</p>
<p>Texto proporcionado por el autor</p>
<p>Imagen, logo LyP.&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/489-preocupa-a-la-región-el-doble-estándar-en-ddhh-y-el-boom-del-autoritarismo">
			&laquo; Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/494-una-mirada-liberal-compendio-cumbre-de-las-américas">
			Una mirada liberal, compendio "Cumbre de las Américas" &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/491-relial-en-el-world-economic-forum#startOfPageId491">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:42:"RELIAL en el World Economic Forum - Relial";s:11:"description";s:157:"Por Agustín Etchebarne La semana pasada se realizó el Foro Económico Mundial para América Latina (World Economic Forum), en Riviera Maya, México, con...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:33:"RELIAL en el World Economic Forum";s:6:"og:url";s:148:"http://relial.org/index.php/productos/archivo/actualidad/item/491-relial-en-el-world-economic-forum/index.php/2013-03-28-23-35-13/democracia-liberal";s:8:"og:title";s:42:"RELIAL en el World Economic Forum - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/161bb2cd9d87d4fb2583e55eca6a3af4_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/161bb2cd9d87d4fb2583e55eca6a3af4_S.jpg";s:14:"og:description";s:157:"Por Agustín Etchebarne La semana pasada se realizó el Foro Económico Mundial para América Latina (World Economic Forum), en Riviera Maya, México, con...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:33:"RELIAL en el World Economic Forum";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}