<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17793:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Opinión</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/399-la-fuerte-oposición-que-santos-enfrentará-en-su-segundo-tiempo">
	  		La fuerte oposición que Santos enfrentará en su segundo tiempo	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/399-la-fuerte-oposición-que-santos-enfrentará-en-su-segundo-tiempo" title="La fuerte oposici&oacute;n que Santos enfrentar&aacute; en su segundo tiempo">
		    	<img src="/media/k2/items/cache/5b98a51d844cf083418c7193dcee292b_XS.jpg" alt="La fuerte oposici&oacute;n que Santos enfrentar&aacute; en su segundo tiempo" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Andrés Molano</p>
<p>&nbsp;</p>
<p>Luego de los comicios del domingo, la política colombiana comenzó a rearmarse, quedando en evidencia el polarizado panorama que deberá enfrentar el reelecto Presidente Juan Manuel Santos en el Congreso. Porque desde la derecha tendrá al Centro Democrático, liderado por el ex Presidente Alvaro Uribe (2002-2010), y por el lado de la izquierda, estarán el Polo Democrático y la Alianza Verde.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/399-la-fuerte-oposición-que-santos-enfrentará-en-su-segundo-tiempo">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana">
	  		Política local y participación ciudadana	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana" title="Pol&iacute;tica local y participaci&oacute;n ciudadana">
		    	<img src="/media/k2/items/cache/34570ba96cc3a4daee50221a47a4e2ec_XS.jpg" alt="Pol&iacute;tica local y participaci&oacute;n ciudadana" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Eugenio Guerrero</p>
<p>&nbsp;</p>
<p>INTRODUCCIÓN</p>
<p>&nbsp;</p>
<p>El presente reporte recoge la capacitación obtenida en la academia de formación Theodor-Heuss-Akademie de la Fundación Friedrich Naumann para la Libertad (FNFF), Gummersbach, que tuvo lugar del 4 al 16 de mayo.. El programa cargado de diversas y extraordinarias experiencias desde una perspectiva de la libertad en las políticas locales y el planteamiento democrático con gobiernos limitados. El tema tratado a lo largo y profundo del programa estuvo basado en la Política Local y la Participación ciudadana. Doce días de aprendizajes interesantes en las dinámicas grupales, un intercambio cultural sumamente grandioso con personas muy inteligentes que defienden la liberad, de alrededor de 20 países, totalmente distintos en culturas y tradiciones pero juntos con una meta: La Libertad.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/394-política-local-y-participación-ciudadana">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa">
	  		La inmensa tarea de los reyes de España	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa" title="La inmensa tarea de los reyes de Espa&ntilde;a">
		    	<img src="/media/k2/items/cache/15d406f06ce12f2ac57cb5137d1afc69_XS.jpg" alt="La inmensa tarea de los reyes de Espa&ntilde;a" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Francisco Franco murió en 1975 seguro de que el futuro de España estaba "atado y bien atado". Nunca he creído la hipótesis de que el Caudillo preparó una transición post mórtem hacia la democracia. Franco era un hombre de orden y cuartel, melancólicamente convencido de que los "demonios familiares" del separatismo y la anarquía inevitablemente conducirían a los españoles a la catástrofe, a menos que una mano dura lo evitara.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/393-la-inmensa-tarea-de-los-reyes-de-españa">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018">
	  		Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018" title="Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018">
		    	<img src="/media/k2/items/cache/5ef17ed4d733d4dc3519b291889643fe_XS.jpg" alt="Colombia decide: Claves para entender las elecciones presidenciales 2014 - 2018" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Análisis de los resultados de la primera vuelta de las elecciones presidenciales en Colombia, de las perspectivas abiertas para la segunda vuelta el próximo 15 de junio y del talante de los dos candidatos en contienda.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/392-colombia-decide-claves-para-entender-las-elecciones-presidenciales-2014-2018">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor">
	  		No más barreras a la creación de valor	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor" title="No m&aacute;s barreras a la creaci&oacute;n de valor">
		    	<img src="/media/k2/items/cache/bebada99aaa9847746eea59472544575_XS.jpg" alt="No m&aacute;s barreras a la creaci&oacute;n de valor" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Emprendedor es aquella persona dispuesta a asumir los riesgos necesarios para obtener las ganancias deseadas a través de desarrollar un negocio, idea, bien o servicio. Lamentablemente el estatismo, burocracias, leyes complicadas, altos impuestos y un estado de derecho endeble han servido como barreras para destruir valor en vez de crearlo; dichos problemas que los liberales han criticado por décadas.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/389-no-más-barreras-a-la-creación-de-valor">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis">
	  		Venezuela: Análisis económico de un país en crisis	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis" title="Venezuela: An&aacute;lisis econ&oacute;mico de un pa&iacute;s en crisis">
		    	<img src="/media/k2/items/cache/e561084e9220383c7204eb40f9bc13f1_XS.jpg" alt="Venezuela: An&aacute;lisis econ&oacute;mico de un pa&iacute;s en crisis" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Durante los últimos quince años en Venezuela se adoptó un sistema político con alto intervencionismo estatal en todos los ámbitos de la vida en sociedad, que sus promotores denominaron "Socialismo del Siglo XXI". El orden institucional se tornó altamente discrecional para aplicar políticas populistas clientelares, ejerciendo un poder casi hegemónico por el uso de la fueza o la amenaza del uso de la fuerza. Las libertades políticas, económicas y civiles se han visto cada vez más restringidas, dado que el régimen ha interpretado, cambiado y acomodado la constitución y las leyes a los intereses del grupo gobernante. Esto ha provocado abuso de poder político, corrupción rampante, altos niveles de inseguridad ciudadana, escasez, inflación, restricción a los derechos de asociación y expresión, entre otros.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/382-venezuela-análisis-económico-de-un-país-en-crisis">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/opinion?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/opinion?start=30" class="pagenav">Anterior</a></li><li><a title="2" href="/index.php/productos/archivo/opinion?start=6" class="pagenav">2</a></li><li><a title="3" href="/index.php/productos/archivo/opinion?start=12" class="pagenav">3</a></li><li><a title="4" href="/index.php/productos/archivo/opinion?start=18" class="pagenav">4</a></li><li><a title="5" href="/index.php/productos/archivo/opinion?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/opinion?start=30" class="pagenav">6</a></li><li><span class="pagenav">7</span></li><li><a title="8" href="/index.php/productos/archivo/opinion?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/opinion?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/opinion?start=54" class="pagenav">10</a></li><li><a title="11" href="/index.php/productos/archivo/opinion?start=60" class="pagenav">11</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/opinion?start=42" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/opinion?start=84" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 7 de 15	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:17:"Opinión - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:62:"http://relial.org/index.php/productos/archivo/opinion?start=36";s:8:"og:title";s:17:"Opinión - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:43:"http://relial.org/media/k2/categories/2.jpg";s:5:"image";s:43:"http://relial.org/media/k2/categories/2.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}}s:6:"module";a:0:{}}