<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7539:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId340"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Diarios venezolanos reciben papel enviado por Andiarios
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/c1a11e28afb03c9d81c096faa0a5ce8e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/c1a11e28afb03c9d81c096faa0a5ce8e_XS.jpg" alt="Diarios venezolanos reciben papel enviado por Andiarios" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Bogotá, Abril 11 de 2014.- Luego de 10 días de recorrido desde Cartagena, y de los respectivos trámites de aduana en Venezuela, los diarios El Impulso, El Nacional y El Nuevo Pías, empiezan a recibir las 52 toneladas de papel enviadas por la prensa colombiana a través de Andiarios.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>El diario El Impulso, de Barquisimeto, en el estado de Lara, fue el primero en recibir la carga de 18 rollos de papel que servirán para algunos días más de circulación en condiciones normales. Por su parte, los diarios El Nacional y el Nuevo País, en Caracas, recibirán 39 y 18 rollos respectivamente.</p>
<p>&nbsp;</p>
<p>Nora Sanín, Directora Ejecutiva de la Asociación Colombiana de Editores de Diarios y Medios Informativos, Andiarios, afirmó que "Es muy satisfactorio contribuir de esta manera a la libertad de prensa y al derecho a la información de los Venezolanos. Hemos recibido muchos mensajes de agradecimiento que nos llenan de emoción"</p>
<p>&nbsp;</p>
<p>Adicional a la entrega de estas 52 toneladas de papel, Andiarios, con el apoyo de los diarios El Colombiano, El Espectador, El Heraldo, El País, El Universal, Hoy Diario del Magdalena, La Opinión, La Patria, y Vanguardia Liberal, todos afiliados a la Asociación; y el Instituto de Ciencia Política Hernán Echavarría Olózaga, anuncian el préstamo de 62 toneladas de papel adicionales que deben llegar a mediados del mes de mayo al diario El Impulso, el más antiguo de Venezuela.</p>
<p>&nbsp;</p>
<p>Por su parte, Miguel Enrique Otero, Director de El Nacional; confirmó que se está concretando el apoyo de varios periódicos del continente, entre ellos el diario El Tiempo de Colombia, para el préstamo de otro cargamento de papel para ese diario, uno de los más importantes de Venezuela y que se ha visto obligado a reducir la publicación de 32 a 8 páginas.</p>
<p>&nbsp;</p>
<p>Este préstamo se enmarca dentro de la iniciativa "Todos somos Venezuela. Sin libertad de prensa no hay democracia", en la que decenas de diarios de la región han publicado en sus páginas artículos tomados de los periódicos venezolanos, como un acto solidario con la prensa de ese país.</p>
<p>&nbsp;</p>
<p>Acerca de Andiarios</p>
<p>&nbsp;</p>
<p>La Asociación Colombiana de Editores de Diarios y Medios Informativos, ANDIARIOS, es una organización sin ánimo de lucro, instituida el 15 de septiembre de 1961 por las empresas periodísticas de mayor prestigio y solidez del país. A partir de 1962, fecha en la cual ANDIARIOS obtuvo su reconocimiento jurídico, la entidad ha venido cumpliendo un papel cada vez más activo en el acontecer periodístico nacional. ANDIARIOS es una institución privada que, comprometida con la libertad de prensa y el derecho a la información, propende por una industria periodística pujante, que sea la fuente principal de información y orientación de los colombianos.</p>
<p>&nbsp;</p>
<p>Afiliados</p>
<p>&nbsp;</p>
<p>ADN, Aja, Al Día, Ámbito Jurídico, Boyacá 7 Días, Diario del Sur, Diario Mío, El Colombiano, El Diario del Otún, El Espectador, El Heraldo, El Meridiano de Córdoba, El Meridiano de Sucre, El Nuevo Día, El Nuevo Siglo, El País, El Pilón, El Propio, El Teso, El Tiempo, El Universal, Gente, Hoy Diario del Magdalena, Kien &amp; Ke, La Crónica del Quindío, La Libertad, La Opinión, La Patria, La República, La Tarde, Llano 7 días, Nuevo Estadio, Portafolio, Publimetro, Q'Hubo, Tolima 7 Días, Vanguardia Liberal, Vea Pues.</p>
<p>&nbsp;</p>
<p>Fuente: Instituto de Ciencia Política - Hernan Echavarria Olozaga - ICP</p>
<p>Foto: ICP</p>
<p>Autor: andiarios, Gentileza del ICP</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/339-lo-que-los-think-tanks-pro-libre-mercado-deben-a-los-“chicago-boys”">
			&laquo; Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/341-la-oposición-venezolana-arrasa">
			La oposición venezolana arrasa &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/340-diarios-venezolanos-reciben-papel-enviado-por-andiarios#startOfPageId340">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:64:"Diarios venezolanos reciben papel enviado por Andiarios - Relial";s:11:"description";s:156:"Bogotá, Abril 11 de 2014.- Luego de 10 días de recorrido desde Cartagena, y de los respectivos trámites de aduana en Venezuela, los diarios El Impulso...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:55:"Diarios venezolanos reciben papel enviado por Andiarios";s:6:"og:url";s:121:"http://relial.org/index.php/productos/archivo/actualidad/item/340-diarios-venezolanos-reciben-papel-enviado-por-andiarios";s:8:"og:title";s:64:"Diarios venezolanos reciben papel enviado por Andiarios - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/c1a11e28afb03c9d81c096faa0a5ce8e_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/c1a11e28afb03c9d81c096faa0a5ce8e_S.jpg";s:14:"og:description";s:156:"Bogotá, Abril 11 de 2014.- Luego de 10 días de recorrido desde Cartagena, y de los respectivos trámites de aduana en Venezuela, los diarios El Impulso...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:55:"Diarios venezolanos reciben papel enviado por Andiarios";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}