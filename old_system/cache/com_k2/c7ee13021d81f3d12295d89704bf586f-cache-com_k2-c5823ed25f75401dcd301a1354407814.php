<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16154:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId327"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El Parlamento Europeo sobre la situación en Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>&nbsp;</p>
<p style="text-align: center;"><span style="font-size: 10pt; text-align: left; font-family: arial, helvetica, sans-serif;">Resolución del Parlamento Europeo, de 27 de febrero de 2014, sobre la situación en Venezuela (2014/2600(RSP)</span></p>
<p style="text-align: center;"><span style="font-size: 10pt; text-align: left; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;– Vistas sus resoluciones anteriores sobre la situación en Venezuela, en particular su Resolución, de 24 de mayo de 2007, sobre el caso de la cadena Radio Caracas Televisión en Venezuela , de 23 de octubre de 2008 sobre las inhabilitaciones políticas en Venezuela , de 7 de mayo de 2009 sobre el caso de Manuel Rosales en Venezuela , de 11 de febrero de 2010 sobre Venezuela , de 8 de julio de 2010 sobre Venezuela, en particular el caso de María Lourdes Afiuni , y de 24 de mayo de 2012 sobre la posible retirada de Venezuela de la Comisión Interamericana de Derechos Humanos ,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">– Vista la declaración del portavoz de Catherine Ashton, Vicepresidenta de la Comisión / Alta Representante de la Unión para Asuntos Exteriores y Política de Seguridad (VP/AR), de 14 de febrero de 2014,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">– Vista la declaración de Catherine Ashton, VP/AR, de 21 de febrero de 2014, sobre los desórdenes en Venezuela,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">– Visto el Pacto Internacional de Derechos Civiles y Políticos, del que Venezuela es parte,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">– Vista la Declaración Universal de Derechos Humanos de 1948,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">– Visto el artículo 110, apartados 2 y 4, de su Reglamento,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">A. Considerando la gravedad de la actual situación de Venezuela; que desde el 12 de febrero de 2014 se han registrado en toda Venezuela marchas pacíficas encabezadas por estudiantes que se han saldado con violencia mortal y ha habido, por lo menos, trece víctimas mortales, más de setenta heridos y cientos de detenidos; que las exigencias de los estudiantes tienen que ver con la incapacidad del Gobierno del Presidente Maduro de resolver los problemas de la elevada tasa de inflación, la delincuencia y la escasez de algunos productos de primera necesidad, así como con los niveles, cada vez más elevados, de corrupción y con la intimidación de los medios de comunicación y de la oposición democrática; que el Gobierno ha culpado de la escasez a los «saboteadores» y a los «empresarios corruptos hambrientos de ganancias»; que Venezuela es el país que cuenta con las reservas energéticas más importantes de América Latina;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">B. Considerando que en los últimos días no ha disminuido el número de manifestaciones, sino que, en realidad, ha aumentado, lo que ha conducido a un aumento del número de muertos, heridos y detenidos como resultado de la represión del movimiento de protesta por parte de las autoridades estatales y de grupos armados ilegales;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">C. Considerando que la tensión política y la polarización en Venezuela son cada vez más importantes; que las autoridades venezolanas, en lugar de contribuir al mantenimiento de la paz y de la calma, han amenazado con llevar a cabo una «revolución armada»;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">D. Considerando los actos de represión ejercida, en particular, contra estudiantes, periodistas, dirigentes de la oposición y activistas pacíficos de la sociedad civil, que han sido perseguidos y privados de su libertad;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">E. Considerando que grupos armados progubernamentales violentos y descontrolados han estado actuando en Venezuela con impunidad durante mucho tiempo; que la oposición ha acusado a estos grupos de incitar a la violencia durante las manifestaciones pacíficas, lo que ha provocado muertes y numerosos heridos; que el Gobierno de Venezuela aún no ha aclarado plenamente esos hechos;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">F. Considerando que los medios de comunicación son objeto de censura e intimidación, que diferentes periodistas han sido golpeados o detenidos o que se ha destruido su material profesional;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">G. Considerando que la libertad de expresión y el derecho a participar en manifestaciones pacíficas son elementos fundamentales de la democracia, y que la igualdad y la justicia para todos no pueden existir sin las libertades fundamentales y el respeto de los derechos de todos los ciudadanos; que la Constitución de Venezuela garantiza el derecho de reunión, de asociación y de manifestación ciudadana por medios pacíficos; que las autoridades estatales tienen el deber de salvaguardar los derechos fundamentales de los ciudadanos de Venezuela, de garantizar su seguridad y proteger sus vidas sin limitar dichos derechos;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">H. Considerando que únicamente el respeto de los derechos y las libertades fundamentales, un diálogo constructivo y respetuoso y la tolerancia podrán ayudar a Venezuela a salir de esta grave crisis y, por ende, a superar futuras dificultades;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">1. Condena todos los actos de violencia y la trágica pérdida de vidas humanas durante las manifestaciones pacíficas del 12 de febrero de 2014 y los días posteriores, y transmite su sincero pésame a los familiares de los fallecidos;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">2. Expresa su inquebrantable solidaridad con el pueblo venezolano y su preocupación por la posibilidad de que las nuevas protestas puedan desembocar en más actos violentos que no harían más que ahondar la brecha entre las posiciones del Gobierno y la oposición y contribuirían a polarizar en mayor medida la delicada situación política que se registra en Venezuela; pide a los representantes de todos los partidos y estamentos de la sociedad venezolana que mantengan la calma tanto en lo que se refiere a sus declaraciones como a las acciones que emprendan;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">3. Recuerda al Gobierno de Venezuela que la libertad de expresión y el derecho a participar en manifestaciones pacíficas son derechos fundamentales de la persona en toda democracia, tal y como reconoce la Constitución venezolana, y pide al Presidente Maduro que respete los tratados internacionales de los que Venezuela es parte, en particular la Carta Democrática Interamericana;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">4. Recuerda al Gobierno de Venezuela su obligación de garantizar la seguridad de todos los ciudadanos del país, independientemente de sus opiniones o adscripciones políticas; expresa su profunda preocupación por la detención de estudiantes y de dirigentes de la oposición y reclama su liberación inmediata;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">5. Recuerda que el respeto del principio de la separación de poderes es fundamental en un régimen democrático, y que el sistema judicial no puede ser utilizado por las autoridades como medio de persecución política y de represión de la oposición democrática; pide a las autoridades venezolanas que retiren las acusaciones infundadas y las órdenes de detención contra dirigentes de la oposición;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">6. Pide a las autoridades venezolanas que desarmen y disuelvan inmediatamente a los grupos armados descontrolados progubernamentales, y que pongan fin a su impunidad; solicita aclaraciones sobre los fallecimientos que se han registrado, con el fin de exigir responsabilidades a los autores por sus actos;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">7. Alienta a todas las partes y, en particular, a las autoridades venezolanas, a mantener un diálogo pacífico con todos los estamentos de la sociedad venezolana para definir los puntos de convergencia y permitir que los interlocutores políticos debatan los problemas de mayor gravedad que afronta el país;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">8. Hace hincapié en que el respeto de la libertad de prensa, de información y de opinión, junto con el pluralismo político, son fundamentales para la democracia; lamenta la existencia de censura en los medios de comunicación e internet y de un acceso limitado a ciertos blogs y redes sociales; condena el acoso de que han sido víctimas varios periódicos y medios audiovisuales, como la cadena NTN24 y la cadena CNN en español, y considera que estas prácticas son contrarias a la Constitución venezolana y a los compromisos asumidos por la República Bolivariana de Venezuela;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">9. Pide que se envíe a Venezuela cuanto antes una delegación ad hoc del Parlamento Europeo para evaluar la situación;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">10. Encarga a su Presidente que transmita la presente Resolución al Consejo, a la Comisión, a la Vicepresidenta de la Comisión / Alta Representante de la Unión para Asuntos Exteriores y Política de Seguridad, al Gobierno y a la Asamblea Nacional de la República Bolivariana de Venezuela, a la Asamblea Parlamentaria Euro-Latinoamericana y al Secretario General de la Organización de los Estados Americanos.</span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/299-manifiesto-de-mérida-de-los-estudiantes">
			&laquo; Manifiesto de Mérida de los estudiantes		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/339-lo-que-los-think-tanks-pro-libre-mercado-deben-a-los-“chicago-boys”">
			Lo que los Think Tanks pro Libre Mercado deben a los “Chicago Boys” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/327-el-parlamento-europeo-sobre-la-situación-en-venezuela#startOfPageId327">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:63:"El Parlamento Europeo sobre la situación en Venezuela - Relial";s:11:"description";s:157:"&amp;nbsp; Resolución del Parlamento Europeo, de 27 de febrero de 2014, sobre la situación en Venezuela (2014/2600(RSP) &amp;nbsp;– Vistas sus resoluc...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:54:"El Parlamento Europeo sobre la situación en Venezuela";s:6:"og:url";s:120:"http://relial.org/index.php/productos/archivo/actualidad/item/327-el-parlamento-europeo-sobre-la-situación-en-venezuela";s:8:"og:title";s:63:"El Parlamento Europeo sobre la situación en Venezuela - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:165:"&amp;amp;nbsp; Resolución del Parlamento Europeo, de 27 de febrero de 2014, sobre la situación en Venezuela (2014/2600(RSP) &amp;amp;nbsp;– Vistas sus resoluc...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:54:"El Parlamento Europeo sobre la situación en Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}