<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9766:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId507"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La pata podrida
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/fd2436c23111d947c615d80fa5115911_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/fd2436c23111d947c615d80fa5115911_XS.jpg" alt="La pata podrida" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Escribo en víspera de las elecciones españolas. Según las encuestas, termina el bipartidismo. Las grandes fuerzas políticas se fragmentarán en cuatro. Lamentablemente, una de las patas de ese nuevo banco trae consigo un grave factor desestabilizador.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Lo explico.</p>
<p>&nbsp;</p>
<p>Tras la muerte de Franco, desde la restauración de la democracia a mediados de la década de los setenta, el centro derecha (UCD y PP) ha gobernado durante 17 años divididos en tres periodos (Suárez-Calvo Sotelo, Aznar y Rajoy), mientras el centro izquierda (PSOE) lo ha hecho por 22 años (González y R. Zapatero). En democracia, Felipe González es la persona que ha ocupado el cargo de Presidente de Gobierno por más tiempo consecutivo: 14 años.</p>
<p>&nbsp;</p>
<p>Estos casi 40 años democráticos, edificados sobre un periodo similar de dictadura franquista lleno de luces y sombras, de atropellos y aciertos, han sido los mejores de la historia de España. El país dio un salto hacia el desarrollo y la modernidad, alcanzó un PIB anual de US$30 000 y florecieron las obras públicas como nunca antes. Fue la etapa en que media docena de compañías españolas se convirtieron en los mayores inversionistas extranjeros en América Latina.</p>
<p>&nbsp;</p>
<p>España es hoy uno de los vivideros más gratos del planeta. Es una de las pocas naciones en el mundo que disponen de medio centenar de ciudades hermosas y perfectamente habitables, dotadas de todos los servicios. La longevidad es altísima, la violencia social mínima y, de acuerdo con todos los estándares, es un país del Primer Mundo. No a la cabeza de ese segmento, pero tampoco en la cola. Radica en una grata medianía razonablemente confortable, como acredita el Índice de Desarrollo Humano que publica anualmente la ONU. En el 2014 ocupaba el lugar número 26 de las casi 200 naciones escrutadas.</p>
<p>&nbsp;</p>
<p>¿Qué ha pasado? ¿Por qué, si el bipartidismo, evidentemente, ha dado resultado, los españoles están a punto de pulverizarlo en las urnas?</p>
<p>&nbsp;</p>
<p>A mi juicio, la razón fundamental es un cambio en las prioridades de la sociedad española. El principal objetivo de los años setenta y ochenta fue consolidar la democracia en el país. Fueron años duros en los que se alternaron las conspiraciones militares con las bombas y los asesinatos de ETA y Terra Lliure, terroristas vascos y catalanes que intentaban crear a sangre y fuego dictaduras independentistas de corte soviético, siniestras organizaciones a las que se agregaba el GRAPO, otro grupo de asesinos, también colocados bajo la autoridad ideológica del marxismo-leninismo.</p>
<p>&nbsp;</p>
<p>Para contrarrestar esa peligrosa deriva, España, de la mano del PSOE, que había abandonado las supersticiones marxistas, se integró en la Unión Europea y en la OTAN, y profundizó el desguace del aparato productivo en manos del Estado, creado en la época esencialmente estatista del franquismo. El PP, cuando ganó las elecciones, continuó por la misma senda liberalizadora, decididamente enfrentado a los violentos, pero poniendo más cuidado en el gasto público y en las variables macroeconómicas, lo que facilitó el ingreso del país en el euro. España había hecho sus deberes brillantemente.</p>
<p>&nbsp;</p>
<p>Las prioridades hoy son otras. La mayor parte de la sociedad española, ya conquistada la democracia, quiere buen gobierno, honradez en la administración de los caudales públicos –descuidada tanto por el PSOE como por el PP--, y oportunidades económicas para ese 22% de personas desempleadas, incluida la mitad de los hombres y mujeres que tienen entre 20 y 30 años.</p>
<p>&nbsp;</p>
<p>La inmensa mayoría está satisfecha dentro de la Unión Europea, no pone en duda que la OTAN es un excelente escudo protector que sirve, además, para amansar a las Fuerzas Armadas, y tampoco discute que el mercado, la existencia de la propiedad privada y la democracia, constituyen un modelo socio-político –la democracia liberal-- hasta ahora insuperado. Discrepan, eso sí, como en todas partes, sobre el monto de la presión fiscal y sobre el mejor destino de la recaudación pública.</p>
<p>&nbsp;</p>
<p>Sin embargo, una de las cuatro formaciones de las que sustituirán al bipartidismo PP-PSOE, tiene unas características diferentes. Es peligrosamente antisistema. Me refiero a Podemos, la versión española del neocomunismo, que ha irrumpido en España bajo el liderazgo de Pablo Iglesias.</p>
<p>&nbsp;</p>
<p>El propósito de este grupo, o al menos de sus líderes más conocidos, no es mejorar la calidad de la democracia liberal, sino destruirla y sustituirla por un modelo más cercano a Venezuela o a Cuba dentro de las coordenadas populistas de quienes postulan una suerte de agresivo tercermundismo antioccidental, como demuestra el sistemático alineamiento de ese grupo en el Parlamento Europeo.</p>
<p>&nbsp;</p>
<p>Pablo Iglesias, este político radical de cabellos largos e ideas cortas y dañinas, ha sido captado en cámara haciendo afirmaciones disparatadas, como que envidia el chavismo venezolano, proponiendo se le entreguen 650 euros mensuales de renta básica a todos los españoles, atacando a los medios de comunicación privados por antidemocráticos, afirmando que la deuda externa no se paga, defendiendo la salida de España de la OTAN y la ruptura de los pactos con Estados Unidos, y proponiendo la instalación de la guillotina por medio de una ominosa mezcla de humor negro y amenaza real.</p>
<p>&nbsp;</p>
<p>Cualquier pacto postelectoral que incluya a este grupo será contraproducente. Cuando los demócratas cedieron ante los fascistas y los nazis, pasó lo que pasó. Cuando los venezolanos les abrieron la puerta a los chavistas, el país se derrumbó. Esa es la pata podrida que puede desestabilizar este nuevo banco.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Texto: Artículo proporcionado por el autor</p>
<p>Foto: El blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/504-terrorismo-y-terrorista">
			&laquo; Terrorismo y terrorista		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
			El líder lilght, el caudillo malo y el profeta enardecido &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/507-la-pata-podrida#startOfPageId507">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:24:"La pata podrida - Relial";s:11:"description";s:158:"Escribo en víspera de las elecciones españolas. Según las encuestas, termina el bipartidismo. Las grandes fuerzas políticas se fragmentarán en cuatro....";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:15:"La pata podrida";s:6:"og:url";s:78:"http://relial.org/index.php/productos/archivo/opinion/item/507-la-pata-podrida";s:8:"og:title";s:24:"La pata podrida - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/fd2436c23111d947c615d80fa5115911_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/fd2436c23111d947c615d80fa5115911_S.jpg";s:14:"og:description";s:158:"Escribo en víspera de las elecciones españolas. Según las encuestas, termina el bipartidismo. Las grandes fuerzas políticas se fragmentarán en cuatro....";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:15:"La pata podrida";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}