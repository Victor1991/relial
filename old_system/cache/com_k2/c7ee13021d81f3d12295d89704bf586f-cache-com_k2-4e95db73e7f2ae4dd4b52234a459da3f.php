<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7683:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId66"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	¿Dónde está America Latina?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><strong></strong>&nbsp;<strong><img style="margin: 2px; border: 2px solid #a7a9ac; float: left;" alt="626px-Latin America terrain" src="images/fotos/626px-Latin_America_terrain.jpg" width="104" height="95" />El análisis de Carlos Alberto Montaner</strong></p>
<p>¿Cómo está su mujer? Depende. ¿Comparada con quién? Ese es un diálogo frecuente entre españoles jocosos. Me imagino que las mujeres pueden responder de la misma manera. Los maridos salimos muy mal parados si nos comparan con Brad Pitt y mucho mejor si el contraste es con Eduardo Gómez, el superfeo padre del portero en la comiquísima serie Aquí no hay quien viva de la televisión madrileña.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Con los países y las regiones sucede lo mismo. Para comprender dónde estamos hay que saber dónde están los otros y a qué ritmo nos movemos. Todo esto viene a cuento de la reciente noticia sobre los países más exitosos de América Latina. Según el cable, las tres economías más ricas de América Latina eran Chile, Panamá (que lleva casi una década creciendo al 8%) y Uruguay.Argentina quedaba relegada a un cuarto lugar, dato que acaso se explica por la falta de transparencia. El gobierno de Cristina Kirchner aparentemente adultera el grado de inflación para maquillar los resultados de su pobre gestión, con lo cual hace casi imposible establecer el PIB real de los argentinos.</p>
<p>Pese a sus limitaciones, el PIB per cápita sigue siendo el dato clave para entender de un chispazo el nivel de prosperidad. Sale de la suma de todos los bienes y servicios producido por una nación, dividida entre el número de sus habitantes. Para que esa cifra tenga algún sentido, es conveniente ajustarla a lo que puede adquirirse con ella o Purchasing Power Parity (PPP). ¿De qué vale ganar 20 dólares por hora si una botella de agua cuesta 50?</p>
<p>Grosso modo, el planeta cuenta con siete mil millones de habitantes y produce anualmente unos 83 mil billones (trillions en inglés) de dólares. Eso da, redondeando la cifra, unos $12 000 per cápita. Algunas sociedades muy prósperas, como la estadounidense, alcanza los $50 000, mientras otras muy pobres, como la haitiana, apenas llegan a los $1700.</p>
<p>Pero retengamos la cifra promedio mundial: 12 000 dólares.</p>
<p>Hay varios países latinoamericanos que, efectivamente, están por encima de esa cifra: Chile, Panamá, Uruguay, Argentina, Puerto Rico, México, Venezuela y Costa Rica. Brasil está exactamente en la frontera: 12 000 dólares.</p>
<p>Pero la mayor parte cae por debajo del promedio del planeta: Perú ($10 800), Colombia ($10 700), Cuba ($10 200), República Dominicana ($9 600), Ecuador ($8 800), El Salvador ($7 700), Paraguay ($6 100), Guatemala ($5 200), Bolivia ($5 000), Honduras ($4 600) y Nicaragu a ($3 300).De esos datos es posible extraer algunas conclusiones:</p>
<p>El impetuoso crecimiento de Chile y Panamá, dos de las economías más abiertas de la región, indica que ese camino es el más corto para llegar al Primer Mundo. Es posible que en el 2020, si no desvían el rumbo, esas dos naciones arriben a un nivel de prosperidad semejante al promedio de la Unión Europea que hoy está en $34 500. (España, en medio de la crisis, mantiene un PIB per cápita de $32 300).</p>
<p>Por la otra punta del razonamiento, los países del llamado Socialismo del Siglo XXI, (Cuba, Venezuela, Ecuador, Bolivia y Nicaragua), con la excepción de Venezuela, que continúa su declive relativo, están todos por debajo del promedio mundial. Eso debería indicarles que transitan en la dirección equivocada.</p>
<p>Venezuela, que en su momento estaba a la cabeza de América Latina, hoy ocupa el sexto puesto en ingreso per cápita, con apenas $13 200, pese al río de petrodólares que recibe. Debe ser el país peor administrado de América Latina.</p>
<p>Brasil sigue siendo el país de ese futuro luminoso que nunca llega. El volumen de su economía es grande porque se trata de una nación de 200 millones de habitantes, pero su desempeño real deja mucho que desear. En el pasado se hablaba de Brasil como Belindia. Una nación que tenía un segmento desarrollado, como el belga, mientras la mayor parte vivía como en la India.</p>
<p>Todavía esa cruel metáfora mantiene su vigencia.</p>
<p>En definitiva, ¿cómo está América Latina? Depende. A mi juicio, el desempeño es mediocre. Podría ser mucho mejor.</p>
<p>&nbsp;</p>
<p>Fuente: Carlos Alberto Montaner, Miembro de la Junta Honorífica de RELIAL.</p>
<p>Fuente:http://commons.wikimedia.org/wiki/File:Latin_America_terrain.jpg</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/56-último-combate-contra-la-prensa-libre">
			Último combate contra la prensa libre &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/66-américa-latina-map#startOfPageId66">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:39:"¿Dónde está America Latina? - Relial";s:11:"description";s:161:"&amp;nbsp;El análisis de Carlos Alberto Montaner ¿Cómo está su mujer? Depende. ¿Comparada con quién? Ese es un diálogo frecuente entre españoles jocos...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:30:"¿Dónde está America Latina?";s:6:"og:url";s:85:"http://www.relial.org/index.php/productos/archivo/opinion/item/66-américa-latina-map";s:8:"og:title";s:39:"¿Dónde está America Latina? - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:165:"&amp;amp;nbsp;El análisis de Carlos Alberto Montaner ¿Cómo está su mujer? Depende. ¿Comparada con quién? Ese es un diálogo frecuente entre españoles jocos...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:30:"¿Dónde está America Latina?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}