<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9428:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId278"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Virtudes republicanas, molestias para el tirano
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/ab98897d60eaa0d05c1862c118538407_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/ab98897d60eaa0d05c1862c118538407_XS.jpg" alt="Virtudes republicanas, molestias para el tirano" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Enrique Fernández García</p>
<p>&nbsp;</p>
<p>La república no es nada, sólo un nombre sin sustancia ni forma.&nbsp;<span style="font-size: 11px;">Cayó Julio César</span></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Desde la Edad Antigua, gracias a Platón y Aristóteles, entre otros pensadores, se ha discutido acerca del mejor modo de organizar políticamente una sociedad. Un debate central tiene que ver con la decisión de respaldar el Gobierno de las leyes o aquél sometido a cualquier antojo de los hombres. No es un tema menor, pues, gracias a la estructura que se levante, pueden multiplicarse los infortunios. Lo señalo porque, a partir del año 527 a. C., quien plantea la instauración de repúblicas tiene como ideal establecer un régimen en donde reinen las reglas jurídicas. Todos, tanto gobernantes como administrados, contarían con la obligación de cumplir las normas que sean adoptadas para regir nuestra coexistencia. Siguiendo esta lógica, ninguno estaría libre de ser castigado por incidir en su incumplimiento. Subrayo que, aun cuando parezca demasiado elemental, este principio continúa siendo difícil de respetar en numerosas sociedades. Estamos acostumbrados a percibir, con demasiada frecuencia, la vulneración de reglas que fueron creadas para defender nuestros derechos. No descubrimos nada nuevo si acusamos a los burócratas de estar a la vanguardia del mencionado despropósito.</p>
<p>&nbsp;</p>
<p>Además del imperio de la Ley, noción indispensable para combatir autocracias, el republicanismo conlleva un rechazo al poder que se origina en la sangre, tradición o fe. Por esta razón, salvo curiosas excepciones, sus partidarios se oponen a las monarquías. Siendo coherentes, deberían resistirse asimismo a los caudillismos de variada especie, puesto que se halla siempre una pretensión real en todos sus practicantes. Nadie está predestinado a ser obedecido por sus semejantes. El concepto de autoridad política está basado solamente en la voluntad de las personas, quienes plasman sus acuerdos en leyes que deben ser cumplidas. El gobernante no tendría sino la obligación de acatar lo dispuesto por los ciudadanos. Teniendo sus propias cualidades, privilegios y deberes, nada le permitiría estar por encima del orden vigente. También, es irrelevante que, para conquistar la gloria política, se aduzca una suerte de compensación por vejaciones del pasado o profecías ideológicas. Ninguno de los mesianismos que se han conocido puede armonizar con ese sistema político. El liderazgo que se funda en esas patrañas es una jefatura ideada para volvernos súbditos. No hay otro móvil que decretar nuestra sujeción al régimen, acabando con la igualdad conquistada en luchas de diferentes épocas.</p>
<p>&nbsp;</p>
<p>Naturalmente, siendo las reglas de convivencia tan importantes, debe demandarse que los ciudadanos tengan un gran compromiso en cuanto a la conducción del Estado. Es que, dentro de una república, los individuos son libres, pero tienen también el deber de contribuir a la vigencia del orden que hace posible el goce de sus derechos. No es casual que muchas guerras hubiesen tenido como meta salvar ese sistema. Necesitamos a sujetos que sigan la senda de Catón o, en los escenarios más obscuros, se comporten como Bruto y Casio. Ellos merecen el mayor de los desagravios, ya que, aunque previendo una condena histórica, optaron por rechazar las ambiciones del dictador. Las astucias de un demagogo debían ser segadas para impedir que la civilidad fuese destrozada. Sólo un genuino enemigo de la barbarie obra con esa valentía; huelga decir que su valor es inestimable para el avance del mundo. Ésa es la clase de actitudes que debe caracterizar a los republicanos. Mientras aumente su aversión a los gobernantes megalómanos, obsesionados por conseguir su endiosamiento, el orden puede considerarse protegido. Cuando no existen personas que repudian a los déspotas, el riesgo de su entronización es alto.</p>
<p>&nbsp;</p>
<p>Por último, para evitar tergiversaciones, es preciso apuntar que no se admite una relación indisoluble entre democracia y republicanismo. Por más cercanos que nos parezcan, son conceptos susceptibles de ocasionar disputas. Esta contingencia motiva el esclarecimiento del asunto que, durante los distintos tiempos, ha tenido más de un expositor. Por ejemplo, en una entrevista que brindó a la televisión francesa hace varias décadas, Hannah Arendt sostuvo que Estados Unidos no era una democracia, sino una república. Sus fundadores lo concibieron para protegerse de un mal indiscutible: la tiranía mayoritaria. Éste era el peligro que angustiaba a los forjadores de una sociedad tan valiosa como ésa. Ello implica que resulte inconcebible la existencia del aludido país, todavía ejemplar en diversas facetas, sin el predominio de los derechos individuales, pues, como ha indicado Ayn Rand, «la primera minoría en la Tierra es el individuo». Consiguientemente, una verdadera república defiende a los individuos de los abusos grupales, masivos, colectivos, aun cuando éstos puedan ser convalidados con legislaciones que contravengan los fundamentos en que se apoya el Estado. Al respecto, es útil resaltar que, una vez corrompida la esencia de una república, las normas aprobadas por sus gobernantes no justifican nuestra obediencia. El tirano es indigno del respeto.</p>
<p>&nbsp;</p>
<p>Fuente: Texto proporcionado por Enrique Fernández García (autor); originalmente publicado en el diario El Día (Santa Cruz, Bolivia)</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/275-uruguay-nuestro-propio-“premio-nobel-de-la-paz”-y-sus-amigos-de-las-farc">
			&laquo; Uruguay: Nuestro propio “Premio Nobel de la Paz” y sus amigos de las FARC		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/283-venezuela-llega-a-su-hora-decisiva">
			Venezuela llega a su hora decisiva &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/278-virtudes-republicanas-molestias-para-el-tirano#startOfPageId278">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:56:"Virtudes republicanas, molestias para el tirano - Relial";s:11:"description";s:160:"En la opinión de Enrique Fernández García &amp;nbsp; La república no es nada, sólo un nombre sin sustancia ni forma.&amp;nbsp;Cayó Julio César Desde l...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:47:"Virtudes republicanas, molestias para el tirano";s:6:"og:url";s:113:"http://www.relial.org/index.php/productos/archivo/opinion/item/278-virtudes-republicanas-molestias-para-el-tirano";s:8:"og:title";s:56:"Virtudes republicanas, molestias para el tirano - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/ab98897d60eaa0d05c1862c118538407_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/ab98897d60eaa0d05c1862c118538407_S.jpg";s:14:"og:description";s:168:"En la opinión de Enrique Fernández García &amp;amp;nbsp; La república no es nada, sólo un nombre sin sustancia ni forma.&amp;amp;nbsp;Cayó Julio César Desde l...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:47:"Virtudes republicanas, molestias para el tirano";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}