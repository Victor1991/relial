<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8827:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId274"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Los partidos se alteran, los sistemas se reemplazan
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/48689e827932dc70ec0a6e6067e8a72b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/48689e827932dc70ec0a6e6067e8a72b_XS.jpg" alt="Los partidos se alteran, los sistemas se reemplazan" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>En Costa Rica la segunda vuelta será entre dos variantes de la socialdemocracia. El profesor y diplomático Luis Guillermo Solís, a la cabeza del Partido de Acción Ciudadana (PAC), se enfrentará al ingeniero Johnny Araya, ex alcalde de San José, líder del Partido de Liberación Nacional (Liberación). El PAC es un desprendimiento de Liberación.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Solís parece ser un keynesiano –más Estado para solucionar los problemas del país-, mientras se supone que Araya sostiene una fórmula cercana al mercado. Cualquiera de los dos que gane respetará la ley. Lo que está en juego es la administración del gobierno y no el modelo político o sistema económico. En eso fue contundente la sociedad costarricense. Más del 80% rechazó decididamente al Frente Amplio, expresión local de la ruptura marxista con la democracia liberal.</p>
<p>&nbsp;</p>
<p>En El Salvador, en cambio, ocurrió algo muy diferente. Se disputarán el poder el maestro Salvador Sánchez Cerén, comunista y ex comandante de la guerrilla, quien casi obtuvo el 50% de los votos representando al FMLN, y el dentista Norman Quijano, anticomunista y candidato de la Alianza Republicana Nacionalista (ARENA). Los dos partidos fueron gestados durante la sangrienta etapa de la Guerra Fría.</p>
<p>&nbsp;</p>
<p>Pero hay algunas diferencias. Sánchez Cerén fue una figura destacada en el conflicto (lo acusan de ser el responsable directo o indirecto de cientos de asesinatos), mientras Quijano no empuñó las armas y se dedicó al deporte, al ejercicio de su profesión de dentista y, llegado el momento, a la política municipal.</p>
<p>&nbsp;</p>
<p>Sánchez Cerén aventajó a Quijano en diez puntos en la primera vuelta, pero hay dos circunstancias que mantienen viva la esperanza de ARENA: un tercer partido de derecha, el del ex presidente Tony Saca, obtuvo el 11% de los votos, mientras se abstuvo de sufragar el 48% de los electores. Quijano piensa que, si logra que los salvadoreños voten, puede ganarle al FMLN. En todo caso, es una tarea enormemente difícil, aunque no imposible.</p>
<p>&nbsp;</p>
<p>No obstante, las diferencias entre estas dos figuras son abismales. Si Quijano gana, intentará frenar la inmensa violencia de las maras, reducir la pobreza y aumentar sustancialmente las inversiones privadas para lograr más y mejores empleos, de manera que cientos de miles de salvadoreños pasen a engrosar las clases medias.</p>
<p>&nbsp;</p>
<p>En el trayecto, como sucede en los países más prósperos del planeta, numerosos empresarios se enriquecerán, pero a Quijano no le importa que haya más ricos. Él es un reformista que desea perfeccionar el sistema. Lo que quiere es que haya menos pobres.</p>
<p>&nbsp;</p>
<p>Si gana Sánchez Cerén la historia será otra. Actuará como un marxista convencido de la maldad intrínseca de un sistema de explotación basado en la propiedad privada, en el que los capitalistas se apropian de la plusvalía de los trabajadores, y optará por una economía planificada, dirigida por los bienintencionados burócratas de su cuerda política, en detrimento de un mercado que, según Marx y él, conduce al enriquecimiento de los poderosos y a la progresiva depauperación y alienación de los trabajadores. Ser rico es malo. La propiedad es un robo.</p>
<p>&nbsp;</p>
<p>Para lograr el reino de la justicia marxista, Sánchez Cerén, aunque le tome cierto tiempo, tendrá que recurrir a la violencia y a la dictadura del proletariado, algo que moralmente justifican todos los revolucionarios que en el mundo han sido. ¿Qué importan unas cuantas vidas sacrificadas cuando está en juego el destino glorioso de la humanidad? Pregúntenle a Stalin, a Mao, a Castro, a Pol Pot.</p>
<p>&nbsp;</p>
<p>¿Cómo lo hará? Seguirá los pasos del Socialismo del siglo XXI, como han hecho Venezuela, Nicaragua, Bolivia y Ecuador. Cambiará la constitución, prorrogará sine die el mandato presidencial, controlará todos los poderes y se hará cargo progresivamente del aparato productivo. El guión es muy conocido.</p>
<p>&nbsp;</p>
<p>Como postulan los comunistas serios, y Sánchez Cerén es uno de ellos, las revoluciones no se llevan a cabo para revocarlas luego en unas ridículas elecciones burguesas. ¿A quién se le puede ocurrir semejante estupidez?</p>
<p>&nbsp;</p>
<p>La alternancia en el poder es entre partidos de una misma familia política, no entre sistemas diferentes. Los partidos se alternan, los sistemas se reemplazan. Una sociedad no puede mudar de piel cada cinco años. El viejo símil es cierto: una pecera se puede convertir en una sopa de pescado. Una sopa de pescado no se puede convertir en una pecera.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: El blog de Montaner</p>
<p>Foto: El blog de Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/271-vargas-llosa-se-debe-defender-el-pluralismo-informativo">
			&laquo; Vargas Llosa: "Se debe defender el pluralismo informativo"		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/275-uruguay-nuestro-propio-“premio-nobel-de-la-paz”-y-sus-amigos-de-las-farc">
			Uruguay: Nuestro propio “Premio Nobel de la Paz” y sus amigos de las FARC &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/274-los-partidos-se-alteran-los-sistemas-se-reemplazan#startOfPageId274">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:60:"Los partidos se alteran, los sistemas se reemplazan - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; En Costa Rica la segunda vuelta será entre dos variantes de la socialdemocracia. El profesor y dip...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:51:"Los partidos se alteran, los sistemas se reemplazan";s:6:"og:url";s:117:"http://www.relial.org/index.php/productos/archivo/opinion/item/274-los-partidos-se-alteran-los-sistemas-se-reemplazan";s:8:"og:title";s:60:"Los partidos se alteran, los sistemas se reemplazan - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/48689e827932dc70ec0a6e6067e8a72b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/48689e827932dc70ec0a6e6067e8a72b_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; En Costa Rica la segunda vuelta será entre dos variantes de la socialdemocracia. El profesor y dip...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:51:"Los partidos se alteran, los sistemas se reemplazan";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}