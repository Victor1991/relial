<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8269:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId464"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Instituciones débiles, seguridad pública y crimen organizado: La Red Liberal de América Latina aprueba Declaración sobre Derechos Humanos en América Latina
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/6faf95a407946923d692f4ddda792716_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/6faf95a407946923d692f4ddda792716_XS.jpg" alt="Instituciones d&eacute;biles, seguridad p&uacute;blica y crimen organizado: La Red Liberal de Am&eacute;rica Latina aprueba Declaraci&oacute;n sobre Derechos Humanos en Am&eacute;rica Latina" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Birgit Lamm</p>
<p>&nbsp;</p>
<p>Del 14 al 17 de noviembre se reunieron en Panamá los miembros de la Red Liberal de América Latina, RELIAL, con el fin de celebrar su Congreso Anual. Las 37 organizaciones miembro volvieron la vista hacia los 10 años de existencia de RELIAL, pero el encuentro sirvió también a la discusión sobre temas actuales de la región.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Congreso RELIAL Juli MinovesAdemás del desarrollo económico de América Latina, sobre todo temas relacionados con los Derechos Humanos y Civiles, estuvieron en la agenda. Durante el primer día el orador principal fue Juli Minoves, Presidente de la Internacional Liberal, quien habló de la reunión de la Presidencia de la Internacional Liberal en Hong Kong y expresó su apoyo a la Revolución de los Paraguas -movimiento que representa a muchos ciudadanos alrededor del mundo que exigen sus derechos políticos frente a regímenes autoritarios.</p>
<p>&nbsp;</p>
<p>Congreso RELIAL Gesine MeissnerGesine Meissner, Eurodiputada del Grupo de la Alianza de los Demócratas y Liberales por Europa, realzó la importancia de los Derechos Humanos para una democracia funcional y se expresó preocupada por la masacre de estudiantes en el municipio mexicano de Iguala.</p>
<p>&nbsp;</p>
<p>Luis Moreno Ocampo, ex Fiscal en Jefe de la Corte Penal Internacional de la Haya, hizo una proyección a futuro. De acuerdo con Moreno Ocampo, el crimen organizado ya hace tiempo opera de manera globalizada, mientras que el combate a la delincuencia aún sigue organizándose a nivel nacional y en el mejor de los casos, a nivel regional (como es el caso de la Europol). Con ello, resulta imposible responder de manera apropiada a las actuales estructuras internacionales del crimen. Las respuestas a estos cuestionamientos del siglo 21 tendrían más bien que formularse de la misma manera global y creativa. También los aspectos relacionados con la criminalidad cotidiana, cada vez se resuelven menos mediante severas reglamentaciones estatales, menos aún en los países de América Latina, donde la ineficiencia policiaca y el problema de la impunidad demandan otras soluciones.</p>
<p>&nbsp;</p>
<p>Congreso RELIAL Luis Moreno OcampoDe acuerdo con Moreno Ocampo, esto afecta principalmente a los estratos sociales más pobres que no se encuentran en posibilidad de pagar costosos servicios de seguridad privada. Sin embargo, también estos grupos vulnerables en términos financieros, podrían protegerse de mejor manera empleando hábiles estrategias, como por ejemplo, a través de aplicaciones de teléfonos inteligentes, mediante las cuales, usuarios del transporte público pueden encontrarse en algún punto para esperar el autobús o tomar un taxi seguro por las noches en zonas inseguras. Los servicios de Taxi como Taxi Beat o Uber que funcionan mediante teléfonos inteligentes, ofrecen actualmente estándares de seguridad más elevados que los que ofrece el registro formal de los taxis, que es fácil de comprar o falsificar. El liberalismo del siglo 21 debe ofrecer y propagar de manera ofensiva soluciones nuevas, creativas y descentralizadas.</p>
<p>&nbsp;</p>
<p>Los reportes de actualidad sobre la situación en Brasil, Venezuela y Chile evidenciaron la difícil situación económica de los primeros. Los altos índices de criminalidad, corrupción e impunidad, aunado a las prácticas de gobiernos intervencionistas, generan fuertes motivos de preocupación.</p>
<p>&nbsp;</p>
<p>A continuación del discurso de Moreno Ocampo, los miembros de RELIAL deliberaron sobre un documento fundamental sobre Derechos Humanos y ciudadanos. El documento fue aprobado por unanimidad y servirá como fundamento para intensificar la labor de la red en el ámbito de los Derechos Humanos y Civiles.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Foto: Fundación Friedrich Naumann</p>
<p>Fuente:&nbsp;<a href="http://www.la.fnst.org">www.la.fnst.org</a></p>
<p>Autor: Birgit Lamm, Directora Regional para América Latina de la Fundación Friedrich Naumann para la Libertad</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/460-red-liberal-de-américa-latina-los-primeros-10-años">
			&laquo; Red Liberal de América Latina, los primeros 10 años		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/465-todos-con-maría-corina-machado">
			Todos con María Corina Machado. &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/464-instituciones-débiles-seguridad-pública-y-crimen-organizado-la-red-liberal-de-américa-latina-aprueba-declaración-sobre-derechos-humanos-en-américa-latina#startOfPageId464">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:169:"Instituciones débiles, seguridad pública y crimen organizado: La Red Liberal de América Latina aprueba Declaración sobre Derechos Humanos en América Latina - Relial";s:11:"description";s:156:"En la opinión de Birgit Lamm &amp;nbsp; Del 14 al 17 de noviembre se reunieron en Panamá los miembros de la Red Liberal de América Latina, RELIAL, con...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:160:"Instituciones débiles, seguridad pública y crimen organizado: La Red Liberal de América Latina aprueba Declaración sobre Derechos Humanos en América Latina";s:6:"og:url";s:225:"http://www.relial.org/index.php/productos/archivo/opinion/item/464-instituciones-débiles-seguridad-pública-y-crimen-organizado-la-red-liberal-de-américa-latina-aprueba-declaración-sobre-derechos-humanos-en-américa-latina";s:8:"og:title";s:169:"Instituciones débiles, seguridad pública y crimen organizado: La Red Liberal de América Latina aprueba Declaración sobre Derechos Humanos en América Latina - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/6faf95a407946923d692f4ddda792716_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/6faf95a407946923d692f4ddda792716_S.jpg";s:14:"og:description";s:160:"En la opinión de Birgit Lamm &amp;amp;nbsp; Del 14 al 17 de noviembre se reunieron en Panamá los miembros de la Red Liberal de América Latina, RELIAL, con...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:160:"Instituciones débiles, seguridad pública y crimen organizado: La Red Liberal de América Latina aprueba Declaración sobre Derechos Humanos en América Latina";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}