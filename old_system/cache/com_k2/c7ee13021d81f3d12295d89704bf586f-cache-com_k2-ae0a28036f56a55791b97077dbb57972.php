<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8625:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId113"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La puerta de los monstruos
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/85b62d4a27ea43297eb1ab349b6e06c6_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/85b62d4a27ea43297eb1ab349b6e06c6_XS.jpg" alt="La puerta de los monstruos" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La opinión de Carlos Alberto Montaner</p>
<p>Dice Nicolás Maduro que el presidente Juan Manuel Santos "le metió una puñalada a Venezuela". No se sabe si esta dramática información forense se la dio confidencialmente un pajarito o si surgió de su legendaria capacidad de observación.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Maduro ve cosas que nadie percibe. Es un vidente. Sólo él, por ejemplo, descubrió su rostro entre las manos de Chávez en un cuadro o foto del Comandante Eterno. Pero ahí no termina la crónica roja colombo-venezolana. De acuerdo con su versión, los colombianos, coludidos con la CIA, intentarían envenenarlo.</p>
<p>&nbsp;</p>
<p>Creo que es importante tener en cuenta la secuencia. Estas revelaciones completan el cuadro clínico. Primero se presentaron las alucinaciones auditivas con pajaritos que le hablaban. Luego comparecieron las alucinaciones visuales con su propia imagen. Ahora contemplamos denuncias de conspiraciones siniestras. Parece que estamos ante un típico caso de esquizofrenia paranoide.</p>
<p>&nbsp;</p>
<p>Los venezolanos, especialmente tras las revelaciones de Mario Silva, discuten si Maduro es un comunista manejado por Cuba o un místico manejado por Sai Baba, pero me sospecho que la duda que hay que despejar es si estamos ante un sujeto afectado por un brote psicótico transitorio, producto del estrés, tratable con unas cuantas pastillas de Risperidona, o si se trata de un esquizofrénico incurable de pronóstico sombrío. (Me temo lo segun</p>
<p>&nbsp;</p>
<p>En todo caso, la "puñalada" colombiana consiste en que el presidente Juan Manuel Santos recibió a Henrique Capriles, el jefe de la oposición venezolana y muy probable ganador real de las elecciones del 14 de abril.</p>
<p>&nbsp;</p>
<p>Santos, en realidad, no hizo nada excepcional. Recibió al representante de, por lo menos, la mitad de la sociedad venezolana. Eso era lo responsable. Las relaciones entre los países no son entre gobiernos, sino entre naciones. No haber recibido a Capriles, o sea, negarle la legitimidad que sus compatriotas le otorgaron en las urnas, sí era una forma de injerencia en los asuntos internos del vecino.</p>
<p>&nbsp;</p>
<p>Los gobiernos son sólo los representantes temporales de las naciones. Cuando Maduro sea amorosamente recluido en alguna institución psiquiátrica, como le ocurrió al presidente tunecino Habib Burguiba, y Capriles ocupe la presidencia, y cuando probablemente sea otro el inquilino del Palacio de Nariño en Bogotá, los vínculos entre las dos sociedades permanecerán inalterables.</p>
<p>&nbsp;</p>
<p>Pero si bien Juan Manuel Santos acertó en recibir a Capriles, tengo la impresión de que se equivoca en el tratamiento dado a los narcoguerrilleros comunistas de las FARC en las negociaciones llevadas a cabo en La Habana.</p>
<p>&nbsp;</p>
<p>De la misma manera que es razonable reconocer la legitimidad de Capriles para hablar en nombre de media Venezuela, no tiene sentido asignarles a losrepresentantes de las FARC el trato de interlocutor válido para discutir el destino político de Colombia, como si se tratara de la otra mitad legítima de la sociedad colombiana.</p>
<p>&nbsp;</p>
<p>No se puede admitir como parte de la discusión con las FARC una pretendida reforma agraria o los derechos de los trabajadores, como si la batalla planteada por el brazo armado del Partido Comunista colombiano se originara en reivindicaciones sociales, y no en la lucha por tomar el poder y establecer un régimen colectivista dictatorial basado en las supersticiones del marxismo-leninismo.</p>
<p>&nbsp;</p>
<p>Si no se ha podido someter militarmente a los criminales, es legítimo buscar el fin del conflicto armado por la vía de conversaciones que conduzcan a un armisticio, pero ello implica el fin de las hostilidades por parte de los subversivos, la entrega de las armas y la subordinación al imperio e la ley.</p>
<p>&nbsp;</p>
<p>También es razonable explorar zonas de perdón y reconciliación, como se ha hecho en docenas de sociedades martirizadas por conflictos sangrientos, pero ello exige el reconocimiento de la culpa y el arrepentimiento por parte de quienes han violado sistemáticamente las reglas, y hasta ahora ésa no parece ser la actitud de las FARC.</p>
<p>&nbsp;</p>
<p>Es probable que Juan Manuel Santos, lleno de buenas intenciones, quiera dejarles la paz a sus compatriotas como su gran legado histórico, pero de la manera en que lo está intentando hay un altísimo riesgo de que les transmita como herencia un Estado institucionalmente muy frágil y políticamente indefenso.</p>
<p>&nbsp;</p>
<p>O sea, la puerta por donde luego se cuelan los monstruos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: Colaboración del autor, Carlos Alberto Montaner, miembro de la Junta Honorífica de RELIAL&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/104-libre-cambio-y-sentido-común">
			&laquo; Libre cambio y sentido común		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/119-si-la-libertad-de-prensa-no-existiese-habría-que-inventarla">
			Si la libertad de prensa no existiese, habría que inventarla &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/113-la-puerta-de-los-monstruos#startOfPageId113">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:35:"La puerta de los monstruos - Relial";s:11:"description";s:157:"La opinión de Carlos Alberto Montaner Dice Nicolás Maduro que el presidente Juan Manuel Santos &quot;le metió una puñalada a Venezuela&quot;. No se sa...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:26:"La puerta de los monstruos";s:6:"og:url";s:93:"http://www.relial.org/index.php/productos/archivo/opinion/item/113-la-puerta-de-los-monstruos";s:8:"og:title";s:35:"La puerta de los monstruos - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/85b62d4a27ea43297eb1ab349b6e06c6_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/85b62d4a27ea43297eb1ab349b6e06c6_S.jpg";s:14:"og:description";s:165:"La opinión de Carlos Alberto Montaner Dice Nicolás Maduro que el presidente Juan Manuel Santos &amp;quot;le metió una puñalada a Venezuela&amp;quot;. No se sa...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:26:"La puerta de los monstruos";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}