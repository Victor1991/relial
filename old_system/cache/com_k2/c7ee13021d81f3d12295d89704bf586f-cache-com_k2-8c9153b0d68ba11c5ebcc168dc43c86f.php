<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8885:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId474"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Maduro huye hacia delante
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/5ab529a5ebdeee2caf2312423d91adb3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/5ab529a5ebdeee2caf2312423d91adb3_XS.jpg" alt="Maduro huye hacia delante" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Maduro anunció su nueva estrategia para enfrentarse a la catástrofe venezolana. Insiste en los errores de siempre. No va a rectificar. Mintió. Inventó culpables y conspiraciones. Optó por huir hacia delante. Lo hizo tras un inútil recorrido en busca de recursos por varios países, incluida China. Apenas consiguió unos pocos créditos y la vaga promesa de ciertas inversiones. Ya no le creen. Incluso, los que tienen ciertas simpatías ideológicas tampoco le creen. Por eso le han cerrado el grifo.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Hacen bien en no confiar en el chavismo. Nadie ignora que esta patulea de incapaces, además de maltratar severamente a la población, y de convertir al país en un narcoestado terriblemente corrupto –el más podrido de América Latina de acuerdo con Transparencia Internacional--, ha malgastado miles de millones de petrodólares. ¿Cuántos? Para que el azorado lector se haga una idea: la cifra es mayor que la suma de todos los ingresos recibidos por el Estado venezolano desde que Simón Bolívar consiguió la independencia en el primer cuarto del siglo XIX.</p>
<p>&nbsp;</p>
<p>Si los chavistas hubieran sabido y querido gobernar razonablemente, tras una década del barril de petróleo a cien dólares, Venezuela hoy sería un país del primer mundo y no una sociedad en plena descomposición, donde las personas se pelean a puñetazos en los supermercados y las farmacias por adquirir un poco de leche o una ampolleta de insulina.</p>
<p>&nbsp;</p>
<p>¿Cómo llegaron a este desastre? Tomen nota los españoles: además del catastrófico padrinazgo cubano, siguieron de cerca los consejos de los profesores comunistas Pablo Iglesias y Juan Carlos Monedero, hoy en Madrid al frente del partido Podemos. Estos personajes llegaron a tener despacho en Miraflores, la casa de gobierno en Venezuela, desde donde pontificaban y recetaban a sus anchas.</p>
<p>&nbsp;</p>
<p>Durante más de seis años, y al costo de varios millones de dólares que recibieron por sus asesorías, los jóvenes "expertos" académicos españoles enseñaron a los chavistas a demoler sin compasión la economía de la nación más rica de América Latina.</p>
<p>&nbsp;</p>
<p>Arribaron a Caracas borrachos de populismo marxista, sin la menor experiencia empresarial –lo que se traduce en que ignoran cómo se crea, conserva o malgasta la riqueza--, convencidos de que la principal tarea de los gobiernos es igualar a las personas por abajo. Objetivo, por cierto, que lograron con creces. Hoy el país es una inmensa pocilga colectiva.</p>
<p>&nbsp;</p>
<p>¿Y ahora qué va a pasar en Venezuela? Un experto en seguridad lo ha vaticinado en un tono sombrío: el chavismo –me ha dicho-- no marcha hacia una revolución o contrarrevolución política, sino hacia un saqueo nacional, monstruoso y definitivo, que llegará a los hoteles y a las casas suntuosas, donde quiera que haya comida.</p>
<p>&nbsp;</p>
<p>Venezuela va hacia el caos, regido por la ley del más fuerte, con cien mil Kalashnikovs, pistolas y cuchillos empuñados por la gente de rompe y rasga. Esos mismos que en el 2014 asesinaron a 25000 personas para despojarlas de los teléfonos móviles, las billeteras y los anillos, ahora acompañados por una enorme turba que se robará televisores, enseres domésticos y todo lo que encuentre a su paso.</p>
<p>&nbsp;</p>
<p>¿Por qué no? Eso fue lo que aprendieron de Hugo Chávez en aquellos paseos televisados en los que el difunto militar repetía alegremente el fatídico "exprópiese" ante cualquier bien que le llamara la atención, mientras sus cómplices, vestidos de rojo, reían y aplaudían irresponsablemente. El teniente coronel les enseñó que en la contemporánea selva urbana no existen los derechos de propiedad. Sencillamente, el dueño es el que tiene la pistola en la mano y está dispuesto a utilizarla. Menudo legado.</p>
<p>&nbsp;</p>
<p>Por supuesto, Maduro todavía tendría la posibilidad de impedir este horror. ¿Cómo? Rectificando. Debería comenzar por abrir los calabozos y liberar a los presos políticos, al tiempo que convoca a un urgente diálogo nacional con la oposición –que hoy tiene el 75% de respaldo popular— para darle un vuelco a la situación mediante una inmediata reforma consensuada.</p>
<p>&nbsp;</p>
<p>¿Por qué no lo hace? Probablemente, se lo impiden los narcogenerales que temen por su bolsa y por su vida, la legión de los corruptos que prefiere continuar esquilmando al país, y sus mentores cubanos, que anualmente reciben miles de millones de dólares en subsidios y están dispuestos a pelear hasta el último venezolano por mantener ese vital flujo de recursos.</p>
<p>&nbsp;</p>
<p>Atrapado en medio de esas fuerzas, Nicolás Maduro marcha a paso firme hacia el precipicio.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner; periodista y escritor. Su último libro es la novela Tiempo de Canallas.</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: El blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/469-escribo-tu-nombre-libertad-el-atentado-de-parís-según-el-liberalismo">
			&laquo; Escribo tu nombre, libertad: El atentado de París según el liberalismo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/479-el-récord-de-probreza-en-venezuela">
			El récord de probreza en Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/474-maduro-huye-hacia-delante#startOfPageId474">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:34:"Maduro huye hacia delante - Relial";s:11:"description";s:156:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Maduro anunció su nueva estrategia para enfrentarse a la catástrofe venezolana. Insiste en los err...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:25:"Maduro huye hacia delante";s:6:"og:url";s:92:"http://www.relial.org/index.php/productos/archivo/opinion/item/474-maduro-huye-hacia-delante";s:8:"og:title";s:34:"Maduro huye hacia delante - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/5ab529a5ebdeee2caf2312423d91adb3_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/5ab529a5ebdeee2caf2312423d91adb3_S.jpg";s:14:"og:description";s:160:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Maduro anunció su nueva estrategia para enfrentarse a la catástrofe venezolana. Insiste en los err...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:25:"Maduro huye hacia delante";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}