<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10723:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId335"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Cien años de un escritor indócil
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/e9432fccf28a953514f077b86e5e657a_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/e9432fccf28a953514f077b86e5e657a_XS.jpg" alt="Cien a&ntilde;os de un escritor ind&oacute;cil" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Sobre Octavio Paz, un texto de Enrique Fernández García*</p>
<p>Ahora en su centenario</p>
<p>&nbsp;</p>
<p>Eso, que fue criticado por George Orwell en Homenaje a Cataluña, generó animosidad en Octavio Paz. Los años le serían útiles para consolidar su repulsión a la dictadura soviética. Por cierto, él fue uno de los escritores que, sin cobardía, censuró las prácticas totalitarias del comunismo en Rusia</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><em>Pero la persona también fue importante, porque es difícil siempre ser fiel a uno mismo durante toda una vida, sin dejarse domesticar por nada ni por nadie, tratando de mantener incólume la independencia y de manifestar libremente la rebeldía cuando se terciara.</em></p>
<p><em>Juan Gallardo Muñoz</em></p>
<p><em><br /></em></p>
<p>Aunque ningún individuo esté condenado a transitar el camino de sus antecesores, pues es factible crear una ruta singular, incluso contradictora de la familia, hay fenómenos que nos afectan con intensidad. Un comportamiento, al igual que una o más ideas, puede resultar decisivo cuando se forja el carácter. En el caso de Octavio Paz Lozano, poeta, ensayista, dramaturgo, periodista, profesor, diplomático y filósofo, la combinación de las letras con el compromiso político obedece a una herencia que merece admiración. Ireneo Paz, su abuelo, fue un escritor que no tuvo problema en recurrir a las armas para salvaguardar sus convicciones liberales. Su ejercicio del periodismo reflejó el rechazo a las infamias de quienes gobernaban México. Hizo lo posible por contribuir a que terminaran las arbitrariedades de Benito Juárez. Por otro lado, Octavio Paz Solórzano, el padre de nuestro intelectual, sirvió con su pluma e ingenio a Emiliano Zapata. Se trató de una persona que, francamente, creía en las reformas planteadas por ese revolucionario. Con seguridad, pese a sus diferencias ideológicas, ambos ascendientes mostraron actitudes que serían adoptadas por el autor de Las peras del olmo, permitiendo su distinción del resto.</p>
<p>&nbsp;</p>
<p>Nacido en un año nada sereno, 1914, Paz nunca se sintió cómodo con el sometimiento. Su rebeldía se notaría pronto, en la escuela, cuando, junto a José Bosch, organizó una sublevación de naturaleza estudiantil. Tenía quince años; no obstante, gracias a ese compañero de aventura, contaba entonces con lecturas que versaban acerca del anarquismo. En cuanto al pensamiento político, cabe aclarar que, como sucedió con muchas personas, fue seducido por el socialismo. Eran posturas razonables en un ambiente que no admitía otras alternativas para protestar contra las miserias de la época. Se confiaba en la izquierda para extinguir las injusticias, engendrar un mundo que no consienta ofensas. Su convicción era tan fuerte que, en ocasión de la Guerra Civil de España, escribió un maravilloso poema, «¡No pasarán!», exteriorizando gran indignación. El año 1937, aprovechando su estadía por un encuentro literario y político en Valencia, intentó dejar los papeles para defender la República. Era un dictado de sus creencias; permanecer al margen del conflicto le parecía inadmisible. Por suerte, para la literatura, se lo persuadió de aportar a esa causa en otros terrenos. Actuó con ese afán, dedicando varias páginas a la defensa del régimen que consideraba legítimo. Al final, sus instigaciones no sirvieron para impedir la victoria del franquismo.</p>
<p>&nbsp;</p>
<p>Durante su estancia en la conflictiva España, nuestro literato se percató de lo perjudicial que ya era el estalinismo. Aun cuando el bando de los republicanos estaba compuesto por izquierdistas, los partidarios del camarada rojo imponían sus normas e infligían castigos. No querían que nadie tuviera independencia de pensar o actuar; sin salvedades, todos debían someterse a sus determinaciones. Eso, que fue criticado por George Orwell en Homenaje a Cataluña, generó animosidad en Octavio Paz. Los años le serían útiles para consolidar su repulsión a la dictadura soviética. Por cierto, él fue uno de los escritores que, sin cobardía, censuró las prácticas totalitarias del comunismo en Rusia. Son cuantiosas las hojas que consagró a la exposición de tales abominaciones. Jamás le interesó complacer a los otros socialistas que, como sea, evitaban cualquier autocrítica para no facilitar las refutaciones del capitalismo. Tenía mayor importancia preocuparse por los individuos que, bajo la excusa de construir el paraíso igualitario, eran constreñidos a ser siervos. Esto no significaba relegar el marxismo, que hallaba todavía rescatable; sin embargo, la fascinación por la ideología justificaba su moderación. Los desastres del colectivismo, así como sus carnicerías, le harían pensar luego que el liberalismo democrático era el mejor modo civilizado de convivencia.</p>
<p>&nbsp;</p>
<p>La justicia fue un valor importante para el nobel de Literatura del año 1990. Las desigualdades lacerantes entre los miembros de una sociedad no le parecían aceptables. El legado de su estirpe lo llevaba también a luchar por los sujetos menos favorecidos. Mas esta misión no la cumplió como un vulgar y descerebrado militante de un partido. Las doctrinas pueden orientarnos en la busca de mejores situaciones; empero, es menester oponerse a su endiosamiento. Esto lo diferenció de muchos intelectuales que, en lugar de pensar con libertad, prefirieron el fanatismo, la radicalidad absurda, los sectarismos. Su relación fue particularmente difícil, hasta feroz, con los apóstoles de la izquierda latinoamericana, esa gente que, desde hace tiempo, repite las mismas sandeces. Les exigía que reflexionaran con seriedad, realizaran propuestas capaces de ayudar al saneamiento del Estado y la sociedad. Era en vano. Como pasó con Neruda, trovador de Stalin, la tozudez no cedía espacio para reconsiderar posiciones ideológicas. Su catecismo no sufrirá cambios ni cuando caigan nuevos muros. Las críticas que le hicieron no pusieron en duda su lucidez; tampoco, por supuesto, socavaron la credibilidad moral de Paz. Es deseable que la obra de un hombre soberano y genial como él nos provoque siempre. Para molestia de sus atacantes, espero que así sea.</p>
<p>&nbsp;</p>
<p>*Escritor, filósofo y abogado,</p>
<p><span id="cloak98034">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak98034').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy98034 = 'c&#97;&#105;d&#111;d&#101;lt&#105;&#101;mp&#111;' + '&#64;';
 addy98034 = addy98034 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak98034').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy98034 + '\'>' + addy98034+'<\/a>';
 //-->
 </script></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/313-¿es-la-desigualdad-un-problema?">
			&laquo; ¿Es la desigualdad un problema?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina">
			La verdad de Hilda Molina &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/335-cien-años-de-un-escritor-indócil#startOfPageId335">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:43:"Cien años de un escritor indócil - Relial";s:11:"description";s:155:"Sobre Octavio Paz, un texto de Enrique Fernández García* Ahora en su centenario &amp;nbsp; Eso, que fue criticado por George Orwell en Homenaje a Cata...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:34:"Cien años de un escritor indócil";s:6:"og:url";s:105:"http://www.relial.org/index.php/productos/archivo/opinion/item/335-cien-aÃ±os-de-un-escritor-indÃ³cil";s:8:"og:title";s:43:"Cien años de un escritor indócil - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/e9432fccf28a953514f077b86e5e657a_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/e9432fccf28a953514f077b86e5e657a_S.jpg";s:14:"og:description";s:159:"Sobre Octavio Paz, un texto de Enrique Fernández García* Ahora en su centenario &amp;amp;nbsp; Eso, que fue criticado por George Orwell en Homenaje a Cata...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:34:"Cien años de un escritor indócil";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}