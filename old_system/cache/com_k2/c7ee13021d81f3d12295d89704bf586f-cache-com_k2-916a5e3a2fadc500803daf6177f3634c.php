<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8869:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId350"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El secreto de los estados totalitarios
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f9a9b7c9f33a923e5475b478a62125ae_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f9a9b7c9f33a923e5475b478a62125ae_XS.jpg" alt="El secreto de los estados totalitarios" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>¿Cuál es la pieza clave en la construcción de la jaula totalitaria? Sencillo: la eliminación real de la separación de poderes, aunque se mantenga la fantasía formal de que continúa existiendo.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Lo explico.</p>
<p>&nbsp;</p>
<p>Max Weber describió el fenómeno y acuñó la frase "monopolio de la violencia". Lo hizo en La política como vocación. Era la facultad que tenían los Estados para castigar. Sólo a ellos les correspondía la responsabilidad de multar, encarcelar, maltratar y hasta matar a quienes violaban las reglas.</p>
<p>&nbsp;</p>
<p>Podían, eso sí, delegar esa facultad, pero sin renunciar a ella. Permitir mafias y bandas paramilitares que actúan al margen de la ley descalificaba totalmente al Estado. Era una disfuncionalidad que lo convertía en una entidad totalmente fallida, en la medida en que abdicaba de una de sus responsabilidades esenciales.</p>
<p>&nbsp;</p>
<p>No obstante, el Estado, si se acomodaba al diseño republicano, incluso si se trataba de una monarquía constitucional, no podía recurrir a los castigos sin que lo decidiera una corte independiente. Este tribunal, a su vez, debía interpretar una ley previa, y sancionar de acuerdo con un código penal igualmente aprobado por un parlamento independiente.</p>
<p>&nbsp;</p>
<p>El Barón de Montesquieu, lector de John Locke, lo había propuesto en 1748 en el Espíritu de las leyes: el Estado debía fragmentar la autoridad en tres poderes independientes y de rango similar para evitar la tiranía. Las monarquías absolutistas reunían en el soberano esas tres facultades y eso, precisamente, las hacía repugnantemente autoritarias.</p>
<p>&nbsp;</p>
<p>Si quien castigaba se arrogaba las facultades de hacer las reglas y de aplicarlas, la sociedad, carente de protección, se convertía en rehén de sus caprichos. Los gobernantes podían hacer de ella y con ella lo que les daba la gana.</p>
<p>&nbsp;</p>
<p>Ese elemento –la separación de poderes— era la médula de las repúblicas creadas los siglos XVIII y XIX tras las revoluciones norteamericana, francesa y, por supuesto, latinoamericanas. De alguna manera, era la garantía de la libertad.</p>
<p>&nbsp;</p>
<p>Este preámbulo viene a cuento del bochornoso espectáculo de la Venezuela de Nicolás Maduro, donde los paramilitares en sus motos, amparados por la complicidad del gobierno, asesinan impunemente a los manifestantes que ejercen su derecho constitucional a manifestarse pacíficamente.</p>
<p>&nbsp;</p>
<p>Viene a cuento de un parlamento convertido en un coso taurino en el que se lidia a la oposición, se le clavan banderillas, se golpea a los diputados que protestan, o los expulsan arbitrariamente, como hicieron con María Corina Machado, y se dictan medidas ajustadas a las necesidades represivas de la oligarquía socialista que gobierna.</p>
<p>&nbsp;</p>
<p>Si Maduro necesita eliminar las manifestaciones de los estudiantes o encerrar a los alcaldes que protestan, o a los líderes a los que teme, como a Leopoldo López, solicita las normas, hechas a la medida por tribunales o por parlamentarios obsecuentes, y da la orden a los cuerpos represivos para que actúen.</p>
<p>&nbsp;</p>
<p>Viene a cuento de unos tribunales que sentencian con arreglo a la voluntad del Poder Ejecutivo, porque la ley ha dejado de ser una norma neutral para convertirse en un instrumento al servicio de la camarilla gobernante, empeñada en arrastrar por la fuerza a los venezolanos hacia "el mar de la felicidad" cubano.</p>
<p>&nbsp;</p>
<p>Un país, Cuba, donde, como en cualquier dictadura totalitaria, sencillamente no creen en las virtudes de la separación de poderes y repiten, con Marx y con Lenin, que ésa es una zarandaja de las sociedades capitalistas para mantener los privilegios de la clase dominante.</p>
<p>&nbsp;</p>
<p>Esta falsificación de las ideas republicanas –las de Bolívar y Martí, las de Juárez— van gestando una nueva facultad propia de este tipo de Estado: desarrollan el monopolio de la intimidación. Gobiernan mediante el miedo. Ese es el elemento que uniforma a la sociedad y la convierte en un coro amaestrado.</p>
<p>&nbsp;</p>
<p>Como quienes mandan hacen las leyes y juzgan e imponen los castigos, acaban por generar un terror insuperable entre los ciudadanos e inducen en ellos una actitud de sumisa obediencia que suelen transmitirles a los hijos "para que no se metan en problemas".</p>
<p>&nbsp;</p>
<p>La víctima termina por colaborador con su verdugo. Ése exactamente es el objetivo. Una vez que las tuercas han sido convenientemente apretadas y la jaula perfeccionada, el común de la gente, con la excepción de un puñado de rebeldes, aplaude y baja la cabeza.</p>
<p>&nbsp;</p>
<p>En ese punto ya no existen vestigios de la separación de poderes.</p>
<p>&nbsp;</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: El blog de Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú">
			&laquo; Unión civil y libertad en el Perú		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/354-chile-¿para-qué-imitar-a-venezuela-cuando-se-puede-emular-a-suiza?">
			Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza? &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios#startOfPageId350">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"El secreto de los estados totalitarios - Relial";s:11:"description";s:157:"En la opinión de Carlos Alberto Montaner &amp;nbsp; ¿Cuál es la pieza clave en la construcción de la jaula totalitaria? Sencillo: la eliminación real...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"El secreto de los estados totalitarios";s:6:"og:url";s:105:"http://www.relial.org/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios";s:8:"og:title";s:47:"El secreto de los estados totalitarios - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f9a9b7c9f33a923e5475b478a62125ae_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f9a9b7c9f33a923e5475b478a62125ae_S.jpg";s:14:"og:description";s:161:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; ¿Cuál es la pieza clave en la construcción de la jaula totalitaria? Sencillo: la eliminación real...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:38:"El secreto de los estados totalitarios";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}