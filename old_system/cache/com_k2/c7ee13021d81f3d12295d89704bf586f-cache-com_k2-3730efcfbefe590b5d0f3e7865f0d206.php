<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7622:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId247"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Argentina, entre los países con menor libertad económica
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/b320775de3c297425b69dccc362220a9_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/b320775de3c297425b69dccc362220a9_XS.jpg" alt="Argentina, entre los pa&iacute;ses con menor libertad econ&oacute;mica" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La pérdida de libertad económica que sufrió el país en la última década fue objeto de análisis de un grupo de ONG´s internacionales y locales, que expusieron sus perspectivas, ayer, en la universidad del CEMA. Las razones que llevaron a esta situación fueron el eje central de todas las exposiciones. El seminario fue coordinado por las ONGs internacionales, Fraser Institute, Atlas y Friedrich Naumann, y por las locales, Libertad y Progreso (LyP), Libertad (de Rosario) y Carlos Pellegrini.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>Según el Índice de Libertad Económica 2013 del Fraser Institute de Canadá, la Argentina está en el puesto 137 de 152. Es decir, está entre los 15 países con menor libertad económica, con un puntaje de 5,69 (10 es el valor más alto). Mientras que nuestros vecinos como Chile, Perú y Costa Rica están en el puesto 11, 22 y 23, respectivamente. Brasil y Colombia están lejos de los primeros puestos pero mejor posicionados que la Argentina, 102 y 96, respectivamente. Venezuela es el último país del ranking.</p>
<p>&nbsp;</p>
<p>Este índice se elabora en base a los resultados de cinco indicadores 1) Tamaño del gobierno; 2) Sistema legal y propiedad privada; 3) Moneda sana; 4) Libertad para comerciar internacionalmente y 5) Regulación.</p>
<p>&nbsp;</p>
<p>En la categoría Sistema Legal y respeto por la propiedad privada, la Argentina tuvo el peor desempeño. Los ataques a la independencia de la justicia por parte del Poder Ejecutivo y el Poder Legislativo, así como las expropiaciones de empresas de capital privado sin indemnización previa, fueron hechos determinantes en los resultados.</p>
<p>&nbsp;</p>
<p>Así afirma el informe elaborado conjuntamente por LyP y Atlas para explicar la caída de la Argentina en materia de libertad económica, a propósito de los resultados del índice del Fraser Institute. El autor del estudio fue Martín Lagos, consejero académico de LyP y ex vicepresidente del BCRA, que también fue uno de los expositores del seminario.</p>
<p>&nbsp;</p>
<p>Exceso de regulación y corrupción</p>
<p>&nbsp;</p>
<p>Otro de los indicadores en los que la Argentina se destacó por su mal desempeño fue el de "regulación". En esta categoría el país obtuvo 5,6 sobre 10. "Este indicador explica el grado de libertad que hay para abrir y cerrar un negocio, para tomar un crédito, contratar o despedir un empleado, entre otras cuestiones", explicó Fred McMahon, uno de los autores del Índice de Libertad Económica del Fraser Institute, y otro de los expositores en el seminario.</p>
<p>&nbsp;</p>
<p>"El exceso de regulaciones es la materia prima para la corrupción", señaló, y aconsejó mirar modelos exitosos de países que sacaron ventajas de sus crisis económicas y que supieron abrirse a los mercados, crecer y desarrollarse.</p>
<p>&nbsp;</p>
<p>Por otro lado, el elevado nivel de inflación que sufre el país tiene un impacto directo en la categoría "moneda sana" del Índice de Libertad Económica.</p>
<p>&nbsp;</p>
<p>Mientras que los resultados en "tamaño del gobierno" estuvieron condicionados por el importante incremento del Gasto Público Consolidado (o sea, el ejecutado por el gobierno nacional, los gobiernos provinciales y municipales) que, de 2003 a 2012, pasó de 29,4 % a 45,9% del Producto Interno Bruto.</p>
<p>&nbsp;</p>
<p>Entre los disertantes, también estuvieron Gerardo Bongiovanni de la fundación Libertad y Ernesto Badaraco, de Carlos Pellegrini.</p>
<p>&nbsp;</p>
<p>fuente: <a href="http://www.libertadyprogresonline.org/2013/12/05/argentina-entre-los-15-paises-con-menor-libertad-economica/">Libertad y Progreso</a></p>
<p>foto: Libertad y Progreso</p>
<p>autor: Libertad y Progreso&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia">
			&laquo; Un peligroso espiral de violencia		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/252-its-snowing-in-mexico-think-tanks-promoting-a-free-economy">
			It's Snowing In Mexico: Think Tanks Promoting A Free Economy &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica#startOfPageId247">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:67:"Argentina, entre los países con menor libertad económica - Relial";s:11:"description";s:161:"La pérdida de libertad económica que sufrió el país en la última década fue objeto de análisis de un grupo de ONG´s internacionales y locales, que exp...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:58:"Argentina, entre los países con menor libertad económica";s:6:"og:url";s:127:"http://www.relial.org/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica";s:8:"og:title";s:67:"Argentina, entre los países con menor libertad económica - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/b320775de3c297425b69dccc362220a9_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/b320775de3c297425b69dccc362220a9_S.jpg";s:14:"og:description";s:161:"La pérdida de libertad económica que sufrió el país en la última década fue objeto de análisis de un grupo de ONG´s internacionales y locales, que exp...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:58:"Argentina, entre los países con menor libertad económica";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}