<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12501:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/60-¿la-derrota-del-liberalismo-en-venezuela?">
	  		¿La derrota del Liberalismo en Venezuela?	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Presentación de Rocío Guijarro en el foro "Los Avances y El Futuro de la libertad en América Latina" Organizado por la Fundación Libertad el 21 de febrero de 2013.&nbsp;Rocío Guijarro es gerente del Centro de Divulgación del Conocimiento Económico Para La Libertad "CEDICE Libertad", instancia de parte del Comité Ejecutivo de la Fundación Internacional Para La Libertad (FIL), Miembro del Patronato de la Fundación Iberoamérica - Europa (FIE), Miembro de la Junta Directiva de la Red Liberal de América Latina RELIAL, Miembro del Comité de Redacción de la revista Perspectiva del Instituto de Ciencia Política, Colombia y de la Revista Procesos de Mercado de Madrid, Miembro del Consejo de Redacción de Estrategia y Negocios del Diario El Nacional, Venezuela.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_9ed89410_1624067368" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/OgxDz3JIxH4?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/60-¿la-derrota-del-liberalismo-en-venezuela?">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/59-el-rumbo-hacia-una-américa-latina-libre-y-próspera-foro-de-caminos-de-la-libertad">
	  		El rumbo hacia una América Latina Libre y Próspera - Foro de Caminos de la Libertad	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Foro organizado por Caminos de La Libertad.</p>
<p>Martes 12 de febrero 2013, Casa Lamm. México D.F</p>
<p>Participaron: Axel Kaiser,, Roberto Salinas, Sergio Sarmiento, Luiz Ferezin e Ian Vázquez y&nbsp;Ricardo López Murphy, Presidente de la Red Liberal de América Latina RELIAL</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_bd9f2548_688293535" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/8fQTDet44XU?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/59-el-rumbo-hacia-una-américa-latina-libre-y-próspera-foro-de-caminos-de-la-libertad">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/58-situación-en-venezuela">
	  		Situación en Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Relato de María Corina Machado sobre el sistema electoral en Venezuela</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_7052c31b_889701340" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/nEoTQlUCEc4?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/58-situación-en-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/55-venezuela-video-censurado-de-la-campaña-de-cedice-libertad-de-elegir">
	  		Venezuela: video censurado de la campaña de CEDICE &#039;Libertad de Elegir&#039;	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/multimedia/videos/item/55-venezuela-video-censurado-de-la-campaña-de-cedice-libertad-de-elegir" title="Venezuela: video censurado de la campa&ntilde;a de CEDICE &amp;#039;Libertad de Elegir&amp;#039;">
		    	<img src="/media/k2/items/cache/220c08548cac211cc7db219bb52f46cf_XS.jpg" alt="Venezuela: video censurado de la campa&ntilde;a de CEDICE &amp;#039;Libertad de Elegir&amp;#039;" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Video Censurado por Procedimiento Administrativo de CONATEL</p>
<p>&nbsp;</p>
<p>Cedice ha sido notificado de un procedimiento administrativo que Conatel ha abierto a los medios de comunicación que han transmitido nuestra campaña institucional en Defensa del Derecho a la Propiedad Privada garantizado en la Constitución vigente.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_fd08c785_1703700102" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/kfbnkGWmpMY?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/55-venezuela-video-censurado-de-la-campaña-de-cedice-libertad-de-elegir">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/multimedia/videos?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/multimedia/videos?start=28" class="pagenav">Anterior</a></li><li><a title="2" href="/index.php/multimedia/videos?start=4" class="pagenav">2</a></li><li><a title="3" href="/index.php/multimedia/videos?start=8" class="pagenav">3</a></li><li><a title="4" href="/index.php/multimedia/videos?start=12" class="pagenav">4</a></li><li><a title="5" href="/index.php/multimedia/videos?start=16" class="pagenav">5</a></li><li><a title="6" href="/index.php/multimedia/videos?start=20" class="pagenav">6</a></li><li><a title="7" href="/index.php/multimedia/videos?start=24" class="pagenav">7</a></li><li><a title="8" href="/index.php/multimedia/videos?start=28" class="pagenav">8</a></li><li><span class="pagenav">9</span></li><li><a title="10" href="/index.php/multimedia/videos?start=36" class="pagenav">10</a></li><li><a title="11" href="/index.php/multimedia/videos?start=40" class="pagenav">11</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/multimedia/videos?start=36" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/multimedia/videos?start=40" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 9 de 11	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:15:"Videos - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:54:"http://relial.org/index.php/multimedia/videos?start=32";s:8:"og:title";s:15:"Videos - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:3:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:72:"/plugins/content/jw_allvideos/jw_allvideos/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:11:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:78:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/mediaplayer/jwplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:79:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/silverlight.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:77:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/wmvplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:86:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/quicktimeplayer/AC_QuickTime.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Multimedia";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:6:"Videos";s:4:"link";s:20:"index.php?Itemid=137";}}s:6:"module";a:0:{}}