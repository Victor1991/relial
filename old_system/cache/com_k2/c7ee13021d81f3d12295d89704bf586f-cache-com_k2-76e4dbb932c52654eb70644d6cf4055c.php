<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8457:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId336"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La verdad de Hilda Molina
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/ee760d1c1474fde7f8f71a96b2452e29_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/ee760d1c1474fde7f8f71a96b2452e29_XS.jpg" alt="La verdad de Hilda Molina" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Por Héctor Ñaupari*</p>
<p>&nbsp;</p>
<p>Hace unos días presentamos en Lima el libro Mi verdad de la doctora Hilda Molina, ante una multitudinaria audiencia, junto a los librepensadores Álvaro Vargas Llosa y Carlos Alberto Montaner, dilectos amigos, en una magnífica cobertura organizada por la notable Yesenia Álvarez y su Instituto Político para la Libertad.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El devoto público de esa noche se dispuso atentamente a escuchar la muy conmovedora historia de esta neurocirujana nacida en Cuba, quien decidió, luego de un largo proceso de convencimiento, no exento de cuestionamientos y apelaciones, por voluntad propia y en el otoño de su vida, que en su país debía existir libertad, igualdad, oportunidades y bienestar. Con su renuncia al proyecto revolucionario –que la despojó de sus títulos y su condición de parlamentaria, entre otros castigos– buscaba que sus compatriotas –"aún un poco más fugitivos que nosotros" como en la Quinta elegía de Rilke– puedan comer, tener un techo y medicinas, caminar por las calles y plazas de su centro histórico, leer periódicos, ser atendidos por sus médicos, viajar y expresar una opinión propia.</p>
<p>&nbsp;</p>
<p>La valiente renuncia de la doctora Molina, "hecha desde lo alto" como destacó Vargas Llosa, tuvo como propósito que en Cuba se recupere la confianza y el respeto por uno mismo y por el prójimo: lo que definimos como la vida en libertad. Tal cual el profético poema Para escribir en el álbum de un tirano de Heberto Padilla, Hilda Molina descubrió "un día su voz fuerte", que empezó a martillar en su interior como una pregunta constante hasta transformarse, años después, en un asordinado grito de espanto y de protesta, y lo hizo su ideal.</p>
<p>&nbsp;</p>
<p>Ese ideal, descubierto tras décadas de dudas, vacilaciones y titubeos, en el que no se omiten sus propios errores y contradicciones, como sus preclaras convicciones, le ha costado a nuestra autora el asedio del gobierno cubano, su aislamiento, su silencio, así como un sinnúmero de insultos. Por ese propósito, esta valerosa mujer lo ha arriesgado todo: en particular, la seguridad de su familia más cercana y de su propia persona, como relata, aguijoneándonos el corazón, en Mi verdad. Su visión, no cabe duda, se convertirá en la realidad meridiana que arribará cuando cedan por fin las tinieblas de adrenalina y pesadilla de la dictadura cubana, pues como ella, citando el poema Discurso en verso de Vicente Valero, también "creo en la claridad de su caída".</p>
<p>&nbsp;</p>
<p>Mi verdad es un libro donde la devoción de Hilda Molina a Dios Padre, a su familia, a su madre, a su hijo, nos hace derramar más de una lágrima: tras su lectura queda patente el sueño de esta mujer porque Cuba sea una nación donde padres, hijos y nietos puedan vivir, trabajar y prosperar sin angustia ni opresión.</p>
<p>&nbsp;</p>
<p>En esta obra se une la vivencia personal de esta doctora en medicina, científica e investigadora, con la historia de Cuba; se descubre la luz de su mensaje, que denuncia, página a página, el camino de servidumbre que este decadente régimen le hace padecer durante más de quince años, que se ensaña con ella –sometiéndola a un trabajo esclavo y dejando sin camas a los enfermos que atendía– como contra sus seres queridos, subrayando de este modo las miopías del autoritarismo, su brutalidad y su sádica intimidación.</p>
<p>&nbsp;</p>
<p>Todo ello se confronta con la esperanza del cambio en el que Hilda cree y que nos transmite en sus reflexiones y vivencias junto a su familia: por su intermedio aprendemos que, cuando los tiempos se ponen difíciles, muy pronto vendrán las soluciones. Por todas estas razones, recomiendo fervorosamente la lectura de Mi verdad de Hilda Molina, testimonio de su voluntad, sus valiosos méritos y se fe inconmovible, y que la senda abierta el día de hoy, con su iluminada y, por muchos momentos, dolorosa certeza, que tanto emociona como denuncia, invite a las nuevas generaciones de latinoamericanos a una concienzuda, seria y solvente reflexión sobre el calvario cubano, y las infinitas posibilidades que se lograrán cuando la libertad se extienda por la isla, según reza el poema, "como el cielo en la línea febril del horizonte".</p>
<p>&nbsp;</p>
<p><span style="font-size: 8pt; font-family: 'Calisto MT', serif;">*Héctor Ñaupari. Lima, 1972. Escritor, ensayista, poeta y abogado. Presidente del Instituto de Estudios de la Acción Humana. Ex presidente de la Red Liberal de América Latina (RELIAL). Autor de <i>Sentido liberal, el sendero urgente de la libertad</i> (2012) y otras publicaciones.&nbsp;</span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/335-cien-años-de-un-escritor-indócil">
			&laquo; Cien años de un escritor indócil		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina">
			Dónde se vive mejor o peor en América Latina &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina#startOfPageId336">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:34:"La verdad de Hilda Molina - Relial";s:11:"description";s:156:"Por Héctor Ñaupari* &amp;nbsp; Hace unos días presentamos en Lima el libro Mi verdad de la doctora Hilda Molina, ante una multitudinaria audiencia, ju...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:25:"La verdad de Hilda Molina";s:6:"og:url";s:92:"http://www.relial.org/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina";s:8:"og:title";s:34:"La verdad de Hilda Molina - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/ee760d1c1474fde7f8f71a96b2452e29_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/ee760d1c1474fde7f8f71a96b2452e29_S.jpg";s:14:"og:description";s:160:"Por Héctor Ñaupari* &amp;amp;nbsp; Hace unos días presentamos en Lima el libro Mi verdad de la doctora Hilda Molina, ante una multitudinaria audiencia, ju...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:25:"La verdad de Hilda Molina";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}