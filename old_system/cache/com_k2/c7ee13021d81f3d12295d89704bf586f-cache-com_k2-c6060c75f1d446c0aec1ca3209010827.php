<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:13459:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId505"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Venezuela: el fin de la hegemonía chavista
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/0060e62cf7c869b03300254ca743ee3c_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/0060e62cf7c869b03300254ca743ee3c_XS.jpg" alt="Venezuela: el fin de la hegemon&iacute;a chavista" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><strong><span style="color: #000000;">"El cuadro político e institucional del país cambió radicalmente. Se acabó la hegemonía que el chavismo ejercía desde diciembre de 1998, cuando Hugo Chávez obtuvo la presidencia de la República de forma holgada. Por primera vez la alternativa democrática consigue un triunfo amplio e inobjetable..."</span></strong></p>
<p>&nbsp;</p>
<p>Trino Márquez, Director Académico de <a href="http://cedice.org.ve/">CEDICE Libertad</a></p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><strong><br /></strong></p>
<p><strong>Datos fundamentales</strong></p>
<p>En las elecciones de los diputados a la Asamblea Nacional del domingo 6 de diciembre, la alternativa democrática obtuvo una categórica victoria. De los 19.504.106 ciudadanos inscritos en el Registro Electoral (REP), 14.503.558 acudieron a los centros de votación (74, 5%). De ellos, 7.707.422 (56,5%) se pronunciaron por los candidatos de la Mesa de la Unidad Democrática (MUD); y 5.599.025 (41.0%) por los candidatos de la alianza oficialista, cuya principal organización es el Partido Socialista Unido de Venezuela (PSUV). La correlación de fuerzas dentro del Parlamento quedó: 112 curules para la MUD y 55 para el oficialismo. Los actores políticos que participaron fuera de estos dos grandes bloques obtuvieron votaciones marginales; quedaron sin representación parlamentaria.</p>
<p>&nbsp;</p>
<p>El método aprobado por el Gobierno en la reforma de la Ley Orgánica de Procesos Electorales (LOPRE) del año 2009, que acabó con el principio de la representación proporcional, en esta oportunidad se le revirtió. La MUD, con 56.5% de los votos, se quedó con 67, 06% de los parlamentarios; el oficialismo, con 41% de los sufragios, tendrá que conformarse con 32.93% de los diputados. La oposición había advertido que este cambio en la representación proporcional era inconveniente y que en algún momento afectaría al oficialismo. No pasó mucho tiempo para que este vaticinio se cumpliera. La oposición obtuvo la mayoría calificada de dos tercios, lo cual le da amplias competencias legislativas.</p>
<p>&nbsp;</p>
<p>El cuadro político e institucional del país cambió radicalmente. Se acabó la hegemonía que el chavismo ejercía desde diciembre de 1998, cuando Hugo Chávez obtuvo la presidencia de la República de forma holgada. Por primera vez la alternativa democrática consigue un triunfo amplio e inobjetable, que no pudo ser escamoteado a pesar de las intenciones de Nicolás Maduro, Presidente de República, y Diosdado Cabello, Presidente de la Asamblea Nacional (AN), de desconocer los resultados. La firmeza del ministro de la Defensa, general Vladimir Padrino López, la valentía de la MUD y la vigilancia y presión internacional, impidieron que se consumara un fraude escandaloso que, seguramente, habría provocado numerosas víctimas civiles.</p>
<p>&nbsp;</p>
<p>El PSUV y el resto de la alianza oficialista perdieron en las barriadas populares de Caracas y en la mayoría de los estados donde tradicionalmente habían triunfado con una clara ventaja. En algunas de estas entidades perdieron por márgenes que oscilan entre 15 y 20 puntos porcentuales, diferencias con las cuales antes ganaban. Se produjo, por lo tanto, una migración de votos movidos por el enorme descontento ante la crisis económica y social existente en el país, cuyos signos dominantes son la inflación, la escasez, el desabastecimiento y la inseguridad personal, producto de la delincuencia desbordada. El Gobierno solo logró preservar su votación en algunos estados con población fundamentalmente rural, donde el gasto público es determinante para el mantenimiento de esas comunidades y el clientelismo se ejerce de forma abierta.</p>
<p>&nbsp;</p>
<p>El pasado 6 de diciembre cristalizó una nueva mayoría. A partir de ahora, el Gobierno y sus aliados no podrán argumentar, para justificar sus atropellos y arbitrariedades, que ellos representan a la mayor parte de los venezolanos. Si bien es cierto que la nación sigue polarizada, el sector conformado por las capas descontentas y desilusionadas con el régimen son mucho mayores que aquellas que lo respaldan.</p>
<p>&nbsp;</p>
<p>La participación masiva de la gente y el enorme caudal de votos contra el Gobierno, se produjeron a pesar de las amenazas del régimen, el terrorismo de Estado que aplicó y el ventajismo obsceno durante la campaña y la fase previa.</p>
<p>&nbsp;</p>
<p><strong>Ganadores y perdedores</strong></p>
<p>Los ganadores de la consulta fueron los millones de venezolanos que acudieron a la urnas, incluidos los afectos al Gobierno. Los comicios demostraron que los venezolanos –a pesar de la vocación y las prácticas violentas y totalitarias de la élite gobernante- siguen confiando en la democracia en cuanto sistema, y en el voto como instrumento de participación, consulta y evaluación de las políticas públicas y la gestión del Gobierno.</p>
<p>&nbsp;</p>
<p>La otra ganadora fue la dirigencia de MUD, que con pulso e inteligencia fue acorralando al Gobierno para que realizara unas elecciones que no quería efectuar porque todos los sondeos previos le daban cifras negativas. La MUD logró eludir los obstáculos hasta encerrar a Maduro y a Cabello en un cuadrilátero electoral del cual no pudieron escaparse. La estrategia constitucional, pacífica, democrática y electoral terminó por imponerla MUD.</p>
<p>&nbsp;</p>
<p>Los otros triunfadores fueron los factores internacionales –Luis Almagro, secretario general de la OEA, expresidentes y exjefes de Estado, parlamentarios de la Unión Europea, Mauricio Macri, intelectuales de renombre planetario- quienes presionaron al Gobierno a través de denuncias y exigencias en las que pedían elecciones justas, equilibrio y, especialmente, respeto a los resultados, que -de acuerdo con los pronósticos- serían favorables a la opción democrática. Estos factores internacionales y la MUD formaron una tenaza que –a pesar de no conseguir la equidad, pues la campaña fue la más desequilibrada de la que se tenga memoria- logró que los comicios se llevaran a cabo en la fecha prevista y que el régimen acatase, a regañadientes, los resultados.</p>
<p>&nbsp;</p>
<p>Los perdedores fueron, en términos globales, el socialismo del siglo XXI, proyecto fundamental del régimen, causante de la crisis económica y social nacional, y la violencia, que los jefes chavistas estimularon durante la fase previa a la consulta y que pretendieron desatar cuando tuvieron evidencias de la debacle electoral. En términos más específicos, los perdedores fueron el PSUV, líder de la alianza oficialista, Nicolás Maduro, quien convirtió -sin que nadie se lo pidiese- la elección en un plebiscito; Diosdado Cabello, quien aspiraba a seguir al frente de la Asamblea; y Jorge Rodríguez, alcalde de Caracas y jefe del Comando de Campaña. En estas figuras se sintetiza la derrota.</p>
<p>&nbsp;</p>
<p><strong>El porvenir inmediato</strong></p>
<p>Venezuela formalmente es una República. La separación, el equilibrio y la cooperación de los poderes públicos se encuentran establecidos en la Constitución. Por primera vez en 17 años el Gobierno tendrá que cohabitar con un poder público controlado mayoritariamente por la oposición. Hasta ahora el Ejecutivo Nacional, primero con Hugo Chávez y luego con Nicolás Maduro, solo había tenido que coexistir con unos pocos gobernadores y alcaldes opositores. La situación actual es inédita, pues una rama del Poder central se encuentra en manos del adversario.</p>
<p>&nbsp;</p>
<p>La respuesta del Presidente de la República ante este nuevo cuadro ha consistido en reafirmar las tensiones con la oposición. Se ha negado a fomentar el diálogo y ha promovido la confrontación, en clara línea divergente con lo que ordena la Carta Magna y con la intención de los millones de venezolanos que votaron el 6-D.</p>
<p>&nbsp;</p>
<p>En este momento resulta imposible pronosticar si la colisión será el eje de la estrategia permanente de Nicolás Maduro y sus colaboradores más inmediatos. Si este fuera el camino, podría valerse de los colectivos para promover la violencia y del Tribunal Supremo de Justicia (TSJ), especialmente de la Sala Constitucional, para crear un conflicto de Poderes, que incluiría al Ejecutivo, a la AN y al TSJ. De optar por esta vía, los graves problemas nacionales se tornarían aún más agudos. Al régimen esta batalla con la Asamblea lo perjudicaría más que a la oposición, pues los ciudadanos dijeron con su voto que están en desacuerdo con las políticas del Gobierno, que están cansados de la pugnacidad permanente fomentada por el régimen y que desean soluciones inmediatas a los déficits que padecen. La superación de esas carencias implica el diálogo y la concertación entre el Gobierno y la oposición. El Ejecutivo es el Poder que cuenta con la capacidad financiera y logística para resolver las dificultades. El Legislativo solo puede actuar para proponer leyes que favorezcan el crecimiento con equidad y controlar la acción del Ejecutivo. Si cada iniciativa de la mayoría de la AN es torpedeada o ignorada por el Gobierno, la pendiente que agarró el domingo 6-D se hará todavía más inclinada.</p>
<p>&nbsp;</p>
<p>Es difícil saber si el sentido común y el instinto de supervivencia política prevalecerán en una élite arrogante, hegemónica y sectaria como la que ha dirigido al país desde febrero de 1999. El país, la MUD y la comunidad internacional la acorralaron una vez y podrían volver a hacerlo.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo">
			&laquo; Las elecciones parlamentarias en Venezuela: ambiente previo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/506-choque-de-trenes-en-venezuela">
			Choque de trenes en Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonía-chavista#startOfPageId505">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:52:"Venezuela: el fin de la hegemonía chavista - Relial";s:11:"description";s:159:"&quot;El cuadro político e institucional del país cambió radicalmente. Se acabó la hegemonía que el chavismo ejercía desde diciembre de 1998, cuando H...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:43:"Venezuela: el fin de la hegemonía chavista";s:6:"og:url";s:114:"http://www.relial.org/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonÃ­a-chavista";s:8:"og:title";s:52:"Venezuela: el fin de la hegemonía chavista - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/0060e62cf7c869b03300254ca743ee3c_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/0060e62cf7c869b03300254ca743ee3c_S.jpg";s:14:"og:description";s:163:"&amp;quot;El cuadro político e institucional del país cambió radicalmente. Se acabó la hegemonía que el chavismo ejercía desde diciembre de 1998, cuando H...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:43:"Venezuela: el fin de la hegemonía chavista";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}