<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7145:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId116"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	“Enseñamos a los empresarios a decir orgullosos: soy liberal”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7a6fe08027b80ee08bda1ed60d73e334_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7a6fe08027b80ee08bda1ed60d73e334_XS.jpg" alt="&ldquo;Ense&ntilde;amos a los empresarios a decir orgullosos: soy liberal&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El doctor Rafael Alfonzo Hernández, presidente del Centro de Divulgación del Conocimiento Económico para Libertad CEDICE LIBERTAD, visitó la sede de Noticia al Día, para compartir parte de lo que será su ponencia en la conferencia Cultura Liberal organizada por la Cámara de Industriales del Zulia, dirigida al empresariado zuliano en el Hotel El Paseo. "Queremos que los empresarios puedan decir orgullosos que son liberales y capitalistas", manifestó el exitoso emprendedor.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El doctor Alfonzo, destaca que aunque en este momento decirse liberal puede ser motivo de categóricas descalificaciones, por parte de sectores del Gobierno Nacional e incluso de la oposición, en realidad no hay nada de mal en ello. "Si alguien dice que quiere trabajar, quiere emprender un negocio y quiera superarse, entonces es un liberal aunque no lo diga", puntualiza el ex presidente ejecutivo de Alfonzo Rivas.</p>
<p>&nbsp;</p>
<p>En ese sentido, el empresario es enfático al afirmar que se deben destruir los paradigmas del subdesarrollo presentes en la cultura del venezolano, entre ellos el mito del Estado como único responsable del bienestar del pueblo. Entre menor control haya por parte del Estado será más fácil llegar al verdadero progreso, comenta.</p>
<p>&nbsp;</p>
<p>"El socialismo es el estado por encima del individuo, el capitalismo en cambio es el individuo, la persona, por encima del estado", explicó el ingeniero.</p>
<p>&nbsp;</p>
<p>Otro de los arquetipos que Alfonzo Hernández desea eliminar es el de la "tesis del más vivo" o la llamada "viveza criolla" que a su juicio perjudica en gran manera los intereses de todo el pueblo para salir del subdesarrollo.</p>
<p>&nbsp;</p>
<p>Para ilustrar este principio, Alfonzo utiliza una parábola práctica: "Desde que somos niños, se nos educa para que seamos más vivos que los demás. En las piñatas infantiles, son los adultos los que alientan a los niños a agarrar la mayor cantidad de caramelos posibles sin importar si tienen que golpear a otros niños, a veces incluso las madres se meten para agarrar más dulces".</p>
<p>&nbsp;</p>
<p>Alfonzo pertenece a una escuela de pensamiento que considera que solo después de superar estos obstáculos culturales muy relacionados con la educación temprana, desde el hogar, entonces se puede avanzar hacía el desarrollo de toda la sociedad en pleno.</p>
<p>&nbsp;</p>
<p>El graduado de la Escuela de Negocios de la Universidad de Harvard, se confiesa sumamente orgulloso de ser un "capitalista", pero aclara que lo que sí se debe combatir es el mercantilismo que aplasta las posibilidades de los pequeños empresarios.</p>
<p>&nbsp;</p>
<p>El empresario quien ha sido galardonado con varios reconocimientos internacionales por su labor desempeñada al frente del CEDICE y que jugó un papel importante en la mesa de negociación entre el empresariado venezolano y el gobierno nacional en el año 2001 y 2002, comparte sus ideas liberales pro capitalistas a través de su cuenta en la red social Twitter @Venelibertario.</p>
<p>&nbsp;</p>
<p>Aurores: Abraham Corona y Stevens Novsak</p>
<p>Foto: CEDICE, Centro de divulgación del conocimiento económico para la libertad</p>
<p>Fuente: CEDICE,</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/115-xi-jinping-en-las-américas">
			&laquo; Xi Jinping en las Américas		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal">
			RELIAL dio la bienvenida a la Internacional Liberal &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/116-“enseñamos-a-los-empresarios-a-decir-orgullosos-soy-liberal”#startOfPageId116">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:75:"“Enseñamos a los empresarios a decir orgullosos: soy liberal” - Relial";s:11:"description";s:157:"El doctor Rafael Alfonzo Hernández, presidente del Centro de Divulgación del Conocimiento Económico para Libertad CEDICE LIBERTAD, visitó la sede de N...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:66:"“Enseñamos a los empresarios a decir orgullosos: soy liberal”";s:6:"og:url";s:145:"http://www.relial.org/index.php/productos/archivo/actualidad/item/116-â€œenseÃ±amos-a-los-empresarios-a-decir-orgullosos-soy-liberalâ€";s:8:"og:title";s:75:"“Enseñamos a los empresarios a decir orgullosos: soy liberal” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/7a6fe08027b80ee08bda1ed60d73e334_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/7a6fe08027b80ee08bda1ed60d73e334_S.jpg";s:14:"og:description";s:157:"El doctor Rafael Alfonzo Hernández, presidente del Centro de Divulgación del Conocimiento Económico para Libertad CEDICE LIBERTAD, visitó la sede de N...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:66:"“Enseñamos a los empresarios a decir orgullosos: soy liberal”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}