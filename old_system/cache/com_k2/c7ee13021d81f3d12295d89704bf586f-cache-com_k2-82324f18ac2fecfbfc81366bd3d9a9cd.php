<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5092:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId43"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	SER LIBRES PARA SER HUMANOS y CARTAS SOBRE MÉXICO
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8b6e33345ac8d5ffd9cf0d107a7d9e9d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8b6e33345ac8d5ffd9cf0d107a7d9e9d_XS.jpg" alt="SER LIBRES PARA SER HUMANOS y CARTAS SOBRE M&Eacute;XICO" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>	  </div>
	  	  
		<div class="clr"></div>

	  	  <!-- Item extra fields -->
	  <div class="itemExtraFields">
	  	<h3>Info</h3>
	  	<ul>
									<li class="even typeTextfield group1">
								<span class="itemExtraFieldsLabel">Titulo:</span>
				<span class="itemExtraFieldsValue">SER LIBRES PARA SER HUMANOS y CARTAS SOBRE MÉXICO</span>
							</li>
												<li class="odd typeTextfield group1">
								<span class="itemExtraFieldsLabel">Autor(es):</span>
				<span class="itemExtraFieldsValue">Isaiah Berlin</span>
							</li>
												<li class="even typeTextfield group1">
								<span class="itemExtraFieldsLabel">Editorial:</span>
				<span class="itemExtraFieldsValue">Friedrich-Naumann-Stiftung für die Freiheit</span>
							</li>
												<li class="odd typeTextfield group1">
								<span class="itemExtraFieldsLabel">Idioma:</span>
				<span class="itemExtraFieldsValue">Español</span>
							</li>
												<li class="even typeTextfield group1">
								<span class="itemExtraFieldsLabel">Descripción:</span>
				<span class="itemExtraFieldsValue">La presente carta de Isaiah Berlin es un apretado resumen de su ideario. En ella, el autor de El erizo y la zorra y Pensadores rusos, entre otros clásicos del liberalismo del siglo XX, explica por qué nunca es aceptable el sacrificio individual en aras de una quimérica –y futura– mejora colectiva y hace una enconada defensa del libre albedrío.</span>
							</li>
									</ul>
	    <div class="clr"></div>
	  </div>
	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="Isaiah_Berlin_100_anios_Isaiah_Berlin.pdf" href="/index.php/biblioteca/item/download/24_8058a62747a2b3470a5504208414a541">Isaiah_Berlin_100_anios_Isaiah_Berlin.pdf</a>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/43-ser-libres-para-ser-humanos-y-cartas-sobre-méxico#startOfPageId43">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:59:"SER LIBRES PARA SER HUMANOS y CARTAS SOBRE MÉXICO - Relial";s:11:"description";s:1:" ";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:50:"SER LIBRES PARA SER HUMANOS y CARTAS SOBRE MÉXICO";s:6:"og:url";s:97:"http://relial.org/index.php/biblioteca/item/43-ser-libres-para-ser-humanos-y-cartas-sobre-méxico";s:8:"og:title";s:59:"SER LIBRES PARA SER HUMANOS y CARTAS SOBRE MÉXICO - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/8b6e33345ac8d5ffd9cf0d107a7d9e9d_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/8b6e33345ac8d5ffd9cf0d107a7d9e9d_S.jpg";s:14:"og:description";s:1:" ";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:20:"Políticas Liberales";s:4:"link";s:63:"/index.php/biblioteca/itemlist/category/13-políticas-liberales";}i:2;O:8:"stdClass":2:{s:4:"name";s:50:"SER LIBRES PARA SER HUMANOS y CARTAS SOBRE MÉXICO";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}