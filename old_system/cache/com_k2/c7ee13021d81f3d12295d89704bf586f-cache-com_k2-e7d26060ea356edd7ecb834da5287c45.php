<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8772:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId341"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La oposición venezolana arrasa
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/23f6a067599ae98276b159b7685c0abf_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/23f6a067599ae98276b159b7685c0abf_XS.jpg" alt="La oposici&oacute;n venezolana arrasa" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>A Nicolás Maduro le salió muy mal la primera ronda de conversaciones en el palacio de Miraflores. No sólo de consignas vive el hombre. Él, su gobierno, y media Venezuela, por primera vez debieron (o pudieron) escuchar en silencio las quejas y recriminaciones de una oposición que representa, cuando menos, a la mitad del país.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El revolucionario es una criatura voraz y extraña que se alimenta de palabras huecas. Era muy fácil declamar el discurso ideológico socialista con voz engolada y la mirada perdida en el espacio, tal vez en busca de pajaritos parlantes o de rostros milagrosos que aparecen en los muros, mientras se acusa a las víctimas de ser fascistas, burgueses, o cualquier imbecilidad que le pase por la cabeza al gobernante.</p>
<p>&nbsp;</p>
<p>El oficialismo habló de la revolución en abstracto. La oposición habló de la vida cotidiana. Para los espectadores no dogmáticos el resultado fue obvio: la oposición arrasó.</p>
<p>&nbsp;</p>
<p>Es imposible defenderse de la falta de leche, de la evidencia de que ese pésimo gobierno ha destruido el aparato productivo, de la inflación, de la huida en masa de los venezolanos más laboriosos, de las pruebas de la corrupción más escandalosa que ha sufrido el país, del saqueo perpetrado diariamente por la menesterosa metrópoli cubana, del hecho terrible que el año pasado fueron asesinados impunemente 25 000 venezolanos por una delincuencia que aumenta todos los días.</p>
<p>&nbsp;</p>
<p>¿Por qué Maduro creó esa guarimba antigubernamental en Miraflores? ¿Por qué pagó el precio de dañar inmensamente la imagen del chavismo y mostrar su propia debilidad dándole tribuna a la oposición?</p>
<p>&nbsp;</p>
<p>Tenía dos objetivos claros y no los logró. El primero era tratar de calmar las protestas y sacar a los jóvenes de las calles. El "Movimiento Estudiantil" –la institución más respetada del país, de acuerdo con la encuesta de Alfredo Keller—había logrado paralizar a Venezuela y mostrar las imágenes de un régimen opresivo patrullado por paramilitares y Guardias Nacionales que se comportaban con la crueldad de los ejércitos de ocupación y ya habían provocado 40 asesinatos.</p>
<p>&nbsp;</p>
<p>El segundo objetivo era reparar su imagen y la del régimen. Las encuestas lo demostraban: están en caída libre. Ya Maduro va detrás de la oposición por unos 18 puntos. Lo culpan (incluso su propia gente) de haber hundido el proyecto chavista y de ser responsable del desabastecimiento y de la violencia. Casi nadie se cree el cuento de que se trata de una conspiración de los comerciantes y de Estados Unidos. La inmensa mayoría del país (81%) respalda la existencia de empresas privadas. Dos de cada tres venezolanos tienen la peor opinión del gobierno cubano.</p>
<p>&nbsp;</p>
<p>Ese fenómeno posee un alto costo político internacional. Ciento noventa y ocho parlamentarios sudamericanos de diversos países, encabezados por la diputada argentina Cornelia Schmidt, se personaron ante la Corte Penal Internacional de La Haya para acusar a Maduro de genocidio, torturas y asesinatos. Eso es muy serio. Puede acabar enrejado, como Milosevic.</p>
<p>&nbsp;</p>
<p>Ser chavista sale muy caro. Lo comprobó el candidato costarricense José María Villalta. Esa (justa) acusación lo pulverizó en las urnas. En una encuesta realizada por Ipsos en Perú se confirmó que el 94% del país rechaza a Maduro y al chavismo. Eso lo sabe Ollanta Humala, quien hoy pone una distancia prudente con Caracas. Ni siquiera al popular Lula da Silva le convienen esas amistades peligrosas. Sólo Rafael Correa, quien padece una notable confusión de valores y no entiende lo que son la libertad y la democracia (en Miami se empeñó en defender a la dictadura de los Castro), insiste en su inquebrantable amistad con Maduro.</p>
<p>&nbsp;</p>
<p>La oposición, como dijo Julio Borges, va a seguir en las calles y, por supuesto, continuará dialogando con el régimen. ¿Hasta cuando? Hasta que suelten a los presos políticos, incluidos los alcaldes opositores, restituyan sus derechos a María Corina Machado y Leopoldo López. Hasta que el régimen renuncie al tutelaje vergonzoso e incosteable de La Habana, configure un Consejo Nacional Electoral neutral y le devuelva la independencia al Poder Judicial. Hasta que el gobierno desista de la deriva comunista y admita que los venezolanos no quieren "navegar hacia el mar cubano de la felicidad". En definitiva, hasta que celebren unas elecciones limpias, con observadores imparciales y se confirme lo que realmente quiere el pueblo: que se vayan Maduro y sus cómplices.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Fuente: Texto proporcionado amablemente por Carlos Alberto Montaner&nbsp;</p>
<p>Foto: Blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/340-diarios-venezolanos-reciben-papel-enviado-por-andiarios">
			&laquo; Diarios venezolanos reciben papel enviado por Andiarios		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/342-unidos-para-un-éxito-mayor-con-la-“alianza-para-centro-américa”">
			Unidos para un éxito mayor con la “Alianza para Centro América” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/341-la-oposición-venezolana-arrasa#startOfPageId341">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:40:"La oposición venezolana arrasa - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; A Nicolás Maduro le salió muy mal la primera ronda de conversaciones en el palacio de Miraflores....";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:31:"La oposición venezolana arrasa";s:6:"og:url";s:97:"http://relial.org/index.php/productos/archivo/actualidad/item/341-la-oposición-venezolana-arrasa";s:8:"og:title";s:40:"La oposición venezolana arrasa - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/23f6a067599ae98276b159b7685c0abf_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/23f6a067599ae98276b159b7685c0abf_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; A Nicolás Maduro le salió muy mal la primera ronda de conversaciones en el palacio de Miraflores....";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:31:"La oposición venezolana arrasa";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}