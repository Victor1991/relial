<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6657:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId434"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Seminario “Fortaleciendo las ONGs”
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/66f461a82eb04828d9f9f9abb89cb004_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/66f461a82eb04828d9f9f9abb89cb004_XS.jpg" alt="Seminario &ldquo;Fortaleciendo las ONGs&rdquo;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Julie Hernandez</p>
<p>&nbsp;</p>
<p>Gracias al apoyo de la Red Liberal de América Latina (RELIAL) y al Instituto de Ciencia Política Hernán Echavarría Olózaga, en el pasado mes de junio tuve la fortuna de participar en el seminario "Fortaleciendo las ONGs", impartido por la Fundación de Friedrich Naumann en la ciudad de Gummersbach, Alemania.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Durante 11 días compartí con jóvenes representantes de organizaciones de la sociedad civil de 27 países en vías de desarrollo, y con un equipo extraordinario de expertos en la materia, quienes nos llevaron paso a paso por el proceso de planeación estratégica necesario para constituir una ONG exitosa, y nos invitaron a reflexionar sobre las fortalezas, debilidades y retos que enfrentan las organizaciones allí representadas.</p>
<p>&nbsp;</p>
<p>En mi opinión, fueron tres los factores clave para el éxito de este proceso de autoevaluación: en primer lugar, la posibilidad de compartir experiencias, ideas e inquietudes con pares de otros países; convirtiéndose en una ventana de oportunidad para analizar desde una perspectiva más amplia los desafíos de nuestras ONGs, reevaluar prácticas y construir propuestas concretas para los interrogantes que iban surgiendo sobre la marcha. Segundo, el nivel de innovación y creatividad de las metodologías utilizadas, que al combinar elementos teórico-prácticos facilitaban el aprendizaje y la aplicabilidad del conocimiento. Y finalmente, el acompañamiento de nuestros mentores en el seminario, Wulf Pabst y Arno Keller, quienes conducían las discusiones hacia puntos de encuentro y aspectos clave a tener en cuenta.</p>
<p>&nbsp;</p>
<p>Las lecciones aprendidas son muchas y de distinta índole, pues superan el ámbito profesional gracias a los lazos de amistad, admiración y respeto que se tejieron entre los participantes. Sin embargo, en pocas palabras podría decir que el seminario nos brindó una "caja de herramientas" a la cual podemos acudir para fortalecer y hacer más pertinente la labor que desempeñan nuestras ONGs, transmitir de una manera más clara y contundente nuestros mensajes, y contribuir al desarrollo de nuestras democracias a través de la promoción de un diálogo fluido y respetuoso entre el Estado y la sociedad civil.</p>
<p>&nbsp;</p>
<p>Este último es sin lugar a dudas un reto demandante y de largo plazo; sin embargo mi paso por Gummersbach me deja con la grata impresión de que alrededor del mundo son cada vez más los jóvenes talentosos dispuestos a liderar, sumar esfuerzos y trabajar con pasión y compromiso en favor de este objetivo común.</p>
<p>&nbsp;</p>
<p>Autor: Julie Hernandez</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: ICP</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación">
			&laquo; Colombia y la farsa de la reconciliación		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/437-“strengthening-ngos-winning-support-for-ideas-and-their-political-implementation”">
			“Strengthening NGOs -Winning Support for Ideas and their Political Implementation” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/434-seminario-“fortaleciendo-las-ongs”#startOfPageId434">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"Seminario “Fortaleciendo las ONGs” - Relial";s:11:"description";s:157:"En la opinión de Julie Hernandez &amp;nbsp; Gracias al apoyo de la Red Liberal de América Latina (RELIAL) y al Instituto de Ciencia Política Hernán Ec...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"Seminario “Fortaleciendo las ONGs”";s:6:"og:url";s:105:"http://www.relial.org/index.php/productos/archivo/opinion/item/434-seminario-“fortaleciendo-las-ongs”";s:8:"og:title";s:47:"Seminario “Fortaleciendo las ONGs” - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/66f461a82eb04828d9f9f9abb89cb004_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/66f461a82eb04828d9f9f9abb89cb004_S.jpg";s:14:"og:description";s:161:"En la opinión de Julie Hernandez &amp;amp;nbsp; Gracias al apoyo de la Red Liberal de América Latina (RELIAL) y al Instituto de Ciencia Política Hernán Ec...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:38:"Seminario “Fortaleciendo las ONGs”";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}