<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10450:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId395"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/df1da44862bfbe0dd2169e4d14570593_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/df1da44862bfbe0dd2169e4d14570593_XS.jpg" alt="Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil &ndash; 10th Edition" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Tax Freedom Day is when the average Brazilian stops paying tax bills and starts hanging on to their money. The objective of the campaign is to create public awareness and show that Brazilian families have made enough money to pay off the total tax bill levied by all levels of government, that tax and spend the equivalent of 35% of the GDP. Studies show that Brazilians "work for the government" for 5 months a year and they lose the equivalent of 5 monthly salaries on taxes, and the population gets little in return given problems with transport, education, hospitals and security.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><img src="images/2014%20tax%20freedom%20day.jpg" alt="2014 tax freedom day" width="567" height="209" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Every year since 2004, Instituto Liberdade (ILRS), celebrate the Tax Freedom Day at the city of Porto Alegre, Brazil. This year, the think tank organized again a campaign to sell gasoline without taxes (53%), on May 20th, with the support and partnerships of the Institute of Entrepreneurial Studies (IEE), the Friedrich Naumann Foundation for Freedom Brazil, the Brazilian Institute of Fiscal Planning (IBPT), the Chamber of Young Leading Shopkeepers from Porto Alegre, the South Commercial Fuels and Lubricants Union (SULPETRO), the Commercial Fuels Distrib. SIM, and the Com. Fuels Distrib. Buffon. There were a total of 6 gas stations all located in the state of Rio Grande do Sul, selling a total of 30,000 liters of gasoline to 1,500 motorized vehicles, and had the media coverage of more than 20 vehicles, during the morning of May 20th.</p>
<p>&nbsp;</p>
<p>The same week was also busy because besides Porto Alegre, there were other organizations located in major cities in Brazil, with similar events, celebrating the TFD, but in different days, like in Vitória, Belo Horizonte (May 23rd), São Paulo, Rio de Janeiro. In the end it the Tax Freedom Week for the all the activities performed.</p>
<p>&nbsp;</p>
<p>Foto: Instituto Liberdade</p>
<p>Fuente: Texto proporcionado por el&nbsp;Instituto Liberdade</p>
<p>Autor:&nbsp;Instituto Liberdade</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
    <!-- Item image gallery -->
  <a name="itemImageGalleryAnchor" id="itemImageGalleryAnchor"></a>
  <div class="itemImageGallery">
	  <h3>Fotos</h3>
	  

<!-- JoomlaWorks "Simple Image Gallery Pro" Plugin (v2.5.7) starts here -->

<ul id="sigProIdead90bc037" class="sigProContainer sigProClassic sigProClassic">
		<li class="sigProThumb">
		<span class="sigProLinkOuterWrapper">
			<span class="sigProLinkWrapper">
				<a href="/media/k2/galleries/395/DLI%202014%20POA%20(2).jpg" class="sigProLink" style="width:180px;height:150px;" rel="lyteshow[galleryead90bc037]" title="" target="_blank">
										<img class="sigProImg" src="/plugins/content/jw_sigpro/jw_sigpro/includes/images/transparent.gif" alt="Click to enlarge image DLI 2014 POA (2).jpg" title="Click to enlarge image DLI 2014 POA (2).jpg" style="width:180px;height:150px;background-image:url(/cache/jw_sigpro/jwsigpro_cache_ead90bc037_dli_2014_poa_(2).jpg);" />
														</a>
			</span>
		</span>
	</li>
		<li class="sigProThumb">
		<span class="sigProLinkOuterWrapper">
			<span class="sigProLinkWrapper">
				<a href="/media/k2/galleries/395/DLI%202014%20POA%20(3).jpg" class="sigProLink" style="width:180px;height:150px;" rel="lyteshow[galleryead90bc037]" title="" target="_blank">
										<img class="sigProImg" src="/plugins/content/jw_sigpro/jw_sigpro/includes/images/transparent.gif" alt="Click to enlarge image DLI 2014 POA (3).jpg" title="Click to enlarge image DLI 2014 POA (3).jpg" style="width:180px;height:150px;background-image:url(/cache/jw_sigpro/jwsigpro_cache_ead90bc037_dli_2014_poa_(3).jpg);" />
														</a>
			</span>
		</span>
	</li>
		<li class="sigProThumb">
		<span class="sigProLinkOuterWrapper">
			<span class="sigProLinkWrapper">
				<a href="/media/k2/galleries/395/DLI%202014%20POA%20(6).jpg" class="sigProLink" style="width:180px;height:150px;" rel="lyteshow[galleryead90bc037]" title="" target="_blank">
										<img class="sigProImg" src="/plugins/content/jw_sigpro/jw_sigpro/includes/images/transparent.gif" alt="Click to enlarge image DLI 2014 POA (6).jpg" title="Click to enlarge image DLI 2014 POA (6).jpg" style="width:180px;height:150px;background-image:url(/cache/jw_sigpro/jwsigpro_cache_ead90bc037_dli_2014_poa_(6).jpg);" />
														</a>
			</span>
		</span>
	</li>
		<li class="sigProThumb">
		<span class="sigProLinkOuterWrapper">
			<span class="sigProLinkWrapper">
				<a href="/media/k2/galleries/395/DLI%202014%20POA%20(7).jpg" class="sigProLink" style="width:180px;height:150px;" rel="lyteshow[galleryead90bc037]" title="" target="_blank">
										<img class="sigProImg" src="/plugins/content/jw_sigpro/jw_sigpro/includes/images/transparent.gif" alt="Click to enlarge image DLI 2014 POA (7).jpg" title="Click to enlarge image DLI 2014 POA (7).jpg" style="width:180px;height:150px;background-image:url(/cache/jw_sigpro/jwsigpro_cache_ead90bc037_dli_2014_poa_(7).jpg);" />
														</a>
			</span>
		</span>
	</li>
		<li class="sigProThumb">
		<span class="sigProLinkOuterWrapper">
			<span class="sigProLinkWrapper">
				<a href="/media/k2/galleries/395/DLI%202014%20POA%20(9).jpg" class="sigProLink" style="width:180px;height:150px;" rel="lyteshow[galleryead90bc037]" title="" target="_blank">
										<img class="sigProImg" src="/plugins/content/jw_sigpro/jw_sigpro/includes/images/transparent.gif" alt="Click to enlarge image DLI 2014 POA (9).jpg" title="Click to enlarge image DLI 2014 POA (9).jpg" style="width:180px;height:150px;background-image:url(/cache/jw_sigpro/jwsigpro_cache_ead90bc037_dli_2014_poa_(9).jpg);" />
														</a>
			</span>
		</span>
	</li>
		<li class="sigProClear">&nbsp;</li>
</ul>


<div class="sigProPrintMessage">
	View the embedded image gallery online at:
	<br />
	<a title="Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition" href="http://www.relial.org/index.php/productos/archivo/actualidad/item/395-celebrating-the-2014-tax-freedom-day-tfd-in-porto-alegre-brazil-%E2%80%93-10th-edition#sigProGalleriaead90bc037">http://www.relial.org/index.php/productos/archivo/actualidad/item/395-celebrating-the-2014-tax-freedom-day-tfd-in-porto-alegre-brazil-%E2%80%93-10th-edition#sigProGalleriaead90bc037</a>
</div>

<!-- JoomlaWorks "Simple Image Gallery Pro" Plugin (v2.5.7) ends here -->

  </div>
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/374-relial-y-su-trabajo-en-derechos-humanos">
			&laquo; RELIAL y su trabajo en Derechos Humanos		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/396-colombia-da-voto-de-confianza-a-santos-y-al-proceso-de-paz">
			Colombia da voto de confianza a Santos y al proceso de paz &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/395-celebrating-the-2014-tax-freedom-day-tfd-in-porto-alegre-brazil-–-10th-edition#startOfPageId395">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:92:"Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition - Relial";s:11:"description";s:152:"Tax Freedom Day is when the average Brazilian stops paying tax bills and starts hanging on to their money. The objective of the campaign is to create...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:83:"Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition";s:6:"og:url";s:150:"http://www.relial.org/index.php/productos/archivo/actualidad/item/395-celebrating-the-2014-tax-freedom-day-tfd-in-porto-alegre-brazil-–-10th-edition";s:8:"og:title";s:92:"Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/df1da44862bfbe0dd2169e4d14570593_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/df1da44862bfbe0dd2169e4d14570593_S.jpg";s:14:"og:description";s:152:"Tax Freedom Day is when the average Brazilian stops paying tax bills and starts hanging on to their money. The objective of the campaign is to create...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:5:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:68:"/plugins/content/jw_sigpro/jw_sigpro/includes/js/lytebox/lytebox.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:66:"/plugins/content/jw_sigpro/jw_sigpro/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";s:6:"screen";s:7:"attribs";a:0:{}}s:59:"/plugins/content/jw_sigpro/jw_sigpro/includes/css/print.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";s:5:"print";s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:8:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_sigpro/jw_sigpro/includes/js/lytebox/lytebox.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:61:"/plugins/content/jw_sigpro/jw_sigpro/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:83:"Celebrating the 2014 Tax Freedom Day (TFD) in Porto Alegre, Brazil – 10th Edition";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}