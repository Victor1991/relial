<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6096:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId427"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El populismo está en guerra con el estado de derecho
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/37e725efe26e0487bc83287a1c350936_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/37e725efe26e0487bc83287a1c350936_XS.jpg" alt="El populismo est&aacute; en guerra con el estado de derecho" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La estabilidad económica y del desarrollo depende de la calidad de las instituciones nacionales, afirma el economista argentino Martin Krause, creador del Índice de Calidad Institucional y académico adjunto del Instituto El Cato.</p>
<p>&nbsp;</p>
<p>Durante su visita a Caracas el pasado jueves, Krause ofreció la conferencia "Política pública liberal: ¿Cómo superar la pobreza?". Ésta se realizó como parte de la reunión general anual de CEDICE Libertad, un instituto de políticas libertarias situado en Venezuela.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Las instituciones están definidas como las reglas que nos permiten coordinar las acciones de los individuos en la sociedad, y "la calidad institucional significa calidad de oportunidades para que cada uno de nosotros podamos progresar", explicó el profesor de la Universidad de Buenos Aires. "Esto no sólo crea un ambiente de prosperidad y una mayor calidad de vida, sino también garantiza los principios de descentralización y límite de poderes".</p>
<p>&nbsp;</p>
<p>Desafortunadamente, el economista argentino analiza cómo las naciones latinoamericanas han sido presas por el populismo por décadas: "El populismo es lo opuesto a las instituciones porque pone responsabilidad en un líder carismático para poder resolver los problemas, en lugar de emplear la ley".</p>
<p>&nbsp;</p>
<p>Krause alega que Venezuela tiene la peor calidad institucional del continente. De hecho, la edición de 2014 de su índice coloca a Venezuela en la posición número 184, detrás de Siria y al frente de Myanmar y Zimbabwe (<a href="index.php/biblioteca/item/360-indice-de-calidad-institucional-2014">p.6 PDF</a>). La situación ha ido empeorando, tomando en cuenta que ha bajado desde la posición 161, cuando el índice empezó a desarrollarse en el año 2007.</p>
<p>&nbsp;</p>
<p>Él dice que hay una gran necesidad de consenso para superar las dificultades económicas: "Los venezolanos deben darse cuenta de que los países desarrollados son aquellos donde existe el respeto a la propiedad privada, libertad económica, y un poder judicial independiente"</p>
<p>&nbsp;</p>
<p>Fuente: panampost.com</p>
<p>Foto: CEDICE</p>
<p>Autor: Sendai Zea</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte">
			&laquo; Manuel una noche echó a andar rumbo al norte		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos">
			El sistema liberal ha mejorado la vida de los peruanos &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho#startOfPageId427">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:62:"El populismo está en guerra con el estado de derecho - Relial";s:11:"description";s:154:"La estabilidad económica y del desarrollo depende de la calidad de las instituciones nacionales, afirma el economista argentino Martin Krause, creador...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:53:"El populismo está en guerra con el estado de derecho";s:6:"og:url";s:120:"http://www.relial.org/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho";s:8:"og:title";s:62:"El populismo está en guerra con el estado de derecho - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/37e725efe26e0487bc83287a1c350936_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/37e725efe26e0487bc83287a1c350936_S.jpg";s:14:"og:description";s:154:"La estabilidad económica y del desarrollo depende de la calidad de las instituciones nacionales, afirma el economista argentino Martin Krause, creador...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:53:"El populismo está en guerra con el estado de derecho";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}