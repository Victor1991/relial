<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:14938:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId268"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Reeleccionismo mágico latinoamericano
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/500a44935c8320008f1c713a63e32b8e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/500a44935c8320008f1c713a63e32b8e_XS.jpg" alt="Reeleccionismo m&aacute;gico latinoamericano" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Javier El-Hage</p>
<p>&nbsp;</p>
<p>Luego de organizar treinta y dos levantamientos armados contra el gobierno, el coronel Aureliano Buendía regresó a Macondo desencantado de la política:</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<blockquote>
<p>Encerrado en su taller, su única relación con el resto del mundo era el comercio de pescaditos de oro. Uno de los antiguos soldados que vigilaron su casa en los primeros días de la paz, iba a venderlos a las poblaciones de la ciénaga, y regresaba cargado de monedas y de noticias. <em>Que el gobierno conservador, decía, con el apoyo de los liberales, estaba reformando el calendario para que cada presidente estuviera cien años en el poder.</em> Que por fin se había firmado el concordato con la Santa Sede, y que había venido desde Roma un cardenal con una corona de diamantes y en un trono de oro macizo, y que los ministros liberales se habían hecho retratar de rodillas en el acto de besarle el anillo. Que la corista principal de una compañía española, de paso por la capital, había sido secuestrada en su camerino por un grupo de enmascarados, y el domingo siguiente había bailado desnuda en la casa de verano del presidente de la república. «No me hables de política —le decía el coronel—. Nuestro asunto es vender pescaditos.»</p>
</blockquote>
<p>&nbsp;</p>
<p>La desvergüenza que desencantó al coronel Buendía sigue estando tan vigente hoy en América Latina como cuando Gabriel García Márquez publicó la novela genial que le ganaría el Premio Nobel de Literatura. Entre antojadizos fraudes electorales, pelotones de fusilamiento, alzamientos armados, golpes de Estado y masacres genocidas, “Cien Años de Soledad” relata la historia universal y fabulosa que experimenta una familia prolífica y longeva en un pueblito de la selva colombiana. La historia está contada de manera tan fantástica que, en vez de indignación e impotencia, los abusos del gobierno provocan en el peor de los casos resignación, en el mejor esperanza, y con seguridad diversión.</p>
<p>&nbsp;</p>
<p>Las reformas constitucionales promovidas por los presidentes latinoamericanos de turno para facilitar sus propias reelecciones son la clase de triquiñuelas que conviene ver con ojos de realismo mágico para evitar caer en la desesperanza. En un artículo previo sobre la nutrida historia de <a href="http://internacional.elpais.com/internacional/2013/05/27/actualidad/1369606489_800894.html">reeleccionismo en el continente americano</a>, ya explicamos en términos jurídicos por qué este tipo de reformas violan el derecho internacional y por qué deberían provocar el rechazo de la Organización de Estados Americanos.</p>
<p>&nbsp;</p>
<p><a href="http://internacional.elpais.com/internacional/2014/01/29/actualidad/1390955328_152316.html">Recientemente</a>, el Frente Sandinista del presidente Daniel Ortega aprovechó su mayoría parlamentaria para reformar la constitución de Nicaragua de manera que el excomandante guerrillero pueda ser reelegido indefinidamente. En realidad, el artículo derogado (147) ya había sido declarado “inaplicable” por el Tribunal Supremo nicaragüense en el 2009, por lo que la reforma fue simplemente para ponerle un sello más grande y solemne a un atropello constitucional previo. (Algo así como cuando luego de la apelación definitiva a cargo de los abogados de la compañía bananera “se estableció por fallo de tribunal y se proclamó en bandos solemnes la inexistencia de los trabajadores”.)</p>
<p>&nbsp;</p>
<p>En 2010, los magistrados del bloque sandinista convocaron a una <a href="http://www.confidencial.com.ni/articulo/1970/corte-sandinista-decreta-la-reeleccion-de-daniel-ortega">reunión secreta</a> (sin los magistrados afines al partido opositor) y ratificaron una sentencia de 2009 que establece que, dado que los legisladores sí pueden ser reelectos según la constitución, la prohibición de la reelección presidencial era “discriminatoria” contra Ortega y violatoria del principio de “igualdad incondicional de todo ciudadano nicaragüense”.</p>
<p>&nbsp;</p>
<p>Bajo ese argumento desvergonzado, cualquier legislador nicaragüense (o cualquier ciudadano) podría arrogarse la potestad de emitir decretos, de comandar la policía y las fuerzas armadas, o de manejar las relaciones exteriores del país —todas éstas atribuciones que la constitución otorga exclusivamente al presidente— porque prohibírselo sería discriminatorio, anticonstitucional e “inaplicable”.</p>
<p>&nbsp;</p>
<p>En un ejercicio similar de alquimia constitucional, el tribunal constitucional de Bolivia autorizó en abril de 2013 la segunda reelección consecutiva del presidente Evo Morales <a href="http://www.kas.de/wf/doc/9857-1442-4-30.pdf">contradiciendo</a> el texto expreso de la constitución que la prohibía (disposición transitoria segunda). Según el razonamiento de este tribunal, el primer mandato del presidente (2006-2010) no contó porque en esos momentos Morales era el presidente de la <em>República de Bolivia</em>, mientras que en 2010 habría sido electo —por primera vez— como presidente del “refundado” <em>Estado Plurinacional de Bolivia.</em></p>
<p>&nbsp;</p>
<p>Como en Nicaragua, los nuevos jurisconsultos de Bolivia no son ni juristas, ni probos, ni independientes, ni cayeron del cielo. Al llegar al poder, Morales <a href="http://www.lostiempos.com/diario/actualidad/nacional/20060313/gobierno-critica-a-instituciones-que-no-siguen-plan-de-austeridad-de_5321_5321.html">redujo</a> el salario de los antiguos magistrados; les mandó una turba de mineros que <a href="http://www.infobae.com/2007/04/26/313521-mineros-evo-morales-lanzan-dinamita-tribunal">dinamitaron</a> el tribunal; y luego, con su constitución de 2010, creó el nuevo “Tribunal Constitucional Plurinacional” e hizo <a href="http://internacional.elpais.com/internacional/2011/10/12/actualidad/1318402613_464387.html">elegir</a> catorce nuevos jueces por voto popular, en un proceso que el Colegio de Abogados de Bolivia <a href="http://www.lostiempos.com/diario/actualidad/politica/20110616/abogados-piden-nueva-eleccion-judicial_130107_262984.html">calificó</a> de “nulo” (en vano por supuesto).</p>
<p>&nbsp;</p>
<p>Uno de los flamantes magistrados electos se llama Gualberto Cusi Mamani, quien se hizo famoso en 2012 cuando realizó una <a href="https://www.youtube.com/watch?v=Jea2Fs0VRdI">demostración</a> televisada de cómo él leía las hojas de coca para decidir sus fallos (no todos sus fallos, sólo los “complejos”, valga la aclaración). El escándalo fue tal que sus colegas magistrados rechazaron sus declaraciones y aclararon que el método jurisdiccional de Cusi Mamani “de ninguna manera representaba” la práctica del resto del tribunal. Hasta una comisión de la Asamblea Plurinacional oficialista le convocó para que se explique. Cusi Mamani <a href="http://www.youtube.com/watch?v=B00bSx0evxA">afirmó</a> a los legisladores que “no cualquier gil lee la coca”, sino solamente los “seres sagrados”, los “impactados por un rayo” o los “gemelos”.</p>
<p>&nbsp;</p>
<p>Tan realista y mágica es la situación actual de Bolivia que luego de este episodio Cusi Mamani se ha convertido en la única esperanza de disidencia en la justicia constitucional boliviana. Molesto por la ridiculización a la que fue sometido por sus colegas, Cusi Mamani se animó a <a href="http://internacional.elpais.com/internacional/2013/09/24/actualidad/1379984463_667709.html">denunciar</a> presiones directas del ejecutivo en el tribunal constitucional y, argumentando “problemas de salud”, fue el único magistrado que se excusó de conocer el asunto de la reelección de Morales. Meses después del fallo reeleccionista y ante una gran expectativa, el magistrado dijo a la prensa que había leído en las hojas de coca que Morales sería reelecto en 2014, pero que no <a href="http://es.noticias.yahoo.com/bolivia-hojas-coca-dan-reelecto-eco-pero-no-161632342.html">terminaría</a> su mandato. Digamos que la opinión <a href="http://www.youtube.com/watch?v=B00bSx0evxA&amp;feature=youtu.be&amp;t=27s">cósmica</a> del magistrado Cusi Mamani no es una de esas opiniones disidentes que terminarán sentando las bases de sólidas líneas jurisprudenciales futuras (como las de Oliver Wendell Holmes Jr. en la Corte Suprema de EEUU a inicios del siglo XX), pero convengamos también que su temeridad y su lastimado orgullo lo convierten hoy por hoy en el juez más honrado de esa corte de justicia.</p>
<p>&nbsp;</p>
<p>La manera escandalosamente tramposa en que Ortega y Morales han pisoteado las constituciones de sus países para lograr su propia re-reelección bien pudiera ser el producto de la fantasía de algún escritor (una especie de “reeleccionismo mágico”) pero es tan real como que Ménem (1994), Cardozo (1997), Fujimori (1993) Chávez (1999-2009), Uribe (2004) y Correa (2008) hicieron lo mismo en sus respectivos países, con sellos congresales y fallos de cortes supremas de por medio. Lastimosamente, la historia del desprecio por el orden constitucional en América Latina es tan real como que Pinochet <a href="http://www.youtube.com/watch?v=SIHm8herJZs">bombardeó</a> el palacio presidencial chileno con aviones de la fuerza aérea y con el presidente dentro, como que Videla <a href="http://www.elmundo.es/elmundo/2009/09/23/internacional/1253721898.html">arrojó</a> miles de prisioneros drogados y desnudos al Atlántico y <a href="#t=258">explicó</a> luego por televisión a sus familias que el “desaparecido, en tanto esté como tal, es una incógnita […] no tiene entidad, no está” y que por eso en el gobierno nacional “no podemos hacer nada”, y como que la dictadura comunista cubana cumplió este mes cincuenta y cinco años en el poder rebalsada del cariño y la admiración (y acaso una gota de sana envidia) de todos los presidentes latinoamericanos que le están visitando en La Habana.</p>
<p>&nbsp;</p>
<p>En Cien Años de Soledad, el coronel Aureliano Buendía se volvió liberal tras presenciar incrédulo cómo don Apolinar Moscote, suegro suyo y corregidor de Macondo por el partido conservador, había abierto con toda soltura las ánforas de las elecciones generales y sacado una por una las papeletas rojas con los votos liberales, antes de sellar las cajas nuevamente y enviarlas a la capital llenas de papeletas azules. “Si hay que ser algo sería liberal —dijo poco tiempo después el coronel—, porque los conservadores son unos tramposos”. Pero después de liderar treinta y dos levantamientos armados contra el gobierno conservador, por la época en que se dedicaba ya solamente a sus pecaditos de oro, se le oyó decir resignado: “La única diferencia actual entre liberales y conservadores, es que los liberales van a misa de cinco y los conservadores van a misa de ocho”.</p>
<p>&nbsp;</p>
<p>fuente: <a href="http://internacional.elpais.com/internacional/2014/01/30/actualidad/1391111239_603537.html">El país</a></p>
<p>foto: El país</p>
<p>Autor: Javier El - Hage</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/263-los-saqueadores">
			&laquo; Los saqueadores		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/271-vargas-llosa-se-debe-defender-el-pluralismo-informativo">
			Vargas Llosa: "Se debe defender el pluralismo informativo" &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/268-reeleccionismo-mágico-latinoamericano#startOfPageId268">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"Reeleccionismo mágico latinoamericano - Relial";s:11:"description";s:155:"En la opinión de Javier El-Hage &amp;nbsp; Luego de organizar treinta y dos levantamientos armados contra el gobierno, el coronel Aureliano Buendía re...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"Reeleccionismo mágico latinoamericano";s:6:"og:url";s:107:"http://www.relial.org/index.php/productos/archivo/opinion/item/268-reeleccionismo-mÃ¡gico-latinoamericano";s:8:"og:title";s:47:"Reeleccionismo mágico latinoamericano - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/500a44935c8320008f1c713a63e32b8e_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/500a44935c8320008f1c713a63e32b8e_S.jpg";s:14:"og:description";s:159:"En la opinión de Javier El-Hage &amp;amp;nbsp; Luego de organizar treinta y dos levantamientos armados contra el gobierno, el coronel Aureliano Buendía re...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:38:"Reeleccionismo mágico latinoamericano";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}