<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10140:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId298"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Manifiesto de Mérida de los estudiantes
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7ab716354432ee12f19b58c60471093b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7ab716354432ee12f19b58c60471093b_XS.jpg" alt="Manifiesto de M&eacute;rida de los estudiantes" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Los estudiantes hemos unificado a la Nación en torno a la conquista de la Libertad de Venezuela. Por eso el régimen castro-comunista con grupos paramilitares y la Guardia Nacional, han asesinado, torturado y apresado compañeros en todo el territorio nacional. El pueblo se ha sumado con nosotros: se ha despertado el espíritu libertario ante la opresión de 15 años de este sistema político. En virtud de este momento histórico, sostenemos ante el país la siguiente postura de manera contundente, honorable e irrenunciable, frente a los que nos quieren dominar y sus colaboradores:</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>1) Todo sistema político es vigente en la medida que sean representativos sus líderes: el pueblo en la calle ha demostrado que hoy dejaron de serlo. En este sentido, nos piden dialogar luego de 15 años de opresión. Nosotros no queremos paz de esclavos: nosotros somos venezolanos libres. No hay diálogo posible con un régimen clientelista y totalitario, que busca hacernos dependientes. La Libertad no está en venta. Nosotros no vamos a legitimar un régimen que ha empobrecido a la mayoría de los venezolanos. Las mesas de diálogo del año 2004, en las que estuvieron Jimmy Carter y César Gaviria, demostraron que no hay buenas intenciones. Este tipo de regímenes son ineficientes: a propósito destruyen a las Naciones para poder controlar mejor. No dialogaremos para perpetuar en el poder a los peores venezolanos. Cualquier posibilidad de diálogo para nosotros, está condicionada imperativamente, por un cambio de raíz del sistema político.</p>
<p>&nbsp;</p>
<p>2) Exigimos como patriotas la retirada de toda fuerza de ocupación militar cubana. Queremos que salgan de nuestro territorio, todos los funcionarios dentro de nuestras instituciones por cuanto representan con su injerencia en nuestros asuntos internos una amenaza a la seguridad de la Nación. Nosotros queremos una Nación soberana.</p>
<p>&nbsp;</p>
<p>3) Vamos a conquistar nuestra Libertad. Libertad individual. Libertad económica para poder vivir producto de nuestro propio esfuerzo. Libertad de expresión para informarnos y expresarnos sin controles de ningún régimen. Libertad política para manifestarnos, reunirnos y organizarnos sin que sea un delito. Libertad para elegir sin que signifique una farsa. No toleramos los controles de este régimen castro-comunista ni de ningún otro que atente contra los valores y principios de los venezolanos.</p>
<p>&nbsp;</p>
<p>4) Exigimos la disolución y desarme de los grupos paramilitares que protege y arma el régimen para infundir el miedo entre los venezolanos. En este sentido, mientras siga el ataque terrorista por parte de estos grupos de manera impune, llamamos al pueblo a que se defienda. Nosotros nos protegemos con las "Trincheras de la Libertad" en nuestras ciudades y pueblos, porque no permitiremos que ataquen a lo más sagrado para nosotros: la familia y el patrimonio.</p>
<p>&nbsp;</p>
<p>5) Exigimos la liberación inmediata de todos los presos políticos, libertad plena para los que fueron detenidos y el regreso de todos los exiliados.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Venezolanos,</p>
<p>&nbsp;</p>
<p>Es este el momento de definir nuestro destino. Está a prueba nuestro gentilicio y hemos sensibilizado al mundo con nuestro coraje. No hay posibilidad de cambio real si no logramos llegar hasta el final: por eso, está prohibido perder. Vamos a vencer, y con el pueblo refundar a la Nación como la más libre del continente. Por la memoria de nuestros caídos no nos vamos a rendir. Es el momento de reescribir la historia de Venezuela y suscribirla con el sudor de nuestro esfuerzo.</p>
<p>&nbsp;</p>
<p>Es por ello, que a partir del día de hoy 02 de Marzo del 2014 desde las trincheras de la Libertad de la Ciudad de Mérida, declaramos constituida la JUNTA PATRIÓTICA ESTUDIANTIL Y POPULAR (JPEP) con la finalidad de devolverle la Libertad y la Soberanía a Venezuela, en reunión de los dirigentes estudiantiles, juveniles y luchadores populares del país, suscribimos los abajo firmantes,</p>
<p>&nbsp;</p>
<p>Libertad o nada.</p>
<p>&nbsp;</p>
<p>Cuenta oficial de la Junta Patriótica: @jpep2014</p>
<p>Anzoátegui: Eduardo Bittar, Marialvic Olivares, José Petete, Johan Infante, Juan Ibor.</p>
<p>Apure: Alberto Mogollón.</p>
<p>Barinas: Luis Octavio Rivas.</p>
<p>Bolívar: Samuel Petit, Ligia Bolívar, Rafael Tirado, Jonás Díaz.</p>
<p>Carabobo: Julio Cesar Rivas, Gustavo Galea, Lorent Saleh, Carlos Graffe, Ivan Uzcátegui, Rita Roa, Sabrina Cruces, Alfredo Stelling, Dimitry Belov, Marco Bozo, Francisco Abreu, Ángel Morales, Hermogenes López, Patricia Salamanca, Betania Bermejo, Angel Morales, Gabriela Suniaga, Freymar Torres, Luis Rodríguez, Daniel Coronel, Rina Rivas.</p>
<p>Caracas: Roderick Navarro, Francisco Márquez, Juan Flores, Juan Requesens, Josmir Gutiérrez, Ana Karina García, Javier González, Juan Carlos Apitz, José Javier Martínez, Guillermo Prieto, Pavel Quintero, Pablo Sanchez.</p>
<p>Lara: Jorge López, Luis Soteldo, Álvaro Avendaño, Iraiby Rodríguez, Raúl Rodríguez, Gabriel Lugo, Guillermo Osorno, Xavier Aldana, Tobias Alvarado.</p>
<p>Mérida: Gaby Arellano Villca Fernández, Liliana Guerrero, Aimara Rivas, Guido Mercado, Diego Rimer, Eloi Araujo, José Pozo, Rafael Chinchilla, Jackson Durán, Alirio Arroyo, Fabiana Santamaría, Rómulo Canelón, Nilver Torres, Miguel Gómez, Gina Rodríguez, Carlos Velasco, Charly Aponte, Carlos Ramírez, Augusto García, Edgar Osuna, Jorge Arellano, Jean Paul Méndez, Carlos Velázquez, Antonio Indriago, Alfredo Ocanto.</p>
<p>Monagas: Miguel Meneses</p>
<p>Portuguesa: Víctor Uribe.</p>
<p>Sucre: Eduardo Roque.</p>
<p>Táchira: José Vicente García, Wilmer Zabaleta, Leonardo Montilla, Reinaldo Manrique, Miguel Ochoa, José Aguilar, Carlos Maldonado, Juan Carlos Palencia.</p>
<p>Trujillo: Gabriel Boscán, Elsi Guillén, Jairo Villa, Luis Borrero, Ronald Flores, Gabriel Pérez, Andrea Bracamonte, Jesús Rubio.</p>
<p>Zulia: Yorman Barilla, Eduardo Fernandez, Juan Urdaneta, Victor Ruz, Luis Diaz.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Roderick Navarro</p>
<p>Fuente: www.rodericknavarro.wordpress.com</p>
<p>Foto: CEDICE</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico">
			&laquo; Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/299-manifiesto-de-mérida-de-los-estudiantes">
			Manifiesto de Mérida de los estudiantes &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/298-manifiesto-de-mérida-de-los-estudiantes#startOfPageId298">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:49:"Manifiesto de Mérida de los estudiantes - Relial";s:11:"description";s:155:"Los estudiantes hemos unificado a la Nación en torno a la conquista de la Libertad de Venezuela. Por eso el régimen castro-comunista con grupos parami...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:40:"Manifiesto de Mérida de los estudiantes";s:6:"og:url";s:112:"http://relial.org/index.php/productos/archivo/actualidad/item/298-manifiesto-de-mÃƒÂ©rida-de-los-estudiantes";s:8:"og:title";s:49:"Manifiesto de Mérida de los estudiantes - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/7ab716354432ee12f19b58c60471093b_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/7ab716354432ee12f19b58c60471093b_S.jpg";s:14:"og:description";s:155:"Los estudiantes hemos unificado a la Nación en torno a la conquista de la Libertad de Venezuela. Por eso el régimen castro-comunista con grupos parami...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:40:"Manifiesto de Mérida de los estudiantes";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}