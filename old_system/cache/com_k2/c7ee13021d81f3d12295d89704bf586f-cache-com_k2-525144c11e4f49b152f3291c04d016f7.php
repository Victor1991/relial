<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9081:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId262"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/5a61d31ed794cb758475f6c89477dfed_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/5a61d31ed794cb758475f6c89477dfed_XS.jpg" alt="CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado m&aacute;s influyentes de Am&eacute;rica Latina" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>CEDICE Libertad ocupa el puesto No. 1 entro los centros de estudios de libre mercado más influyentes de América Latina, y el No. 2 entre los institutos más relevantes de la región en el índice 2013 sobre centros de estudios o think tanks que realiza anualmente la Universidad de Pennsylvania.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>1900 expertos en políticas públicas alrededor del mundo votaron por los institutos, que a su juicio, tienen más relevancia en diversas regiones y diversas áreas de estudio en el mundo.</p>
<p>&nbsp;</p>
<p>Caracas 27 de Enero 2014 – El día de ayer fue presentado el Índice "2013 Global Go to Think Tank Index", realizado por el profesor James MacGann, director del departamento de Relaciones Internacionales, Centros de Estudios y Sociedad Civil de la Universidad de Pennsylvania. El evento tuvo lugar en el World Bank en la ciudad de Washington</p>
<p>&nbsp;</p>
<p>En este Índice, entre los Centros afines a la economía de mercado con más alto puntaje en América Latina, se encuentran CEDICE Libertad en Venezuela (Centro de Divulgación del Conocimiento Económico para la Libertad) en 1er lugar, luego el Centro de Estudios Públicos de Chile que ocupa el 2do lugar, y en la clasificación total de América Latina, CEDICE ocupó el 2do lugar.</p>
<p>&nbsp;</p>
<p>Según McGann, el estudio refleja el rol protagónico que tienen los llamados Think Tanks, centros que desempeñan importantes funciones para el desarrollo de la sociedad civil, abriendo espacios de debate y promoviendo las libertades en sus países. Su rol adquiere gran importancia para promover las bases de un verdadero Estado de Derecho, en el que se asegure un marco de respeto a la libertad individual y los derechos civiles. Estos Think Tanks están llevando un mayor protagonismo desde la sociedad civil para el desarrollo de políticas públicas.</p>
<p>&nbsp;</p>
<p>Venezuela ocupa los últimos lugares en los índices de libertad económica, calidad institucional, derechos de propiedad, transparencia, entre otros, sin embargo aquellos que trabajan para incrementar la libertad económica y el respeto a la propiedad privada con programas educacionales, como CEDICE Libertad, aparecen muy bien juzgados por expertos internacionales.</p>
<p>&nbsp;</p>
<p>Actualmente, Venezuela cuenta solamente con 17 centros de pensamiento, mientras que el país vecino, Colombia, cuenta con 40.</p>
<p>&nbsp;</p>
<p>CEDICE se ubica en el puesto # 1 como instituto liberal y #2 en el Índice 2013 para América Latina y ocupa el lugar 128 de los 150 Top Think Tanks en el Mundo.</p>
<p>&nbsp;</p>
<p>CEDICE Libertad además figura con 8 menciones en este ranking del Índice Global de los 150 mejores Think Tanks 2013 entre:</p>
<p>&nbsp;</p>
<ul>
<li>&nbsp;Los 45 institutos más relevantes en Latinoamérica: #02</li>
<li>&nbsp;En el puesto 128 de los mejores 150 del Mundo.</li>
<li>&nbsp;Mejor uso de las redes sociales: #42</li>
<li>&nbsp;Mejor uso del internet para involucrar al público: #36</li>
<li>&nbsp;Con mayor impacto en las políticas públicas (Global): #63</li>
<li>&nbsp;Think tank como mejor desarrollo Internacional: #49</li>
<li>&nbsp;Principales Think Tanks con presupuestos de menos de 5 millones de dólares: #17</li>
<li>&nbsp;Mejores Conferencias: #31</li>
<li>&nbsp;Think Tank de América Latina más seguido en Twitter, con 41.089 seguidores.</li>
</ul>
<p>&nbsp;</p>
<p>Rocio Guijarro, gerente del Centro, manifestó que esta buena noticia en estos momentos para Venezuela es un reconocimiento especialmente porque este año se celebra el 30 aniversario de la institución para el cual se planean una serie de actividades y manifestó que seguirán haciendo su trabajo en pro de del libre mercado, la libertad económica y la defensa de los derechos de propiedad, para contrarrestar el socialismo que solo empobrece a los pueblos.</p>
<p>&nbsp;</p>
<p>El índice incluye centros de diversas tendencias, a nivel mundial, el Instituto Brookings de USA, recibió nuevamente la mayor cantidad de votos.</p>
<p>&nbsp;</p>
<p>El índice completo lo pueden bajar de la página desde <a href="index.php/biblioteca/item/261-2013-global-go-to-think-thank">aquí</a></p>
<p>&nbsp;</p>
<p>Contacto: <span id="cloak56518">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak56518').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy56518 = 'r&#111;c&#105;&#111;c&#101;d&#105;c&#101;' + '&#64;';
 addy56518 = addy56518 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak56518').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy56518 + '\'>' + addy56518+'<\/a>';
 //-->
 </script></p>
<p>Fuente: Rocio Guijarro&nbsp;</p>
<p>Foto: CEDICE Libetad</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/260-liberales-y-liberales">
			&laquo; Liberales y liberales		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/264-relial-felicitaciones-a-los-think-tanks-reconocidos-en-el-índice-“2013-global-go-to-think-tank”">
			RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”, &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/262-cedice-libertad-ocupa-el-puesto-no-1-entre-los-centros-de-estudios-de-libre-mercado-más-influyentes-de-américa-latina#startOfPageId262">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:129:"CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina - Relial";s:11:"description";s:155:"CEDICE Libertad ocupa el puesto No. 1 entro los centros de estudios de libre mercado más influyentes de América Latina, y el No. 2 entre los instituto...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:120:"CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina";s:6:"og:url";s:193:"http://www.relial.org/index.php/productos/archivo/actualidad/item/262-cedice-libertad-ocupa-el-puesto-no-1-entre-los-centros-de-estudios-de-libre-mercado-más-influyentes-de-américa-latina	200";s:8:"og:title";s:129:"CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/5a61d31ed794cb758475f6c89477dfed_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/5a61d31ed794cb758475f6c89477dfed_S.jpg";s:14:"og:description";s:155:"CEDICE Libertad ocupa el puesto No. 1 entro los centros de estudios de libre mercado más influyentes de América Latina, y el No. 2 entre los instituto...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:120:"CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}