<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7386:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId414"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Defaults eran los de antes
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f75f45065e491a3adc61e72a384867bb_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f75f45065e491a3adc61e72a384867bb_XS.jpg" alt="Defaults eran los de antes" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="margin-left: 30px;">La opinión de Martín Simonetta</p>
<p style="margin-left: 30px;">&nbsp;</p>
<p style="margin-left: 30px;">A finales de 1902, las costas de Venezuela fueron bombardeadas por unidades navales de Gran Bretaña y Alemania, a las que se sumaron las de Italia, con el objetivo de exigir el cobro de deudas del gobierno venezolano pendientes con particulares europeos.</p>
<p style="margin-left: 30px;">&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Ante tal situación el dictador venezolano Cipriano Castro –a cargo entre 1899-1908- optó por aceptar las condiciones de los acreedores el 1º de enero de 1903, reconociendo -el 13 de febrero- a través del protocolo de Washington, la justicia de los reclamos europeos. De esta forma, algunas de las deudas se pagaron de inmediato y otras en los años subsiguientes, estableciéndose en garantía hasta el 30 % del ingreso de las aduanas de La Guayra y Puerto Cabello, lo que fue percibido por agentes belgas.</p>
<p>&nbsp;</p>
<p>Ante tal situación, el entonces Ministro de Relaciones Exteriores de nuestro país –casi previendo la crítica situación de la Argentina en las décadas subsiguientes- dirigió una nota con fecha 29 de diciembre de 1902 al ministro argentino en Washington, Martín García Merou, para que éste la presentara al gobierno norteamericano. La nota incluyó lo que más tarde conocería como "Doctrina Drago", cuyo argumento central es que "la deuda pública no puede dar lugar a la intervención armada, ni menos a la ocupación material del suelo de las naciones americanas por una potencia europea".</p>
<p>&nbsp;</p>
<p>¿El default después del default?</p>
<p>&nbsp;</p>
<p>Los tiempos han cambiado. Y la Argentina, a más de un siglo de la mencionada doctrina, ha recorrido un voluminoso camino en materia de cesación de pagos.</p>
<p>&nbsp;</p>
<p>La ausencia de un sistema ejecutable de premios y castigos ha hecho que los gobiernos reciban los beneficios del endeudamiento público mientras los costos sean transferidos a los prestamistas del dinero -a través del no pago, retrasos o quitas-, y a las futuras generaciones de argentinos.</p>
<p>&nbsp;</p>
<p>Ya en el default con acreedores privados que tuvo lugar en el contexto de la crisis 2001-2002, la ciudadanía argentina no percibió al crecimiento del gasto público y al endeudamiento –que se duplicó entre 1997 y el año 2001- como nocivos para la economía, y apoyó la cesación de pagos con alegría. Esto se manifiesta en la forma en que una importante porción adhirió a la declaración de tal default realizada en la Cámara de Diputados en enero del 2001.</p>
<p>&nbsp;</p>
<p>Los problemas fiscales, derivados del incremento del gasto público y los problemas de endeudamiento derivados han sido analizados por el premio Nobel de Economía 1986, James Buchanan. El padre de la teoría de la Elección Pública ha sugerido el establecimiento de restricciones constitucionales como única forma efectiva de limitar el financiamiento descontrolado de los gobiernos y sus consecuencias.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Poco más de una década después, la cercanía de un posible nuevo default con los "fondos buitres" continúa siendo un argumento que goza de gran popularidad, detrás del que subyace la idea de que es el acreedor -supuestamente externo-, y no los gobernantes dispendiosos- la causa de la pauperización que vive el pueblo argentino.</p>
<p>&nbsp;</p>
<p>Fuente: Colaboración del autor,&nbsp;<span style="font-size: 11px;">Martín Simonetta es Director Ejecutivo de Fundación <a href="http://www.atlas.org.ar/">Atlas 1853</a></span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/412-hacia-un-sistema-de-pensiones-justo-y-equitativo">
			&laquo; Hacia un sistema de pensiones justo y equitativo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman">
			El legado de Milton Friedman &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes#startOfPageId414">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:35:"Defaults eran los de antes - Relial";s:11:"description";s:156:"La opinión de Martín Simonetta &amp;nbsp; A finales de 1902, las costas de Venezuela fueron bombardeadas por unidades navales de Gran Bretaña y Aleman...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:26:"Defaults eran los de antes";s:6:"og:url";s:93:"http://www.relial.org/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes";s:8:"og:title";s:35:"Defaults eran los de antes - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f75f45065e491a3adc61e72a384867bb_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f75f45065e491a3adc61e72a384867bb_S.jpg";s:14:"og:description";s:160:"La opinión de Martín Simonetta &amp;amp;nbsp; A finales de 1902, las costas de Venezuela fueron bombardeadas por unidades navales de Gran Bretaña y Aleman...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:26:"Defaults eran los de antes";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}