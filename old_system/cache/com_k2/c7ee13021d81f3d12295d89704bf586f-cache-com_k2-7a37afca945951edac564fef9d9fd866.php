<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9762:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId489"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/2039f9c06c46d5e4b5d871c0089d076b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/2039f9c06c46d5e4b5d871c0089d076b_XS.jpg" alt="Preocupa a la regi&oacute;n el doble est&aacute;ndar en DDHH y el boom del autoritarismo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: justify;">Por Carlos A. Manfroni</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">En un seminario organizado en Lima por la Red Liberal de América Latina y la Fundación Friedrich Naumann, representantes de organizaciones de diversos países de la región manifestaron su preocupación por el doble estándar en derechos humanos que se puede ver en países como Argentina, Bolivia o Venezuela y el florecimiento de los autoritarismos en la región.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p style="text-align: justify;">Por Argentina, concurrió Carlos Manfroni, en representación de la Fundación Libertad y Progreso, quien ilustró con estadísticas, documentos y filmaciones el doble estándar empleado por los jueces argentinos a la hora de juzgar los hechos de terrorismo de la guerrilla de los '70 y las violaciones cometidas por militares en aquella época, así como también de qué manera la imposición de ese doble estándar sirvió para quebrantar la independencia del Poder Judicial. "Al amparo de un tema tabú acerca del que pocos se atreven a discutir el Gobierno comenzó a ejercer su dominio sobre los jueces", explicó.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Manfroni habló de la situación de desamparo en la que están los familiares de las víctimas de la guerrilla, que provocó más de 1.000 muertos civiles, conforme a las estadísticas del CELTYV. También alertó sobre el avance del terrorismo islámico en América latina y de qué manera la inversión de los países de Medio Oriente en Venezuela, Nicaragua, Ecuador y Bolivia ayuda a sostener a los autoritarismos.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Por su lado, Rubén Darío Cuellar, coordinador del Observatorio de Derechos Humanos de la Fundación Nueva Democracia, de Bolivia, recordó que el lema de Evo Morales había sido "ni un solo muerto más", pero que ya hay 65 muertos por violencia directa del gobierno boliviano, en ejecuciones disfrazadas.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">El experto de Bolivia dijo también que antes de la llegada del actual presidente, había 6.000 detenidos en Bolivia y que ahora hay 16.000, la mayoría con prisión preventiva. Un fiscal boliviano que se refugió en Brasil reveló de qué modo se armaban las operaciones para detener opositores; lo declaró ante la justicia brasileña, pero su testimonio no fue aceptado por no estar legalizado por el cónsul boliviano, quien a su vez se niega a hacerlo. Cuellar contó también que un opositor acusado de terrorismo en ese país estuvo en prisión preventiva durante seis años y la audiencia para considerar la detención se suspendió 91 veces, hasta que fue finalmente liberado por falta de pruebas.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">En tanto el director ejecutivo del Observatorio de Derechos y Justicia de Ecuador y ex magistrado del Tribunal Constitucional, Enrique Herrería, contó que en su país, cuando el presidente se refiere en forma crítica a una persona, a las 24 horas, los fiscales inician una causa penal contra el que fue señalado. Herrería mostró una filmación en la cual Rafael Correa declaró, públicamente, que no sólo es el presidente del Poder Ejecutivo, sino también del Poder Legislativo y del Poder Judicial.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Por su parte, el representante del Centro de Divulgación del Conocimiento Económico para la Libertad, de Venezuela, comenzó pidiendo disculpas por los males que su país provocó a la región y destacó que el conocido dirigente opositor Leopoldo López, actualmente encarcelado, fue responsabilizado por tres muertes que ocurrieron durante las marchas de protestas, con la paradoja de que esas tres personas fueron justamente asesinadas por la Policía de Seguridad del Estado. Recordó que el año ya se registraron 34 muertos y en el anterior fueron 54.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Seguidamente habló Francisco Quezada, representante del Centro de Investigaciones Económicas Nacionales de Guatemala, quien explicó de qué modo se ha logrado que la izquierda ocupe todos los cargos académicos y sea la única voz escuchada en materia de derechos humanos.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Por su lado, el enviado por el Observatorio de Política y Estrategia en América Latina, de Colombia, Andrés Molano Rojas, expresó su preocupación por la iniciativa del ex presidente Ernesto Samper —en su momento acusado de recibir dinero del narcotráfico—, en orden a la creación de una Corte Penal Sudamericana.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Luego, Francisco Rivas, muy conocido en México por su tarea al frente del Observatorio Nacional Ciudadano, dijo que si bien en su país la persecución oficial es más sutil, la cantidad de muertos y desaparecidos es elevadísima y él mismo recibe periódicamente amenazas de muerte.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Al finalizar, la directora regional de la fundación Friedrich Naumann, Birgit Lamm, y el moderador del encuentro, Mario Brenes, exhortaron a los expositores a seguir trabajando en red y a aumentar su exposición, a fin de potenciar su voz en el continente en defensa de los derechos y las libertades de todos.</p>
<p style="text-align: justify;">&nbsp;</p>
<p>Autor: Carlos A. Manfroni</p>
<p>Fuente: <a href="http://www.libertadyprogresoonline.org">www.libertadyprogresoonline.org</a></p>
<p>Foto: Libertad y Progreso</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/488-educación-y-libertad-garantías-para-una-sociedad-armónica">
			&laquo; Educación y Libertad, garantías para una sociedad armónica		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/491-relial-en-el-world-economic-forum">
			RELIAL en el World Economic Forum &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/489-preocupa-a-la-región-el-doble-estándar-en-ddhh-y-el-boom-del-autoritarismo#startOfPageId489">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:85:"Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo - Relial";s:11:"description";s:155:"Por Carlos A. Manfroni &amp;nbsp; En un seminario organizado en Lima por la Red Liberal de América Latina y la Fundación Friedrich Naumann, representa...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:76:"Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo";s:6:"og:url";s:146:"http://www.relial.org/index.php/productos/archivo/actualidad/item/489-preocupa-a-la-región-el-doble-estándar-en-ddhh-y-el-boom-del-autoritarismo";s:8:"og:title";s:85:"Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/2039f9c06c46d5e4b5d871c0089d076b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/2039f9c06c46d5e4b5d871c0089d076b_S.jpg";s:14:"og:description";s:159:"Por Carlos A. Manfroni &amp;amp;nbsp; En un seminario organizado en Lima por la Red Liberal de América Latina y la Fundación Friedrich Naumann, representa...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:76:"Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}