<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7355:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId84"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Cuenta pública, no perdamos su sentido
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><br /><img style="margin: 2px; border: 1px solid #f2f2f2; float: left;" src="images/logos/lyd%20nuevo.jpg" alt="lyd nuevo" width="120" height="120" /></p>
<p>A continuación reproducimos la columna de Cecilia Cifuentes, investigadora del Programa Económico de LyD, publicada en La Tercera</p>
<p>&nbsp;</p>
<p>Uno de los temas centrales de la última cuenta pública del Presidente Piñera fue el proceso de desarrollo chileno, que números más números menos, sin duda ha mostrado una mayor velocidad en los últimos tres años.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>No obstante, efectivamente seremos un país más desarrollado cuando los comentarios posteriores al discurso tengan un sentido más constructivo. Esto porque tanto este discurso como otros buscan mostrar los avances y metas de un gobierno, de la mejor forma posible, y es difícil pensar que fueran algo distinto a lo que son. Sin embargo, gran parte de la oposición no pierde la oportunidad de criticar y desconocer los avances que efectivamente existen, dando pie de alguna forma, a los incidentes posteriores, que parecen tan fuera de lugar en el día que se recuerda la gesta heroica de un patriota, que sin duda tuvo a Chile en primer lugar. Es positivo también que en un discurso de esta naturaleza, junto con los logros se planteen los desafíos, de tal forma de reconocer la agenda que siempre estará pendiente.</p>
<p>Sinteticemos algunos logros concretos de este gobierno; en cuanto a la aspiración al desarrollo, la cuenta mostró en forma correcta que no se trata sólo de PIB per capita, ya que inevitablemente los mayores ingresos están unidos a mejor calidad de vida; más vacaciones, más cultura, más entretención, más deporte y más lectura, entre otros. En otro aspecto, el avance en la reconstrucción, propio de un país desarrollado, es motivo de legítimo orgullo. Es evidente que queda mucho por avanzar, y probablemente todos los países del mundo así lo sientan, pero igualmente claro es el hecho de que Chile está mejor ahora que a inicios del año 2010, y a la hora de las evaluaciones el gobierno puede mostrar un grado satisfactorio de cumplimiento de su programa.</p>
<p>Es también un hecho notable que los principales logros de este gobierno se estén dando en materia laboral, no sólo en términos de creación de puestos de trabajo, sino también respecto a la calidad de ese empleo, en un contexto de remuneraciones reales que han crecido a un ritmo muy favorable. Notable porque efectivamente en el campo legislativo, salvo la ampliación del post natal, no se ha realizado ninguna modificación laboral de importancia, lo que muestra que no existe mejor política laboral que el pleno empleo, ya que logra empoderar a todos los trabajadores, y no sólo a los sectores formales y con empleos más protegidos.</p>
<p>Pero aún quedan desafíos importantes; al respecto probablemente lo más destacado sea en el tema de educación preescolar, ya que sin duda es el camino más efectivo para resolver los problemas de desigualdad, aunque con resultados que se ven en el mediano plazo. Igualmente parece muy positivo el anuncio de la modificación de la política de capacitación laboral, ya que las cifras son elocuentes en mostrar que las brechas de ingreso dependen en un grado muy elevado del capital humano de los trabajadores. Una política de capacitación laboral mejor focalizada y efectiva es la herramienta más poderosa para lograr mayor equidad en el corto plazo.</p>
<p>Los déficits también son abordados en el discurso, siendo el tema energético probablemente el más significativo, y en el cual se percibe una cierta falta de liderazgo en el propio gobierno para lograr los necesarios avances. Se pretende suplir esta falla, pero probablemente la falta de un consenso nacional haga muy difícil la tarea en este campo no solo para este gobierno sino también para el que venga.</p>
<p>En definitiva, el gobierno puede estar muy satisfecho de su gestión ya que es posible mostrar avances evidentes, de ahí la importancia de quien asuma en el próximo período pueda ser un agente favorable para continuar en esta línea.</p>
<p>Fuente: <a href="http://www.lyd.com/">Libertad y Desarrollo</a>, Chile</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/85-el-papel-higiénico-y-el-socialismo">
			El papel higiénico y el socialismo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/84-cuenta-pública-no-perdamos-su-sentido#startOfPageId84">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:48:"Cuenta pública, no perdamos su sentido - Relial";s:11:"description";s:155:"A continuación reproducimos la columna de Cecilia Cifuentes, investigadora del Programa Económico de LyD, publicada en La Tercera &amp;nbsp; Uno de lo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:39:"Cuenta pública, no perdamos su sentido";s:6:"og:url";s:107:"http://www.relial.org/index.php/productos/archivo/actualidad/item/84-cuenta-pública-no-perdamos-su-sentido";s:8:"og:title";s:48:"Cuenta pública, no perdamos su sentido - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:159:"A continuación reproducimos la columna de Cecilia Cifuentes, investigadora del Programa Económico de LyD, publicada en La Tercera &amp;amp;nbsp; Uno de lo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:39:"Cuenta pública, no perdamos su sentido";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}