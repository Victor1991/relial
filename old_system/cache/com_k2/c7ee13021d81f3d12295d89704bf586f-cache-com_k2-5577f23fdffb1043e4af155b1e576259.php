<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8318:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId302"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El legado de Hugo Chávez
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/274936c4b649c88ffad7944bfc7a744a_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/274936c4b649c88ffad7944bfc7a744a_XS.jpg" alt="El legado de Hugo Ch&aacute;vez" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>¿Cuál es el legado de Hugo Chávez? Al fin y al cabo, gobernó a su antojo durante 14 años (1999-2013). El periodo más largo de la historia de Venezuela, exceptuado Juan Vicente Gómez (1908-1935), otro militar de mano dura que se murió mandando.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Digámoslo rápidamente: la herencia que les dejó a sus atribulados compatriotas fue la cubanización de Venezuela.</p>
<p>&nbsp;</p>
<p>El 5 de marzo del 2013 se anunció la muerte de Hugo Chávez. Se cumplían 60 años exactos de la de Stalin. Chávez estaba clínicamente muerto desde mucho antes. Tal vez desde el 29 de diciembre anterior, cuando lo operaron en La Habana, pero lo mantuvieron artificialmente "vivo", con el encefalograma plano, conectado a máquinas que estimulaban los latidos de su inútil corazón.</p>
<p>&nbsp;</p>
<p>Durante ese periodo, el gobierno cubano se dedicó febrilmente a organizar la transmisión de la autoridad en Venezuela. No podían desconectarlo hasta tanto no tuvieran todas las riendas del poder en las manos. Los Castro defendían subsidios por trece mil millones de dólares anuales, incluidos unos cien mil barriles diarios de petróleo, de los cuales Cuba reexporta la mitad.</p>
<p>&nbsp;</p>
<p>Era importante prolongar el control del rico país sudamericano mientras se pudiese. En Venezuela se cumplía el destino trágico de las colonias: nutrir a la Metrópolis. más o menos como los insectos cautivos alimentan a las tarántulas que los van devorando lentamente.</p>
<p>&nbsp;</p>
<p>Lo extraño, en este caso, es que el insecto es mucho mayor que la tarántula. ¿Cómo una pequeña, improductiva y empobrecida Isla caribeña, anclada en un herrumbroso pasado soviético borrado de la historia, puede controlar a una nación mucho más grande, moderna, rica, poblada y educada, sin que siquiera haya existido una previa guerra de conquista? La clave de esa anomalía está en Chávez.</p>
<p>&nbsp;</p>
<p>El axioma funciona así: Hugo Chávez se convirtió en el caudillo de Venezuela. Un caudillo es alguien que voluntariamente o por la fuerza asume el liderazgo para dirigir a una sociedad en la dirección que él decide. Entre las prerrogativas del caudillo está la de transferir su autoridad a otra persona o entidad. Al borde de la tumba, presionado por los Castro, Chávez, admitió la designación de Nicolás Maduro, sugerida por La Habana. Ergo, de facto, Cuba es el gran poder en Venezuela.</p>
<p>&nbsp;</p>
<p>Este vasallaje contranatura comenzó en 1994, cuando Hugo Chávez conoció a Fidel Castro y el cubano lo sedujo, pero se selló totalmente a partir de abril del 2002, cuando el ejército venezolano le dio un golpe a Chávez y lo obligó a renunciar, al menos durante 48 horas.</p>
<p>&nbsp;</p>
<p>A partir de ese episodio, Chávez no creyó nunca más en sus compatriotas, políticos o militares, y se entregó totalmente en las manos de "los cubanos". ¿Qué le daban los cubanos? Una visión, un método y una misión, pero, sobre todo, informes de inteligencia sobre políticos, periodistas y militares. Detectaban o magnificaban deslealtades y se las revelaban. La información era poder. Cuba reunía y entregaba toda la información, subrayando los peligros para que Chávez estuviera eternamente agradecido.</p>
<p>&nbsp;</p>
<p>Nadie conocía mejor los secretos de las tribus chavistas, muchas de ellas mal avenidas, que "los cubanos". Lo conocían todo: los delitos de los narcogenerales, los robos de la boliburguesía, las infidelidades de los supuestos aliados, la conducta íntima de los jefes, sus familias, sus hijos. Esa información podía destrozar a cualquiera que se les opusiera.</p>
<p>&nbsp;</p>
<p>Ese poder siniestro convertía a La Habana en el único factor aglutinante. Las tribus chavistas le temían. Las sujetaba firmemente por la entrepierna. Como en los versos de Borges, los unía el espanto. Cuando alguien se rebelaba contra su autoridad, le entregaban el dossier de sus inmundicias o le deslizaban un par de datos. No hacía falta más.</p>
<p>&nbsp;</p>
<p>Cuando desconectaron a Chávez, ya Maduro había sido ungido, violando la Constitución. Era el hombre de los cubanos. Se había graduado en La Habana en la Escuela del Partido. No era un político, ni un militar, ni una figura de peso. Era un monigote al servicio de Cuba. Era el legado de Chávez.</p>
<p>&nbsp;</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: Texto proporcionado amablemente por Carlos Alberto Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/297-por-qué-perdió-rafael-correa">
			&laquo; Por qué perdió Rafael Correa		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/303-milton-friedman-y-la-verdad-histórica">
			Milton Friedman y la verdad histórica &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/302-el-legado-de-hugo-chávez#startOfPageId302">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:34:"El legado de Hugo Chávez - Relial";s:11:"description";s:159:"En la opinión de Carlos Alberto Montaner &amp;nbsp; ¿Cuál es el legado de Hugo Chávez? Al fin y al cabo, gobernó a su antojo durante 14 años (1999-201...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:25:"El legado de Hugo Chávez";s:6:"og:url";s:92:"http://www.relial.org/index.php/productos/archivo/opinion/item/302-el-legado-de-hugo-chávez";s:8:"og:title";s:34:"El legado de Hugo Chávez - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/274936c4b649c88ffad7944bfc7a744a_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/274936c4b649c88ffad7944bfc7a744a_S.jpg";s:14:"og:description";s:163:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; ¿Cuál es el legado de Hugo Chávez? Al fin y al cabo, gobernó a su antojo durante 14 años (1999-201...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:25:"El legado de Hugo Chávez";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}