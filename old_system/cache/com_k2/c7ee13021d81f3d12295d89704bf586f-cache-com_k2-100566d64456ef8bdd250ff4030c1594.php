<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6050:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId363"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/3dcbd8056f504532259e733a4b9e2ae3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/3dcbd8056f504532259e733a4b9e2ae3_XS.jpg" alt="Venezuela: Fundaci&oacute;n HACER lanza Campa&ntilde;a de Solidaridad Internacional con la sociedad civil venezolana" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Haciendo uso de la etiqueta #HACERlibreVZ, la Fundación HACER lanzó una campaña de Solidaridad Internacional con la sociedad civil venezolana, que ha venido siendo víctima durante tres meses, de la brutal represión por parte del régimen de Nicolás Maduro.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>La campaña esta siendo viralizada exitosamente a través de Twitter, Facebook e Instagram. De la misma, participan mujeres jóvenes de toda América Latina, que apoyan la restauración de las libertades del pueblo venezolano, enviándonos sus fotos para ser difundidas junto a frases de grandes pensadores y personajes de la historia.</p>
<p>&nbsp;</p>
<p>En esta oportunidad, la campaña solicitó el apoyo voluntario de jóvenes latinoamericanas, en solidaridad con sus pares venezolanas, las que lideradas por la Diputada María Corina Machado, se han convertido en importantes protagonistas de los esfuerzos por liberar a Venezuela del yugo Cubano.</p>
<p>&nbsp;</p>
<p>Solicitamos entonces el apoyo de todos aquellos que aprecian la libertad y la democracia alrededor del continente de la siguiente manera:</p>
<p>&nbsp;</p>
<p>* Poniéndole "me gusta" a las imágenes que forman parte de la campaña.</p>
<p>&nbsp;</p>
<p>* Compartiendo las publicaciones que contienen las imágenes con sus contactos.</p>
<p>&nbsp;</p>
<p>En Twitter: https://twitter.com/ebiglione</p>
<p>En Facebook: https://www.facebook.com/HACERDC</p>
<p>En Instagram: http://instagram.com/hacerdc</p>
<p>&nbsp;</p>
<p>La Fundación HACER es una organización sin fines de lucro abocada a la promoción del estudio de temas vinculados a países Latinoamericanos y a la comunidad hispana de los Estados Unidos; desde una perspectiva basada en el respeto de valores de libertad personal y económica, gobierno limitado y responsabilidad individual. HACER cuenta con sede en la ciudad de Washington DC y equipos de trabajo en Argentina, Bolivia, Colombia, Uruguay y Costa Rica.</p>
<p>&nbsp;</p>
<p>Fuente: HACER</p>
<p>Foto: HACER</p>
<p>Autor: HACER</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/357-impacto-en-los-medios-del-indice-de-calidad-institucional">
			&laquo; Impacto en los medios del Indice de Calidad Institucional		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/374-relial-y-su-trabajo-en-derechos-humanos">
			RELIAL y su trabajo en Derechos Humanos &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/363-venezuela-fundación-hacer-lanza-campaña-de-solidaridad-internacional-con-la-sociedad-civil-venezolana#startOfPageId363">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:113:"Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana - Relial";s:11:"description";s:155:"Haciendo uso de la etiqueta #HACERlibreVZ, la Fundación HACER lanzó una campaña de Solidaridad Internacional con la sociedad civil venezolana, que ha...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:104:"Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana";s:6:"og:url";s:169:"http://relial.org/index.php/productos/archivo/actualidad/item/363-venezuela-fundación-hacer-lanza-campaña-de-solidaridad-internacional-con-la-sociedad-civil-venezolana";s:8:"og:title";s:113:"Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/3dcbd8056f504532259e733a4b9e2ae3_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/3dcbd8056f504532259e733a4b9e2ae3_S.jpg";s:14:"og:description";s:155:"Haciendo uso de la etiqueta #HACERlibreVZ, la Fundación HACER lanzó una campaña de Solidaridad Internacional con la sociedad civil venezolana, que ha...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:104:"Venezuela: Fundación HACER lanza Campaña de Solidaridad Internacional con la sociedad civil venezolana";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}