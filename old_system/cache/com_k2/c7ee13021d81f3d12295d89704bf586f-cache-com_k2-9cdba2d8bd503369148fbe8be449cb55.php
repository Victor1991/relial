<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9770:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId495"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Indignación ciudadana en Guatemala se refleja en el aumento de protestas
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/3946cc5a2ed843c2c9fca0b4efcd28ba_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/3946cc5a2ed843c2c9fca0b4efcd28ba_XS.jpg" alt="Indignaci&oacute;n ciudadana en Guatemala se refleja en el aumento de protestas" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Los guatemaltecos están hartos no solo de la corrupción, sino también por la candidatura de Manuel Baldizón, que comprometería el futuro de la tierra del quetzal.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Continúa el clima de agitación en Guatemala, en un movimiento social espontáneo que ya lleva ahora más de un mes. Al escándalo de defraudación en las aduanas, que involucró directamente al hoy prófugo secretario privado de la vicepresidenta, sucedió la renuncia de esta, Roxana Baldetti, sin duda la segunda figura del Gobierno, y el de tres ministros del gabinete, entre ellos Mauricio López Bonilla en la importante cartera de gobernación. Luego, para reforzar el cuadro, se ha confirmado lo que ya muchos sabíamos: en el instituto de seguridad social, el IGSS, se ha denunciado un contrato fraudulento para atender a los pacientes de diálisis. La escasa idoneidad de la empresa contratante ha llevado a la muerte de 13 enfermos y son ahora decenas los detenidos y varios los prófugos.</p>
<p>&nbsp;</p>
<p>La indignación ciudadana en Guatemala no cesa: se presume que el propio presidente está involucrado en tramas de corrupción que implican a muchos funcionarios de todo rango y, por eso, crecen las demandas para que Otto Pérez Molina renuncie a la primera magistratura. Pero el clamor es más amplio, intenso como nunca en las redes sociales y se dirige ahora, sobre todo, contra Manuel Baldizón, un candidato que promete abiertamente toda clase de dádivas y regalos a la gente si sale elegido en los comicios generales a desarrollarse el próximo 6 de septiembre.</p>
<p>&nbsp;</p>
<p>Baldizón, con amplios recursos financieros cuya procedencia se cuestiona y primero hasta abril en las encuestas, inició su campaña con el eslogan "Le toca", aludiendo al hecho de que en Guatemala, en las últimas ocasiones, el candidato que resulta segundo en una elección gana la siguiente. Como él quedó de segundo en 2011 detrás del actual presidente –que a su vez había quedado en segundo lugar en 2007–, ideó ese motivo publicitario que parecía prometerle seguros resultados.</p>
<p>&nbsp;</p>
<p>Pero la gente reaccionó en su contra con inusitada fuerza, en un movimiento espontáneo que no tiene comparación con nada conocido. "No te toca, Baldizón", es la consigna que ahora recorre todas las redes sociales, que enarbolan ciudadanos comunes no ligados a ningún partido en los mítines del candidato y que recorre el país con la fuerza de un huracán.</p>
<p>&nbsp;</p>
<p>"La gente común quiere renovar un sistema político que se caracteriza por el abuso, el trasiego de fondos y las relaciones con el narcotráfico".</p>
<p>&nbsp;</p>
<p>Baldizón es una figura pintoresca, un hombre desvergonzado al que se asocia con esquemas de corrupción y que plagió su tesis doctoral y un libro que publicó el año pasado, copiándolos abiertamente de textos que encontró en internet. Controla hoy varios medios de comunicación y sus seguidores, casi siempre pagados, muestran una actitud agresiva y poco considerada contra los demás partidos. Por eso ha atraído la animadversión de una amplia franja de la ciudadanía, que piensa que, de llegar al poder, la corrupción se hará incontenible y quebrará financieramente el Estado, con las muchas promesas que ha hecho y que en alguna medida tendrá que cumplir.</p>
<p>&nbsp;</p>
<p>Pero el movimiento social que lo rechaza no se dirige solo contra él y su millonaria campaña: por todo el país personas de variada condición han salido a borrar, con brochas y pintura, la propaganda ilegal que todos los partidos pintan en piedras, árboles y postes de alumbrado, en una competencia que deteriora el ambiente y el maravilloso paisaje de Guatemala. La gente, la gente común podríamos decir, está en pie de lucha y quiere renovar a fondo un sistema político que se caracteriza hoy por el abuso, el trasiego de fondos y las relaciones con el narcotráfico.</p>
<p>&nbsp;</p>
<p>Hay motivos, pues, para el optimismo. Pero los hay, no cabe duda, también para la preocupación. Crece el clamor, como decíamos, para que el presidente Otto Pérez renuncie a su cargo, pues todo apunta a que él, también, está involucrado en la generalizada corrupción. Pero esa demanda –comprensible sin duda–, se acompaña en muchos casos con un reclamo para que no se realicen las elecciones generales y se convoque, en cambio, a una asamblea constituyente que dicte nuevas leyes para renovar por completo el sistema político actual.</p>
<p>&nbsp;</p>
<p>Son, por lo general, organizaciones de extrema izquierda las que demandan esta ruptura, que significaría sin duda un salto en el vacío similar al que se produjo en Venezuela, Bolivia y otros países, con la terribles consecuencias que todos conocemos. Algunos analistas, ingenuamente, respaldan esta idea, sin valorar la importancia que tiene para cualquier nación mantener un mínimo respeto a las instituciones.</p>
<p>&nbsp;</p>
<p>La situación es dinámica y cambia día a día, por lo que resulta difícil pronosticar su desenlace. Pero Guatemala ha despertado del letargo político en que se hallaba y ahora muestra una decisión y un deseo de renovación que –pese a los riesgos– resulta alentadora para su futuro y para el del resto de América Latina, aquejada de los mismos males que padece la tierra del quetzal.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Carlos Sabino,&nbsp;Sociólogo, escritor y profesor universitario, Sabino es director de programas de máster y doctorado en Historia de la <a href="http://www.ufm.edu/">Universidad Francisco Marroquín</a> de Guatemala.</p>
<p>&nbsp;</p>
<p>Fuente: <a href="http://es.panampost.com/carlos-sabino/2015/05/26/indignacion-ciudadana-en-guatemala-se-refleja-en-el-aumento-de-protestas/">Panampost</a></p>
<p>&nbsp;</p>
<p>Foto: RELIAL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/494-una-mirada-liberal-compendio-cumbre-de-las-américas">
			&laquo; Una mirada liberal, compendio "Cumbre de las Américas"		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-éxito">
			Chile, de espaldas al éxito &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/495-indignación-ciudadana-en-guatemala-se-refleja-en-el-aumento-de-protestas#startOfPageId495">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:82:"Indignación ciudadana en Guatemala se refleja en el aumento de protestas - Relial";s:11:"description";s:157:"Los guatemaltecos están hartos no solo de la corrupción, sino también por la candidatura de Manuel Baldizón, que comprometería el futuro de la tierra...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:73:"Indignación ciudadana en Guatemala se refleja en el aumento de protestas";s:6:"og:url";s:143:"http://www.relial.org/index.php/productos/archivo/actualidad/item/495-indignación-ciudadana-en-guatemala-se-refleja-en-el-aumento-de-protestas";s:8:"og:title";s:82:"Indignación ciudadana en Guatemala se refleja en el aumento de protestas - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/3946cc5a2ed843c2c9fca0b4efcd28ba_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/3946cc5a2ed843c2c9fca0b4efcd28ba_S.jpg";s:14:"og:description";s:157:"Los guatemaltecos están hartos no solo de la corrupción, sino también por la candidatura de Manuel Baldizón, que comprometería el futuro de la tierra...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:73:"Indignación ciudadana en Guatemala se refleja en el aumento de protestas";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}