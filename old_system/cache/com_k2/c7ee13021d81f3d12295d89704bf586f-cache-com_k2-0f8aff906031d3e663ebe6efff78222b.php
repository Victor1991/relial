<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7761:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId288"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/d383d2a7f18b38f50f531c6f6759cc5a_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/d383d2a7f18b38f50f531c6f6759cc5a_XS.jpg" alt="CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represi&oacute;n que atentan contra la paz y la Democracia en el pa&iacute;s" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: center;"><br />CEDICE Libertad</p>
<p style="text-align: center;">Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>1. Los venezolanos hemos sido espectadores y participantes de una semana muy conflictiva. Los estudiantes han tomado las calles para reclamar más seguridad ciudadana y que el gobierno se aboque a resolver problemas esenciales que hasta la fecha han estado crónicamente desatendidos. La respuesta del gobierno fue el exceso de represión y la detención de un grupo de ellos a quienes se les violaron las garantías más esenciales del derecho a la defensa, el debido proceso y la presunción de inocencia</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>2. Las manifestaciones han sido reprimidas con violencia. El régimen no está respetando el derecho a la protesta y se ha apoyado en la acción de grupos paramilitares, los llamados colectivos, que se presentan como parte del brazo armado de "la revolución armada".</p>
<p>&nbsp;</p>
<p>3. El régimen permite la acción impune de estos colectivos que recorren calles y avenidas de las principales ciudades para amedrentar y posiblemente infiltrar las manifestaciones estudiantiles para generar violencia y daños a la propiedad.</p>
<p>&nbsp;</p>
<p>4. El régimen, de nuevo sin respetar el debido proceso, ha comenzado una persecución contra los líderes de la oposición. A Leopoldo López lo apresaron, a la diputada María Corina Machado le están queriendo abrir un proceso para allanarle la inmunidad parlamentaria y al resto de los líderes de la Mesa de la Unidad Democrática los amenazan con costos y sanciones difusas.</p>
<p>&nbsp;</p>
<p>5. Mientras todo esto ocurre los problemas del país continúan agravándose. La economía se sigue deteriorando y la inseguridad sigue asolando a los ciudadanos. Estas manifestaciones expresan el descontento por un orden social y económico que niega libertades y derechos mientras conduce con ineficiencia y mala fe los destinos del país. Estas manifestaciones no son las causantes de los problemas sino un reclamo porque los problemas se agravan sin que el régimen quiera y pueda resolverlos.</p>
<p>&nbsp;</p>
<p>6. Siempre hemos dicho que el pésimo desempeño de la gestión pública iba a traer problemas de gobernabilidad y conflictividad social. Estamos hoy frente a la constatación de nuestras previsiones. El régimen sigue sin darse por aludido, intensificando la crisis, hundiendo a la economía en más controles, tratando los problemas económicos sin respetar los derechos de propiedad, asumiendo como inexorable el tránsito hacia el comunismo, a pesar de la explicita oposición de los venezolanos, y negando a los demócratas el derecho a la disidencia.</p>
<p>&nbsp;</p>
<p>7. CEDICE- Libertad exige respeto por los derechos de los venezolanos, el cese de la represión desproporcionada y la criminalización de la disidencia. El régimen debe garantizar las condiciones de respeto, reconocimiento y libertades que permita el concurso de todos los ciudadanos en la solución de los problemas del país. El régimen debe oír lo que los venezolanos le están exigiendo: Que cese definitivamente una forma de hacer gobierno que se ha deslindado del pacto constitucional y quiere imponer por la fuerza un socialismo que nadie quiere y que el gobierno no puede seguir sosteniendo a través de esquemas populistas que no garantizan otra cosa que el quiebre del país por inflación, endeudamiento creciente, escasez y desempleo.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/73-violencia-en-venezuela">
			&laquo; Violencia en Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/291-comunicado-de-fundación-internacional-para-la-libertad">
			Comunicado de Fundación Internacional para la Libertad &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/288-cedice-libertad-ante-los-atentados-a-las-libertades-en-venezuela-repudiamos-la-violencia-y-la-represión-que-atentan-contra-la-paz-y-la-democracia-en-el-país#startOfPageId288">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:169:"CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país - Relial";s:11:"description";s:153:"CEDICE Libertad Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:160:"CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país";s:6:"og:url";s:229:"http://www.relial.org/index.php/productos/archivo/comunicados/item/288-cedice-libertad-ante-los-atentados-a-las-libertades-en-venezuela-repudiamos-la-violencia-y-la-represión-que-atentan-contra-la-paz-y-la-democracia-en-el-país";s:8:"og:title";s:169:"CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/d383d2a7f18b38f50f531c6f6759cc5a_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/d383d2a7f18b38f50f531c6f6759cc5a_S.jpg";s:14:"og:description";s:153:"CEDICE Libertad Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:160:"CEDICE Libertad  Ante los atentados a las Libertades en Venezuela, repudiamos la violencia y la represión que atentan contra la paz y la Democracia en el país";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}