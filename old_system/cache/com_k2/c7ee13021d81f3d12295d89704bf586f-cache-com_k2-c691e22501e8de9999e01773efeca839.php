<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9050:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId257"></span>

<div id="k2Container" class="itemView itemIsFeatured">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La CELAC contra la Carta Democrática Interamericana
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/3409e45349ec9f6b3397bfe10e87a1d0_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/3409e45349ec9f6b3397bfe10e87a1d0_XS.jpg" alt="La CELAC contra la Carta Democr&aacute;tica Interamericana" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<div>La opinión de Carlos Alberto Montaner</div>
<div>&nbsp;</div>
<div>El general Raúl Castro es el presidente pro tempore de la CELAC y todos han ido a La Habana, como los ratones tras la flauta de Hamelin, a celebrar una segunda cumbre.</div>
<div>&nbsp;</div>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>¿A qué juegan los gobiernos de América Latina? Aparentemente, el primer objetivo del organismo, según declararan en su documento fundacional, es: “Reafirmar que la preservación de la democracia y de los valores democráticos, la vigencia de las instituciones y el Estado de Derecho, el compromiso con el respeto y la plena vigencia de todos los derechos humanos para todos, son objetivos esenciales de nuestros países”.</div>
<div>&nbsp;</div>
<div><span style="font-size: 11px;">¿Qué entienden esta gente por democracia? Cuba, como les corresponde a los países desovados por la extinta URSS, es una vieja dictadura unipartidista de más de medio siglo, en la que no existen libertades individuales, ni se respetan los derechos humanos. Mientras se celebra la CELAC, la policía política acosa y aporrea a las Damas de Blanco y a los demócratas de la oposición que se atreven a protestar. ¿Alguien lo ignora?</span></div>
<div>&nbsp;</div>
<div><span style="font-size: 11px;">Raúl y su tropa estalinista no lo ocultan. Son brutal y orgullosamente francos. Tienen coartadas legales para fusilar o encarcelar. Defienden paladinamente ese modo de estabular a la sociedad y afirman que se trata del sistema más abierto, democrático y solidario de la historia. Ni siquiera admiten que torturan a los disidentes. Los opositores no son personas: son gusanos, escoria extirpable a culatazos por oponerse a la felicidad del pueblo y querer entregarle el país al imperialismo yanqui.&nbsp;</span></div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>No hay una violación flagrante de las reglas. Las reglas lo permiten. No hay que “desaparecer” a los enemigos. Se les machaca públicamente. La Constitución, calcada del modelo soviético, le concede al Partido Comunista la facultad en exclusiva de organizar a la sociedad a su antojo. Ese bodrio legal ha sido refrendado por la inmensa mayoría. Los cubanos, como los norcoreanos o cualquier ciudadano aterrorizado, votan lo que les pongan delante mientras sueñan con una balsa. Todo y todos se subordinan a los fines del marxismo-leninismo y se prohíbe cualquier conducta que contradiga estos principios. El pasado, el presente y el futuro están atados y bien atados.</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Y hay elecciones. Cada cierto tiempo, la dictadura, como sucedía en el bloque del Este en Europa, realiza unos comicios muy controlados para legitimar en el poder a unas autoridades que sirven como correa de transmisión a las iniciativas del Castro que esté al frente del manicomio cubano. Son los apparatchiks. Es la nomenclatura obediente y memoriosa. Un orfeón asombrosamente afinado que canta a capella las consignas del Partido.</div>
<div>&nbsp;</div>
<div><span style="font-size: 11px;">Como era evidente que los comunistas habían construido un modelo político distinto (el del totalitarismo marxista-leninista), y reclamaban el derecho a una denominación de origen diferente, los defensores de la democracia liberal definieron el sistema político que ellos proponían en un documento vinculante llamado Carta Democrática Interamericana, firmado en Lima el 11 de septiembre de 2001.</span></div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Ahí están todos los elementos de fondo para el ejercicio real de la democracia republicana: elecciones libres y plurales, separación de poderes, libertades individuales, incluidas la de prensa y asociación, transparencia, neutralidad del Estado de Derecho, respeto, tolerancia. Era exactamente la antítesis del modelo impuesto por los Castro en Cuba. Lo contrario a lo que hoy condona e ignora la CELAC.</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Pero a los políticos latinoamericanos les importa un bledo decir una cosa en la Carta Democrática Interamericana y hacer otra muy distinta en los aquelarres organizados por CELAC. Como en el famoso poema de Walt Whitman, repiten el “me contradigo, y qué”. Ahí estará en La Habana, incluso, el Secretario General de la OEA, el señor José Miguel Insulza, quien debería ser el guardián de la Carta Democrática Interamericana, prueba viviente de que la esquizofrenia ideológica existe y es incurable.&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Nada de esto, me temo, es nuevo. Uno de los rasgos más desagradables de muchos políticos latinoamericanos es la hipocresía. Tienen varios discursos. Varias caras. Dicen que son pragmáticos. No es verdad. Son cínicos. Durante décadas, los vecinos convivían en silencio con polvorientas dictaduras como las de Stroessner, Somoza o Trujillo. Ahora les importa muy poco lo que sucede en Cuba o Venezuela. Es el imperio de la inmundicia moral.&nbsp;</div>
<div>&nbsp;</div>
<div>Fuente: colaboración del autor</div>
<div><strong>Carlos Alberto Montaner es miembro de la Junta Honorífica de RELIAL</strong></div>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/255-human-rights-watch">
			&laquo; Human Rights Watch		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/259-celac-una-infamia-para-la-democracia-en-la-región">
			CELAC: Una infamia para la democracia en la región &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/257-la-celac-contra-la-carta-democrática-interamericana#startOfPageId257">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:61:"La CELAC contra la Carta Democrática Interamericana - Relial";s:11:"description";s:155:"La opinión de Carlos Alberto Montaner &amp;nbsp; El general Raúl Castro es el presidente pro tempore de la CELAC y todos han ido a La Habana, como los...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:52:"La CELAC contra la Carta Democrática Interamericana";s:6:"og:url";s:122:"http://www.relial.org/index.php/productos/archivo/actualidad/item/257-la-celac-contra-la-carta-democrática-interamericana";s:8:"og:title";s:61:"La CELAC contra la Carta Democrática Interamericana - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/3409e45349ec9f6b3397bfe10e87a1d0_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/3409e45349ec9f6b3397bfe10e87a1d0_S.jpg";s:14:"og:description";s:159:"La opinión de Carlos Alberto Montaner &amp;amp;nbsp; El general Raúl Castro es el presidente pro tempore de la CELAC y todos han ido a La Habana, como los...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:52:"La CELAC contra la Carta Democrática Interamericana";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}