<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9058:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId479"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El récord de probreza en Venezuela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f3ad4a234535b69ec9bf916a25462992_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f3ad4a234535b69ec9bf916a25462992_XS.jpg" alt="El r&eacute;cord de probreza en Venezuela" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Andrés Oppenheimer</p>
<p>&nbsp;</p>
<p>La historia reciente de Venezuela debería ser de enseñanza obligatoria en todas las universidades del mundo, como ejemplo de un milagro económico al revés: a pesar de haberse beneficiado del boom petrolero más grande de su historia, el país hoy en día tiene más altos niveles de pobreza que antes.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Un nuevo estudio realizado en conjunto por tres importantes universidades venezolanas -la Universidad Católica Andrés Bello, Universidad Central de Venezuela</p>
<p>&nbsp;</p>
<p>Un nuevo estudio realizado en conjunto por tres importantes universidades venezolanas -la Universidad Católica Andrés Bello, Universidad Central de Venezuela y la Universidad Simón Bolívar- muestra que el 48.4 por ciento de los hogares venezolanos se encuentra por debajo de la línea de pobreza en el 2014, frente al 45 por ciento de los hogares en 1998, antes de que el difunto Presidente Hugo Chávez asumiera el poder y el país se beneficiara de casi una década de aumento en los precios del petróleo.</p>
<p>&nbsp;</p>
<p>Luis Pedro España, profesor de la Universidad Católica Andrés Bello y coautor del estudio, me dijo en una entrevista telefónica que la encuesta nacional de 5 mil 400 personas fue realizada en octubre del 2014, y utilizó la misma metodología que un estudio similar realizado en 1998 por la oficina de estadísticas del gobierno de entonces.</p>
<p>&nbsp;</p>
<p>El nuevo estudio de la pobreza contrasta con las cifras del gobierno del Presidente Nicolás Maduro, según las cuales la pobreza ha disminuido bajo la "revolución socialista" de Chávez.</p>
<p>&nbsp;</p>
<p>Según el Instituto Nacional de Estadística (INE) del gobierno de Venezuela, la tasa de pobreza ha caído del 44 por ciento de los hogares en 1998 al 27.3 por ciento de los hogares en el 2013. Sin embargo, el INE no ha publicado las cifras del 2014.</p>
<p>&nbsp;</p>
<p>La Comisión Económica para América Latina y el Caribe de las Naciones Unidas (CEPAL), que utiliza cifras oficiales de Venezuela, informó recientemente que la pobreza en aquel país -incluyendo la pobreza extrema - se incrementó casi 10 puntos porcentuales durante el 2013. Sin embargo, la CEPAL tampoco proporcionó cifras para el 2014.</p>
<p>&nbsp;</p>
<p>Cuando le pregunté a España por la disparidad entre su estudio y las estadísticas oficiales de pobreza, dijo que se ha producido una "brutal caída del poder adquisitivo de los venezolanos en 2014", y que la disparidad radica en gran medida en que las cifras del INE y la CEPAL no reflejan las cifras del 2014.</p>
<p>&nbsp;</p>
<p>Venezuela que, tras la destrucción de gran parte de su sector privado depende de las exportaciones de petróleo para el 96 por ciento de sus ingresos externos, es uno de los países más afectados por el colapso de los precios mundiales del petróleo. Después de que pasaron de 9 dólares por barril cuando Chávez fue elegido en 1998 a un récord de 145 por barril en el 2008, los precios del petróleo han caído a cerca de 45 por barril actualmente.</p>
<p>&nbsp;</p>
<p>La gestión de Chávez y su sucesor, Maduro, espantó a la inversión nacional y extranjera, y dio lugar a una escasez generalizada de leche, papel higiénico y otros productos básicos, junto con una tasa de inflación anual del 64 por ciento, la más alta del mundo.</p>
<p>&nbsp;</p>
<p>"Y todo indica que el 2015 va a ser peor", dice España. "Todos los indicadores económicos muestran que nos estamos dirigiendo hacia una inflación del 100 por ciento o 120 por ciento este año, que será un récord en la historia de Venezuela".</p>
<p>&nbsp;</p>
<p>Maduro culpa a una supuesta "guerra económica" de la oligarquía y el imperialismo, y cita como ejemplo las recientes sanciones anuncias por Washington. Sin embargo, Estados Unidos dice que las sanciones se limitan exclusivamente a la revocación de visas de entrada para funcionarios venezolanos acusados de corrupción o abusos a los derechos humanos.</p>
<p>&nbsp;</p>
<p>Mi opinión: Es difícil recordar otro caso de un país que haya recibido tanto dinero en los últimos años, y haya terminado más pobre que antes. Según el Banco Central de Venezuela, el gobierno obtuvo 325 mil millones dólares de exportaciones de petróleo entre 1998 y 2008 -más que el producto bruto interno de varios países latinoamericanos juntos.</p>
<p>&nbsp;</p>
<p>Pero, en lugar de aprovechar los buenos tiempos para invertir en educación, ciencia, tecnología e innovación, o por lo menos crear un colchón financiero para los años de las vacas flacas, Venezuela se embarcó en una fiesta populista de subsidios, corrupción y expropiaciones sin sentido. Es una película que hemos visto una y otra vez en América Latina, y que invariablemente termina mal.</p>
<p>&nbsp;</p>
<p>Ahora, se ha caído el último argumento del mal llamado "Socialismo del siglo 21", que es el haber supuestamente reducido la pobreza. Como lo muestra el nuevo estudio de las tres universidades, el petropopulismo de Venezuela solo dio lugar a una ilusión pasajera de justicia social, y dejó al país con más pobres que antes.</p>
<p>&nbsp;</p>
<p>Autor: Andrés Oppenheimer</p>
<p>Fuente: <a href="http://www.reforma.com">www.reforma.com</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/474-maduro-huye-hacia-delante">
			&laquo; Maduro huye hacia delante		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/483-entre-los-comisarios-y-el-mercado">
			Entre los comisarios y el mercado &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/479-el-récord-de-probreza-en-venezuela#startOfPageId479">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:44:"El récord de probreza en Venezuela - Relial";s:11:"description";s:157:"En la opinión de Andrés Oppenheimer &amp;nbsp; La historia reciente de Venezuela debería ser de enseñanza obligatoria en todas las universidades del m...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:35:"El récord de probreza en Venezuela";s:6:"og:url";s:102:"http://www.relial.org/index.php/productos/archivo/opinion/item/479-el-récord-de-probreza-en-venezuela";s:8:"og:title";s:44:"El récord de probreza en Venezuela - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f3ad4a234535b69ec9bf916a25462992_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f3ad4a234535b69ec9bf916a25462992_S.jpg";s:14:"og:description";s:161:"En la opinión de Andrés Oppenheimer &amp;amp;nbsp; La historia reciente de Venezuela debería ser de enseñanza obligatoria en todas las universidades del m...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:35:"El récord de probreza en Venezuela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}