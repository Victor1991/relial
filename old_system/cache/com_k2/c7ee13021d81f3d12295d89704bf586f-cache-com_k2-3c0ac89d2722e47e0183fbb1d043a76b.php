<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7273:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId77"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El ciudadano perfecto
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><img style="margin: 3px; border: 1px solid #cccccc; float: left;" alt="Peru - IPL" src="images/logos/Peru%20-%20IPL.png" width="172" height="46" />Cuando uno escucha lo que se dice de las virtudes de la formación militar de boca tanto de civiles como militares, se tiene la sensación de que la perfección ciudadana&nbsp;se&nbsp;alcanza en los cuarteles.</p>
<p>&nbsp;Por Paul Laurent</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Aunque la predilección por los guerreros y sus hazañas es de antigua data, todo indica que fue con el "despotismo ilustrado" (siglo XVIII) que esa vieja fascinación por lo castrense volvió a catapultarse en el imaginario del hombre moderno. Por lo pronto, Napoleón concibió que había que uniformar a los ciudadanos, como muestra de su alto y desinteresado servicio a la república... y luego al imperio... su imperio...</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Obviamente la barroca Europa occidental anterior al arribo de Bonaparte no estaba apta para ese tipo de inventos. La herencia comunal-republicana de donde se forjó era por naturaleza contraria a despotismos de esa traza. Por ende, la novedad no provino de su seno, sino allende sus fronteras.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>En el siglo XII Juan de Salisbury no salía de su asombro ante la noticia de que los electores imperiales habían acordado darle el título de sacro-emperador a un alemán.&nbsp;¡Qué correncia! ¿Cómo eligen para esa cargo a un príncipe acostumbrado sólo a mandar a siervos y campesinos y no a hombres libres, a ciudadanos, a ciudadanos de ciudades, no de esos extraños parajes?, bien pudo proferir malhumorado en célebre autor del&nbsp;Policraticus.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Sin duda, hombres como Salisbury tenían una concepción de la ciudadanía aún ligada a su auténtico y único origen: la ciudad, que en lenguaje profano no es otro ámbito que el mercado. Ese hábitat donde en los últimos años muchos de los potenciales conscriptos han ido introduciéndose con el fin de contribuir a su propia causa, que es la de progresar tanto para bien personal como para el de los suyos.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>¿Qué tipo de educación le podrá brindar a estos personajes el mercado? ¿A levantarse temprano, a no perder el tiempo, a respetar la palabra empeñada y los contratos, a no estafar, a ser puntual, a hacer valer sus derechos, a exigir igualdad de trato frente a los demás? ¿Exactamente todo aquello que se enseña en un cuartel?</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>¿Qué tipo de hombre es el que surge del mercado, y cuál el que sale de un cuartel? ¿Se puede hablar de libertad entre quienes pactan voluntariamente entre sí de la misma forma como se habla de libertad de aceptar o no aceptar ante quien recibe una orden de un superior castrense?</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Si partimos que desde la retórica estatal la libertad no tiene nada que ver con la libertad que reza el ideario liberal nacido de la más pura urbanidad, poco o nada habrá de esperarse de lo que diga un ministro que aboga por una "libertad de elegir" que es más próxima a los "ciudadanos" de Cuba y de Corea del Norte (la máxima expresión del "despotismo ilustrado"), por muy liberal que ese ministro diga ser.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Si en su momento el presidente Ollanta Humala habló de los soldados como "guardianes socráticos de la nación", habremos de entender que estamos ante una casta diferenciada del resto de la población, ¿Un estado dentro de otro estado? En términos de Bertrand de Jouvenel, ¿un "estado Minotauro", ese monstruo que exige tributos sangrientos, que impone el servicio militar obligatorio que algunos ilusos imaginaban como un imposible?</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: Artículo Publicado originalmente en&nbsp;http://pchrlaurent.wordpress.com/&nbsp;republicado con la autorización de su autor, en <a href="http://www.iplperu.org/">IPL Perú</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/277-venezuela-se-cae-de-maduro-que-el-pueblo-reclama-libertad">
			&laquo; Venezuela: Se cae de Maduro que el pueblo reclama libertad		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/104-libre-cambio-y-sentido-común">
			Libre cambio y sentido común &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/77-el-ciudadano-perfecto#startOfPageId77">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:30:"El ciudadano perfecto - Relial";s:11:"description";s:155:"Cuando uno escucha lo que se dice de las virtudes de la formación militar de boca tanto de civiles como militares, se tiene la sensación de que la per...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:21:"El ciudadano perfecto";s:6:"og:url";s:87:"http://www.relial.org/index.php/productos/archivo/opinion/item/77-el-ciudadano-perfecto";s:8:"og:title";s:30:"El ciudadano perfecto - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:155:"Cuando uno escucha lo que se dice de las virtudes de la formación militar de boca tanto de civiles como militares, se tiene la sensación de que la per...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:21:"El ciudadano perfecto";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}