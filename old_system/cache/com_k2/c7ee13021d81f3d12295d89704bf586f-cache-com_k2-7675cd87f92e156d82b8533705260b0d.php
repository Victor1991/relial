<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11609:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId435"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Venezuela: Un país en coma
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/57df6a0f34180006582f429068c6ac21_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/57df6a0f34180006582f429068c6ac21_XS.jpg" alt="Venezuela: Un pa&iacute;s en coma" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="margin-left: 30px; text-align: justify;">En la opinión de Antonella Marty</p>
<p style="margin-left: 30px; text-align: justify;">&nbsp;</p>
<p style="margin-left: 30px; text-align: left;">"Ante el silencio cómplice de tantos, la demagogia ha hecho y deshecho a gusto, jugando con el pueblo venezolano, y sometiéndolo al peor de los infiernos. No espere a vivir en comunismo para reaccionar ante estas atrocidades, no espere que le suceda a usted".&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p style="margin-left: 30px; text-align: justify;">&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Este artículo no pretende ser una simple nota informativa adicional sobre la situación en Venezuela. Carga en sí, además de datos específicos y coyunturales de la política económica del país, una cuota de sensaciones personales.</p>
<p>&nbsp;</p>
<p>Muchos lectores estarán al tanto de que en Venezuela se intentan aplicar, por ejemplo, más sistemas de control, tales como el nuevo Sistema Biométrico de captahuellas para prohibir las supuestas "compras que ocasionan saqueos en los supermercados" según el gobierno, de modo que se "mantenga el orden de la salida de los alimentos". Es así que el grupo que detenta el poder, al igual que todos los gobiernos rebozados en demagogia e inoperancia, intenta echar más leña al fuego. No, la solución no es más control.</p>
<p>&nbsp;</p>
<p>Venezuela es el ejemplo más claro de que la simple posesión de recursos naturales no hace mejor a un país o a su economía si los recursos no saben utilizarse de modo correcto y no demagógicamente. El petróleo en manos del gobierno ha servido para financiar a gusto los negocios corruptos y programas populistas del chavismo, haciendo uso de los barriles de petróleo como si fuesen golosinas para repartir de modo desmedido a un grupo de niños. En este país petrolero, lamentablemente, los ciudadanos tienen que hacer largas colas para conseguir comida.</p>
<p>&nbsp;</p>
<p>El pueblo venezolano ha sido sometido a atroces castigos, atravesando la destrucción moral de la sociedad, y encontrándose bajo una manipulación mediática, consecuencia de la inexistencia de libertad de prensa y expresión.</p>
<p>&nbsp;</p>
<p>Venezuela se encuentra actualmente dominada por la violencia diaria de los miembros que conforman la cúpula dictatorial de Nicolás Maduro y, otro tipo de delincuentes –similares a los delincuentes del gobierno- que abundan en las calles de Caracas convocando a la inseguridad constante, resultado de la producción desmedida de pobres.</p>
<p>&nbsp;</p>
<p>En este sentido, la pobreza es una de las pocas cosas que el gobierno chavista ha logrado producir satisfactoriamente y de modo abundante en estos quince años de poder. Basta con observar Petare, uno de los barrios –o villas- más grandes, más poblados e inseguros de América Latina, transformándose en una de las vistas más impactantes y tristes de la ciudad: valle tras valle de chozas humildes con colores, una arriba de la otra.</p>
<p>&nbsp;</p>
<p>Este proyecto de gobierno perverso y comunista intenta alcanzar el control absoluto de la vida humana de todo venezolano: busca una sociedad de súbditos.</p>
<p>&nbsp;</p>
<p>El gobierno tiene el ferviente deseo de perpetuarse en el poder. Así como lo ha hecho la dictadura de los Castro en Cuba, donde ante el silencio internacional y la opresión de un pueblo, se ha mantenido en el poder por más de cinco décadas –situación únicamente posible si se masacran las libertades y la integridad de la sociedad, llevando al pueblo hacia una utopía socialista inexistente, cimentada en el engaño y el rencor.</p>
<p>&nbsp;</p>
<p>Sin embargo, aunque a los venezolanos les hayan quitado sus libertades (al igual que a los cubanos) dentro de ellos perdura la sed de progreso, evolución y libertad –elementos inexistentes en la esencia y raíz del comunismo. Aquella sed de libertad nunca podrá ser expropiada, controlada, racionada, o eliminada por la arrogancia de la demagogia populista de turno; la libertad se lleva adentro.</p>
<p>&nbsp;</p>
<p>Hoy, al transitar las calles de Caracas, se quiebra algo en mí al presenciar la muerte en vida de un país, y observar la indiferencia de los medios de comunicación internacionales, y de tantos individuos y gobiernos cómplices de esta atrocidad.</p>
<p>&nbsp;</p>
<p>Comercios cerrados por excusas gubernamentales tales como la "especulación"; anaqueles vacíos por los controles de precios; ciudadanos haciendo largas colas y desesperados por comida; vidas que dejan de lado el goce individual y la búsqueda de la felicidad para tener que sobrevivir el día a día; la ciudad militarizada; protestas silenciadas; un pueblo reprimido y resignado que ha tenido que aceptar la situación de sumisión luego de largos meses de luchar en las calles por una salida, por un futuro mejor. Hoy, los grafitis y pancartas anti-dictadura en las calles de Caracas, conforman no más que el anhelo de un país que otrora fue, y hace años intenta volver a ser, grande y libre.</p>
<p>&nbsp;</p>
<p>Esta es la realidad de un país que está cercano a una situación de estado vegetativo. Un país muerto en vida, aniquilado, y con una pequeña cuota de esperanza como un suero, que cada día escasea más. La desilusión parece haberse personificado, y se hace presente en Venezuela, provocando en las calles una sensación de estado de guerra permanente.</p>
<p>&nbsp;</p>
<p>Desearía hacer un pronóstico sosteniendo que Venezuela está cada vez más lejos de ser una segunda Cuba. Es claro que no todo está perdido, sin embargo, resulta factible decir que ni este año, ni el que viene, y probablemente tampoco dentro de la próxima década podremos ver cambios de libertad en el país, a menos que se produzca un milagro radical que ponga en jaque a la dictadura. Hoy la solución inmediata no existe, el cambio sólo será a largo plazo.</p>
<p>&nbsp;</p>
<p>El gobierno revolucionario no dejará el poder de un modo sencillo y de un día para el otro. Es simple, la cúpula chavista carece de los estímulos para hacerlo, ¿por qué dejaría el poder si cuenta con comodidad y todas las necesidades absolutamente cubiertas de aquí a largos años? La salida llevará más que las aplaudidas y necesitadas protestas de este año.</p>
<p>&nbsp;</p>
<p>Ante el silencio cómplice de tantos, la demagogia ha hecho y deshecho a gusto, jugando con el pueblo venezolano, y sometiéndolo al peor de los infiernos. No espere a vivir en comunismo para reaccionar ante estas atrocidades, no espere que le suceda a usted.</p>
<p>&nbsp;</p>
<p>A modo de conclusión y reflexión, dejo aquí parte de uno de los versos del admirable poeta inglés, Rudyard Kipling, del año 1914 y titulado "Una canción en la tormenta":</p>
<p>&nbsp;</p>
<p>"Asegúrate bien de que a tu lado peleen los océanos eternos, aunque esta noche el viento en contra y las mareas nos hagan su juguete [...] En todo tiempo de angustia y también en el de nuestra salvación, el juego vence siempre al jugador y el barco a su tripulación [...] Asegúrate bien, a pesar de que las olas y el viento en reserva guardan ráfagas aún más poderosas, que los que cumplimos las guardias asignadas, ni por un instante descuidemos la vigilancia [...] De cualquier pérdida podremos sacar provecho, salvo de la pérdida del regreso".</p>
<p>&nbsp;</p>
<p>Estos versos quizás describan en cierta forma el modo en que sobreviven los venezolanos su tormenta de cada día. No olvidemos a Venezuela, que no se pierda el regreso a un país mejor.</p>
<p>&nbsp;</p>
<p>Autor: Antonella Marty,&nbsp;miembro fundador del Grupo Joven Fundación Libertad</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/428-joven-becario-de-universidad-de-la-libertad-es-candidato-a-regidor">
			&laquo; Joven becario de Universidad de la Libertad es candidato a regidor		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/436-reporte-visita-de-martín-krause-a-venezuela">
			Reporte, visita de Martín Krause a Venezuela &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/435-venezuela-un-país-en-coma#startOfPageId435">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:36:"Venezuela: Un país en coma - Relial";s:11:"description";s:155:"En la opinión de Antonella Marty &amp;nbsp; &quot;Ante el silencio cómplice de tantos, la demagogia ha hecho y deshecho a gusto, jugando con el pueblo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:27:"Venezuela: Un país en coma";s:6:"og:url";s:147:"http://www.relial.org/index.php/productos/archivo/actualidad/item/435-venezuela-un-pa%C3%ADs-en-coma/index.php/productos/capacitaciones-y-formacion";s:8:"og:title";s:36:"Venezuela: Un país en coma - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/57df6a0f34180006582f429068c6ac21_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/57df6a0f34180006582f429068c6ac21_S.jpg";s:14:"og:description";s:163:"En la opinión de Antonella Marty &amp;amp;nbsp; &amp;quot;Ante el silencio cómplice de tantos, la demagogia ha hecho y deshecho a gusto, jugando con el pueblo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:27:"Venezuela: Un país en coma";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}