<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6289:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId482"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	LIBERTAD PARA LOS PRESOS POLÍTICOS
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/464b3f74fd6601955ccb022f610c3111_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/464b3f74fd6601955ccb022f610c3111_XS.jpg" alt="LIBERTAD PARA LOS PRESOS POL&Iacute;TICOS" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="margin-left: 30px;"><strong>CEDICE Libertad ante la detención de Antonio Ledezma</strong></p>
<p>Exigimos el respeto al Estado de Derecho, a la diversidad de formas de pensar y actuar y a la libertad en el más amplio sentido de la expresión.</p>
<p>La detención del Alcalde Metropolitano de Caracas, elegido en votación popular, representa una nueva agresión al Estado de Derecho y a la libertad por parte del Gobierno Nacional.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Antonio Ledezma se suma a la lista integrada por Leopoldo López, Daniel Ceballos, los estudiantes detenidos y otros venezolanos que se encuentran encarcelados por diferir del socialismo del siglo XXI, modelo que intenta imponer por la fuerza el régimen.</p>
<p>&nbsp;</p>
<p>Al ataque sistemático a la propiedad privada y a la iniciativa particular, al establecimiento de excesivos controles contraproducentes para el funcionamiento eficiente de la economía, se suma el cerco que se ha tendido en el plano político. Ledezma es el nuevo rostro de la represión.</p>
<p>&nbsp;</p>
<p>Cada vez son más los líderes de la oposición democrática intimidados, perseguidos y apresados. Sobre María Corina Machado y Julio Borges, dos dirigentes fundamentales, pesan amenazas de encarcelamiento que en cualquier momento podrían materializarse.</p>
<p>&nbsp;</p>
<p>El Gobierno optó por la vía autoritaria y militarista para enfrentar los graves problemas socioeconómicos que padece el país. En vez de convocar a todos los sectores de la vida nacional para buscar una solución compartida a la grave situación de escasez, inflación, desabastecimiento, deterioro de la salud y los servicios públicos e inseguridad que existen, prefiere aplicar medidas represivas que exacerban la confrontación y dividen y deterioran aún más a la sociedad.</p>
<p>&nbsp;</p>
<p>El desarrollo y el bienestar de Venezuela no se construirán con políticas hegemónicas excluyentes que vulneran los derechos constitucionales de los ciudadanos, sino a partir de prácticas que respeten el Estado de Derecho, la diversidad de formas de pensar y actuar, y la libertad en el más amplio sentido de la expresión.</p>
<p>&nbsp;</p>
<p>Cedice-Libertad se une al reclamo que tanto en Venezuela como en el exterior exige la liberación inmediata del alcalde Ledezma, de Leopoldo López, Daniel Ceballos y los demás presos políticos encerrados en las cárceles del régimen, sin causa alguna.</p>
<p>&nbsp;</p>
<p><strong>Democracia es Libertad.</strong></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/447-declaración-de-relial-sobre-derechos-humanos">
			&laquo; Declaración de RELIAL sobre Derechos Humanos		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/485-comunicado-ipl-en-panamá">
			Comunicado IPL en Panamá &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/482-libertad-para-los-presos-políticos#startOfPageId482">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:44:"LIBERTAD PARA LOS PRESOS POLÍTICOS - Relial";s:11:"description";s:154:"CEDICE Libertad ante la detención de Antonio Ledezma Exigimos el respeto al Estado de Derecho, a la diversidad de formas de pensar y actuar y a la lib...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:35:"LIBERTAD PARA LOS PRESOS POLÍTICOS";s:6:"og:url";s:108:"http://www.relial.org/index.php/productos/archivo/comunicados/item/482-libertad-para-los-presos-polÃ­ticos";s:8:"og:title";s:44:"LIBERTAD PARA LOS PRESOS POLÍTICOS - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/464b3f74fd6601955ccb022f610c3111_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/464b3f74fd6601955ccb022f610c3111_S.jpg";s:14:"og:description";s:154:"CEDICE Libertad ante la detención de Antonio Ledezma Exigimos el respeto al Estado de Derecho, a la diversidad de formas de pensar y actuar y a la lib...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:35:"LIBERTAD PARA LOS PRESOS POLÍTICOS";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}