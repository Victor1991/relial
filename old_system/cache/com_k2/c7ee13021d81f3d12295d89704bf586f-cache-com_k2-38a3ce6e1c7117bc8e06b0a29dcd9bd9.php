<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6412:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId303"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Milton Friedman y la verdad histórica
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7fb770f34c796f7501d3cf0f0dc39075_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7fb770f34c796f7501d3cf0f0dc39075_XS.jpg" alt="Milton Friedman y la verdad hist&oacute;rica" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>Estando cerca de cumplirse los 102 años del nacimiento de Milton Friedman, uno de los más importantes economistas del siglo XX, profesor de la Universidad de Chicago y Premio Nobel de Economía en 1976, es pertinente señalar que sus tesis y pensamiento, así como su vinculación con América Latina, especialmente con Chile, no ha dejado de ser controversial para un sector de dicha sociedad.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Al respecto, un libro que he leído recientemente y cuya lectura deseo recomendarles es Un legado de libertad: Milton Friedman en Chile , cuyo principal mérito es despejar toda polémica en torno a considerar Al economista norteamericano un estrecho y directo colaborador de la dictadura de entonces, como muchos prefieren creer hasta hoy.</p>
<p>&nbsp;</p>
<p>Un legado de libertad aclara ese terrible malentendido, malévolamente explotado por los "fascistas de izquierda" y neo-violentistas que se pretenden garantes de la moral, la libertad y los derechos de las personas, cuando sólo las aborrecen.</p>
<p>&nbsp;</p>
<p>Más allá de la conferencia "Bases para un desarrollo económico" que ofreció Friedman en 1975, y de una carta que dirige al General Augusto Pinochet, no existe una mayor vinculación entre el régimen y el autor de Libertad de elegir. Cabe destacar que en toda su trayectoria académica Friedman fue un defensor de la libertad política y la libertad económica, pues muchos años antes de su visita a Chile, en 1947, funda junto a Friedrich A. von Hayek la Mont Pelerin Society, que "tuvo por objeto (según Friedman) promover una filosofía liberal clásica, es decir, una economía libre, una sociedad libre tanto civilmente como en derechos humanos", tal cual cita Axel Kaiser en el ensayo que ofrece en el libro bajo comentario.</p>
<p>&nbsp;</p>
<p>Asimismo, Jaime Bellolio en su ensayo cita el discurso de Milton Friedman realizado en noviembre de 1991, con motivo de la inauguración del Smith Center de la California State University, donde señala que "en Chile, la presión por la libertad política, que fue (en parte) generada por la libertad económica y los exitosos resultados económicos, terminó en un plebiscito que introdujo la democracia. Ahora, luego de un largo tiempo, Chile tiene las tres cosas: libertad política, libertad humana y libertad económica".</p>
<p>&nbsp;</p>
<p>Por eso recomiendo este libro en estas fechas de celebraciones a Milton Friedman: porque la verdad, aunque tarde y se nuble con las oscuridades de sus enemigos, siempre sale a la luz.</p>
<p>&nbsp;</p>
<p>Foto: IEAH</p>
<p>Fuente: Hector Ñaupari</p>
<p>Autor: Hector Ñaupari</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/302-el-legado-de-hugo-chávez">
			&laquo; El legado de Hugo Chávez		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/304-un-apunte-sobre-bitcoin">
			Un apunte sobre Bitcoin &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/303-milton-friedman-y-la-verdad-histórica#startOfPageId303">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:47:"Milton Friedman y la verdad histórica - Relial";s:11:"description";s:158:"En la opinión de Héctor Ñaupari &amp;nbsp; Estando cerca de cumplirse los 102 años del nacimiento de Milton Friedman, uno de los más importantes econo...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:38:"Milton Friedman y la verdad histórica";s:6:"og:url";s:105:"http://www.relial.org/index.php/productos/archivo/opinion/item/303-milton-friedman-y-la-verdad-histórica";s:8:"og:title";s:47:"Milton Friedman y la verdad histórica - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/7fb770f34c796f7501d3cf0f0dc39075_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/7fb770f34c796f7501d3cf0f0dc39075_S.jpg";s:14:"og:description";s:162:"En la opinión de Héctor Ñaupari &amp;amp;nbsp; Estando cerca de cumplirse los 102 años del nacimiento de Milton Friedman, uno de los más importantes econo...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:38:"Milton Friedman y la verdad histórica";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}