<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10140:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId313"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	¿Es la desigualdad un problema?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/4251dec72b18ac89643edfb7a8300016_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/4251dec72b18ac89643edfb7a8300016_XS.jpg" alt="&iquest;Es la desigualdad un problema?" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<h3 class="bajada_despliegue_nota">En la opinión de Axel Kaiser</h3>
<h3 class="bajada_despliegue_nota">&nbsp;</h3>
<h3 class="bajada_despliegue_nota">"Todos somos diferentes, es decir, desiguales. Nuestros talentos, capacidades, inteligencia, disposición al esfuerzo y todos los demás factores que definen nuestro ingreso varían de una persona a otra..."</h3>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>A muchas personas, el mero hecho de formular esta pregunta les parece inaceptable. Evidentemente, piensan ellos, la desigualdad es un gran problema. Como diría la Presidenta Bachelet: la desigualdad es nuestro "gran enemigo". La declaración de Bachelet, sin duda en sintonía con el Zeitgeist, no es irrelevante. Como advirtiera John Stuart Mill, el clima de opinión intelectual define en buena medida la evolución institucional de un país y puede tener consecuencias desastrosas.</p>
<p>&nbsp;</p>
<p>De ahí que sea pertinente examinar el postulado igualitarista críticamente de modo de establecer qué es realmente lo que ataca y qué es lo que propone. Como primera cuestión, este ejercicio requiere analizar cuál es el origen de la desigualdad. Y este no es otro, como notó Courcelle-Seneuil hace un siglo y medio, que la naturaleza humana.</p>
<p>&nbsp;</p>
<p>Todos somos diferentes, es decir, desiguales. Nuestros talentos, capacidades, inteligencia, disposición al esfuerzo y todos los demás factores que definen nuestro ingreso varían de una persona a otra. En una sociedad de personas libres estas desigualdades afloran permitiendo que cada uno haga el mejor uso de los talentos, suerte y capacidades de que dispone para servir a otros. Esto es lo que se conoce como principio de división del trabajo que Adam Smith explicara tan magistralmente en "La Riqueza de las Naciones", obra poco leída por liberales y aún menos leída por los críticos del liberalismo.</p>
<p>&nbsp;</p>
<p>Bajo un "sistema de libertad natural", como lo llamó Smith, habrá algunos que sean panaderos, otros ingenieros, habrá abogados, herreros, profesores, deportistas, campesinos, obreros, etc. También habrá muchos que cambien de profesión en el camino, mientras otros comenzarán pobres y terminarán ricos, y viceversa.</p>
<p>&nbsp;</p>
<p>En este sistema los ingresos variarán de acuerdo a la valoración que el resto de los miembros de la sociedad hace del aporte de cada persona. Se trata de un sistema que satisface necesidades y deseos ajenos, y en el cual los méritos no juegan ni pueden jugar un rol relevante.</p>
<p>&nbsp;</p>
<p>Cuando usted va a comprar carne de cerdo no le interesa saber si el carnicero fue personalmente a cazar, cuchillo en mano, un jabalí en la montaña o si el animal fue producido en masa a un mínimo esfuerzo. Tampoco le interesa si el productor de un cierto bien es buena persona. Usted no paga por el mérito sino por el producto. Si es bueno y a un precio razonable, lo compra; si no, busca otro. En ese sentido el consumidor, como explicó Ludwig von Mises, es despiadado y el empresario está obligado a satisfacerlo si quiere sobrevivir.</p>
<p>&nbsp;</p>
<p>Esta libertad de elegir de acuerdo a las propias valoraciones constituye la esencia de la democracia del mercado y es lo que explica que Alexis Sánchez gane miles de veces más por patear una pelota que una enfermera por salvar vidas, a pesar de que lo primero sea menos meritorio que lo segundo. Lo fascinante de este sistema de libertad es que, a pesar de contravenir intuiciones de justicia bastante generalizadas, es sin duda alguna el que permite el mayor progreso económico y social para todos los miembros de la comunidad.</p>
<p>&nbsp;</p>
<p>Si mañana un ingeniero japonés descubriera la fórmula para producir energía limpia a costo casi cero, no solo ese ingeniero se haría millonario, sino que el ingreso de la mayoría de los habitantes del mundo se incrementaría exponencialmente. Esa es la historia del capitalismo, el que indudablemente no produce igualdad sino riqueza. Cuando Friedrich Hayek observó, para escándalo de los socialistas, que la desigualdad era parte fundamental de la economía de libre mercado, no estaba más que constatando que esta se deriva del principio de división del trabajo sobre el que descansa nuestro bienestar y nuestra civilización.</p>
<p>&nbsp;</p>
<p>En ese contexto, sostener, como hizo Bachelet, que la desigualdad es el enemigo equivale a afirmar que la libertad y la diversidad humana son el enemigo. Si no fuera así y la libertad no fuera considerada el enemigo, no sería necesario reemplazar la cooperación voluntaria de las personas por intervención estatal, que es lo que proponen los igualitaristas a sabiendas de que solo el Estado permite alcanzar, mediante la coacción, resultados políticamente deseados como la igualdad. La mejor prueba de que la búsqueda de igualdad es, a pesar del notable esfuerzo de John Rawls, necesariamente incompatible con la libertad son los regímenes totalitarios socialistas. Su máxima fue precisamente que la desigualdad y, por tanto, la economía libre eran el gran enemigo.</p>
<p>&nbsp;</p>
<p>El resultado es conocido. Obviamente, esto no es lo que pretende Bachelet ni la mayoría de los igualitaristas. Pero el camino que proponen recorrer, muchas veces con las mejores intenciones, sin duda conduce en la dirección de restringir la libertad de las personas afectando el bienestar de la sociedad. La fórmula liberal, por el contrario, propone maximizar espacios de libertad y ayudar solo a quienes por sus medios no logran surgir. En otras palabras, para los verdaderos liberales la desigualdad no es el problema. El problema es la pobreza. Lo que importa es que todos estén mejor y no que estén igual. Si un liberal tuviera que elegir entre duplicar los ingresos actuales de todos los chilenos, desde el más rico al más pobre, manteniendo con ello la desigualdad relativa existente hoy, o reducir a la mitad los ingresos del 15% más acomodado para convertirnos en un país muchísimo más igualitario, el liberal elegiría la primera opción. En cambio, un igualitarista convencido, como Bachelet, de que la desigualdad y no la pobreza es el gran enemigo a ser derrotado, preferiría la segunda opción desmejorando a algunos sin mejorar a nadie.</p>
<p>&nbsp;</p>
<p>Autor: Axel Kaiser</p>
<p>Foto: El mercurio, blog</p>
<p>Fuente: El mercurio; blog</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/311-rusia-y-la-nueva-guerra-fría">
			&laquo; Rusia y la nueva Guerra Fría		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/335-cien-años-de-un-escritor-indócil">
			Cien años de un escritor indócil &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/313-¿es-la-desigualdad-un-problema?#startOfPageId313">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:41:"¿Es la desigualdad un problema? - Relial";s:11:"description";s:154:"En la opinión de Axel Kaiser &amp;nbsp; &quot;Todos somos diferentes, es decir, desiguales. Nuestros talentos, capacidades, inteligencia, disposición...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:32:"¿Es la desigualdad un problema?";s:6:"og:url";s:98:"http://www.relial.org/index.php/productos/archivo/opinion/item/313-¿es-la-desigualdad-un-problema";s:8:"og:title";s:41:"¿Es la desigualdad un problema? - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/4251dec72b18ac89643edfb7a8300016_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/4251dec72b18ac89643edfb7a8300016_S.jpg";s:14:"og:description";s:162:"En la opinión de Axel Kaiser &amp;amp;nbsp; &amp;quot;Todos somos diferentes, es decir, desiguales. Nuestros talentos, capacidades, inteligencia, disposición...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:32:"¿Es la desigualdad un problema?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}