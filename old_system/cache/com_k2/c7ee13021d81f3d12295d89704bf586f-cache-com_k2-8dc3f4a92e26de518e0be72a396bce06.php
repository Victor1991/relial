<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8427:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId469"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Escribo tu nombre, libertad: El atentado de París según el liberalismo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/3bfa5a7e41ccb228078a88e28d2b13ec_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/3bfa5a7e41ccb228078a88e28d2b13ec_XS.jpg" alt="Escribo tu nombre, libertad: El atentado de Par&iacute;s seg&uacute;n el liberalismo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>En estos difíciles momentos, cuando el olor de la pólvora y la sangre del aberrante atentado de París no se han disipado del todo, resulta indispensable hallar una respuesta enérgica ante la brutalidad de este crimen, así como aliviar las repercusiones que este nefasto hecho tendrá en occidente.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Sostengo que es posible evaluar con realismo la magnitud de estos homicidios si consideramos a la libertad como el bien político más elevado, y explicar debidamente su significación en los términos de su expresión intelectual más acabada, el liberalismo. De esta manera, los periodistas y caricaturistas asesinados en Francia han muerto en nombre de una libertad insustituible, que nos interpela siempre a quienes habitamos en las sociedades generadas y nutridas por ella: la de creer lo que mejor les parezca, realizando sus proyectos de vida en torno a esas creencias y pareceres particulares, y que se proyectan en los demás en torno a un marco institucional que, respetando y haciendo respetar al prójimo y sus derechos, no contempla el crimen como respuesta si tales creencias y su expresión resultan blasfemas a otros.</p>
<p>&nbsp;</p>
<p>Así explicado, se intuiría a primera vista que esa visión no entraña ningún peligro. Pero este acto barbárico nos ha sacado de nuestra zona de confort. Cual un alarido que no cesa, nos grita de forma rotunda que la libertad y su solo ejercicio es peligrosa para muchos, que hay millones de seres humanos convencidos de ello y por ende prefieren –sin dudarlo un instante– la devota sumisión como la agradecida esclavitud, que no surge naturalmente, que es resultado de un delicado y paciente trabajo de filigrana, que en cada momento es posible perderla sin recuperarla o lográndolo a un altísimo costo y que está permanentemente a merced de quienes, saturados como están de sus ímpetus autoritarios, van a terminar de una vez y para siempre con ella mediante una revolución, un golpe de Estado, una corrupción sin freno ni límite, o, según este caso, ejerciendo el terrorismo como antesala para imponer un totalitarismo religioso de fervorosos feligreses en lo que alguna vez fuera una república democrática de ciudadanos libres.</p>
<p>&nbsp;</p>
<p>En cuanto es un disparo a sangre fría al corazón de nuestra libertad, rechazar enérgicamente este acto vesánico es el primero de muchos pasos para garantizar la supervivencia del estado de derecho y el régimen democrático en nuestros países. Y no sólo los deben dar los gobiernos que garantizan nuestra vida y seguridad, persiguiendo, enjuiciando y encarcelando con el máximo rigor a quienes, como los terroristas de París, tienen como propósito de vida destruirnos si no comulgamos con su proyecto teocrático. También, las empresas, los medios de comunicación, los líderes y organizaciones sociales y culturales, entre otros, que no deben ser presas del pánico y autocensurarse por temor a la violencia islamista: se volverían rehenes de estos extremistas, acabarían con la libertad de expresión que tanto costó en occidente, y que millares han respaldado en calles y plazas estos días, pero sobre todo deshonrarían el legado de los redactores y caricaturistas de Charlie Hebdo, quienes cumplieron cabalmente con la frase que Cervantes le hace decir al Quijote: "por la libertad así como por la honra se puede y debe aventurar la vida, y, por el contrario, el cautiverio es el mayor mal que puede venir a los hombres".</p>
<p>&nbsp;</p>
<p>Finalmente, este atentado debe constituir el parte aguas definitivo para los musulmanes de Francia y del mundo que seguramente ansían la paz y vivir según el modo de vida occidental, de rechazar este crimen y distinguirse de una vez de los grupos terroristas que merecen toda la severi­dad penal y militar que corresponda. Su cada vez más peligroso silencio incrementa los temores del resto de la sociedad civilizada, alimenta los votos y los argumentos de los nacionalistas europeos y de otras latitudes que harán lo indecible para expulsarlos de sus países, y justifica, en particular, la desconfianza de que el Islam inicie un proceso de secularización como lo han hecho otras religiones. Así, para poder convivir civilizadamente, católicos, musulmanes, ateos, todos, como en el poema de Eluard, debemos escribir tu nombre, libertad, para realizarla y ejercerla. Ojalá así sea.</p>
<p>&nbsp;</p>
<p>Autor: Héctor Ñaupari</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: IEAH</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/468-si-no-es-ahora-¿cuándo?">
			&laquo; Si no es ahora, ¿cuándo?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/474-maduro-huye-hacia-delante">
			Maduro huye hacia delante &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/469-escribo-tu-nombre-libertad-el-atentado-de-parís-según-el-liberalismo#startOfPageId469">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:81:"Escribo tu nombre, libertad: El atentado de París según el liberalismo - Relial";s:11:"description";s:159:"En la opinión de Héctor Ñaupari &amp;nbsp; En estos difíciles momentos, cuando el olor de la pólvora y la sangre del aberrante atentado de París no se...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:72:"Escribo tu nombre, libertad: El atentado de París según el liberalismo";s:6:"og:url";s:133:"http://relial.org/index.php/productos/archivo/opinion/item/469-escribo-tu-nombre-libertad-el-atentado-de-parís-según-el-liberalismo";s:8:"og:title";s:81:"Escribo tu nombre, libertad: El atentado de París según el liberalismo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/3bfa5a7e41ccb228078a88e28d2b13ec_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/3bfa5a7e41ccb228078a88e28d2b13ec_S.jpg";s:14:"og:description";s:163:"En la opinión de Héctor Ñaupari &amp;amp;nbsp; En estos difíciles momentos, cuando el olor de la pólvora y la sangre del aberrante atentado de París no se...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:72:"Escribo tu nombre, libertad: El atentado de París según el liberalismo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}