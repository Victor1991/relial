<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16300:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId104"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Libre cambio y sentido común
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: left;"><br /><br /><img src="images/logos/LYP%20nuevo%20cuadrado.jpg" width="110" height="89" alt="LYP nuevo cuadrado" style="margin: 3px; float: left;" />El análisis de Alberto Benegas Lynch &nbsp;&nbsp;</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Estamos otra vez en plena época del mercantilismo que irrumpió en el siglo XVII en el que se destacaba la manía por la acumulación de dinero en el comercio exterior, los balances comerciales "favorables", todo en el contexto de lo que luego se bautizó como el Dogma Montaigne.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Se ha perdido nuevamente la brújula de la economía puesto que en general se piensa que la gracia del comercio internacional consiste en maximizar los saldos de caja sin percatarse que, al igual que una empresa, lo relevante es el patrimonio neto y no los índices de liquidez como objetivo. Si se exporta por valor de cien y en el viaje el producto exportado se deteriora con lo que se puede comprar en el exterior (importar) solamente por valor de cincuenta, el balance comercial es "favorable" pero sin embargo se ha producido una pérdida evidente.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Lo ideal para un país es comprar todo lo que se necesita del exterior y no vender nada, con lo cual los extranjeros estarían obsequiando sus productos. Para una persona es lo mismo, lo ideal sería poder comprar y comprar sin vender nada, pero y a bastante dificultoso se hace que nos regalen para nuestro cumpleaños para pretender que el prójimo nos regale todo el tiempo todo lo que necesitamos. Entonces, no hay más remedio que vender para poder comprar ya sea a nivel individual o grupal: en este último caso decimos que el costo de las importaciones son las exportaciones.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Por esto es que el balance comercial es irrelevante, lo importante es el balance de pagos que, en un mercado abierto, está siempre equilibrado debido a las entradas y salidas de capital. Exportación e importación son dos caras del mismo proceso. Al exportar entran divisas con lo que el tipo de cambio se modifica incrementando el valor de la moneda local frente a la extranjera que queda depreciada. Esta depreciación hace que se torne más barato importar que al proceder, en consecuencia sucede el fenómeno inverso, es decir, se aprecia la divisa extranjera y se deprecia la local con lo que se estimulan las exportaciones y así sucesivamente.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Sostener que se debe "vivir con lo nuestro" constituye un desatino superlativo puesto que, además de retrotraernos a la autarquía de la cavernas, supone que se puede exportar sin importar, lo que naturalmente hace que el tipo de cambio refleje depreciaciones astronómicas de la divisa extranjera lo cual, paradójicamente, bloquea las mismas exportaciones (los costos de los bienes se elevan) ya que no se permite la válvula de escape de las importaciones que, precisamente, harán posible exportar.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Desde luego que los aranceles y tarifas dificultan el mencionado proceso abierto así como también las manipulaciones en el tipo de cambio que necesariamente debe ser libre al efecto de que funcione el antedicho mecanismo regulador y también se distorsiona cuando irrumpe la deuda pública externa que simula entrada de capitales lo que afecta el balance de pagos artificialmente.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El Dogma Montaigne se traduce en nuestro caso en que el que vende se enriquece a costa del comprador puesto que incrementa su saldo de caja sin prestar atención al lado no-monetario de la transacción que es lo que le permite mejorar su situación al comprador que precisamente compró debido a que estima en más el bien recibido que la suma monetaria que entregó a cambio. Ambas partes se benefician debido a la valorización cruzada del dinero y del bien en cuestión.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Desde la perspectiva de la sociedad abierta, las naciones se constituyen al solo efecto de evitar los enormes riesgos de concentración de poder de un gobierno universal, por ello el planeta está fraccionado en naciones y éstas a su vez en provincias y en municipios para diluir y mitigar los efectos del poder concentrado. Pero de allí a tomarse seriamente las fronteras como si fueran alambrados culturales hay un salto lógico inaceptable.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Los aranceles siempre significan mayor erogación por unidad de producto, es decir, menor productividad y, por ende, menor cantidad de productos que a su vez significan menor nivel de vida puesto que la lista de los bienes se reduce respecto de lo que hubieran sido de no haber mediado el arancel.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Entonces, en mal llamado "proteccionismo" en verdad desprotege a los consumidores y solo significa una pantalla para cubrir privilegios de empresarios ineficientes. La alegada protección a "la industria incipiente" se basa en premisas erróneas puesto que supone aranceles transitorios para permitir que la industria madure, sin ver que si la empresa necesita un período antes de ser competitiva son los mismos empresarios los que deben absorber los costos y no trasladárselos sobre las espaldas de los consumidores. Como en cualquier evaluación de proyectos, no siempre las ganancias comienzan en el momento uno sino que requieren de tiempo, para lo cual se buscan fondos en el mercado al efecto de financiar el período necesario, transcurrido el cual los beneficios más que compensarán los quebrantos. Si los recursos no se obtuvieran en el mercado, es por uno de dos motivos: o el proyecto no es rentable, o siendo rentable hay otros que merecen atención prioritaria y como todo no puede hacerse al mismo tiempo debido a que los recursos son limitados, el proyecto en cuestión debe postergarse o darse de baja por ineficiente.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Por esta especie de esquizofrenia del balance comercial es que Jaques Rueff en The Balance of Payments afirma que "no dudaría en recomendar la eliminación de las estadísticas del comercio exterior" debido a la permanente tentación de los gobernantes de intervenir con lo que inexorablemente generan las antedichas distorsiones.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Respecto al tema laboral, se piensa equivocadamente que la completa eliminación de aranceles provocará desempleo. Muy por el contrario, la desaparición de las trabas al comercio exterior libera trabajo para desempeñarse en otras tareas inconcebibles hasta el momento puesto que estaba esterilizado en otras faenas. Esto es lo que ocurrió con el hombre de la barra de hielo cuando apareció el refrigerador o con el fogonero cuando se fabricó la locomotora diesel. Los recursos son limitados y las necesidades son ilimitadas, el recurso por excelencia es el trabajo puesto que no se concibe la producción de ningún bien ni la prestación de ningún servicio sin su concurso. En competencia a las empresas les interesa la capacitación de futuro personal para poder sacar partida de nuevos productos en el mercado. La eliminación de aranceles tiene el mismo efecto que el descubrimiento de un procedimiento más productivo: reduce la inversión por unidad de producto con lo que se hacen posibles más productos. La tragedia de la desocupación se debe a la legislación laboral y no a la mayor productividad que eleva salarios e ingresos en términos reales debido a las mayores tasas de capitalización que la eficiencia permite.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Se ha sostenido que los aranceles puedan justificarse cuando se hace dumping, lo cual tampoco es cierto. El dumping es venta bajo el costo, situación que puede significar simplemente un quebranto impuesto al empresario debido a las condiciones de mercado, o puede ocurrir deliberadamente como política comercial con la idea de incrementar la tajada en el mercado. Si es lo primero no hay nada que comentar, si es lo segundo deben tenerse en cuenta los anticuerpos del propio mercado.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Dichos anticuerpos consisten en que los competidores compran al precio de dumping y revenden al precio de mercado, y si no lo pudieran hacer debido a que, por ejemplo, se trata de un producto a medida como las turbinas de cierto avión los competidores esperarán a que el que hace dumping agote su stock para luego seguir en la misma situación anterior. Este tema es una típica pregunta de examen puesto que habitualmente el estudiante asume que la venta de quien hace dumping es mayor de la del propio ejemplo. Si fuera mayor la situación es distinta puesto que el empresario en cuestión deberá expandir sus instalaciones para cumplir con la demanda total a ese precio, lo cual significa un precio de liquidación que beneficia a los consumidores. Pero ni bien el empresario del dumping pretenda restringir su producción para sacar ventaja de su posición en el mercado aparecerán competidores que venderán las diferencias para satisfacer la demanda o, como queda dicho, comprarán al empresario del dumping para el consiguiente arbitraje.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Todo lo dicho no toma en cuenta que muchas veces cuando se alega dumping no se verifican los libros contables del que se dice lo práctica y es meramente una excusa para defenderse de productores más eficientes. Por otra parte, debe subrayarse que si en un país se practica dumping al exterior con el apoyo gubernamental es ese país el perjudicado y, como ha enfatizado Milton Friedman, el resto del mundo es beneficiado equivalente a la situación en la que decidiera regalarle sus productos al extranjero lo cual es un motivo para celebrar en el extranjero.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>A veces las argumentaciones resultan tragicómicas como cuando se dice que se imponen aranceles "en reciprocidad" por los establecidos por otros. Este peculiar razonamiento se traduce en que porque el país receptor se perjudicó porque sus ventas son bloqueadas en el exterior, "en represalia" duplican el perjuicio aumentado los costos de los bienes que importa. ¡Vaya represalia!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El librecambio tiene mala prensa no solo por las incomprensiones mencionadas sino debido a que de tanto en tanto se han impuesto llamadas "aperturas" manejando los aranceles como una política de chantaje con la idea de que bajen los precios internos los cuales se elevan debido a las inflaciones internas con lo que las políticas se traducen en una estructura arancelaria en forma de serrucho que provoca cuellos de botella insalvables entre las industrias finales y sus respectivos insumos, al tiempo que se manipula el tipo de cambo con alquimias inauditas y se incrementa la deuda estatal que, como se ha señalado, interfiere en el balance de pagos permitiendo importaciones y viajes al exterior que la situación económica no hace posible.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Se suelen generar interminables debates tan insustanciales como impropios sobre posibles devaluaciones, sin la más mínima sospecha de que el precio de la divisa extranjera frente a la moneda local depende de las cantidades ofrecidas y demandadas lo cual resultará en el tipo de cambio de mercado en el que no habrán faltantes ni sobrantes, situación que no depende de voluntarismos de ningún megalómano. En el mismo error garrafal caen los burócratas (y algunos empresarios y sindicalistas malinformados) a través del tristemente célebre espectáculo del "acuerdo de precios", como si éstos indicadores fueran consecuencia de decisiones independientemente de lo ofrecido y demandado en cada reglón. Si lo acordado es más bajo que el precio de mercado habrán faltantes y si es más alto habrán sobrantes, esto resulta inexorable.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Se dice que las integraciones regionales constituyen un primer paso para la liberación al mundo pero este "primer paso" lleva siglos ya que los economistas clásicos refutaron las tesis mercantilistas en el siglo XIII. Es increíble que las tecnologías hayan reducido notablemente los costos de transporte desde la época de las carretas y ahora resulta que al llegar la mercadería a la aduana se revierten décadas y décadas, como si esas reducciones colosales no hubieran tenido lugar. Es como ha dicho irónicamente el decimonónico Bastiat para ridiculizar al mal llamado proteccionismo, "los productores de velas debe aconsejar el tapiar todas las ventanas para protegerse de la competencia desleal del sol". Comprar barato es de sentido común, pero las xenofobias no ayudan a la comprensión de este precepto básico para prosperar. Es como reza el conocido dictum de Voltaire "el sentido común es el menos común de los sentidos".</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: <a href="http://www.libertadyprogresonline.org/2013/05/30/librecambio-y-sentido-comun/">Libertad y Progreso&nbsp;</a></p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/77-el-ciudadano-perfecto">
			&laquo; El ciudadano perfecto		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/113-la-puerta-de-los-monstruos">
			La puerta de los monstruos &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/104-libre-cambio-y-sentido-común#startOfPageId104">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:38:"Libre cambio y sentido común - Relial";s:11:"description";s:156:"El análisis de Alberto Benegas Lynch &amp;nbsp;&amp;nbsp; &amp;nbsp; Estamos otra vez en plena época del mercantilismo que irrumpió en el siglo XVII e...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:29:"Libre cambio y sentido común";s:6:"og:url";s:96:"http://www.relial.org/index.php/productos/archivo/opinion/item/104-libre-cambio-y-sentido-común";s:8:"og:title";s:38:"Libre cambio y sentido común - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:168:"El análisis de Alberto Benegas Lynch &amp;amp;nbsp;&amp;amp;nbsp; &amp;amp;nbsp; Estamos otra vez en plena época del mercantilismo que irrumpió en el siglo XVII e...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:29:"Libre cambio y sentido común";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}