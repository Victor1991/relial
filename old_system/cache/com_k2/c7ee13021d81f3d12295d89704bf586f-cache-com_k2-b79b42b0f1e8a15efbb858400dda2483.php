<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5589:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId218"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Partidos políticos de RELIAL se reunieron en Guatemala
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/aa5045f13216477abf2a0e16a08acd59_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/aa5045f13216477abf2a0e16a08acd59_XS.jpg" alt="Partidos pol&iacute;ticos de RELIAL se reunieron en Guatemala" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Dando continuidad a los esfuerzos por mejorar y dinamizar la relación entre los partidos políticos y los think tanks de RELIAL, se llevó a cabo un encuentro con los partidos miembros de la red, quienes se dieron cita en Antigua para ser parte de la reunión de Comité Ejecutivo de la Internacional Liberal.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>En esta reunión, los partidos políticos ocuparon un papel central: cada uno explicó la estructura y situación de sus partidos en relación a la problemática actual de sus países; asimismo, expusieron necesidades de sus organizaciones, alrededor de las cuales los think tanks de RELIAL pudieran colaborar.</p>
<p>&nbsp;</p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="images/working%20group/Guatemala_10.2013%20153.JPG" alt="Guatemala 10.2013 153" width="448" height="336" /></p>
<p>&nbsp;</p>
<p><a href="index.php/multimedia/fotos/item/214-partidos-pol%C3%ADticos-de-relial-se-reunieron-en-guatemala">ver galeria</a></p>
<p>&nbsp;</p>
<p>Entre los aspectos claves para mejorar esta relación de cooperación, se destaca la importancia de la formación ideológica de los cuadros, la capacitación de las nuevas generaciones y el asesoramiento en temas de agenda para la formulación de políticas públicas de corte liberal.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Agradecimiento Institucional</p>
<p>&nbsp;</p>
<p>RELIAL agradece a la Universidad Francisco Marroquín (UFM), por haber permitido llevar a cabo sus reuniones de trabajo en la Casa Popenoe, inmueble histórico en la ciudad de La Antigua, Guatemala.</p>
<p>&nbsp;</p>
<p><a href="index.php/multimedia/fotos/item/215-relial-visita-la-ufm">Link a la galería</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/209-relial-dio-la-bienvenida-a-la-internacional-liberal">
			&laquo; RELIAL dio la bienvenida a la Internacional Liberal		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/219-encuentro-clave-entre-directivos-il-–-relial">
			Encuentro clave entre Directivos IL – RELIAL &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/218-partidos-políticos-de-relial-se-reunieron-en-guatemala#startOfPageId218">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:64:"Partidos políticos de RELIAL se reunieron en Guatemala - Relial";s:11:"description";s:156:"Dando continuidad a los esfuerzos por mejorar y dinamizar la relación entre los partidos políticos y los think tanks de RELIAL, se llevó a cabo un enc...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:55:"Partidos políticos de RELIAL se reunieron en Guatemala";s:6:"og:url";s:125:"http://www.relial.org/index.php/productos/archivo/actualidad/item/218-partidos-políticos-de-relial-se-reunieron-en-guatemala";s:8:"og:title";s:64:"Partidos políticos de RELIAL se reunieron en Guatemala - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/aa5045f13216477abf2a0e16a08acd59_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/aa5045f13216477abf2a0e16a08acd59_S.jpg";s:14:"og:description";s:156:"Dando continuidad a los esfuerzos por mejorar y dinamizar la relación entre los partidos políticos y los think tanks de RELIAL, se llevó a cabo un enc...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:55:"Partidos políticos de RELIAL se reunieron en Guatemala";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}