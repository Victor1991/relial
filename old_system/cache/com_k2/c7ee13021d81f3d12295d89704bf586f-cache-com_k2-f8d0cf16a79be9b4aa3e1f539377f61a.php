<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8632:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId433"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Colombia y la farsa de la reconciliación
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f68bc1606a499c66a1eabd66e99d6817_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f68bc1606a499c66a1eabd66e99d6817_XS.jpg" alt="Colombia y la farsa de la reconciliaci&oacute;n" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>El presidente Juan Manuel Santos ha llevado a algunas víctimas a La Habana para que se reconcilien con sus verdugos. La idea detrás de la ceremonia se origina en las terapias sicológicas. Es una extensión de los procesos de sanación de las parejas en las que se produce un agravio severo. Quien cometió la falta asume la culpa, se arrepiente, y la víctima perdona. A partir de ese punto retoman la relación y, poco a poco, se restauran los vínculos emocionales. Sin ese proceso es difícil la recuperación de la confianza en el otro.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>El problema de ese modelo de terapia es que sólo funciona entre individuos, no colectivamente. Es probable que las víctimas realmente perdonen, porque se liberan de la angustia que producen el odio y el deseo de venganza. No obstante, es muy raro, casi inexistente, el arrepentimiento de quienes cometen crímenes contra "enemigos de clase" mientras luchan por causas que a ellos les parecen justas.</p>
<p>&nbsp;</p>
<p>El Che Guevara lo expresó en una frase sincera y elocuente: "El odio como factor de lucha, el odio intransigente al enemigo, que impulsa más allá de las limitaciones naturales del ser humano y lo convierte en una eficaz, violenta, selectiva y fría máquina de matar".</p>
<p>&nbsp;</p>
<p>¿Se imagina alguien a Guevara o a Stalin avergonzados y contritos por sus asesinatos? ¿O a Hitler, Mussolini, Franco, Pinochet o Videla? ¿Puede alguien creer que Tiro Fijo o Mono Jojoy estarían dispuestos a arrepentirse de sus crímenes "revolucionarios"? ¿Lo está Timoshenko, el actual jefe de las FARC?</p>
<p>&nbsp;</p>
<p>La Habana tampoco es el lugar ideal para intentar la reconciliación. La Isla no es, precisamente, el cantón de Basilea. ¿Se arrepienten los anfitriones cubanos de los miles de fusilados, de la persecución a los homosexuales, de los actos de repudio? ¿Se arrepienten Fidel y Raúl Castro de haber hundido un barco cargado de refugiados en el que se ahogaron dos docenas de niños, o del derribo sobre aguas internacionales de dos avionetas desarmadas que auxiliaban balseros? ¿Se arrepienten de la muerte de Oswaldo Payá y de Harold Cepero?</p>
<p>&nbsp;</p>
<p>Los tupamaros, los montoneros, los escuadrones de la muerte de la derecha asesina, las narcoguerrilas comunistas de las FARC y los narcoparamilitares que los combatían, todos esos grupos violentos y delirantes, a la derecha y a la izquierda, no creen que tienen nada de qué arrepentirse. Están llenos de justificaciones y coartadas ideológicas y políticas.</p>
<p>&nbsp;</p>
<p>Hace años, intrigado por esa falta de empatía, le pregunté a una persona que había "ejecutado" a trece enemigos políticos si sentía algún remordimiento. Paradójicamente, era un hombre bueno y tierno en el ámbito familiar. Incluso, era tímido y compasivo. Los había matado unas veces por medio de atentados y otras en balaceras provocadas por los otros. Eran crímenes políticos. Me miró con asombro y me respondió sin la menor vacilación: "sí, me remuerde la conciencia por todos los que se me escaparon". Y luego procedió a relatarme varios intentos fallidos de quitarles la vida a otros pistoleros violentos.</p>
<p>&nbsp;</p>
<p>No se puede creer en estos procesos colectivos de reconciliación. Suelen ser una farsa. A mi juicio, las narcoguerrillas comunistas de las FARC están dispuestas a abandonar las armas, pero sólo para tratar de llegar al gobierno por la vía chavista de un proceso electoral. No han renunciado a conquistar el poder ni a crear una dictadura colectivista, sino al método hasta ahora empleado. Realmente, no piden perdón. Juegan a ello. (París, ya se sabe, bien vale una misa).</p>
<p>&nbsp;</p>
<p>Con cien o docientos millones de dólares que les proporcionen el narcotráfico, más lo que aporte Venezuela, y agazapados tras el mascarón de proa de un rostro izquierdista potable, como hicieron los comunista en El Salvador escudados tras Mauricio Funes, van a tratar de llegar a la Casa de Nariño "legalmente", aprovechando las divisiones y la debilidad de los grupos democráticos. Una vez ocupada la poltrona comenzaría la fiesta clientelista y prebendaria hasta reclutar a una precaria mayoría y con ella desmantelar totalmente los fundamentos de la República.</p>
<p>&nbsp;</p>
<p>Santos lo sabe, pero su objetivo, como el de media Colombia, es terminar la guerra a cualquier precio. Veremos si luego los colombianos consiguen mantener las libertades y ganar la partida. Ojalá que "estalle la paz", pero que ése no sea el inicio de otra expresión del horror.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaner</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: El bol de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos">
			&laquo; El sistema liberal ha mejorado la vida de los peruanos		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/434-seminario-“fortaleciendo-las-ongs”">
			Seminario “Fortaleciendo las ONGs” &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación#startOfPageId433">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:50:"Colombia y la farsa de la reconciliación - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; El presidente Juan Manuel Santos ha llevado a algunas víctimas a La Habana para que se reconcilien...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:41:"Colombia y la farsa de la reconciliación";s:6:"og:url";s:108:"http://www.relial.org/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación";s:8:"og:title";s:50:"Colombia y la farsa de la reconciliación - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f68bc1606a499c66a1eabd66e99d6817_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f68bc1606a499c66a1eabd66e99d6817_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; El presidente Juan Manuel Santos ha llevado a algunas víctimas a La Habana para que se reconcilien...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:41:"Colombia y la farsa de la reconciliación";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}