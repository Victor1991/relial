<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8379:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId85"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El papel higiénico y el socialismo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>&nbsp;</p>
<p><br /><img src="images/logos/montaner%20.jpg" alt="montaner " width="120" height="71" style="float: left;" />&nbsp;El análisis de Carlos Alberto Montaner<span style="font-size: 11px;">&nbsp;</span></p>
<p>&nbsp;</p>
<p>&nbsp;Nicolás Maduro afronta la crisis del papel higiénico. Los venezolanos están indignados. Hay escasez en el país. Han debido importar urgentemente 50 millones de rollos por temor a desórdenes populares.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Nadie sabe por dónde puede comenzar una rebelión popular. (Es la primera vez que se va a convocar a las barricadas a una muchedumbre de gentes sentadas).</p>
<p>Yoani Sánchez, que es muy práctica, les ha sugerido que le pidan a Cuba una edición diaria del periódico Granma. Los cubanos hace medio siglo que utilizan el Granma para ese asunto oscuro, solitario y delicado. Nadie toma en serio su contenido, pero todos coinciden en que el continente soluciona un problema generalmente cotidiano.</p>
<p>Es verdad que cuando la tinta tiene demasiado plomo, o cuando la textura es muy áspera, la zona se resiente y pica, pero el régimen lo justifica explicando que es la consecuencia del duro bloqueo de los pérfidos gringos.</p>
<p>Sólo que ésa es una oportunidad magnífica de convertir el revés en victoria. Es entonces cuando cobra todo su significado la heroica consigna revolucionaria: "lucharemos con las uñas contra el imperialismo yanqui". (Eso: con las uñas, pero sin pasarse para no hacerse daño).</p>
<p>Seamos justos. Es importante no dejarse llevar por las pasiones. Es cierto que el socialismo ha provocado la escasez de papel higiénico, pero el sistema también atenúa las consecuencias.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Los venezolanos cada vez comerán menos, ergo, lo presumible es que necesiten cantidades decrecientes de ese producto superfluo consumido, fundamentalmente, por la decadente burguesía.</p>
<p>Según los cálculos del Ministerio de Planificación, un sesudo equipo de investigadores dirigido por el señor Jorge Giordani, dada la ingestión, digestión y deyección de fibra prevista para el próximo quinquenio –el socialismo del Siglo XXI todo lo prevé y calcula–, es posible que en el 2018 bastará un confeti para que cada venezolano mantenga gloriosamente resplandeciente el orificio de salida.</p>
<p>Pero hay más. Tal vez antes de la llegada de esa fecha, Fidel Castro, si persiste en sus ensayos genéticos, haya resuelto el problema con un hombre nuevo que, además de parecerse al Che en sus valores morales, nacerá con un aparato digestivo modificado para solucionar revolucionariamente ese urticante problema. Ya lo ha advertido jubilosamente: "con patria, pero sin ano".</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>¿Por qué faltan en Venezuela el papel higiénico, el pollo, la leche, la harina para arepas, el jabón y así hasta el 21% de los productos habitualmente consumidos por los venezolanos?</p>
<p>Según el señor Maduro (no se sabe si de su propia cosecha o por confesión de algún pajarito delator), se debe a los acaparadores y a los canallas productores que quieren perjudicar su labor para generar la insubordinación popular.</p>
<p>Según la experiencia acumulada a lo largo de un siglo, la culpa está en otra parte: en la planificación y en la asignación artificial de los precios.</p>
<p>Esto se lo advirtió inútilmente Ludwig von Mises a Lenin en 1921 en una serie de artículos, luego reunidos en un libro, titulado Socialismo.</p>
<p>Los burócratas, por muy instruidos que sean, no pueden decidir eficientemente qué, cuánto o cuándo debe y quiere consumir la sociedad.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>No hay mejor mecanismo para construir la prosperidad y para abastecer a una sociedad apropiadamente que las decisiones que toma el consumidor soberano con su dinero, indicándoles con sus preferencias al productor y al comerciante lo que debe ofertarle y qué precio está dispuesto a pagar.</p>
<p>Por eso es absurdo decidir arbitrariamente los precios. El precio es el lenguaje que se habla en el mundo del mercado. Mientras más variada y copiosa sea la oferta, menores serán los precios porque la competencia será más intensa.</p>
<p>Si Estados Unidos es hoy una de las economías más "baratas" del planeta es porque existen cuarenta marcas de papel higiénico que tienen que competir en precio y calidad para conquistar las preferencias del consumidor.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Hasta ahora, no existe manera alguna de sustituir eficazmente el libre intercambio productor-comerciante-consumidor, expresado por medio de los precios y la competencia.</p>
<p>Milton Friedman solía decir que si se pusiera al frente del desierto del Sahara a un gobierno planificador, al cabo de pocos años tendría que importar arena. Además del papel higiénico, claro.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: <a href="http://www.elblogdemontaner.com/">Carlos Alberto Montaner</a>, Miembro de la Junta Honorífica de RELIAL</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/84-cuenta-pública-no-perdamos-su-sentido">
			&laquo; Cuenta pública, no perdamos su sentido		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/114-tax-freedom-day-2013-en-rio-grande-do-sul-novena-edición">
			Tax Freedom Day 2013 en Rio Grande do Sul - novena. edición &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/85-el-papel-higiénico-y-el-socialismo#startOfPageId85">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:44:"El papel higiénico y el socialismo - Relial";s:11:"description";s:156:"&amp;nbsp; &amp;nbsp;El análisis de Carlos Alberto Montaner&amp;nbsp; &amp;nbsp; &amp;nbsp;Nicolás Maduro afronta la crisis del papel higiénico. Los v...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:35:"El papel higiénico y el socialismo";s:6:"og:url";s:106:"http://www.relial.org/index.php/productos/archivo/actualidad/item/85-el-papel-higiÃ©nico-y-el-socialismo";s:8:"og:title";s:44:"El papel higiénico y el socialismo - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:176:"&amp;amp;nbsp; &amp;amp;nbsp;El análisis de Carlos Alberto Montaner&amp;amp;nbsp; &amp;amp;nbsp; &amp;amp;nbsp;Nicolás Maduro afronta la crisis del papel higiénico. Los v...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:35:"El papel higiénico y el socialismo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}