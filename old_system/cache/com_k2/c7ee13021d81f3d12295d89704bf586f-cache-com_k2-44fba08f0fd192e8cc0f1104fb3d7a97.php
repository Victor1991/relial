<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9115:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId402"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El buen salvaje
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/bbdd9bab523659f72e79235cef0a8565_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/bbdd9bab523659f72e79235cef0a8565_XS.jpg" alt="El buen salvaje" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><span><span>En la opinión de Hugo Vera Ojeda</span></span></p>
<p>&nbsp;</p>
<p>¿Quien no ha escuchado alguna vez el dicho de que todo tiempo pasado fue mejor? Esto aunque pareciera indicar un tiempo más mediato y relacionado a nuestra propia vida, en realidad cala más profundamente en el imaginario de la gente. Muchos creen que efectivamente ha existido una era de oro o paraíso terrenal, como también lo describen los textos sagrados de no pocas religiones y en donde existía un hombre bueno, sin concepto del mal. Lo que lejos de ser una inocente creencia esta induce a sostener que es mejor destruir el orden actual y volver a esa era de oro o primitiva en donde no existía la noción de la maldad pues no existía la propiedad privada.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Las ideas del fin del mundo, la destrucción del orden actual o las limpiezas étnicas y el posterior advenimiento de una era dorada, un nuevo orden o una raza superior, son ramas de un mismo tronco ideológico. Esta idea mitológica esta tan metida en la gente que hasta convive con su arte como canciones, literatura o películas que se vuelven fuertes canales emocionales. Por citar algunos ejemplos, tenemos la famosísima canción Imagíne de John Lenon, en donde habla de lo maravilloso que sería un mundo sin fronteras ni posesiones, libros como Pocahontas o películas como Avatar, en donde se dejan patentes la superioridad del buen salvaje en armonía con la naturaleza y sin nociones de la propiedad privada ni la maldad.</p>
<p>&nbsp;</p>
<p>No pocos pensadores de la ilustración han pergeñado&nbsp; la idea de un hombre no civilizado, sin historia, política y sociabilidad a quien podrían moldear según sus parámetros. La idea de un estado natural del hombre era el resultado de un experimento teórico, designaba el residuo que quedaría después de desproveer al individuo moderno de todo lo que él es artificial. Esta idea incluso ha motivado el nacimiento de tres escuelas que discutieron su esencia, la de Rosseau, que sostuvo la superioridad ética de un hipotético hombre salvaje que viviendo en perfecta armonía con la naturaleza, no conocería la maldad producida por la cultura.</p>
<p>&nbsp;</p>
<p>La segunda escuela, era la encabezada por el ingles Thomas Hobbes que tenia la hipótesis contraria, la superioridad del hombre civilizado sobre el salvaje y finalmente la escuela del filosofo y escritor francés Michel de Montaigne que sostenía una hipótesis ecléctica o intermedia, según el cual los usos y costumbres morales de los pueblos no se pueden incluir en una escala de valores unitaria, ya que están determinados geográficamente.</p>
<p>&nbsp;</p>
<p>Aunque estas creencias chocaron frontalmente con la realidad corroborada por no pocos colonizadores que eran testigos de la brutalidad de los hombres con practicas primitivas, incluidas las de canibalismos, la idea rosseuniana, es la que más ha influenciado en no pocas generaciones. Hasta hoy día es muy atractiva la idea del buen salvaje que muestre el camino del retorno a la naturaleza de los hombres civilizados que están destruyendo el mundo. No son pocos los movimientos que aun hoy generan toda una filosofía de vida en contra de la evolución y el desarrollo del ser humano, despreciando su tecnología y abogando por la destrucción del capitalismo, pues lo cree el causante de todos los desastres de la naturaleza. Muchas culturas están muy fanatizadas con esa concepción.</p>
<p>&nbsp;</p>
<p>En la cultura paraguaya, muy influenciada con la guaraní, es muy fuerte la creencia en el mito del Yvymarae´ÿ o tierra sin mal, en donde el maíz crece solo y los hombres son inmortales. Incluso el icono de su literatura, Augusto Roa Bastos, le ha dedicado una obra de teatro con el nombre La Tierra sin mal. En otras, como el caso de Bolivia y Ecuador, los mitos precolombinos están insertadas en su propia constitución, como el caso del&nbsp; sumak kawsay o buen vivir, en la constitución ecuatoriana, el suma qamaña o vivir bien o la versión paraguaya de&nbsp; Yvymarae´ÿ , llamada ivi maraei en la constitución boliviana. Por supuesto el contexto de esta inserciones están más bien dirigidas a romper la tradición occidental de la propiedad privada, pues bajo estos enunciados se reconoce supuestos derechos ancestrales por encima de los derechos de propiedad.</p>
<p>&nbsp;</p>
<p>Esto tiene una implicancia destructiva mucho más profunda de lo imaginado, pues de cierta forma se busca hacer mella y finalmente destruir, el sistema capitalista para buscar el retorno a la era de oro en donde no existía el mal, pues no se tenía el concepto de propiedad privada y había una armonía perfecta entre el hombre y la naturaleza, como se mencionara más arriba. Estos son solo ejemplos de cuan influenciado esta el pensamiento humano con la búsqueda del orden nuevo y su retorno a la era dorada que no se diferencia al paraíso terrenal bíblico o el paraíso socialista marxista.&nbsp;<span style="line-height: 115%; font-family: 'Agfa Rotis Sans Serif';"><strong> &nbsp;</strong></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span><span>Foto: Fundación Libertad Paraguay</span></span></p>
<p><span><span>Autor: Hugo Vera Ojeda, Presidente de la Fundación Libertad Paraguay&nbsp;</span></span></p>
<p><span><span>Fuente: Texto proporcionado porción el autor</span></span></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/400-política-local-y-participación-ciudadana">
			&laquo; Política local y participación ciudadana		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/404-recuerden-que-el-socialismo-es-imposible">
			Recuerden que el socialismo es imposible &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/402-el-buen-salvaje#startOfPageId402">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:24:"El buen salvaje - Relial";s:11:"description";s:155:"En la opinión de Hugo Vera Ojeda &amp;nbsp; ¿Quien no ha escuchado alguna vez el dicho de que todo tiempo pasado fue mejor? Esto aunque pareciera indi...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:15:"El buen salvaje";s:6:"og:url";s:82:"http://www.relial.org/index.php/productos/archivo/opinion/item/402-el-buen-salvaje";s:8:"og:title";s:24:"El buen salvaje - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/bbdd9bab523659f72e79235cef0a8565_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/bbdd9bab523659f72e79235cef0a8565_S.jpg";s:14:"og:description";s:159:"En la opinión de Hugo Vera Ojeda &amp;amp;nbsp; ¿Quien no ha escuchado alguna vez el dicho de que todo tiempo pasado fue mejor? Esto aunque pareciera indi...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:15:"El buen salvaje";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}