<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8788:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId406"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El misterioso caso de los comunistas incapaces de aprender
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f203d630ce0c3265f9c9461092194e6b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f203d630ce0c3265f9c9461092194e6b_XS.jpg" alt="El misterioso caso de los comunistas incapaces de aprender" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Jorge Giordani es un viejo comunista que hasta hace pocas fechas fue el Ministro de Planificación y Finanzas del chavismo, primero con Hugo Chávez y luego con Nicolás Maduro. Tiene fama de haber sido un funcionario honrado en un gobierno en el que abundan los rateros.</p>
<p>&nbsp;</p>
<p>Nadie, sin embargo, ha acusado a Giordani de ser competente. Sería una peligrosa temeridad. No se metía la plata de los demás en el bolsillo. Lo que hacía era destruirla en esa trituradora implacable de riqueza que es la ideología marxista. Es uno de los responsables del hundimiento económico del país. Cuando llegó al poder había seis millones y medio de pobres. Cuando lo dejó, hace unos días, la cifra había aumentado a más de nueve.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>Giordani se despidió del cargo con una larga carta en la que culpa a los demás del desastre económico venezolano. Sus culpables son el irresponsable gasto público, la corrupción, PDVSA y el pobre Nicolás Maduro, quien supuestamente ha traicionado al socialismo y al legado inmarcesible de Hugo Chávez. (Inmarcesible, Nicolás, quiere decir que no se marchita. Y marchita no es una marcha pequeña de estudiantes indignados, sino un verbo que procede del latín).</p>
<p>&nbsp;</p>
<p>El ingeniero Giordani no es capaz de advertir que el error intelectual está en el presupuesto ideológico. Cuando se debilitan los derechos de propiedad y las decisiones económicas las toman los funcionarios; cuando se potencia la aparición del estado-empresario y se estatiza el aparato productivo; cuando se eliminan las principales libertades porque la crítica se convierte en traición a la patria; inevitablemente surge la escasez, se deteriora progresivamente el entorno físico por falta de mantenimiento, y comienza un acelerado proceso de empobrecimiento colectivo que no tiene fin ni alivio. Mañana siempre será peor que hoy.</p>
<p>&nbsp;</p>
<p>Mientras los venezolanos leían la carta de Giordani, los cubanos, asombrados, repasaban otra misiva escrita por el comunista, escritor y exembajador Rolando López del Amo, jubilado en La Habana tras haber ocupado diversos cargos de primer rango en la diplomacia castrista. El texto puede localizarse en Internet, donde circula profusamente (<a href="http://www.cubano1erplano.com/2014/06/cuba-una-vez-mas-sobre-la-burocracia-y.html">http://www.cubano1erplano.com/2014/06/cuba-una-vez-mas-sobre-la-burocracia-y.html#</a>)</p>
<p>&nbsp;</p>
<p>El señor López del Amo tiene una explicación parcialmente diferente a la de Giordani. Supone que el responsable del desastre cubano es el burocratismo, ese enmarañado ejército de funcionarios indolentes que no deja que el país avance. Como es una persona seria, no culpa al embargo norteamericano, ni a la sequía, ni a los ciclones, porque el país no padece hace tiempo estos fenómenos naturales. Cree que el mal está en otra parte: es la malvada gente que entorpece la marcha gloriosa del socialismo.</p>
<p>&nbsp;</p>
<p>Termina su carta con un conmovedor llamado a sus camaradas: "Estamos en el año 56 de nuestra experiencia revolucionaria y no podemos continuar cometiendo los mismos errores ni ofreciendo las mismas justificaciones. Se impone un cambio de mentalidad, de actitud, de estructuras y de personas para lograr el sueño colectivo de un socialismo próspero y sostenible".</p>
<p>&nbsp;</p>
<p>¡Madre mía! Estamos ante un comunista inaccesible al desaliento. ¡Qué gente más dura de molleras! Cincuenta y seis años de fracasos continuados y barbarie, de "oprobio y bobería", como Borges decía del peronismo, no le han bastado para entender que el sistema no sirve para nada en ninguna latitud. Ni con los laboriosos alemanes o norcoreanos, ni con los muy serios checos y húngaros, y mucho menos con los caribeños de Cuba o Venezuela.</p>
<p>&nbsp;</p>
<p>Es posible, sin embargo, que Raúl Castro, finalmente, haya comprendido esta dolorosa verdad. Lo triste es que la educación del hermano de Fidel ha durado más de medio siglo y costado miles de vidas y la ruina completa de una nación. (Fidel, en cambio, es indiferente a la realidad y morirá defendiendo las mismas tonterías de siempre). En todo caso, mientras el embajador López del Amo escribía su carta, el zar de la economía cubana, un excoronel llamado Marino Murillo, anunciaba que todos los restaurantes del país serían privatizados.</p>
<p>&nbsp;</p>
<p>Es el principio del fin del loco proyecto marxista del colectivismo, pero no de la dictadura. Ahora, poco a poco, sin prisa, pero sin tregua, como le gusta repetir a Raúl Castro, quieren desmantelar el socialismo y gobernar con mano férrea un país pseudo capitalista. Ya no son marxistas. Son, simplemente, una banda autoritaria de gente decidida a mandar a palos. Puros matones.</p>
<p>&nbsp;</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/404-recuerden-que-el-socialismo-es-imposible">
			&laquo; Recuerden que el socialismo es imposible		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/409-españa-bienvenido-mr-chávez">
			España: Bienvenido, Mr. Chávez &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/406-el-misterioso-caso-de-los-comunistas-incapaces-de-aprender#startOfPageId406">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:67:"El misterioso caso de los comunistas incapaces de aprender - Relial";s:11:"description";s:155:"Jorge Giordani es un viejo comunista que hasta hace pocas fechas fue el Ministro de Planificación y Finanzas del chavismo, primero con Hugo Chávez y l...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:58:"El misterioso caso de los comunistas incapaces de aprender";s:6:"og:url";s:125:"http://www.relial.org/index.php/productos/archivo/opinion/item/406-el-misterioso-caso-de-los-comunistas-incapaces-de-aprender";s:8:"og:title";s:67:"El misterioso caso de los comunistas incapaces de aprender - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f203d630ce0c3265f9c9461092194e6b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f203d630ce0c3265f9c9461092194e6b_S.jpg";s:14:"og:description";s:155:"Jorge Giordani es un viejo comunista que hasta hace pocas fechas fue el Ministro de Planificación y Finanzas del chavismo, primero con Hugo Chávez y l...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:58:"El misterioso caso de los comunistas incapaces de aprender";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}