<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8954:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId409"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	España: Bienvenido, Mr. Chávez
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/bc61c35998920c79a57e03ad91265e8b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/bc61c35998920c79a57e03ad91265e8b_XS.jpg" alt="Espa&ntilde;a: Bienvenido, Mr. Ch&aacute;vez" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>por Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>La idiotez política está al alcance de cualquier pueblo. Ninguna sociedad está libre de recorrer ese camino. Quien lo dude, debe pensar en la Alemania de Hitler, la Italia de Mussolini, la Cuba de los Castro o la Venezuela de Hugo Chávez. Sobran los ejemplos.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>"Podemos" es un partido político chavista, oficialmente creado hace pocos meses en España. Pablo Iglesias es su cara más visible. Se trata de un joven profesor universitario, desaliñado, con barba rala y cola de caballo, quien no vacila en defender el uso de la guillotina para traerle la felicidad a la sociedad española.</p>
<p>&nbsp;</p>
<p>El personaje y su partido han entrado en la vida pública española sorpresivamente. En las elecciones al Parlamento Europeo, la novísima organización obtuvo 1 200 000 votos y cinco escaños. Esto ha desatado las alarmas.</p>
<p>&nbsp;</p>
<p>El calificativo de chavista a "Podemos" no es gratuito, sino todo lo contrario. Ha sido muy costoso. De acuerdo con una investigación llevada a cabo por el diario El País, los directivos españoles de esa organización, por medio de una Fundación, han recibido unos cuatro millones de dólares a lo largo de los años en concepto de "asesorías" por parte de la Venezuela de Hugo Chávez.</p>
<p>&nbsp;</p>
<p>Por el tipo de medidas llevadas a cabo por el chavismo se pueden deducir las creencias que prevalecen en "Podemos". Forman parte del extendido grupo de fabricantes de miseria que militan tras las banderas del populismo. Cuando Chávez llegó al poder había seis millones y medio de pobres. Hoy existen más de nueve y el país padece todo tipo de escaseces en medio de la mayor violencia. En Caracas, dicen, ya no se contabilizan los habitantes sino los supervivientes.</p>
<p>&nbsp;</p>
<p>¿Pueden semejantes personajes llegar al poder en España en un futuro próximo? El periodista Federico Jiménez Losantos, quien procede del Partido Comunista y ha evolucionado hacia el liberalismo, afirma que sí, y yo creo que tiene razón. Pudiera ocurrir.</p>
<p>&nbsp;</p>
<p>Todos los elementos están dados para la tormenta perfecta. Los dos grandes partidos nacionales –los populares y los socialistas– se han desacreditado por culpa de la corrupción. Hay un 25% de desempleo, que alcanza el 50 entre los más jóvenes. El separatismo catalán y vasco va en aumento. La monarquía cuenta con un tibio respaldo. Muchos jóvenes bien preparados emigran hacia otras naciones porque no encuentran posibilidades de prosperar en España.</p>
<p>&nbsp;</p>
<p>Las propuestas de "Podemos" son totalmente disparatadas, pero eso jamás ha sido un obstáculo para ganar elecciones en épocas de crisis. Prometen repartirlo todo porque, para esta gente, corta de entendederas, el problema no está en la limitada generación de riquezas por la debilidad del tejido empresarial, sino en la mala distribución de ella.</p>
<p>&nbsp;</p>
<p>Creen que la forma de combatir el desempleo es repartir el trabajo "justamente". Si el 100% de los trabajadores sólo trabajaran el 75% de la jornada laboral, el 25 que no encuentra empleo podría hallarlo.</p>
<p>&nbsp;</p>
<p>Creen que, si se reduce la edad de la jubilación a los 60 años, en lugar de los 65 hoy vigentes, habría más empleos disponibles y más tiempo para disfrutar del ocio. (¿Por qué no a los 50?).</p>
<p>&nbsp;</p>
<p>Creen que basta con despojar de sus excedentes a los que tienen, para otorgarle a toda persona radicada en la Península un subsidio "digno" que le permita vivir decorosamente.</p>
<p>&nbsp;</p>
<p>Es decir, "Podemos" promete terminar con la crisis esforzándose menos, en lugar de más, que es lo que dictan el sentido común y la experiencia. Pero esas propuestas, que arruinan a cualquier sociedad, son gratas a los oídos de muchos electores.</p>
<p>&nbsp;</p>
<p>¿Son demócratas los miembros de Podemos? No lo creo. No se puede ser chavista y demócrata simultáneamente, de la misma manera que no se puede apoyar al fascismo o al comunismo y creer en la libertad. Es lo que los clásicos llamaban contradictio in terminis, algo así como un cuadrado redondo.</p>
<p>&nbsp;</p>
<p>La vía electoral, eso sí, es la fórmula para llegar al poder y, desde allí dinamitar las instituciones que sostienen el Estado de Derecho. Esto es lo que han llevado a cabo la Venezuela de Chávez y Maduro y, hasta cierto punto, el Ecuador de Rafael Correa.</p>
<p>&nbsp;</p>
<p>¿Cómo pueden llegar al poder semejantes personajes? Jiménez Losantos piensa que mediante un pacto con los socialistas, los comunistas de Izquierda Unida y los separatistas más radicales de Cataluña y el País Vasco. Un nuevo Frente Popular que se inclinaría paulatinamente hacia el estatismo-colectivista-populista, que derrote al centro derecha en las urnas y precipite a España en una etapa revolucionaria.</p>
<p>&nbsp;</p>
<p>¡Dios nos coja confesados!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: El blog de Montaner</p>
<p>Foto: EL Blog de Montaner</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/406-el-misterioso-caso-de-los-comunistas-incapaces-de-aprender">
			&laquo; El misterioso caso de los comunistas incapaces de aprender		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/412-hacia-un-sistema-de-pensiones-justo-y-equitativo">
			Hacia un sistema de pensiones justo y equitativo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/409-españa-bienvenido-mr-chávez#startOfPageId409">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:41:"España: Bienvenido, Mr. Chávez - Relial";s:11:"description";s:156:"por Carlos Alberto Montaner &amp;nbsp; La idiotez política está al alcance de cualquier pueblo. Ninguna sociedad está libre de recorrer ese camino. Qu...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:32:"España: Bienvenido, Mr. Chávez";s:6:"og:url";s:136:"http://relial.org/index.php/productos/archivo/opinion/item/409-espa%C3%B1a-bienvenido-mr-ch%C3%A1vez/index.php/productos/archivo/opinion";s:8:"og:title";s:41:"España: Bienvenido, Mr. Chávez - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/bc61c35998920c79a57e03ad91265e8b_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/bc61c35998920c79a57e03ad91265e8b_S.jpg";s:14:"og:description";s:160:"por Carlos Alberto Montaner &amp;amp;nbsp; La idiotez política está al alcance de cualquier pueblo. Ninguna sociedad está libre de recorrer ese camino. Qu...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:32:"España: Bienvenido, Mr. Chávez";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}