<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4105:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId502"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Education Policies – The Key to Enlightenment, Participation and Progress
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/9b8bb3eb74bd3cd97bb3679c60950d61_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/9b8bb3eb74bd3cd97bb3679c60950d61_XS.jpg" alt="Education Policies &ndash; The Key to Enlightenment, Participation and Progress" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item text -->
	  <div class="itemFullText">
	  	<p style="text-align: center;">Education Policies – The Key to Enlightenment, Participation and Progress<br />8 – 20 November 2015<br />Gummersbach and Stuttgart</p>	  </div>
	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
  
  
  
	
  
	<div class="clr"></div>

    <!-- Item video -->
  <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

  <div class="itemVideoBlock">
  	<h3>K2_MEDIA</h3>

				<span class="itemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:500px;" class="avPlayerContainer">
		<div id="AVPlayerID_b23762ae_1896833726" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/nt7Kfmg7zFM?rel=0&amp;fs=1&amp;wmode=transparent" width="500" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		
	  
	  
	  <div class="clr"></div>
  </div>
  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/multimedia/videos/item/502-education-policies-–-the-key-to-enlightenment-participation-and-progress#startOfPageId502">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:84:"Education Policies – The Key to Enlightenment, Participation and Progress - Relial";s:11:"description";s:122:"Education Policies – The Key to Enlightenment, Participation and Progress8 – 20 November 2015Gummersbach and Stuttgart";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:75:"Education Policies – The Key to Enlightenment, Participation and Progress";s:6:"og:url";s:83:"http://www.relial.org/index.php/productos/archivo/opinion/item/502-thomas-jefferson";s:8:"og:title";s:84:"Education Policies – The Key to Enlightenment, Participation and Progress - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/9b8bb3eb74bd3cd97bb3679c60950d61_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/9b8bb3eb74bd3cd97bb3679c60950d61_S.jpg";s:14:"og:description";s:122:"Education Policies – The Key to Enlightenment, Participation and Progress8 – 20 November 2015Gummersbach and Stuttgart";}}s:5:"links";a:0:{}s:11:"styleSheets";a:3:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:72:"/plugins/content/jw_allvideos/jw_allvideos/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:11:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:78:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/mediaplayer/jwplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:79:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/silverlight.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:77:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/wmvplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:86:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/quicktimeplayer/AC_QuickTime.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:6:"Videos";s:4:"link";s:28:"/index.php/multimedia/videos";}i:4;O:8:"stdClass":2:{s:4:"name";s:75:"Education Policies – The Key to Enlightenment, Participation and Progress";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}