<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16669:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Comunicados</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
	  		RELIAL alerta sobre democracia en Nicaragua	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua" title="RELIAL alerta sobre democracia en Nicaragua">
		    	<img src="/media/k2/items/cache/8240a1907e29481b04619a0df33df9ab_XS.jpg" alt="RELIAL alerta sobre democracia en Nicaragua" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><strong>Miembros de la Red Liberal de América Latina (RELIAL)</strong>, ante la decisión del Tribunal Electoral de Nicaragua de despojar a 28 diputados electos por el pueblo nicaragüense en el año 2011 de sus credenciales como miembros del Poder Legislativo de ese país, manifestamos nuestra indignación por estos hechos que violentan la decisión soberana de un pueblo que fue a las urnas y eligió a sus representantes ante el parlamento y preocupación por las actuaciones de las instituciones nicaragüenses que han orquestado la desaparición de la oposición con el propósito de dejar como único candidato presidencial al señor Daniel Ortega para el proceso electoral de noviembre del 2016.</p>
<p>&nbsp;</p>
<p>Reconocemos que la democracia y los derechos de los ciudadanos en Nicaragua han recibido un fuerte ataque y nos sumamos a aquellas personas de todo el mundo que hoy levantan la voz ante este golpe dado a los legisladores separados de sus cargos solo por representar los intereses de la población que no piensa como este gobierno.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/520-relial-alerta-sobre-democracia-en-nicaragua">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/500-comunicado-relial-sobre-tucuman">
	  		Comunicado RELIAL sobre Tucuman	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Los representantes de los partidos e instituciones integrantes de la Red Liberal de América Latina (RELIAL), reunidos en el Grupo de Trabajo Realpolitik Buenos Aires 2015, manifiestan su preocupación por las dificultades experimentadas en las elecciones de la provincia de Tucumán.</p>
<p>&nbsp;</p>
<p>El camino de la libertad comienza con el respeto a la decisión ciudadana expresada en elecciones libres, sin trampas electorales ni prácticas clientelares que atentan contra la dignidad de las personas.</p>
<p>&nbsp;</p>
<p>Por eso, pedimos al gobierno argentino que garantice elecciones pacíficas y transparentes, y contemple las iniciativas políticas y de la Sociedad Civil que buscan mejorar el sistema electoral argentino.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/500-comunicado-relial-sobre-tucuman">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/490-ante-la-situación-política-en-guatemala">
	  		Ante la situación política en Guatemala	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/comunicados/item/490-ante-la-situación-política-en-guatemala" title="Ante la situaci&oacute;n pol&iacute;tica en Guatemala">
		    	<img src="/media/k2/items/cache/f2db05517411d6eb0e1fc32654b32d49_XS.jpg" alt="Ante la situaci&oacute;n pol&iacute;tica en Guatemala" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<div>La Red Liberal de América Latina (RELIAL) está al pendiente de la situación política en Guatemala.&nbsp;</div>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/490-ante-la-situación-política-en-guatemala">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/485-comunicado-ipl-en-panamá">
	  		Comunicado IPL en Panamá	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/comunicados/item/485-comunicado-ipl-en-panamá" title="Comunicado IPL en Panam&aacute;">
		    	<img src="/media/k2/items/cache/60cac5bf67bfb0259c686e8da95fd599_XS.jpg" alt="Comunicado IPL en Panam&aacute;" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Ante las recientes acciones difamatorias e intimidatorias que hoy afectan directamente a una de las principales organizaciones miembro de la Red Liberal de América Latina, RELIAL expresa pleno apoyo y solidaridad con el Instituto Político para la Libertad (IPL – Perú), presidido por Yesenia Álvarez.</p>
<p>&nbsp;</p>
<p>Comprometidos con los derechos humanos, la paz y el diálogo, RELIAL condena como inadmisible el acoso y la agresión que hoy sufren los ciudadanos demócratas quienes, en el marco de la Cumbre de las Américas que se desarrolla en Panamá, hoy se manifiestan por las libertades en Cuba y Venezuela. Solicitamos a la Organización de Estados Americanos (OEA) y a las entidades a cargo de la Cumbre otorguen garantías a los activistas y representantes de la sociedad civil para su pleno ejercicio de libertad de expresión.</p>
<p>&nbsp;</p>
<p>Nuestro sólido respaldo hacia nuestra organización miembro –IPL Perú- en su lucha y fomento de los principios democráticos y compromiso por la libertad en América Latina.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/485-comunicado-ipl-en-panamá">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/482-libertad-para-los-presos-políticos">
	  		LIBERTAD PARA LOS PRESOS POLÍTICOS	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/comunicados/item/482-libertad-para-los-presos-políticos" title="LIBERTAD PARA LOS PRESOS POL&Iacute;TICOS">
		    	<img src="/media/k2/items/cache/464b3f74fd6601955ccb022f610c3111_XS.jpg" alt="LIBERTAD PARA LOS PRESOS POL&Iacute;TICOS" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p style="margin-left: 30px;"><strong>CEDICE Libertad ante la detención de Antonio Ledezma</strong></p>
<p>Exigimos el respeto al Estado de Derecho, a la diversidad de formas de pensar y actuar y a la libertad en el más amplio sentido de la expresión.</p>
<p>La detención del Alcalde Metropolitano de Caracas, elegido en votación popular, representa una nueva agresión al Estado de Derecho y a la libertad por parte del Gobierno Nacional.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/482-libertad-para-los-presos-políticos">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/comunicados/item/447-declaración-de-relial-sobre-derechos-humanos">
	  		Declaración de RELIAL sobre Derechos Humanos	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/comunicados/item/447-declaración-de-relial-sobre-derechos-humanos" title="Declaraci&oacute;n de RELIAL sobre Derechos Humanos">
		    	<img src="/media/k2/items/cache/be76d1a55ee5ffb3b2dc895570c95b36_XS.jpg" alt="Declaraci&oacute;n de RELIAL sobre Derechos Humanos" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p><span style="font-size: 10pt;"><span size="2">El evento en conmemoración a los 10 años de RELIAL &nbsp;-una década traducida en compromiso, entrega y salvaguardia de la democracia liberal, la responsabilidad individual, la primacía del Estado de derecho y el impulso a la economía de mercado- fue, además un acontecimiento particularmente simbólico pues, en el marco de un ejercicio pluralista y democrático, aprobamos la Declaración de RELIAL sobre los Derechos Humanos</span>.</span></p>
<p><span style="font-size: 10pt;"><br /></span></p>
<p><span style="font-size: 10pt;">	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/comunicados/item/447-declaración-de-relial-sobre-derechos-humanos">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><span class="pagenav">Inicio</span></li><li class="pagination-prev"><span class="pagenav">Anterior</span></li><li><span class="pagenav">1</span></li><li><a title="2" href="/index.php/productos/archivo/comunicados?start=6" class="pagenav">2</a></li><li><a title="3" href="/index.php/productos/archivo/comunicados?start=12" class="pagenav">3</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/comunicados?start=6" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/comunicados?start=12" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 1 de 3	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:20:"Comunicados - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:74:"http://www.relial.org/index.php/productos/archivo/comunicados?limitstart=0";s:8:"og:title";s:20:"Comunicados - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:47:"http://www.relial.org/media/k2/categories/4.jpg";s:5:"image";s:47:"http://www.relial.org/media/k2/categories/4.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}}s:6:"module";a:0:{}}