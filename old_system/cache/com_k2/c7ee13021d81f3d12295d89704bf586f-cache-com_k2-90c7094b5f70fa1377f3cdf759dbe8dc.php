<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8670:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId457"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Bachelet de espaldas al Chile moderno
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/edc1d0314df26f4954651131e82a7939_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/edc1d0314df26f4954651131e82a7939_XS.jpg" alt="Bachelet de espaldas al Chile moderno" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Chile está dejando de ser la esperanza latinoamericana. Eso es grave para todos, no sólo para los chilenos.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Durante décadas, especialmente desde la llegada de la democracia a ese país en 1989 (aunque la transformación había comenzado en época de la dictadura de Pinochet), resultaba obvio que la libertad, el funcionamiento de las instituciones de derecho, la apertura al mundo, la competencia, el mercado, y la supremacía de la sociedad civil en el terreno económico, habían probado que ése era el camino de la prosperidad para toda América Latina.</p>
<p>&nbsp;</p>
<p>En Chile se confirmaba que la democracia liberal era la vía. El país, dentro de ese esquema, se había puesto a la cabeza de América Latina, más de la mitad de la sociedad se inscribía en los niveles sociales medios, y la pobreza había pasado del 46 al 12%. Una verdadera proeza.</p>
<p>&nbsp;</p>
<p>La nación de “la loca geografía” –una larga franja de tierra temblorosa situada entre el Pacífico y los Andes-- estaba a pocos pasos del umbral del Primer Mundo, definido como las naciones que alcanzan los $25 000 de producción anual per cápita. Bastaba recorrer las calles de Santiago y hablar con las gentes para percibir una sensación de optimismo y progreso mayor que en cualquier otra gran ciudad latinoamericana.</p>
<p>&nbsp;</p>
<p>Ese espíritu se está apagando. Los datos de la encuestadora chilena Plaza Pública (CADEM) no dejan lugar a dudas. Un 71% de los ciudadanos piensa que la economía se ha estancado. Sólo un 27% opina lo contrario. Tras dos generaciones de prosperidad anual notable, con pocas contramarchas, el primer año de la presidente Bachelet se saldará con apenas un 1.6 de crecimiento, pese a que Sebastián Piñera le entregó un país que funcionaba a plena máquina.</p>
<p>&nbsp;</p>
<p>Naturalmente, eso tiene un costo. Cuando Bachelet llegó al poder, hace sólo ocho meses, un 78%, de los chilenos tenía una buena imagen de ella. Hoy sólo la aprecia el 48, mientras su gobierno es aún más impopular: apenas un 37% lo respalda.</p>
<p>&nbsp;</p>
<p>¿Por qué se ha frenado Chile? Fundamentalmente, por una ruptura clarísima sobre el modelo de desarrollo. Los inversionistas locales y extranjeros tienen dudas y se abstienen. Ven a la señora Bachelet más cerca del viejo Chile estatista-populista que de luna nación moderna basada en las ideas de la libertad económica, y no pueden evitar una desagradable sensación de deja vu que los retrotrae a los turbulentos años del allendismo.</p>
<p>&nbsp;</p>
<p>La perciben como una persona encharcada en las supersticiones ideológicas del “distribuicionismo igualitario”, obsesionada con el Índice Gini, y no en la creación de riquezas, que es lo que realmente importa. Al fin y al cabo, el coeficiente Gini de Venezuela es “mejor” que el de Chile y no creo que nadie en sus cabales piense que la gravísima situación del manicomio chavista es preferible a la chilena.</p>
<p>&nbsp;</p>
<p>Si la presidente Bachelet no rectifica, muy probablemente provocará la salida de la Democracia Cristiana de la coalición de gobierno. Es increíble que esta señora no advierta que la buena experiencia de las ideas de la libertad en su país ha corrido hacia el centro a todo el espectro político chileno.</p>
<p>&nbsp;</p>
<p>El socialcristianismo de izquierda de los años cincuenta y sesenta del siglo pasado ya no es lo que era. La democracia cristiana de Frei Ruiz-Tagle es diferente a la de Frei Montalva, su padre, porque entre ambos mundos existe medio siglo de inocultables éxitos liberales y el hundimiento de las recetas estatistas.</p>
<p>&nbsp;</p>
<p>El socialismo de Ricardo Lago tiene muy poco que ver con el de Salvador Allende, aunque respetuosamente cultive su memoria, porque en el camino de la lucha por la libertad, Lago se transformó en un genuino socialdemócrata y enterró el lastre marxista.</p>
<p>&nbsp;</p>
<p>En cambio, quienes no se han movido de su posición fanática son los comunistas (esos que Bachelet se empeñó tercamente en llevar a la Concertación) y continúan defendiendo un empobrecedor modelo de sociedad, pero en el pecado ideológico llevan la penitencia: la bonita Camilo Vallejo, quien era muy popular cuando figuraba como revoltosa líder estudiantil de la oposición, tras pasar al parlamento, hoy apenas tiene el aprecio del 3% de los chilenos.</p>
<p>&nbsp;</p>
<p>Ojalá Chile retorne al carril del sentido común y el buen gobierno. Fue un faro para orientar a los latinoamericanos. Perderlo, insisto, nos perjudicará a todos.</p>
<p>&nbsp;</p>
<p>Foto: El blog de Montaner</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/449-sobre-el-congreso-y-aniversario-de-relial-en-panamá">
			&laquo; Sobre el Congreso y Aniversario de RELIAL		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/460-red-liberal-de-américa-latina-los-primeros-10-años">
			Red Liberal de América Latina, los primeros 10 años &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/457-bachelet-de-espaldas-al-chile-moderno#startOfPageId457">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:46:"Bachelet de espaldas al Chile moderno - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Chile está dejando de ser la esperanza latinoamericana. Eso es grave para todos, no sólo para los...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:37:"Bachelet de espaldas al Chile moderno";s:6:"og:url";s:104:"http://www.relial.org/index.php/productos/archivo/opinion/item/457-bachelet-de-espaldas-al-chile-moderno";s:8:"og:title";s:46:"Bachelet de espaldas al Chile moderno - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/edc1d0314df26f4954651131e82a7939_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/edc1d0314df26f4954651131e82a7939_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Chile está dejando de ser la esperanza latinoamericana. Eso es grave para todos, no sólo para los...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:37:"Bachelet de espaldas al Chile moderno";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}