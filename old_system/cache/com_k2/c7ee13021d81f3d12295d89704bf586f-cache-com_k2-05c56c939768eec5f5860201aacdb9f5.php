<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11359:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId431"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El sistema liberal ha mejorado la vida de los peruanos
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/4653e069ed7369840191e8bf38ab8dc9_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/4653e069ed7369840191e8bf38ab8dc9_XS.jpg" alt="El sistema liberal ha mejorado la vida de los peruanos" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Entrevista a Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>El abogado, ensayista y poeta limeño analiza el "giro capitalista" que ha asumido Perú, explicando cómo la economía de mercado ha llevado progreso y bienestar al país latinoamericano.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>Nacido en Lima en 1972, el abogado, ensayista y poeta Héctor Ñaupari es una de las voces liberales más influyentes de Perú. Aprovechando su reciente visita a España, Libre Mercado se ha reunido con Ñaupari para conversar sobre el éxito socioeconómico que ha cosechado su país durante los últimos años.</p>
<p>&nbsp;</p>
<p>Pregunta: ¿Qué ha pasado en Perú? Hace unos 25 años, el país atravesaba una situación muy delicada: violencia terrorista, fragilidad institucional, declive económico... Pero, desde hace años, casi todas las noticias que nos llegan son francamente positivas, al menos en lo tocante al crecimiento económico y el desarrollo social.</p>
<p>&nbsp;</p>
<p>ADVERTISEMENT</p>
<p>&nbsp;</p>
<p>Respuesta: Lo que está pasando en Perú es algo maravilloso. En primer lugar, se han consolidado reformas estructurales que empezaron a ser implementadas a comienzos de los años 90, bajo la influencia de Mario Vargas Llosa. Estas reformas buscaban un modelo económico en el que la inflación fuese controlada, el crecimiento fuese sostenible, las instituciones protegiesen las libertades y los derechos individuales.</p>
<p>&nbsp;</p>
<p>Desde entonces, se redujo enormemente la burocracia, se privatizaron empresas y se liberalizaron sectores. Gobiernos de distinto signo político entendieron que la población apoyaba estas reformas, por lo que Perú acumula ya muchos años de continuidad en estas políticas. Eso sí: es importante destacar el rol que han tenido las élites intelectuales que, principalmente a través de su influencia en el Ministerio de Economía y Finanzas, han defendido, profundizado y protegido estas reformas.</p>
<p>&nbsp;</p>
<p>- ¿Podemos decir entonces que hay un "búnker liberal" en la órbita del Ministerio de Economía peruano?</p>
<p>&nbsp;</p>
<p>Así es, felizmente. Todos los Ministros que han desfilado durante los últimos años han cuidado la estabilidad fiscal, han promovido medidas liberales, han frenado las tentaciones populistas, han promovido la apertura comercial...</p>
<p>&nbsp;</p>
<p>- Y, sin embargo, estas medidas tienen respaldo social, no se han quedado solamente en las altas esferas.</p>
<p>&nbsp;</p>
<p>Los peruanos valoran el emprendimiento y aprecian el resultado de estas medidas. Hoy exportamos bienes y servicios a muchos más países que antaño, y esto es algo que entienden perfectamente los ciudadanos. El sistema liberal ha mejorado la vida de los peruanos y nadie quiere renunciar a eso.</p>
<p>&nbsp;</p>
<p>- Hay quienes afirman que Perú podría estar desarrollando una burbuja inmobiliaria. Por otro lado, no faltan quienes quitan hierro al crecimiento económico de los últimos años, aludiendo que se debe al "boom" experimentado en el precio internacional de las materias primas, como por ejemplo el cobre.</p>
<p>&nbsp;</p>
<p>La mayor parte del ahorro personal de los peruanos no está en los bancos, sino en el hogar. La desconfianza hacia el sistema financiero hace que muchos proyectos inmobiliarios se financien de forma directa, sin intermediación. Por eso, incluso admitiendo que los precios inmobiliarios han aumentado, no hablamos de una estructura capaz de desencadenar grandes desajustes financieros.</p>
<p>&nbsp;</p>
<p>En lo tocante al boom de las materias primas, es cierto que las exportaciones de los años pasados se han apoyado mucho en este aspecto, pero ahora vemos que la economía sigue comportándose con dinamismo a pesar del enfriamiento en los precios internacionales. Eso sí: lejos de entender el boom del cobre como un problema, todo esto hay que interpretarlo como una oportunidad. De hecho, tenemos mucho camino por recorrer en campos como los hidrocarburos, los minerales o los metales.</p>
<p>&nbsp;</p>
<p>Esto también tendría buenas consecuencias fiscales, pues la mayor recaudación fiscal que generarían estos sectores podría reducir más aún la carga impositiva que soportan los contribuyentes individuales.</p>
<p>&nbsp;</p>
<p>- Frente a los altos niveles de gasto público observados en Europa, el Estado peruano maneja un presupuesto cercano al 20% del PIB. ¿Cuál es el panorama fiscal del país?</p>
<p>&nbsp;</p>
<p>Perú tiene una situación presupuestaria de notable equilibrio. Prácticamente todo el gasto viene de los impuestos, no se recurre a la deuda ni a los aranceles para sufragar los desembolsos de las Administraciones. De hecho, es posible que en la próxima legislatura se haya completado por completo el pago de la deuda pública, lo que liberaría una parte del gasto para dedicarlo a otras cuestiones.</p>
<p>&nbsp;</p>
<p>- En el ámbito del desarrollo humano, Perú también acumula muchos años de buenas noticias.</p>
<p>&nbsp;</p>
<p>La pobreza se ha reducido de forma sistemática. También la desigualdad viene cayendo de forma continuada. Todo esto se ha dado porque el crecimiento económico genera un notable auge de la clase media, lo que redunda en una mejora sustancial de la calidad de vida de los ciudadanos. ¿Qué podemos hacer para seguir avanzando en la buena dirección? Una de las tareas pendientes es la de introducir un mayor grado de participación privada en la educación y la sanidad.</p>
<p>&nbsp;</p>
<p>- Perú tiene un sistema de capitalización de las pensiones muy similar al chileno. ¿Qué resultados ha arrojado este modelo basado en el ahorro individual?</p>
<p>&nbsp;</p>
<p>Nuestro modelo ha aprendido mucho del paradigma chileno. En este sentido, hemos "copiado" lo que funciona bien. Para el caso peruano, los resultados ya son notables: hay ahorro acumulado por valor de 25.000 millones de dólares, lo que equivale ya al 10% del PIB. La población peruana respalda el modelo y, de hecho, quiere tener más y más poder sobre la gestión y el desempeño de las aseguradoras que manejan el dinero que entra al sistema. Este celo muestra que el sistema ha alimentado un cambio de mentalidad.</p>
<p>&nbsp;</p>
<p>- ¿Cómo ve la crisis española desde Perú?</p>
<p>&nbsp;</p>
<p>Entiendo que hay dos grandes problemas. El primero, es el mercado laboral, donde sería necesario liberalizarlo por completo para así permitir que se reduzca la tasa de paro de forma significativa. El segundo, son los impuestos, que tienen que bajar para ayudar a que España recupere el crecimiento y la riqueza perdida.</p>
<p>&nbsp;</p>
<p>- ¿Qué me dice del vecino del Sur, Chile?</p>
<p>&nbsp;</p>
<p>Me temo que el nuevo gobierno de Michelle Bachelet puede llevar a Chile por un camino de continuo retroceso. Parece que la izquierda chilena quiere recoger el testigo de Hugo Chávez y liderar el socialismo a nivel regional. El problema es que esta pretensión supondría la destrucción de un modelo liberal que ha convertido a Chile en un país desarrollado.</p>
<p>&nbsp;</p>
<p>No podemos quedarnos de brazos cruzados, la "batalla" que se va a librar en Chile es muy importante y los liberales de todo el mundo debemos estar muy atentos a todo lo que está ocurriendo.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Fuente: <a href="http://www.libremercado.com">www.libremercado.com</a></p>
<p>Foto: IEAH</p>
<p>Autor: Diego Sanchez de la Cruz</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho">
			&laquo; El populismo está en guerra con el estado de derecho		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación">
			Colombia y la farsa de la reconciliación &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos#startOfPageId431">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:63:"El sistema liberal ha mejorado la vida de los peruanos - Relial";s:11:"description";s:158:"Entrevista a Héctor Ñaupari &amp;nbsp; El abogado, ensayista y poeta limeño analiza el &quot;giro capitalista&quot; que ha asumido Perú, explicando có...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:54:"El sistema liberal ha mejorado la vida de los peruanos";s:6:"og:url";s:151:"http://www.relial.org/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos/index.php/relial/sobre-relial";s:8:"og:title";s:63:"El sistema liberal ha mejorado la vida de los peruanos - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/4653e069ed7369840191e8bf38ab8dc9_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/4653e069ed7369840191e8bf38ab8dc9_S.jpg";s:14:"og:description";s:170:"Entrevista a Héctor Ñaupari &amp;amp;nbsp; El abogado, ensayista y poeta limeño analiza el &amp;quot;giro capitalista&amp;quot; que ha asumido Perú, explicando có...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:54:"El sistema liberal ha mejorado la vida de los peruanos";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}