<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5458:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId267"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Crisis en América Latina por factores institucionales
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/2e2c1711fe12b24ae23d95c35bfd21c2_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/2e2c1711fe12b24ae23d95c35bfd21c2_XS.jpg" alt="Crisis en Am&eacute;rica Latina por factores institucionales" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Ricardo López Murphy</p>
<p>&nbsp;</p>
<p>Ricardo López habla de los factores institucionales que impiden la integración de Latinoamérica, a un mundo global, para mejorar los niveles de vida. Explica que esos problemas son consecuencia de la lucha por la independencia de España, las guerras civiles y el desorden y anarquía por exceso de poder que han tenido sus gobernantes.	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>También, menciona tres elementos importantes que han marcado su historia: la propiedad tomada como privilegio concedido por la autoridad monárquica o el Estado; el mercantilismo asociado al monopolio del comercio exterior, problemas fiscales y desorden monetario.</p>
<p>&nbsp;</p>
<p>Asimismo, se refiere a los intentos de crecimiento de la región latinoamericana que ha mostrado reformas exitosas, estabilización de economías y retrasos en otras. Finalmente, habla sobre las instituciones de la libertad, constituidas por procesos de mercado, tolerancia cultural, protección a la propiedad privada y fortalecimiento civil que plantea como mejor alternativa para el avance económico de América Latina.</p>
<p>&nbsp;</p>
<p>Para ver el video favor de dar clic <a href="http://newmedia.ufm.edu/gsm/index.php/Murphycrisisamerica">aquí</a></p>
<p>&nbsp;</p>
<p>Autor: new media UFM</p>
<p>Fuente: <a href="http://newmedia.ufm.edu/gsm/index.php/Murphycrisisamerica">new media UFM</a></p>
<p>Logo: UFM</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/264-relial-felicitaciones-a-los-think-tanks-reconocidos-en-el-índice-“2013-global-go-to-think-tank”">
			&laquo; RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”,		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/269-apuleyo-el-sistema-de-partidos-podría-colapsar-si-no-renueva-dirigencia">
			Apuleyo: el sistema de partidos podría colapsar si no renueva dirigencia &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/267-crisis-en-américa-latina-por-factores-institucionales#startOfPageId267">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:63:"Crisis en América Latina por factores institucionales - Relial";s:11:"description";s:158:"En la opinión de Ricardo López Murphy &amp;nbsp; Ricardo López habla de los factores institucionales que impiden la integración de Latinoamérica, a un...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:54:"Crisis en América Latina por factores institucionales";s:6:"og:url";s:124:"http://www.relial.org/index.php/productos/archivo/actualidad/item/267-crisis-en-américa-latina-por-factores-institucionales";s:8:"og:title";s:63:"Crisis en América Latina por factores institucionales - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/2e2c1711fe12b24ae23d95c35bfd21c2_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/2e2c1711fe12b24ae23d95c35bfd21c2_S.jpg";s:14:"og:description";s:162:"En la opinión de Ricardo López Murphy &amp;amp;nbsp; Ricardo López habla de los factores institucionales que impiden la integración de Latinoamérica, a un...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:54:"Crisis en América Latina por factores institucionales";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}