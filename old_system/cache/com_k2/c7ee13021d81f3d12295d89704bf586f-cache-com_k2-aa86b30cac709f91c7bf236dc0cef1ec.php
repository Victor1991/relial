<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6490:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId312"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Superar el limbo de gobernabilidad en Bogotá
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f5ddf7bd97d01d87f4a7985398aea709_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f5ddf7bd97d01d87f4a7985398aea709_XS.jpg" alt="Superar el limbo de gobernabilidad en Bogot&aacute;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p align="center" style="text-align: center;"><b>Comunicado de Prensa del Instituto de Ciencia Política “Hernán Echavarría Olózaga”</b></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>El Instituto de Ciencia Política "Hernán Echavarría Olózaga", su Consejo Directivo y su Consejo de Fundadores, en relación con los hechos ocurridos el día de ayer, 19 de marzo de 2014, y que conciernen al normal ejercicio del gobierno y la administración de la capital de la República, se permite manifestar a la opinión pública:</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>1. Que desde el primer momento observó con preocupación el impacto negativo que en el buen gobierno y la recta administración del Distrito Capital tuvo el empeño del ex alcalde mayor, Gustavo Petro Urrego, en mantenerse en el cargo a toda costa, anteponiendo a las apremiantes necesidades de la ciudad sus propios intereses personales.</p>
<p>&nbsp;</p>
<p>2. Que la forma en que el ex alcalde Petro Urrego movilizó recursos públicos, instrumentalizó la burocracia distrital, abusó de los instrumentos judiciales, y apeló a la agitación social para mantenerse en el cargo, pese a la sanción impuesta por el Ministerio Público y recientemente validada por la Rama Judicial, no se compadece con la dignidad de su investidura y la responsabilidad política que le es inherente.</p>
<p>&nbsp;</p>
<p>3. Que el recurso a las instancias internacionales para sustraerse al cumplimiento de las decisiones legítimas de las autoridades nacionales, tal como lo ha hecho el ex alcalde Petro Urrego, no sólo constituye un abuso de aquellas sino que acaba erosionando de manera irresponsable el Estado de Derecho y el orden jurídico interno.</p>
<p>&nbsp;</p>
<p>4. Que espera que, de conformidad de la ley, se activen los procedimientos previstos a efectos de convocar elección de Alcalde Mayor, con el fin de superar el limbo de gobernabilidad en el que se encuentra la ciudad y ponerla al día con los desafíos que enfrenta en materia de movilidad, desarrollo sostenible, vivienda, servicios públicos, infraestructura y competitividad.</p>
<p>&nbsp;</p>
<p>5. Que seguirá con atención la evolución de los acontecimientos e intervendrá en todos los escenarios —nacionales e internacionales, incluso nuevamente ante el Sistema Interamericano de Derechos Humanos si es el caso—, en representación de la sociedad civil y en defensa de los intereses de la ciudad y el imperio de la ley.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: right;">Bogotá D.C, 20 de marzo de 2014.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/comunicados/item/291-comunicado-de-fundación-internacional-para-la-libertad">
			&laquo; Comunicado de Fundación Internacional para la Libertad		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/comunicados/item/423-proyecto-de-declaración-de-relial-sobre-la-situación-de-los-derechos-humanos-en-nicaragüa">
			Declaración de RELIAL sobre la situación de los Derechos Humanos en Nicaragua &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/comunicados/item/312-superar-el-limbo-de-gobernabilidad-en-bogotá#startOfPageId312">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:54:"Superar el limbo de gobernabilidad en Bogotá - Relial";s:11:"description";s:163:"Comunicado de Prensa del Instituto de Ciencia Política “Hernán Echavarría Olózaga” &amp;nbsp; &amp;nbsp; El Instituto de Ciencia Política &quot;Hernán...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:45:"Superar el limbo de gobernabilidad en Bogotá";s:6:"og:url";s:112:"http://relial.org/index.php/productos/archivo/comunicados/item/312-superar-el-limbo-de-gobernabilidad-en-bogotá";s:8:"og:title";s:54:"Superar el limbo de gobernabilidad en Bogotá - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/f5ddf7bd97d01d87f4a7985398aea709_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/f5ddf7bd97d01d87f4a7985398aea709_S.jpg";s:14:"og:description";s:175:"Comunicado de Prensa del Instituto de Ciencia Política “Hernán Echavarría Olózaga” &amp;amp;nbsp; &amp;amp;nbsp; El Instituto de Ciencia Política &amp;quot;Hernán...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:11:"Comunicados";s:4:"link";s:20:"index.php?Itemid=133";}i:3;O:8:"stdClass":2:{s:4:"name";s:45:"Superar el limbo de gobernabilidad en Bogotá";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}