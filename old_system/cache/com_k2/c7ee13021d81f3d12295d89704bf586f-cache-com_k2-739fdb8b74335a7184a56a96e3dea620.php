<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9778:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId488"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Educación y Libertad, garantías para una sociedad armónica
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/c3698948bbef8e01b61b372d4a29088b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/c3698948bbef8e01b61b372d4a29088b_XS.jpg" alt="Educaci&oacute;n y Libertad, garant&iacute;as para una sociedad arm&oacute;nica" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Entrevista a Rocío Guijarro, Gerente General de CEDICE Libertad</p>
<p>Publicada en AMINISTÍA Internacional</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>La difusión de información en cualquier área del conocimiento y la garantía de gozar de plena libertad social, son aspectos que, si se trabajan e impulsan con más ahínco, conducirían a nuestra sociedad hacia un estado más armónico, unido y constructivo. Así lo destacó la gerente general del Centro de Divulgación del Conocimiento Económico, <a href="http://cedice.org.ve/" target="_blank">CEDICE Libertad</a>, Rocío Guijarro.</p>
<p>&nbsp;</p>
<p>Con más de 30 años de trayectoria en Venezuela, CEDICE Libertad ha trabajado en la promoción, educación y defensa de los principios que sustentan la libre empresa y la libertad individual como base fundamental para lograr un sistema más democrático y más participativo.</p>
<p>&nbsp;</p>
<p>Para la representante de esta asociación civil, la concordia debe ser la base para la mejor relación entre los ciudadanos y para el mejor convivir: “Si no hay concordia no hay posibilidad de que funcionemos mejor como sociedad. Sin ella, tampoco podremos ser creadores y promotores de un país con mayor calidad de vida”.</p>
<p>&nbsp;</p>
<p>Destacó que el valor de la libertad, es el principio fundamental para lograr una verdadera mejoría social. “Sin la libertad, no tenemos la posibilidad de elegir aquello que considero que es lo mejor para mí y para los que me rodean. En la medida en que el marco de libertades sea más amplio, la confianza entre los individuos se refuerza, y el accionar de cada persona será más participativo y efectivo”, dijo Guijarro.</p>
<p>&nbsp;</p>
<p>En una sociedad libre, los ciudadanos no son coaccionados por imposiciones particulares. En libertad, las personas que cultivan su coeficiente intelectual son capaces de reconocer si están siendo víctimas de vulneración de derechos, y tendrán el poder de reaccionar de manera más acertada para que la transgresión no continúe sucediendo, explicó la representante de CEDICE Libertad.</p>
<p>&nbsp;</p>
<p>Igualmente, el valor del respeto y la responsabilidad son preceptos que deben volver a formar parte de la idiosincrasia del ciudadano. Guijarro destacó que el venezolano presenta actualmente una merma en la práctica de estos valores, situación que nos ha llevado hacia ambientes de corrupción y violencia generalizada.</p>
<p>&nbsp;</p>
<p>La también filósofa apuesta a la promoción del respeto, y más en el contexto nacional en el que vivimos, donde el disentimiento es penalizado por algunas cúpulas de poder. “Debemos entender que cada persona es un mundo que tiene el derecho de opinar y pensar diferente. Al respetar esta naturaleza humana, tendremos más posibilidades de coexistir en un entorno de paz”, manifestó Guijarro.</p>
<p>&nbsp;</p>
<p>Indicó que es importante no ahondar en las diferencias, sino más bien, en las líneas o conceptos que nos unen como seres que viven en una misma tierra: “Si creemos que merecemos vivir en un sistema democrático, donde prevalezca el equilibrio de poderes y la libertad individual, debatamos con respeto estos principios para llegar a un acuerdo social que nos beneficie a todos”.</p>
<p>&nbsp;</p>
<p>El exceso de polarización y la incapacidad de dialogar para acordar, son factores que inciden en una continua descomposición social que se presenta cada vez más insostenible. Nuestro trabajo debe estar orientado a unir esfuerzos para enfrentar esta situación a través de mesas del trabajo basadas en el diálogo constructivo, resaltó Guijarro.</p>
<p>&nbsp;</p>
<p>CEDICE Libertad cree en el poder de la palabra para orientar y educar. Es por ello, que realizan constantes foros y conversatorios en varios lugares del país, con acceso libre, para que las personas puedan entender el sistema en el que viven, comprendan la razón de las leyes y medidas que se decretan, y finalmente, posean un criterio más amplio de lo que sucede a su alrededor.</p>
<p>&nbsp;</p>
<p>Pero esa formación no es solamente impartida a los adultos. La organización también se dirige a centros de educación básica, diversificada y universitaria, para hablar con los más jóvenes sobre temas profundos, como el sistema de mercado, la política, la economía, etc. de una manera sencilla.&nbsp;&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p>Finalmente, Guijarro señaló que las organizaciones civiles deben ampliar más las posibilidades de diálogo social para incentivar el sentido de pertenecía y la participación activa en las personas, con el fin lograr un cambio real que nos permita tener mayores oportunidades de desarrollo y bienestar.</p>
<p>&nbsp;</p>
<p>“Es imprescindible conservar esa semilla de optimismo, y la conciencia de que todo mejorará en la medida en que participemos, exijamos y trabajemos por un país en el que nos sintamos orgullosos de pertenecer”, puntualizó Guijarro.</p>
<p>&nbsp;</p>
<p>Para conocer más información sobre la asociación civil Centro de Divulgación del Conocimiento Económico, CEDICE Libertad, y las iniciativas que adelanta, visita su página en internet <a href="http://cedice.org.ve/" target="_blank" rel="nofollow">cedice.org.ve</a>&nbsp;o síguelos a través de su twitter <a href="https://twitter.com/cedice" target="_blank" rel="nofollow">@CEDICE</a>.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Texto: Daniela Damiano<br />Originalmente publicado en: <br /><a href="http://www.amnistia.me/profiles/blogs/la-promocion-de-la-educacion-y-la-libertad-es-la-garantia-de-una-?xg_source=twitter">http://www.amnistia.me/profiles/blogs/la-promocion-de-la-educacion-y-la-libertad-es-la-garantia-de-una-?xg_source=twitter</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/480-se-incrementa-la-represión-económica-en-venezuela">
			&laquo; Se incrementa la represión económica en Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/489-preocupa-a-la-región-el-doble-estándar-en-ddhh-y-el-boom-del-autoritarismo">
			Preocupa a la región el doble estándar en DDHH y el boom del autoritarismo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/488-educación-y-libertad-garantías-para-una-sociedad-armónica#startOfPageId488">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:70:"Educación y Libertad, garantías para una sociedad armónica - Relial";s:11:"description";s:157:"Entrevista a Rocío Guijarro, Gerente General de CEDICE Libertad Publicada en AMINISTÍA Internacional &amp;nbsp; La difusión de información en cualquie...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:61:"Educación y Libertad, garantías para una sociedad armónica";s:6:"og:url";s:130:"http://www.relial.org/index.php/productos/archivo/actualidad/item/488-educación-y-libertad-garantías-para-una-sociedad-armónica";s:8:"og:title";s:70:"Educación y Libertad, garantías para una sociedad armónica - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/c3698948bbef8e01b61b372d4a29088b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/c3698948bbef8e01b61b372d4a29088b_S.jpg";s:14:"og:description";s:161:"Entrevista a Rocío Guijarro, Gerente General de CEDICE Libertad Publicada en AMINISTÍA Internacional &amp;amp;nbsp; La difusión de información en cualquie...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:61:"Educación y Libertad, garantías para una sociedad armónica";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}