<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8466:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId344"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Dónde se vive mejor o peor en América Latina
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/fe4bbe81600a40063594e597e00eb05b_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/fe4bbe81600a40063594e597e00eb05b_XS.jpg" alt="D&oacute;nde se vive mejor o peor en Am&eacute;rica Latina" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>¿Cuál es el mejor vividero de América Latina? Costa Rica. ¿Cuál es el peor? Cuba. Y hay una gran distancia entre ambos países.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>¿Cómo se sabe? Lo afirma, indirectamente, el Índice de Progreso Social del 2014, una sabia entidad sin fines de lucro ni prejuicios ideológicos, dirigida por un puñado de profesionales de primer rango. Se puede googlear fácilmente por medio de Internet. Vale la pena examinarlo.</p>
<p>&nbsp;</p>
<p>Los expertos han ponderado 56 factores importantes que miden la calidad de vida de 132 naciones. Estos elementos, a su vez, se inscriben en tres grandes categorías: Necesidades humanas básicas, Fundamentos del bienestar y Oportunidades.</p>
<p>&nbsp;</p>
<p>De acuerdo con el Índice, secamente objetivo, los cinco primeros países de América Latina son Costa Rica, que ocupa el lugar número 25 entre las 132 naciones escrutadas. Uruguay es el segundo y 26 del planeta. Chile aparece en el tercero y 30 del mundo. Panamá es el cuarto y 38 de la lista. Argentina es el quinto y su lugar es el 42.</p>
<p>&nbsp;</p>
<p>Los cinco últimos países de Hispanoamérica son: Bolivia el 71, Paraguay 72, Nicaragua 74; Honduras 77; y Cuba, finalmente, el 79. La Isla queda muy mal situada aunque no omiten los manoseados argumentos de la salud y la educación. Forman parte de la ecuación.</p>
<p>&nbsp;</p>
<p>Los diez países con mejor índice de progreso social son los sospechosos habituales de siempre y aparecen en este orden: Nueva Zelanda, Suiza, Islandia, Holanda, Noruega, Suecia, Finlandia, Dinamarca, Canadá y Australia.</p>
<p>&nbsp;</p>
<p>Estados Unidos comparece en el lugar número 16, Francia en el 20 y España, pese a la crisis, en un honroso 21, algo mejor que Portugal, que se sitúa en el 22.</p>
<p>&nbsp;</p>
<p>Obsérvese que no se mide desarrollo económico y científico, ni se contrasta el PIB per cápita de las naciones, sino se calcula el progreso social valorando elementos como la nutrición, los cuidados médicos, el acceso a agua potable y alcantarillados, vivienda, seguridad, educación, acceso a la información y a la comunicación, sustentabilidad, cuidado del ecosistema, derechos individuales, libertades, tolerancia, inclusión, y otros factores que explican por qué hay países de los que emigran en masa las personas y países de los que apenas se despiden los ciudadanos.</p>
<p>&nbsp;</p>
<p>No es una casualidad que en Estados Unidos, foco receptor de inmigrantes legales o indocumentados, no existen concentraciones significativas de costarricenses, uruguayos, chilenos o panameños, pero sí las hay de salvadoreños, nicaragüenses, hondureños y cubanos. Huyen del desastre.</p>
<p>&nbsp;</p>
<p>El signo de las migraciones (que el Índice no pondera, por cierto), es, a mi juicio, el síntoma más claro de la calidad general de vida de cualquier sociedad. La mayor parte de la gente emigra en busca de oportunidades de mejorar que no encuentran en su propio terruño.</p>
<p>&nbsp;</p>
<p>Hay tres consideraciones importantes que se desprenden del repaso del Índice de Progreso Social. La primera, es que en América Latina las naciones que se autodenominan "progresistas", las del Socialismo del Siglo XXI, son, en general, las que menos progresan. Venezuela es el país número 67 del universo analizado, Ecuador el 50 y, como queda dicho, Nicaragua el 74, y Cuba, en la cola, el 79. Una vergüenza.</p>
<p>&nbsp;</p>
<p>La segunda, es que los treinta países que mejor puntuación obtienen son democracias liberales en las que rige la economía de mercado y se disfruta de libertades políticas. Podrán tener una mayor presión fiscal, como sucede en Dinamarca, o menor, como ocurre en Suiza, pero ese factor no altera el dato esencial de que se trata de los países más habitables del planeta.</p>
<p>&nbsp;</p>
<p>La última observación es que esta nueva medición reitera, por otra vía, lo que también nos dice el Índice de Desarrollo Humano de Naciones Unidas, o incluso el que mide la "percepción de corrupción" compilado por Transparencia Internacional. Los países menos corruptos son los más prósperos y desarrollados. La lista es aproximadamente la misma.</p>
<p>&nbsp;</p>
<p>En realidad: nada nuevo bajo el sol. Pero esta vez todo está organizado de una manera más persuasiva para que lo entienda todo aquel que no esté cegado por el dogmatismo ideológico. Googléenlo y lo comprobarán.</p>
<p>&nbsp;</p>
<p>Fuente: Texto proporcionado por&nbsp;Carlos Alberto Montaner</p>
<p>Foto: El Blog de Montaner</p>
<p>Autor:&nbsp;Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/336-la-verdad-de-hilda-molina">
			&laquo; La verdad de Hilda Molina		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/346-unión-civil-y-libertad-en-el-perú">
			Unión civil y libertad en el Perú &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina#startOfPageId344">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:55:"Dónde se vive mejor o peor en América Latina - Relial";s:11:"description";s:159:"En la opinión de Carlos Alberto Montaner &amp;nbsp; ¿Cuál es el mejor vividero de América Latina? Costa Rica. ¿Cuál es el peor? Cuba. Y hay una gran d...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:46:"Dónde se vive mejor o peor en América Latina";s:6:"og:url";s:113:"http://www.relial.org/index.php/productos/archivo/opinion/item/344-dónde-se-vive-mejor-o-peor-en-américa-latina";s:8:"og:title";s:55:"Dónde se vive mejor o peor en América Latina - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/fe4bbe81600a40063594e597e00eb05b_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/fe4bbe81600a40063594e597e00eb05b_S.jpg";s:14:"og:description";s:163:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; ¿Cuál es el mejor vividero de América Latina? Costa Rica. ¿Cuál es el peor? Cuba. Y hay una gran d...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:46:"Dónde se vive mejor o peor en América Latina";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}