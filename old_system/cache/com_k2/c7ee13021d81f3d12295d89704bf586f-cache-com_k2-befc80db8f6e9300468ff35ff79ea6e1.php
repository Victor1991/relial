<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6804:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId511"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	CEDICE Libertad en Venezuela entre los mejores think tanks del mundo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/ca9456ad89fef6c66a71b99b32dfe05e_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/ca9456ad89fef6c66a71b99b32dfe05e_XS.jpg" alt="CEDICE Libertad en Venezuela entre los mejores think tanks del mundo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><strong>Cedice Libertad</strong> entre los primeros Centros de Estudios <em>(think tank</em>) de acuerdo al ranking que elabora la Universidad de Pensilvania</p>
<p><strong>Como difusor del pensamiento liberal y por su influencia en la opinión pública venezolana</strong></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p><strong>El índice elaborado por la Universidad de Pensilvania</strong> posicionó a la organización entre los 100 mejores "tanques de pensamiento" de todo el mundo y entre los veinte con más impacto en políticas públicas, por su labor de más de tres décadas de trayectoria.</p>
<p>&nbsp;</p>
<p>El Centro de Divulgación del Conocimiento Económico para la Libertad (Cedice-Libertad), fue catalogado como el segundo think tank (tanque de pensamiento) liberal del mundo, de acuerdo con el índice "2015 Global Go To Think Tanks" elaborado por el Instituto Lauder de la Universidad de Pensilvania.&nbsp;</p>
<p>&nbsp;</p>
<p>La escala, desarrollada por el Programa de Tanques de Pensamiento y Sociedades Civiles de dicha institución, analiza variables como los temas de interés de los think tanks, su preocupación por el desarrollo internacional y su influencia en las políticas públicas.</p>
<p>&nbsp;</p>
<p>En este último aspecto, Cedice Libertad obtuvo la posición número 13 sobre 54 organizaciones estudiadas. También se ubicó entre los 50 mejores think tanks independientes (no afiliados a partidos u otros movimientos) y entre los 100 mejores de todo el mundo, incluyendo Estados Unidos, sobre una base de 175 organizaciones analizadas.</p>
<p>&nbsp;</p>
<p>Resalta igualmente que Cedice-Libertad se ubicó en la décima posición entre los 90 think tanks de distinta ideología evaluados en Suramérica Hostilidad contra los think tanks.</p>
<p>&nbsp;</p>
<p>El estudio de la Universidad de Pensilvania, que llega este año a su novena edición, también profundizó en las circunstancias de trabajo de los think tanks y organizaciones no gubernamentales (ONG) en general en ambientes hostiles</p>
<p>Al respecto, aunque el análisis resalta que este tipo de organizaciones ha crecido en el último siglo, también alerta sobre una progresiva disminución de su impacto debido a circunstancias hostiles en todo el mundo, entre las que se incluyen "un ambiente político y regulatorio crecientemente hostil" y la disminución del financiamiento para la investigación en políticas públicas.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Acerca de Cedice Libertad</strong></p>
<p>Cedice Libertad (Centro de Divulgación del Conocimiento Económico, A.C.) es una organización sin fines de lucro que tiene como misión divulgar, formar, generar conocimiento y promover los principios que sustentan la libertad individual, el respeto a los derechos de propiedad, el libre mercado y la cultura liberal, como bases para lograr una sociedad de ciudadanos libres y responsables</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/509-un-diálogo-sobre-argentina">
			&laquo; Un diálogo sobre Argentina		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/513-juntos-en-el-compromiso-por-la-libertad-personal-alrededor-del-mundo">
			Juntos en el compromiso por la libertad personal alrededor del mundo &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo#startOfPageId511">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:77:"CEDICE Libertad en Venezuela entre los mejores think tanks del mundo - Relial";s:11:"description";s:153:"Cedice Libertad entre los primeros Centros de Estudios (think tank) de acuerdo al ranking que elabora la Universidad de Pensilvania Como difusor del p...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:68:"CEDICE Libertad en Venezuela entre los mejores think tanks del mundo";s:6:"og:url";s:134:"http://relial.org/index.php/productos/archivo/actualidad/item/511-cedice-libertad-en-venezuela-entre-los-mejores-think-tanks-del-mundo";s:8:"og:title";s:77:"CEDICE Libertad en Venezuela entre los mejores think tanks del mundo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/ca9456ad89fef6c66a71b99b32dfe05e_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/ca9456ad89fef6c66a71b99b32dfe05e_S.jpg";s:14:"og:description";s:153:"Cedice Libertad entre los primeros Centros de Estudios (think tank) de acuerdo al ranking que elabora la Universidad de Pensilvania Como difusor del p...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:68:"CEDICE Libertad en Venezuela entre los mejores think tanks del mundo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}