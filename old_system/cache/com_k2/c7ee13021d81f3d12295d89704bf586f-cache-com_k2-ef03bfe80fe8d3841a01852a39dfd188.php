<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:18043:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Actualidad</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica">
	  		Argentina, entre los países con menor libertad económica	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica" title="Argentina, entre los pa&iacute;ses con menor libertad econ&oacute;mica">
		    	<img src="/media/k2/items/cache/b320775de3c297425b69dccc362220a9_XS.jpg" alt="Argentina, entre los pa&iacute;ses con menor libertad econ&oacute;mica" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La pérdida de libertad económica que sufrió el país en la última década fue objeto de análisis de un grupo de ONG´s internacionales y locales, que expusieron sus perspectivas, ayer, en la universidad del CEMA. Las razones que llevaron a esta situación fueron el eje central de todas las exposiciones. El seminario fue coordinado por las ONGs internacionales, Fraser Institute, Atlas y Friedrich Naumann, y por las locales, Libertad y Progreso (LyP), Libertad (de Rosario) y Carlos Pellegrini.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia">
	  		Un peligroso espiral de violencia	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia" title="Un peligroso espiral de violencia">
		    	<img src="/media/k2/items/cache/258ee2700b8562b5d51ebf2117179b3d_XS.jpg" alt="Un peligroso espiral de violencia" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Discurso oficial da licencia para agresiones políticas</p>
<p>&nbsp;</p>
<p>El martes 26 de noviembre, faltando 8 días para el cierre del lapso de campaña electoral y 12 para las elecciones municipales; un grupo de motorizados portando franelas y emblemas del partido de gobierno impidió que simpatizantes del candidato a alcalde Miguel Cocchiola se concentraran en Valencia, en la esquina de la Espiga de Oro, en La Isabelica.&nbsp; Sujetos armados con palos, piedras y hasta cabillas los emboscaron. Once personas resultaron golpeadas y con heridas. Héctor Benavides fue hospitalizado debido a que lo golpearon con una cabilla en la frente. En el mismo lugar fue atacada la camioneta en la que se desplazaba el alcalde de Naguanagua, Alejandro Feo La Cruz. Los cristales del vehículo fueron rotos. Sin embargo, la caravana de apoyo se realizó.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/239-lo-que-no-se-mide-no-se-puede-mejorar-indices-para-la-libertad">
	  		&quot;Lo que no se mide, no se puede mejorar&quot; Indices para la Libertad	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/239-lo-que-no-se-mide-no-se-puede-mejorar-indices-para-la-libertad" title="&amp;quot;Lo que no se mide, no se puede mejorar&amp;quot; Indices para la Libertad">
		    	<img src="/media/k2/items/cache/78a38d90a5f5af5857b8e93fa4dd5a84_XS.jpg" alt="&amp;quot;Lo que no se mide, no se puede mejorar&amp;quot; Indices para la Libertad" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p style="text-align: justify;">Lo que no se mide no se puede mejorar. Asumiendo esta frase como premisa, la Red Liberal de América Latina (RELIAL), la <a href="http://www.la.fnst.org/">Fundación Friedrich Naumann para la Libertad</a> (México) y <a href="http://www.caminosdelalibertad.com/">Caminos de la Libertad</a> propiciaron la presentación de tres índices claves para el desarrollo, crecimiento y prosperidad: <a href="index.php/biblioteca/item/235-indice-de-libertad-econ%C3%B3mica-en-el-mundo-2013">Índice de Libertad Económica</a>, <a href="index.php/biblioteca/item/236-%C3%ADndice-internacional-de-derechos-de-propiedad-reporte-2013">Índice de Derechos Propiedad</a> e <a href="index.php/biblioteca/item/57-indice-de-calidad-institucional-2013">Índice de Calidad Institucional</a>.</p>
<p style="text-align: justify;">&nbsp;</p>
<p>Por <a href="https://twitter.com/silviamercadoa?utm_source=fb&amp;utm_medium=fb&amp;utm_campaign=silviamercadoa&amp;utm_content=405110772356759552">Silvia Mercado</a> (RELIAL)</p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/239-lo-que-no-se-mide-no-se-puede-mejorar-indices-para-la-libertad">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/238-metiéndose-en-honduras">
	  		Metiéndose en Honduras	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/238-metiéndose-en-honduras" title="Meti&eacute;ndose en Honduras">
		    	<img src="/media/k2/items/cache/e67ec824afbc9f855ad850f1b49c5b05_XS.jpg" alt="Meti&eacute;ndose en Honduras" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Victor Becerra del blog de <a href="http://wikikeando.wordpress.com/2013/11/25/metiendose-en-honduras/">Victor Becerra</a></p>
<p>&nbsp;</p>
<p style="text-align: justify;">A menos que Xiomara Castro de Zelaya decida dinamitar su incipiente carrera política no reconociendo los resultados de este domingo, las elecciones presidenciales hondureñas fueron anticlimáticas. Para bien de Honduras. A los agobiantes problemas que desangran a Honduras, como el narcotráfico, la violencia y ser uno de los tres países más pobres del continente, no era necesario sumar uno más con un posible gobierno de la familia Zelaya alineado a la estrategia cubano-venezolano de "dominar" la región, además de sus propuestas de "refundar" al Estado hondureño, crear una nueva constitución, impulsar un "socialismo democrático</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/238-metiéndose-en-honduras">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/237-relial-en-atención-y-solidaridad-con-venezuela">
	  		RELIAL en atención y solidaridad con Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/237-relial-en-atención-y-solidaridad-con-venezuela" title="RELIAL en atenci&oacute;n y solidaridad con Venezuela">
		    	<img src="/media/k2/items/cache/84ac056b57dd032fcf18a346d4a81feb_XS.jpg" alt="RELIAL en atenci&oacute;n y solidaridad con Venezuela" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>&nbsp;</p>
<p>RELIAL en atención y solidaridad con Venezuela</p>
<p><strong><span style="font-size: 11px;">Comunidado de VENTE Venezuela/&nbsp;</span><em style="font-size: 11px;">Movimiento político de ciudadanos libres</em></strong></p>
<p>&nbsp;</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/237-relial-en-atención-y-solidaridad-con-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos">
	  		Ya no nos podemos conformar con menos	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos" title="Ya no nos podemos conformar con menos">
		    	<img src="/media/k2/items/cache/7e64c4d2a4a242251ffdaa790b21fa01_XS.jpg" alt="Ya no nos podemos conformar con menos" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>"Ya no nos podemos conformar con menos"</p>
<p>&nbsp;</p>
<p>Comentario sobre el seminario from Government to Good Governance</p>
<p>&nbsp;</p>
<p>Por: Carolina Gómez, cuya participación en el seminario fue posible gracias al apoyo de Relial al Instituto de Ciencia Política (ICP) y a la Revista Perspectiva.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/229-ya-no-nos-podemos-conformar-con-menos">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/actualidad?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/actualidad?start=60" class="pagenav">Anterior</a></li><li><a title="5" href="/index.php/productos/archivo/actualidad?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/actualidad?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/actualidad?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/actualidad?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/actualidad?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/actualidad?start=54" class="pagenav">10</a></li><li><a title="11" href="/index.php/productos/archivo/actualidad?start=60" class="pagenav">11</a></li><li><span class="pagenav">12</span></li><li><a title="13" href="/index.php/productos/archivo/actualidad?start=72" class="pagenav">13</a></li><li><a title="14" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">14</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/actualidad?start=72" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 12 de 14	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:19:"Actualidad - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:69:"http://www.relial.org/index.php/productos/archivo/actualidad?start=66";s:8:"og:title";s:19:"Actualidad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:47:"http://www.relial.org/media/k2/categories/3.jpg";s:5:"image";s:47:"http://www.relial.org/media/k2/categories/3.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}}s:6:"module";a:0:{}}