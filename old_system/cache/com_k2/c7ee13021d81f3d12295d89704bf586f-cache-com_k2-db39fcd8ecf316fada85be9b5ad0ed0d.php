<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10030:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId243"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Un peligroso espiral de violencia
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/258ee2700b8562b5d51ebf2117179b3d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/258ee2700b8562b5d51ebf2117179b3d_XS.jpg" alt="Un peligroso espiral de violencia" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Discurso oficial da licencia para agresiones políticas</p>
<p>&nbsp;</p>
<p>El martes 26 de noviembre, faltando 8 días para el cierre del lapso de campaña electoral y 12 para las elecciones municipales; un grupo de motorizados portando franelas y emblemas del partido de gobierno impidió que simpatizantes del candidato a alcalde Miguel Cocchiola se concentraran en Valencia, en la esquina de la Espiga de Oro, en La Isabelica.&nbsp; Sujetos armados con palos, piedras y hasta cabillas los emboscaron. Once personas resultaron golpeadas y con heridas. Héctor Benavides fue hospitalizado debido a que lo golpearon con una cabilla en la frente. En el mismo lugar fue atacada la camioneta en la que se desplazaba el alcalde de Naguanagua, Alejandro Feo La Cruz. Los cristales del vehículo fueron rotos. Sin embargo, la caravana de apoyo se realizó.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Luis Guillermo Carvajal, periodista de DAT TV, quien se desplazaba hacia el aeropuerto internacional de Valencia Arturo Michelena, en un vehículo junto con su equipo de prensa y el de N24, con la intención de cubrir el regreso de Cocchiola al país, fue interceptado en una de las tres alcabalas que la Guardia Nacional Bolivariana (GNB) había dispuesto en la vía hacia el terminal aéreo. Al ser detenido se resistió a que sin razón alguna le quitaran su documento de identidad y le decomisaran sus equipos de trabajo (cámaras de video, grabadoras) y con ese pretexto un funcionario identificado como L. Palacios G, perteneciente al Comando Regional #2, Destacamento #25, después de esposarlo y con la colaboración de otros guardias nacionales no identificados, lo mantuvo secuestrado por más de 4 horas y lo golpeó en la cabeza y el rostro, lo insultó, lo escupió y lo amenazó con desnudarlo, tomarle fotos y subirlas a las redes públicas para desprestigiarlo.</p>
<p>&nbsp;</p>
<p>Un poco más temprano, cerca de las 12 del mediodía &nbsp;el dirigente opositor y ex candidato presidencial, Henrique Capriles Radonski, denunció a través de su perfil en la red social Twitter (@hcapriles) un acto vandálico cometido en su contra. "Trataron de incendiar la unidad donde nos trasladábamos en Maracay". El tuit estaba acompañado de una imagen que mostraba la parte lateral de un autobús chamuscado, producto del denunciado intento de incendio. Capriles y su equipo se encontraban en Maracay, capital del Estado Aragua, cuando fueron víctimas de agresores que pretendían quemar el autobús. La intervención de un grupo de motorizados que pasaba por allí impidió la consumación del hecho, rescatándolos y ahuyentando a los violentos.</p>
<p>&nbsp;</p>
<p>En horas de la noche, fue asesinado de varios disparos José (Cheo) Chirinos, candidato a concejal por la Mesa de la Unidad Democrática (MUD) por el circuito uno de Baralt, cuando salía de una emisora de radio donde ofrecía una entrevista.</p>
<p>&nbsp;</p>
<p>Se conoció que Chirinos se encontraba en la emisora La Voz de la Patria 88.9 FM en el sector Mene Grande, y en el momento en que salía del lugar llegaron dos individuos a bordo de una motocicleta y sin mediar palabras le dispararon en varias oportunidades. También resultó herida otra persona, que se encontraba junto al candidato.</p>
<p>&nbsp;</p>
<p>Todos estos incidentes, que retratan diferentes espacios de violencia, ocurren en un mismo día, al igual que las tres "cadenas" de Nicolás Maduro, a las 12:44, 4:54 y 6:56 PM, totalizando 2 horas y 42 minutos de trasmisión en donde el poder del Estado opera convertido en maquinaria de propaganda del odio y la exclusión. En cada alocución, en cada intervención, a través del modelaje, se incita al odio, se deshumaniza al adversario, relevando de responsabilidad a los posibles agresores y legitimando a los vengadores de la afrenta contra la "revolución". Esta campaña tal y como advertimos ha escalado a una fase ulterior y muestra ya un peligroso espiral en donde cualquier grupo armado, oficial o paraoficial, puede y suele extralimitarse en su misión y en donde los poderes públicos se mantienen de espalda al país&nbsp; y a sus ciudadanos.</p>
<p>&nbsp;</p>
<p>Una vez más solicitamos su solidaridad y lanzamos un alerta. Les pedimos transmitir este mensaje a todas las redes de aliados por la Democracia. Igualmente, les rogamos mantenerse alertas ante cualquier acción contra el liderazgo de la coalición democrática, pues los peligros que sobre ellos y la ciudadanía se ciernen deben ser conocidos y denunciados con fuerza dentro y fuera de nuestras fronteras</p>
<p>&nbsp;</p>
<p>Enlaces:</p>
<p>&nbsp;</p>
<p>Agresión simpatizantes de Cocchiola:&nbsp;<a href="http://www.el-nacional.com/politica/violencia-trataron-recibimiento-Miguel-Cocchiola_0_307769533.html">http://www.el-nacional.com/politica/violencia-trataron-recibimiento-Miguel-Cocchiola_0_307769533.html</a></p>
<p>&nbsp;</p>
<p>Agresión a Capriles y su equipo:&nbsp;<a href="http://www.ultimasnoticias.com.ve/noticias/actualidad/politica/capriles-r-denuncia-presunta-agresion-en-aragua.aspx">http://www.ultimasnoticias.com.ve/noticias/actualidad/politica/capriles-r-denuncia-presunta-agresion-en-aragua.aspx</a><br />&nbsp;<br /><a href="http://www.sunoticiero.com/index.php/nacionales-not/32724-equipo-de-capriles-en-aragua-venian-dispuestos-a-matar-nos-querian-quemar-vivos-fotos">http://www.sunoticiero.com/index.php/nacionales-not/32724-equipo-de-capriles-en-aragua-venian-dispuestos-a-matar-nos-querian-quemar-vivos-fotos</a></p>
<p>&nbsp;</p>
<p>Agresión a periodista de DAT TV:&nbsp;<a href="http://www.eluniversal.com/nacional-y-politica/131128/lo-que-mas-le-molesto-fue-que-nunca-baje-la-cabeza">http://www.eluniversal.com/nacional-y-politica/131128/lo-que-mas-le-molesto-fue-que-nunca-baje-la-cabeza</a></p>
<p><a href="http://www.entornointeligente.com/articulo/1648580/Comunicado-Colegio-Nacional-de-Periodistas-Carabobo-28112013">http://www.entornointeligente.com/articulo/1648580/Comunicado-Colegio-Nacional-de-Periodistas-Carabobo-28112013</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>fuente: Vente Venezuela</p>
<p>foto:&nbsp;Vente Venezuela</p>
<p>autor:&nbsp;Vente Venezuela</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/239-lo-que-no-se-mide-no-se-puede-mejorar-indices-para-la-libertad">
			&laquo; "Lo que no se mide, no se puede mejorar" Indices para la Libertad		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/247-argentina-entre-los-países-con-menor-libertad-económica">
			Argentina, entre los países con menor libertad económica &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia#startOfPageId243">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:42:"Un peligroso espiral de violencia - Relial";s:11:"description";s:156:"Discurso oficial da licencia para agresiones políticas &amp;nbsp; El martes 26 de noviembre, faltando 8 días para el cierre del lapso de campaña elect...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:33:"Un peligroso espiral de violencia";s:6:"og:url";s:103:"http://www.relial.org/index.php/productos/archivo/actualidad/item/243-un-peligroso-espiral-de-violencia";s:8:"og:title";s:42:"Un peligroso espiral de violencia - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/258ee2700b8562b5d51ebf2117179b3d_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/258ee2700b8562b5d51ebf2117179b3d_S.jpg";s:14:"og:description";s:160:"Discurso oficial da licencia para agresiones políticas &amp;amp;nbsp; El martes 26 de noviembre, faltando 8 días para el cierre del lapso de campaña elect...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:33:"Un peligroso espiral de violencia";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}