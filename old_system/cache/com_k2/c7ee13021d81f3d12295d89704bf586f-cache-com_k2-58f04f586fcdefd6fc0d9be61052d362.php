<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8956:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId361"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Ideas y pensamientos de valor en los 30 años de CEDICE Libertad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/4a071c64184f6ed127d1b90fcde1a863_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/4a071c64184f6ed127d1b90fcde1a863_XS.jpg" alt="Ideas y pensamientos de valor en los 30 a&ntilde;os de CEDICE Libertad" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Durante la jornada de los 30 años de CEDICE aprovechamos para tomarnos varios guayoyos con los ponentes del evento "América Latina: La Libertad es el futuro". En esta nota recolectamos algunos de los pensamientos e ideas expresadas por algunos de los invitados.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p><img src="images/cedice1.jpg" alt="cedice1" width="490" height="300" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;José Luis Cordeiro es un venezolano residente del estado de California, Estados Unidos, en donde se desempeña como profesor de la Universidad de la Singularidad (una institución patrocinada por Google y la NASA y ubicada en Silicon Valley. Allí se encargan de "reunir, educar e inspirar a un grupo de dirigentes que se esfuercen por comprender y facilitar el desarrollo exponencial de las tecnologías y promover, aplicar, orientar y guiar estas herramientas para resolver los grandes desafíos de la humanidad"). Su ponencia en el evento se centro en el papel de la tecnología como parte de un nuevo paradigma energético y petrolero.</p>
<p>&nbsp;</p>
<p>En una agradable charla con Cordeiro le preguntamos ¿cómo haría Venezuela para reintegrarse a la modernidad? "Yo me la paso viajando y estoy convencido de que los jóvenes venezolanos están muy preparados, son súper pilas, quieren vivir en un mundo mejor, han estado involucrados en parte de la situación actual. Yo sé que cuando esto cambie Venezuela va a repuntar rápidamente. Pero hay que estar al día con lo que está pasando en el mundo. Yo ayer estuve recorriendo las protestas, hablando con los jóvenes y me sorprenden con tantas cosas que están haciendo. El problema es salir del gobierno totalitario" respondió. Calificó el evento como genial "es súper primer mundo. El problema es, claro, la situación del país, es complicada y todavía falta un tiempito para que cambie, pero en cuanto lo haga el futuro de Venezuela es muy bueno".</p>
<p>&nbsp;</p>
<p>Por otro lado la alemana Birgit Lamm, directora para América Latina de la Fundación Friedrich Naumann para la Libertad, se presentó en el Centro Cultural Chacao durante el primer día de ponencias del evento. En su intervención habló sobre el socialismo a través del mundo.</p>
<p>&nbsp;</p>
<p>Con Lamm conversamos sobre los programas sociales arraigados en los proyectos socialistas y la forma en que se debe abordar a los beneficiados para presentar otras ofertas "Para los jóvenes es muy importante enterarse de sus opciones, de sus talentos y seguir adelante con responsabilidad en su propia vida. Eso para mí es el mensaje clave, oportunidades para los jóvenes y para todos según sus talentos". Lo que percibe en Venezuela lo califica como "preocupante", refiriéndose a la situación económica, la inflación y la inseguridad "Los jóvenes no salen de noche, no me puedo imaginar una vida de estudiante sin salir o ir al cine o una fiesta, aquí todos se encierran y no se atreven a usar un taxi normal si no es de confianza. Pero al mismo tiempo me dan esperanza los jóvenes, como salen a la calle y protestan por sus intereses", expresó.</p>
<p>&nbsp;</p>
<p><img style="float: right;" src="images/cedice3.jpg" alt="cedice3" width="183" height="194" /></p>
<p>El mexicano Luciano Quadri fue otro de nuestros entrevistados, licenciado en Ciencias políticas y Administración pública, actualmente se desempeña como Coordinador de Asuntos Internacionales en la Secretaria del Medio Ambiente en México.</p>
<p>&nbsp;</p>
<p>En los 30 años de CEDICE participó como ponente en temas energéticos, tanto renovables como no renovables. En relación a esto le pedimos su opinión sobre Venezuela en materia energética "Hay que diversificar la matriz energética. ¿Esto qué significa? que necesitamos diversificar las fuentes de energía, no pueden ser solo termoeléctricas o hidroeléctricas. Así se genera inversión privada, competencia, nuevas tecnologías, desarrollo, industria. Construir y crear por ejemplo paneles solares. Siendo el pilar de la economía (venezolana) la energía, lo más importante es diversificarla" expresó.</p>
<p>&nbsp;</p>
<p>Sobre las ventajas de ser un país petrolero, Quadri respondió que "Todos los recursos son buenos para el país, el problema es como los administras. No se puede decir que es la maldición de los recursos lo que te hará pobre, todo depende de un buen gobierno". Dio como ejemplo a Noruega por poseer una huella de carbono bajísima (bajo índice de contaminación) a pesar de poseer una faja petrolera eficiente y que funciona bajo administración del Estado pero con participación y competencias privadas. "Es una gran empresa y un gran país".</p>
<p>&nbsp;</p>
<p>Autores: Luis Rodrigo y Mónica Duarte</p>
<p>Foto: <a href="http://guayoyoenletras.net/index.php/2012-08-06-05-07-46/personaje/1431-ideas-y-pensamientos-de-valor-en-los-30-anos-de-cedice-libertad">El Guayoyo en letras</a></p>
<p>Fueste:&nbsp;<a href="http://guayoyoenletras.net/index.php/2012-08-06-05-07-46/personaje/1431-ideas-y-pensamientos-de-valor-en-los-30-anos-de-cedice-libertad">El Guayoyo en letras</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/359-el-gran-debate">
			&laquo; El gran debate		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/362-el-mundial-de-la-calidad-institucional">
			El mundial de la Calidad Institucional &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/361-ideas-y-pensamientos-de-valor-en-los-30-años-de-cedice-libertad#startOfPageId361">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:73:"Ideas y pensamientos de valor en los 30 años de CEDICE Libertad - Relial";s:11:"description";s:155:"Durante la jornada de los 30 años de CEDICE aprovechamos para tomarnos varios guayoyos con los ponentes del evento &quot;América Latina: La Libertad e...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:64:"Ideas y pensamientos de valor en los 30 años de CEDICE Libertad";s:6:"og:url";s:131:"http://www.relial.org/index.php/productos/archivo/opinion/item/361-ideas-y-pensamientos-de-valor-en-los-30-años-de-cedice-libertad";s:8:"og:title";s:73:"Ideas y pensamientos de valor en los 30 años de CEDICE Libertad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/4a071c64184f6ed127d1b90fcde1a863_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/4a071c64184f6ed127d1b90fcde1a863_S.jpg";s:14:"og:description";s:159:"Durante la jornada de los 30 años de CEDICE aprovechamos para tomarnos varios guayoyos con los ponentes del evento &amp;quot;América Latina: La Libertad e...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:64:"Ideas y pensamientos de valor en los 30 años de CEDICE Libertad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}