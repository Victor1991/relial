<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:17176:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Actualidad</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/327-el-parlamento-europeo-sobre-la-situación-en-venezuela">
	  		El Parlamento Europeo sobre la situación en Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>&nbsp;</p>
<p style="text-align: center;"><span style="font-size: 10pt; text-align: left; font-family: arial, helvetica, sans-serif;">Resolución del Parlamento Europeo, de 27 de febrero de 2014, sobre la situación en Venezuela (2014/2600(RSP)</span></p>
<p style="text-align: center;"><span style="font-size: 10pt; text-align: left; font-family: arial, helvetica, sans-serif;"><br /></span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;– Vistas sus resoluciones anteriores sobre la situación en Venezuela, en particular su Resolución, de 24 de mayo de 2007, sobre el caso de la cadena Radio Caracas Televisión en Venezuela , de 23 de octubre de 2008 sobre las inhabilitaciones políticas en Venezuela , de 7 de mayo de 2009 sobre el caso de Manuel Rosales en Venezuela , de 11 de febrero de 2010 sobre Venezuela , de 8 de julio de 2010 sobre Venezuela, en particular el caso de María Lourdes Afiuni , y de 24 de mayo de 2012 sobre la posible retirada de Venezuela de la Comisión Interamericana de Derechos Humanos ,</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/327-el-parlamento-europeo-sobre-la-situación-en-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/299-manifiesto-de-mérida-de-los-estudiantes">
	  		Manifiesto de Mérida de los estudiantes	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/299-manifiesto-de-mérida-de-los-estudiantes" title="Manifiesto de M&eacute;rida de los estudiantes">
		    	<img src="/media/k2/items/cache/778faba3e3dc8c6c6db24b403da494ae_XS.jpg" alt="Manifiesto de M&eacute;rida de los estudiantes" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Los estudiantes hemos unificado a la Nación en torno a la conquista de la Libertad de Venezuela. Por eso el régimen castro-comunista con grupos paramilitares y la Guardia Nacional, han asesinado, torturado y apresado compañeros en todo el territorio nacional. El pueblo se ha sumado con nosotros: se ha despertado el espíritu libertario ante la opresión de 15 años de este sistema político. En virtud de este momento histórico, sostenemos ante el país la siguiente postura de manera contundente, honorable e irrenunciable, frente a los que nos quieren dominar y sus colaboradores:</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/299-manifiesto-de-mérida-de-los-estudiantes">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/298-manifiesto-de-mérida-de-los-estudiantes">
	  		Manifiesto de Mérida de los estudiantes	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/298-manifiesto-de-mérida-de-los-estudiantes" title="Manifiesto de M&eacute;rida de los estudiantes">
		    	<img src="/media/k2/items/cache/7ab716354432ee12f19b58c60471093b_XS.jpg" alt="Manifiesto de M&eacute;rida de los estudiantes" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Los estudiantes hemos unificado a la Nación en torno a la conquista de la Libertad de Venezuela. Por eso el régimen castro-comunista con grupos paramilitares y la Guardia Nacional, han asesinado, torturado y apresado compañeros en todo el territorio nacional. El pueblo se ha sumado con nosotros: se ha despertado el espíritu libertario ante la opresión de 15 años de este sistema político. En virtud de este momento histórico, sostenemos ante el país la siguiente postura de manera contundente, honorable e irrenunciable, frente a los que nos quieren dominar y sus colaboradores:</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/298-manifiesto-de-mérida-de-los-estudiantes">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico">
	  		Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico" title="Algunos aspectos de la iniciativa de Ley Federal de Competencia Econ&oacute;mica en M&eacute;xico">
		    	<img src="/media/k2/items/cache/71ab1197965a26d2e4379f8b23c36ebb_XS.jpg" alt="Algunos aspectos de la iniciativa de Ley Federal de Competencia Econ&oacute;mica en M&eacute;xico" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En general, se trata de una iniciativa que actualiza de manera sustantiva el perfil de la Comisión de Competencia Económica y la dota de herramientas vanguardistas en el combate a prácticas anticompetitivas. Sin embargo, el proceso en que se apruebe debe ser abierto a la sociedad y a los propios agentes económicos.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/293-declaración-de-los-amigos-de-la-carta-democrática-interamericana-sobre-la-situación-en-venezuela">
	  		Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p style="text-align: center;"><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">Los Amigos de la Carta Democrática Interamericana expresan su rechazo ante los hechos ocurridos con ocasión de la manifestación pacífica convocada por organizaciones estudiantiles el 12 de febrero pasado en Venezuela. En este sentido, el grupo de Los Amigos lamenta la pérdida de vidas humana y los heridos, y hace pública su enérgica condena a la detención de más de 100 estudiantes, algunos de los cuales han denunciado que sufrieron atentados a su integridad física.</span></p>
<p><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><br /></span></p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/293-declaración-de-los-amigos-de-la-carta-democrática-interamericana-sobre-la-situación-en-venezuela">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks">
	  		Think Tanks And The Power of Networks	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks" title="Think Tanks And The Power of Networks">
		    	<img src="/media/k2/items/cache/dbe05350458c15fa6c802fb686391131_XS.jpg" alt="Think Tanks And The Power of Networks" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>The output of think tanks usually falls into four categories: published research, educational programs, advocacy campaigns, and "do-tank" products. "Do-tanks" provide services, such as pro bono legal defense, or management and technical support.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/actualidad/item/292-think-tanks-and-the-power-of-networks">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/actualidad?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/actualidad?start=42" class="pagenav">Anterior</a></li><li><a title="4" href="/index.php/productos/archivo/actualidad?start=18" class="pagenav">4</a></li><li><a title="5" href="/index.php/productos/archivo/actualidad?start=24" class="pagenav">5</a></li><li><a title="6" href="/index.php/productos/archivo/actualidad?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/actualidad?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/actualidad?start=42" class="pagenav">8</a></li><li><span class="pagenav">9</span></li><li><a title="10" href="/index.php/productos/archivo/actualidad?start=54" class="pagenav">10</a></li><li><a title="11" href="/index.php/productos/archivo/actualidad?start=60" class="pagenav">11</a></li><li><a title="12" href="/index.php/productos/archivo/actualidad?start=66" class="pagenav">12</a></li><li><a title="13" href="/index.php/productos/archivo/actualidad?start=72" class="pagenav">13</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/actualidad?start=54" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/actualidad?start=78" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 9 de 14	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:19:"Actualidad - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:65:"http://relial.org/index.php/productos/archivo/actualidad?start=48";s:8:"og:title";s:19:"Actualidad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:43:"http://relial.org/media/k2/categories/3.jpg";s:5:"image";s:43:"http://relial.org/media/k2/categories/3.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}}s:6:"module";a:0:{}}