<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11564:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId439"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Qual Brasil queremos? - Membros da RELIAL participaram do debate (Portugués)
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/b7e607e23f1646b9ce9f7d0da4fbe580_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/b7e607e23f1646b9ce9f7d0da4fbe580_XS.jpg" alt="Qual Brasil queremos? - Membros da RELIAL participaram do debate (Portugu&eacute;s)" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Vários institutos parceiros da Fundação Friedrich Naumann para a Liberdade (FNF) no Brasil realizaram em setembro de 2014 quatro grande eventos em diferentes partes do Brasil para debater uma única questão: Qual Brasil queremos? - um tema de grande importância, especialmente neste momento, na véspera das eleições no Brasil. Em outubro de 2014 o Brasil vai escolher quem vai governar o país pelos próximos quatro anos em um dos momentos mais difíceis da economia nas últimas décadas.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>A série de eventos começou nos dias 6 e 7 de setembro em São Paulo com a "Conferência da Escola Austríaca" do Instituto Ludwig von Mises, seguiu com o "5º Fórum Liberdade e Democracia" do Instituto de Formação de Líderes (IFL) de Belo Horizonte, no dia 8 de setembro e com o "1º Fórum Liberdade e Democracia" do IFL de São Paulo, no dia 9 de setembro e terminou com o "Congresso Nacional 2014" do Estudantes pela Liberdade (EpL), nos dias 13 e 14 de setembro em São Paulo. A FNF Brasil apoiou ambos os Fórums do IFL (em Belo Horizonte e São Paulo) bem como o Congresso Nacional 2014 do EpL.</p>
<p>&nbsp;</p>
<p>Os dois integrantes da Relial – Red Liberal de America Latina - Ricardo López Murphy, ex-ministro de Defesa e de Economia da Argentina durante o governo de Fernando de La Rúa, e presidente da Relial e o colunista Carlos Alberto Montaner, membro do conselho editorial do jornal norte-americano Miami Herald e analista político da CNN em espanhol bem como o americano Ron Paul, ex-congressista republicano do Texas foram os palestrantes de destaque do 1º Fórum Liberdade e Democracia do IFL em São Paulo.</p>
<p>&nbsp;</p>
<p>Com o título do seu Fórum o IFL provocou um amplo debate sobre quais são os principais problemas do Brasil e quais as alternativas para solucioná-los.</p>
<p>&nbsp;</p>
<p>O discurso de abertura do Ron Paul abordou a política, economia, a defesa das liberdades individuais, o respeito ao indivíduo e a tolerância. Para ele, a proteção da liberdade em todos os âmbitos é o objetivo principal. Depois da sua palestra ele respondeu a perguntas do público presente, entre outras, sobre o sentido de uma guerra e a sua opinião sobre a moeda virtual Bitcoin. A revista Veja aproveitou a vinda do Ron Paul ao 1º Fórum Liberdade e Democracia em SP para realizar uma entrevista exclusiva que foi publicada nas suas páginas amarelas desta semana com o título "Radical Libertário".</p>
<p>&nbsp;</p>
<p>No painel "Intervencionismo e suas Consequências", Ricardo López Murphy focou às intervenções do Governo argentino, especialmente nas áreas econômica, monetária e fiscal. Em uma entrevista para a Revista Exame após o evento, Ricardo López Murphy falou sobre a influência do Brasil nos países vizinhos. Segundo ele, a chegada da candidata à presidência, Marina Silva, levaria a uma pressão por mudanças em outros paísesl atino-americanos. (veja também: http://exame.abril.com.br/brasil/noticias/mudanca-politica-no-brasil-afetara-paises-vizinhos-diz-ex-ministro-argentino)</p>
<p>&nbsp;</p>
<p>O jornalista Carlos Alberto Montaner, Presidente de honra da Unión Liberal Cubana, membro da RELIAL e eleito pela revista Foreign Policy como um dos 50 intelectuais mais influentes do mundo ibero-americano falou sobre os 11 princípios de um Estado livre, sem intervenções. O escritor e exilado cubano citou, entre outros, a subsidiariedade, o estado de direito e a transparência, a educação e a eficiência, bem como a competitividade e a privatização. Também destacou a importância de permitir lucros para obter uma sociedade próspera. Além disso, refletiu sobre a relação entre a intervenção estatal e a corrupção.</p>
<p>&nbsp;</p>
<p>A deputada venezuelana cassada Maria Corina Machado, infelizmente foi impedida de viajar ao Brasil para participar no 1º Fórum Liberdade e Democracia SP. Mas, enviou uma mensagem de vídeo ao Fórum Liberdade e Democracia SP no qual ela lamenta muito que Chávez tenha deixado a Venezuela pior que há 50 anos. (veja http://www1.folha.uol.com.br/fsp/mundo/184889-deputada-cassada-diz-que-chavez-deixou-pais-pior-que-ha-50-anos.shtml)</p>
<p>&nbsp;</p>
<p>A Liberdade de expressão e a Liberdade de imprensa no Brasil foram tópicos da palestra de Patrícia Blanco, Presidente Executiva do Instituto Palavra Aberta, entidade que vem se destacando na defesa e na promoção da liberdade de expressão e da liberdade de imprensa. Ela relatou que o Brasil tem mais do que 200 projetos de lei para restringir ou regular a publicidade e alertou para o fato de que, infelizmente, a interatividade das novas mídias é utilizada cada vez mais para ameaças da liberdade de expressão na web, um direito fundamental. Além disso, Blanco mostrou-se muito preocupada com o fato de o Portal Eleição Transparente (http://www.eleicaotransparente.com.br/#/infografico/estado), organizado pela Associação Brasileira de Jornalismo Investigativo (Abraji) com a ajuda de empresas de mídia e tecnologia que costumam ser alvos de processos de supressão de informações em período eleitoral, já ter registrado até o dia 9 de setembro de 2014, 94 ações no ambiente digital. Ou seja, até agora, candidatos às eleições de 2014 entraram com 94 ações na justiça brasileira lutando contra a divulgação de informações.</p>
<p>&nbsp;</p>
<p>Alexandre Schwartsman, um dos principais economistas brasileiros e ex-Diretor do Banco Central, além de economista-chefe para a América Latina do ABN Amro Bank e do Grupo Santander Brasil, comentou as perspectivas para o Brasil em 2015. Segundo ele "se fizer a coisa certa, 2015 será um ano 'horroroso' para o Brasil". Ou seja, ajustes fiscais, a liberação de preços controlados e um câmbio flutuante devem afetar a economia brasileira no ano que vem, embora sejam medidas necessárias para preparar um futuro crescimento econômico. (veja também: http://www.infomoney.com.br/mercados/noticia/3568313/fizer-coisa-certa-2015-sera-ano-horroroso-para-brasil-diz)</p>
<p>&nbsp;</p>
<p>O Fórum contou também com a presença do humorista Danilo Gentili. Ele foi homenageado com o Prêmio Liberdade por sua coragem na defesa das liberdades individuais, contra toda espécie de autoridade, especialmente a estatal. Gentili, que ficou visivelmente emocionado com o recebimento deste prêmio, realizou no dia 13 de setembro um Stand up na Conferência Nacional 2014 do EpL em São Paulo.</p>
<p>&nbsp;</p>
<p>O 1º Fórum Liberdade e Democracia na capital paulista, promovido pelo Instituto de Formação de Líderes de São Paulo (IFL/SP), reuniu empresários e especialistas para apresentar um debate de ideias e caminhos para o desenvolvimento do Brasil, abrangendo as perspectivas econômica, política e social a partir da experiência de países da América Latina.</p>
<p>&nbsp;</p>
<p>O evento já acontece na cidade de Belo Horizonte (MG) desde 2010, organizado pelo Instituto de Formação de Líderes de Belo Horizonte (IFL/BH). A inspiração para a realização do evento foi o Fórum da Liberdade que acontece há mais de 27 anos em Porto Alegre, organizado pelo Instituto de Estudos Empresariais (IEE), que é parceiro de longa data da FNF no Brasil.</p>
<p>&nbsp;</p>
<p>O Diretor do IFL de SP, Tomás Martins, e seus colegas, estão de parabéns pela organização do Fórum, que ofereceu ao público bastante material para reflexão. Afinal, qual Brasil queremos?</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Beate Forbriger</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: FNF Brasil</p>
<p>www.spforumliberdadedemocracia.com.br</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/436-reporte-visita-de-martín-krause-a-venezuela">
			&laquo; Reporte, visita de Martín Krause a Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/440-la-idiotez-es-eterna">
			La idiotez es eterna &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/439-qual-brasil-queremos?-membros-da-relial-participaram-do-debate-portugues#startOfPageId439">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:86:"Qual Brasil queremos? - Membros da RELIAL participaram do debate (Portugués) - Relial";s:11:"description";s:156:"Vários institutos parceiros da Fundação Friedrich Naumann para a Liberdade (FNF) no Brasil realizaram em setembro de 2014 quatro grande eventos em dif...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:77:"Qual Brasil queremos? - Membros da RELIAL participaram do debate (Portugués)";s:6:"og:url";s:142:"http://www.relial.org/index.php/productos/archivo/actualidad/item/439-qual-brasil-queremos?-membros-da-relial-participaram-do-debate-portugues";s:8:"og:title";s:86:"Qual Brasil queremos? - Membros da RELIAL participaram do debate (Portugués) - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/b7e607e23f1646b9ce9f7d0da4fbe580_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/b7e607e23f1646b9ce9f7d0da4fbe580_S.jpg";s:14:"og:description";s:156:"Vários institutos parceiros da Fundação Friedrich Naumann para a Liberdade (FNF) no Brasil realizaram em setembro de 2014 quatro grande eventos em dif...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:77:"Qual Brasil queremos? - Membros da RELIAL participaram do debate (Portugués)";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}