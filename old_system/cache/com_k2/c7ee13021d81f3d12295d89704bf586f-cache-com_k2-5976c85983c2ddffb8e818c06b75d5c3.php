<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16541:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId271"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Vargas Llosa: &quot;Se debe defender el pluralismo informativo&quot;
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/16db9b88d9515580a78d5965b066ac2d_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/16db9b88d9515580a78d5965b066ac2d_XS.jpg" alt="Vargas Llosa: &amp;quot;Se debe defender el pluralismo informativo&amp;quot;" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>Un reportaje de Raúl Peñaranda, Especial para Página Siete</p>
<p>&nbsp;</p>
<p>El afamado novelista y ensayista peruano, ganador del Premio Nobel 2010, considera que los monopolios y la concentración mediática son negativos para la democracia. Reiteró sus críticas al presidente Evo Morales, pero dijo que se debe "respetar" a su gobierno.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Además de ser un reconocido novelista, Mario Vargas Llosa es un afamado y potente ensayista, quien -a través de los textos que distribuye internacionalmente- defiende unas ideas tan liberales y librepensantes como polémicas y, a veces, inesperadas. Por ejemplo, hace tres años se lanzó contra la entonces candidata peruana Keiko Fujimori, hija del expresidente, y respaldó al postulante insospechado, Ollanta Humala, quien -según dice- desde que asumió el poder "no lo ha defraudado". Junto con ello, un liberal como él está ahora a favor de ponerle límites a los monopolios de la prensa y en ese sentido respalda una campaña contra el tradicional diario limeño El Comercio. Así es Vargas Llosa, dispuesto, a los 77 años, a seguir asumiendo batallas a favor de lo que él llama "la libertad". En ese marco, continúa siendo crítico de los gobiernos "populistas" de América Latina, especialmente uno, el que dirige el presidente Evo Morales. Dialogué con Vargas Llosa de éste y otros temas durante casi una hora en la que fue la más extensa entrevista exclusiva concedida durante su estadía en Santa Cruz.</p>
<p>&nbsp;</p>
<p>Usted es una celebridad a la que siempre se le pide opiniones. ¿Le aburre dar entrevistas?</p>
<p>&nbsp;</p>
<p>Bueno, depende, a veces las entrevistas son muy aburridas (risas). Depende de quién las hace, a veces son entretenidas y a uno lo hacen trabajar intelectualmente, pero hay preguntas que tienen lugares comunes y que exigen respuestas también con lugares comunes.</p>
<p>&nbsp;</p>
<p>¿Y cuáles son las preguntas que a usted le desagradan más?</p>
<p>&nbsp;</p>
<p>Las que tienen que ver con la chismografía, las que tienen que ver con la maledicencia, que es hoy un alimento muy favorecido por la prensa, que tiene que ver con la prensa amarilla y que va penetrando también, lamentablemente, a la prensa seria, porque existe esa demanda de escándalo, de amarillismo, creo que es uno de los problemas de la información de nuestro tiempo.</p>
<p>&nbsp;</p>
<p>Hay algunas personas en Bolivia que creen que usted es "lo más cercano a un Nobel que Bolivia va a tener", por su ligazón con el país por haber pasado su infancia en Cochabamba. ¿Qué opina usted de esa apreciación? Su discurso de aceptación del Premio Nobel empezó diciendo que su experiencia más importante –aprender a leer– ocurrió en Cochabamba. Y su primera esposa fue boliviana...</p>
<p>&nbsp;</p>
<p>Bueno, y la segunda en cierto modo también, porque Patricia nació en Cochabamba, aunque a los 40 días de nacida fue llevada a Perú, pero nació en Bolivia. Así que claramente tengo una proclividad sentimental por las bolivianas (risas).</p>
<p>&nbsp;</p>
<p>Pero hay algo de cierto en eso, yo fui formado por Bolivia. Mis primeros 10 años fueron bolivianos. Creo que en esos años se formaron ciertos ingredientes de la personalidad que desde entonces me han acompañado por el mundo. Sí, de alguna manera soy tan boliviano como peruano, como español. También debo ser algo francés o inglés, porque también viví muchos años en ambas culturas. Yo creo que es una tendencia de la época, cada vez pertenecemos a más culturas, a más sociedades, porque hay una movilidad extraordinaria que no había en el pasado. Y eso creo que es bueno, ojalá fuéramos todos ciudadanos del mundo, habría menos prejuicios y enconos, que nacen muchas veces del nacionalismo. Si uno viaja por el mundo es muy difícil que sea nacionalista, descubre que hay una comunidad de intereses y de estímulos que hace que los humanos, aunque hablemos distintos idiomas y tengamos costumbres diversas, compartamos una identidad muy profunda que nos acerca más que nos separa.</p>
<p>&nbsp;</p>
<p>Pero yo veo en el entusiasmo de esta visita, quizás me equivoco, una especie de orgullo porque este señor, que es un afamado escritor, es de alguna manera un poco boliviano.</p>
<p>&nbsp;</p>
<p>Gracias. Bueno, yo recuerdo mucho los años de mi infancia porque fueron para mí años de felicidad; yo viví en esos años realmente lo que llaman el paraíso de la infancia; yo tenía una familia muy grande, casi bíblica, y vivía arropado por mi madre, mis abuelos, mis tíos, era un niño muy engreído.</p>
<p>&nbsp;</p>
<p>¿Qué opina sobre la virulenta reacción del Gobierno boliviano sobre su llegada al país?</p>
<p>&nbsp;</p>
<p>El presidente Morales decía que yo venía a intervenir en la política boliviana, que me mandaba a Bolivia un señor que yo ni conozco, que no sé quién es (Sánchez Berzaín, NdE), y todo eso me pareció un disparate, una paranoia, yo dije el otro día que no soy peligroso (risas). Las opiniones que tengo sobre el Gobierno boliviano las he expuesto muchísimas veces y he sido muy explícito sobre las cosas que critico y por qué lo hago. Pero no he venido a Bolivia a conspirar, desde luego que no. He venido por una invitación cordialísima de mi amigo Óscar Ortiz y de la Fundación Nueva Democracia, que agradezco muchísimo. Y sobre todo he venido a conocer las misiones jesuíticas, que son algo sobre lo que tenía una gran curiosidad, unas ganas muy grandes de poder visitarlas y me alegra enormemente haberlo hecho. La verdad es que ha sido una experiencia extraordinaria. Y ha sido también una experiencia inolvidable comprobar el dinamismo y la pujanza de Santa Cruz y de los cruceños, que empujan al país entero hacia adelante.</p>
<p>&nbsp;</p>
<p>¿Usted ha tenido con respecto al Gobierno boliviano un cambio de opinión? Dijo al recibir el Nobel que Bolivia es una de las "democracias populistas y payasas" del continente. Pero en su viaje a Bolivia ha señalado que respalda a un gobierno que nace del voto popular.</p>
<p>&nbsp;</p>
<p>Ambas cosas son verdad. Lo que yo he dicho es que respeto a los presidentes que son elegidos democráticamente, sí. Lamento mucho que la gente elija presidentes populistas, porque creo que siempre que ocurre a sus países les va mal y a veces muy mal, y que desaprovechan muchísimas oportunidades. Pero si son elegidos, y uno cree en la democracia, hay que aceptar esos resultados. La gente debe elegir, lo cual no quiere decir que siempre elija bien.</p>
<p>&nbsp;</p>
<p>Yo creo que el boliviano es un gobierno populista que no está trabajando bien por el futuro del país. Y pienso que la oposición boliviana tiene un trabajo fuerte que hacer para cambiar la manera de pensar de esa mayoría de bolivianos. Me parece que los electores que apoyan a Evo están cavando su propia ruina; el populismo siempre arruina a los países, los empobrece, crea además un desbarajuste en el campo institucional, que a la larga y a la corta se paga con la reducción de la calidad de la democracia. Yo no respeto a los dictadores, los dictadores me parecen despreciables y hay que enfrentarlos. Y a los gobiernos democráticos hay que respetarlos, aunque sin dejar de criticar lo que consideramos que está mal.</p>
<p>&nbsp;</p>
<p>Usted ha apoyado al gobierno de Ollanta Humala. Pero en Perú hay sectores que critican a Humala con los mismos argumentos con los que la oposición critica a Morales en Bolivia; por ejemplo, acusaciones de que es populista y proclive al autoritarismo.</p>
<p>&nbsp;</p>
<p>Ello es falso, esas críticas son infundadas. El gobierno de Humala es un gobierno que pudo haber sido peligroso porque en su momento estuvo muy cerca de Chávez. En la primera vuelta de su segunda candidatura yo no voté por él, voté por Toledo. Y quedaron para la segunda vuelta Humala y Keiko Fujimori, la hija del dictador, en realidad era el dictador el que dirigía la campaña desde la cárcel. Su victoria hubiera significado la reivindicación de una dictadura horrorosa, que fue ladrona, asesina. En ese momento fue Humala quien me pidió una entrevista, no fue iniciativa mía. Tuvimos una larga conversación y me impresionó mucho la buena voluntad que él mostró, además de su promesa de que no aplicaría su programa original a fin de conseguir el apoyo de los sectores que no son colectivistas ni socialistas o nacionalistas.</p>
<p>&nbsp;</p>
<p>Luego fue a la Universidad de San Marcos y señaló una "hoja de ruta" que ha cumplido escrupulosamente. La democracia en Perú no ha sufrido merma alguna, absolutamente ninguna. Y la política de economía de mercado se ha mantenido como durante el gobierno de Toledo o el segundo mandato de Alan García. Entonces, comparar al gobierno de Humala con el de Evo Morales es una injusticia flagrante.</p>
<p>&nbsp;</p>
<p>Usted ha polemizado con el diario El Comercio en el tema de las acusaciones que se han lanzado contra éste por un intento de monopolizar el mercado de diarios en su país. ¿Cuáles son los argumentos suyos en ese sentido? ¿Por qué es importante el pluralismo informativo?</p>
<p>&nbsp;</p>
<p>La Constitución peruana señala que en Perú no puede haber monopolio ni acaparamiento. Entonces, la compra por parte de El Comercio del grupo Epensa le ha dado prácticamente el 80% de la información y la publicidad de diarios de mi país. Bueno, eso es acaparamiento y casi, casi, un monopolio. Si uno tiene ese grado de control, la manipulación puede ser extraordinaria, sobre todo en épocas electorales. Entonces, hay que mantener el pluralismo informativo y la independencia de los medios, en Perú, en Bolivia y en todos los países, y hay que luchar contra los monopolios y la concentración, que afectan a la democracia. En mi país un grupo de ocho periodistas ha presentado un amparo al Poder Judicial contra el monopolio de El Comercio y creo que ésa es la vía que hay que apoyar.</p>
<p>&nbsp;</p>
<p>Usted ha dicho en varias ocasiones que se debe trabajar para lograr mejores días para los pueblos indígenas del continente, incluso utilizando la palabra "emancipación". Pero usted critica a un presidente, como Evo Morales, que justamente dice representar a los indígenas.</p>
<p>&nbsp;</p>
<p>Que diga que representa a los indígenas no quiere decir que lo esté haciendo. Quiere decir o que él está equivocado o que está usando una fórmula demagógica. Los indígenas de Bolivia y de otros países no necesitan demagogia, necesitan crear una situación que les permita salir de la pobreza, salir de la ignorancia, tener oportunidades para desarrollarse. El problema es otro. ¿Cómo hace un país que tiene culturas distintas para darles a todas ellas igualdad de oportunidades y permitirles que alcancen la modernidad y el progreso?</p>
<p>&nbsp;</p>
<p>Para eso no hay una sola receta eficiente y eficaz que podamos utilizar. La triste realidad es que los países que tienen culturas avanzadas y culturas primitivas, a la hora de progresar, las primitivas van desapareciendo, se van extinguiendo, se van quedando más aisladas, porque las culturas digamos "más modernas" absorben o, sin quererlo, destruyen a las otras. Eso no es justo, pero si queremos corregir esa injusticia, debemos crear un modelo que sea eficaz. Ningún país lo ha conseguido, ninguno. Los países que tenían culturas avanzadas y culturas primitivas han terminado, lamentablemente, afectando a las primitivas.</p>
<p>&nbsp;</p>
<p>La fórmula ideal sería que esas culturas, digamos indígenas, pudieran conservar todo lo que hay en ellas que sea compatible con la modernidad. Respetar sin duda su lengua, ciertas costumbres, ciertas tradiciones y renunciar a ciertas cosas que son incompatibles con la modernidad.</p>
<p>&nbsp;</p>
<p>NdE.- Ésta es una parte de la entrevista que Raúl Peñaranda le realizó a Mario Vargas Llosa, la otra, referida a su actividad literaria, se publica en el suplemento Ideas de hoy.</p>
<p>&nbsp;</p>
<p>"Los electores que apoyan a Evo están cavando su propia ruina; el populismo siempre arruina a los países".</p>
<p>&nbsp;</p>
<p>"Lo que yo he dicho es que respeto a los presidentes que son elegidos democráticamente, sí".</p>
<p>&nbsp;</p>
<p>"Yo fui formado por Bolivia... De alguna manera soy tan boliviano como peruano o como español".</p>
<p>&nbsp;</p>
<p>Fuente: <a href="http://www.paginasiete.bo/nacional/2014/2/2/debe-defender-pluralismo-informativo-12936.html">Página siete</a></p>
<p>Foto: Fundación Nueva Democracia</p>
<p>Autor:&nbsp;Raúl Peñaranda</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/268-reeleccionismo-mágico-latinoamericano">
			&laquo; Reeleccionismo mágico latinoamericano		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/274-los-partidos-se-alteran-los-sistemas-se-reemplazan">
			Los partidos se alteran, los sistemas se reemplazan &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/271-vargas-llosa-se-debe-defender-el-pluralismo-informativo#startOfPageId271">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:67:"Vargas Llosa: "Se debe defender el pluralismo informativo" - Relial";s:11:"description";s:156:"Un reportaje de Raúl Peñaranda, Especial para Página Siete &amp;nbsp; El afamado novelista y ensayista peruano, ganador del Premio Nobel 2010, conside...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:68:"Vargas Llosa: &quot;Se debe defender el pluralismo informativo&quot;";s:6:"og:url";s:166:"http://relial.org/index.php/productos/archivo/opinion/item/271-vargas-llosa-se-debe-defender-el-pluralismo-informativo/index.php/2013-03-28-23-35-13/propiedad-privada";s:8:"og:title";s:77:"Vargas Llosa: &quot;Se debe defender el pluralismo informativo&quot; - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/16db9b88d9515580a78d5965b066ac2d_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/16db9b88d9515580a78d5965b066ac2d_S.jpg";s:14:"og:description";s:160:"Un reportaje de Raúl Peñaranda, Especial para Página Siete &amp;amp;nbsp; El afamado novelista y ensayista peruano, ganador del Premio Nobel 2010, conside...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:58:"Vargas Llosa: "Se debe defender el pluralismo informativo"";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}