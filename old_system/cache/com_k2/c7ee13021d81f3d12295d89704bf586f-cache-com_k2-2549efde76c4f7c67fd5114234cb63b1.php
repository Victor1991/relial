<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:13161:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId503"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Las elecciones parlamentarias en Venezuela: ambiente previo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/6ef598b10d1539793d4ace8d8b7b613f_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/6ef598b10d1539793d4ace8d8b7b613f_XS.jpg" alt="Las elecciones parlamentarias en Venezuela: ambiente previo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><strong>Las elecciones parlamentarias en Venezuela: ambiente previo</strong></p>
<p><strong>REPORTE RELIAL</strong></p>
<p>Trino Márquez, director académico <a href="http://cedice.org.ve/">CEDICE Libertad</a></p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p><strong><br /></strong></p>
<p><strong><br /></strong></p>
<p><strong>1. Informaciones importantes</strong></p>
<p>El próximo 6 de diciembre se realizarán en Venezuela las elecciones de diputados a la Asamblea Nacional, organizadas por el Consejo Nacional Electoral (CNE), institución en la cual el Gobierno cuenta con cuatro de los cinco rectores que lo dirigen. Las decisiones se toman por mayoría.&nbsp;</p>
<p>&nbsp;</p>
<p>El país cuenta con 30.825.782 habitantes, de los cuales 19.504.106 se encuentran en el Registro Electoral (REP). Las encuestadoras indican una intención de ir a votar cercana a 70%. Se seleccionarán 167 parlamentarios repartidos en 87 circunscripciones electorales. El sistema de votación es mixto. 113 parlamentarios serán votados de forma nominal y 51 mediante el llamado "voto lista" o de partidos. Además, desde la Constitución de 1999, se incluye la selección de tres parlamentarios indígenas en representación de esas comunidades.</p>
<p>&nbsp;</p>
<p>Las dos fuerzas más importantes que concurren al proceso son el Gran Polo Patriótico Bolivariano, conformado por la alianza oficialista, cuya principal organización es el Partido Socialista Unido de Venezuela (PSUV), en el gobierno desde el 2 de febrero de 1999; y la Mesa de la Unidad Democrática (MUD), que abarca una amplia gama de partidos y organizaciones opositoras, que van desde la socialdemocracia y el socialcristianismo hasta la izquierda moderada. Asisten como factores independientes algunos grupos que se han desprendido del Polo Patriótico o de la MUD. En este último caso, la formación de varios de esos pequeños grupos ha sido propiciada por el oficialismo para escamotear a sus adversarios.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>2. El ambiente preelectoral</strong></p>
<p><strong>La situación socioeconómica</strong></p>
<p>Todas las encuestadoras importantes, incluidas las que trabajan para el Gobierno, dan una clara ventaja a la oposición. La diferencia varía entre 15 y 30 puntos porcentuales. Esta diferencia tan marcada, que expresa el rechazo al mandato de Nicolás Maduro y Diosdado Cabello, pues hay un gobierno bicéfalo, es el resultado de los graves problemas económicos y sociales existentes. Venezuela es la nación con la más alta inflación del planeta. 2015 cerrará con una tasa superior a 200% en el renglón de los alimentos; a este factor se suman la escasez y el desabastecimiento, especialmente de medicamentos, nunca antes vistos en la nación desde que el Banco Central lleva esos registros. Para ocultar esas cifras, el Banco Central dejó de publicarlas a partir de diciembre de 2014. Una de las expresiones más dramáticas de la crisis económica se encuentra en las largas colas (famosas en los noticieros del continente) para conseguir productos de primera necesidad y medicinas, que se forman alrededor de las farmacias y automercados públicos y privados. A estos conflictos deben sumarse el deterioro global de los servicios públicos, la corrupción, la inseguridad personal y la violencia. Venezuela es, según, Latinobarómeto, uno de los países más corruptos e inseguros de la región.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Este cuadro, presentado apenas en sus rasgos más gruesos, ha conducido al deterioro de la imagen del régimen. Este fenómeno es captado por las encuestas de opinión. Ante la erosión, ¿cuál ha sido la respuesta del oficialismo?</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>El ventajismo, abuso de poder y confusión desembozados</strong></p>
<p>&nbsp;</p>
<p>El régimen venezolano está reñido con las elecciones transparentes, justas y equilibradas. Convoca los comicios para cumplir con las formas y mostrar una fachada democrática. Como se encuentra acorralado por la Constitución y la vigilancia internacional, apela a formas grotescas de ventajismo y abuso de poder. En la Constitución de 1999 se prohíbe el financiamiento público de las organizaciones políticas. Este veto opera para los partidos opositores, pero no para los oficialistas, que cuentan con los inmensos recursos del Gobierno y el Estado.</p>
<p>&nbsp;</p>
<p>El presidente Maduro convirtió las elecciones legislativas en un plebiscito. Se transformó en el "líder" de la campaña, solo que de una manera muy extraña: la imagen utilizada para apoyar sus candidatos no es la de él, sino la de Hugo Chávez. La milmillonaria propaganda se fundamenta en la figura del caudillo desaparecido. A los funcionarios de los organismos públicos se les obliga a buscar votos para los aspirantes oficialistas. Cada empleado debe conseguir diez personas que vayan a sufragar y es presionado a participar en los actos de apoyo a los aspirantes del Polo Patriótico.</p>
<p>&nbsp;</p>
<p>El Sistema Nacional de Medios Públicos -conformado por una extensa red de televisoras de señal abierta, radios, emisoras comunitarias, periódicos- financiado con fondos públicos, se volcó al apoyo de los postulados por el oficialismo. Durante todo el día promueven a sus candidatos y denigran de los opositores.</p>
<p>&nbsp;</p>
<p>El Gobierno utilizó la mayoría de rectores que controla en el CNE para diseñar eu tarjetón electoral que propicia la confusión de los votantes opositores. Al lado derecho de la tarjeta de la MUD, que dice UNIDAD, colocó una tarjeta casi idéntica con la leyenda MIN UNIDAD. El MIN (Movimiento de Integración Nacionalista) es una agrupación financiada por el régimen, que se autoproclama, con el respaldo del Gobierno, en la "verdadera" oposición, e invirtió ingentes recursos para confundir al elector de la alternativa democrática. Buena parte de los reducidos fondos de los opositores han tenido que destinarse a aclarar la confusión.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Presos políticos y violencia</strong></p>
<p>Las elecciones suelen efectuarse en ambientes donde no existen presos políticos. El régimen venezolano no cree en ese principio. De acuerdo con el Foro Penal Venezolano, ONG especializada en el tema, en Venezuela se encuentran en las cárceles 87 presos de conciencia, más de los que hay en Cuba. Los más emblemáticos son el líder del partido Voluntad Popular, Leopoldo López, y los alcaldes Antonio Ledezma y Daniel Ceballos, cuya libertad ha sido exigida por expresidentes y exjefes de Estado de Hispanoamérica, la Unión Europea y numerosas organizaciones y parlamentos de países democráticos e intelectuales de prestigio internacional. Los dirigentes democráticos y los aspirantes de la oposición viven bajo la continua amenaza del Gobierno, que ha politizado el Poder Judicial para utilizarlo contra los factores que lo adversan.</p>
<p>&nbsp;</p>
<p>En repetidas oportunidades Nicolás Maduro, Diosdado Cabello y otros altos dirigentes del régimen, han señalado que las elecciones deben ganarse "como sea". Han amenazado con tomar las calles en caso de que pierdan. La intimidación ha servido para fomentar los atropellos, las arbitrariedades y sobre todo la violencia contra la oposición. En dos recorridos –uno en el estado Miranda y otro en el estado Bolívar- el grupo que acompañaba al excandidato presidencial y gobernador de Miranda, Henrique Capriles, fue agredido por bandas oficialistas con armas de fuego. El 25 de noviembre, el secretario juvenil del partido Acción Democrática en Altagracia de Orituco, estado Guárico, fue asesinado por un sector vinculado con el Gobierno mientras acompañaba en un recorrido por esa zona a Lilian Tintori, esposa de Leopoldo López. Estos episodios de violencia se han repetido a lo largo del breve período de solo tres semanas que dura la campaña electoral.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>La vigilancia internacional</strong></p>
<p>Los desmanes y arbitrariedades perpetradas por el gobierno venezolano, con la complicidad del CNE, a diferencia de otras oportunidades, han sido registrados por la comunidad internacional. El secretario general de la OEA, Luis Almagro, le dirigió una correspondencia detallada a la presidenta del organismo comicial, Tibisay Lucena, en la cual detalla los numerosos delitos electorales cometidos por el régimen y exige elecciones libres y justas. Esa misiva nunca fue respondida por Lucena, pero sí por Maduro, quien llamó "basura" al Secretario General. A la carta de Almagro se suman las advertencias de fraude señaladas por distintas personalidades y organizaciones internacionales, al igual que las peticiones para que se realicen unas elecciones confiables.</p>
<p>&nbsp;</p>
<p>El Gobierno, presionado por esas demandas y controles externos y de la MUD, ha sido arrinconado y obligado a realizar unas elecciones a las que no quería concurrir, y que intentó evitar por distintas vías. Habrá que esperar el 6-D para saber cuál será su reacción frente a una derrota que parece inevitable. Habrá que ver cómo se comportan las Fuerzas Armadas. Esta es la gran incógnita. Yo apuesto porque defenderán la Constitución y la Democracia.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/499-recordando-la-lucha-peruana-contra-el-terrorismo">
			&laquo; Recordando la lucha peruana contra el terrorismo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/505-venezuela-el-fin-de-la-hegemonía-chavista">
			Venezuela: el fin de la hegemonía chavista &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo#startOfPageId503">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:68:"Las elecciones parlamentarias en Venezuela: ambiente previo - Relial";s:11:"description";s:155:"Las elecciones parlamentarias en Venezuela: ambiente previo REPORTE RELIAL Trino Márquez, director académico CEDICE Libertad &amp;nbsp; 1. Informacion...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:59:"Las elecciones parlamentarias en Venezuela: ambiente previo";s:6:"og:url";s:124:"http://relial.org/index.php/productos/archivo/actualidad/item/503-las-elecciones-parlamentarias-en-venezuela-ambiente-previo";s:8:"og:title";s:68:"Las elecciones parlamentarias en Venezuela: ambiente previo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/6ef598b10d1539793d4ace8d8b7b613f_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/6ef598b10d1539793d4ace8d8b7b613f_S.jpg";s:14:"og:description";s:159:"Las elecciones parlamentarias en Venezuela: ambiente previo REPORTE RELIAL Trino Márquez, director académico CEDICE Libertad &amp;amp;nbsp; 1. Informacion...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:59:"Las elecciones parlamentarias en Venezuela: ambiente previo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}