<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9160:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId460"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Red Liberal de América Latina, los primeros 10 años
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/d197c421d422f5cbf569ea13f09ef700_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/d197c421d422f5cbf569ea13f09ef700_XS.jpg" alt="Red Liberal de Am&eacute;rica Latina, los primeros 10 a&ntilde;os" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>La batalla permanente por la libertad en nuestros países tiene en la Red Liberal de América Latina, RELIAL, a uno de sus más decididas protagonistas, y esta entidad acaba de cumplir sus primeros diez años de trabajo y acción, celebrándolos con un gran Congreso en Panamá, organizado por la Fundación Libertad de ese espléndido país.</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Coronada esta primera década de esfuerzos, se recordó allí que, como todo aquello que es importante o significativo en la vida de las personas, las organizaciones o los países, la Red Liberal de América Latina empezó como un sueño, una visión que compartían, sin haber nunca coincidido antes para ello, diversas personalidades liberales en la región, nucleadas en institutos de investigación y partidos políticos entonces dispersos y sin vínculos, y que requerían de una estrategia de conjunto para hacer realidad un esfuerzo común.</p>
<p>&nbsp;</p>
<p>Así, la formación de la Red Liberal de América Latina coincidió con los propósitos de utilizar las herramientas de la globalización en pro del desarrollo de los países de la región y de difundir las ideas de la libertad y sus prácticas políticas en el continente. Bajo el determinante auspicio de la Fundación Friedrich Naumann para la Libertad, y con su soporte permanente, generoso y comprometido, el gran mérito de RELIAL fue lograr la conexión entre institutos y partidos liberales y generar un espacio que les permitiera trabajar juntos en un ambiente de cooperación mutua y armonía. Para lograrlo, los fundadores de RELIAL establecieron un mandato para que la Red se convirtiera en un foro regional reconocido e influyente para fortalecer el liberalismo y potenciar el impacto de sus políticas públicas, desarrollando una corriente de opinión afín, lo que se ha ido logrando con un continua institucionalización de la red, el desarrollo de sus diversos Congresos, la publicación de varios libros y centenares de artículos, así como oportunas declaraciones y pronunciamientos para condenar atropellos y amenazas a la libertad en la región, que desgraciadamente no han sido pocas.</p>
<p>&nbsp;</p>
<p>De este modo, la experiencia de RELIAL ha permitido a los liberales latinoamericanos redescubrir a Adam Smith y su Teoría de los sentimientos morales: sus avances y avatares han demostrado que la empatía smithiana, en tanto vínculo que amista y hermana a los hombres, es esencial para fortalecer la libertad y su legado fundamental: la civilización. Incrementa esa empatía la lucha por un ideal común, como es el caso de los miembros de esta Red, los cuales han combatido, en algunos casos muy duramente, contra tenaces adversarios de las libertades, como han sido, desde el poder, los socialistas del siglo XXI en Venezuela o la decrépita dictadura cubana.</p>
<p>&nbsp;</p>
<p>A su vez, la experiencia de RELIAL ha servido para llevar a la práctica la tesis del más reconocido de los integrantes de su Comité Honorífico, el Premio Nobel de Literatura 2010 Mario Vargas Llosa, de exponer y defender un liberalismo "ampliado", es decir, no restringido únicamente a su expresión económica, sino concebido de manera integral: en este liberalismo, además de la economía, tienen preponderante cabida la cultura, la defensa del Estado de derecho y por consiguiente de los derechos humanos, la promoción de los derechos de propiedad, la defensa del medio ambiente, la lucha contra la corrupción, el combate a la pobreza y la indispensable tarea política para hacer realidad el sueño de libertad. Este liberalismo ampliado, tomando los elementos antes descritos como indispensables para alcanzar el progreso, la modernidad y el desarrollo, ha sido un tema permanente en la Red, demandando para su análisis, ingentes esfuerzos teóricos, de revisión de buenas prácticas y estrategias para ser implementado.</p>
<p>&nbsp;</p>
<p>Para los meses que vienen, y como resultado de su último Congreso, RELIAL ha aprobado una Carta de Derechos Humanos, que establece su inmanente compromiso con los derechos fundamentales; priorizará la Alianza del Pacifico como un marco para el análisis de la agenda política y económica; mantendrá alerta con Venezuela; promoverá el rescate del modelo chileno como la experiencia exitosa en materia económica y social; y mantendrá como eje transversal la promoción de la democracia liberal. Así, la libertad, como en el poema de Octavio Paz, es "el sueño en el que somos nuestro sueño", y con RELIAL, el sueño por una América Latina libre continúa, con sus logros, resultados y desafíos, esperemos que por muchos años más.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Héctor Ñaupari*</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: IEAH</p>
<p>&nbsp;</p>
<p>*Ex Presidente de la Red Liberal de América Latina, Presidente del Instituto de Estudios de la Acción Humana, autor de Sentido liberal, La nueva senda de la libertad, Libertad para todos, entre otras publicaciones.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/457-bachelet-de-espaldas-al-chile-moderno">
			&laquo; Bachelet de espaldas al Chile moderno		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/464-instituciones-débiles-seguridad-pública-y-crimen-organizado-la-red-liberal-de-américa-latina-aprueba-declaración-sobre-derechos-humanos-en-américa-latina">
			Instituciones débiles, seguridad pública y crimen organizado: La Red Liberal de América Latina aprueba Declaración sobre Derechos Humanos en América Latina &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/460-red-liberal-de-américa-latina-los-primeros-10-años#startOfPageId460">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:62:"Red Liberal de América Latina, los primeros 10 años - Relial";s:11:"description";s:158:"En la opinión de Héctor Ñaupari &amp;nbsp; La batalla permanente por la libertad en nuestros países tiene en la Red Liberal de América Latina, RELIAL,...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:53:"Red Liberal de América Latina, los primeros 10 años";s:6:"og:url";s:119:"http://www.relial.org/index.php/productos/archivo/opinion/item/460-red-liberal-de-américa-latina-los-primeros-10-años";s:8:"og:title";s:62:"Red Liberal de América Latina, los primeros 10 años - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/d197c421d422f5cbf569ea13f09ef700_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/d197c421d422f5cbf569ea13f09ef700_S.jpg";s:14:"og:description";s:162:"En la opinión de Héctor Ñaupari &amp;amp;nbsp; La batalla permanente por la libertad en nuestros países tiene en la Red Liberal de América Latina, RELIAL,...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:53:"Red Liberal de América Latina, los primeros 10 años";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}