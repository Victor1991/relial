<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8320:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId514"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Perú: retos de gestión para el próximo gobierno
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/c501a702ef05e90d163a1eeeb1633357_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/c501a702ef05e90d163a1eeeb1633357_XS.jpg" alt="Per&uacute;: retos de gesti&oacute;n para el pr&oacute;ximo gobierno" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>"Si se gobierna con buenos gestores podríamos estar al inicio de una verdadera transformación".</p>
<p>&nbsp;</p>
<p>En la opinión de Daniel Córdova,</p>
<p>Presidente del<a href="http://www.invertir.org.pe/"> Instituto Invertir</a>, Perú</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Una segunda vuelta entre PPK y Keiko Fujimori tiene, sin duda, un aire de alivio para la economía. Ninguno de los dos candidatos cuestiona el "modelo", tan vapuleado por la izquierda; pero eficaz al punto de habernos puesto en un mundo con inflación moderada, menos pobreza y una nueva clase media emergente que vive mucho mejor que hace 25 años.</p>
<p>&nbsp;</p>
<p>No obstante, ambos también están de acuerdo en que la debilidad del "modelo" ha sido que el crecimiento no ha estado acompañado de reformas institucionales ni de un desarrollo adecuado de infraestructura, lo que a la larga ha generado que ese crecimiento se vea seriamente amenazado. ¿Quiénes son los responsables? En gran parte el Congreso de la República, que no ha aprobado ninguna reforma de importancia en los últimos cinco años y que además ha demostrado una gran debilidad; por ejemplo, al demorarse una eternidad para nombrar a los directores del BCR, y al no haber podido acordar la designación del Defensor del Pueblo.</p>
<p>&nbsp;</p>
<p>Pero el mayor responsable resulta el Poder Ejecutivo que, salvo honrosas excepciones, no diseñó las reformas estructurales que eran necesarias para mantener el crecimiento económico, mejorar el gasto público e incentivar la inversión privada. Existieron algunos pocos esfuerzos loables, aunque dispersos y claramente insuficientes; no obstante, el manejo de los conflictos sociales alrededor de proyectos mineros y energéticos, así como la generación de infraestructuras, fueron un total fracaso. Prueba de ello es que los dos proyectos emblemáticos que este gobierno debió sacar adelante, Conga y Tía María, se han visto frustrados.</p>
<p>&nbsp;</p>
<p>La inversión privada estuvo atenta y dispuesta, pero la la inexperiencia y desidia del Ejecutivo generó una inercia cuyas consecuencias deberemos enfrentar durante el próximo gobierno. En las zonas donde las industrias extractivas vieron frustradas sus inversiones no ha faltado únicamente "diálogo"; ha faltado la presencia del Estado, del Gobierno central que diseñe una estrategia de desarrollo y la ejecución de proyectos. Ello implicaba necesariamente una capacidad en la Presidencia del Consejo de Ministros para convocar a ministros, gobernadores y alcaldes alrededor de proyectos de desarrollo articulados.</p>
<p>&nbsp;</p>
<p>Lamentablemente, en los casos de Conga y Tía María no hubo una estrategia de envergadura que buscara desarrollar la agricultura o mejorar los servicios de vivienda, educación, salud, seguridad y transporte de las comunidades. Tampoco el sector privado tuvo en ese momento la visión de desarrollo proyectos, a través de su responsabilidad social corporativa, donando expedientes técnicos o utilizando mecanismos como OXI.</p>
<p>&nbsp;</p>
<p>Por otra lado, las fallas del Estado para impulsar Asociaciones Público Privadas (APP) han sido claras. ProInversión ha sido un ente pasivo que ha recibido iniciativas privadas, pero que no ha estado en capacidad de analizarlas. Y casos como los del aeropuerto Jorge Chávez, con el concesionario impedido de cumplir con la construcción de una pista (no recibe aún los terrenos), muestran cómo incluso con el servicio público en marcha, la debilidad del Estado puede echar a perder un caso de éxito a nivel internacional. Ello para no hablar de las tristes carreteras Panamericana y Central, las más importantes del país, que por tramos parecen rutas secundarias de cualquier país de ingreso medio alto (siendo generosos).</p>
<p>&nbsp;</p>
<p>Los retos del próximo gobierno no serán macroeconómicos; serán de gestión pública, de voluntad política y de fortalecimiento institucional. Si se gobierna sin politiquería y con buenos gestores podríamos estar al inicio de cinco años de una verdadera gran transformación.</p>
<p>&nbsp;</p>
<p><strong>Daniel Córdova</strong> es Presidente del <a href="http://invertir.pe/">Instituto Invertir</a>, Perú.&nbsp;</p>
<p>Fuente:&nbsp;<a href="http://elmontonero.pe/columnas/retos-de-gestion-para-el-proximo-gobierno">http://elmontonero.pe/columnas/retos-de-gestion-para-el-proximo-gobierno</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/512-el-líder-lilght-el-caudillo-malo-y-el-profeta-enardecido">
			&laquo; El líder lilght, el caudillo malo y el profeta enardecido		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/521-educar-además-del-estado-sí-es-posible">
			EDUCAR -además del Estado- SÍ ES POSIBLE &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno#startOfPageId514">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:59:"Perú: retos de gestión para el próximo gobierno - Relial";s:11:"description";s:156:"&quot;Si se gobierna con buenos gestores podríamos estar al inicio de una verdadera transformación&quot;. &amp;nbsp; En la opinión de Daniel Córdova,...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:50:"Perú: retos de gestión para el próximo gobierno";s:6:"og:url";s:112:"http://relial.org/index.php/productos/archivo/opinion/item/514-perú-retos-de-gestión-para-el-próximo-gobierno";s:8:"og:title";s:59:"Perú: retos de gestión para el próximo gobierno - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/c501a702ef05e90d163a1eeeb1633357_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/c501a702ef05e90d163a1eeeb1633357_S.jpg";s:14:"og:description";s:168:"&amp;quot;Si se gobierna con buenos gestores podríamos estar al inicio de una verdadera transformación&amp;quot;. &amp;amp;nbsp; En la opinión de Daniel Córdova,...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:50:"Perú: retos de gestión para el próximo gobierno";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}