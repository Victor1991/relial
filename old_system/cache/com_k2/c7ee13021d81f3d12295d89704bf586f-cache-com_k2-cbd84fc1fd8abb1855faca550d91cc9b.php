<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6789:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId451"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Columnas por la Libertad, muros a derribar
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7af0193b8351bd1eb04c09dfe5731fa3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7af0193b8351bd1eb04c09dfe5731fa3_XS.jpg" alt="Columnas por la Libertad, muros a derribar" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>El 9 de noviembre, Día Mundial de la Libertad, se conmemoran los 25</p>
<p>años de la caída del muro de Berlín. Frontera artificial levantada el 13 de</p>
<p>agosto de 1961 por el régimen comunista de la autodenominada República</p>
<p>Democrática Alemana (RDA) para evitar la huida de los berlineses del</p>
<p>este a la Alemania capitalista.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>La división de Europa tuvo como consecuencia la evolución de las sociedades</p>
<p>que estaban de uno u otro lado del muro de forma diferente. Aquellas</p>
<p>del occidente se beneficiaron de economías capitalistas, con crecimiento</p>
<p>económico y consolidación de sus democracias. Por contraparte,</p>
<p>los que se encontraban en la parte oriental, bajo regímenes comunistas</p>
<p>con economías centralizadas, atropello de libertades y la transgresión de</p>
<p>derechos fundamentales, experimentaron años de retraso.</p>
<p>&nbsp;</p>
<p>En sus 28 años de existencia, el Muro fue testigo de más de 5.000 intentos</p>
<p>de fuga, que costaron la vida a centenares de personas.</p>
<p>&nbsp;</p>
<p>Cuando se produjo la caída del muro de Berlín o "Muro de la Vergüenza"</p>
<p>en 1989, por las ansias de libertades políticas y sociales de los pueblos</p>
<p>bajo ese sistema totalitario y una economía decadente, fueron una lápida</p>
<p>para el utópico sueño marxista. El 9 de noviembre, los alemanes se concentraron</p>
<p>en la Puerta de Brandenburgo, y otros lugares de Berlín, para</p>
<p>derribar el muro y hacer desaparecer una cicatriz artificial creada por los</p>
<p>políticos soviéticos en el corazón de un pueblo.</p>
<p>&nbsp;</p>
<p>En las semanas y meses posteriores, la incontenible sed de libertad condujo</p>
<p>al derrumbe del imperio soviético y al fin de la Guerra Fría. Lamentablemente</p>
<p>la esperanza albergada del triunfo de los principios de</p>
<p>la libertad y los derechos personales con la caída del muro de Berlín</p>
<p>y la derrota del comunismo soviético contrasta con los muchos muros</p>
<p>todavía existentes.</p>
<p>&nbsp;</p>
<p>Este documento, que agrupa a influyentes académicos e intelectuales</p>
<p>que se han destacado en su defensa por las ideas de una sociedad libre,</p>
<p>nos invita a reflexionar sobre los muros que faltan por derribar en América</p>
<p>Latina. Se trata un llamado para renovar y fortalecer nuestro compromiso</p>
<p>con la promoción de la libertad, la democracia y la paz.</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  	  <!-- Item attachments -->
	  <div class="itemAttachmentsBlock">
		  <span>Descargar libro en formato PDF</span>
		  <ul class="itemAttachments">
		    		    <li>
			    <a title="Columnas_Libertad_2014_1.pdf" href="/index.php/productos/archivo/actualidad/item/download/115_9c79b33078d7861a3af5109a78568f39">Columnas_Libertad_2014_1.pdf</a>
			    			    <span>(281 K2_DOWNLOADS)</span>
			    		    </li>
		    		  </ul>
	  </div>
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/441-conferencia-vouchers-educativos-60-años-después">
			&laquo; Conferencia Vouchers Educativos: 60 años después		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/471-los-cinco-errores-de-obama-en-su-nueva-política-sobre-cuba">
			Los cinco errores de Obama en su nueva política sobre Cuba &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/451-columnas-por-la-libertad-muros-a-derribar#startOfPageId451">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:51:"Columnas por la Libertad, muros a derribar - Relial";s:11:"description";s:157:"El 9 de noviembre, Día Mundial de la Libertad, se conmemoran los 25 años de la caída del muro de Berlín. Frontera artificial levantada el 13 de agosto...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:42:"Columnas por la Libertad, muros a derribar";s:6:"og:url";s:107:"http://relial.org/index.php/productos/archivo/actualidad/item/451-columnas-por-la-libertad-muros-a-derribar";s:8:"og:title";s:51:"Columnas por la Libertad, muros a derribar - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/7af0193b8351bd1eb04c09dfe5731fa3_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/7af0193b8351bd1eb04c09dfe5731fa3_S.jpg";s:14:"og:description";s:157:"El 9 de noviembre, Día Mundial de la Libertad, se conmemoran los 25 años de la caída del muro de Berlín. Frontera artificial levantada el 13 de agosto...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:42:"Columnas por la Libertad, muros a derribar";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}