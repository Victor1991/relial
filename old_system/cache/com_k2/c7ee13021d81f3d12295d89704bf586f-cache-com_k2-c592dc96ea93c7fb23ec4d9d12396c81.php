<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9071:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId496"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Chile, de espaldas al éxito
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/b3a54ecc0915f9347c3f53fa31d161fe_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/b3a54ecc0915f9347c3f53fa31d161fe_XS.jpg" alt="Chile, de espaldas al &eacute;xito" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>¿Cuántas reformas se necesitan para destruir treinta años de progreso?</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>Su nombre es Verónica Michelle Bachelet Jeria, tiene 63 años, es pediatra e hija de un general de la fuerza aérea muerto en prisión a poco de acontecer el golpe de Estado contra Salvador Allende. Fue enviada a un centro de detención junto a su madre y después forzada al exilio. Pese a tan complicada historia, se ha caracterizado por su carisma, sencillez y marcada empatía. Una sonrisa siempre presente y un tono conciliador. Se trata, ni más ni menos que de la actual presidente de Chile, que retornó al poder tras cuatro años del gobierno opositor liderado por Sebastián Piñera. Una mujer que pese a mostrar un particular estilo de liderazgo, lejano a lo que nos tiene acostumbrados la región, ha demostrado estar dispuesta a patear el tablero, a rebelarse contra el status quo de manera radical, transformando en hechos aquellas promesas de cambio que, gracias al apoyo de una minoría, la llevaron al triunfo electoral.</p>
<p>&nbsp;</p>
<p>Es así como Chile se ha sumido en un paradigma de país muy diferente, más cerca del viejo modelo estatista, que está haciendo temblar las bases y fundamentos del éxito que supieron coronarlo como ejemplo de gestión y desarrollo en América Latina y en el mundo. Lejos de intentar mantener su condición de excepción en la región, se suma así a la peligrosa "moda" del socialismo del siglo XXI. Argentina, Venezuela, Bolivia, Ecuador, definitivamente han logrado marcar tendencia.</p>
<p>&nbsp;</p>
<p>Durante décadas, resultó obvio que la libertad, el funcionamiento de las instituciones, la apertura al mundo, la competencia, el mercado, definían el camino a seguir. La evidencia respaldaba esta creencia. ¿Repasamos? Chile logró crecer y desarrollarse extraordinariamente en los últimos treinta años. Más que quintuplicó su ingreso real per cápita, la pobreza que superaba el 45% cayó a menos del 8% y se logró minimizar la indigencia, ubicándose por debajo del 3%. Fue el país que más movilidad social ascendente experimentó en la región. La esperanza de vida creció en más de 10 años, la escolaridad de los adultos en más de 3 y hoy son diez veces más los jóvenes que acceden a la educación superior.</p>
<p>&nbsp;</p>
<p>De este modo, mejoró notoriamente su nivel promedio de vida, logrando ubicarse en el puesto 41 entre 187 países del mundo en el ranking de Desarrollo Humano, siendo el primero entre las naciones de América Latina y el Caribe.</p>
<p>&nbsp;</p>
<p>¿Les suena a Revolución? sí, y de las buenas. Bolivariana, seguramente que no.</p>
<p>&nbsp;</p>
<p>Lo conseguido ha dado un tenor diferente a las aspiraciones y problemas de los chilenos. Con cuestiones básicas resueltas, de esas que aún se discuten en países tercermundistas como los nuestros, puede darse el lujo de plantear demandas a otra escala. Sin embargo, lejos de revalorizar y mejorar el modelo vigente, habiendo crecido la torta, la atención se puso en la repartija de los logros obtenidos. Los reclamos de ciertos sectores de la ciudadanía comenzaron a resonar y Bachelet rápidamente logró capitalizarlos. Fue así como la lucha contra la desigualdad, se transformó en su bastión.</p>
<p>&nbsp;</p>
<p>En campaña, la mandataria presentó 50 propuestas de las más diversas, para los 100 días iniciales de gobierno. Más allá de la cantidad, lo preocupante es el contenido de algunas y la celeridad con la que pretenden ser ejecutadas. La reforma educacional y la impositiva, entre las más controvertidas en ésta "búsqueda de equidad" y de "inclusión social", ya se encuentran en marcha y refuerzan la idea de un Estado más presente, la reivindicación de lo público y la lucha por una sociedad "más justa", generando incentivos completamente ajenos a los que condujeron al éxito.</p>
<p>&nbsp;</p>
<p>Esto, ha generado una sensación de deja vu que retrotrae a los desafortunados años del allendismo, con las consecuencias negativas que acarrea no sólo en el ámbito social sino económico. Nada oportuno para el delicado momento que afronta el país, que lo que menos necesitaba era sumar incertidumbre al mercado.</p>
<p>&nbsp;</p>
<p>Mirar más allá de la cordillera podría ser un gran indicio de hacia dónde se dirigen los chilenos, sin un freno a tiempo. Con un vecino como Argentina debería bastar.</p>
<p>&nbsp;</p>
<p>Bachelet podrá impulsar una, diez o mil reformas. Si el descontento de la ciudadanía que hoy se está reflejando en la caída de popularidad de la mandataria, no se traduce en límites concretos, el avance será inevitable. Y este, es el gran problema, que podría encontrarnos a todos como testigos del comienzo del fin de la era que fuera bautizada por Milton Friedman como el "milagro chileno".-</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor:&nbsp;María José Romano Boscarino</p>
<p>Fuente: <a href="http://www.federalismoylibertad.org/chile-de-espaldas-al-exito/">Federalismo y Libertad</a></p>
<p>Foto: Federalismo y Libertad Buenos Aires, colaborador de Libertad y Progreso</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/495-indignación-ciudadana-en-guatemala-se-refleja-en-el-aumento-de-protestas">
			&laquo; Indignación ciudadana en Guatemala se refleja en el aumento de protestas		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/498-¿invertir-o-no-invertir-en-cuba?-esa-es-la-pregunta-cuba-en-su-nueva-etapa-análisis-foda">
			¿Invertir o no invertir en Cuba? Esa es la pregunta. Cuba en su nueva etapa: Análisis FODA &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-éxito#startOfPageId496">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:37:"Chile, de espaldas al éxito - Relial";s:11:"description";s:158:"¿Cuántas reformas se necesitan para destruir treinta años de progreso? &amp;nbsp; Su nombre es Verónica Michelle Bachelet Jeria, tiene 63 años, es ped...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:28:"Chile, de espaldas al éxito";s:6:"og:url";s:150:"http://www.relial.org/index.php/productos/archivo/actualidad/item/496-chile-de-espaldas-al-%C3%A9xito/index.php/2013-03-28-23-35-13/democracia-liberal";s:8:"og:title";s:37:"Chile, de espaldas al éxito - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/b3a54ecc0915f9347c3f53fa31d161fe_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/b3a54ecc0915f9347c3f53fa31d161fe_S.jpg";s:14:"og:description";s:162:"¿Cuántas reformas se necesitan para destruir treinta años de progreso? &amp;amp;nbsp; Su nombre es Verónica Michelle Bachelet Jeria, tiene 63 años, es ped...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:28:"Chile, de espaldas al éxito";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}