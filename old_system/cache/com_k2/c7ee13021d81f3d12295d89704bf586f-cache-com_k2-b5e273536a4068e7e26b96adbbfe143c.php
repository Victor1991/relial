<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8473:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId245"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	La grandeza de Mandela
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8012f255a337782bffaadea968723f36_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8012f255a337782bffaadea968723f36_XS.jpg" alt="La grandeza de Mandela" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>Nelson Mandela, una de las figuras más nobles y admirables del siglo XX, fue amigo de Fidel Castro. ¿Y qué? David Rockefeller también presumía de los mismos lazos. Carlos Andrés Pérez, hasta poco antes de exiliarse, pensaba que Fidel era amigo suyo. Al fin y al cabo, cuando Hugo Chávez, en 1992, trató de derrocarlo a tiros, y dejó decenas de muertos en las calles de Caracas, Fidel le mandó un mensaje de solidaridad a CAP y condenó la acción fascista del teniente coronel.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Se ha dicho muchas veces: la política hace extraños compañeros de cama. (Groucho Marx aseguraba que no era la política, sino el matrimonio lo que hacía extraños compañeros de cama, pero ésa es otra historia). Mandela tenía buenas razones para mostrarse agradecido al dictador cubano. Fidel había sido intensamente solidario &nbsp;con quienes se oponían al apartheid, aunque liquidar la segregación racial era más una coartada política lateral que el objetivo básico de La Habana.</p>
<p>&nbsp;</p>
<p>Lo esencial, dentro de la lógica de la Guerra Fría, era conquistar territorios para mayor gloria de Moscú y del mundillo dominado por los comunistas. Por eso Castro, de un modo oportunista, en su momento cambió de alianzas y mandó sus tropas a consolidar el poder en Etiopía y liquidar a los somalíes en el desierto de Ogaden. Su lucha no era contra la supremacía blanca, sino por la supremacía roja.</p>
<p>&nbsp;</p>
<p>No obstante, para Mandela, o para cualquiera dentro de su piel curtida a palos y calabozos, que un país remoto como Cuba, dirigido por un líder blanco, enviara a pelear a cientos de miles de soldados durante catorce años consecutivos contra los intereses de Sudáfrica, y a veces contra el ejército de ese país, era algo que debía &nbsp;agradecerse. Por las razones que fueran –y las de Cuba tenían que ver con la enfermiza personalidad de Fidel Castro, un tipo que se creía Napoleón y parece que lo era--, la pequeña Isla caribeña se convirtió en una fuente constante de solidaridad y ayuda.</p>
<p>&nbsp;</p>
<p>A Mandela no hay que juzgarlo por sus amigos, sino por su inmensa condición de estadista. Fue prudente y flexible, como sólo lo son las grandes figuras de la historia. Llegó a la cárcel como un marxista dispuesto a recurrir al terrorismo para lograr sus propósitos,&nbsp; y, progresivamente, desechó los disparates ideológicos y renunció a las actitudes violentas. Entró en la prisión como un Lenin justamente colérico y 27 años más tarde salió como un Gandhi sensato y apacible.</p>
<p>&nbsp;</p>
<p>Podía estar lleno de rencores –era lo natural--, pero se los tragó y supo darle la mano al adversario y sustituir lo que pudo haber sido una infinita cadena de venganzas por un simple mecanismo de arrepentimiento público y solicitud de perdón. Junto a Desmond Tutu propició la creación de la Comisión de la Verdad y la Reconciliación. El lema era muy elocuente: "Sin perdón no hay futuro, pero sin confesión no existirá el perdón".</p>
<p>&nbsp;</p>
<p>Llegó a ser amigo del último gobernante blanco, Frederik William de Klerk, con quien compartió el Premio Nobel en 1993, y fue capaz de entender que la regla de la mayoría no podía utilizarse para humillar y barrer del país al 20% de blancos que durante siglos los habían maltratado. Los blancos eran una tribu más de las varias que componían el país. Sus antepasados procedían de Inglaterra u Holanda, pero eran y se sentían sudafricanos. Había que contar con ellos, que, además, tenían los capitales económico y el humano.</p>
<p>&nbsp;</p>
<p>Mandela no intentó la aventura colectivista ni despojó de sus bienes a los blancos para favorecer a los negros, lo que hubiera sido una medida inmensamente popular, aunque hubiese destrozado la economía. Es verdad que comenzó a gobernar en 1994, varios años después del derribo del Muro de Berlín, y de la desaparición de la URSS y de casi todo el bloque comunista europeo, pero tuvo la prudencia de entenderlo, mantenerse dentro de la ley y respetar la propiedad privada y el mercado.</p>
<p>&nbsp;</p>
<p>No quiso eternizarse en el poder. Lo hubiera podido hacer. Lo idolatraban. Gestionó el país democráticamente durante cinco años y le dio paso a otros gobernantes. Sabía que en las naciones serias las instituciones tienen más peso que las personas que administran el Estado.</p>
<p>&nbsp;</p>
<p>Fue, por todo eso, uno de los grandes políticos del siglo XX.&nbsp; Sin duda, el mayor de África.</p>
<p>&nbsp;</p>
<p>fuente: texto proporcionado amablemente por Carlos Alberto Montaner</p>
<p>foto: Blog de Montaner</p>
<p>autor: Carlos Alberto Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/242-honduras-o-el-fin-del-chavismo">
			&laquo; Honduras o el fin del chavismo		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/248-hay-una-camino-pero-no-es-éste">
			Hay una camino, pero no es éste &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela#startOfPageId245">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:31:"La grandeza de Mandela - Relial";s:11:"description";s:155:"En la opinión de Carlos Alberto Montaner &amp;nbsp; Nelson Mandela, una de las figuras más nobles y admirables del siglo XX, fue amigo de Fidel Castro...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:22:"La grandeza de Mandela";s:6:"og:url";s:89:"http://www.relial.org/index.php/productos/archivo/opinion/item/245-la-grandeza-de-mandela";s:8:"og:title";s:31:"La grandeza de Mandela - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/8012f255a337782bffaadea968723f36_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/8012f255a337782bffaadea968723f36_S.jpg";s:14:"og:description";s:159:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; Nelson Mandela, una de las figuras más nobles y admirables del siglo XX, fue amigo de Fidel Castro...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:22:"La grandeza de Mandela";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}