<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6068:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId264"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”,
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/47359a90eed3ee35f2dab5a3c718abb3_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/47359a90eed3ee35f2dab5a3c718abb3_XS.jpg" alt="RELIAL: felicitaciones a los think tanks reconocidos en el &Iacute;ndice &ldquo;2013 Global Go To Think Tank&rdquo;," style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>La Red Liberal de América Latina (RELIAL) felicita a los think tanks y centros de investigación de la región que fueron reconocidos y destacados en altas y meritorias posiciones en el Índice "2013 Global Go To Think Tank", realizado por el departamento de Relaciones Internacionales, Centro de Estudios y Sociedad Civil de la Universidad de Pennsylvania.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>De las organizaciones miembro de RELIAL, en el Top Think Tanks de mayor influencia en América Latina, se encuentran el Centro de Divulgación del Conocimiento Económico (CEDICE Libertad/Venezuela), Libertad y Desarrollo (LyD/Chile), Instituto Ecuatoriano de Economía Política (IEEP/Ecuador), Fundación Libertad (Argentina), Instituto de Ciencias Políticas (ICP/Colombia), el Instituto de Estudios Empresariais (IEE/Brasil), el Centro de Investigaciones Económicas Nacionales (CIEN/Guatemala) y Fundación Para el Progreso (FPP/Chile).&nbsp;</p>
<p>&nbsp;</p>
<p>También la Fundación Libertad y Progreso (LyP/Argentina)&nbsp; fue reconocida en la lista de los mejores nuevos think tanks, así también el Instituto Liberdade (Brasil) por su distinción en uso de redes sociales. Varios de nuestros institutos fueron destacados en diversas categorías, lo que da cuenta de la gran labor que día a día llevan a cabo.</p>
<p>&nbsp;</p>
<p>RELIAL también fue distinguida por entre las mejores redes que colaboran al trabajo de los think tanks, así como la Fundación Friedrich Naumann para la Libertad destacada entre las mejores organizaciones&nbsp; de cooperación y relacionamiento entre partidos políticos y think tanks.</p>
<p>&nbsp;</p>
<p>Felicitaciones y que sigan cosechando éxitos</p>
<p>&nbsp;</p>
<p>RELIAL, 28 de enero, México D.F</p>
<p>&nbsp;</p>
<p>Para descargar el índice dar clic <a href="index.php/biblioteca/item/261-2013-global-go-to-think-thank">aquí</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/262-cedice-libertad-ocupa-el-puesto-no-1-entre-los-centros-de-estudios-de-libre-mercado-más-influyentes-de-américa-latina">
			&laquo; CEDICE Libertad ocupa el puesto No. 1 entre los centros de estudios de libre mercado más influyentes de América Latina		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/267-crisis-en-américa-latina-por-factores-institucionales">
			Crisis en América Latina por factores institucionales &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/264-relial-felicitaciones-a-los-think-tanks-reconocidos-en-el-índice-“2013-global-go-to-think-tank”#startOfPageId264">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:111:"RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”, - Relial";s:11:"description";s:156:"La Red Liberal de América Latina (RELIAL) felicita a los think tanks y centros de investigación de la región que fueron reconocidos y destacados en al...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:102:"RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”,";s:6:"og:url";s:170:"http://www.relial.org/index.php/productos/archivo/actualidad/item/264-relial-felicitaciones-a-los-think-tanks-reconocidos-en-el-índice-“2013-global-go-to-think-tank”";s:8:"og:title";s:111:"RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”, - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/47359a90eed3ee35f2dab5a3c718abb3_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/47359a90eed3ee35f2dab5a3c718abb3_S.jpg";s:14:"og:description";s:156:"La Red Liberal de América Latina (RELIAL) felicita a los think tanks y centros de investigación de la región que fueron reconocidos y destacados en al...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:102:"RELIAL: felicitaciones a los think tanks reconocidos en el Índice “2013 Global Go To Think Tank”,";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}