<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6605:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId277"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Venezuela: Se cae de Maduro que el pueblo reclama libertad
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/f8a458e18503c86603dc02339a944feb_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/f8a458e18503c86603dc02339a944feb_XS.jpg" alt="Venezuela: Se cae de Maduro que el pueblo reclama libertad" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Eneas Biglione</p>
<p>&nbsp;</p>
<p>El pasado 12 de Febrero la sociedad civil venezolana, haciendo gala de una importante madurez cívica, salió decidida a las calles de su país a demostrar que las cosas ya no pueden continuar así, que las políticas intervencionistas y abusivas impuestas por el actual régimen – electo en condiciones particularmente cuestionables y teledirigido desde La Habana – han hecho que el día a día se torne insostenible.	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	El pueblo venezolano, auto-convocado desde las redes sociales, muy pese al fuerte bloqueo de páginas web y las medidas extremas de censura de prensa, se hartó de la impunidad, de la corrupción, de la represión, de la inseguridad rampante, de la violación flagrante y cotidiana de sus derechos humanos, de la destrucción de la divisa nacional, del saqueo a las empresas públicas y del desabastecimiento, entre muchos otros abusos y trabas al desarrollo y la convivencia pacífica.</p>
<p>&nbsp;</p>
<p>La fachada democrática del régimen no tardó en terminar de caerse, llamando de inmediato a la brutal represión generalizada por medio de la violencia a través de la Guardia Nacional Bolivariana (GNB), el SEBIN y los grupos armados no oficiales que, desde su creación, se dedican a derramar sangre y cometer delitos en las calles con total impunidad. El resultado fueron cientos de encarcelados, tres muertos y un número importante de heridos pertenecientes al valiente movimiento estudiantil en particular y al pueblo venezolano en general. La tolerancia, el diálogo y las libertades individuales fueron pisoteadas una vez más por un régimen que parece dispuesto a aferrarse con uñas y dientes al poder y al costo que sea.</p>
<p>&nbsp;</p>
<p>La importancia del tema no es menor. Desde que Hugo Chávez fue electo en 1998, el régimen venezolano ha venido ejerciendo una perversa influencia sobre sus países satélites del socialismo del siglo XXI en el continente americano y ha venido estableciendo alianzas con países de otros continentes cuyo accionar amenaza la integridad futura del mundo occidental. En resumidas cuentas, desde HACER manifestamos total solidaridad con el pueblo de Venezuela, esperando que a la mayor brevedad posible logre reencausar sus esfuerzos hacia un modelo de país verdaderamente efectivo, libre, democrático y sostenible en el tiempo. Del desafío que los venezolanos enfrentan hoy, la lección que rescatamos para toda América Latina está muy clara: un pueblo habla, grita y reforma aquello que está mal. En el proceso, sus cuerpos podrán ser heridos o encarcelados, pero sus almas... jamás podrán serlo.</p>
<p>&nbsp;</p>
<p>Autor: Eneas A. Biglione es Director Ejecutivo de la Fundación HACER:</p>
<p>Fuente: <a href="http://www.hacer.org/">www.hacer.org</a></p>
<p>Foto: HACER</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/76-la-pasión-por-la-igualdad">
			&laquo; La pasión por la igualdad		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/77-el-ciudadano-perfecto">
			El ciudadano perfecto &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/277-venezuela-se-cae-de-maduro-que-el-pueblo-reclama-libertad#startOfPageId277">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:67:"Venezuela: Se cae de Maduro que el pueblo reclama libertad - Relial";s:11:"description";s:156:"En la opinión de Eneas Biglione &amp;nbsp; El pasado 12 de Febrero la sociedad civil venezolana, haciendo gala de una importante madurez cívica, salió...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:58:"Venezuela: Se cae de Maduro que el pueblo reclama libertad";s:6:"og:url";s:124:"http://www.relial.org/index.php/productos/archivo/opinion/item/277-venezuela-se-cae-de-maduro-que-el-pueblo-reclama-libertad";s:8:"og:title";s:67:"Venezuela: Se cae de Maduro que el pueblo reclama libertad - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/f8a458e18503c86603dc02339a944feb_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/f8a458e18503c86603dc02339a944feb_S.jpg";s:14:"og:description";s:160:"En la opinión de Eneas Biglione &amp;amp;nbsp; El pasado 12 de Febrero la sociedad civil venezolana, haciendo gala de una importante madurez cívica, salió...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:58:"Venezuela: Se cae de Maduro que el pueblo reclama libertad";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}