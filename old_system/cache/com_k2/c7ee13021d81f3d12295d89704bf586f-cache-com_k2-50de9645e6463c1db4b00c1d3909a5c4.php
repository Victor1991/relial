<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10397:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId251"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Memoria del saqueo
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8ee107fb8e11fa27c5eb0c84c03d7dff_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8ee107fb8e11fa27c5eb0c84c03d7dff_XS.jpg" alt="Memoria del saqueo" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p style="text-align: justify;" align="center"><span style="line-height: 115%;">En la opinión de José Guillermo Godoy,&nbsp;Las jornadas del 9, 10 y 11 de diciembre en Tucumán (Argentina).&nbsp;</span></p>
<p style="text-align: justify;" align="center"><span style="text-align: left;">A comienzos del siglo XX, Tucumán tenía los índices de crecimiento económico más alto de Argentina, que por entonces ostentaba los niveles de desarrollo más altos del mundo, en claro contraste con sus pares en el continente. Pero, en el devenir del siglo XX el signo de la decadencia dominó el ritmo de la escena. Argentina se latinoamericanizó y Tucumán se argentinizó. La irrupción del kirchnerismo marca una primera década del siglo XXI que mirada a la distancia nos revela una atmosfera estancada y muerta. En la actualidad, en el caótico paisaje político y social que nos desvela, parecen ganar terreno la inconformidad y la desesperanza ante un destino social que se percibe como inevitable.</span></p>
<p>&nbsp;</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>No es difícil inferir que símbolos de esta decadencia -que es sobre todo moral- fueron las olas de saqueos acaecidos en los últimos días. Durante 48 horas los habitantes tucumanos sufrimos un hecho inédito en la historia local: que el contrato básico de la convivencia social, aquel que garantiza la protección de la vida y de los bienes de los ciudadanos, quedó suspendido.</p>
<p>&nbsp;</p>
<p>Los sucesos ocurridos en Córdoba, alimentaron un clima de temor e incertidumbre a nivel nacional que irrumpió con dureza en Tucumán el lunes 9 de diciembre por la tarde, favorecido por una huelga de policías provinciales. Casi por sorpresa comenzó a respirarse una atmosfera tensa. Los negocios bajaban sus ventanas, los empleados tapaban las vidrieras con cartones para evitar ser convertidos fácilmente en carnadas, los autos eran retirados de las concesionarias, gendarmería recomendaba a los camiones no circular por las rutas y el transporte público dejaba de funcionar. Se escuchaban rumores de saqueos por todos lados. La paranoia ocupo la ciudad. Ni los autos querían detenerse en los semáforos por temor a ser atacados.</p>
<p>&nbsp;</p>
<p>Al finalizar el día lunes, la sensación de saqueo dejó de ser una simple sensación: se confirmaba un robo masivo a un negocio de lácteos en la capital tucumana. Este hecho fue el disparador de una ola de saqueos en más de 200 locales de casi toda la provincia. Ante la falta de ayuda policial, los empleados de comercio, con el apoyo de vecinos, crearon un sistema de defensa que incluía la creación de vallas en las calles para contener a los saqueadores, y la dotación de todo lo necesario para la defensa: pistolas, escopetas, palos y cuchillos. A la vez articularon un sistema de comunicación y ayuda mutua.</p>
<p>&nbsp;</p>
<p>El martes a la mañana la ciudad amaneció en vilo. Banco, bares, supermercados, comercios y transporte público sin funcionar. Podía sentirse la sensación de guerra ante un enemigo interno y anónimo que solo lograba ser identificado por el ruido estremecedor de motos organizadas en bandas que circulaban a toda velocidad explorando el terreno y buscando una nueva carnada. Uno de los comerciantes saqueados reconoció a vecinos suyos participando del botín. La situación se asemejaba a un estado hobbesiano. El desconcierto y la desconfianza fueron en aumento. Las noticias sobre muertos no tardaron en aparecer.</p>
<p>&nbsp;</p>
<p>Por la tarde del martes, y de manera casi mágica, el silencio ensordecedor que invadía la ciudad desde horas tempranas fue interrumpido por el ruido de cacerolas anónimas. Las redes sociales replicaron este fenómeno de manera simultánea. Los vecinos, sin abandonar sus casas o estando muy cerca de ellas por temor a ser saqueados, se concentraron en las esquinas, y desde los balcones no pararon de sonar las cacerolas.</p>
<p>&nbsp;</p>
<p>Participé de una concentración de empleados de una feria junto a vecinos a pocas cuadras de mí casa. La mayoría de los participantes podía ser calificada por sus ingresos como de clase medio baja. Uno de los vecinos exclamó "en dos días vamos a ver a todos estos saqueadores haciendo cola en el banco para cobrar el subsidio". Otra reclamaba exaltada "cuando estemos reconstruyendo los locales nos van a visitar AFIP, Rentas y DIPSA" (en referencia a los organismo recaudadores de impuestos), y remató "salen unos saqueadores y entran otros".</p>
<p>&nbsp;</p>
<p>Finalmente a las 20 horas del martes la policía llegó a un acuerdo salarial con el gobierno provincial, y retomó sus funciones. El temor ciudadano bajo de nivel, pero no por ello el clima de indignación. Un grupo de vecinos y comerciantes del centro marcharon hacia plaza Independencia para expresar, frente al palacio de gobierno, su repudio a una administración que los había dejado solos. La respuesta fue una brutal represión policial.</p>
<p>&nbsp;</p>
<p>Algo muy viscoso y oscuro recorrió el cuerpo de la sociedad tucumana: el titánico chantaje corporativo de quienes conocían el riesgo que iba a desatar su "protesta" y sin embargo se retiraron de sus funciones para dejar liberados los barrios que inmediatamente se convirtieron en tierra de saqueos, ahora, en complicidad con la cúpula gobernante, diseñaban "el escenografía del miedo", para evitar toda respuesta social. Mientras tanto, las pantallas televisivas se llenaban de imágenes de los festejos que, en Capital Federal, encabezada la presidente Fernández de Kirchner con motivo de los 30 años de la vuelta de la democracia en Argentina, en dramático contraste con la trágica situación que miles de ciudadanos estaban padeciendo en nuestra provincia.</p>
<p>&nbsp;</p>
<p>El miércoles la ciudad amaneció con las actividades restablecidas. En las calles podía notarse las huellas de lo que había sido un verdadero teatro de combate: vidrios rotos, barricadas, vallas, palos y piedras por doquier. Emergieron las protestas y los escraches a representantes de las fuerzas de seguridad: un local comercial exhibía un cartel que decía "aquí no atendemos a policías". Las protestas se multiplicaron y derivaron en la noche en una emocionante concentración pacífica de más de 15 mil tucumanos en la plaza independencia. Los mismo que habían sido víctimas de noches de vigilia y miedo, ahora eran los protagonistas de una autentica noche de esperanza.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: José Guillermo Godoy</p>
<p>Foto: Fundación Federalismo y Libetad</p>
<p>Fuente: Fundación Federalismo y Libertad</p>
<p><a href="http://www.federalismoylibertad.org">www.federalismoylibertad.org</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/250-argentina-saqueos-en-¿un-país-con-buena-gente?">
			&laquo; Argentina: Saqueos en ¿un país con buena gente?		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/263-los-saqueadores">
			Los saqueadores &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo#startOfPageId251">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:27:"Memoria del saqueo - Relial";s:11:"description";s:156:"En la opinión de José Guillermo Godoy,&amp;nbsp;Las jornadas del 9, 10 y 11 de diciembre en Tucumán (Argentina).&amp;nbsp; A comienzos del siglo XX, T...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:18:"Memoria del saqueo";s:6:"og:url";s:85:"http://www.relial.org/index.php/productos/archivo/opinion/item/251-memoria-del-saqueo";s:8:"og:title";s:27:"Memoria del saqueo - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/8ee107fb8e11fa27c5eb0c84c03d7dff_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/8ee107fb8e11fa27c5eb0c84c03d7dff_S.jpg";s:14:"og:description";s:164:"En la opinión de José Guillermo Godoy,&amp;amp;nbsp;Las jornadas del 9, 10 y 11 de diciembre en Tucumán (Argentina).&amp;amp;nbsp; A comienzos del siglo XX, T...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:18:"Memoria del saqueo";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}