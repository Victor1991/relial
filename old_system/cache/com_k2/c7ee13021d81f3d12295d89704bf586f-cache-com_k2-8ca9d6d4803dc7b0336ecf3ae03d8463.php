<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8724:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId75"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Prestigio internacional de la oposición
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p><img style="margin: 2px; border: 2px solid #e1dbdb; float: left;" alt="cedice" src="images/content/logos/cedice.jpg" width="100" height="100" /><br style="clear: none;" />El éxito de la gira latinoamericana de Leopoldo López, William Dávila, María Corina Machado y otros diputados, revela el enorme prestigio internacional de nuestra oposición.</p>
<p><span style="color: #000000;">Por Trino Márquez.</span></p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>En contraste, Nicolás Maduro y el entorno que lo rodea creen que viven en los años 50 y 60 del siglo XX, en plena Guerra Fría, cuando los gobiernos gorilas de América Latina y cualquier otra región del planeta, podían actuar libre e impunemente contra toda oposición interna, porque una espesa capa de impunidad los protegía. Esos gobiernos formaban parte de la zona de influencia o patio trasero de alguna de las dos superpotencias que se repartían el mundo, que les blindaban frente a cualquier ataque de las fuerzas enemigas internas y, por supuesto, externas. Los gobiernos títeres eran peones de un ajedrez cuyas piezas se movían a control remoto desde Estados Unidos o Rusia, según fuese el territorio.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Maduro habita todavía en ese mundo. Esta creencia absurda, por la miopía que refleja, está afectando gravemente su imagen internacional. Muestra un severo déficit en el terreno diplomático. Parece que nunca hubiese caminado por los pasillos de la Casa Amarilla, ni visitado ninguna Cancillería de país extranjero alguno. Su reacción frente a los buenos oficios protocolares de distintos países y organismos multilaterales, ante la crisis política e institucional que atraviesa el país, ha sido tan destemplada y torpe, que su figura de hombre amable labrada durante la época de Ministro de Relaciones Exteriores, se achicharró. Tampoco el canciller que tiene a su lado, lo ayuda en nada. Elías Jaua vive obsesionado alimentando su reconcomio con Capriles y sus deseos de vengar la derrota humillante que "El Flaco" le infringió en Miranda. La ofuscación y falta de habilidades, le impiden analizar el escenario internacional con la frialdad necesaria y recomendar a su jefe que guarde la compostura debida.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>En pocos días Maduro peleó con Barack Obama, José Miguel Insulza, el canciller peruano y el español, Álvaro Uribe. El motivo de su ira reside en que estas personalidades han llamado a la paz y al entendimiento, al respeto de las instituciones republicanas y de la voluntad popular expresada en las urnas electorales el 14 de abril. Todo dentro de los cánones democráticos y de las normas de convivencia indispensables en un continente que durante décadas ha sufrido golpes de Estado, violencia y conflictos desgarradores.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Esta actitud desaforada de Maduro ha coincidido con la proyección mundial de los videos en los cuales Diosdado Cabello, primero les prohíbe hablar a los parlamentarios opositores en la Asamblea Nacional, y luego sus matones golpean a María Corina, a Julio Borges y a otros parlamentarios.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>La intemperancia antidemocrática del candidato que triunfó de forma sospechosa en los comicios del 14-A, ha sido respondida con una firmeza inusual por los factores democráticos de América Latina y Europa. La oposición venezolana, por primera vez en muchos años, recibe la solidaridad internacional que tanto necesitaba.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Esa ayuda se afinca en varios factores. La oposición venezolana constituye un ejemplo de seriedad y unidad en todo el mundo. Luego de los numerosos tropiezos, existe un liderazgo consolidado y legitimado por una votación que superó los 7.500.000 sufragios, que, según numerosos indicios, conquistó la victoria en la cita de abril. Cuenta con un líder indiscutible, Henrique Capriles, que se mantiene aferrado -al igual que los demás dirigentes de la MUD- a la vía electoral, democrática, pacífica y constitucional, sin que los abusos y provocaciones del Gobierno los hayan alejado de ese camino.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>La consistencia y seriedad de la oposición, convertida en interlocutora fundamental de los gobiernos y fuerzas democráticas del exterior, le han otorgado un sólido prestigio, que contrasta con el descrédito de un Gobierno inseguro, lerdo y autoritario, colocado a la espalda de las tendencias democráticas modernas. La diplomacia petrolera no le está resultando suficiente a Maduro para comprar la conciencia de los demócratas del continente. Con el crudo negro ha podido acallar y neutralizar algunos gobiernos limosneros, pero no a sus congresos, expresión de la soberanía popular.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>La presión internacional y las enormes torpezas de Maduro permiten ser optimistas. La democracia venezolana no está sola, como en el pasado. Hay muchos amigos del exterior que quieren verla renacer.</p>
<p>Autor: <strong>Trino Márquez,</strong> Sociólogo y Doctor en ciencias sociales de la Universidad Central de Venezuela donde también curso maestría en filosofía. Ha ejercido como profesor titular y docente del doctorado de ciencias sociales de la Universidad Central de Venezuela. Ha escrito varios libros y publicado en diversas revistas. En la actualidad es Director Académico <a href="http://cedice.org.ve/">CEDICE Libertad.</a></p>
<p>&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/56-último-combate-contra-la-prensa-libre">
			&laquo; Último combate contra la prensa libre		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/76-la-pasión-por-la-igualdad">
			La pasión por la igualdad &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/75-prestigio-internacional-de-la-oposición#startOfPageId75">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:49:"Prestigio internacional de la oposición - Relial";s:11:"description";s:157:"El éxito de la gira latinoamericana de Leopoldo López, William Dávila, María Corina Machado y otros diputados, revela el enorme prestigio internaciona...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:7:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:40:"Prestigio internacional de la oposición";s:6:"og:url";s:106:"http://www.relial.org/index.php/productos/archivo/opinion/item/75-prestigio-internacional-de-la-oposición";s:8:"og:title";s:49:"Prestigio internacional de la oposición - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:157:"El éxito de la gira latinoamericana de Leopoldo López, William Dávila, María Corina Machado y otros diputados, revela el enorme prestigio internaciona...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:40:"Prestigio internacional de la oposición";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}