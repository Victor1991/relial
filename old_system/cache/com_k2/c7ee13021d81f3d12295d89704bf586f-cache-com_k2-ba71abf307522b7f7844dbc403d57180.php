<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8012:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId310"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	México insurgente
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/9962afd4d7801adbe96e49a2f2eab69f_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/9962afd4d7801adbe96e49a2f2eab69f_XS.jpg" alt="M&eacute;xico insurgente" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Martín Becerra</p>
<p>&nbsp;</p>
<p>La declaración como actores preponderantes a América Móvil y Televisa por parte de la autoridad regulatoria de telecomunicaciones de México, afectando los potentes mercados que regentean desde hace décadas los Slim (dueño de Telmex y América Móvil) y los Azcárraga (dueños de Televisa), es una novedad volcánica en América latina que cuestiona mitos sobre la regulación de medios.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>&nbsp;</p>
<p>México registra uno de los mayores niveles de concentración de la región, tanto en telecomunicaciones como en televisión. La envergadura de América Móvil y Televisa, las fortunas acumuladas por sus propietarios y las prácticas anticompetitivas inherentes a su posición dominante tuvieron como facilitador un sistema político históricamente esquivo a los efectos de la concentración.</p>
<p>&nbsp;</p>
<p>La identificación de Televisa y Telmex/América Móvil como estorbos consta en los cables difundidos por WikiLeaks, originados en la embajada y el consulados de EE.UU. en México. La concentración de los negocios de telecomunicaciones y de televisión es un tópico de las relaciones bilaterales a partir del Tratado de Libre Comercio para América del Norte de 1994 (ver, al respecto, WikiMediaLeaks: la relación entre medios y gobiernos de América latina bajo el prisma de los cables de WikiLeaks, libro publicado por el autor de esta nota junto con Sebastián Lacunza en 2012). En el caso de las empresas de telefonía de Slim, EE.UU. también advirtió sobre las tarifas que obstruyen el acceso de los sectores más vulnerables de un país con casi 120 millones de habitantes.</p>
<p>&nbsp;</p>
<p>Paradójicamente, el presidente Enrique Peña Nieto, cuya candidatura en el PRI fue promovida por Televisa, concretó una reforma constitucional que conmueve la médula del multimedios de los Azcárraga. El llamado "telecandidato" hace tan sólo dos años, hoy rompe los lazos filiales del sempiterno PRI con Televisa y también con Slim. Peña Nieto arremete contra conglomerados no sólo enraizados en la economía y la cultura mexicanas, sino que se trata de grupos con ramificaciones en toda América y proyección global, cuyos dueños se codean con las mayores fortunas del planeta.</p>
<p>&nbsp;</p>
<p>El sentido de la reforma constitucional fue potenciado con la decisión del Instituto Federal de Telecomunicaciones (IFT), autoridad regulatoria con relativa autonomía del poder político. Para declarar preponderantes a Televisa y a América Móvil (que opera como Telcel), el IFT constató que éstos controlan más del 50% de sus mercados y que tapan la concurrencia de terceros. Además de la prevista apertura de nuevas cadenas de tv, el IFT determinó que Televisa debe proveer gratuitamente su señal a los cableoperadores competidores (must offer), limitar la exclusividad de contenidos de interés público y compartir su infraestructura. También América Móvil debe compartir infraestructura, tendrá tarifas y planes regulados y se controlarán los servicios de roaming nacional e internacional para proteger a los consumidores.</p>
<p>&nbsp;</p>
<p>Así, pues, hoy en Latinoamérica hay diversas estrategias para desconcentrar mercados de medios. Estas oscilan entre la estatización (Venezuela), la desinversión (Argentina) y la agregación de competencia con afectación de los niveles extraordinarios de rentabilidad (México).</p>
<p>&nbsp;</p>
<p>La vía mexicana se inscribe en el cuestionamiento a magnates de medios en la región, pero se rebela contra ciertos mitos: por un lado, que la bandera de la desconcentración es patrimonio exclusivo de los llamados "populismos de nueva izquierda" (Venezuela, Ecuador, Bolivia, en menor medida la Argentina), en una lectura sobreideologizada de las leyes de medios de Sudamérica. Y, por otro lado, que no es posible encarar simultáneamente la desconcentración audiovisual y de telecomunicaciones (vale enunciar la posición dominante del Grupo Clarín pero se disimula la de Telefónica), por lo que la política debería elegir un mal menor y ser asimétrica en la defensa del interés público.</p>
<p>&nbsp;</p>
<p>Foto: perfil.com</p>
<p>Fuente: <a href="http://www.perfil.com">www.perfil.com</a></p>
<p>Autor: MArtín Becerra&nbsp;</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/309-la-oportunidad-perdida">
			&laquo; La oportunidad perdida		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/311-rusia-y-la-nueva-guerra-fría">
			Rusia y la nueva Guerra Fría &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/310-méxico-insurgente#startOfPageId310">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:27:"México insurgente - Relial";s:11:"description";s:158:"En la opinión de Martín Becerra &amp;nbsp; La declaración como actores preponderantes a América Móvil y Televisa por parte de la autoridad regulatoria...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:18:"México insurgente";s:6:"og:url";s:85:"http://www.relial.org/index.php/productos/archivo/opinion/item/310-méxico-insurgente";s:8:"og:title";s:27:"México insurgente - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/9962afd4d7801adbe96e49a2f2eab69f_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/9962afd4d7801adbe96e49a2f2eab69f_S.jpg";s:14:"og:description";s:162:"En la opinión de Martín Becerra &amp;amp;nbsp; La declaración como actores preponderantes a América Móvil y Televisa por parte de la autoridad regulatoria...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:18:"México insurgente";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}