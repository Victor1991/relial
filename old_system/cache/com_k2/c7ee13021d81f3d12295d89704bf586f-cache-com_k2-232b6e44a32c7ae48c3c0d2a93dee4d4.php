<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:15308:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId296"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/71ab1197965a26d2e4379f8b23c36ebb_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/71ab1197965a26d2e4379f8b23c36ebb_XS.jpg" alt="Algunos aspectos de la iniciativa de Ley Federal de Competencia Econ&oacute;mica en M&eacute;xico" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En general, se trata de una iniciativa que actualiza de manera sustantiva el perfil de la Comisión de Competencia Económica y la dota de herramientas vanguardistas en el combate a prácticas anticompetitivas. Sin embargo, el proceso en que se apruebe debe ser abierto a la sociedad y a los propios agentes económicos.</p>
<p>&nbsp;	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p>El pasado 19 de febrero el Ejecutivo Federal envió a la Cámara de Diputados la iniciativa de Ley Federal de Competencia Económica, misma que de ser aprobada sustituirá a la Ley vigente en la materia desde 1993. Con esta iniciativa se inicia la reglamentación de las reformas constitucionales que se llevaron a cabo durante 2013. Es importante, por ello, que se abran canales de participación ciudadana para que esta reglamentación tome en cuenta todas las visiones y se puedan aterrizar de manera exitosa las reformas planteadas.</p>
<p>&nbsp;</p>
<p>Derivado de un análisis inicial a la iniciativa, destacamos algunos aspectos clave que deberán reforzarse en las discusiones legislativas, previo a la aprobación de la iniciativa.</p>
<p>&nbsp;</p>
<p>1. Sobre la organización de la Comisión</p>
<p>&nbsp;</p>
<p>La Comisión estará integrada por un pleno de 7 comisionados (antes de la reforma eran 5).</p>
<p>&nbsp;</p>
<p>Se crea una Autoridad Investigadora con autonomía técnica y de gestión (Art. 26) que tiene como objetivo separar la parte de investigación de la parte responsable de la resolución de las posibles violaciones a la Ley. El titular de esta autoridad será electo por 5 de los 7 miembros del pleno de la Comisión. Esta autoridad podrá solicitar a las autoridades públicas nacionales y extranjeras información para la sustanciación de los casos, además de que podrá presentar denuncia y querella ante la PGR por violaciones a los principios de libre competencia y concurrencia (Art. 28)</p>
<p>&nbsp;</p>
<p>Se crea una Contraloría Interna que vigilará los ingresos y egresos de la Comisión, así como el régimen de responsabilidades de los servidores públicos (Art. 37). Esta Contraloría gozará de autonomía técnica y contará con un titular electo por las dos tercera partes de la Cámara de Diputados (Art. 40).</p>
<p>&nbsp;</p>
<p>Los anteriores servidores públicos (Comisionados, Autoridad Investigadora y Contralor Interno), una vez que terminen su encargo, no podrán desempeñarse como consejeros, administradores, directores, gerentes, etc., de un agente económico que haya estado sujeto a los procedimientos de la ley por un tiempo equivalente a la tercera parte que haya durado su encargo (si el encargo dura 6 años, la prohibición, 2).</p>
<p>&nbsp;</p>
<p>2. De las facultades de la Comisión</p>
<p>&nbsp;</p>
<p>Al respecto se deben destacar tres puntos que constituyen los mayores retos del nuevo organismo autónomo. En primer lugar, la coordinación con autoridades públicas para la investigación, combate y prevención de prácticas violatoria de la Ley. Al ser un organismo autónomo deberá ser claro en establecer los mecanismos de coordinación y cumplimiento con las demás autoridades. En segundo lugar, la Comisión mantiene la facultad de ejercitar las acciones colectivas. Al respecto, de manera reglamentaria deberá instrumentar una política activa para fomentar el ejercicio de esta herramienta jurídica.</p>
<p>&nbsp;</p>
<p>Por último, la Comisión tiene entre sus facultades emitir opiniones de manera propia o a petición del ejecutivo sobre: Programas y políticas de autoridades públicas; disposiciones, reglas, acuerdos, circulares y demás actos de carácter general que emitan las autoridades públicas; iniciativas de leyes y reglamentos; disposiciones de tratados internacionales, y, en los procesos de desincorporación de entidades y activos públicos. Si bien estas opiniones no tendrán carácter vinculante, le dan a la Comisión un papel activo en el debate público en torno al mercado.</p>
<p>&nbsp;</p>
<p>3. Conflicto de competencias con IFT</p>
<p>&nbsp;</p>
<p>Las reformas en materia de competencia y telecomunicaciones otorgaron al Instituto Federal de Telecomunicaciones facultades en materia de competencia económica dentro de ese mercado. Sin embargo, en ciertos casos puede haber conflictos de competencias. La iniciativa prevé un mecanismo a través del cual un Tribunal Colegiado de Circuito especializado en la materia sea el que determine finalmente la competencia de ambos organismos autónomos.</p>
<p>&nbsp;</p>
<p>4. Sobre el control de precios</p>
<p>&nbsp;</p>
<p>En una nota del diario Reforma, se aseguraba que la iniciativa "refuerza el control de precios", esto no es así, pues se mantienen los supuestos en la determinación de precios máximos vigentes en la Ley. Esto es importante, ya que las políticas de precios máximos provocan distorsiones en el mercado que son perjudiciales tanto para el productor como para el consumidor.</p>
<p>&nbsp;</p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="images/Ley%20vigente.jpg" alt="Ley vigente" width="638" height="416" /></p>
<p>&nbsp;</p>
<p>5. Sobre las conductas anticompetitivas (Caso de insumos esenciales)</p>
<p>&nbsp;</p>
<p>La iniciativa establece las prácticas anticompetitivas que se pueden observar en los mercados. Entre ellas se encuentran las prácticas monopólicas absolutas (ej. fijar o manipular precios, o dividir o asignar porciones de mercado mediante clientela, proveedores, tiempos o espacios determinados), las prácticas monopólicas relativas (ventas o transacciones condicionadas, concertación entre agentes económicos para ejercer presión contra otro, precios predatorios), y las concentraciones ilícitas. Muchas de estas prácticas ya estaban contempladas en la Ley Vigente y se habían fortalecido en la última reforma de 2011.</p>
<p>&nbsp;</p>
<p>En este rubro se debe destacar la introducción de un elemento muy novedoso en una Ley de competencia, incluso tomando en cuenta estándares internacionales. Se trata de la introducción de un nuevo tipo de práctica monopólica relativa que castiga la denegación, restricción de acceso o acceso en condiciones discriminatorias de un insumo esencial por parte de uno o varios agentes económicos. Esto puede ser sobre todo relevante en mercados downstream en el sector energético y eléctrico, así como en sistemas de transporte.</p>
<p>&nbsp;</p>
<p>En la literatura, la denominada doctrina de los insumos esenciales [1] surgió en Estados Unidos en 1912 con un caso del sistema de trenes en Saint Louis. En este caso, existían 24 operadores de transporte en la estación de trenes de Saint Louis, de éstos se formó una asociación de 14 compañías. Ésta empezó a controlar la infraestructura de acceso de los trenes a la estación, incluido un puente que era imposible de duplicar. A la larga, esta asociación excluyó a 10 competidores del uso de la infraestructura y por lo tanto las dejó fuera de un mercado relevante. En este caso la Corte de Estados Unidos determinó que la asociación debía dar acceso al insumo esencial (infraestructura de vía). Desde entonces se han establecido criterios en otros casos que han ido fortaleciendo esta doctrina.</p>
<p>&nbsp;</p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="images/EUA%20MEX.jpg" alt="EUA MEX" width="633" height="294" /></p>
<p>&nbsp;</p>
<p>En el caso europeo [2], la Comisión de Competencia Europea inició la aplicación de criterios de insumos esenciales en 1992 con el caso Sealink/B&amp;I Holyhead y posteriormente en un relacionado el de Sea Containers/Stena Sealink. El caso consistía en que la compañía Sealink era propietaria del puerto de Holyhead y lo usaba además para ofrecer servicio de transporte; en el primer caso, existía tratamiento desfavorable para competidores que usaban los servicios del puerto de carga; en el segundo, el quejoso no requería los servicios del puerto pero necesitaba acceso para entrar en mercados relevantes. En ambos casos la Comisión falló a favor de los quejosos garantizando acceso al insumo esencial (puerto).</p>
<p>&nbsp;</p>
<p>Hay que notar que la determinación de estos casos requiere de un alto nivel de estudio por parte de la autoridad. Asimismo, se deben notar las limitaciones que se tienen en estos casos: a través de la determinación de la autoridad para que el agente dominante negocie o llegue a un arreglo contractual con el demandante del acceso al insumo, se trata de compensar la debilidad de la estructura competitiva de un mercado, debido a la existencia de un insumo esencial. Esto es, si bien no resuelve el problema de fondo en materia de competencia, a través de estas decisiones se puede mantener o crear ciertos esquemas de competencia.</p>
<p>&nbsp;</p>
<p>La doctrina de los insumos esenciales no ha estado exenta de críticas y se sigue debatiendo su uso y alcances [3]. Por lo que, seguramente, será centro de la polémica en torno a la discusión de esta iniciativa. Sin embargo, es un elemento que debe mantenerse en la Ley para fomentar la competencia en sectores, que por las mismas reformas, enfrentarán procesos de desincorporación o de apertura de mercados en distintos segmentos de la cadena del sector (claramente el energético). Mantener este criterio también será fundamental para la determinación de casos en los nuevos Tribunales especializados en materia de competencia.</p>
<p>&nbsp;</p>
<p>6. Sobre las sanciones</p>
<p>&nbsp;</p>
<p>En este rubro, la iniciativa retoma buenas prácticas internacionales que se habían adoptado con la reforma de 2011 a la Ley vigente. En este sentido se mantienen penas de hasta el 10% de los ingresos del agente económico o hasta 180 mil veces el salario mínimo vigente en el Distrito Federal para quienes incurran en prácticas anticompetitivas (Art. 120). Asimismo, la iniciativa establece criterios de desincorporación de activos en la proporción necesaria para restablecer la competencia en el mercado. Esto último es relevante, sobre todo, en el caso de fusiones entre empresas.</p>
<p>&nbsp;</p>
<p>Si bien no es un resumen exhaustivo se trataron de recoger los puntos más relevantes de la propuesta. En general, se trata de una iniciativa que actualiza de manera sustantiva el perfil de la Comisión de Competencia Económica y la dota de herramientas vanguardistas en el combate a prácticas anticompetitivas. Sin embargo, el proceso en que se apruebe debe ser abierto a la sociedad y a los propios agentes económicos; la importancia de la iniciativa es para el mercado de la misma magnitud que la reforma política para la democracia. En la medida en que el congreso garantice la pluralidad de visiones en la discusión de la iniciativa se fortalecerá para asegurar un mercado en competencia y libertad que beneficie a los consumidores.</p>
<p>&nbsp;</p>
<p>Autor: <a href="https://twitter.com/carlosmartinezv">Carlos Martinez Velázquez</a></p>
<p>Foto: Animal político</p>
<p>Fuente: <a href="http://www.animalpolitico.com/blogueros-c-al-cubo/2014/02/21/algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-economica/#axzz2tpOqOLNG">Animal político</a></p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/293-declaración-de-los-amigos-de-la-carta-democrática-interamericana-sobre-la-situación-en-venezuela">
			&laquo; Declaracion de los Amigos de la Carta Democrática Interamericana Sobre la Situación en Venezuela		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/298-manifiesto-de-mérida-de-los-estudiantes">
			Manifiesto de Mérida de los estudiantes &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico#startOfPageId296">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:94:"Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México - Relial";s:11:"description";s:154:"En general, se trata de una iniciativa que actualiza de manera sustantiva el perfil de la Comisión de Competencia Económica y la dota de herramientas...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:85:"Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México";s:6:"og:url";s:155:"http://www.relial.org/index.php/productos/archivo/actualidad/item/296-algunos-aspectos-de-la-iniciativa-de-ley-federal-de-competencia-económica-en-méxico";s:8:"og:title";s:94:"Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/71ab1197965a26d2e4379f8b23c36ebb_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/71ab1197965a26d2e4379f8b23c36ebb_S.jpg";s:14:"og:description";s:154:"En general, se trata de una iniciativa que actualiza de manera sustantiva el perfil de la Comisión de Competencia Económica y la dota de herramientas...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:85:"Algunos aspectos de la iniciativa de Ley Federal de Competencia Económica en México";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}