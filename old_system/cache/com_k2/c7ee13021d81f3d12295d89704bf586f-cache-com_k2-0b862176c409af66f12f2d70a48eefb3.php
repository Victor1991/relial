<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4855:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId53"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/90701d02ae3da0e5a21abbd900c25748_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/90701d02ae3da0e5a21abbd900c25748_XS.jpg" alt="Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item text -->
	  <div class="itemFullText">
	  	<p>— February 06, 2009 — Carlos Alberto Montaner responde a diversos cuestionamientos relacionados con el asistencialismo, el cual desde la perspectiva liberal, es una pauta de comportamiento muy nociva para las sociedades, dado que este modelo no permite un adecuado desarrollo social, pues es evidente que la riqueza creada por la sociedad, rápidamente se convierte en botín para los políticos.</p>
<p>&nbsp;</p>
<p>Por otro lado, manifiesta que liberales proponen políticas de Estado que fomenten riqueza, con el fin de que las personas no necesiten asistencia y obtengan a cambio mejor nivel educativo, valores adecuados y capacidad de decisión con respecto a sus fondos económicos.</p>
<p>&nbsp;</p>
<p>Una producción de New Media<br />Universidad Francisco Marroquín. Guatemala 2009<br /><a href="http://www.newmedia.ufm.edu">http://www.newmedia.ufm.edu</a><br /><a href="http://www.ufm.edu">http://www.ufm.edu</a></p>	  </div>
	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
  
  
  
	
  
	<div class="clr"></div>

    <!-- Item video -->
  <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

  <div class="itemVideoBlock">
  	<h3>K2_MEDIA</h3>

				<span class="itemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:500px;" class="avPlayerContainer">
		<div id="AVPlayerID_471e9750_294406334" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/nUAwbyq3d5o?rel=0&amp;fs=1&amp;wmode=transparent" width="500" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		
	  
	  
	  <div class="clr"></div>
  </div>
  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/multimedia/videos/item/53-liberalismo-y-asistencialismo-entrevista-a-dr-carlos-alberto-montaner#startOfPageId53">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:80:"Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner - Relial";s:11:"description";s:157:"— February 06, 2009 — Carlos Alberto Montaner responde a diversos cuestionamientos relacionados con el asistencialismo, el cual desde la perspectiva l...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:71:"Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner";s:6:"og:url";s:127:"http://www.relial.org/index.php/multimedia/videos/item/53-liberalismo-y-asistencialismo-entrevista-a-dr-carlos-alberto-montaner";s:8:"og:title";s:80:"Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/90701d02ae3da0e5a21abbd900c25748_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/90701d02ae3da0e5a21abbd900c25748_S.jpg";s:14:"og:description";s:157:"— February 06, 2009 — Carlos Alberto Montaner responde a diversos cuestionamientos relacionados con el asistencialismo, el cual desde la perspectiva l...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:3:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:72:"/plugins/content/jw_allvideos/jw_allvideos/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:11:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:78:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/mediaplayer/jwplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:79:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/silverlight.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:77:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/wmvplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:86:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/quicktimeplayer/AC_QuickTime.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Multimedia";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:6:"Videos";s:4:"link";s:20:"index.php?Itemid=137";}i:2;O:8:"stdClass":2:{s:4:"name";s:71:"Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}