<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5813:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId426"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Asociación de Jóvenes Argentinos Liberales
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/2d535442c2c0b0669d8f5a051ed00bcc_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/2d535442c2c0b0669d8f5a051ed00bcc_XS.jpg" alt="Asociaci&oacute;n de J&oacute;venes Argentinos Liberales" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En Junio de 2014 se conformó JAL, la primera Asociación de Jóvenes Argentinos Liberales con el apoyo e impulso de la Fundación Friedrich Naumann para la Libertad.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>&nbsp;</p>
<p>La misma, cuenta con la participación de representantes de las siguientes organizaciones: Estudiantes por la Libertad, Fundación Bases, Fundación Federalismo y Libertad, HACER (Hispanic American Center for Economic Research), Fundación Libertad, Fundación Progreso y Libertad, Instituto Amagi, Centro de Estudios LIBRE, Club de la Libertad, Fundación Libertad y Progreso e IDEAR.</p>
<p>&nbsp;</p>
<p>Diferentes tipos de instituciones están invitadas a sumarse a esta nueva iniciativa que pretende consolidar la unión de la juventud liberal de todo el país para difundir las ideas de la libertad y ser capaz de cambiar los paradigmas vigentes en la Argentina.</p>
<p>&nbsp;</p>
<p>Es por ello que ha asumido dentro de ese marco el compromiso de difundir e implementar principios y valores liberales tales como el Gobierno Limitado, Estado de Derecho, la Libertad Económica y Política, la Responsabilidad Individual y el Federalismo, así como fomentar una agenda temática liberal que dé respuesta a las diversas problemáticas que aquejan al país, y en ese marco, fortalecer la cooperación y coordinación entre sus miembros.</p>
<p>&nbsp;</p>
<p>Esta asociación apuesta a la energía, la motivación, el compromiso, la capacidad y el trabajo en equipo de quienes, en cada uno de sus espacios, promueven la participación activa, la formación, el debate de ideas. Aquellos jóvenes que buscan generar un verdadero cambio en la sociedad y convertirse en protagonistas en la construcción de un futuro de progreso y libertad.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: María José Romaro Boscarino</p>
<p>Foto: JAL</p>
<p>Fuente: Texto proporcionado por el autor</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/actualidad/item/425-diálogo-“la-defensa-de-la-libertad-y-la-lucha-contra-el-racismo”">
			&laquo; Diálogo “La defensa de la libertad y la lucha contra el racismo”		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/actualidad/item/428-joven-becario-de-universidad-de-la-libertad-es-candidato-a-regidor">
			Joven becario de Universidad de la Libertad es candidato a regidor &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/actualidad/item/426-asociación-de-jóvenes-argentinos-liberales#startOfPageId426">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:53:"Asociación de Jóvenes Argentinos Liberales - Relial";s:11:"description";s:156:"En Junio de 2014 se conformó JAL, la primera Asociación de Jóvenes Argentinos Liberales con el apoyo e impulso de la Fundación Friedrich Naumann para...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:44:"Asociación de Jóvenes Argentinos Liberales";s:6:"og:url";s:114:"http://www.relial.org/index.php/productos/archivo/actualidad/item/426-asociación-de-jóvenes-argentinos-liberales";s:8:"og:title";s:53:"Asociación de Jóvenes Argentinos Liberales - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/2d535442c2c0b0669d8f5a051ed00bcc_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/2d535442c2c0b0669d8f5a051ed00bcc_S.jpg";s:14:"og:description";s:156:"En Junio de 2014 se conformó JAL, la primera Asociación de Jóvenes Argentinos Liberales con el apoyo e impulso de la Fundación Friedrich Naumann para...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:10:"Actualidad";s:4:"link";s:20:"index.php?Itemid=132";}i:3;O:8:"stdClass":2:{s:4:"name";s:44:"Asociación de Jóvenes Argentinos Liberales";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}