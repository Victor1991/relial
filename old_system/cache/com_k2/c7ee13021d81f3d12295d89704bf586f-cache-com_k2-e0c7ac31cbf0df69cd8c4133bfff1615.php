<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16732:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

				<!-- Category block -->
		<div class="itemListCategory">

			
			
						<!-- Category title -->
			<h2>Opinión</h2>
			
						<!-- Category description -->
			<p></p>
			
			<!-- K2 Plugins: K2CategoryDisplay -->
			
			<div class="clr"></div>
		</div>
		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación">
	  		Colombia y la farsa de la reconciliación	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación" title="Colombia y la farsa de la reconciliaci&oacute;n">
		    	<img src="/media/k2/items/cache/f68bc1606a499c66a1eabd66e99d6817_XS.jpg" alt="Colombia y la farsa de la reconciliaci&oacute;n" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>El presidente Juan Manuel Santos ha llevado a algunas víctimas a La Habana para que se reconcilien con sus verdugos. La idea detrás de la ceremonia se origina en las terapias sicológicas. Es una extensión de los procesos de sanación de las parejas en las que se produce un agravio severo. Quien cometió la falta asume la culpa, se arrepiente, y la víctima perdona. A partir de ese punto retoman la relación y, poco a poco, se restauran los vínculos emocionales. Sin ese proceso es difícil la recuperación de la confianza en el otro.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/433-colombia-y-la-farsa-de-la-reconciliación">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos">
	  		El sistema liberal ha mejorado la vida de los peruanos	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos" title="El sistema liberal ha mejorado la vida de los peruanos">
		    	<img src="/media/k2/items/cache/4653e069ed7369840191e8bf38ab8dc9_XS.jpg" alt="El sistema liberal ha mejorado la vida de los peruanos" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Entrevista a Héctor Ñaupari</p>
<p>&nbsp;</p>
<p>El abogado, ensayista y poeta limeño analiza el "giro capitalista" que ha asumido Perú, explicando cómo la economía de mercado ha llevado progreso y bienestar al país latinoamericano.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/431-el-sistema-liberal-ha-mejorado-la-vida-de-los-peruanos">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho">
	  		El populismo está en guerra con el estado de derecho	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho" title="El populismo est&aacute; en guerra con el estado de derecho">
		    	<img src="/media/k2/items/cache/37e725efe26e0487bc83287a1c350936_XS.jpg" alt="El populismo est&aacute; en guerra con el estado de derecho" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>La estabilidad económica y del desarrollo depende de la calidad de las instituciones nacionales, afirma el economista argentino Martin Krause, creador del Índice de Calidad Institucional y académico adjunto del Instituto El Cato.</p>
<p>&nbsp;</p>
<p>Durante su visita a Caracas el pasado jueves, Krause ofreció la conferencia "Política pública liberal: ¿Cómo superar la pobreza?". Ésta se realizó como parte de la reunión general anual de CEDICE Libertad, un instituto de políticas libertarias situado en Venezuela.</p>
<p>&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/427-el-populismo-está-en-guerra-con-el-estado-de-derecho">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte">
	  		Manuel una noche echó a andar rumbo al norte	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte" title="Manuel una noche ech&oacute; a andar rumbo al norte">
		    	<img src="/media/k2/items/cache/00c636e00bee0ba03b364841363f738b_XS.jpg" alt="Manuel una noche ech&oacute; a andar rumbo al norte" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opiniòn de Carlos A. Montaner</p>
<p>&nbsp;</p>
<p>Vale la pena, aunque pone los pelos de punta, leer el informe sobre "Extorsiones y Secuestros por Rescate" de la empresa Hazelwood Street, dirigida por el abogado Bruce Kaplan. Se puede consultar por medio de internet. De esos sangrientos y millonarios negocios viven los más siniestros grupos terroristas del planeta: los narcotraficantes, las mafias étnicas, los mareros, y todo aquel que posee un arma de fuego, y carece de escrúpulos y de temor a una casi siempre inexistente justicia.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/422-manuel-una-noche-echó-a-andar-rumbo-al-norte">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman">
	  		El legado de Milton Friedman	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman" title="El legado de Milton Friedman">
		    	<img src="/media/k2/items/cache/390c9d1de2a80a844d0e01ba21c1192e_XS.jpg" alt="El legado de Milton Friedman" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>En la opinión de Óscar Álvarez Araya</p>
<p>&nbsp;</p>
<p>El 31 de julio del 2014 se cumplen los 102 años del nacimiento de Milton Friedman, economista, estadístico e intelectual estadounidense, Padre del Monetarismo Moderno y Premio Nobel de Economía en 1976. De origen judío austro húngaro, había nacido el 31 de julio de 1912 en Brooklyn, Nueva York.</p>
	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/419-el-legado-de-milton-friedman">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes">
	  		Defaults eran los de antes	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes" title="Defaults eran los de antes">
		    	<img src="/media/k2/items/cache/f75f45065e491a3adc61e72a384867bb_XS.jpg" alt="Defaults eran los de antes" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p style="margin-left: 30px;">La opinión de Martín Simonetta</p>
<p style="margin-left: 30px;">&nbsp;</p>
<p style="margin-left: 30px;">A finales de 1902, las costas de Venezuela fueron bombardeadas por unidades navales de Gran Bretaña y Alemania, a las que se sumaron las de Italia, con el objetivo de exigir el cobro de deudas del gobierno venezolano pendientes con particulares europeos.</p>
<p style="margin-left: 30px;">&nbsp;	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/productos/archivo/opinion/item/414-defaults-eran-los-de-antes">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/productos/archivo/opinion?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/productos/archivo/opinion?start=18" class="pagenav">Anterior</a></li><li><a title="1" href="/index.php/productos/archivo/opinion?limitstart=0" class="pagenav">1</a></li><li><a title="2" href="/index.php/productos/archivo/opinion?start=6" class="pagenav">2</a></li><li><a title="3" href="/index.php/productos/archivo/opinion?start=12" class="pagenav">3</a></li><li><a title="4" href="/index.php/productos/archivo/opinion?start=18" class="pagenav">4</a></li><li><span class="pagenav">5</span></li><li><a title="6" href="/index.php/productos/archivo/opinion?start=30" class="pagenav">6</a></li><li><a title="7" href="/index.php/productos/archivo/opinion?start=36" class="pagenav">7</a></li><li><a title="8" href="/index.php/productos/archivo/opinion?start=42" class="pagenav">8</a></li><li><a title="9" href="/index.php/productos/archivo/opinion?start=48" class="pagenav">9</a></li><li><a title="10" href="/index.php/productos/archivo/opinion?start=54" class="pagenav">10</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/productos/archivo/opinion?start=30" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/productos/archivo/opinion?start=84" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 5 de 15	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:17:"Opinión - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:8:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:62:"http://relial.org/index.php/productos/archivo/opinion?start=24";s:8:"og:title";s:17:"Opinión - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:43:"http://relial.org/media/k2/categories/2.jpg";s:5:"image";s:43:"http://relial.org/media/k2/categories/2.jpg";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}}s:6:"module";a:0:{}}