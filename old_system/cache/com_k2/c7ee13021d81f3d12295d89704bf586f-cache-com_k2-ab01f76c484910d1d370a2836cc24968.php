<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9038:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId354"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza?
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/7081cca2f9cd0c06f2cce9e93d01dda9_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/7081cca2f9cd0c06f2cce9e93d01dda9_XS.jpg" alt="Chile: &iquest;para qu&eacute; imitar a Venezuela cuando se puede emular a Suiza?" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  <!-- Item introtext -->
	  <div class="itemIntroText">
	  	<p>En la opinión de Carlos Alberto Montaner</p>
<p>&nbsp;</p>
<p>La presidente chilena Michelle Bachelet quiere reducir la desigualdad. Me sospecho que se refiere a la desigualdad de resultados, que es la que mide el coeficiente Gini. Pero es posible que en su afán nivelador acabe desplumando a la gallina de los huevos de oro.</p>
	  </div>
	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	
<p>Corrado Gini fue un brillante estadístico italiano de principios del siglo XX, fascista en su juventud, quien, fiel a sus orígenes ideológicos, propenso a estabular a las personas en estamentos, dividió a la sociedad en quintiles y midió los niveles de ingresos que percibía cada 20%.</p>
<p>&nbsp;</p>
<p>En su fórmula matemática, cero correspondía a una sociedad en la que todos recibían la misma renta, y cien a aquella en la que una persona acaparaba la totalidad de los ingresos. De su índice se colegía que las sociedades más justas eran las que se acercaban a O, y las más injustas, las que se aproximaban a 100.</p>
<p>&nbsp;</p>
<p>Como suelen decir los brasileros, Gini tenía razón, pero poca, y la poca que tenía no servía de nada. Chile, de acuerdo con el Banco Mundial, tiene 52.1 de desigualdad (mejor que Brasil, Colombia y Panamá, por cierto), mientras Etiopía, la India y Mali andan por el 33. Es difícil creer que estos tres países son más justos que Chile.</p>
<p>&nbsp;</p>
<p>Es verdad que los países escandinavos, los mejor organizados y ricos del planeta, se mueven en una franja entre 20 y 30, pero Kenya exhibe un honroso 29 que sólo demuestra que la poca riqueza que produce está menos mal repartida que la que muestra Sudáfrica con 63.1, uno de los peores guarismos del mundo.</p>
<p>&nbsp;</p>
<p>Es una lástima que, pese a su experiencia como jefe de gobierno, la señora Bachelet no haya advertido que su país logró ponerse a la cabeza de América Latina, y consiguió reducir la pobreza de un 45% a un 13%, no repartiendo, sino creando riqueza.</p>
<p>&nbsp;</p>
<p>Cuando la señora Bachelet examina a las sociedades escandinavas observa que hay en ellas un alto nivel de riqueza e igualdad junto a una tasa impositiva cercana al 50% del PIB y supone, equivocadamente, que los tres datos se encadenan. Incurre en un non sequitur.</p>
<p>&nbsp;</p>
<p>Sencillamente, no es cierto. La riqueza escandinava, como la de cualquier sociedad, se debe a la laboriosidad y la creatividad de todos los trabajadores dentro de las empresas, desde el presidente hasta el señor de la limpieza, pasando por los ejecutivos.</p>
<p>&nbsp;</p>
<p>Supongo que ella entiende que donde único se crea riqueza es en actividades que generan beneficio, ahorran, innovan e invierten. Es decir, en las empresas, de cualquier tamaño que sean.</p>
<p>&nbsp;</p>
<p>¿Y por qué está mejor repartida la riqueza en Escandinavia que en Chile?</p>
<p>&nbsp;</p>
<p>Los socialistas suelen pensar que es el resultado de la alta tasa impositiva, pero no es verdad. La falacia lógica parte de creer que la consecuencia se deriva de la premisa, cuando no es así. Sucede a la inversa: el alto gasto público es posible (aunque no sea conveniente) porque la sociedad segrega una gran cantidad de excedente.</p>
<p>&nbsp;</p>
<p>Lo que genera la equidad en las sociedades prósperas y abiertas es la calidad de su aparato productivo. Si una sociedad fabrica maquinarias apreciadas, objetos con alto contenido tecnológico, medicinas valiosas y originales, o suministra servicios sofisticados por medio de su tejido empresarial, será recompensada por el mercado y podrá y tendrá que pagarles a los trabajadores un salario sustancial de acuerdo con sus calificaciones para poder reclutarlos y competir.</p>
<p>&nbsp;</p>
<p>Si Bachelet desea reducir la pobreza chilena y construir una sociedad más equitativa, no debe generar una atmósfera de lucha de clases y obstaculizar la labor de las empresas, sino todo lo contrario: debe facilitarla.</p>
<p>&nbsp;</p>
<p>¿Cómo? Propiciando las inversiones nacionales y extranjeras con un clima económico y legal hospitalario; agilizando y simplificando los trámites burocráticos, incluida la solución de los inevitables conflictos; facilitando la entrada al mercado de los emprendedores; estimulando la investigación; creando infraestructuras (puertos marítimos y aéreos, carreteras, telefonía, electrificación, Internet) que aceleren las transacciones; multiplicando el capital humano y cultivando la estabilidad institucional, la transparencia y la honradez administrativa.</p>
<p>&nbsp;</p>
<p>Es verdad que ese tipo de gobierno no gana titulares de periódicos ni el aplauso de la devastadora izquierda revolucionaria, pero logra multiplicar la riqueza, disminuye la pobreza y aumenta el porcentaje de la renta que recibe la clase trabajadora.</p>
<p>&nbsp;</p>
<p>Lo dicho: ¿para qué imitar a Venezuela cuando se puede emular a Suiza? Casi nadie sabe quién es el presidente de Suiza, pero hacia ese país se abalanza el dinero cada vez que hay una crisis. Por algo será.</p>
<p>&nbsp;</p>
<p>Autor: Carlos Alberto Montaber</p>
<p>Fuente: Texto proporcionado por el autor</p>
<p>Foto: El blog de Montaner</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
    <!-- Item navigation -->
  <div class="itemNavigation">
  	<span class="itemNavigationTitle">más en esta categoría</span>

				<a class="itemPrevious" href="/index.php/productos/archivo/opinion/item/350-el-secreto-de-los-estados-totalitarios">
			&laquo; El secreto de los estados totalitarios		</a>
		
				<a class="itemNext" href="/index.php/productos/archivo/opinion/item/355-el-porqué-de-la-crisis-venezolana">
			El porqué de la crisis venezolana &raquo;
		</a>
		
  </div>
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/productos/archivo/opinion/item/354-chile-¿para-qué-imitar-a-venezuela-cuando-se-puede-emular-a-suiza?#startOfPageId354">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:78:"Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza? - Relial";s:11:"description";s:153:"En la opinión de Carlos Alberto Montaner &amp;nbsp; La presidente chilena Michelle Bachelet quiere reducir la desigualdad. Me sospecho que se refiere...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:69:"Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza?";s:6:"og:url";s:216:"http://www.relial.org/index.php/productos/archivo/opinion/item/354-chile-ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿para-quÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â©-imitar-a-venezuela-cuando-se-puede-emular-a-suiza";s:8:"og:title";s:78:"Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza? - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:81:"http://www.relial.org/media/k2/items/cache/7081cca2f9cd0c06f2cce9e93d01dda9_S.jpg";s:5:"image";s:81:"http://www.relial.org/media/k2/items/cache/7081cca2f9cd0c06f2cce9e93d01dda9_S.jpg";s:14:"og:description";s:157:"En la opinión de Carlos Alberto Montaner &amp;amp;nbsp; La presidente chilena Michelle Bachelet quiere reducir la desigualdad. Me sospecho que se refiere...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Productos";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:7:"Archivo";s:4:"link";s:20:"index.php?Itemid=130";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"Opinión";s:4:"link";s:20:"index.php?Itemid=131";}i:3;O:8:"stdClass":2:{s:4:"name";s:69:"Chile: ¿para qué imitar a Venezuela cuando se puede emular a Suiza?";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}