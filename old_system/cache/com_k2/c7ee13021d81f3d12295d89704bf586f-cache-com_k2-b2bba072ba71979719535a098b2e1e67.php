<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:14723:"
<!-- Start K2 Category Layout -->
<div id="k2Container" class="itemListView">

	
	
		<!-- Blocks for current category and subcategories -->
	<div class="itemListCategoriesBlock">

		
		
	</div>
	


		<!-- Item list -->
	<div class="itemList">

		
				<!-- Primary items -->
		<div id="itemListPrimary">
						
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/54-carlos-alberto-montaner-comenta-las-últimas-elecciones-en-américa-latina">
	  		Carlos Alberto Montaner comenta las últimas elecciones en América Latina	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/multimedia/videos/item/54-carlos-alberto-montaner-comenta-las-últimas-elecciones-en-américa-latina" title="Carlos Alberto Montaner comenta las &uacute;ltimas elecciones en Am&eacute;rica Latina">
		    	<img src="/media/k2/items/cache/a27a3b73d355048c6bab885897085f62_XS.jpg" alt="Carlos Alberto Montaner comenta las &uacute;ltimas elecciones en Am&eacute;rica Latina" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Entrevista a Carlos Alberto Montaner, escritor cubano, realizada el 9 de dicembre de 2009 en el Cato Institute.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_64bd8e78_1462385217" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/h9Gq4OYKBrI?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/54-carlos-alberto-montaner-comenta-las-últimas-elecciones-en-américa-latina">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/53-liberalismo-y-asistencialismo-entrevista-a-dr-carlos-alberto-montaner">
	  		Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/multimedia/videos/item/53-liberalismo-y-asistencialismo-entrevista-a-dr-carlos-alberto-montaner" title="Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner">
		    	<img src="/media/k2/items/cache/90701d02ae3da0e5a21abbd900c25748_XS.jpg" alt="Liberalismo y asistencialismo: entrevista a Dr. Carlos Alberto Montaner" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>— February 06, 2009 — Carlos Alberto Montaner responde a diversos cuestionamientos relacionados con el asistencialismo, el cual desde la perspectiva liberal, es una pauta de comportamiento muy nociva para las sociedades, dado que este modelo no permite un adecuado desarrollo social, pues es evidente que la riqueza creada por la sociedad, rápidamente se convierte en botín para los políticos.</p>
<p>&nbsp;</p>
<p>Por otro lado, manifiesta que liberales proponen políticas de Estado que fomenten riqueza, con el fin de que las personas no necesiten asistencia y obtengan a cambio mejor nivel educativo, valores adecuados y capacidad de decisión con respecto a sus fondos económicos.</p>
<p>&nbsp;</p>
<p>Una producción de New Media<br />Universidad Francisco Marroquín. Guatemala 2009<br /><a href="http://www.newmedia.ufm.edu">http://www.newmedia.ufm.edu</a><br /><a href="http://www.ufm.edu">http://www.ufm.edu</a></p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_471e9750_430534783" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/nUAwbyq3d5o?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/53-liberalismo-y-asistencialismo-entrevista-a-dr-carlos-alberto-montaner">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/52-los-guardianes-de-chávez">
	  		Los guardianes de Chávez	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/multimedia/videos/item/52-los-guardianes-de-chávez" title="Los guardianes de Ch&aacute;vez">
		    	<img src="/media/k2/items/cache/22c02097e4438bd2f2f3fe4a6a3ab0e1_XS.jpg" alt="Los guardianes de Ch&aacute;vez" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>¿Por qué Chávez está armando a grupos irregulares de chavistas? Impresionante reportaje de la televisión española.</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_493809b7_966181386" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/GGgf4pdCUos?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/52-los-guardianes-de-chávez">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									
						
			<div class="itemContainer itemContainerLast" style="width:100.0%;">
				
<!-- Start K2 Item Layout -->
<div class="catItemView groupPrimary">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="catItemHeader">
		
	  	  <!-- Item title -->
	  <h3 class="catItemTitle">
			
	  				<a href="/index.php/multimedia/videos/item/51-documental-completo-los-nietos-de-la-revolución-cubana">
	  		Documental Completo: Los nietos de la Revolución Cubana	  	</a>
	  	
	  		  </h3>
	  
		  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="catItemImageBlock">
		  <span class="catItemImage">
		    <a href="/index.php/multimedia/videos/item/51-documental-completo-los-nietos-de-la-revolución-cubana" title="Documental Completo: Los nietos de la Revoluci&oacute;n Cubana">
		    	<img src="/media/k2/items/cache/eb6c7c01c4e98e1f2578f9959463b973_XS.jpg" alt="Documental Completo: Los nietos de la Revoluci&oacute;n Cubana" style="width:100px; height:auto;" />
		    </a>
		  </span>
		  <div class="clr"></div>
	  </div>
	  
	  	  <!-- Item introtext -->
	  <div class="catItemIntroText">
	  	<p>Plasma la frustración actual de una generación de jóvenes que rechaza los dogmas revolucionarios y se siente condenada a la marginalidad por el sistema.</p>
<p>&nbsp;</p>
<p>Dirigido por el cubano-estadounidense Carlos Montaner, el documental de una hora de duración, grabado de forma clandestina en la isla, desprende a través de los testimonios recogidos, un pesimismo ácido y una insatisfacción permanente hacia el régimen cubano.</p>
<p>&nbsp;</p>
<p>De la visión que los jóvenes entrevistados dan de la vida cubana se concluye que "no existe una vinculación emocional de éstos con el proceso histórico de la revolución", señaló el creador, hijo del periodista y escritor cubano exiliado Carlos Alberto Montaner.</p>
<p>&nbsp;</p>
<p>El documental ofrece numerosos testimonios directos de jóvenes, la mayor parte menores de 35 años, muchos de ellos desmotivados y dominados por una suerte de apatía respecto de la posibilidad de lograr un mejor futuro. Entre los jóvenes entrevistados figuran las ...</p>	  </div>
	  
		<div class="clr"></div>

	  
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

  
	<div class="clr"></div>

    <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3>K2_RELATED_VIDEO</h3>
				<span class="catItemVideo">

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) starts here -->

<div class="avPlayerWrapper avVideo">
	<div style="width:400px;" class="avPlayerContainer">
		<div id="AVPlayerID_d0f317a2_1347748405" class="avPlayerBlock">
			<iframe src="http://www.youtube.com/embed/yxRVj3HWQog?rel=0&amp;fs=1&amp;wmode=transparent" width="400" height="300" frameborder="0" allowfullscreen title="JoomlaWorks AllVideos Player"></iframe>					</div>
	</div>
</div>

<!-- JoomlaWorks "AllVideos" Plugin (v4.4) ends here -->

</span>
		  </div>
  
  
  <div class="clr"></div>

	
		<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="/index.php/multimedia/videos/item/51-documental-completo-los-nietos-de-la-revolución-cubana">
			leer más		</a>
	</div>
	
	<div class="clr"></div>

	
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
			</div>
						<div class="clr"></div>
									<div class="clr"></div>
		</div>
		
		
		
	</div>

	<!-- Pagination -->
		<div class="k2Pagination">
		<ul><li class="pagination-start"><a title="Inicio" href="/index.php/multimedia/videos?limitstart=0" class="pagenav">Inicio</a></li><li class="pagination-prev"><a title="Anterior" href="/index.php/multimedia/videos?start=32" class="pagenav">Anterior</a></li><li><a title="2" href="/index.php/multimedia/videos?start=4" class="pagenav">2</a></li><li><a title="3" href="/index.php/multimedia/videos?start=8" class="pagenav">3</a></li><li><a title="4" href="/index.php/multimedia/videos?start=12" class="pagenav">4</a></li><li><a title="5" href="/index.php/multimedia/videos?start=16" class="pagenav">5</a></li><li><a title="6" href="/index.php/multimedia/videos?start=20" class="pagenav">6</a></li><li><a title="7" href="/index.php/multimedia/videos?start=24" class="pagenav">7</a></li><li><a title="8" href="/index.php/multimedia/videos?start=28" class="pagenav">8</a></li><li><a title="9" href="/index.php/multimedia/videos?start=32" class="pagenav">9</a></li><li><span class="pagenav">10</span></li><li><a title="11" href="/index.php/multimedia/videos?start=40" class="pagenav">11</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php/multimedia/videos?start=40" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Final" href="/index.php/multimedia/videos?start=40" class="pagenav">Final</a></li></ul>		<div class="clr"></div>
		Página 10 de 11	</div>
	
	</div>
<!-- End K2 Category Layout -->
";s:4:"head";a:10:{s:5:"title";s:15:"Videos - Relial";s:11:"description";s:0:"";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:8:"keywords";N;s:6:"rights";N;s:6:"og:url";s:54:"http://relial.org/index.php/multimedia/videos?start=36";s:8:"og:title";s:15:"Videos - Relial";s:7:"og:type";s:7:"Article";s:14:"og:description";s:0:"";}}s:5:"links";a:0:{}s:11:"styleSheets";a:3:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:72:"/plugins/content/jw_allvideos/jw_allvideos/tmpl/Classic/css/template.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:11:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:67:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:78:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/mediaplayer/jwplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:79:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/silverlight.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:77:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/wmvplayer/wmvplayer.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:86:"/plugins/content/jw_allvideos/jw_allvideos/includes/js/quicktimeplayer/AC_QuickTime.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Multimedia";s:4:"link";s:1:"#";}i:1;O:8:"stdClass":2:{s:4:"name";s:6:"Videos";s:4:"link";s:20:"index.php?Itemid=137";}}s:6:"module";a:0:{}}