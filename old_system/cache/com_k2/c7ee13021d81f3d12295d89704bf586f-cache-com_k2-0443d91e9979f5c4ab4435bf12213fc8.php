<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8910:"

<!-- Start K2 Item Layout -->
<span id="startOfPageId432"></span>

<div id="k2Container" class="itemView">

	<!-- Plugins: BeforeDisplay -->
	
	<!-- K2 Plugins: K2BeforeDisplay -->
	
	<div class="itemHeader">

		
	  	  <!-- Item title -->
	  <h2 class="itemTitle">
			
	  	El suicidio del humorista
	  	
	  </h2>
	  
		
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  
  <!-- K2 Plugins: K2AfterDisplayTitle -->
  
	
	
  <div class="itemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  
	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  
	  	  <!-- Item Image -->
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="/media/k2/items/cache/8376aace7af18ea8cafa499d7e69a6ec_XL.jpg" title="K2_CLICK_TO_PREVIEW_IMAGE">
		  		<img src="/media/k2/items/cache/8376aace7af18ea8cafa499d7e69a6ec_XS.jpg" alt="El suicidio del humorista" style="width:100px; height:auto;" />
		  	</a>
		  </span>

		  
		  
		  <div class="clr"></div>
	  </div>
	  
	  	  	  	  <!-- Item fulltext -->
	  <div class="itemFullText">
	  	</p>
<p align="center" style="text-align: justify;">A propósito de Robin Williams, el derecho al suicidio y la incorrección política por Héctor Ñaupari[1]</p>
<p>&nbsp;</p>
<p>De acuerdo con el libro del historiador inglés Paul Johnson, Humoristas[2] , éstos parecen condenados a perpetrar un desencantado final a sus propias vidas. El suicidio del brillante actor Robin Williams parece confirmar esa regla. Sostiene el autor de Intelectuales que un rasgo común a los cómicos es que son personas sin una gota de humor en su fuero más íntimo.</p>
<p>&nbsp;</p>
<p>Johnson nos habla en su libro de una aflicción que enhebra a la mayor parte de los bromistas profesionales, causado su fina percepción de las desgracias propias y ajenas, lo cual los hace propensos al llamado "síndrome de la melancolía" que aquejó al príncipe Hamlet cuando recuerda al bufón Yorick, en la tragedia que lleva su nombre: "¿Qué se hicieron tus burlas, tus brincos, tus cantares y aquellos chistes repentinos que de ordinario animaban la mesa con alegre estrépito?" [3] se pregunta el perturbado príncipe de Dinamarca.</p>
<p>&nbsp;</p>
<p>Orillados a ese mismo sentimiento por la repentina – mas no inesperada – desaparición del protagonista de La sociedad de los poetas muertos, acogemos como veraz la impresión de Johnson que el comediante muerto por propia mano se despoja así de sus personajes risibles y sus locuras, retirándose la máscara bufa, transmutada, a sus ojos, en una mueca macabra, que debía sentir como una pesada carga, y que su desaparición, sujeta a la poética ironía, se nos muestra como exclusiva del teatro o la gran pantalla, en este caso.</p>
<p>&nbsp;</p>
<p>Pero nuestra natural curiosidad por la desgracia ajena nos lleva a considerar, ya desde una perspectiva de libertad, que el deceso del intérprete de Patch Adams no es, del todo, producto de ese delirio que se manifiesta como una insuperable tristeza, un humor sombrío y una decidida soledad, sino resultado de su propia y personalísima voluntad. Como comprobó el médico y psiquiatra libertario, contracultural y políticamente incorrecto, Thomas Szasz, desde su libro El mito de la enfermedad mental[4] : la mente no es un órgano anatómico como el corazón o el hígado. Por lo tanto, no puede haber, literalmente hablando, enfermedad mental.</p>
<p>&nbsp;</p>
<p>Szasz sostiene que, siendo la mente inasible e intangible, no hay manera real ni científica posible de establecer una relación de causalidad entre ésta y su trastorno; por lo tanto, si la causa del mismo es desconocida, ningún diagnóstico puede, en consecuencia, reflejarlo. Más todavía, las curas empleadas contra las enfermedades mentales se dirigen en realidad a incapacitar neurológicamente al paciente, porque no se puede "curar" un pensamiento, una emoción o una conducta, dado que ésta no puede ser diagnosticada.</p>
<p>&nbsp;</p>
<p>Así, la melancolía es razón necesaria pero no suficiente del suicidio de Williams. Si aceptamos que hubo en él una alteración involuntaria de conducta, o un insalvable conflicto personal, también debe existir esa decisión basada en su libre albedrío, que le hace decidir cuándo irse de este mundo sin dar a nadie explicaciones, ejerciendo así un derecho fundamental, un rasgo ineludible de su condición humana, su "última y definitiva libertad", siguiendo siempre a Szasz.</p>
<p>&nbsp;</p>
<p>Como éste sostiene en su elocuente defensa del derecho de cada persona a elegir una muerte voluntaria, Libertad fatal. Ética y política del suicidio, darse fin es "una protección frente a un destino considerado peor que la muerte [...] es una falacia atribuir el suicidio a las condiciones actuales del sujeto, sea depresión u otra enfermedad o sufrimiento. Quitarse la vida es una acción orientada al futuro, una anticipación, una red de seguridad existencial. La gente ahorra no porque sea pobre, sino para evitar llegar a ser pobre. La gente se suicida no porque sufra, sino para evitar un sufrimiento futuro"[5] .</p>
<p>&nbsp;</p>
<p>Y si de sortear un padecimiento hablamos, si es cierto que la cancelación de su más reciente serie, The crazy ones, gatilló su determinación final, encontramos un problema social más profundo en la desaparición de Williams: el creciente autoritarismo de lo políticamente correcto, denunciado como "pestilente" por Paul Johnson en su libro, donde muchos estilos de humor son censurados, llegando incluso a ser castigados con la prisión. Esta dictadura de los mediocres o subalternos debe ser combatida en nombre de la libertad de reírnos de los demás, puesto que si hay un grupo más valioso que los estadistas, intelectuales y generales, éstos son los humoristas, que nos ayudan a mitigar la agonía de este valle de lágrimas, y nos brindan la posibilidad de ser felices. Y por eso, te damos las gracias, Robin Williams, donde quiera que estés.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>[1] Jurista, poeta y ensayista peruano. Ex Presidente de la Red Liberal de América Latina. Presidente del Instituto de Estudios de la Acción Humana. Autor de Sentido liberal, Libertad para todos, entre otras publicaciones.</p>
<p>[2] Johnson, Paul. Humoristas. Barcelona, Ático de los libros, 2012.</p>
<p>[3] Shakespeare, William. Hamlet. Madrid, En la oficina de Villalpando, traducción de Inarco Celenio, 1798.</p>
<p>[4] Al respecto, ver el artículo La enfermedad mental: un concepto anticuado, de Fernando Luis Gómez Sunday.</p>
<p>[5] Szasz, Thomas. Libertad fatal: ética y política del suicidio. Madrid, Paidós Ibérica, 2002.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Autor: Héctor Ñaupari</p>
<p>Foto: IEAH</p>
<p>Fuente: Texto proporcionado por el autor</p>	  </div>
	  	  
		<div class="clr"></div>

	  
		
	  <!-- Plugins: AfterDisplayContent -->
	  
	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  
	  <div class="clr"></div>
  </div>

		<!-- Social sharing -->
	<div class="itemSocialSharing">

				<!-- Twitter Button -->
		<div class="itemTwitterButton">
			<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">
				K2_TWEET			</a>
			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		</div>
		
				<!-- Facebook Button -->
		<div class="itemFacebookButton">
			<div id="fb-root"></div>
			<script type="text/javascript">
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
		</div>
		
				<!-- Google +1 Button -->
		<div class="itemGooglePlusOneButton">
			<g:plusone annotation="inline" width="120"></g:plusone>
			<script type="text/javascript">
			  (function() {
			  	window.___gcfg = {lang: 'en'}; // Define button default language here
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
		</div>
		
		<div class="clr"></div>
	</div>
	
    <div class="itemLinks">

		
	  
	  
		<div class="clr"></div>
  </div>
  
  
  
	
  
	<div class="clr"></div>

  
  
  
  <!-- Plugins: AfterDisplay -->
  
  <!-- K2 Plugins: K2AfterDisplay -->
  
  
 
		<div class="itemBackToTop">
		<a class="k2Anchor" href="/index.php/biblioteca/item/432-el-suicidio-del-humorista#startOfPageId432">
			arriba		</a>
	</div>
	
	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
";s:4:"head";a:10:{s:5:"title";s:34:"El suicidio del humorista - Relial";s:11:"description";s:158:"A propósito de Robin Williams, el derecho al suicidio y la incorrección política por Héctor Ñaupari[1] &amp;nbsp; De acuerdo con el libro del historia...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:9:{s:8:"keywords";N;s:6:"rights";N;s:5:"title";s:25:"El suicidio del humorista";s:6:"og:url";s:73:"http://relial.org/index.php/biblioteca/item/432-el-suicidio-del-humorista";s:8:"og:title";s:34:"El suicidio del humorista - Relial";s:7:"og:type";s:7:"Article";s:8:"og:image";s:77:"http://relial.org/media/k2/items/cache/8376aace7af18ea8cafa499d7e69a6ec_S.jpg";s:5:"image";s:77:"http://relial.org/media/k2/items/cache/8376aace7af18ea8cafa499d7e69a6ec_S.jpg";s:14:"og:description";s:162:"A propósito de Robin Williams, el derecho al suicidio y la incorrección política por Héctor Ñaupari[1] &amp;amp;nbsp; De acuerdo con el libro del historia...";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:39:"/media/k2/assets/js/jquery-1.8.2.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:36:"/media/k2/assets/js/k2.noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/components/com_k2/js/k2.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:164:"
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});var K2SitePath = '/';";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Biblioteca";s:4:"link";s:20:"index.php?Itemid=134";}i:1;O:8:"stdClass":2:{s:4:"name";s:33:"Ensayos y reflexiones académicas";s:4:"link";s:76:"/index.php/biblioteca/itemlist/category/22-ensayos-y-reflexiones-académicas";}i:2;O:8:"stdClass":2:{s:4:"name";s:25:"El suicidio del humorista";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}