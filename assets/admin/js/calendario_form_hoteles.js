$("#form_modal").submit(function(e) {
     var post_url = $(this).attr("action"); //get form action url
     var request_method = $(this).attr("method"); //get form GET/POST method
     var form_data = $(this).serialize(); //Encode form elements for submission
     var data = new FormData(this);
     e.preventDefault(); // avoid to execute the actual submit of the form.

     $.ajax({
          url : post_url,
          type: request_method,
          data : data,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(data){
               swal(data.mensaje, '', data.tipo);
               $('#registar_hotel').modal('hide');
               get_hoteles();
               // alert(data); // show response from the php script.
          }
     });

});

$('#registar_hotel').on('shown.bs.modal', function (e) {
     showMap2();
})

function showMap2() {
     var lat, lng;
     if($("#lat1").val() && $("#long1").val()){
          lat = $("#lat1").val() ;
          lng = $("#long1").val() ;
     }else{
          lat = '19.432611182986452' ;
          lng = '-99.1332117799011';
     }

     center = new google.maps.LatLng(lat, lng);
     map = new google.maps.Map(document.getElementById('map1'), {
          zoom: 17,
          center: center,
          scrollwheel: false,
          streetViewControl: false,
          mapTypeControl: false,
     });

     marker = new google.maps.Marker({
          position: center,
          map: map,
          draggable: true,
     });

     google.maps.event.addListener(marker, 'dragend', function(){
          changeCenter2(marker.getPosition().lat(), marker.getPosition().lng());
     });
}

function searchLocationNear2() {
     var direccion = $("[name=direccion_hotel]").val();

     var geocoder = new google.maps.Geocoder();
     geocoder.geocode({address: direccion}, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
               changeCenter2(results[0].geometry.location.lat(), results[0].geometry.location.lng());
          }
     });
}

function changeCenter2(latt, lngg) {
   var c = new google.maps.LatLng(latt, lngg);
   //map.setZoom(16);
   map.setCenter(c);
   marker.setPosition(c);
   $("#lat1").val(latt);
   $("#long1").val(lngg);
}

function get_hoteles() {
     $.ajax({
          url: baseUrl+'admin/calendario/get_hoteles_evento',
          type: 'POST',
          dataType: 'json',
          data: {evento_id: $('#evento_id').val()}
     })
     .done(function(respuesta) {
          ver_hoteles(respuesta)
     })
     .fail(function() {
          console.log("error");
     })
     .always(function() {
          console.log("complete");
     });
}

$('#registar_hotel').on('hidden.bs.modal', function () {
     $("[name=hotel_id]").val('');
     $("[name=nombre_hotel]").val('');
     $("#div_img").hide();
     $("#img_logo").attr('src', '');
     $("[name=direccion_hotel]").val('');
     $("[name=tarifa]").val('');
     $("[name=evento_id]").val('');
     $("[name=lat1]").val('');
     $("[name=long1]").val('');
});


function editar_hotel(data) {
     var array = data.split(",");
     $("[name=hotel_id]").val(array[0]);
     $("[name=nombre_hotel]").val(array[1]);
     if (array[2]) {
          $("#div_img").show();
          var img = baseUrl+'uploads/logo_hotel/'+array[2];
          $("#img_logo").attr('src',img);
     }else{
          $("#div_img").hide();
          $("#img_logo").attr('src', '');
     }
     $("[name=direccion_hotel]").val(array[3]);
     $("[name=evento_id]").val(array[4]);
     $("[name=lat1]").val(array[5]);
     $("[name=long1]").val(array[6]);
     $("[name=tarifa]").val(array[7]);

     searchLocationNear2();
     $('#registar_hotel').modal('show');
}

function eliminar_hotel($id) {
     swal({
          title: "Eliminar el hotel",
          text: "¿Desar eliminar el hotel?",
          type: "warning",
          showCancelButton: true,
          showCancelText: "Cancelar",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
     },
     function(){
          swal("Deleted!", "Your imaginary file has been deleted.", "success");
     });
}

function ver_hoteles(data) {
     $("#list_hoteles").html('');
     $.each(data, function( index, value ) {
          var img = value.logo == '' || value.logo == null ? 'https://i.pinimg.com/originals/cb/a4/fd/cba4fdff18fe7e587119fc812a59060e.png' : baseUrl + 'uploads/logo_hotel/'+value.logo;
          var tarif =  value.tarifa != '' && value.tarifa != null ?  '<p><i class="fa fa-map-marker" aria-hidden="true"></i> '+value.tarifa+'</p>' : '';

          $("#list_hoteles").append('<div class="col-md-3">\n\
                                        <div class="card">\n\
                                             <img style="height:250px; border-bottom: 1px solid #ddd;" src="'+img+'" alt="">\n\
                                             <div>\n\
                                                  <h4><i class="fa fa-building" aria-hidden="true"></i> '+value.nombre+'</h4>\n\
                                                  <p><i class="fa fa-map-marker" aria-hidden="true"></i> '+value.ubicacion+'</p>\n\
                                                  '+tarif+'\n\
                                             </div>\n\
                                             <div>\n\
                                                  <a class="btn btn-sm btn-info" onclick="editar_hotel(\''+Object.values(value)+'\')"><i class="fa fa-edit"></i> Editar</a>\n\
                                                  <a class="btn btn-sm btn-danger" onclick="eliminar_hotel('+value.id+')"><i class="fa fa-trash"></i> Eliminar</a>\n\
                                             </div>\n\
                                        </div>\n\
                                   </div>');
     });
}

$( document ).ready(function() {

     if ($('#evento_id').val() != '') {

          get_hoteles();
          var table = $('#myTable').DataTable({
               "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json",
                    'processing': '<div class="spinner"></div>'
               },
               "ajax": baseUrl+'admin/calendario/get_registro/'+$('#evento_id').val()
          });
     }
});


function cambiar_estatus($registro, $estatus) {
     console.log($registro);
     console.log($estatus);
     var mensaj = $estatus == 1 ? 'Aceptar el registro' : 'Negar el registro';
     swal({
          title: mensaj,
          text: "¿Desar continuar?",
          type: "warning",
          showCancelButton: true,
          cancelButtonText: "Cancelar",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Continuar",
          closeOnConfirm: true
     },
     function(){
          $.ajax({
               url:  baseUrl+'admin/calendario/cambiar_estatus/',
               type: 'POST',
               dataType: 'json',
               data: {id: $registro, estatus:$estatus}
          })
          .done(function(data) {
               $('#myTable').DataTable().ajax.reload();
               $('#ver_informacion').modal('hide');
               swal(data.mens, '', data.tipo);
          })
          .fail(function() {
               console.log("error");
          })
          .always(function() {
               console.log("complete");
          });

     });
}

function ver_informacion(id) {
     $('#img_boleto').hide();
     $('#pdf_boleto').hide();
     $.getJSON( baseUrl+'admin/calendario/evento/'+id, function( data ) {
               $('#nombre').text(data.nombre);
               $('#apellidos').text(data.apellidos);
               $('#organizacion').text(data.organizacion);
               $('#pais').text(data.pais);
               $('#correo').text(data.correo);
               $('#telefono').text(data.telefono);
               $('#direccion').text(data.direccion);
               $('#acompanante').text(data.acompanante);
               $('#tipo_asistente').text(data.tipo_asistente == 1 ? 'Ponente' : 'Expectador');
               $('#acompanando').text(data.acompanando == 1 ? 'Si' : 'No');
               $('#local_foraneo').text(data.local_foraneo == 1 ? 'Local' : 'Foraneo');
               if (data.boleto) {
                    $('#div_boleto').show();
                    var arc_bolet = baseUrl+'uploads/boletos/'+data.boleto;
                    console.log(arc_bolet);
                    if (data.tipo == 'pdf') {
                         $('#pdf_boleto').show();
                         $('#pdf_boleto').attr('src', arc_bolet);
                    }else{
                         $('#img_boleto').show();
                         $('#img_boleto').attr('src', arc_bolet);
                    }
               }else{
                    $('#div_boleto').hide();
               }
               // if (!data.estatus) {
                    $('#div_cambiar_estatus').show();
                    $('#btn_apetar').attr('onClick', 'cambiar_estatus('+data.id+',1);');
                    $('#btn_denegar').attr('onClick',  'cambiar_estatus('+data.id+',2);');
                    $('#btn_incompleto').attr('onClick',  'cambiar_estatus('+data.id+',3);');
               // }else {
               //      $('#div_cambiar_estatus').hide();
               // }

     });
     $('#ver_informacion').modal('show');
}
