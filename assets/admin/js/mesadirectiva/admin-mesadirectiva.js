$(document).ready(function() {
    //helper: para mantener la fila de la tabla
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width());
        });
        
        return $helper;
    };
    
    //Make banners table sortable
    $('#mesadirectiva_list tbody').sortable({
        helper: fixHelperModified,
        stop: function(event, ui) {renumber_table('#mesadirectiva_list')}
    }).disableSelection();
    
    //Delete button in table rows
    $('.table').on('click', '.btn-delete', function() {
        r = confirm('Desea eliminar este registro');
        if (r) {
            return true;
        } else {
            return false;
        }
    });
    
    
});

//renumber table rows
function renumber_table(tableId) {
    $(tableId + ' tr').each(function() {
        count = $(this).parent().children().index($(this));
        $(this).find('.priority').html(count);
        $(this).find('.inpt-priority').val(count);
    });
    
    $.post('/admin/mesadirectiva/organizar', $('#frm_mesadirectiva').serialize(), function(data) {
        //console.log(data)
    });
}