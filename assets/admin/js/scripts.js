(function ($) {
    "use strict";
    $.extend({
        debounce: function(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate)
                        func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow)
                    func.apply(context, args);
            };
        }
    });

    
    $(document).ready(function () {
        /*==Left Navigation Accordion ==*/
        if ($.fn.dcAccordion) {
            $('#nav-accordion').dcAccordion({
                eventType: 'click',
                autoClose: true,
                saveState: true,
                disableLink: true,
                speed: 'slow',
                showCount: false,
                autoExpand: true,
                classExpand: 'dcjq-current-parent'
            });
        }
        /*==Slim Scroll ==*/
        if ($.fn.slimScroll) {
            $('.event-list').slimscroll({
                height: '305px',
                wheelStep: 20
            });
            $('.conversation-list').slimscroll({
                height: '360px',
                wheelStep: 35
            });
            $('.to-do-list').slimscroll({
                height: '300px',
                wheelStep: 35
            });
        }
        /*==Nice Scroll ==*/
        if ($.fn.niceScroll) {


            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

            $(".leftside-navigation").getNiceScroll().resize();
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();

            $(".right-stat-bar").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

        }

        /*==Easy Pie chart ==*/
        if ($.fn.easyPieChart) {

            $('.notification-pie-chart').easyPieChart({
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                },
                barColor: "#39b6ac",
                lineWidth: 3,
                size: 50,
                trackColor: "#efefef",
                scaleColor: "#cccccc"

            });

            $('.pc-epie-chart').easyPieChart({
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                },
                barColor: "#5bc6f0",
                lineWidth: 3,
                size:50,
                trackColor: "#32323a",
                scaleColor:"#cccccc"

            });

        }

        /*== SPARKLINE==*/
        if ($.fn.sparkline) {

            $(".d-pending").sparkline([3, 1], {
                type: 'pie',
                width: '40',
                height: '40',
                sliceColors: ['#e1e1e1', '#8175c9']
            });



            var sparkLine = function () {
                $(".sparkline").each(function () {
                    var $data = $(this).data();
                    ($data.type == 'pie') && $data.sliceColors && ($data.sliceColors = eval($data.sliceColors));
                    ($data.type == 'bar') && $data.stackedBarColor && ($data.stackedBarColor = eval($data.stackedBarColor));

                    $data.valueSpots = {
                        '0:': $data.spotColor
                    };
                    $(this).sparkline($data.data || "html", $data);


                    if ($(this).data("compositeData")) {
                        $spdata.composite = true;
                        $spdata.minSpotColor = false;
                        $spdata.maxSpotColor = false;
                        $spdata.valueSpots = {
                            '0:': $spdata.spotColor
                        };
                        $(this).sparkline($(this).data("compositeData"), $spdata);
                    };
                });
            };

            var sparkResize;
            $(window).resize(function (e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(function () {
                    sparkLine(true)
                }, 500);
            });
            sparkLine(false);



        }

//
//        if ($.fn.plot) {
//            var datatPie = [30, 50];
//            // DONUT
//            $.plot($(".target-sell"), datatPie, {
//                series: {
//                    pie: {
//                        innerRadius: 0.6,
//                        show: true,
//                        label: {
//                            show: false
//
//                        },
//                        stroke: {
//                            width: .01,
//                            color: '#fff'
//
//                        }
//                    }
//                },
//                legend: {
//                    show: true
//                },
//                grid: {
//                    hoverable: true,
//                    clickable: true
//                },
//                colors: ["#ff6d60", "#cbcdd9"]
//            });
//        }

        /*==Collapsible==*/
        $('.widget-head').click(function (e) {
            var widgetElem = $(this).children('.widget-collapse').children('i');

            $(this)
                .next('.widget-container')
                .slideToggle('slow');
            if ($(widgetElem).hasClass('ico-minus')) {
                $(widgetElem).removeClass('ico-minus');
                $(widgetElem).addClass('ico-plus');
            } else {
                $(widgetElem).removeClass('ico-plus');
                $(widgetElem).addClass('ico-minus');
            }
            e.preventDefault();
        });




        /*==Sidebar Toggle==*/

        $(".leftside-navigation .sub-menu > a").click(function () {
            var o = ($(this).offset());
            var diff = 80 - o.top;
            if (diff > 0)
                $(".leftside-navigation").scrollTo("-=" + Math.abs(diff), 500);
            else
                $(".leftside-navigation").scrollTo("+=" + Math.abs(diff), 500);
        });



        $('.sidebar-toggle-box .fa-bars').click(function (e) {

            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

            $('#sidebar').toggleClass('hide-left-bar');
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();
            $('#main-content').toggleClass('merge-left');
            e.stopPropagation();
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });
        $('.toggle-right-box .fa-bars').click(function (e) {
            $('#container').toggleClass('open-right-panel');
            $('.right-sidebar').toggleClass('open-right-bar');
            $('.header').toggleClass('merge-header');

            e.stopPropagation();
        });

        $('.header,#main-content,#sidebar').click(function () {
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });


        $('.panel .tools .fa').click(function () {
            var el = $(this).parents(".panel").children(".panel-body");
            if ($(this).hasClass("fa-chevron-down")) {
                $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
                el.slideUp(200);
            } else {
                $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
                el.slideDown(200); }
        });



        $('.panel .tools .fa-times').click(function () {
            $(this).parents(".panel").parent().remove();
        });

        // tool tips

        $('.tooltips').tooltip();

        // popovers

        $('.popovers').popover();

        if ($('.html-ckeditor')[0]) {
            $.each($('.html-ckeditor'), function(index, value) {
                //console.log($('.html-ckeditor')[index].getAttribute('id'));
                var _id = $('.html-ckeditor')[index].getAttribute('id');
                if (_id) {
                    console.log($('#' + _id).data('toolbar'));
                    if ($('#' + _id).data('toolbar') == 'basic') {
                        CKFinder.setupCKEditor(null, '/assets/admin/js/ckfinder/');
                        CKEDITOR.replace(_id, {
                            language: 'es',
                            toolbarGroups: [
                                {name: 'clipboard', groups: ['clipboard', 'undo']},
                                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                                {name: 'links', groups: ['links']},
                                {name: 'forms', groups: ['forms']},
                                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                                {name: 'insert', groups: ['insert']},
                                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                                {name: 'styles', groups: ['styles']},
                                {name: 'colors', groups: ['colors']},
                                {name: 'about', groups: ['about']},
                                {name: 'tools', groups: ['tools']},
                                {name: 'document', groups: ['mode', 'document', 'doctools']},
                                {name: 'others', groups: ['others']}
                            ],
                            removeButtons: 'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Templates,Save,NewPage,Preview,Print,About,ShowBlocks,Anchor,Font,BidiLtr,BidiRtl,Language,Cut,Copy,Paste,PasteText,PasteFromWord,Replace,Find,SelectAll,Scayt,CreateDiv,Blockquote,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,FontSize,BGColor,Strike,Outdent,Indent'
                        });
                    } else {
                        CKFinder.setupCKEditor(null, '/assets/admin/js/ckfinder/');
                        CKEDITOR.replace(_id, {
                            language: 'es',
                            toolbarGroups: [
                                {name: 'clipboard', groups: ['clipboard', 'undo']},
                                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                                {name: 'links', groups: ['links']},
                                {name: 'tools', groups: ['tools']},
                                {name: 'document', groups: ['mode', 'document', 'doctools']},
                                {name: 'forms', groups: ['forms']},
                                '/',
                                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                                {name: 'insert', groups: ['insert']},
                                '/',
                                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                                {name: 'styles', groups: ['styles']},
                                {name: 'colors', groups: ['colors']},
                                {name: 'about', groups: ['about']},
                                {name: 'others', groups: ['others']}
                            ],
                            removeButtons: 'Form,HiddenField,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,About,ShowBlocks,Font,Anchor,Language,BidiRtl,BidiLtr,Save,NewPage,Preview,Print,Templates'
                        });
                    }
                }
            });
        }
        
        //datepicker
        if ($('.date-picker')[0]) {
            $('.date-picker').datepicker({
                locale: 'es',
                format: 'dd-mm-yyyy',
                autoclose: true
            });
        }
        
        if ($('.date-picker2')[0]) {
            $('.date-picker2').datepicker({
                locale: 'es',
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        }
        
        $('.timepicker').timepicker({
            defaultTime: false
        });

        $('.timepicker-24').timepicker({
            autoclose: true,
            minuteStep: 1,
            showMeridian: false
        });
        
        
        

    });
    
})(jQuery);

function generate_alert_message(_type, _msg) {
        var _alert = '';
        var _classname = '';

        switch(_type) {
            case 'danger':
            case 'error':
                _classname = 'alert-danger';
                break;
            case 'success':
                _classname = 'alert-success';
                break;
            case 'info':
            case 'message':
                _classname = 'alert-info';
                break;
            case 'warning':
            default:
                _classname = 'alert-warning';
                break;
        }

        _alert = '<div class="alert '+_classname+' alert-dismissible" role="alert">'
                + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                + _msg + '</div>';

        return _alert;
    }
    