$(document).ready(function() {
    if ($('#dia_completo').is(':checked')) {
        $('.col-calendar').hide();
    }

    $('#dia_completo').click(function() {
        if ($(this).is(':checked')) {
            $('.col-calendar').hide();
        } else {
            $('.col-calendar').show();
        }
    });

    $('form').submit(function() {
        $('.form-actions').hide();
        $('.load-actions').show();
        return true;
    });

    $('.lnk-delete').click(function(e) {
        if (confirm("Desea eliminar este evento?")) {
            return true;
        } else {
            $("body").trigger("click");
            return false;
        }
    });

     showMap();
     if ($("[name=pais_id]").val() != '') {
          get_estado();
     }


});


var center, map, marker;

   function destroyMap(){
       $("#map").empty();
       center = null;
       map = null;
       marker = null;
   }

   function refresamapa(){
       setTimeout('showMap()',1000);
   }

   function intmap() {
       map = new google.maps.Map(document.getElementById('map'), {
         center: {lat: -34.397, lng: 150.644},
         zoom: 8
       });
   }

   function showMap() {
        var lat, lng;
        console.log($("#lat").val());
          console.log($("#lng").val());
        if($("#lat").val() && $("#lng").val()){
             lat = $("#lat").val() ;
             lng = $("#lng").val() ;
        }else{
             lat = '19.432611182986452' ;
             lng = '-99.1332117799011';
        }

        center = new google.maps.LatLng(lat, lng);

        map = new google.maps.Map(document.getElementById('map'), {
             zoom: 17,
             center: center,
             scrollwheel: false,
             streetViewControl: false,
             mapTypeControl: false,
        });

        marker = new google.maps.Marker({
             position: center,
             map: map,
             draggable: true,
        });

        google.maps.event.addListener(marker, 'dragend', function(){
             changeCenter(marker.getPosition().lat(), marker.getPosition().lng());
        });
   }

   function changeCenter(latt, lngg) {
       var c = new google.maps.LatLng(latt, lngg);
       //map.setZoom(16);
       map.setCenter(c);
       marker.setPosition(c);
       $("#lat").val(latt);
       $("#lng").val(lngg);
   }

   function get_estado() {
        var pais_id = $("[name=pais_id]").val();
        var estado_id = $("#estado_id").val();
        $.getJSON(baseUrl +"admin/mapa/get_estados_pais_json/"+pais_id, function( data ) {
             var optString = '<option disabled selected value> -- Estado -- </option>';
             $.each( data, function( key, val ) {
                  var selected = estado_id == val['id'] ? 'selected' : '';
                  optString += '<option '+selected+' value="' + val['id'] + '">' + val['nombre'] + '</option>';
             });
             $('[name=estado_id]').html(optString);
        });
        setTimeout('searchLocationNear()',1000);
   }


   function searchLocationNear() {
        var direccion = $("[name=pais_id] option:selected").text();

        if ($("[name=estado_id] option:selected").val() != '') {
             direccion = direccion+ "," + $("[name=estado_id] option:selected").text();
        }

        if ($("[name=estado_id] option:selected").val() != '') {
             direccion = direccion+ "," + $("[name=direccion]").val();
        }

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({address: direccion}, function (results, status) {
             if (status == google.maps.GeocoderStatus.OK) {
                  changeCenter(results[0].geometry.location.lat(), results[0].geometry.location.lng());
             }
        });
   }

   $('.solo-numero').keyup(function (){
        this.value = this.value.replace(/[^0-9,.]/g, '').replace(/,/g, '.');
   });
