$(function() {
    $('#btn-upload-img').click(function(e) {
        e.preventDefault();
        
        var _img = $('#img_id').val();
        var _principal = $('#img_principal').is(':checked') ? 1 : 0;
        var _mostrar = $('#mostrar').is(':checked') ? 1 : 0;
        var _gal = $('#gal_id').val();
        var _desc = $('#img_desc').val();
        var _url = $('#upload-img').attr('action');
        
        modal_loading_show();
        
        $.ajaxFileUpload({
            url: _url,
            secureuri: false,
            fileElementId: 'img_archivo',
            dataType: 'json',
            data: {
                id: _img,
                galeria: _gal,
                descripcion: _desc,
                principal: _principal,
                mostrar: _mostrar
            },
            success: function(data, status) {
                console.log(data);
                if (data.estatus == 'OK') {
                    if (data.accion == 'edicion') {
                        if (_principal && !data.imagen.principal) {
                            $('#item-'+_img).removeClass('primary-gal');
                        }
                        actualizar_imagen(data.imagen);
                    } else {
                        nueva_imagen(data.imagen);
                    }
                    mostrar_mensaje(data.mensaje, 'success')
                } else {
                    mostrar_mensaje(data.mensaje, 'danger')
                }
                modal_loading_show();
                cerrar_modal();
            },
            error: function (xhr, status) {
                cerrar_modal();
                modal_loading_show();
                console.log(xhr);
                console.log(status);
                alert('Error al subir imagen');
            },
            complete: function () {
                modal_loading_show();
            }
        });
        return false;
    });
    
    $('#modal-imagen').on('hide.bs.modal', function (event) {
        $('#img_src').hide();
        $('#img_src').html('');
        $('#upload-img').trigger("reset");
    });
    
    $('.btn-guardar-gal').click(function(e){
        e.preventDefault();
        
        $.ajax({
            url: $('#form_galeria').attr('action'),
            type: 'post',
            data: $('#form_galeria').serialize(),
            dataType: 'json',   
            beforeSend: function () {
                $('#form_galeria .actions').fadeOut();
                $('.form-loading').fadeIn();
            },
            success: function(data, status) {
                if (data.status != 'ERROR') {
                    $('.mensaje-galeria').html('<div class="alert alert-success alert-dismisable">'
                        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + data.mensaje + '</div>');
                } else {
                    $('.mensaje-galeria').html('<div class="alert alert-danger alert-dismisable">'
                        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + data.mensaje + '</div>');
                }
            },
            error: function () {
                $('.mensaje-galeria').html('<div class="alert alert-danger alert-dismisable">'
                        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + 'ERROR: No fue posible guardar la información debido a un error del sistema. Por favor, vuelva a intentarlo.</div>');
            },
            complete: function () {
                $('.form-loading').fadeOut();
                $('#form_galeria .actions').fadeIn();
            }
        });
    });
    
    
    $('#imgs-galeria').on('click', '.btn-edit', function(e){
        e.preventDefault();
        
        var _item = $(this).data('img');
        var _principal = $(this).data('principal');
        var _mostrar = $(this).data('mostrar');
        
        
        if (_principal == 1) {
            $('#img_principal').attr('checked', true);
        } else {
            $('#img_principal').attr('checked', false);
        }
        
        if (_mostrar == 1) {
            $('#mostrar').attr('checked', true);
        } else {
            $('#mostrar').attr('checked', false);
        }
        
        $('#img_id').val(_item);
        $('#img_desc').val($('#item-'+_item+ ' .desc').text());
        $('#img_src').show();
        $('#img_src').html('<img src="'+($('#item-'+_item+ ' img').attr('src'))+'" style="width:100px;">');
        $('#modal-imagen').modal();
    });
    
    $('#imgs-galeria').on('click', '.btn-delete', function(e){
        e.preventDefault();
        var _img = $(this).data('img');
        
        var resp = confirm("Desea realmente eliminar esta imagen?");
        if (resp) {
            $.ajax({
                url: '/admin/galerias/eliminar_imagen/' + _img,
                type: 'get',
                dataType: 'json',                
                success: function(data, status) {
                    if (data.status != 'ERROR') {
                        mostrar_mensaje('La imagen fue eliminada', 'success');
                        $('#item-' + _img).remove();
                    } else {
                        mostrar_mensaje('La imagen no pudo ser eliminada', 'danger');
                    }
                }
            });
        }
        
    });
});

function modal_loading_show() {
    if ($('.modal-loading').is(":visible")) {
        $('.modal-loading').fadeOut();
        $('.modal-btn-actions').fadeIn();
    } else {
        $('.modal-btn-actions').fadeOut();
        $('.modal-loading').fadeIn();
    }
}

function cerrar_modal() {
    $('#upload-img').trigger("reset");
    $('#img_src').html();
    $('#img_src').hide();
    $('#modal-imagen').modal('hide');
}

function nueva_imagen(_info) {
    if (_info.principal) {
        $('#imgs-galeria .block-img').removeClass('primary-gal');
    }
    
    var _elem = '<div class="col-sm-6 col-md-3 block-img ' + (_info.principal ? 'primary-gal' : '') + '" id="item-' + _info.id + '">'
                + '<div class="thumbnail">'
                    + '<img src="' + _info.path_url + '/' + _info.archivo + '" alt="">'
                    + '<div class="caption">'
                        + '<p class="desc">' + _info.descripcion + '</p>'
                        + '<div class="m-b-5 text-center">'
                            + '<button type="button" data-img="' + _info.id + '" class="btn btn-primary m-r-5 btn-edit"><i class="fa fa-edit"></i></button>'
                            + '<button type="button" data-img="' + _info.id + '" class="btn btn-danger btn-delete"><i class="fa fa-trash-o"></i></button>'
                        + '</div>'
                    + '</div>'
                + '</div>'
            + '</div>';
    
    $('#imgs-galeria').prepend(_elem);
}


function actualizar_imagen(_info) {
    if (_info.principal) {
        $('#imgs-galeria .block-img').removeClass('primary-gal');
        $('#item-' + _info.id).addClass('primary-gal');
    }
    
    $('#item-' + _info.id + ' img').attr('src', _info.path_url + '/' + _info.archivo);
    $('#item-' + _info.id + ' .desc').html(_info.descripcion);
    $('#item-' + _info.id + '.btn-edit').data('principal', _info.principal);
}

function actualizar_imagenes(_evento) {
    $.get(_root_url + 'galerias/imagenes/'+_evento).success(function(data) {
        $('#files').html(data);
    });
}

function mostrar_mensaje(message, type){
    var msg = generate_alert_message(type, message);
    $('.alerts-box').html(msg);
}

jQuery.extend({
    handleError: function( s, xhr, status, e ) {
        // If a local callback was specified, fire it
        if ( s.error )
            s.error( xhr, status, e );
        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
        else if(xhr.responseText)
            console.log(xhr.responseText);
    }
});