$("#form_banner").submit(function(e) {
     var post_url = $(this).attr("action"); //get form action url
     var request_method = $(this).attr("method"); //get form GET/POST method
     var form_data = $(this).serialize(); //Encode form elements for submission
     var data = new FormData(this);
     e.preventDefault(); // avoid to execute the actual submit of the form.

     $.ajax({
          url : post_url,
          type: request_method,
          data : data,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(data){

               $('#modal_form_banner').modal('hide');

               swal({title: data.mensaje, text: '', type: data.tipo},
                  function(){
                      location.reload();
                  }
               );
               // alert(data); // show response from the php script.
          }
     });

});


$('#modal_form_banner').on('hidden.bs.modal', function () {
     $('#img_banner').attr('src', '');
     $('#banner_id').val('');
     $('#m_titulo').val('');
     $('#m_correo').val('');
     $('#m_telefono').val('');
     $('#m_descripcion').text('');
     $('#m_link').val('');
     $('#pais_id').val('');
     $('#m_nueva_ventana').prop('checked', false);
     $('#m_estatus').prop('checked', false);

});


function editar_banner(data) {
     var obj = JSON.parse(data);
     console.log(obj);
     $('#img_banner').attr('src', '/uploads/banners/'+obj.imagen);
     $('#banner_id').val(obj.id);
     $('#m_titulo').val(obj.titulo);
     $('#m_correo').val(obj.mail);
     $('#m_telefono').val(obj.telefono);
     $('#m_descripcion').text(obj.descripcion);
     $('#pais_id').val(obj.pais_id);
     $('#m_link').val(obj.link);
     if (obj.nueva_ventana == 1) {
          $('#m_nueva_ventana').prop('checked', true);
     }
     if (obj.estatus == 1) {
          $('#m_estatus').prop('checked', true);
     }

     $('#modal_form_banner').modal('show');
}


function eliminar_directorio($id) {
     swal({
          title: "Eliminar el directorio",
          text: "¿ Desar eliminar el directorio ?",
          type: "warning",
          showCancelButton: true,
          showCancelText: "Cancelar",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
     },
     function(){
          $.ajax({
               url: '/admin/directorio/eliminar_banner/'+$id,
               type: 'GET',
               dataType: 'json'
          })
          .done(function(data) {
               swal({title: data.mensaje, text: '', type: data.tipo},
                  function(){
                      location.reload();
                  }
               );
          })
          .fail(function() {
               console.log("error");
          })
          .always(function() {
               console.log("complete");
          });

     });
}
