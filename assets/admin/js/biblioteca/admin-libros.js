$(document).ready(function() {
    $('#filter-table-data').on('click', '.eliminar', function() {
        if (confirm('¿Desea eliminar el libro seleccionado?')) {
            return true;
        } else {
            return false;
        }
    });
    
    if($('.destacar_libro').length > 0){
            $('body').delegate('.destacar_libro','change',function(){
                var id = $(this).val();
                $.ajax({
                    url: baseUrl+'admin/biblioteca/update_destacado',
                    type: 'POST',
                    data: {id:id},
                    success: function (r) {
                        console.log(r);
                    },
                    error: function (r, e) {
                        console.log(r, e);
                    }
                });
            });
            
        }
});