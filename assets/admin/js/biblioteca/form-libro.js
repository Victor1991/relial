$(document).ready(function() {
    var maxLen = $('#descripcion').data('max-len');
    $('#descripcion').maxlength({
        feedbackText: '{r} caracteres restantes ({m} máximo)',
        max: maxLen
    });
    
    $("#btn-guardar").click(function (e) {
        show_load_submit();
    });
    
    
    $('#flipbook').change(function(){
        
        if($(this).is(':checked')){
            $('.module_upload_flipbook').slideDown(400);
        }else{
            $('.module_upload_flipbook').slideUp(400);
        }
        
    });
    
});

function show_load_submit() {
    $('.form-actions').hide();
    $('.form-loading').show();
}



