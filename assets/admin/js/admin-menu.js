$(document).ready(function () {
    var item_edited;
    var displayFormOptions = function(type, form) {
        $(form + ' .type-page').addClass('hidden');
        console.log(form + ' .type-page-url');
        switch (type) {
            case 'url':
                $(form + ' .type-page-url').removeClass('hidden');
                break;
            case 'uri': 
                $(form + ' .type-page-uri').removeClass('hidden');
                break;
            case 'pagina':
                $(form + ' .type-page-page').removeClass('hidden');
                break;
        }
    };
    
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target);
        $('#inp-menu-txt').val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
    };
    
    $('body').on('change', '.rd-btn-type', function() {
        var val = $(this).val();
        
        if($(this).is(':checked')) {
            var form = $(this).closest('form').attr('id'); 
            displayFormOptions(val, '#'+form);
        }
    });
    
    $('#nestable_list_3').nestable({
        group: 1,
        maxDepth: 3
    }).on('change', updateOutput);
        
    
    $('#add-item-list').click(function() {
        $.ajax({
            url: '/admin/menus/crear_link',
            data: $('#form_nuevo').serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
                $('.load-btn-actions').show();
                $('.modal-btn-actions').hide();
            },
            success: function(data) {
                if (data.estatus == 'ok') {
                    var _item = '<li class="dd-item dd3-item" data-id="'+data.item.id+'">'
                            + '<div class="dd-handle dd3-handle"></div>'
                            + '<div class="dd3-content">'
                            + '<a href="#" class="dd3-action-detail">'+data.item.titulo+'</a>'
                            +'</div>'
                        + '</li>';
                    $('#nestable_list_3 ol:first').append(_item);
                    $('#main-content .wrapper').prepend(generate_alert_message('success', data.mensaje));
                } else {
                    $('#main-content .wrapper').prepend(generate_alert_message('error', data.mensaje));
                }
            },
            error: function(xhr, status) {
                alert('Ocurrio un problema al realizar la petición');
            },
            complete: function(xhr, status) {
                $('.load-btn-actions').hide();
                $('.modal-btn-actions').show();
            }
        });
    });
    
    $('#nestable_list_3').on('click', '.dd3-action-detail', function() {
        item_edited = $(this);
        var item = $(this).closest('.dd-item').data('id');
        $('#modal-edit-item .modal-body').html('<div class="load"><img src="/assets/admin/images/circular_load.GIF"></div>');
        $.get('/admin/menus/editar/'+item, {}, function(resp) {
            $('#modal-edit-item .modal-body').html(resp);
        });
        
        $('#modal-edit-item').modal();
        
    });
    
    $('.btn-editar-guardar').click(function() {
        var data = $('#form_editar').serialize();
        var item = $('#form_editar [name="id_enlace"]').val();
        $('.modal-footer .load-btn-actions').show();
        $('.modal-footer .modal-btn-actions').hide();
        $.post('/admin/menus/editar/'+item, data, function(resp) {
            //$('#modal-edit-item .modal-body').html(resp);
            var _msg = '';
            if (resp.estatus == 'ok') {
                $('#modal-edit-item').modal('hide');
                item_edited.text($('#form_editar [name="titulo"]').val());
                _msg = generate_alert_message('success', resp.mensaje);
                $('#main-content .wrapper').prepend(_msg);
            } else {
                _msg = generate_alert_message('error', resp.mensaje);
                $('#modal-edit-item .modal-body').prepend(_msg);
            }
        }, 'json');
    });
    
    $('#btn-save-menu').click(function() {
        var _params = $('#inp-menu-txt').val();
        
        if (_params.length < 5) {
            return false;
        }
        
        $.ajax({
            url: '/admin/menus/order',
            data: {params: _params},
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
                $('.load-save-menu').show();
                $('#btn-save-menu').hide();
            },
            success: function(data) {
                if (data.estatus == 'ok') {
                    $('#main-content .wrapper').prepend(generate_alert_message('success', data.mensaje));
                } else {
                    $('#main-content .wrapper').prepend(generate_alert_message('error', data.mensaje));
                }
            },
            error: function(xhr, status) {
                alert('Ocurrio un problema al realizar la petición');
            },
            complete: function(xhr, status) {
                $('.load-save-menu').hide();
                $('#btn-save-menu').show();
            }
        });
    });
    
    $('.btn-editar-delete').click(function() {
        var item = $('#form_editar [name="id_enlace"]').val();
        if (confirm("Si el link actual tiene hijos, estos también serán eliminados.\n ¿Desea eliminar el link actual del menú?")) {
            $.post('/admin/menus/eliminar/', {item: item}, function(resp) {
                var _msg = '';
                if (resp.estatus == 'ok') {                    
                    item_edited.closest('.dd-item').remove();
                    $('#main-content .wrapper').prepend(generate_alert_message('success', resp.mensaje));
                } else {
                    $('#main-content .wrapper').prepend(generate_alert_message('error', resp.mensaje));
                }
                $('#modal-edit-item').modal('hide');
            }, 'json');
        } 
    });
    
});