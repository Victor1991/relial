$(document).ready(function () {
   $('.institucion-panel').click(function(){
       var clave = $(this).data('clave');
       var $parent = $(this).closest('#'+clave);
       if($parent.find('.panel-body').is(':visible')){
           $(this).find('i').attr('class','fa fa-caret-right');
           $parent.find('.panel-body').slideUp(400);
       }else{
           $(this).find('i').attr('class','fa fa-caret-down');
           $parent.find('.panel-body').slideDown(400);
       }
   }); 
});

function nueva_institucion(nombre,clave,id){
    $('#nombre').val(nombre);
    $('#clave').val(clave);
    $('#form-instituciones').attr('action',baseUrl+'admin/mapa/index/'+id);
    $('#modalInstituciones').modal('show');
}

function edit_institucion(institucion,nombre,clave,id,nombre_institucion,web){
    $('#nombre').val(nombre);
    $('#clave').val(clave);
    $('#nombre-institucion').val(nombre_institucion);
    $('#web').val(web);
    $('#form-instituciones').attr('action',baseUrl+'admin/mapa/index/'+id+'/'+institucion);
    $('#modalInstituciones').modal('show');
}

function delete_institucion(id){
    swal({
        title: "Eliminar registro.",
        text: "¿Desea eliminar esta institución?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: 'green'
    }, function () {
        window.location = baseUrl+'admin/mapa/delete/'+id;
    });
}