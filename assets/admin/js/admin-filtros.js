$(function() {

    fnFiltros = {
        $content: $('#filter-table-data'),
        $filtro_form: $('#filters form'),
        $load: $('#filter-load'),
        f_modulo: $('input[name="f_modulo"]').val(),
        /**
         * Constructor
         */
        init: function() {
            $('a.cancel').button();

            //listener for select elements
            $('select', fnFiltros.$filtro_form).on('change', function() {
                //build the form data
                form_data = fnFiltros.$filtro_form.serialize();
                //fire the query
                fnFiltros.aplicar_filtro(fnFiltros.f_modul0, form_data);
            });

            //listener for keywords
            $('input[type="text"]', fnFiltros.$filtro_form).on('keyup', $.debounce(function() {
                //build the form data
                form_data = fnFiltros.$filtro_form.serialize();

                fnFiltros.aplicar_filtro(fnFiltros.f_modulo, form_data);
            }, 500));

            //listener for pagination
            $('body').on('click', '.pagination a', function(e) {
                e.preventDefault();
                url = $(this).attr('href');
                form_data = fnFiltros.$filtro_form.serialize();
                fnFiltros.aplicar_filtro(fnFiltros.f_modulo, form_data, url);
            });

            //clear filters
            $('a.cancel', fnFiltros.$filtro_form).click(function(e) {
                e.preventDefault();
                //reset the defaults
                //$('select', filter_form).children('option:first').addAttribute('selected', 'selected');
                $('select', fnFiltros.$filtro_form).val('0');

                //clear text inputs
                $('input[type="text"]').val('');

                //build the form data
                form_data = fnFiltros.$filtro_form.serialize();

                fnFiltros.aplicar_filtro(fnFiltros.f_modulo, form_data);
            });

            //prevent default form submission
            fnFiltros.$filtro_form.submit(function(e) {
                e.preventDefault();
            });

            // trigger an event to submit immediately after page load
            //fnFiltros.$filtro_form.find('select').first().trigger('change');
        },
        //launch the query based on modulo
        aplicar_filtro: function(modulo, form_data, url) {
            form_action = fnFiltros.$filtro_form.attr('action');
            post_url = form_action ? form_action : SITE_URL + 'admin/' + modulo;

            if (typeof url !== 'undefined') {
                post_url = url;
            }

            //limpiar_notificaciones();
            fnFiltros.$load.fadeIn();
            fnFiltros.$content.fadeOut('fast', function() {
                //send the request to the server
                $.post(post_url, form_data, function(data, response, xhr) {

                    var ct = xhr.getResponseHeader('content-type') || '',
                            html = '';

                    if (ct.indexOf('application/json') > -1 && typeof data == 'object') {
                        html = 'html' in data ? data.html : '';

                        fnFiltros.handler_response_json(data);
                    }
                    else {
                        html = data;
                    }

                    //success stuff here
                    //pyro.chosen();
                    fnFiltros.$content.html(html).fadeIn('fast');
                    fnFiltros.$load.fadeOut();
                });
            });
        },
        handler_response_json: function(json) {
            if ('update_filter_field' in json && typeof json.update_filter_field == 'object')
            {
                $.each(json.update_filter_field, fnFiltros.update_filter_field);
            }
        },
        update_filter_field: function(field, data) {
            var $field = fnFiltros.$filtro_form.find('[name=' + field + ']');

            if ($field.is('select')) {
                if (typeof data == 'object') {
                    if ('options' in data) {
                        var selected, value;

                        selected = $field.val();
                        $field.children('option').remove();

                        for (value in data.options) {
                            $field.append('<option value="' + value + '"' + (value == selected ? ' selected="selected"' : '') + '>' + data.options[value] + '</option>');
                        }
                    }
                }
            }
        }
    };

    fnFiltros.init();

});