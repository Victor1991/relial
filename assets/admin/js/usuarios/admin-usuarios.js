$(document).ready(function() {
    $('#filter-table-data').on('click', '.eliminar', function(e) {
        e.preventDefault();
        var _conf = confirm("¿Desea eliminar este usuario?");
        if (_conf) {
            location.href=$(this).attr('href');
        }
        
        return false;
    });
});