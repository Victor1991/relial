<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Recuperar contraseña</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
            <tr>
                <td align="center" style="padding: 40px 0 30px 0;">
                    <img src="<?php echo admin_assets('img/logo-login.png'); ?>" alt="Logo" style="display: block;" />
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px; border: 1px solid #DDD;">
                    <h3>Recuperar contraseña</h3>
                    <p>En respuesta a tu petición, te hacemos llegar un enlace con el que podrás reestablecer tu contraseña de <?php echo $sitio; ?>:</p>
                    <p style="text-align: center;">
                        <a href="<?php echo $link; ?>" style="text-decoration:none; color: #FFF; background-color: #666; padding:10px 16px;
                           font-weight:bold; margin-right:10px; text-align:center; cursor:pointer; display: inline-block;">
                            Recuperar contraseña
                        </a>
                    </p>
                    <p>Si no te funciona el enlace superior, copia y pega el siguiente texto en la barra de direcciones de tu navegador:</p>
                    <p><a href="<?php echo $link; ?>"><?php echo $link; ?></a></p>
                </td>
            </tr>
            <tr>
                <td style="font-size: 10px; color: #888;">En caso de que no hayas solicitado la recuperación de tu contraseña, haz caso omiso a este correo electrónico. No es necesario que respondas este correo. El mismo ha sido generado por un sistema automático de envío.</td>
            </tr>
        </table>
    </body>
</html>
