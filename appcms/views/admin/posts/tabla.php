<table id="tbl-posts" class="table table-striped">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Categoría</th>
            <th>Autor</th>
            <th>Fecha</th>
            <th>Estatus</th>
            <th class="col-sm-1 text-center"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($posts as $post): ?>
            <tr>
                <td>
                    <a href="<?php echo base_url('admin/posts/form/'.$post->id); ?>">
                        <?php echo $post->titulo; ?>
                    </a>
                </td>
                <td><?php echo $post->categoria_nombre; ?></td>
                <td><?php echo $post->autor_nombre; ?></td>
                <td><?php echo mysql_to_date($post->fecha); ?></td>
                <td><?php echo $post->estatus ? '<span class="c-green">publicado</span>' : '<span class="c-red">borrador</span>' ; ?></td>
                <td>
                    <a href="<?php echo base_url('admin/posts/eliminar/'.$post->id); ?>" class="btn btn-danger btn-sm eliminar" alt="eliminar" title="eliminar">
                        <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>                        
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6"><?php echo $pagination['links']; ?></td>
        </tr>
    </tfoot>
</table>

