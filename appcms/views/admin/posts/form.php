<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        
        <?php show_alerts(); ?>
        
        <form method="post" action="<?php echo base_url('admin/posts/form/' . $id); ?>" id="form_post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-8">
                    <section class="panel">
                        <header class="panel-heading">
                            <?php echo $titulo_form; ?>                       
                        </header>
                        <div class="panel-body">                            
                            <div class="form-group">
                                <label>Titulo</label>
                                <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Titulo" value="<?php echo set_value('titulo', $titulo); ?>">
                            </div>
                             
                            <div class="form-group">
                                <label>Resumen</label>
                                <textarea rows="5" data-max-len="500" name="resumen" id="resumen" class="form-control"><?php echo set_value('resumen', $resumen); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Contenido</label>
                                <textarea rows="10" class="form-control html-ckeditor" name="contenido" id="contenido"><?php echo set_value('contenido', $contenido); ?></textarea>
                            </div>   
                        </div>
                    </section>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Estatus</label>
                                <?php echo form_dropdown('estatus', $estatus_dpdwn, $estatus, 'id="estatus" class="form-control"') ?>
                            </div>
                            <div class="form-group">
                                <label>Categoría</label>
                                <?php echo form_dropdown('categoria', $categorias, $categoria_id, 'id="categoria" class="form-control"') ?>
                            </div>
                            <div class="form-group">
                                <label>Fecha</label>
                                <div class="input-group">
                                    <input type="text" name="fecha" id="fecha" readonly="" value="<?php echo set_value('fecha', $fecha); ?>" class="form-control date-picker">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>   
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <?php echo form_checkbox('evento', 1, (bool)$evento); ?>&nbsp;Marcar como evento.
                                    </label>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <label>Imagen principal</label>
                                <input type="file" name="imagen" id="imagen" data-width="" data-height="">
                                <?php if ($id && $imagen != ''): ?>
                                    <div style="text-align:center; padding:5px; border:1px solid #ccc;margin-top: 10px;">
                                        <img src="<?php echo base_url('uploads/posts/' . $imagen); ?>" alt="imagen actual" class="img-responsive">
                                        <br/>Imagen actual
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <div class="form-actions m-t-20">
                                    <div class="row">
                                        <div class="col-sm-6 m-b-5">
                                            <a href="<?php echo base_url('admin/posts'); ?>" class="btn btn-default btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-sm-6 m-b-5">
                                            <button type="submit" class="btn btn-primary btn-block" id="btn-guardar">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-loading text-center" style="display:none">
                                    <img src="/assets/admin/img/circular_load.GIF">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- page end-->
    </section>
</section>
<!--main content end-->        