<section id="main-content">
     <section class="wrapper">
          <!-- page start-->
          <?php show_alerts(); ?>
          <section class="panel">
               <header class="panel-heading">
                    Evento
               </header>
               <div class="panel-body">
                    <input type="hidden" id="evento_id" value="<?=$evento_id?>">
                    <form method="post" action="<?php echo base_url('admin/calendario/form_calendario/'.$evento_id); ?>" class="form-horizontal" enctype="multipart/form-data">
                         <div class="form-group">
                              <label class="control-label col-sm-2">Título</label>
                              <div class="col-sm-10">
                                   <input type="text" name="titulo" id="titulo" class="form-control" value="<?php echo $titulo; ?>" placeholder="Evento sin título">
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-sm-2">Inicio</label>
                              <div class="col-sm-3">
                                   <input type="text" name="fecha_inicio" id="fecha_inicio" class="form-control date-picker" value="<?php echo $fecha_inicio; ?>" placeholder="dd-mm-yyyy">
                              </div>
                              <div class="col-sm-2 col-calendar">
                                   <div class="input-group bootstrap-timepicker">
                                        <input type="text" name="hora_inicio" id="hora_inicio" class="form-control timepicker" value="<?php echo $hora_inicio; ?>" placeholder="00:00">
                                        <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                        </span>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-sm-2">Fin</label>
                              <div class="col-sm-3">
                                   <input type="text" name="fecha_fin" id="fecha_fin" class="form-control date-picker" value="<?php echo $fecha_fin; ?>" placeholder="dd-mm-yyyy">
                              </div>
                              <div class="col-sm-2 col-calendar">
                                   <div class="input-group bootstrap-timepicker">
                                        <input type="text" name="hora_fin" id="hora_fin" class="form-control timepicker" value="<?php echo $hora_fin; ?>" placeholder="00:00">
                                        <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                        </span>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <div class="col-sm-10 col-sm-offset-2">
                                   <div class="checkbox m-b-15">
                                        <label>
                                             <input type="checkbox" name="dia_completo" id="dia_completo" value="1" <?php echo $dia_completo ? 'checked' : ''; ?>>
                                             <i class="input-helper"></i>
                                             Todo el día
                                        </label>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-sm-2">Imagen<span class="help-block">(400x450px)</span></label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <input type="file" name="imagen" id="imagen">
                                        <?php if ($imagen != ''): ?>
                                             <div style="text-align:center; padding:5px; border:1px solid #ccc;margin-top: 10px;">
                                                  <img src="<?php echo base_url('uploads/eventos/' . $imagen); ?>" alt="imagen actual" class="img-responsive">
                                                  <br/>Imagen actual
                                             </div>
                                        <?php endif; ?>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-sm-2">Descripción</label>
                              <div class="col-sm-10">
                                   <textarea rows="5" name="descripcion" id="descripcion" class="form-control"><?php echo $descripcion; ?></textarea>
                              </div>
                         </div>
                         <div class="form-group">
                              <div class="col-sm-10 col-sm-offset-2">
                                   <div class="form-actions">
                                        <a href="<?php echo base_url('admin/calendario'); ?>" class="btn btn-default">Cancelar</a>
                                        <button type="submit" name="btn-submit" class="btn btn-primary">Guardar</button>
                                   </div>
                                   <img class="load-actions pull-right" src="<?php echo base_url('assets/admin/img/circular_load.GIF'); ?>" style="display: none;">
                              </div>
                         </div>
                    </form>


               </div>
          </section>
     </section>
</section>
