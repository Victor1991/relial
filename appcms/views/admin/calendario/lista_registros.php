<div class="table-responsive">
    <table class="table table-striped table-bordered" style="width:100%" id="myTable">
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre(s)</th>
          <th>Apellido(s)</th>
          <th>Organización</th>
          <th>País</th>
          <th>Correo</th>
          <th>Teléfono</th>
          <th>Estatus</th>
          <th></th>
        </tr>
      </thead>
    </table>
</div>



<div class="modal fade" id="ver_informacion" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Información del usuario</h4>
               </div>
               <div class="modal-body">
                    <div class="col-md-4">
                         <h4 class="tit_mod">Nombre(s)</h4>
                         <span id="nombre"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">Apellido(s)</h4>
                         <span id="apellidos"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">Organización</h4>
                         <span id="organizacion"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">País</h4>
                         <span id="pais"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">Correo</h4>
                         <span id="correo"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">Teléfono</h4>
                         <span id="telefono"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">Ponente o expectador</h4>
                         <span id="tipo_asistente"></span>
                    </div>
                    <div class="col-md-8">
                         <h4 class="tit_mod">Dirección</h4>
                         <span id="direccion"></span>
                    </div>
                    <div class="col-md-4">
                         <h4 class="tit_mod">Acompañado</h4>
                         <span id="acompanando"></span>
                    </div>
                    <div class="col-md-8">
                         <h4 class="tit_mod">Acompañante</h4>
                         <span id="acompanante"></span>
                    </div>
                    <div class="col-md-12">
                         <h4 class="tit_mod">Local / Foraneo</h4>
                         <span id="local_foraneo"></span>
                    </div>

                    <div class="col-md-12 text-center" id="div_boleto" >
                         <h4 class="tit_mod">Boleto</h4>
                         <img src="" alt="" id="img_boleto" style="display:none;">
                         <iframe src="" width="850" height="500" frameborder="0" allowfullscreen id="pdf_boleto"  style="display:none;"></iframe>
                    </div>

               </div>
               <div class="clearfix">
               </div>
               <br>
               <br>
               <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                    <div id="div_cambiar_estatus">
                         <button type="button" id="btn_apetar" class="btn btn-success">Aceptar</button>
                         <button type="button" id="btn_incompleto" class="btn btn-warning">Información incompleta</button>
                         <button type="button" id="btn_denegar" class="btn btn-danger">Denegar</button>
                    </div>
               </div>
          </div>
     </div>
</div>
