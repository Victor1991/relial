<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Calendario
                <span class="tools pull-right">
                    <a href="<?php echo base_url('admin/calendario/form'); ?>" class="btn btn-info btn-sm" >
                        <i class="fa fa-plus-circle"></i> Nuevo evento
                    </a>
                    <a href="<?php echo base_url('admin/calendario/form_calendario'); ?>" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus-circle"></i> Programar en calendario
                    </a>
                </span>
            </header>
            <div class="panel-body">
                <div id="calendar" class="has-toolbar"></div>
            </div>
        </section>
    </section>
</section>
