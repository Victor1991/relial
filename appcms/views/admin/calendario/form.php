<section id="main-content">
     <section class="wrapper">
          <!-- page start-->
          <?php show_alerts(); ?>
          <section class="panel">
               <header class="panel-heading">
                    Evento
                    <span class="pull-right">
                         <ul class="nav panel-tabs" style="bottom: -5px;">
                              <li class="active"><a href="#tab1" data-toggle="tab">Información</a></li>
                              <?php if($evento_id): ?>
                                   <li><a href="#tab2" data-toggle="tab">Hoteles</a></li>
                                   <!-- <li><a href="#tab3" data-toggle="tab">Galeria</a></li> -->
                                   <li><a href="#tab4" data-toggle="tab">Registros</a></li>
                              <?php endif; ?>
                         </ul>
                    </span>
               </header>
               <div class="panel-body">
                    <input type="hidden" id="evento_id" value="<?=$evento_id?>">
                    <div class="tab-content">
                         <div class="tab-pane active" id="tab1">



                              <?php $this->load->view('admin/calendario/form_include') ?>


                         </div>
                         <div class="tab-pane" id="tab2">
                              <?php $this->load->view('admin/calendario/form_hoteles_include') ?>
                         </div>
                         <!-- <div class="tab-pane" id="tab3">

                         </div> -->
                         <div class="tab-pane" id="tab4">
                              <?php $this->load->view('admin/calendario/lista_registros') ?>

                         </div>

                    </div>

               </div>
          </section>
     </section>
</section>
