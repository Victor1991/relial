<div class="row">
     <div class="col-xs-12">
          <div class="panel panel-info">
               <div class="panel-heading">
                    <div class="panel-title">
                         <div class="row">
                              <div class="col-xs-6">
                                   <h5><i class="fa fa-building" aria-hidden="true"></i> Hoteles registrados</h5>
                              </div>
                              <div class="col-xs-6">
                                   <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#registar_hotel">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Registrar Hotel
                                   </button>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="panel-body">
                    <div class="row" id="list_hoteles">




		         </div>

               </div>

          </div>
     </div>
</div>


<div class="modal fade" id="registar_hotel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog">
          <div class="modal-content">
               <form method="post" action="<?=base_url('admin/calendario/guardar_hotel')?>" id="form_modal" enctype="multipart/form-data" >
                    <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Información Hotel</h5>
                    </div>
                    <div class="modal-body">
                         <input type="hidden" name="hotel_id" value="">
                         <input type="hidden" name="evento_id" value="<?=$evento_id?>">
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Nombre Hotel:</label>
                                        <input type="text" required class="form-control" id="nombre_hotel" placeholder="Hotel" name="nombre_hotel">
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Tarifa:</label>
                                        <input type="text" required class="form-control" id="tarifa" placeholder="Tarifa" name="tarifa">
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Logo</label>
                                        <div class="fg-line">
                                             <input type="file" name="imagen" id="imagenlogo">
                                             <div id="div_img" style="text-align:center; display: none; padding:5px; border-radius: 5px; border:1px solid #ccc;margin-top: 10px;">
                                                  <img id="img_logo" src="" alt="imagen actual" class="img-responsive" style="margin: 0 auto;">
                                                  <br/>Imagen actual
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Dirección:</label>
                                        <input type="text" required class="form-control" id="direccion_hotel" placeholder="Dirección" name="direccion_hotel" onkeyup="searchLocationNear2()">
                                        <br>
                                        <div class="col-sm-12">
                                             <div class="row">
                                                  <input type="hidden" name="lat1" id="lat1" value="">
                                                  <input type="hidden" name="long1" id="long1" value="">

                                                  <div id="map1" style="width: 100%; height: 350px; border-radius: 5px;"></div>
                                             </div>

                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                         <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
               </form>
          </div>
     </div>
</div>

<style media="screen">


/* Cards */
.posts-box {
  max-width: 1024px;
  margin: 0 auto;
  padding: 40px 10px;
  text-align: center;
  list-style: none;
}

.posts-box li {
  display: inline-block;
  width: 320px;
  max-width: 100%;
  padding: 10px;
  vertical-align: top;
}

.card {
     margin-bottom: 20px;
  text-align: left;
  border-radius: 2px;
  background: #FFFFFF;
  box-shadow:
    0 2px 4px rgba(0,0,0,0.10),
    0 1px 2px rgba(0,0,0,0.22);
}

.card img {
  display: block;
  width: 100%;
}

.card div {
  padding: 16px 20px;
  border-bottom: 1px solid #EEEEEE;
}

.card div h3 {
  margin: 8px 0;
  font-size: .9em;
}

.card div p {
  margin: 8px 0;
  font-size: .9em;
}

.button {
  color: #FFC107;
  text-transform: uppercase;
  text-decoration: none;
  font-size: .8em;
}

.button:hover {
  opacity: .7;
}


</style>
