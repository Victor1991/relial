<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <?php show_alerts(); ?>
        <div class="row">
            <div class="col-sm-4">                
                <section class="panel">
                    <header class="panel-heading">
                        Nuevo enlace
                    </header>
                    <div class="panel-body">
                        <form role="form" id="form_nuevo">
                            <div class="form-group">
                                <label>Tipo de enlace</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_enlace" value="url" class="rd-btn-type" checked>
                                        <i class="input-helper"></i>
                                        Enlace externo (URL)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_enlace" value="uri" class="rd-btn-type">
                                        <i class="input-helper"></i>
                                        Enlace del sitio (URI)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_enlace" value="pagina" class="rd-btn-type">
                                        <i class="input-helper"></i>
                                        Página
                                    </label>
                                </div>
                            </div>
                            <div class="form-group fg-line">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control input-sm" id="link-titulo">
                            </div>
                            <div class="form-group fg-line type-page type-page-url">
                                <label>URL</label>
                                <input type="text" name="url" class="form-control input-sm" id="link-url" placeholder="http://ejemplo.com/">
                            </div>
                            <div class="form-group fg-line type-page type-page-uri hidden">
                                <label>URI</label>
                                <input type="text" class="form-control input-sm" name="uri" id="link-uri" placeholder="categorias/categoria1">
                            </div>
                            <div class="form-group fg-line type-page type-page-page hidden">
                                <label>Página</label>
                                <div class="fg-line select">    
                                    <?php echo form_dropdown('pagina', $dpdwn_paginas, '', 'class="form-control"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Abrir link en</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="target" value="0" class="rd-btn-target" checked>
                                        <i class="input-helper"></i>
                                        Misma ventana (default)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="target" value="1" class="rd-btn-target">
                                        <i class="input-helper"></i>
                                        Nueva ventana (_blank)
                                    </label>
                                </div>
                            </div>
                            <div class="load-btn-actions">
                                <img src="<?php echo admin_assets('images/circular_load.GIF'); ?>">
                            </div>
                            <div class="btn-actions">
                                <button type="button" id="add-item-list" class="btn btn-primary btn-sm btn-block">Agregar al menú</button>
                            </div>
                        </form>  
                    </div>
                </section>
            </div>
            <div class="col-sm-8">  
                <section class="panel">
                    <header class="panel-heading">
                        Menú principal
                    </header>
                    <div class="panel-body pd-b-20">                        
                        <div class="dd" id="nestable_list_3">
                            <ol class="dd-list">
                                <?php foreach ($menu as $value): ?>
                                <li class="dd-item dd3-item" data-id="<?php echo $value['id']; ?>">
                                    <div class="dd-handle dd3-handle"></div>
                                    <div class="dd3-content">
                                        <a href="#" class="dd3-action-detail">
                                            <?php echo $value['titulo'] ?>
                                        </a>
                                    </div>
                                    <?php if (count($value['children'])): ?>
                                    <ol class="dd-list">
                                        <?php foreach ($value['children'] as $child): ?>
                                        <li class="dd-item dd3-item" data-id="<?php echo $child['id']; ?>">
                                            <div class="dd-handle dd3-handle"></div>
                                            <div class="dd3-content">
                                                <a href="#" class="dd3-action-detail">
                                                    <?php echo $child['titulo'] ?>
                                                </a>
                                            </div>
                                            <?php if (count($child['children'])): ?>
                                            <ol class="dd-list">
                                                <?php foreach ($child['children'] as $child2): ?>
                                                <li class="dd-item dd3-item" data-id="<?php echo $child2['id']; ?>">
                                                    <div class="dd-handle dd3-handle"></div>
                                                    <div class="dd3-content">
                                                        <a href="#" class="dd3-action-detail"><?php echo $child2['titulo'] ?></a>
                                                    </div>
                                                </li>
                                                <?php endforeach; ?>
                                            </ol>
                                            <?php endif; ?>
                                        </li>
                                        <?php endforeach; ?>
                                    </ol>
                                    <?php endif; ?>
                                </li>
                                <?php endforeach; ?>
                            </ol>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <input type="hidden" value="" name="inp-menu-txt" id="inp-menu-txt">
                        <div class="load-save-menu" style="display: none;">
                            <img src="<?php echo admin_assets('images/circular_load.GIF'); ?>">
                        </div>
                        <button type="button" id="btn-save-menu" class="btn btn-primary">Guardar</button>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit-item">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar enlace</h4>
            </div>
            <div class="modal-body pd-15"></div>
            <div class="modal-footer">
                <div class="load-btn-actions">
                    <img src="<?php echo admin_assets('images/circular_load.GIF'); ?>">
                </div>
                <div class="modal-btn-actions">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger btn-editar-delete">Elimnar</button>
                    <button type="button" class="btn btn-primary btn-editar-guardar">Guardar cambios</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->