<form role="form" id="form_editar" method="post" action="">
    <input type="hidden" name="id_enlace" value="<?php echo $link->id; ?>">
    <div class="form-group">
        <label>Tipo de enlace</label>
        <div class="radio">
            <label>
                <?php echo form_radio('tipo_enlace', 'url', (bool)('url' == $link->tipo_link), 'class="rd-btn-type"'); ?>
                <i class="input-helper"></i>
                Enlace externo (URL)
            </label>
        </div>
        <div class="radio">
            <label>
                <?php echo form_radio('tipo_enlace', 'uri', (bool)('uri' == $link->tipo_link), 'class="rd-btn-type"'); ?>
                <i class="input-helper"></i>
                Enlace del sitio (URI)
            </label>
        </div>
        <div class="radio">
            <label>
                <?php echo form_radio('tipo_enlace', 'pagina', (bool)('pagina' == $link->tipo_link), 'class="rd-btn-type"') ?>
                <i class="input-helper"></i>
                Página
            </label>
        </div>
    </div>
    <div class="form-group fg-line">
        <label>Título</label>
        <input type="text" class="form-control input-sm" name="titulo" value="<?php echo $link->titulo ?>">
    </div>
    <div class="form-group fg-line type-page type-page-url <?php echo ('url' == $link->tipo_link) ? '' : 'hidden'; ?>">
        <label>URL</label>
        <input type="text" class="form-control input-sm" name="url" placeholder="http://ejemplo.com" value="<?php echo $link->url; ?>">
    </div>
    <div class="form-group fg-line type-page type-page-uri <?php echo ('uri' == $link->tipo_link) ? '' : 'hidden'; ?>">
        <label>URI</label>
        <input type="text" class="form-control input-sm" name="uri" value="<?php echo $link->uri; ?>">
    </div>
    <div class="form-group fg-line type-page type-page-page <?php echo ('pagina' == $link->tipo_link) ? '' : 'hidden'; ?>">
        <label>Página</label>
        <div class="fg-line select">    
            <?php echo form_dropdown('pagina', $dpdwn_paginas, $link->pagina_id, 'class="form-control"'); ?>
        </div>
    </div>
    <div class="form-group">
        <label>Abrir link en</label>
        <div class="radio">
            <label>
                <?php echo form_radio('target', '0', (bool)(0 == $link->target), 'class="rd-btn-target"') ?>
                <i class="input-helper"></i>
                Misma ventana (default)
            </label>
        </div>
        <div class="radio">
            <label>
                <?php echo form_radio('target', '1', (bool)(1 == $link->target), 'class="rd-btn-target"') ?>
                <i class="input-helper"></i>
                Nueva ventana (_blank)
            </label>
        </div>
    </div>
</form> 