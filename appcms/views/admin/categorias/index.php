<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Categorías
                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/categorias/form'); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nueva
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table id="tbl-paginas" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                function lista_categorias($parent_id, $cats, $sub = '') {
                                    foreach ($cats[$parent_id] as $cat):
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url('admin/categorias/form/' . $cat->id); ?>">
                                                    <?php echo $sub . $cat->nombre; ?>
                                                </a>
                                            </td>
                                            <td></td>
                                            <td class="col-sm-1">
                                                <a href="<?php echo base_url('admin/categorias/eliminar/' . $cat->id); ?>" class="btn btn-danger btn-sm btn-delete">
                                                    <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        if (isset($cats[$cat->id]) && count($cats[$cat->id]) > 0) {
                                            $sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
                                            $sub2 .= '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
                                            lista_categorias($cat->id, $cats, $sub2);
                                        }
                                    endforeach;
                                }

                                if (isset($categorias[0])) {
                                    lista_categorias(0, $categorias);
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>