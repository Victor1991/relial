<!DOCTYPE html>
<html lang="es-MX">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Suma Web Diseño">
        <link rel="shortcut icon" href="images/favicon.png">

        <title>Panel de administración</title>

        <!--Core CSS -->
        <link href="<?php echo admin_assets('bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/bootstrap-reset.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="<?php echo admin_assets('css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/style-responsive.css'); ?>" rel="stylesheet" />
        <link href="<?php echo admin_assets('css/orange-theme.css'); ?>" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo admin_assets('js/bootstrap-datepicker/css/datepicker.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo admin_assets('js/bootstrap-timepicker/css/timepicker.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo admin_assets('js/bootstrap-daterangepicker/daterangepicker-bs3.css'); ?>" />


        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">

          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.0/css/font-awesome.css">

        <?php if (isset($this->assets['css'])): ?>
            <?php foreach ($this->assets['css'] as $css) : ?>
            <link type="text/css" rel="stylesheet" href="<?php echo base_url('/assets/' . trim($css, '/')); ?>" />
            <?php endforeach; ?>
        <?php endif; ?>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <section id="container" >
            <!--header start-->
            <header class="header fixed-top clearfix">
                <!--logo start-->
                <div class="brand">
                    <a href="<?php echo base_url('admin'); ?>" class="logo">
                        <img src="<?= admin_assets('images/logo-admin.png'); ?>" alt="">
                    </a>
                    <div class="sidebar-toggle-box">
                        <div class="fa fa-bars"></div>
                    </div>
                </div>
                <!--logo end-->
<!--
                <div class="nav notify-row" id="top_menu">
                      notification start
                    <ul class="nav top-menu">
                         notification dropdown start
                        <li id="header_notification_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                                <i class="fa fa-bell-o"></i>
                                <span class="badge bg-warning">3</span>
                            </a>
                            <ul class="dropdown-menu extended notification">
                                <li>
                                    <p>Notifications</p>
                                </li>
                                <li>
                                    <div class="alert alert-info clearfix">
                                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                                        <div class="noti-info">
                                            <a href="#"> Server #1 overloaded.</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="alert alert-danger clearfix">
                                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                                        <div class="noti-info">
                                            <a href="#"> Server #2 overloaded.</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="alert alert-success clearfix">
                                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                                        <div class="noti-info">
                                            <a href="#"> Server #3 overloaded.</a>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </li>
                         notification dropdown end
                    </ul>
                      notification end
                </div>-->
                <div class="top-nav clearfix">
                    <!--search & user info start-->
                    <ul class="nav pull-right top-menu">
                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle icon-user" href="#">
                                <!--<img alt="" src="images/avatar1_small.jpg">-->
                                <i class="fa fa-user"></i>
                                <span class="username"><?php echo $this->session->userdata('usuario_username'); ?></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <li><a href="<?php echo base_url('admin/usuarios/editar/' . $this->session->userdata('usuario_id')); ?>"><i class=" fa fa-suitcase"></i>Perfil</a></li>
                                <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-key"></i> Salir</a></li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->

                    </ul>
                    <!--search & user info end-->
                </div>
            </header>
            <!--header end-->
            <aside>
                <div id="sidebar" class="nav-collapse">
                    <!-- sidebar menu start-->            <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li>
                                <a class="<?= $this->uri->segment(2) == '' ? 'active':'' ?>" href="<?= base_url('admin'); ?>">
                                    <i class="fa fa-dashboard"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'paginas' ? 'active':'' ?>" href="<?= base_url('admin/paginas'); ?>">
                                    <i class="fa fa-file"></i>
                                    <span>Páginas</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'menus' ? 'active':'' ?>" href="<?= base_url('admin/menus'); ?>">
                                    <i class="fa fa-file"></i>
                                    <span>Menú</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'posts' ? 'active':'' ?>" href="<?= base_url('admin/posts'); ?>">
                                    <i class="fa fa-file-text"></i>
                                    <span>Artículos</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'categorias' ? 'active':'' ?>" href="<?= base_url('admin/categorias'); ?>">
                                    <i class="fa fa-folder-open"></i>
                                    <span>Categorías</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'biblioteca' ? 'active':'' ?>" href="<?= base_url('admin/biblioteca'); ?>">
                                    <i class="fa fa-book"></i>
                                    <span>Biblioteca</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'usuarios' ? 'active':'' ?>" href="<?= base_url('admin/usuarios'); ?>">
                                    <i class="fa fa-users"></i>
                                    <span>Usuarios</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'galerias' ? 'active':'' ?>" href="<?= base_url('admin/galerias'); ?>">
                                    <i class="fa fa-camera"></i>
                                    <span>Galerias</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'banners' ? 'active':'' ?>" href="<?= base_url('admin/banners'); ?>">
                                    <i class="fa fa-picture-o"></i>
                                    <span>Banners</span>
                                </a>
                            </li>



                            <li>
                                <a class="<?= $this->uri->segment(2) == 'mesadirectiva' ? 'active':'' ?>" href="<?= base_url('admin/mesadirectiva'); ?>">
                                    <i class="fa fa-users"></i>
                                    <span>Mesa directiva</span>
                                </a>
                            </li>


                            <li>
                                <a class="<?= $this->uri->segment(2) == 'calendario' ? 'active':'' ?>" href="<?= base_url('admin/calendario'); ?>">
                                    <i class="fa fa-calendar"></i>
                                    <span>Calendario</span>
                                </a>
                            </li>

                            <li>
                                <a class="<?= $this->uri->segment(2) == 'frases' ? 'active':'' ?>" href="<?= base_url('admin/frases'); ?>">
                                    <i class="fa fa-quote-left"></i>
                                    <span>Frases</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'contacto' ? 'active':'' ?>" href="<?= base_url('admin/contacto'); ?>">
                                    <i class="fa fa-phone"></i>
                                    <span>Inicio</span>
                                </a>
                            </li>
                            <li>
                                <a class="<?= $this->uri->segment(2) == 'mapa' ? 'active':'' ?>" href="<?= base_url('admin/mapa'); ?>">
                                    <i class="fa fa-map-o"></i>
                                    <span>Mapa</span>
                                </a>
                            </li>
                            <li>
                               <a class="<?= $this->uri->segment(2) == 'directorio' ? 'active':'' ?>" href="<?= base_url('admin/directorio'); ?>">
                                   <i class="fa fa-sitemap"></i>
                                   <span>Mapa</span>
                               </a>
                          </li>
<!--                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-laptop"></i>
                                    <span>Layouts</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="boxed_page.html">Boxed Page</a></li>
                                    <li><a href="horizontal_menu.html">Horizontal Menu</a></li>
                                    <li><a href="language_switch.html">Language Switch Bar</a></li>
                                </ul>
                            </li>                            -->
                        </ul>
                    </div>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->
