<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Mesa directiva y Junta de Honor

                    </header>
                    <div class="panel-body">
                        <table id="tbl-gpos-banners" class="table table-striped">
                            <thead>
                                <tr>
                                    <td>Grupo</td>
                                    <td class=" col-sm-4 text-center">&nbsp;</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo (count($mesadirectiva_grupos) < 1)?'<tr><td style="text-align:center;" colspan="2">No hay grupos creados aún.</td></tr>':''?>
                                <?php if ($mesadirectiva_grupos): ?>
                                    <?php foreach ($mesadirectiva_grupos as $grupo): ?>
                                    <tr>
                                        <td><?php echo $grupo->nombre; ?></td>
                                        <td class="text-right">
                                            <a href="<?php echo base_url('admin/mesadirectiva/grupo/' . $grupo->id); ?>" class="btn btn-primary btn-sm">
                                                <i class="fa fa-picture-o"></i><span class="hidden-xs hidden-sm">&nbsp;Miembros</span>
                                            </a>
                                           
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<script type="text/javascript">
function confeliminar(){
	return confirm('Desea eliminar este grupo de banners?');
}
</script>