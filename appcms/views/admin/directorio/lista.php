<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">

                    <header class="panel-heading">
                        Banners: <?php echo $banners_grupo->nombre; ?>
                        <span class="tools pull-right">
                            <a onclick="$('#modal_form_banner').modal('show');" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nuevo
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <form id="frm_banners">
                            <div class="table-responsive">
                                <table id="banners_list" class="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <th>Orden</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Teléfono</th>
                                            <th>Estatus</th>
                                            <th class="col-sm-2">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <?php echo (count($banners) < 1) ? '<tr><td style="text-align:center;" colspan="5">Aún no hay banners.</td></tr>' : '' ?>
                                    <?php if ($banners): ?>
                                        <tbody>
                                            <?php foreach ($banners as $banner): ?>
                                                <tr data-id="<?php echo $banner->id; ?>">
                                                    <td>
                                                        <input type="hidden" class="inpt-priority" name="orden[<?php echo $banner->id ?>]" value="<?php echo $banner->orden; ?>">
                                                        <span class="priority"><?php echo $banner->orden ?></span>
                                                    </td>
                                                    <td>
                                                         <?=$banner->titulo; ?>
                                                    </td>
                                                    <td>
                                                         <?=$banner->mail; ?>
                                                    </td>
                                                    <td>
                                                         <?=$banner->telefono; ?>
                                                    </td>
                                                    <td>
                                                         <?=$banner->estatus == 1 ? '<h4><span class="label label-success">Activo</span></h4>' : '<h4><span class="label label-danger">Inactivo</span></h4>' ; ?>
                                                    </td>
                                                    <td>
                                                        <a onclick="editar_banner('<?=htmlspecialchars(json_encode($banner), ENT_QUOTES, 'UTF-8')?>')" class="btn btn-success btn-sm">
                                                            <i class="fa fa-edit"></i><span class="hidden-sm hidden-xs">&nbsp;Editar</span>
                                                        </a>
                                                        <a onclick="eliminar_directorio(<?=htmlspecialchars(json_encode($banner->id), ENT_QUOTES, 'UTF-8')?>)" class="btn btn-danger btn-sm">
                                                            <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    <?php endif; ?>
                                </table>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<?php $this->load->view('admin/directorio/banner_form'); ?>
