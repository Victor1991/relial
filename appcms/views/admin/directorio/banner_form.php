<div class="modal fade" id="modal_form_banner" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-body">
                    <form method="post" action="<?=base_url('admin/directorio/insert')?>" id="form_banner" class="form-horizontal" enctype="multipart/form-data">
                         <input type="hidden" name="banner_id" id="banner_id" value="">
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Titulo</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="titulo" id="m_titulo" required placeholder="Titulo" value="">
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Correo</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="correo" id="m_correo" placeholder="Correo" value="">
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Teléfono</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="telefono" id="m_telefono" placeholder="Teléfono" value="">
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Pais</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <select class="form-control" name="pais_id" id="pais_id">
                                             <option value="" hidden>-- Seleccina un pais</option>
                                             <?php foreach ($paises as $key => $pais): ?>
                                                  <option value="<?=$pais->id?>"><?=$pais->nombre?></option>
                                             <?php endforeach; ?>
                                        </select>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Estatus</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <input type="checkbox" class="form-control input-sm" name="estatus" id="m_estatus" value="1">
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Link</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="link" id="m_link" placeholder="http://ejemplo.com" value="">
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <div class="col-sm-10 col-sm-offset-2">
                                   <div class="checkbox">
                                        <label>
                                             <input type="checkbox" name="nueva_ventana" value="1"
                                             id="m_nueva_ventana">
                                             <i class="input-helper"></i>
                                             Abrir link en una nueva ventana
                                        </label>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Descripción</label>
                              <div class="col-sm-10">
                                   <div class="fg-line">
                                        <textarea rows="2" data-max-len="" name="descripcion" id="m_descripcion" class="form-control"></textarea>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="col-sm-2 control-label">Imagen <span class="help-block"></span></label>
                              <div class="col-sm-10">
                                   <input type="file" name="imagen" id="m_imagen" data-width="" data-height="">
                                   <img src="" class="img-responsive" style="max-height:250px;" id="img_banner" alt="">
                              </div>
                         </div>
                         <div class="form-group">
                              <div class="col-sm-10 col-sm-offset-2">
                                   <div class="form-actions" style="float: right;">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                                        <button type="submit" class="btn btn-primary " id="btn-guardar">Guardar</button>

                                   </div>


                                   <div class="form-loading text-center" style="display:none">
                                        <img src="/assets/admin/img/circular_load.GIF">
                                   </div>
                              </div>
                         </div>
                    </form>

               </div>
          </div>
     </div>
</div>
