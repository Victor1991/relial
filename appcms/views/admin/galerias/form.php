<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Nueva Galería
                    </header>
                    <div class="panel-body">
                        <form method="post" action="" class="form-horizontal">
                            <div class="form-group">
                                <label for="titulo" class="col-sm-2 control-label">Título</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="titulo" id="titulo" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="activa" class="col-sm-2 control-label">Activa</label>
                                <div class="col-sm-8">
                                    <div class="radio m-b-15">
                                        <label>
                                            <input type="radio" name="activa" value="0" checked="">
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                    <div class="radio m-b-15">
                                        <label>
                                            <input type="radio" name="activa" value="1">
                                            <i class="input-helper"></i>
                                            Si
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" id="submit" class="btn btn-primary">Crear Galeria</button>
                                    <a href="<?php echo base_url('admin/galerias'); ?>" class="btn btn-default">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>