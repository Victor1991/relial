<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-8">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Imágenes galería
                    </header>
                    <div class="panel-body">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-white btn-sm"><i class="fa fa-check-square-o"></i> Select all</button>
                            <button type="button" class="btn btn-white btn-sm"><i class="fa fa-folder-open"></i> Add New</button>
                            <button type="button" class="btn btn-white btn-sm"><i class="fa fa-trash-o"></i> Delete</button>
                        </div>
                        <a href="#" type="button" class="btn pull-right btn-sm"><i class="fa fa-upload"></i> Upload New File</a>
                        
                        <div id="gallery" class="media-gal">
                            <?php foreach ($galeria->imagenes as $imagen): ?>
                            <div class="images item">
                                <a href="#modal-img-detail" data-toggle="modal">
                                    <img src="<?php echo base_url('uploads/galerias/' . $imagen->archivo); ?>" alt="" />
                                </a>
                                <p><?php echo $imagen->descripcion; ?></p>
                            </div>
                            <?php endforeach; ?>
                        </div>                        
                    </div>
                </section>
            </div>
            <div class="col-sm-4">
                <div class="panel">
                    <header class="panel-heading">
                        Datos galería
                    </header>
                    <div class="panel-body">
                        <!-- Formulraio detalles galería -->
                        <form method="post" action="<?php echo base_url('admin/galerias/form/' . $galeria->id); ?>" class="form-horizontal" id="form_galeria">
                            <input type="hidden" name="gal_id" id="gal_id" value="<?php echo $galeria->id; ?>" >
                            <div class="form-group">
                                <label for="titulo" class="col-sm-2 control-label">Título</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $galeria->titulo; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="activa" class="col-sm-2 control-label">Activa</label>
                                <div class="col-sm-8">
                                    <div class="radio m-b-15">
                                        <label>
                                            <input type="radio" name="activa" value="0" <?php echo $galeria->estatus ? '' : 'checked'; ?>>
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                    <div class="radio m-b-15">
                                        <label>
                                            <input type="radio" name="activa" value="1" <?php echo $galeria->estatus ? 'checked' : ''; ?>>
                                            <i class="input-helper"></i>
                                            Si
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-loading text-right" style="display: none;">
                                Guardando...
                                <img src="<?php echo base_url('assets/admin/img/circular_load.GIF'); ?>">                    
                            </div>
                            <div class="form-group actions">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="#" class="btn btn-primary btn-guardar-gal">Guardar Cambios</a>
                                    <a href="<?php echo base_url('admin/galerias/'); ?>" class="btn btn-default">Cancelar</a>
                                </div>
                            </div>
                        </form>
                        <!-- // Formulraio detalles galería -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>



<!-- Modal -->
<div class="modal fade" id="modal-img-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Media Gallery</h4>
            </div>

            <div class="modal-body row">

                <div class="col-md-5 img-modal">
                    <img src="<?php echo base_url('uploads/galerias/430550d8c6ab2b68fad30f3bcfb5fc86.jpg'); ?>" alt="">
                    <a href="#" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit Image</a>
                    <a href="#" class="btn btn-white btn-sm"><i class="fa fa-eye"></i> View Full Size</a>

                    <p class="mtop10"><strong>File Name:</strong> 430550d8c6ab2b68fad30f3bcfb5fc86.jpg</p>
                    <p><strong>File Type:</strong> jpg</p>
                    <p><strong>Resolution:</strong> 300x200</p>
                    <p><strong>Uploaded By:</strong> <a href="#">ThemeBucket</a></p>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label> Name</label>
                        <input id="name" value="img01.jpg" class="form-control">
                    </div>
                    <div class="form-group">
                        <label> Tittle Text</label>
                        <input id="title" value="awesome image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label> Description</label>
                        <textarea rows="2" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label> Link URL</label>
                        <input id="link" value="images/gallery/img01.jpg" class="form-control">
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-danger" type="button">Delete</button>
                        <button class="btn btn-primary" type="button">Save changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- modal -->