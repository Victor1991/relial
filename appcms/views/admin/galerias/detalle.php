<?php // _dump($galeria->imagenes);die; ?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-8">
                <div class="alerts-box">
                <?php show_alerts(); ?>
                </div>
                
                <section class="panel">
                    <header class="panel-heading">
                        Imágenes galería
                        <span class="tools pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-imagen">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nuevo
                            </button>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="row" id="imgs-galeria">
                            <?php foreach ($galeria->imagenes as $imagen): ?>
                                <div class="col-sm-6 col-md-3 block-img <?php echo ($imagen->principal) ? 'primary-gal' : ''; ?>" id="item-<?php echo $imagen->id ?>">
                                    <div class="thumbnail">
                                        <img src="<?php echo base_url('uploads/galerias/' . $imagen->archivo); ?>" alt="">
                                        <div class="caption">
                                            <p class="desc"><?php echo $imagen->descripcion; ?></p>

                                            <div class="m-b-5 text-center">
                                                <button type="button" data-img="<?php echo $imagen->id; ?>" data-mostrar="<?php echo $imagen->mostrar ?>" data-principal="<?php echo $imagen->principal; ?>" class="btn btn-primary m-r-5 btn-edit"><i class="fa fa-edit"></i></button>
                                                <button type="button" data-img="<?php echo $imagen->id; ?>" class="btn btn-danger btn-delete"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>                        
                    </div>
                </section>
            </div>
            <div class="col-sm-4">
                <div class="panel">
                    <header class="panel-heading">
                        Datos galería
                    </header>
                    <div class="panel-body">
                        <!-- Formulraio detalles galería -->
                        <form method="post" action="<?php echo base_url('admin/galerias/form/' . $galeria->id); ?>" id="form_galeria">
                            <input type="hidden" name="gal_id" id="gal_id" value="<?php echo $galeria->id; ?>" >
                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $galeria->titulo; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="titulo">Fecha galeria:</label>
                                <div class="input-group">
                                    <input type="text" name="fecha_vista" id="fecha_vista" readonly="" value="<?php echo set_value('fecha_vista', $galeria->fecha_vista); ?>" class="form-control date-picker2">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="titulo">Mostrar en inicio:</label>
                                <div class="radio m-b-15">
                                    <label>
                                        <input type="checkbox" name="inicio" value="1" <?php echo $galeria->inicio ? 'checked':''; ?>>
                                        <i class="input-helper"></i>Mostrar
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="activa">Activa</label>
                                <div class="radio m-b-15">
                                    <label>
                                        <input type="radio" name="activa" value="0" <?php echo $galeria->estatus ? '' : 'checked'; ?>>
                                        <i class="input-helper"></i>No
                                    </label>
                                </div>
                                <div class="radio m-b-15">
                                    <label>
                                        <input type="radio" name="activa" value="1" <?php echo $galeria->estatus ? 'checked' : ''; ?>>
                                        <i class="input-helper"></i>Si
                                    </label>
                                </div>
                            </div>
                            <div class="form-loading text-right" style="display: none;">
                                Guardando...
                                <img src="<?php echo base_url('assets/admin/img/circular_load.GIF'); ?>">                    
                            </div>
                            <div class="form-group actions">
                                <div class="col-sm-12">
                                    <a href="#" class="btn btn-primary btn-block btn-guardar-gal">Guardar Cambios</a>
                                    <a href="<?php echo base_url('admin/galerias/'); ?>" class="btn btn-default btn-block">Cancelar</a>
                                </div>
                            </div>
                        </form>
                        <!-- // Formulraio detalles galería -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<div class="modal fade" id="modal-imagen" data-backdrop="false" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modal-imagen-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-imagen-label">Imagen</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="upload-img" action="<?php echo base_url('admin/galerias/guardar_imagen'); ?>">
                    <input type="hidden" name="img_id" id="img_id" value="0">
                    <div id="img_src" style="display:none;"></div>
                    <div class="form-group">
                        <label>Imagen (Tamaño máx. 2MB)</label>
                        <input type="file" name="img_archivo" id="img_archivo">
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="img_principal" id="img_principal" value="1">Imagen principal
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="mostrar" id="mostrar" value="1">Mostrar
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea name="img_desc" id="img_desc" rows="3" class="form-control"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="modal-loading" style="display: none;">
                    Guardando información...
                    <img src="<?php echo base_url('assets/admin/img/circular_load.GIF'); ?>">                    
                </div>
                <div class="modal-btn-actions">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="btn-upload-img">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
