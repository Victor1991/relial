<?php // _dump($galerias); ?>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Galerias
                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/galerias/form'); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nuevo
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="tbl-paginas" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="max-width: 100px;">&nbsp;</th>
                                        <th>Título</th>
                                        <th>No. imágenes</th>
                                        <th>Fecha creacion</th>
                                        <th>Estatus</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($galerias as $galeria): ?>
                                        <tr>
                                            <td style="max-width: 100px; max-height: 100px; overflow: hidden; padding: 0;">
                                                <img style="max-width: 72px; max-height: 72px;" src="<?php echo $galeria->archivo ? base_url('uploads/galerias/' . $galeria->archivo) : base_url('assets/admin/img/noimg.png'); ?>">
                                            </td>
                                            <td><?php echo $galeria->titulo; ?></td>
                                            <td><?php echo $galeria->num_imagenes; ?></td>
                                            <td><?= date('Y-m-d',  strtotime($galeria->fecha_creacion)) ?></td>
                                            <td><?php echo $galeria->estatus ? '<span class="c-green">Visible</span>' : '<span class="c-red">No visible</span>'; ?></td>
                                            <td class="col-sm-2">
                                                <a href="<?php echo base_url('admin/galerias/detalle/' . $galeria->id); ?>" class="btn btn-success btn-sm">
                                                    <i class="fa fa-edit"></i><span class="hidden-sm hidden-xs">&nbsp;Editar</span>
                                                </a>
                                                <a href="<?php echo base_url('admin/galerias/eliminar/' . $galeria->id); ?>" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>