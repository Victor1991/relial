<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Administración | CMS</title>

        <!--Core CSS -->
        <link href="<?php echo admin_assets('bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/bootstrap-reset.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="<?php echo admin_assets('css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/style-responsive.css'); ?>" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="login-body">

        <div class="container">
            
            <form method="post" class="form-signin" action="">
                <div class="login-header">
                    <img style="width: 50px" src="<?php echo admin_assets('images/map1.png'); ?>">
                    <h3>ADMIN - RELIAL</h3>
                </div>
                <div class="login-wrap">
                    <?php if ($errores): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $errores ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($mensajes): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $mensajes; ?>
                        </div>
                    <?php endif; ?>
                    <div class="user-login-info">
                        <input type="text" name="username" class="form-control" placeholder="Usuario">
                        <input type="password" name="password" class="form-control" placeholder="Contraseña">
                    </div>                    
                    <button class="btn btn-lg btn-login btn-block" type="submit">Accesar</button>                    
                </div>                
            </form>
            <div class="login-exta-links">
                <a href="<?php echo base_url('login/recuperar_password'); ?>">¿Olvido su contraseña?</a>
            </div>
        </div>


        <!-- Javascript Libraries -->
        <script src="<?php echo admin_assets('js/jquery.js'); ?>"></script>
        <script src="<?php echo admin_assets('bs3/js/bootstrap.min.js'); ?>"></script>

    </body>
</html>