<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Mapa
                    </header>
                    <div class="panel-body">
                        <?php show_alerts(); ?>
                        <div class="row">
                            <?php foreach ($paises as $p): ?>
                                <div class="col-md-12" id="<?= $p->clave ?>">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title" style="line-height: 35px;">
                                                <a style="cursor: pointer;" class="institucion-panel" data-clave="<?= $p->clave ?>">
                                                    <i class="fa fa-caret-down"></i>&nbsp;&nbsp;&nbsp;<?= $p->nombre ?>
                                                </a>
                                                <button onclick="nueva_institucion('<?= $p->nombre ?>', '<?= $p->clave ?>',<?= $p->id ?>)" class="btn-success btn pull-right"> <i class="fa fa-plus-circle"></i> Nueva institución</button>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <h3 class="panel-title">Instituciones</h3>
                                            <table class="table table-instituciones">
                                                <thead>
                                                    <tr>
                                                        <th><i class="fa fa-gears"></i></th>
                                                        <th>Nombre</th>
                                                        <th>Web</th>
                                                        <th>Url</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($p->instituciones as $instituciones): ?>
                                                        <tr>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <a href="#" onclick="edit_institucion(<?= $instituciones->id ?>, '<?= $p->nombre ?>', '<?= $p->clave ?>',<?= $p->id ?>, '<?= $instituciones->nombre ?>', '<?= $instituciones->web ?>')" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                                                                    <a href="#" onclick="delete_institucion(<?= $instituciones->id ?>)" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                                                                </div>
                                                            </td>
                                                            <td class="nombre"><?= $instituciones->nombre ?></td>
                                                            <td class="web"><?= $instituciones->web ?></td>
                                                            <td class="url"><?= $instituciones->url ?></td>
                                                        </tr>
                                                    <?php endforeach; ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="modal fade" id="modalInstituciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form-instituciones" action="" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="title-form-institucion">Agregar nueva institución</h4>
                        </div>
                        <div class="modal-body" style="padding-left: 50px;padding-right: 50px;">

                            <h3 class="panel-title">País</h3>
                            <hr>
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input readonly="" type="text" class="form-control" id="nombre">
                            </div>
                            <div class="form-group">
                                <label for="clave">Clave</label>
                                <input readonly="" type="text" class="form-control" id="clave">
                            </div>

                            <h3 class="panel-title">Institución</h3>
                            <hr>
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input name="nombre_institucion" type="text" class="form-control" id="nombre-institucion">
                            </div>
                            <div class="form-group">
                                <label for="clave">Web</label>
                                <input name="web" type="text" class="form-control" id="web">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cerrar</button>
                            <button class="btn btn-success" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



    </section>
</section>
