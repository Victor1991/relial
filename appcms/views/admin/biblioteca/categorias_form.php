<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $titulo_form; ?>
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/biblioteca/categoria_form/' . $id); ?>" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Categoría padre</label>
                                <div class="col-sm-10">
                                    <?php echo form_dropdown('parent', $categorias, $parent, 'id="parent" class="form-control"') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre', $nombre); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Descripción</label>
                                <div class="col-sm-10">
                                    <textarea rows="5" name="descripcion" id="descripcion" class="form-control"><?php echo set_value('descripcion', $descripcion); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="<?php echo base_url('admin/biblioteca/categorias/'); ?>" class="btn btn-default">Cancelar</a>
                                    <button type="submit" class="btn btn-primary">Guardar</button>                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>