<table id="tbl-posts" class="table table-striped">
    <thead>
        <tr>
            <th>Destacar</th>
            <th>Titulo</th>
            <th>Categoría</th>
            <th>Autor(es)</th>
            <th>Edición</th>
            <th>Estatus</th>
            <th class="col-sm-1 text-center"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php if (count($libros) > 0): ?>
        <?php foreach ($libros as $libro): ?>
            <tr>
                <td class="text-center"><input class="destacar_libro" <?= $libro->destacado ? 'checked="checked"':'' ?> type="radio" name="destacar" value="<?= $libro->id ?>"></td>
                <td>
                    <a href="<?php echo base_url('admin/biblioteca/form/'.$libro->id); ?>">
                        <?php echo $libro->titulo; ?>
                    </a>
                </td>
                <td><?php echo $libro->categoria_nombre; ?></td>
                <td><?php echo $libro->autor; ?></td>
                <td><?php echo $libro->edicion; ?></td>
                <td><?php echo $libro->estatus ? '<span class="c-green">Visible</span>' : '<span class="c-red">No visible</span>' ; ?></td>
                <td>
                    <a href="<?php echo base_url('admin/biblioteca/eliminar/'.$libro->id); ?>" class="btn btn-danger btn-sm eliminar" alt="eliminar" title="eliminar">
                        <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php endif;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6"><?php echo $pagination['links']; ?></td>
        </tr>
    </tfoot>
</table>

