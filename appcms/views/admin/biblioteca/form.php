<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <?php show_alerts(); ?>

        <form method="post" action="<?php echo base_url('admin/biblioteca/form/' . $id); ?>" id="form_post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-8">
                    <section class="panel">
                        <header class="panel-heading">
                            <?php echo $titulo_form; ?>                       
                        </header>
                        <div class="panel-body">                            
                            <div class="form-group">
                                <label>Titulo</label>
                                <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo set_value('titulo', $titulo); ?>">
                            </div>
                            <div class="form-group">
                                <label>Autor(es)</label>
                                <input type="text" class="form-control" name="autor" id="autor" placeholder="Ej: Autor1, Autor2, etc" value="<?php echo set_value('autor', $autor); ?>">
                            </div>  
                            <div class="form-group">
                                <label>Editorial</label>
                                <input type="text" class="form-control" name="editorial" id="editorial" value="<?php echo set_value('editorial', $editorial); ?>">
                            </div> 
                            <div class="form-group">
                                <label>Edición</label>
                                <input type="text" class="form-control" name="edicion" id="edicion" value="<?php echo set_value('edicion', $edicion); ?>">
                            </div> 
                            <div class="form-group">
                                <label>Idioma</label>
                                <input type="text" class="form-control" name="idioma" id="idioma" value="<?php echo set_value('idioma', $idioma); ?>">
                            </div> 
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea rows="8" data-max-len="500" name="descripcion" id="descripcion" class="form-control"><?php echo set_value('descripcion', $descripcion); ?></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Estatus</label>
                                <?php echo form_dropdown('estatus', $estatus_dpdwn, $estatus, 'id="estatus" class="form-control"') ?>
                            </div>
                            <div class="form-group">
                                <label>Categoría</label>
                                <?php echo form_dropdown('categoria', $categorias, $categoria_id, 'id="categoria" class="form-control"') ?>
                            </div>
                            <div class="form-group">
                                <label>Año publicación</label>
                                <input type="text" name="anio_publicacion" id="anio_publicacion" value="<?php echo set_value('anio_publicacion', $anio_publicacion); ?>" class="form-control" maxlength="4">
                            </div>   
                            <div class="form-group">
                                <label>Archivo (Tamaño máx. 10MB)</label>
                                <input type="file" name="archivo" id="archivo" data-width="" data-height="">
                                <?php if ($id && $archivo != ''): ?>
                                    <div style="text-align:center; padding:5px; margin-top: 10px;">
                                        <a href="<?php echo base_url('uploads/biblioteca/' . $archivo); ?>" alt="imagen actual" class="btn btn-default btn-sm btn-block" target="_blank">
                                            <i class="fa fa-download"></i>&nbsp;Ver archivo actual
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Portada (Tamaño máx. 5MB)</label>
                                <input type="file" name="portada" id="portada" data-width="" data-height="">
                                <?php if ($id && $portada != ''): ?>
                                    <div style="text-align:center; padding:5px; border:1px solid #ccc;margin-top: 10px;">
                                        <img src="<?php echo base_url('uploads/biblioteca/' . $portada); ?>" alt="imagen actual" class="img-responsive">
                                        <br/>Portada actual
                                    </div>
                                <?php endif; ?>
                            </div>

                            <hr>

                            <div class="checkbox">
                                <label><input id="flipbook" name="flipbook" type="checkbox" value="1">Tiene Flipbook</label>
                            </div>
                            
                            <div class="form-group module_upload_flipbook" style="<?= $this->input->post('flipbook') == 1 ? 'display: block;':'display: none;' ?> ">
                                <label>Flipbook (Archivo zip)</label>
                                <input type="file" name="archivo_flipbook" data-width="" data-height="">
                            </div>
                            <div class="form-group module_upload_flipbook" style="<?= $this->input->post('flipbook') == 1 ? 'display: block;':'display: none;' ?> ">
                                <label>Nombre archivo principal:</label>
                                <input class="form-control" type="text" name="archivo_principal">
                            </div>


                            <div class="form-group">
                                <div class="form-actions m-t-30">
                                    <div class="row">
                                        <div class="col-sm-6 m-b-5">
                                            <a href="<?php echo base_url('admin/biblioteca'); ?>" class="btn btn-default btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-sm-6 m-b-5">
                                            <button type="submit" class="btn btn-primary btn-block" id="btn-guardar">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-loading text-center" style="display:none">
                                    <img src="/assets/admin/img/circular_load.GIF">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- page end-->
    </section>
</section>
<!--main content end-->        