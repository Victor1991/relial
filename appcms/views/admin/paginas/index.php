<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Páginas
                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/paginas/form'); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nueva
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table id="tbl-paginas" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Titulo</th>
                                    <th>Estatus</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <?php echo (count($paginas) < 1) ? '<tr><td style="text-align:center;" colspan="3">Aún no hay páginas.</td></tr>' : '' ?>
                            <?php if ($paginas): ?>
                                <tbody>
                                    <?php foreach ($paginas as $pagina): ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url('admin/paginas/form/' . $pagina->id); ?>">
                                                    <?php echo $pagina->titulo; ?>
                                                </a>
                                            </td>
                                            <td><?php echo $pagina->estatus ? '<span class="c-green">Visible</span>' : '<span class="c-red">No visible</span>'; ?></td>
                                            <td class="col-sm-1">
                                                <a href="<?php echo base_url('admin/paginas/eliminar/' . $pagina->id); ?>" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>