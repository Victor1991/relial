<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $titulo_form; ?>
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/paginas/form/' . $id); ?>" >
                            <div class="form-group">
                                <label>Titulo</label>
                                <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo set_value('titulo', $titulo); ?>">
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <input type="text" class="form-control" name="slug" id="slug" readonly="" value="<?php echo set_value('slug', $slug); ?>">
                            </div>
                            <div class="form-group">
                                <label>Estatus</label>
                                <?php echo form_dropdown('estatus', $estatus_dpdwn, $estatus, 'id="estatus" class="form-control"') ?>
                            </div>
                            <div class="form-group">
                                <label>Contenido</label>
                                <textarea rows="10" class="form-control html-ckeditor" name="contenido" id="contenido"><?php echo set_value('contenido', $contenido); ?></textarea>
                            </div>                    
                            <div class="form-group">
                                <a href="<?php echo base_url('admin/paginas/'); ?>" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Guardar</button>                                
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>