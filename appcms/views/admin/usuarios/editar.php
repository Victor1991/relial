<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Editar usuario
                    </header>
                    <div class="panel-body">
                        <form method="post" action="" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Roles</label>
                                <div class="col-sm-4">
                                    <?php echo form_dropdown('rol_id', $roles_dpdwn, $usuario->rol_id, 'class="form-control" id="rol"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Activo</label>
                                <div class="col-sm-4">
                                    <?php echo form_dropdown('activo', array('1' => 'Si', '0' => 'No'), $usuario->activo, 'class="form-control" id="activo"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre', $usuario->nombre); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Apellidos</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="apellidos" id="apellidos" value="<?php echo set_value('apellidos', $usuario->apellidos); ?>" placeholder="Apellidos">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email', $usuario->email); ?>" placeholder="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="username" id="username" value="<?php echo set_value('username', $usuario->username); ?>" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nueva Contraseña</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Confirmar contraseña</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Vuelva a escribir la contraseña">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="<?php echo base_url('admin/usuarios'); ?>" class="btn btn-default">Cancelar</a>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>