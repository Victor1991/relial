<div class="box-pagination pagination-right">
    <?php echo $pagination['links']; ?>
</div>
<table id="tbl-usuarios" class="table table-striped">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Username</th>
            <th>E-mail</th>
            <th>Rol</th>
            <th>Activo</th>
            <th>Último acceso</th>
            <th class="text-center"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?php echo $usuario->nombre_completo; ?></td>
                <td><?php echo $usuario->username; ?></td>
                <td><?php echo $usuario->email; ?></td>
                <td><?php echo $usuario->rol_nombre; ?></td>
                <td><?php echo $usuario->activo ? '<span class="c-green">Si</span>' : '<span class="c-red">No</span>'; ?></td>
                <td><?php echo mysql_to_datetime($usuario->ultimo_login); ?></td>
                <td>
                    <a href="<?php echo base_url('admin/usuarios/editar/'.$usuario->id); ?>" class="btn btn-success btn-sm editar" alt="editar" title="editar"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo base_url('admin/usuarios/eliminar/'.$usuario->id); ?>" class="btn btn-danger btn-sm eliminar" alt="eliminar" title="eliminar"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>                        
    </tbody>   
</table>
<div class="box-pagination pagination-right">
    <?php echo $pagination['links']; ?>
</div>

