<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Usuarios
                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/usuarios/agregar'); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nuevo
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="filters">
                            <form method="post" action="<?php echo base_url('admin/usuarios/index'); ?>" class="row">
                                <input type="hidden" id="f_modulo" name="f_modulo" value="usuarios">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Activo</label>
                                        <select name="f_activo" class="form-control">
                                            <option value="0">--Todos--</option>
                                            <option value="1">Si</option>
                                            <option value="2">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Rol</label>
                                        <?php echo form_dropdown('f_rol', $roles_dpdwn, '', 'class="form-control"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Buscar</label>
                                        <input type="text" name="f_buscar" id="f_buscar" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group m-t-25">
                                        <a href="#" name="f_btn_cancelar" id="f_btn_cancelar" class="btn btn-default btn-block cancel">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                            <hr>
                        </div>
                        <div id="filter-load">
                            <img src="<?php echo admin_assets('img/circular_load.GIF'); ?>">
                        </div>
                        <div id="filter-table-data">
                            <?php echo $tabla; ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>