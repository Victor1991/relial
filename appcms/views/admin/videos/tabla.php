<table id="tbl-videos" class="table table-striped">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Link</th>
            <th>Estatus</th>
            <th class="text-center"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($videos as $video): ?>
            <tr>
                <td><?php echo $video->titulo; ?></td>
                <td>
                    <a href="<?php echo $video->link; ?>" class="btn btn-default btn-sm" target="_blank">
                        <i class="fa fa-film"></i>&nbsp;Ver video
                    </a>
                </td>
                <td><?php echo $video->estatus ? '<span class="c-green">visible</span>' : '<span class="c-red">No visible</span>' ; ?></td>
                <td>
                    <a href="<?php echo base_url('admin/videos/eliminar/'.$video->id); ?>" class="btn btn-danger btn-sm eliminar" alt="eliminar" title="eliminar">
                        <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                    </a>
                    <a href="<?php echo base_url('admin/videos/form/'.$video->id); ?>" class="btn btn-success btn-sm btn-edit" alt="editar" title="editar">
                        <i class="fa fa-edit"></i><span class="hidden-sm hidden-xs">&nbsp;Editar</span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>                        
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6"><?php echo $pagination['links']; ?></td>
        </tr>
    </tfoot>
</table>

