<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $titulo_form; ?>
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/videos/form/' . $id); ?>" >
                            <div class="form-group">
                                <label>Estatus</label>
                                <?php echo form_dropdown('estatus', array('0' => 'No visible', '1' => 'Visible'), $estatus, 'id="estatus" class="form-control"') ?>
                            </div>
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo set_value('titulo', $titulo); ?>">
                            </div>
                            <div class="form-group">
                                <label>
                                    Link 
                                    <?php if ($id && $link): ?>
                                    &nbsp;(<a href="<?php echo $link ?>" target="_blank">Ver video actual</a>)
                                    <?php endif; ?>
                                </label>
                                <input type="text" name="link" id="link" value="<?php echo set_value('link', $link); ?>" class="form-control">
                            </div>                    
                            <div class="form-group text-right">
                                <a href="<?php echo base_url('admin/videos/'); ?>" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Guardar</button>                                
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>