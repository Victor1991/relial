    </section>

        <!-- Placed js at the end of the document so the pages load faster -->

        <!--Core js-->
        <script>
            var baseUrl = '<?= base_url() ?>';
        </script>

        <script src="<?= admin_assets('js/jquery.js'); ?>"></script>
        <script src="<?= admin_assets('bs3/js/bootstrap.min.js'); ?>"></script>
        <script class="include" type="text/javascript" src="<?= admin_assets('js/jquery.dcjqaccordion.2.7.js'); ?>"></script>
        <script src="<?= admin_assets('js/jquery.scrollTo.min.js'); ?>"></script>
        <script src="<?= admin_assets('js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js'); ?>"></script>
        <script src="<?= admin_assets('js/jquery.nicescroll.js'); ?>"></script>
        <!--Easy Pie Chart-->
        <script src="<?= admin_assets('js/easypiechart/jquery.easypiechart.js'); ?>"></script>
        <!--Sparkline Chart-->
        <script src="<?= admin_assets('js/sparkline/jquery.sparkline.js'); ?>"></script>
        <!--jQuery Flot Chart-->
        <script src="<?= admin_assets('js/flot-chart/jquery.flot.js'); ?>"></script>
        <script src="<?= admin_assets('js/flot-chart/jquery.flot.tooltip.min.js'); ?>"></script>
        <script src="<?= admin_assets('js/flot-chart/jquery.flot.resize.js'); ?>"></script>
        <script src="<?= admin_assets('js/flot-chart/jquery.flot.pie.resize.js'); ?>"></script>
        <script type="text/javascript" src="<?= admin_assets('js/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
        <script type="text/javascript" src="<?= admin_assets('js/bootstrap-daterangepicker/moment.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= admin_assets('js/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
        <script type="text/javascript" src="<?= admin_assets('js/bootstrap-timepicker/js/bootstrap-timepicker.js'); ?>"></script>
        <!-- ckedito y ckfinder -->
        <script src="<?= admin_assets('js/ckeditor/ckeditor.js'); ?>"></script>
        <script src="<?= admin_assets('js/ckfinder/ckfinder.js'); ?>"></script>
        <script src="<?= admin_assets('js/ckeditor/config.js'); ?>"></script>

        <!-- <script type="text/javascript" src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDp6N78xk7nRnBS0rQ7HW3S73_AMNPB0ic"></script>

        <!--common script init for all pages-->
        <script src="<?= admin_assets('js/scripts.js'); ?>"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js">
     


        <?php if (isset($this->assets['js'])): ?>
            <?php foreach ($this->assets['js'] as $js) : ?>
                <script src="<?php echo base_url('/assets/' . trim($js, '/')); ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>
    </body>
</html>
