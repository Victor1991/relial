<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $titulo_form; ?>
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/contacto'); ?>" >
                            <div class="form-group">
                                <label>Información de contacto</label>
                                
                                <textarea rows="10" data-toolbar="basic" class="form-control html-ckeditor" name="texto" id="texto"><?php echo set_value('texto',$informacion->texto); ?></textarea>
                            </div>                    
                            <div class="form-group">
                                <label>Información de inicio</label>
                                
                                <textarea rows="10" data-toolbar="basic" class="form-control html-ckeditor" name="inicio" id="inicio"><?php echo set_value('inicio',$inicio->texto); ?></textarea>
                            </div>                    
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">Guardar</button>                                
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>