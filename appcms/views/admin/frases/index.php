<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Frases
                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/frases/form'); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nueva
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table id="tbl-paginas" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Autor</th>
                                    <th>Frase</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <?php echo (count($frases) < 1) ? '<tr><td style="text-align:center;" colspan="3">Aún no hay frases.</td></tr>' : '' ?>
                            <?php if ($frases): ?>
                                <tbody>
                                    <?php foreach ($frases as $frase): ?>
                                        <tr>
                                            <td><?php echo $frase->autor; ?></td>
                                            <td><?php echo $frase->texto; ?></td>
                                            <td class="col-sm-2">
                                                <a href="<?php echo base_url('admin/frases/form/' . $frase->id); ?>" class="btn btn-success btn-sm">
                                                    <i class="fa fa-edit"></i><span class="hidden-sm hidden-xs">&nbsp;Editar</span>
                                                </a>
                                                <a href="<?php echo base_url('admin/frases/eliminar/' . $frase->id); ?>" class="btn btn-danger btn-sm eliminar">
                                                    <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>