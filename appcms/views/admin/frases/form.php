<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $titulo_form; ?>
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/frases/form/' . $id); ?>" >
                            <div class="form-group">
                                <label>Autor</label>
                                <input type="text" class="form-control" name="autor" id="autor" value="<?php echo set_value('autor', $autor); ?>">
                            </div>
                            <div class="form-group">
                                <label>Frase</label>
                                <textarea rows="10" class="form-control" name="texto" id="texto"><?php echo set_value('texto', $texto); ?></textarea>
                            </div>                    
                            <div class="form-group text-right">
                                <a href="<?php echo base_url('admin/frases/'); ?>" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Guardar</button>                                
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>