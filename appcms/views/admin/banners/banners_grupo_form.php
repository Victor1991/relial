<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Nuevo grupo de banners
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/banners/banners_grupos_form/' . $id); ?>" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="nombre" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre', $nombre); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <a href="<?php echo base_url('admin/banners'); ?>" class="btn btn-default">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>