<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Banners: <?php echo $banners_grupo->nombre; ?>
                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/banners/banner_form/'.$banners_grupo->id); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nuevo
                            </a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <form id="frm_banners">
                            <div class="table-responsive">
                                <table id="banners_list" class="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <th>Orden</th>
                                            <th>Titulo</th>
                                            <th>Fecha Disponible</th>
                                            <th>Fecha Expira</th>
                                            <th class="col-sm-2">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <?php echo (count($banners) < 1) ? '<tr><td style="text-align:center;" colspan="5">Aún no hay banners.</td></tr>' : '' ?>
                                    <?php if ($banners): ?>
                                        <tbody>
                                            <?php foreach ($banners as $banner): ?>
                                                <tr data-id="<?php echo $banner->id; ?>">
                                                    <td>
                                                        <input type="hidden" class="inpt-priority" name="orden[<?php echo $banner->id ?>]" value="<?php echo $banner->orden; ?>">
                                                        <span class="priority"><?php echo $banner->orden ?></span>
                                                    </td>
                                                    <td>
                                                        <?php echo date_expired($banner->fecha_inicio, $banner->fecha_expira) ? '<i class="fa fa-exclamation-circle c-red"></i>' . $banner->titulo : $banner->titulo ?>
                                                    </td>
                                                    <td><?php echo mysql_to_date($banner->fecha_inicio) ?></td>
                                                    <td><?php echo mysql_to_date($banner->fecha_expira) ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('admin/banners/banner_form/' . $banners_grupo->id . '/' . $banner->id); ?>" class="btn btn-success btn-sm">
                                                            <i class="fa fa-edit"></i><span class="hidden-sm hidden-xs">&nbsp;Editar</span>
                                                        </a>
                                                        <a href="<?php echo base_url('admin/banners/eliminar_banner/' . $banner->id); ?>" class="btn btn-danger btn-sm">
                                                            <i class="fa fa-trash-o"></i><span class="hidden-sm hidden-xs">&nbsp;Eliminar</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    <?php endif; ?>                        
                                </table>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
