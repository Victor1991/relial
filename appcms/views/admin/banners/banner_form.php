<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $form_titulo; ?>
                    </header>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url('admin/banners/banner_form/' . $banners_grupo_id) . '/' . $banner_id; ?>" id="form_banner" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="banner_id" id="banner_id" value="<?php echo $banner_id; ?>">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Titulo</label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="titulo" id="titulo" placeholder="Titulo" value="<?php echo set_value('titulo', $titulo); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fecha disponible</label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm date-picker" name="fecha_inicio" id="fecha_inicio" placeholder="dd-mm-yyyy" value="<?php echo set_value('fecha_inicio', $fecha_inicio); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fecha expira</label>
                                <div class="col-sm-10">                              
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm date-picker" name="fecha_expira" id="fecha_expira" placeholder="dd-mm-yyyy" value="<?php echo set_value('fecha_expira', $fecha_expira); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Link</label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="link" id="link" placeholder="http://ejemplo.com" value="<?php echo set_value('link', $link); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div class="checkbox">
                                        <label>
                                            <?php echo form_checkbox(array('name' => 'nueva_ventana', 'value' => 1, 'checked' => set_checkbox('nueva_ventana', 1, $nueva_ventana))) ?>
                                            <i class="input-helper"></i>
                                            Abrir link en una nueva ventana
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Descripción</label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <textarea rows="2" data-max-len="<?php echo $max_length; ?>" name="descripcion" id="descripcion" class="form-control"><?php echo set_value('descripcion', $descripcion); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Imagen <span class="help-block"><?php echo $max_width . 'x' . $max_height . 'px'; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="file" name="imagen" id="imagen" data-width="<?php echo $max_width ?>" data-height="<?php echo $max_height; ?>">
                                    <?php if ($banner_id && $imagen != ''): ?>
                                        <div style="text-align:center; padding:5px; border:1px solid #ccc;margin-top: 10px;"><img src="<?php echo base_url('uploads/banners/' . $imagen); ?>" alt="imagen actual" class="img-responsive"><br/>Imagen actual</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div class="form-actions">
                                        <a href="<?php echo base_url('admin/banners/grupo/' . $banners_grupo_id); ?>" class="btn btn-default">Cancelar</a>
                                        <button type="submit" class="btn btn-primary" id="btn-guardar">Guardar</button>
                                    </div>
                                    <div class="form-loading text-center" style="display:none">
                                        <img src="/assets/admin/img/circular_load.GIF">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>