<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <?php show_alerts(); ?>
                <section class="panel">
                    <header class="panel-heading">
                        Grupos de Banners
<!--                        <span class="tools pull-right">
                            <a href="<?php echo base_url('admin/banners/banners_grupos_form'); ?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-circle"></i>&nbsp;Nuevo Grupo
                            </a>
                        </span>-->
                    </header>
                    <div class="panel-body">
                        <table id="tbl-gpos-banners" class="table table-striped">
                            <thead>
                                <tr>
                                    <td>Grupo</td>
                                    <td class=" col-sm-4 text-center">&nbsp;</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo (count($banners_grupos) < 1)?'<tr><td style="text-align:center;" colspan="2">No hay grupos de banners creados aún.</td></tr>':''?>
                                <?php if ($banners_grupos): ?>
                                    <?php foreach ($banners_grupos as $grupo): ?>
                                    <tr>
                                        <td><?php echo $grupo->nombre; ?></td>
                                        <td class="text-right">
                                            <a href="<?php echo base_url('admin/banners/grupo/' . $grupo->id); ?>" class="btn btn-primary btn-sm">
                                                <i class="fa fa-picture-o"></i><span class="hidden-xs hidden-sm">&nbsp;Banners</span>
                                            </a>
                                            <a href="<?php echo base_url('admin/banners/banners_grupos_form/' . $grupo->id); ?>" class="btn btn-success btn-sm">
                                                <i class="fa fa-cog"></i><span class="hidden-xs hidden-sm">&nbsp;Configuración</span>
                                            </a>
                                            <a href="<?php echo base_url('admin/banners/eliminar_grupo/' . $grupo->id); ?>" class="btn btn-danger btn-sm" onclick="return confeliminar();">
                                                <i class="fa fa-trash-o"></i><span class="hidden-xs hidden-sm">&nbsp;Eliminar</span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<script type="text/javascript">
function confeliminar(){
	return confirm('Desea eliminar este grupo de banners?');
}
</script>