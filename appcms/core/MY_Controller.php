<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $ci = get_instance();
        
        $this->load->model(array('usuario_model', 'rol_model'));
        
        $ci->assets['js'] = array();
        $ci->assets['css'] = array();        
    }
    
    public function add_asset($type, $var) {
        array_push(get_instance()->assets[$type], $var);
    }
    
}

class Admin_Controller extends MY_Controller {
    private $template;
    
    public function __construct() {
        parent::__construct();
        
        $this->load->library('auth');
        $this->load->helper(array('date', 'alert'));
        
        if (!$this->auth->is_logged_in()) {
            redirect('login');
        }  
        
//        $this->load->model(array('usuario_model', 'rol_model'));
        $this->usuario = $this->usuario_model->get_by_id($this->session->userdata('usuario_id'));
        $this->usuario->rol = $this->rol_model->get_by_id($this->usuario->id);
    }
    
    function view($view, $vars = array(), $string = false) {
        //if there is a template, use it.
        $template = '';
        if ($this->template) {
            $template = $this->template . '_';
        }

        if ($string) {
            $result = $this->load->view('admin/' . $template . 'header', $vars, true);
            $result .= $this->load->view('admin/' . $view, $vars, true);
            $result .= $this->load->view('admin/' . $template . 'footer', $vars, true);

            return $result;
        } else {
            $this->load->view('admin/' . $template . 'header', $vars);
            $this->load->view('admin/' . $view, $vars);
            $this->load->view('admin/' . $template . 'footer', $vars);
        }

        //reset $this->template to blank
        $this->template = false;
    }

    /* Template is a temporary prefix that lasts only for the next call to view */
    function set_template($template) {
        $this->template = $template;
    }

}

class Public_Controller extends MY_Controller {
    public function __construct() {
        parent::__construct();
        
        //load the theme package
        $this->load->add_package_path(APPPATH.'themes/'.FRONT_THEME);
        $this->load->helper(array('date'));
        $this->load->model(array('categoria_model', 'post_model', 'menu_model', 'banner_model'));
        $this->load->library('breadcrumbs');
        $this->breadcrumbs->push('Inicio', '/');
        
        $this->categorias = $this->categoria_model->get_all();
        $this->banner_header = $this->banner_model->get_random_banner(1);
        
        
        
    }
    
    function view($view, $vars = array(), $string = false) {
        $vars['menu_links'] = $this->menu_model->get_menu_tree($this->session->userdata('rol_id'), true);
        
        if ($string) {
            $result = $this->load->view('header', $vars, true);
            $result .= $this->load->view($view, $vars, true);
            $result .= $this->load->view('footer', $vars, true);

            return $result;
        } else {
            $this->load->view('header', $vars);
            $this->load->view($view, $vars);
            $this->load->view('footer', $vars);
        }
    }

    /*
      This function simply calls $this->load->view()
     */

    function partial($view, $vars = array(), $string = false) {
        if ($string) {
            return $this->load->view($view, $vars, true);
        } else {
            $this->load->view($view, $vars);
        }
    }

}

