<?php

$lang['login_incorrecto'] = 'Usuario y/o contraseña incorrectos.';
$lang['login_bloqueado'] = 'La cuenta ha sido bloqueada.';
$lang['login_cuenta_no_activa'] = 'La cuenta no ha sido activada.';