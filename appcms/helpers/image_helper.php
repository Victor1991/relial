<?php

function resize_image($width, $height, $src, $dest = '', $quality = '70%') {
    if ($dest == '') {
        $dest = $src;
    }
    
    $config['image_library'] = 'gd2';
    $config['source_image'] = $src;
    $config['new_image'] = $dest;
    $config['quality'] = $quality;
    $config['create_thumb'] = FALSE;
    $config['maintain_ratio'] = TRUE;
    $config['width'] = $width;
    $config['height'] = $height;
    
    $CI = &get_instance();
    $CI->load->library('image_lib', $config);
    $CI->image_lib->resize();
    
    if (!$CI->image_lib->resize()) {
        $res['status'] = FALSE;
        $res['errors'] = $CI->image_lib->display_errors();
    } else {
        $res['status'] = TRUE;
    }
    
    return $res;
}