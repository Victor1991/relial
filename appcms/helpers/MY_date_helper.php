<?php

function mysql_to_datetime($date) {
    if (empty($date) OR $date == '0000-00-00 00:00:00') {
        return "";
    }
    
    return date("d-m-Y H:m", strtotime($date));
}

function mysql_to_date($date) {
    if (empty($date) OR $date == '0000-00-00') {
        return "";
    }
    
    return date("d-m-Y", strtotime($date));
}

function mysql_to_time($time, $format12 = TRUE) {
    if (empty($time) OR is_null($time)) {
        return NULL;
    }
    
    if ($format12) {
        $hora = explode(':', $time);
        $tmp = 'AM';
        
        if ($hora[0] == '00') {
            $hora[0] == '12';
        } elseif ($hora[0] > 12) {
            $tmp = 'PM';
            $hora[0] = $hora[0] - 12;
        } elseif ($hora[0] == 12) {
            $tmp = 'PM';
        }
        
        return $hora[0] . ':' . $hora[1] . ' ' . $tmp;
        
    } else {
        return $time;
    }
    
}

function datetime_to_mysql($date) {
    if (empty($date)) {
        return FALSE;
    }
    
    return date("Y-m-d H:i:s", strtotime($date));
}

function date_to_mysql($date) {
    if (empty($date)) {
        return FALSE;
    }
    
    return date("Y-m-d", strtotime($date));
}

function time_to_mysql($time, $format12=TRUE) {
    if (empty($time) OR is_null($time)) {
        return NULL;
    }
    
    if ($format12) {    //formato de 12
        $arr = explode(' ', $time);
        $hora = explode(':',$arr[0]);
        if ($arr[1] == 'PM' && $hora[0] < 12) {
            $hora[0] = 12 + (int)$hora[0];            
        } elseif ($arr[1] == 'AM' && $hora[0] == 12) {
            $hora[0] = '00';
        }
        
        return $hora[0] . ':' . $hora[1];        
    } else {    //formato 24
        return $time;
    }
}

function date_to_uri($date, $format = 'user') {
    $uri = '';
    $format = strtolower($format);
    $arr_date = explode('-', $date);    
    if (count($arr_date) == 3) {
        if ($format == 'mysql') {
            $uri = $arr_date[0].'/'.$arr_date[1].'/'.$arr_date[2];
        } elseif ($format == 'user') {
            $uri = $arr_date[2].'/'.$arr_date[1].'/'.$arr_date[0];
        }
    }
    
    return $uri;
}

function date_expired($start_date, $end_date = NULL, $date = NULL) {
    if (is_null($date)) {
        $date = date("Y-m-d");
    }
    if ($start_date == '0000-00-00' OR is_null($start_date)) {
        $available_exist = false;
        $available = '';
    } else {
        $available_exist = true;
        $available = $start_date;
    }

    if ($end_date == '0000-00-00' OR is_null($end_date)) {
        $expire_exist = false;
        $expire = '';
    } else {
        $expire_exist = true;
        $expire = $end_date;
    }


    if ((!$available_exist || $date >= $available) && (!$expire_exist || $date < $expire)) {
        return FALSE;
    }
    
    return TRUE;
}