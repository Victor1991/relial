<?php 

class Articulos extends Public_Controller {
    public function __construct() {
        parent::__construct();
        $this->breadcrumbs->push('Archivo', 'articulos');
    }
    
    public function index () {
        $this->load->helper(array('pagination', 'date'));
        
        $this->load->model('frase_model');
        
        $filtro = array(
            'estatus' => 1,
            'no_categoria' => 23
        );
        
        //$data['posts'] = $this->post_model->ultimos_posts(10);
        $pagination = create_pagination('articulos/index', $this->post_model->num_posts($filtro), 30, 3);
        $data['posts'] = $this->post_model->get_many($filtro, array('po.fecha' => 'desc'), $pagination['limit'], $pagination['offset']);
        $data['pagination'] = $pagination;
        $data['sidebar'] = $this->prepare_sidebar();
        
         $data['informacion'] = $this->frase_model->get_by_id(8);
        
        $this->view('articulos', $data);        
    }
    
    public function detalle($slug) {
        $slug = htmlentities($slug, ENT_QUOTES);
         $this->load->model('frase_model');
        $post = $this->post_model->get_by(array(
            'slug' => $slug,
            'estatus' => 1
        ));
        
        if (!$post) {
            show_404();
            die;
        }
        
        $data['post'] = $post;
        $data['sidebar'] = $this->prepare_sidebar();
        
        $this->breadcrumbs->push($post->categoria_nombre, 'articulos/categoria/' . $post->categoria_slug);
        $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->view('articulo_detalle', $data);
    }
    
    public function categoria($slug) {
        $slug = htmlentities($slug, ENT_QUOTES);        
        $categoria = $this->categoria_model->get_por_slug($slug);
        
          $this->load->model('frase_model');
        
        if (!$categoria) {
            show_404();
            die;
        }
        
        $this->load->helper(array('pagination', 'date'));
        
        //$data['posts'] = $this->post_model->ultimos_posts(10);
        $pagination = create_pagination('articulos/categoria/'.$slug, $this->post_model->num_posts(array('categoria' => $categoria->id, 'estatus' => 1)), 10, 4);
        $categoria->posts = $this->post_model->get_many(array('categoria' => $categoria->id, 'estatus' => 1), array('po.fecha' => 'desc'), $pagination['limit'], $pagination['offset']);
        $data['pagination'] = $pagination;
        $data['sidebar'] = $this->prepare_sidebar();
        $data['categoria'] = $categoria;
        
        $this->breadcrumbs->push($categoria->nombre, 'articulos/categoria/' . $categoria->slug);
         $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->view('categoria_detalle', $data);
    }
    
    public function buscar() {
        $titulo = $this->input->get('term', TRUE);
        $data['posts'] = $this->post_model->get_many(array('titulo' => $titulo, 'estatus' => 1), array('fecha' => 'desc'));
        $data['ultimos_posts'] = $this->post_model->ultimos_posts(3);
        
        $this->view('buscar_articulo', $data);
    }
    
    private function prepare_sidebar() {
        $data['ultimos_posts'] = $this->post_model->ultimos_posts(5);
        $data['categorias'] = $this->categoria_model->get_many_by(array('modulo' => 'blog', 'exclude' => 23));
        
        return $this->load->view('sidebar_articulos', $data, TRUE);
    }
}
