<?php

class Mapa extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mapa_model');
    }

    public function index($pais = FALSE, $institucion = FALSE) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre_institucion', 'Nombre de la institución', 'trim|required');
        $this->form_validation->set_rules('web', 'Web de la institución', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $data['paises'] = $this->mapa_model->get_paises();
            $this->add_asset('css', 'admin/plugins/sweetalert/sweetalert.css');
            $this->add_asset('js', 'admin/plugins/sweetalert/sweetalert.min.js');
            $this->add_asset('js', 'admin/js/mapa/mapa.js');
            $this->view('mapa/index', $data);
        } else {
            if ($institucion) {
                $save['id'] = $institucion;
            }
            $save['pais_id'] = $pais;
            $save['nombre'] = $this->input->post('nombre_institucion');
            $save['web'] = $this->input->post('web') ? $this->input->post('web'):'';
            $save['url'] = $this->input->post('web') ? 'http://' . $this->input->post('web') : '';
            if ($this->mapa_model->save($save)) {
                $this->session->set_flashdata('mensajes', 'Se ha realizado la modificación correctamente.');
                redirect('admin/mapa');
            } else {
                $this->session->set_flashdata('errores', 'Ocurrio error al realizar la modificación.');
                redirect('admin/mapa');
            }
        }
    }

    public function delete($id) {
        if ($id) {
            $this->mapa_model->delete($id);
            $this->session->set_flashdata('mensajes', 'Se ha eliminado la institución.');
            redirect('admin/mapa');
        } else {
            show_404();
        }
    }

     public function get_estados_pais_json($pais_id = false){
          $repuseta = array();
          if ($pais_id) {
               $repuseta = $this->mapa_model->get_estados_pais($pais_id);
          }
          echo json_encode($repuseta);
     }

}
