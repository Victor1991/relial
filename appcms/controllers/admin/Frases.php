<?php

class Frases extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('frase_model');
    }

    public function index() {
        $data['frases'] = $this->frase_model->get_all();

        $this->view('frases/index', $data);
    }

    public function form($id = FALSE) {
        $this->load->helper('form');
        $this->load->library('form_validation');

        //valores por default
        $data = array(
            'id' => FALSE,
            'texto' => '',
            'autor' => ''
        );

        $data['titulo_form'] = $id ? 'Editar frase' : 'Nueva frase';

        if ($id) {
            $frase = $this->frase_model->get_by_id($id);
            if (!$frase) {
                $this->session->set_flashdata('errores', 'No se encontro la frase solicitada');
                redirect('admin/frases');
            }
            $data = array_merge($data, (array) $frase);
            ;
        }

        $this->form_validation->set_rules('texto', 'Frase', 'required|max_length[500]');
        $this->form_validation->set_rules('autor', 'Autor', 'trim|required');

        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }

            $this->add_asset('js', 'admin/js/frases/admin-frases.js');

            $this->view('frases/form', $data);
        } else {
            $save['texto'] = $this->input->post('texto');
            $save['autor'] = $this->input->post('autor');

            if ($id) {
                $res = $this->frase_model->update($id, $save);
            } else {
                $res = $this->frase_model->insert($save);
            }

            if ($res) {
                $this->session->set_flashdata('mensajes', 'Se ha guardado la frase');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar la frase');
            }

            redirect('admin/frases/');
        }
    }

    public function eliminar($id) {
        $frase = $this->frase_model->get_by_id($id);
        if ($frase) {
            $this->frase_model->eliminar($id);
            $this->session->set_flashdata('mensajes', 'Se ha eliminado la frase.');
        } else {
            $this->session->set_flashdata('errores', 'No se encontro la frase a eliminar.');
        }

        redirect('admin/frases/');
    }

    public function contacto() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        
        $this->form_validation->set_rules('texto', 'Información', 'required');
        if ($this->form_validation->run() == false) {
            $data['titulo_form'] = 'Información de contacto';
            $data['informacion'] = $this->frase_model->get_by_id(8);
            $data['inicio'] = $this->frase_model->get_by_id(9);
            $this->view('frases/contacto', $data);
        }else{
            $this->frase_model->update(8, ['texto' => $this->input->post('texto')]);
            $this->frase_model->update(9, ['texto' => $this->input->post('inicio')]);
            $this->session->set_flashdata('mensajes','Se ha actualizado la información de contaco correctamente.');
            redirect('admin/contacto');
        }
    }

}
