<?php

class Biblioteca extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('biblioteca_model', 'categoria_model'));
    }

    public function index() {
        $this->load->helper(array('pagination', 'form', 'date'));

        $where = array('estatus' => 0);

        $where['estatus'] = $this->input->post('f_estatus') ? (int) $this->input->post('f_estatus') : $where['estatus'];

        if ($this->input->post('f_categoria')) {
            $where['categoria'] = (int) $this->input->post('f_categoria');
        }

        if ($this->input->post('f_buscar')) {
            $where['buscar'] = $this->input->post('f_buscar');
        }

        $pagination = create_pagination('admin/biblioteca/index', $this->biblioteca_model->num_libros($where), 50);

        $tabla['libros'] = $this->biblioteca_model->get_many_libros($where, array('titulo' => 'asc'), $pagination['limit'], $pagination['offset']);
        //_dump($tabla['libros']);die;
        $tabla['pagination'] = $pagination;
        $data['tabla'] = $this->load->view('admin/biblioteca/tabla', $tabla, TRUE);

//        _dump($tabla['posts']);

        if ($this->input->is_ajax_request()) {
            echo $data['tabla'];
        } else {
            $categorias = $this->categoria_model->get_many_by(array('modulo' => 'biblioteca','staus' => 1));
            $data['categorias_dpdwn'] = $this->_array_dropdown($categorias, 'id', 'nombre', '-- todas --');
            $this->add_asset('js', 'admin/js/admin-filtros.js');
            $this->add_asset('js', 'admin/js/biblioteca/admin-libros.js');
            $this->view('biblioteca/index', $data);
        }
    }

    public function form($id = FALSE) {
        $config['upload_path'] = 'uploads/biblioteca/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 1024 * 2;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);


        $this->load->helper(array('form', 'date', 'text'));
        $this->load->library('form_validation');

        //valores por default
        $data = array(
            'id' => FALSE,
            'autor' => '',
            'categoria_id' => 0,
            'titulo' => '',
            'slug' => '',
            'descripcion' => '',
            'editorial' => '',
            'estatus' => 0,
            'portada' => '',
            'archivo' => '',
            'edicion' => '',
            'idioma' => '',
            'anio_publicacion' => ''
        );

        $data['titulo_form'] = $id ? 'Editar libro' : 'Nuevo libro';

        if ($id) {
            $libro = $this->biblioteca_model->get_libro($id);
            if (!$libro) {
                show_404();
                die();
            }
            $data = array_merge($data, (array) $libro);
            ;
        }

        $data['categorias'] = $this->_array_dropdown($this->categoria_model->get_many_by(array('modulo' => 'biblioteca'), array('nombre' => 'asc')), 'id', 'nombre', 'seleccione una categoría');
        $data['estatus_dpdwn'] = array(
            0 => 'No visible',
            1 => 'Visible'
        );


        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('descripcion', 'Descripcion', 'trim|max_length[500]');
        $this->form_validation->set_rules('anio_publicacion', 'Fecha publicación', 'trim');

//        if (!$id) {
//            $this->form_validation->set_rules('imagen', 'Imagen', 'callback__imagen_check');
//        }

        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            $this->show_form($data);
        } else {

            $save['categoria_id'] = (int) $this->input->post('categoria');
            $save['autor'] = $this->input->post('autor');
            $save['estatus'] = (int) $this->input->post('estatus');
            $save['titulo'] = $this->input->post('titulo');
            $save['editorial'] = $this->input->post('editorial');
            $save['descripcion'] = $this->input->post('descripcion');
            $save['idioma'] = $this->input->post('idioma');
            $save['edicion'] = $this->input->post('edicion');
            $save['anio_publicacion'] = $this->input->post('anio_publicacion');

            if ($this->input->post('titulo') != $data['titulo']) {
                $slug = url_title(convert_accented_characters($this->input->post('titulo')), 'dash', TRUE);
                $slug = $this->biblioteca_model->validar_slug($slug, $id);
                $save['slug'] = $slug;
            }

            //cargamos la portada
            $uploaded = $this->upload->do_upload('portada');

            //actualizar
            if ($id) {  //actualizar imagen
                $save['id'] = $id;

                if ($uploaded) {
                    if ($data['portada'] != '') {
                        $file = 'uploads/biblioteca/' . $data['portada'];

                        //eliminar la imagen anterior si existe
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {
                //nuevo post
                if (!$uploaded && isset($_FILES['portada']['tmp_name']) && $_FILES['portada']['tmp_name'] != '') {
                    $this->session->set_flashdata('errores', $this->upload->display_errors());
                    $this->show_form($data);
                    return;
                }
            }

            //se cargo una imagen
            if ($uploaded) {
                $image = $this->upload->data();
                //redimensionar imagen
//                $img_w = 600;
//                $img_h = 200;
//                if ($img_h != $image['image_height'] || $img_w != $image['image_width']) {
//                    $this->load->helper('image');
//                    resize_image($img_w, $img_h, 'uploads/posts/'.$image['file_name']);
//                }

                $save['portada'] = $image['file_name'];
            }


            //////////////////archivo de descarga
            $config['upload_path'] = 'uploads/biblioteca/';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = 1024 * 1024 * 5;
            $config['encrypt_name'] = true;

            // Alternately you can set preferences by calling the ``initialize()`` method. Useful if you auto-load the class:
            $this->upload->initialize($config);
            $uploaded = $this->upload->do_upload('archivo');

            //actualizar
            if ($id) {  //actualizar imagen
                $save['id'] = $id;

                if ($uploaded) {
                    if ($data['archivo'] != '') {
                        $file = 'uploads/biblioteca/' . $data['archivo'];

                        //eliminar la imagen anterior si existe
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {
                //nuevo
                if (!$uploaded && isset($_FILES['archivo']['tmp_name']) && $_FILES['archivo']['tmp_name'] != '') {
                    $this->session->set_flashdata('errores', '<p>Error al subir archivo:</p>' . $this->upload->display_errors());
                    $this->show_form($data);
                    return;
                }
            }

            //se cargo un archivo
            if ($uploaded) {
                $archivo = $this->upload->data();

                $save['archivo'] = $archivo['file_name'];
            }

            /* SUBIR ARCHIO COMPRIMIDO */
            if ($this->input->post('flipbook')) {
                $config['upload_path'] = PAHT_MATERIAL;
                $config['encrypt_name'] = TRUE;
                $config['allowed_types'] = '*';
                $this->upload->initialize($config);
                $nombre = FALSE;
                if ($this->upload->do_upload('archivo_flipbook')) {
                    $nombre = $this->upload->data('file_name');
                    $nombre_carpeta = $this->upload->data('raw_name');
                    mkdir(PAHT_MATERIAL . $nombre_carpeta);
                } else {
                    $this->session->set_flashdata('errores', $this->upload->display_errors());
                }
                $zip = new ZipArchive;
                if ($zip->open(PAHT_MATERIAL . $nombre) === TRUE) {
                    $zip->extractTo(PAHT_MATERIAL . $nombre_carpeta);
                    $zip->close();
                    unlink(PAHT_MATERIAL . $nombre);
                    $save['archivo2'] = base_url('material/' . $nombre_carpeta . '/' . $this->input->post('archivo_principal'));
                } else {
                    $this->session->set_flashdata('errores', 'El archivo no es complatible.');
                }
            }
            /* FINAL SUBIR ARCHIVO COMPRIMIDO */

            if ($id) {
                $res = $this->biblioteca_model->actualizar_libro($id, $save);
            } else {
                $res = $this->biblioteca_model->agregar_libro($save);
            }

            if ($res) {
                $this->session->set_flashdata('mensajes', 'Se ha guardado el libro: <b>' . $save['titulo'] . '</b>');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar el libro');
            }

            redirect('admin/biblioteca/');
        }
    }

    private function show_form($data) {
        $this->add_asset('js', 'admin/js/jquery.maxlength/jquery.plugin.min.js');
        $this->add_asset('js', 'admin/js/jquery.maxlength/jquery.maxlength.min.js');
        $this->add_asset('js', 'admin/js/biblioteca/form-libro.js');

        $this->view('biblioteca/form', $data);
    }

    public function eliminar($id) {
        $id = (int) $id;
        $libro = $this->biblioteca_model->get_libro($id);
        if (!$libro) {
            $this->session->set_flashdata('errores', 'No se encontro el libro a eliminar');
        }

        $this->biblioteca_model->eliminar($id);
        $this->session->set_flashdata('mensajes', 'Se ha eliminado el libro');

        redirect('admin/biblioteca');
    }

    public function categorias() {
        $categorias = $this->categoria_model->get_many_by(array('modulo' => 'biblioteca'), array('nombre', 'asc'));

        $data['categorias'] = array();
        foreach ($categorias as $categoria) {
            $data['categorias'][$categoria->parent][] = $categoria;
        }

        $data['categorias_padre'] = isset($data['categorias'][0]) ? count($data['categorias'][0]) : 0;
        $this->add_asset('js', 'admin/js/biblioteca/admin-categorias.js');

        $this->view('biblioteca/categorias', $data);
    }

    public function categoria_form($id = FALSE) {
        $this->load->library('form_validation');
        $this->load->helper('form');

        $data['titulo_form'] = 'Nueva categoria';
        $data['id'] = FALSE;
        $data['nombre'] = '';
        $data['slug'] = '';
        $data['parent'] = '0';
        $data['descripcion'] = '';

        if ($id) {
            $data = array_merge($data, (array) $this->categoria_model->get($id));
            $data['titulo_form'] = 'Editar categoria';
        }

        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('parent', 'Categoria padre', 'trim');
        $this->form_validation->set_rules('slug', 'Slug', 'trim');

        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }

            $categorias = $this->categoria_model->get_many_by(array('modulo' => 'biblioteca', 'parent' => 0), array('nombre', 'asc'));
            $data['categorias'] = $this->_array_dropdown($categorias, 'id', 'nombre', "--- Ninguna ---");
            $this->view('biblioteca/categorias_form', $data);
        } else {
            $save['id'] = $id;
            $save['nombre'] = $this->input->post('nombre');
            $save['parent'] = $this->input->post('parent');
            $save['slug'] = $this->input->post('slug');
            $save['descripcion'] = strip_tags($this->input->post('descripcion'));
            $save['modulo'] = 'biblioteca';

            if ($this->input->post('nombre') != $data['nombre']) {
                $this->load->helper('text');
                $slug = url_title(convert_accented_characters($this->input->post('nombre')), 'dash', TRUE);

                if ($id) {
                    $slug = $this->categoria_model->validar_slug($slug, $id);
                } else {
                    $slug = $this->categoria_model->validar_slug($slug);
                }

                $save['slug'] = $slug;
            }

            if ($id) {
                $res = $this->categoria_model->update($id, $save);
            } else {
                $res = $this->categoria_model->insert($save);
            }

            if ($res) {
                $this->session->set_flashdata('mensajes', 'Se ha guardado la categoria: <b>' . $save['nombre'] . '</b>');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar la categoria');
            }

            redirect('admin/biblioteca/categorias');
        }
    }

    public function categoria_eliminar($id) {
        $id = (int) $id;
        $categoria = $this->categoria_model->get($id);
        if (!$categoria) {
            $this->session->set_flashdata('errores', 'No se encontro la categoría solicitada');
        }

        $this->categoria_model->delete($id);
        $this->session->set_flashdata('mensajes', 'Se ha eliminado la categoría');

        redirect('admin/biblioteca/categorias');
    }

    private function _array_dropdown($objs, $attr_key, $attr_val, $primero = NULL) {
        $array = array();

        if (!is_null($primero)) {
            $array[0] = $primero;
        }

        foreach ($objs as $value) {
            $array[$value->$attr_key] = $value->$attr_val;
        }

        return $array;
    }

    public function update_destacado() {
        $this->biblioteca_model->destacar($this->input->post('id'));
        echo '1';
    }

}
