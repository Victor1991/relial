<?php

class Paginas extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        
        $this->load->model('pagina_model');
    }
    
    public function index() {
        $data['paginas'] = $this->pagina_model->get_many_by();
        
        $this->view('paginas/index', $data);
    }
    
    public function form($id=FALSE) {
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'text'));
        
        $data['estatus_dpdwn'] = array(
            0 => 'No visible',
            1 => 'Visible'
        );
        
        $data['titulo_form'] = 'Nueva página';
        $data['id'] = FALSE;
        $data['titulo'] = '';
        $data['slug'] = '';
        $data['estatus'] = '';
        $data['contenido'] = '';
        
        if ($id) {
            $data = array_merge($data, (array) $this->pagina_model->get_by_id($id));  
            $data['titulo_form'] = 'Editar página';
        }

        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('contenido', 'Contenido', 'trim|required');
        $this->form_validation->set_rules('estatus', 'Estatus', 'trim');
        $this->form_validation->set_rules('slug', 'Slug', 'trim');
        
        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            
            $this->view('paginas/form', $data);
        } else {
            $save['titulo'] = $this->input->post('titulo');
            $save['estatus'] = $this->input->post('estatus');
            $save['contenido'] = $this->input->post('contenido');
            $save['slug'] = $this->input->post('slug');
            
            if ($this->input->post('titulo') != $data['titulo']) {
                $slug = url_title(convert_accented_characters($this->input->post('titulo')), 'dash', TRUE);
                $slug = $this->pagina_model->validar_slug($slug, $id);
                $save['slug'] = $slug;
            }
            if ($id) {
                if ($this->pagina_model->actualizar($id, $save)) {
                    $this->session->set_flashdata('mensajes', 'Se han guardado los cambios de la página.');
                } else {
                    $this->session->set_flashdata('errores', 'No se han podido guardar los cambios de la página.');
                }
            } else {
                if ($this->pagina_model->agregar($save)) {
                    $this->session->set_flashdata('mensajes', 'Se ha guardado la información de la nueva página.');
                } else {
                    $this->session->set_flashdata('errores', 'No se ha podido crear la nueva página.');
                }
            }
            
            redirect('admin/paginas/');
        }
    }
    
    public function eliminar($id) {
        if(is_numeric($id)){
            if ($this->pagina_model->delete($id)) {
                $this->session->set_flashdata('mensajes', 'Se ha eliminado la página del sistema');
            } else {
                $this->session->set_flashdata('errores', 'No fue posible eliminar la página. Por favor vuelva a intentarlo.');
            }
        }
        
        redirect('admin/paginas');
    }
}