<?php

class Calendario extends Admin_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->model(array('evento_model', 'mapa_model', 'hoteles_model', 'registro_model'));
    }

    public function index () {
        $data = array();

        $this->add_asset('css', 'admin/js/fullcalendar/bootstrap-fullcalendar.css');
        // $this->add_asset('js', 'admin/js/fullcalendar/fullcalendar.min.js');
        // $this->add_asset('js', 'admin/js/fullcalendar/lang/es.js');
        $this->add_asset('js', 'admin/js/calendario.js');
        $this->view('calendario/index', $data);
    }

    public function form ($id=FALSE) {

         $this->add_asset('css', 'admin/css/panel_tab.css');
         $this->add_asset('css', 'admin/plugins/sweetalert/sweetalert.css');
         $this->add_asset('js', 'admin/plugins/sweetalert/sweetalert.min.js');
         $this->add_asset('js', 'admin/js/calendario_form_hoteles.js');

         // config imagen
         $config['upload_path'] = 'uploads/eventos';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size'] = 1024 * 1024 * 2;
         $config['encrypt_name'] = true;
         $this->load->library('upload', $config, 'img');

         // config $pdf
         $config['upload_path'] = 'uploads/programa';
         $config['allowed_types'] = 'pdf';
         $config['max_size'] = 1024 * 1024 * 2;
         $config['encrypt_name'] = true;
         $this->load->library('upload', $config, 'pdf');



        $this->load->library('form_validation');
        $this->load->helper('form');

        $data['evento_id'] = $id;
        $data['titulo'] = '';
        $data['programa'] = '';
        $data['fecha_inicio'] = date('d-m-Y');
        $data['fecha_fin'] = date('d-m-Y');
        $data['hora_inicio'] = date('h:00 A');
        $data['hora_fin'] = date('h:00 A', strtotime('+1 hour'));
        $data['dia_completo'] = 0;
        $data['costo'] = '';
        $data['descripcion'] = '';
        $data['direccion'] = '';
        $data['imagen'] = '';
        $data['pais_id'] = '';
        $data['estado_id'] = '';
        $data['lat'] = '';
        $data['long'] = '';
        $data['paises'] = $this->mapa_model->get_all_paises();

        if ($id) {
            $evento = $this->evento_model->get_by_id($id);
            if(!$evento) {
                $this->session->set_flashdata('errores', 'No se encontro el evento solicitado.');
                redirect('admin/calendario');
            }

            $data['titulo'] = $evento->titulo;
            $data['fecha_inicio'] = mysql_to_date($evento->fecha_inicio);
            $data['hora_inicio'] = mysql_to_time($evento->hora_inicio);
            $data['fecha_fin'] = mysql_to_date($evento->fecha_fin);
            $data['hora_fin'] = mysql_to_time($evento->hora_fin);
            $data['dia_completo'] = $evento->dia_completo;
            $data['descripcion'] = $evento->descripcion;
            $data['direccion'] = $evento->direccion;
            $data['imagen'] = $evento->imagen;
            $data['costo'] = $evento->costo;
            $data['pais_id'] = $evento->pais_id;
            $data['estado_id'] = $evento->estado_id;
            $data['lat'] = $evento->lat;
            $data['long'] = $evento->long;
            $data['programa'] = $evento->programa;

        }

        $this->form_validation->set_rules('titulo','Título','trim|required|max_length[100]');
        $this->form_validation->set_rules('fecha_inicio','Inicio','trim|required');
        $this->form_validation->set_rules('fecha_fin','Fin','trim|required');

        if (!$this->input->post('dia_completo')) {
            $this->form_validation->set_rules('hora_inicio','Hora inicio','trim|required');
            $this->form_validation->set_rules('hora_fin','Hora fin','trim|required');
        }

        if ($this->form_validation->run() === FALSE) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }

            $this->add_asset('js', 'admin/js/calendario-form.js');
            $this->view('calendario/form', $data);
        } else {
            $uploaded = $this->img->do_upload('imagen');

            $save['id'] = $id;
            $save['titulo'] = strip_tags($this->input->post('titulo'));
            $save['fecha_inicio'] = date_to_mysql($this->input->post('fecha_inicio'));
            $save['fecha_fin'] = date_to_mysql($this->input->post('fecha_fin'));
            $save['dia_completo'] = (int)$this->input->post('dia_completo');
            $save['hora_inicio'] = $save['dia_completo'] ? NULL : time_to_mysql($this->input->post('hora_inicio'));
            $save['hora_fin'] = $save['dia_completo'] ? NULL : time_to_mysql($this->input->post('hora_fin'));
            $save['direccion'] = $this->input->post('direccion');
            $save['descripcion'] = $this->input->post('descripcion');
            $save['costo'] = $this->input->post('costo');

            $save['pais_id'] =  $this->input->post('pais_id');
            $save['estado_id'] =  $this->input->post('estado_id');
            $save['lat'] =  $this->input->post('lat');
            $save['long'] =  $this->input->post('long');


            if ($id) {  //actualizar evento
                if ($uploaded) {
                    if ($data['imagen'] != '') {
                        $file = 'uploads/eventos/' . $data['imagen'];

                        //eliminar la imagen anterior si existe
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {    //nuevo evento
                if (!$uploaded) {
                    $this->session->set_flashdata('errores', $this->img->display_errors());
                    $this->view('calendario/form', $data);
                    return;
                }
            }

            if ($uploaded) {
                $image = $this->img->data();
                $save['imagen'] = $image['file_name'];
            }

            $uploaded_prog = $this->pdf->do_upload('programa');

              if ($id) {  //actualizar evento
                  if ($uploaded_prog)  {
                      if ($data['programa'] != '') {
                          $file = 'uploads/programa/' . $data['programa'];

                          //eliminar la imagen anterior si existe
                          if (file_exists($file)) {
                              unlink($file);
                          }
                      }
                  }
              }

              if ($uploaded_prog) {
                  $pdf = $this->pdf->data();
                  $save['programa'] = $pdf['file_name'];
              }


            $save_id = $this->evento_model->guardar($save);
            if ($save_id) {
                $this->session->set_flashdata('mensajes', 'Se guardo el evento: ' . $save['titulo']);
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar el evento: ' . $save['titulo']);
            }

            redirect('admin/calendario/form/'.$save_id);
        }

    }


    public function form_calendario($id=FALSE) {

         $this->add_asset('css', 'admin/css/panel_tab.css');
         $this->add_asset('css', 'admin/plugins/sweetalert/sweetalert.css');
         $this->add_asset('js', 'admin/plugins/sweetalert/sweetalert.min.js');
         $this->add_asset('js', 'admin/js/calendario_form_hoteles.js');

         // config imagen
         $config['upload_path'] = 'uploads/eventos';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size'] = 1024 * 1024 * 2;
         $config['encrypt_name'] = true;
         $this->load->library('upload', $config, 'img');

        $this->load->library('form_validation');
        $this->load->helper('form');

        $data['evento_id'] = $id;
        $data['titulo'] = '';
        $data['programa'] = '';
        $data['fecha_inicio'] = date('d-m-Y');
        $data['fecha_fin'] = date('d-m-Y');
        $data['hora_inicio'] = date('h:00 A');
        $data['hora_fin'] = date('h:00 A', strtotime('+1 hour'));
        $data['dia_completo'] = 0;
        $data['descripcion'] = '';
        $data['imagen'] = '';

        if ($id) {
           $evento = $this->evento_model->get_by_id($id);
           if(!$evento) {
                $this->session->set_flashdata('errores', 'No se encontro el evento solicitado.');
                redirect('admin/calendario');
           }

           $data['titulo'] = $evento->titulo;
           $data['fecha_inicio'] = mysql_to_date($evento->fecha_inicio);
           $data['hora_inicio'] = mysql_to_time($evento->hora_inicio);
           $data['fecha_fin'] = mysql_to_date($evento->fecha_fin);
           $data['hora_fin'] = mysql_to_time($evento->hora_fin);
           $data['dia_completo'] = $evento->dia_completo;
           $data['descripcion'] = $evento->descripcion;
           $data['direccion'] = $evento->direccion;
           $data['imagen'] = $evento->imagen;

        }

        $this->form_validation->set_rules('titulo','Título','trim|required|max_length[100]');
        $this->form_validation->set_rules('fecha_inicio','Inicio','trim|required');
        $this->form_validation->set_rules('fecha_fin','Fin','trim|required');

        if (!$this->input->post('dia_completo')) {
           $this->form_validation->set_rules('hora_inicio','Hora inicio','trim|required');
           $this->form_validation->set_rules('hora_fin','Hora fin','trim|required');
        }

        if ($this->form_validation->run() === FALSE) {
           if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
           }

           $this->add_asset('js', 'admin/js/calendario-form.js');
           $this->view('calendario/form_evento_calendario', $data);
        } else {
           $uploaded = $this->img->do_upload('imagen');

           $save['id'] = $id;
           $save['titulo'] = strip_tags($this->input->post('titulo'));
           $save['fecha_inicio'] = date_to_mysql($this->input->post('fecha_inicio'));
           $save['fecha_fin'] = date_to_mysql($this->input->post('fecha_fin'));
           $save['dia_completo'] = (int)$this->input->post('dia_completo');
           $save['hora_inicio'] = $save['dia_completo'] ? NULL : time_to_mysql($this->input->post('hora_inicio'));
           $save['hora_fin'] = $save['dia_completo'] ? NULL : time_to_mysql($this->input->post('hora_fin'));
           $save['direccion'] = $this->input->post('direccion');
           $save['descripcion'] = $this->input->post('descripcion');
           $save['en_calendario'] = '1';



           if ($id) {  //actualizar evento
                if ($uploaded) {
                    if ($data['imagen'] != '') {
                        $file = 'uploads/eventos/' . $data['imagen'];

                        //eliminar la imagen anterior si existe
                        if (file_exists($file)) {
                           unlink($file);
                        }
                    }
                }
           } else {    //nuevo evento
                if (!$uploaded) {
                    $this->session->set_flashdata('errores', $this->img->display_errors());
                    $this->view('calendario/form_evento_calendario', $data);
                    return;
                }
           }

           if ($uploaded) {
                $image = $this->img->data();
                $save['imagen'] = $image['file_name'];
           }


           $save_id = $this->evento_model->guardar($save);
           if ($save_id) {
                $this->session->set_flashdata('mensajes', 'Se guardo el evento: ' . $save['titulo']);
           } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar el evento: ' . $save['titulo']);
           }

           redirect('admin/calendario/form_calendario/'.$save_id);
        }

    }



    public function eventos() {
        //plugin fullcalendar ?start=2013-12-01&end=2014-01-12&_=1386054751381
        $filtros['inicio'] = $this->input->get('start');
        $filtros['fin'] = $this->input->get('end');

        $filtros['inicio'] = date("Y-m-d", $filtros['inicio']);
        $filtros['fin'] = date("Y-m-d", $filtros['fin']);

        $eventos = $this->evento_model->get_many_by($filtros);

        if ($this->input->is_ajax_request()) {
            $res = $this->_prepare_results_fullcalendar($eventos);

            echo json_encode($res);
        }
    }



    public function eliminar($id) {
        $id = is_numeric($id) ? (int) $id : 0;
        $evento = $this->evento_model->get_by_id($id);

        if (!$evento) {
            $this->session->set_flashdata('No se encontro el evento solicitado');
            redirect('admin/calendario');
        } elseif ($evento->imagen != '') {
            $file = 'uploads/eventos/' . $evento->imagen;
            //eliminar la imagen
            if (file_exists($file)) {
                unlink($file);
            }
        }

        $this->evento_model->eliminar($id);
        $this->session->set_flashdata('mensajes', 'Se ha eliminado el evento: ' . $evento->titulo);
        redirect('admin/calendario');
    }


    private function _prepare_results_fullcalendar($rows) {
        $results = array();
        foreach ($rows as $row) {
            $color = $row->en_calendario == 1 ? '#1fb5ad' : '#57c8f1';
            $url = $row->en_calendario == 1 ? base_url('admin/calendario/form_calendario/'.$row->id) : base_url('admin/calendario/form/'.$row->id);
            $res = array(
                'id' => $row->id,
                'color' => $color,
                'title' => $row->titulo,
                'allDay' => (bool)$row->dia_completo,
                'start' => $row->dia_completo ? $row->fecha_inicio : $row->fecha_inicio . ' ' . $row->hora_inicio,
                'end' => $row->dia_completo ? $row->fecha_fin : $row->fecha_fin . ' ' . $row->hora_fin,
                'url' => $url
            );
            $results[] = $res;
        }

        return $results;
    }

     public function guardar_hotel(){
          $this->load->library('form_validation');
          $this->form_validation->set_rules('nombre_hotel','Nombre Hotel','trim|required');
          $this->form_validation->set_rules('direccion_hotel','Dirección','trim|required');

          $save['logo'] = '';
          if ($_FILES['imagen']['name'] != '') {
               // Config upload
               $config['upload_path'] = 'uploads/logo_hotel/';
               $config['allowed_types'] = "gif|jpg|jpeg|png";
               $config['max_size'] = 1024 * 1024 * 5;
               $config['encrypt_name'] = true;
               $this->load->library('upload', $config);

               $this->upload->initialize($config);
               if ($this->upload->do_upload('imagen')) {
                   $save['logo'] = $this->upload->data('file_name');
               } else {
                    $respuesta = array(
                         'tipo' => 'error',
                         'mensaje' => $this->upload->display_errors()
                    );
                    echo json_encode($respuesta, true);
                    die;
               }
          }

          $respuesta = array();
          if ($this->form_validation->run() == TRUE) {
               $save['nombre'] = $this->input->post('nombre_hotel');
               $save['ubicacion'] = $this->input->post('direccion_hotel');
               $save['evento_id'] = $this->input->post('evento_id');
               $save['tarifa'] = $this->input->post('tarifa');
               $save['lat'] = $this->input->post('lat1');
               $save['long'] = $this->input->post('long1');

               if ($this->input->post('hotel_id')) {
                    if ($save['logo'] == '') {
                         unset($save['logo']);
                    }
                    $saved = $this->hoteles_model->update($this->input->post('hotel_id'), $save);
               }else{
                    $saved = $this->hoteles_model->insert($save);
               }
               if ($saved) {
                    $respuesta = array(
                         'tipo' => 'success',
                         'mensaje' => 'Éxito al guardar la información'
                    );
               }else{
                    $respuesta = array(
                         'tipo' => 'error',
                         'mensaje' => 'Error al guardar la información'
                    );
               }

          }else{
               $respuesta = array(
                    'tipo' => 'validacion',
                    'mensaje' => $this->form_validation->error_array()
               );
          }
          echo json_encode($respuesta, true);
     }


     public function get_hoteles_evento(){
          $evento_id = $this->input->post('evento_id');
          $hoteles = $this->hoteles_model->get_hotele_evento($evento_id);
          echo json_encode($hoteles);
     }

     public function estatus_registro($estatus){
          if ($estatus == 0) {
               return '<span class="label label-primary">Por definir</span>';
          }else if ($estatus == 1) {
               return '<span class="label label-success">Aceptado</span>';
          }else if ($estatus == 3) {
               return '<span class="label label-warning">Información incompleta </span>';
          }else{
               return '<span class="label label-danger">Denegado</span>';
          }
     }

     public function get_registro($evento_id){
          $json = array('data' => array());
          $return = $this->registro_model->get_registro_evento($evento_id);
          foreach ($return as $key => $value) {
               $json['data'][$key] = array(
                                        $value->id,
                                        $value->nombre,
                                        $value->apellidos,
                                        $value->organizacion,
                                        $value->pais,
                                        $value->correo,
                                        $value->telefono,
                                        $this->estatus_registro($value->estatus),
                                        '<button onclick="ver_informacion('.$value->id.')" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Ver</button>',
                                   );
          }
          echo json_encode($json, true);
     }

     public function evento($evento_id){
          $evento = $this->registro_model->get_by_id($evento_id);
          echo json_encode($evento);
     }

     public function cambiar_estatus(){
          $save['estatus'] = $this->input->post('estatus');
          $mens = array('tipo' => 'error', 'mens' => 'Error al guardar la información');
          $cambio = $this->registro_model->update($this->input->post('id'), $save);
          if ($cambio) {
               if ($save['estatus'] == 1) {
                    $this->mail_confirmacion($this->input->post('id'), true);
               }else if($save['estatus'] == 3) {
                    $this->mail_confirmacion($this->input->post('id'));
               }
               $mens['tipo'] = 'success';
               $mens['mens'] = 'Exito al guardar la información';
          }
          echo json_encode($mens);
     }

     public function mail_confirmacion($registro, $exito = false){
          $config['protocol'] = 'mail';
          $config['charset'] = 'utf-8';
          $config['mailtype'] = 'html';
          $config['wordwrap'] = TRUE;
          $this->email->initialize($config);
          $this->email->from('silvia.mercado@fnst.org', 'Contacto Relial');
          $return = $this->registro_model->get_by_id($registro);
          $return->evento = $this->evento_model->get_by_id($return->evento_id);
          $this->email->to($return->correo);
          // $this->email->message($this->load->view('admin/mail/confirmacion', $return, TRUE));
          if ($exito) {
               $this->email->subject('Registro exitoso');
               $this->email->message($this->load->view('admin/mail/confirmacion', $return, TRUE));

               // $this->load->view('admin/mail/confirmacion', $return);
          }else{
               $this->email->subject('Información incompleta');
               $this->email->message($this->load->view('admin/mail/informacion_incompleta', $return, TRUE));

               // $this->load->view('admin/mail/informacion_incompleta', $return);
          }
          $this->email->send();
     }



}
