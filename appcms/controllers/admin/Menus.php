<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends Admin_Controller {

    /**
     * The current active section.
     *
     * @var int
     */
    protected $section = 'links';

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('menu_model');
        $this->load->model('pagina_model');
//
////        $this->template
////                ->append_js('module::navigation.js')
////                ->append_css('module::navigation.css');
//
//        // get menus del sitio
//        $data['menus'] = $this->menu_model->get_grupos();
//        $data['dpdwn_menus'] = array();
//        foreach ($data['menus'] as $value) {
//            $data['dpdwn_menus'][$value->id] = $value->titulo; 
//        }
//
//        // get páginas del sitio
//        $tree = array();
//
//        if ($paginas = $this->pagina_model->get_all()) {
//            foreach ($paginas as $pagina) {
//                $tree[$pagina->parent_id][] = $pagina;
//            }
//        }
//
//        unset($pages);
//        $this->template->pages_select = $tree;
//
//        // Set the validation rules for the navigation items
//        $this->form_validation->set_rules($this->validation_rules);
    }

    public function index() {
        $this->load->helper('form');
        $menu_links = $this->menu_model->get_menu_tree($this->session->userdata('rol_id'), false);
//        _dump($menu_links);
//        $menus = array();
//        
//        foreach ($this->grupos as $grupo) {
//            $menus[$grupo->id] = $this->menu_model->get_link_tree($grupo->id);
//        }
//
        $data['menu'] = $menu_links;
        $paginas = $this->pagina_model->get_many_by(array(), array('titulo' => 'asc'));
        $data['dpdwn_paginas'] = array();
        foreach ($paginas as $pagina) {
            $data['dpdwn_paginas'][$pagina->id] = $pagina->titulo; 
        }
        
        $this->add_asset('css', 'admin/js/nestable/jquery.nestable.css');
        $this->add_asset('js', 'admin/js/nestable/jquery.nestable.js');
        $this->add_asset('js', 'admin/js/admin-menu.js');
        
        $this->view('menus/index', $data);
    }

    /**
     * Ordenar los links
     */
    public function order() {
        $params = $this->input->post('params');                
        $params = json_decode($params);
        
        if (is_array($params)) {
            //reset all parent > child relations
            $this->menu_model->actualiza_orden($params);
            echo json_encode(array('estatus' => 'ok', 'mensaje' => 'Los datos han sido actualizados'));
        } else {
            echo json_encode(array('estatus' => 'error', 'mensaje' => 'No se pudo realizar la actualización. Los parametros enviados son erroneos'));
        }
    }
    
    public function crear_link() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        if (!$this->input->is_ajax_request()) {
            show_404();
            die;
        }
        
        $this->form_validation->set_rules('tipo_enlace', 'Tipo de enlace', 'trim|required');
        $this->form_validation->set_rules('titulo', 'Título', 'trim|required');
        $this->form_validation->set_rules('target', 'Abrir link en', 'trim|required');
        
        if ($this->form_validation->run()) {
            $save['tipo_link'] = $this->input->post('tipo_enlace');
            $save['titulo'] = $this->input->post('titulo');
            $save['target'] = $this->input->post('target');
            
            switch ($save['tipo_link']) {
                case 'url':
                    $save['url'] = trim($this->input->post('url'));
                    $save['uri'] = '';
                    $save['pagina_id'] = 0;
                    break;
                case 'uri':
                    $save['url'] = '';
                    $save['uri'] = trim($this->input->post('uri'));
                    $save['pagina_id'] = 0;
                    break;
                case 'pagina':
                    $save['url'] = '';
                    $save['uri'] = '';
                    $save['pagina_id'] = (int)$this->input->post('pagina');
                    break;
                default :
                    $save['url'] = '';
                    $save['uri'] = '';
                    $save['pagina_id'] = 0;
                    break;                
            }
            
            $id = $this->menu_model->crear($save);
            
            if ($id) {
                $save['id'] = $id;
                echo json_encode(array(
                    'estatus' => 'ok',
                    'mensaje' => 'El link se ha creado.',
                    'item' => $save
                ));
            } else {
                echo json_encode(array(
                    'estatus' => 'error',
                    'mensaje' => 'Hubo un error al crear el link, por favor vuelva a intentarlo.'
                ));
            }            
        } else {
            echo json_encode(array(
                'estatus' => 'error',
                'mensaje' => validation_errors()
            ));
        }
    }
    
    public function editar($id) {
        $id = (int)$id;
        $link = $this->menu_model->get_link_by_id($id);
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        if (!$link) {
            show_404();
        } 
        
        $this->form_validation->set_rules('tipo_enlace', 'Tipo de enlace', 'trim|required');
        $this->form_validation->set_rules('titulo', 'Título', 'trim|required');
        $this->form_validation->set_rules('target', 'Abrir link en', 'trim|required');
        
        if ($this->form_validation->run()) {
            $update = array(
                'titulo' => $this->input->post('titulo'),
                'tipo_link' => $this->input->post('tipo_enlace'),
                'target' => $this->input->post('target')
            );
            
            switch ($update['tipo_link']) {
                case 'url':
                    $update['url'] = trim($this->input->post('url'));
                    $update['uri'] = '';
                    $update['pagina_id'] = 0;
                    break;
                case 'uri':
                    $update['url'] = '';
                    $update['uri'] = trim($this->input->post('uri'));
                    $update['pagina_id'] = 0;
                    break;
                case 'pagina':
                    $update['url'] = '';
                    $update['uri'] = '';
                    $update['pagina_id'] = (int)$this->input->post('pagina');
                    break;
                default :
                    $update['url'] = '';
                    $update['uri'] = '';
                    $update['pagina_id'] = 0;
                    break;
            }
            
            if ($this->menu_model->actualizar($id, $update)) {
                echo json_encode(array(
                    'estatus' => 'ok',
                    'mensaje' => 'Los datos han sido actualizados'
                ));
            } else {
                echo json_encode(array(
                    'estatus' => 'error',
                    'mensaje' => 'Hubo un erro al actualizar los datos'
                ));
            }
        } else {
            $data['link'] = $link;
            $paginas = $this->pagina_model->get_many_by(array(), array('titulo' => 'asc'));
            $data['dpdwn_paginas'] = array();
            foreach ($paginas as $pagina) {
                $data['dpdwn_paginas'][$pagina->id] = $pagina->titulo; 
            }

            $this->load->view('admin/menus/ajax/form', $data);
        }
    }

    public function ajax_link_detalle($link_id) {
        $link = $this->menu_model->get_url($link_id);

        $this->load->view('admin/ajax/link_details', array('link' => $link));
    }

    /**
     * Delete an existing navigation link
     *
     * @param int $id The ID of the navigation link
     */
    public function eliminar() {
        if (!$this->input->is_ajax_request()) {
            show_404();
            die;
        }
        
        $id = (int)$this->input->post('item');
        
         $this->menu_model->eliminar_hijos($id);
        $this->menu_model->eliminar($id);
        
        echo json_encode(array('estatus' => 'ok', 'mensaje' => 'Link eliminado'));
    }
    
}
