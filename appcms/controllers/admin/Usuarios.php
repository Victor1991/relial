<?php

class Usuarios extends Admin_Controller {
    private $validation_rules = array(
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Contraseña',
            'rules' => 'min_length[6]|max_length[20]'
        ),
        'username' => array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|min_length[3]|max_length[20]'
        ),
        array(
            'field' => 'conf_password',
            'label' => 'Confirmar contraseña',
            'rules' => 'matches[password]'
        ),
        array(
            'field' => 'rol_id',
            'label' => 'Rol',
            'rules' => 'required|integer'
        )
    );

    public function __construct() {
        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->model('rol_model');
    }
    
    public function index() {
        $this->load->helper(array('pagination', 'form'));
        
        $where = array('activo' => 0);
        
        //Filtros
        $where['activo'] = $this->input->post('f_activo') ? (int)$this->input->post('f_activo') : $where['activo'];
        
        if ($this->input->post('f_rol')) {
            $where['rol_id'] = (int) $this->input->post('f_rol');
        }
        if ($this->input->post('f_buscar')) {
            $where['nombre'] = $this->input->post('f_buscar');
        }
        
        $pagination = create_pagination('admin/usuarios/index', $this->usuario_model->count_by($where), 50);
        
        $tabla['usuarios'] = $this->usuario_model->get_many_by($where, $pagination['limit'], $pagination['offset']);        
        $tabla['pagination'] = $pagination;
        $data['tabla'] = $this->load->view('admin/usuarios/tabla', $tabla, TRUE);
        
        if ($this->input->is_ajax_request()) {
            echo $data['tabla'];
        } else {
            $data['errores'] = $this->session->flashdata('errores');
            $data['mensajes'] = $this->session->flashdata('mensajes');
            $data['roles_dpdwn'] = $this->rol_model->get_opciones_dropdown('-- Todos --');
            $this->add_asset('js', 'admin/js/admin-filtros.js');
            $this->add_asset('js', 'admin/js/usuarios/admin-usuarios.js');
            $this->view('usuarios/index', $data);
        }
    }
    
    public function agregar() {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        $this->validation_rules['email']['rules'] .= '|callback__email_check';
        $this->validation_rules['password']['rules'] .= '|required';
        $this->validation_rules['username']['rules'] .= '|callback__username_check';
        
//        $this->form_validation->set_rules('rol', 'Rol', 'trim|required|integer');
//        $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[12]|is_unique[usuarios.username]');
//        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
//        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[8]');
//        $this->form_validation->set_rules('conf_password', 'Confirmar contraseña', 'matches[password]');
        
        $this->form_validation->set_rules($this->validation_rules);
        
        if ($this->form_validation->run() === FALSE) {
            $data['errores'] = validation_errors() ? validation_errors() : $this->session->flashdata('errores');
            $data['roles_dpdwn'] = $this->rol_model->get_opciones_dropdown(); 
            
            $this->view('usuarios/agregar', $data);
        } else {
            $save['rol_id'] = $this->input->post('rol_id');
            $save['nombre'] = $this->input->post('nombre');
            $save['apellidos'] = $this->input->post('apellidos');
            $save['email'] = $this->input->post('email');
            $save['username'] = $this->input->post('username');
            $save['password_hashed'] = $this->auth->hash_password($this->input->post('password'));
            
            $id = $this->usuario_model->insert($save);
            if ($id) {
                $this->session->set_flashdata('mensajes', 'Nuevo usuario guardado.');
            } else {
                $this->session->set_flashdata('errores', 'Error: No se pudo guardar el nuevo usuario.');
            }
            
            redirect('admin/usuarios');
        }
        
    }
    
    public function editar($id = 0) {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        if (!($usuario = $this->usuario_model->get_by_id($id))) {
            $this->session->set_flashdata('errores', 'No se encontro el usuario solicitado.');
            redirect('admin/usuarios');
        }

        //Verificamos si se cambia el username
        if ($usuario->username != $this->input->post('username')) {
            $this->validation_rules['username']['rules'] .= '|callback__username_check';
        }

        // verificamos si se intenta cambiar el email
        if ($usuario->email != $this->input->post('email')) {
            $this->validation_rules['email']['rules'] .= '|callback__email_check';
        }

        // validaciones
        $this->form_validation->set_rules($this->validation_rules);

        if ($this->form_validation->run() === true) {
            $update['nombre'] = $this->input->post('nombre');
            $update['apellidos'] = $this->input->post('apellidos');
            $update['email'] = $this->input->post('email');
            $update['activo'] = $this->input->post('activo');
            $update['username'] = $this->input->post('username');
            // solo se permite asignar el rol de admin a los administradores
            $update['rol_id'] = (!$this->auth->is_admin() && $this->input->post('rol_id') == 1) ? $usuario->rol_id : $this->input->post('rol_id');

            // Password provided, hash it for storage
            if ($this->input->post('password')) {
                $update['password_hashed'] = $this->auth->hash_password($this->input->post('password'));
            }

            if ($this->usuario_model->update($id, $update)) {
                $this->session->set_flashdata('mensajes', 'Los datos han sido actualizados');
            } else {
                $this->session->set_flashdata('errores', 'No fue posible actualizar los datos.');
            }

            redirect('admin/usuarios');
        } else {
            $data['usuario'] = $usuario;
            $data['roles_dpdwn'] = $this->rol_model->get_opciones_dropdown();
            $data['errores'] = validation_errors() ? validation_errors() : $this->session->flashdata('errores');
            
            $this->view('usuarios/editar', $data);
        }
        
    }
    
    public function eliminar($id=0) {
        $ids = ($id > 0) ? array($id): $this->input->post('action_to');
        
        if (!empty($ids)) {
            $eliminados = 0;
            $a_eliminar = 0;
            $eliminados_ids = array();
            foreach ($ids as $id) {
                // Evitamos que el administrador se elimine a si mismo
                if ($this->session->userdata('usuario_id') == $id) {
                    $this->session->set_flashdata('errores', 'ERROR: No puedes borrarte a ti mismo!');
                    continue;
                }

                if ($this->usuario_model->delete($id)) {
                    $eliminados_ids[] = $id;
                    $eliminados++;
                }
                
                $a_eliminar++;
            }

            if ($a_eliminar > 0) {
                $this->session->set_flashdata('mensajes', $eliminados . ' usuarios de ' . $a_eliminar . ' han sido borrados.');
            }
        } else {
            $this->session->set_flashdata('errores', 'Es necesario seleccionar algunos usuarios.');
        }

        redirect('admin/usuarios');
    }

    public function _username_check() {
        if ($this->auth->existe_username($this->input->post('username'))) {
            $this->form_validation->set_message('_username_check', 'El username ya existe');
            return false;
        }
        return true;
    }
    
    public function _email_check() {
        if ($this->auth->existe_email($this->input->post('email'))) {
            $this->form_validation->set_message('_email_check', 'El email ya es usado por otra cuenta.');
            return false;
        }
        return true;
    }

}