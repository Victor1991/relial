<?php

class Directorio extends Admin_Controller {
    private $max_length_desc;

    public function __construct() {
        parent::__construct();

        $this->load->model('banner_model');
        $this->load->model('mapa_model');

        //caracteres máximos en la descripción
        $this->max_length_desc = 150;
        $this->load->library('form_validation');
        $this->load->helper('form');

    }

    public function index() {
         $id = 2;
         $data['banners_grupo'] = $this->banner_model->get_grupo($id);
         if (!$data['banners_grupo']) {
              $this->session->set_flashdata('errores', 'No se encontro el grupo de banners solicitado.');
              redirect('admin/lista');
         }

         $data['id'] = $id;
         $data['paises'] = $this->mapa_model->get_all_paises();
         $data['banners'] = $this->banner_model->get_banners_por_grupo($id);
         $this->add_asset('js', 'admin/js/banners/admin-banners.js');
         $this->add_asset('js', 'admin/js/directorio.js');

         $this->view('directorio/lista', $data);
    }


    public function eliminar_grupo($id) {
        $grupo = $this->banner_model->get_grupo($id);
        if (!$grupo) {
            $this->session->set_flashdata('errores', 'No se encontro el grupo de banners solicitado.');
        } else {
            $this->banner_model->eliminar_banners_grupo($id);
            $this->session->set_flashdata('mensajes', 'Se ha eliminado el grupo de banners.');
        }
        redirect('admin/banners');
    }

    public function grupo($id) {
        $data['banners_grupo'] = $this->banner_model->get_grupo($id);
        if (!$data['banners_grupo']) {
            $this->session->set_flashdata('errores', 'No se encontro el grupo de banners solicitado.');
            redirect('admin/banners');
        }

        $data['id'] = $id;
        $data['banners'] = $this->banner_model->get_banners_por_grupo($id);

        $this->add_asset('js', 'admin/js/banners/admin-banners.js');

        $this->view('banners/banners', $data);
    }

    public function insert(){
         // config imagen
         $config['upload_path'] = 'uploads/banners/';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size'] = 1024 * 1024 * 2;
         $config['encrypt_name'] = true;
         $this->load->library('upload', $config ,'img');

          $this->form_validation->set_rules('titulo','Título','trim|required|max_length[100]');
          if ($this->form_validation->run() === FALSE) {
              if (validation_errors()) {
                 echo json_encode(array('tipo' => 'validacion', 'mensaje' => $this->form_validation->error_array()));
              }
          } else {

              $uploaded = $this->img->do_upload('imagen');
               if ($this->input->post('banner_id')) {
                       $save['id'] = $this->input->post('banner_id');
               }

              $save['titulo'] = strip_tags($this->input->post('titulo'));
              $save['mail'] = $this->input->post('correo');
              $save['telefono'] = $this->input->post('telefono');
              $save['descripcion'] = $this->input->post('descripcion');
              $save['link'] = $this->input->post('link');
              $save['nueva_ventana'] = $this->input->post('nueva_ventana') ? 1 : 0;
              $save['estatus'] = $this->input->post('estatus') ? 1: 0;
              $save['banners_grupo_id'] = 2;
              $save['pais_id'] = $this->input->post('pais_id');

              if ($_FILES['imagen']['name'] != '') {


                   if ($uploaded) {
                        $image = $this->img->data();
                        $save['imagen'] = $image['file_name'];
                   }else {
                        echo json_encode(array('tipo' => 'error', 'mensaje' =>  $this->img->display_errors()));
                   }
              }

              $save_id = $this->banner_model->guardar_banner($save);
              if ($save_id) {
                   echo json_encode(array('tipo' => 'success', 'mensaje' =>  'Se guardo el evento: ' . $save['titulo']));
              } else {
                 echo json_encode(array('tipo' => 'error', 'mensaje' =>  'Se guardo el evento: ' . $save['titulo']));
              }

    }
}

     public function eliminar_banner($banner_id) {
          $banner = $this->banner_model->get_banner($banner_id);
          if (!$banner) {
               echo json_encode(array('tipo' => 'error', 'mensaje' =>  'No se encontro en directorio'));
          } else {
               $eliminar = $this->banner_model->eliminar_banner($banner_id);
               if ($eliminar) {
                    echo json_encode(array('tipo' => 'success', 'mensaje' =>  'Se guardo el directorio '));
               } else {
                    echo json_encode(array('tipo' => 'success', 'mensaje' =>  'Error al eliminar, el directorio '));
               }
          }

     }

    public function organizar() {
        $banners = $this->input->post('orden');

        if (is_array($banners)) {
            $this->banner_model->organizar($banners);
        }

        echo json_encode(array('status' => 'ok', 'message' => 'success'));
    }

    public function _imagen_check() {
        if ($_FILES['imagen']['tmp_name'] == "") {
            $this->form_validation->set_message('_imagen_check', 'La imagen es obligatoria.');
            return false;
        }
        return true;
    }
}
