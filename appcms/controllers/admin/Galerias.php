<?php

class Galerias extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        
        $this->load->model('galeria_model');
    }
    
    public function index () {        
        
        $data['galerias'] = $this->galeria_model->get_all();
        
        $this->add_asset('js', 'admin/js/admin-galerias.js');
        $this->view('galerias/index', $data);
    }
    
    public function form($id = FALSE) {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        $data = array(
            'id' => $id,
            'titulo' => '',
            'estatus' => 0,
            'fecha_vista' => mysql_to_date(date('Y-m-d')),
            'inicio' => 1
        );
        
        if ($id) {
            $galeria = $this->galeria_model->get_by_id($id);
            
            $data['id'] = $galeria->id;
            $data['titulo'] = $galeria->titulo;
            $data['estatus'] = $galeria->estatus;
            $data['fecha_vista'] = $galeria->fecha_vista;
            $data['inicio'] = $galeria->inicio;
        }
        
        $this->form_validation->set_rules('titulo', 'Título', 'trim|required');
        $this->form_validation->set_rules('activa', 'Activa', 'trim|required|integer');
        
        if ($this->form_validation->run() === FALSE) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            
            $this->view('galerias/form', $data);
        } else{
            $save = array(
                'id' => $id,
                'titulo' => $this->input->post('titulo'),
                'estatus' => $this->input->post('activa'),
                'fecha_vista' => $this->input->post('fecha_vista'),
                'inicio' => $this->input->post('inicio') ? 1:0
            );
            
            $id = $this->galeria_model->guardar($save);
            
            if ($this->input->is_ajax_request()) {
                $response['estatus'] = $id ? 'OK' : 'ERROR';
                $response['mensaje'] = $id ? 'Datos guardados' : 'ERROR: No fueron guardado los datos.';
                
                echo json_encode($response);
            } else {
                if ($id) {
                    $this->session->set_flashdata('mensajes', 'Se ha guardado la galería.');
                    redirect('admin/galerias/detalle/'.$id);
                } else {
                    $this->session->set_flashdata('errores', 'No se guardo la galería, por favor, vuleva a intentarlo');
                    redirect('admin/galerias', 'refresh');
                }
            }
        }
        
    }
    
    public function detalle($id) {
        $this->load->helper('form');
        
        $id = (int) $id;
        
        $galeria = $this->galeria_model->get_by_id($id);
        
        if (!$galeria) {
            $this->session->set_flashdata('errores', 'No se encontro la galería solicitada.');
            redirect('admin/galerias');
        }
        
        $galeria->imagenes = $this->galeria_model->get_imagenes($galeria->id);
//        _dump($galeria->imagenes);die;
        $data['galeria'] = $galeria;
        
        $this->add_asset('js', 'admin/plugins/ajaxFileUploader/ajaxfileupload.js');
        $this->add_asset('js', 'admin/js/detalle-galeria.js'); 
        
        $this->view('galerias/detalle', $data);
    }
    
    public function eliminar($id) {
        $galeria = $this->galeria_model->get_by_id($id);
        
        if ($galeria) {
            $this->_vaciar_galeria($id);
            $this->galeria_model->eliminar($id);
            $this->session->set_flashdata('mensajes', 'La galería ha sido eliminada');
        } else {
            $this->session->set_flashdata('errores', 'No se encontro la galería solicitada.');
        }
        
        redirect('admin/galerias');
    }


//    public function detalle($id) {
//        $this->load->model('usuario_model');
//        $id = (int) $id;
//        
//        $galeria = $this->galeria_model->get_by_id($id);
//        
//        if (!$galeria) {
//            show_404();
//            die;
//        }
//        
//        $galeria->imagenes = $this->galeria_model->get_imagenes($galeria->id);
//        $data['diputados_dpdwn'] = $this->usuario_model->options_dropdown(FALSE, 'diputado');
//        $data['galeria'] = $galeria;
//        $data['errores'] = $this->session->flashdata('errores');
//        $data['mensajes'] = $this->session->flashdata('mensajes');
//        
//        $this->view('admin/galeria/detalle', $data);
//    }
    
    
    public function guardar_imagen() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $config['upload_path'] = 'uploads/galerias';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 1024 * 10;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);
        
        $id = $this->input->post('id');
        //_dump($_FILES['img_archivo']);die;;
        if ($id) {
            $imagen = $this->galeria_model->get_imagen($id);
            if (!$imagen) {
                echo json_encode(array('estatus' => 'ERROR', 'mensaje' => 'No se encontro la imagen solicitada'));
                return;
            }
        } else {
            //$this->form_validation->set_rules('img_archivo', 'Imagen', 'callback__imagen_check');
        }
        
        $this->form_validation->set_rules('galeria', 'Galería', 'trim|required|integer');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|max_length[200]');        
        
        if ($this->form_validation->run() === false) {
            echo json_encode(array('estatus' => 'ERROR', 'mensaje' => validation_errors()));
            return;
        } else {
            
            $uploaded = $this->upload->do_upload('img_archivo');
            if ($id) {  //actualizar imagen
                $save['id'] = $id;
                if ($uploaded) {
                    if ($imagen->archivo != '') {
                        $file = 'uploads/galerias/' . $imagen->archivo;

                        //eliminar la imagen anterior si existe
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {    //nueva imagen
                if (!$uploaded) {
                    echo json_encode(array('estatus' => 'ERROR', 'mensaje' => $this->upload->display_errors()));
                    return;
                }
            }

            if ($uploaded) {
                $image = $this->upload->data();
                $save['archivo'] = $image['file_name'];
            }
            
            $save['descripcion'] = strip_tags($this->input->post('descripcion'));
            $save['galeria_id'] = $this->input->post('galeria');
            $save['principal'] = (int)$this->input->post('principal');
            $save['mostrar'] = (int)$this->input->post('mostrar');
            
            
            $row = $this->galeria_model->guardar_imagen($save);
            if ($row) {
                $id = isset($save['id']) ? $save['id'] : $row;
                if (isset($save['id'])) {
                    $id = $save['id'];
                } else {
                    $id = $row;
                    $this->galeria_model->incrementar_imagenes($save['galeria_id']);
                }
                $response = array(
                    'estatus' => 'OK',
                    'mensaje' => 'La imagen fue guardada',
                    'accion' => isset($save['id']) ? 'edicion' : 'nueva',
                    'imagen' => array(
                        'id' => $id,
                        'archivo' => isset($save['archivo']) ? $save['archivo'] : $imagen->archivo,
                        'path_url' => base_url('uploads/galerias'),
                        'descripcion' => $save['descripcion'],
                        'principal' => $save['principal']
                    )
                );
            } else {
                $response = array(
                    'estatus' => 'ERROR',
                    'mensaje' => 'No se pudo guardar los datos de la imagen.'
                );
            }

            echo json_encode($response);
        }
    }
    
    
    public function guardar_imagen__0000() {
        $this->load->library('form_validation');
        $this->load->helper('form_helper');
        
        $id = (int) $this->input->post('id');
        $galeria = (int) $this->input->post('galeria');
        $descripcion = strip_tags($this->input->post('descripcion'));
        
        if ($id < 1) {
            $res = $this->agregar_imagen($galeria, $descripcion);
            $res['accion'] = 'nueva';
        } else {
            $res = $this->actualizar_imagen($id, $descripcion);
            $res['accion'] = 'edicion';
        }
        
        $res['estatus'] = $res['estatus'] ? 'OK' : 'ERROR';
        
        echo json_encode($res);
    }
    
    private function agregar_imagen($galeria, $descripcion) {
        if (!isset($_FILES['img_archivo']['tmp_name'])) {
            $res = array('estatus' => FALSE, 'mensaje' => 'La imagen es obligatoria');
        } else {
            $info = $this->upload_imagen();
            
            if ($info['estatus']) {
                $data = array(
                    'galeria_id' => $galeria,
                    'archivo' => $info['imagen']['file_name'],
                    'descripcion' => $descripcion
                );
                $id = $this->galeria_model->guardar_imagen($data);
                if ($id) {
                    $res = array(
                        'estatus' => TRUE, 
                        'mensaje' => 'Imagen agregada', 
                        'imagen' => array(
                            'id' => $id, 
                            'descripcion' => $descripcion,
                            'archivo' => $info['imagen']['file_name'],
                            'path_url' => base_url('uploads/galerias/')
                         )
                     );
                } else {
                    $res = array('estatus' => FALSE, 'mensaje' => $info['mensaje']);
                }
            } else {
                $res = array('estatus' => FALSE, 'mensaje' => $info['mensaje']);
            }
        }
        
        return $res;
    }
    
    private function actualizar_imagen($id, $descripcion) {
        $imagen = $this->galeria_model->get_imagen($id);
        $info = NULL;
        
        if (!$imagen) {
            return array('estatus' => FALSE, 'mensaje' => 'La referencia a la imagen a editar no existe.');
        }
        
        if (isset($_FILES['img_archivo']['tmp_name'])) {
            $info = $this->upload_imagen();
            if ($info['estatus']) {
                $data['imagen'] = $info['imagen']['file_name'];
            }
        }
        
        $data['id'] = $id;
        $data['descripcion'] = $descripcion;
        if ($this->galeria_model->guardar($id, $data)) {
            if (isset($data['imagen']) && !empty($data['imagen'])) {
                @unlink('./uploads/galerias/'.$imagen->imagen);
            }
            
            $imagen = $this->galeria_model->get_imagen($id);
            $res = array(
                'estatus' => TRUE,
                'mensaje' => '' . $info['mensaje'],
                'imagen' => array(
                    'id' => $imagen->id,
                    'descripcion' => $imagen->descripcion,
                    'archivo' => $imagen->imagen,
                    'path_url' => base_url('uploads/galerias/')
                )
            );
        } else {
            $res = array(
                'estatus' => FALSE,
                'mensaje' => 'Los datos no pudieron ser actualizados.'
            );
        }
        
        return $res;
    }
    
    
    public function imagenes($evento, $string = FALSE) {
        $imagenes = $this->galeria_model->get_imagenes($evento);
        if ($string) {
            return $this->load->view('admin/galeria/grid_imagenes', array('imagenes' => $imagenes), TRUE);
        } else {
            $this->load->view('admin/galeria/grid_imagenes', array('imagenes' => $imagenes));
        }
    }
    
    
    public function eliminar_imagen($id) {
        $status = "ERROR";
        $message = "No se pudo eliminar la imagen";
        $imagen = $this->galeria_model->get_imagen($id);
        
        if ($imagen) {
            if ($this->galeria_model->eliminar_imagen($id)) {
                @unlink('./public/galerias/full/'.$imagen->imagen);
                $status = "OK";
                $message = "Se ha eliminado la imagen del sistema";
            }
        } else {
            $message = "No se encontro la imagen.";
        }
        
        echo json_encode(array("estatus" => $status, "mensaje" => $message));
    }
    
    public function upload_imagen() {
        $res = array(
            'estatus' => TRUE,
            'mensaje' => '',
            'imagen' => NULL
        );
        $file_element_name = 'img_archivo';
        
        $config['upload_path'] = './uploads/galerias';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file_element_name)) {
            $res['estatus'] = FALSE;
            $res['mensaje'] = 'ERROR UPLOAD: '.$this->upload->display_errors('', '');
        } else {
            $res['estatus'] = TRUE;
            $res['mensaje'] = '';
            $res['imagen'] = $this->upload->data();
        }
        
        @unlink($_FILES[$file_element_name]);
        
        return $res;
    }
    
    
    public function _imagen_check() {
        if ($_FILES['img_archivo']['tmp_name'] == "") {
            $this->form_validation->set_message('_imagen_check', 'La imagen es obligatoria.');
            return false;
        }
        return true;
    }
    
    private function _vaciar_galeria($galeria) {
        $imagenes = $this->galeria_model->get_imagenes($galeria);
        
        foreach ($imagenes as $imagen) {
            @unlink('./public/galerias/full/'.$imagen->imagen);
        }
        
        $this->galeria_model->eliminar_imagenes_galeria($galeria);
    }
}