<?php

class Mesadirectiva extends Admin_Controller {
    private $max_length_desc;
    
    public function __construct() {
        parent::__construct();

        $this->load->model('mesadirectiva_model');
        
       
        $this->max_length_desc = 150;
    }

    public function index() {
        $data['mesadirectiva_grupos'] = $this->mesadirectiva_model->get_grupos();
        $this->view('mesadirectiva/mesadirectiva_grupos', $data);
    }

    
   
    public function mesadirectiva_grupos_form($id = false) {
        $this->load->library('form_validation');

        $data['id'] = $id;
        $data['nombre'] = '';

        if ($id) {
            $mesadirectiva_grupo = $this->mesadirectiva_model->get_grupo($id);

            if (!$mesadirectiva_grupo) {
                $this->session->set_flashdara('error', 'No se encontro el grupo de banners.');
                redirect('admin/mesadirectiva');
            } else {
                $data = array_merge($data, (array) $mesadirectiva_grupo);
            }
        }

        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->view('mesadirectiva/mesadirectiva_grupo_form', $data);
        } else {
            $save['id'] = $id;
            $save['nombre'] = $this->input->post('nombre');

            $this->mesadirectiva_model->guardar_mesadirectiva_grupo($save);

            $this->session->set_flashdata('mensajes', 'Los datos han sido guardados.');

            redirect('admin/mesadirectiva');
        }
    }
 /*
    public function eliminar_grupo($id) {
        $grupo = $this->banner_model->get_grupo($id);
        if (!$grupo) {
            $this->session->set_flashdata('errores', 'No se encontro el grupo de banners solicitado.');
        } else {
            $this->banner_model->eliminar_banners_grupo($id);
            $this->session->set_flashdata('mensajes', 'Se ha eliminado el grupo de banners.');
        }

        redirect('admin/banners');
    }
  * */
  

    public function grupo($id) {
        $data['mesadirectiva_grupo'] = $this->mesadirectiva_model->get_grupo($id);
        if (!$data['mesadirectiva_grupo']) {
            $this->session->set_flashdata('errores', 'No se encontro el grupo de banners solicitado.');
            redirect('admin/mesadirectiva');
        }

        $data['id'] = $id;
        
        
        
        $data['mesadirectiva'] = $this->mesadirectiva_model->get_mesadirectiva_por_grupo($id);
        
        
        //_dump($this->db->last_query());
        
        $this->add_asset('js', 'admin/js/mesadirectiva/admin-mesadirectiva.js');
        
        $this->view('mesadirectiva/mesadirectiva', $data);
    }
    
    
    
    
    

    public function mesadirectiva_form($grupo_id, $id = false) {

        $config['upload_path'] = 'uploads/banners';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 1024 * 2;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);


        $this->load->helper(array('form', 'date'));
        $this->load->library('form_validation');
        $grupo = $this->mesadirectiva_model->get_grupo($grupo_id);
        
      
        
        //valores por default
        $data = array(
            'banner_id' => $id, 
            'banners_grupo_id' => $grupo_id, 
            'titulo' => '', 
            'descripcion' => '',
            'fecha_inicio' => '', 
            'fecha_expira' => '',
            'imagen' => '', 
            'link' => '',
            'nueva_ventana' => false,
            'max_length' => $this->max_length_desc,
            'max_width' => $grupo->img_width,
            'max_height' => $grupo->img_height
        );
        
      
        
        $data['form_titulo'] = 'Nuevo integrante';
        
        
         

        if ($id) {
            $data = array_merge($data, (array) $this->mesadirectiva_model->get_mesadirectiva($id));
            $data['fecha_inicio'] = date_to_mysql($data['fecha_inicio']);
            $data['fecha_expira'] = date_to_mysql($data['fecha_expira']);
            $data['nueva_ventana'] = (bool) $data['nueva_ventana'];
            $data['form_titulo'] = 'Editar miembro';
        }
        
        

        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|max_length['.$this->max_length_desc.']');
        $this->form_validation->set_rules('fecha_inicio', 'Fecha inicio', 'trim');
        $this->form_validation->set_rules('fecha_expira', 'Fecha expira', 'trim');
        $this->form_validation->set_rules('link', 'Link', 'trim|prep_url');
        $this->form_validation->set_rules('nueva_ventana', 'Nueva ventana', 'trim');
        
   
        if (!$id) {
            $this->form_validation->set_rules('imagen', 'Imagen', 'callback__imagen_check');
        }
       
         
        

        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            
            $this->add_asset('js', 'admin/plugins/jquery.maxlength/jquery.plugin.min.js');
            $this->add_asset('js', 'admin/plugins/jquery.maxlength/jquery.maxlength.min.js');
            $this->add_asset('js', 'admin/js/mesadirectiva/form-mesadirectiva.js');
            
            $this->view('mesadirectiva/mesadirectiva_form', $data);
            
            
        } else {

            $uploaded = $this->upload->do_upload('imagen');

            $save['banners_grupo_id'] = $grupo_id;
            $save['titulo'] = $this->input->post('titulo');
            $save['descripcion'] = $this->input->post('descripcion');
            $save['fecha_inicio'] = date_to_mysql($this->input->post('fecha_inicio'));
            $save['fecha_expira'] = date_to_mysql($this->input->post('fecha_expira'));
            $save['link'] = $this->input->post('link');
            $save['nueva_ventana'] = (int)$this->input->post('nueva_ventana');

            
            if ($id) { 
                $save['id'] = $id;

                if ($uploaded) {
                    if ($data['imagen'] != '') {
                        $file = 'uploads/banners/' . $data['imagen'];

                        
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {   
                if (!$uploaded) {
                    $this->session->set_flashdata('errores', $this->upload->display_errors());
                    $this->view('mesadirectiva/mesadirectiva_form', $data);
                    return;
                }
            }

            if ($uploaded) {
                $image = $this->upload->data();

                
                $save['imagen'] = $image['file_name'];
            }
            
            
           
            
            $this->mesadirectiva_model->guardar_banner($save);

            $this->session->set_flashdata('mensajes', 'Se ha guardado el banner');

            redirect('admin/mesadirectiva/mesadirectiva_form/'.$grupo_id);
           
        }
    }




/*
     * 


    public function eliminar_banner($banner_id) {
        $banner = $this->banner_model->get_banner($banner_id);
        if (!$banner) {
            $this->session->set_flashdata('errores', 'No se encontro el banner solicitado.');
        } else {
            $this->banner_model->eliminar_banner($banner_id);
            $this->session->set_flashdata('mensajes', 'Se ha borrado el banner');
        }

        redirect('admin/banners/grupo/' . $banner->banners_grupo_id);
    }

     * * 
     */
    
    
    public function organizar() {
        $mesadirectiva = $this->input->post('orden');
        
      
        
        if (is_array($mesadirectiva)) {
            $this->mesadirectiva_model->organizar($mesadirectiva);
        }
        
        echo json_encode(array('status' => 'ok', 'message' => 'success'));
    }

  
    
    
    
      public function _imagen_check() {
        if ($_FILES['imagen']['tmp_name'] == "") {
            $this->form_validation->set_message('_imagen_check', 'La imagen es obligatoria.');
            return false;
        }
        return true;
    } 
}
    
