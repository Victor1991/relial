<?php

class Categorias extends Admin_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('categoria_model');
    }
    
    public function index() {
        $categorias = $this->categoria_model->get_many_by(array('modulo' => 'blog'), array('nombre', 'asc'));
        
        $data['categorias'] = array();
        foreach ($categorias as $categoria) {
            $data['categorias'][$categoria->parent][] = $categoria;
        }
        
        $data['categorias_padre'] = isset($data['categorias'][0]) ? count($data['categorias'][0]) : 0;
        $this->add_asset('js', 'admin/js/admin-galerias.js');
        
        $this->view('categorias/index', $data);
    }
    
    public function form($id=FALSE) {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        $data['titulo_form'] = 'Nueva categoria';
        $data['id'] = FALSE;
        $data['nombre'] = '';
        $data['slug'] = '';
        $data['parent'] = '0';
        $data['descripcion'] = '';
        
        if ($id) {
            $data = array_merge($data, (array) $this->categoria_model->get($id));  
            $data['titulo_form'] = 'Editar categoria';
        }

        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('parent', 'Categoria padre', 'trim');
        $this->form_validation->set_rules('slug', 'Slug', 'trim');
        
        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            
            $categorias = $this->categoria_model->get_many_by(array('modulo' => 'blog', 'parent' => 0), array('nombre', 'asc'));
            $data['categorias'] = $this->_array_dropdown($categorias, 'id', 'nombre', "--- Ninguna ---");
            $this->view('categorias/form', $data);
        } else {
            $save['id'] = $id;
            $save['nombre'] = $this->input->post('nombre');
            $save['parent'] = $this->input->post('parent');
            $save['slug'] = $this->input->post('slug');
            $save['descripcion'] = strip_tags($this->input->post('descripcion'));
            $save['modulo'] = 'blog';
            
            if ($this->input->post('nombre') != $data['nombre']) {
                $this->load->helper('text');
                $slug = url_title(convert_accented_characters($this->input->post('nombre')), 'dash', TRUE);

                if ($id) {
                    $slug = $this->categoria_model->validar_slug($slug, $id);
                } else {
                    $slug = $this->categoria_model->validar_slug($slug);
                }
                
                $save['slug'] = $slug;
            }
            
            if ($id) {
                $res = $this->categoria_model->update($id,$save);
            } else {
                $res = $this->categoria_model->insert($save);
            }
            
            if ($res) {
                $this->session->set_flashdata('mensajes', 'Se ha guardado la categoria: <b>' . $save['nombre'] . '</b>');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar la categoria');
            }

            redirect('admin/categorias');            
        }
    }
    
    public function eliminar($id) {
        if(is_numeric($id)){
            if ($this->categoria_model->delete($id)) {
                $this->session->set_flashdata('mensajes', 'Se ha eliminado la categoría del sistema');
            } else {
                $this->session->set_flashdata('errores', 'No fue posible eliminar la categoría. Por favor vuelva a intentarlo.');
            }
        }
        
        redirect('admin/categorias');
    }
    
    private function _array_dropdown($objs, $attr_key, $attr_val, $primero = NULL) {
        $array = array();
        
        if (!is_null($primero)) {
            $array[0] = $primero;
        }
        
        foreach ($objs as $value) {
            $array[$value->$attr_key] = $value->$attr_val;
        }
        
        return $array;
    }
}