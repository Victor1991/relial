<?php

class Posts extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        
        $this->load->model('post_model');
        $this->load->model('categoria_model');
    }
    
    public function index() {
        $this->load->helper(array('pagination','form','date'));
        
        $where = array('estatus' => 0);
        
        $where['estatus'] = $this->input->post('f_estatus') ? (int)$this->input->post('f_estatus') : $where['estatus'];
        
        if ($this->input->post('f_categoria')) {
            $where['categoria'] = (int) $this->input->post('f_categoria');
        }
        
        if ($this->input->post('f_buscar')) {
            $where['titulo'] = $this->input->post('f_buscar');
        }
        
        $pagination = create_pagination('admin/posts/index', $this->post_model->num_posts($where), 50);
        
        $tabla['posts'] = $this->post_model->get_many($where, array('fecha' => 'desc'),$pagination['limit'], $pagination['offset']);        
        
//        _dump($this->db->last_query());die;
        
        $tabla['pagination'] = $pagination;
        $data['tabla'] = $this->load->view('admin/posts/tabla', $tabla, TRUE);
        
//        _dump($tabla['posts']);
        
        if ($this->input->is_ajax_request()) {
            echo $data['tabla'];
        } else {
            $categorias = $this->categoria_model->get_many_by(array('modulo' => 'blog')); 
            $data['categorias_dpdwn'] = $this->_array_dropdown($categorias, 'id', 'nombre', '-- todas --');
            $this->add_asset('js', 'admin/js/admin-filtros.js');
            $this->add_asset('js', 'admin/js/posts/admin-posts.js');
            $this->view('posts/index', $data);
        }
    }
    
    public function form($id=FALSE) {
        $config['upload_path'] = 'uploads/posts';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 1024 * 5;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);


        $this->load->helper(array('form', 'date','text'));
        $this->load->library('form_validation');
        
        //valores por default
        $data = array(
            'id' => FALSE,
            'autor_id' => 0,
            'categoria_id' => 0,
            'titulo' => '',
            'slug' => '',
            'resumen' => '',
            'contenido' => '',
            'estatus' => 0,
            'imagen' => '',
            'comentarios_estatus' => 'always',
            'comentarios_cant' => 0,
            'publico' => 1,
            'evento' => 0,
            'fecha' => date('Y-m-d')
        );
        
        $data['titulo_form'] = $id ? 'Editar artículo' : 'Nuevo artículo';

        if ($id) {
            $post = $this->post_model->get_by_id($id);
            if (!$post) {
                show_404();
                die();
            }
            $data = array_merge($data, (array) $post);            ;
        }
        
        $data['fecha'] = mysql_to_date($data['fecha']);
        $data['categorias'] = $this->_array_dropdown($this->categoria_model->get_many_by(array('modulo' => 'blog'), 
                array('nombre' => 'asc')), 'id', 'nombre', 'seleccione una categoría');
        $data['estatus_dpdwn'] = array(
            0 => 'Borrador',
            1 => 'Publicado'
        );
            
        
        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('resumen', 'Resumen', 'trim|max_length[500]');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim');
        
//        if (!$id) {
//            $this->form_validation->set_rules('imagen', 'Imagen', 'callback__imagen_check');
//        }

        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            $this->show_form($data);            
        } else {

            $save['categoria_id'] = (int)$this->input->post('categoria');
            $save['autor_id'] = (int)$this->session->userdata('usuario_id');
            $save['estatus'] = (int)$this->input->post('estatus');
            $save['evento'] = (int)$this->input->post('evento');
            $save['titulo'] = $this->input->post('titulo');
            $save['resumen'] = $this->input->post('resumen');
            $save['contenido'] = $this->input->post('contenido');
            $save['fecha'] = date_to_mysql($this->input->post('fecha'));
            
            if ($this->input->post('titulo') != $data['titulo']) {
                $slug = url_title(convert_accented_characters($this->input->post('titulo')), 'dash', TRUE);
                $slug = $this->post_model->validar_slug($slug, $id);                
                $save['slug'] = $slug;
            }
            
            //cargamos la imagen en caso de haber
            $uploaded = $this->upload->do_upload('imagen');
            
            //actualizar
            if ($id) {  //actualizar imagen
                $save['id'] = $id;

                if ($uploaded) {
                    if ($data['imagen'] != '') {
                        $file = 'uploads/posts/' . $data['imagen'];

                        //eliminar la imagen anterior si existe
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {    
                //nuevo post
                if (!$uploaded && isset($_FILES['imagen']['tmp_name']) && $_FILES['imagen']['tmp_name'] != '') {
                    $this->session->set_flashdata('errores', $this->upload->display_errors());
                    $this->show_form($data);
                    return;
                }
            }

            //se cargo una imagen
            if ($uploaded) {
                $image = $this->upload->data();
                //redimensionar imagen
//                $img_w = 600;
//                $img_h = 200;
//                if ($img_h != $image['image_height'] || $img_w != $image['image_width']) {
//                    $this->load->helper('image');
//                    resize_image($img_w, $img_h, 'uploads/posts/'.$image['file_name']);
//                }
                
                $save['imagen'] = $image['file_name'];
            }
            
            if ($id) {
                $res = $this->post_model->update($id,$save);
            } else {
                $res = $this->post_model->insert($save);
            }
            
            if ($res) {
                $this->session->set_flashdata('mensajes', 'Se ha guardado el post: <b>' . $save['titulo'] . '</b>');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar el post');
            }

            redirect('admin/posts/');
        }
    }
    
    private function show_form($data) {
        $this->add_asset('js', 'admin/js/jquery.maxlength/jquery.plugin.min.js');
        $this->add_asset('js', 'admin/js/jquery.maxlength/jquery.maxlength.min.js');
        $this->add_asset('js', 'admin/js/posts/form-posts.js');
        
        $this->view('posts/form', $data);
    }
    
    public function eliminar($id) {
        $post = $this->post_model->get_by_id($id);
        
        if (!$post) {
            $this->session->set_flashdata('errores', 'No se encontro el posts solicitado');
            redirect('admin/posts');
        }
        
        //eliminamos la imagen del servidor
        $this->_eliminar_imagen($post->imagen);        
        //eliminamos los comentarios
        $this->_eliminar_comentarios($post->id);
        //eliminamos el post
        $this->post_model->delete($post->id);
        
        $this->session->set_flashdata('mensajes', 'El post "'.$post->titulo.'" ha sido eliminado');
        redirect('admin/posts');
    }
    
    private function _eliminar_imagen($imagen) {
        if (!empty($imagen)) {
            $file = 'uploads/posts/' . $imagen;
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
    
    private function _eliminar_comentarios($post_id) {
        $this->load->model('comentario_model');
        $this->comentario_model->eliminar_por_entrada('blog', $post_id);
    }
    
    private function _array_dropdown($objs, $attr_key, $attr_val, $primero = NULL) {
        $array = array();
        
        if (!is_null($primero)) {
            $array[] = $primero;
        }
        
        foreach ($objs as $value) {
            $array[$value->$attr_key] = $value->$attr_val;
        }
        
        return $array;
    }
    
}
