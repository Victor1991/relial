<?php

class Dashboard extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'date', 'text'));
        $this->load->library('form_validation');
    }

    public function index() {
        $this->view('dashboard');
    }

    public function test_zip() {
        $this->form_validation->set_rules('prueba', 'Producto', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->view('test');
        } else {
            
//            _dump($_FILES);die;
            
            $config['upload_path'] = PAHT_MATERIAL;
            $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = '*';

            $this->load->library('upload', $config);

            $nombre = FALSE;
            
            if ($this->upload->do_upload('archivo')) {
                $nombre = $this->upload->data('file_name');
                $nombre_carpeta = $this->upload->data('raw_name');
                mkdir(PAHT_MATERIAL.$nombre_carpeta);
            } else{
                _dump($this->upload->display_errors());
            }
            $zip = new ZipArchive;
            if ($zip->open(PAHT_MATERIAL.$nombre) === TRUE) {
                $zip->extractTo(PAHT_MATERIAL.$nombre_carpeta);
                $zip->close();
                unlink(PAHT_MATERIAL.$nombre);
                _dump('SI');
            } else {
                _dump('NO');
            }
        }
    }

}
