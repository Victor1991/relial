<?php

class Videos extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('video_model');
        
    }
    
    public function index() {
        $this->load->helper(array('pagination', 'form'));
        
        $where = array('activo' => 0);
        
        //Filtros
        $where['activo'] = $this->input->post('f_activo') ? (int)$this->input->post('f_activo') : $where['activo'];
        
        if ($this->input->post('f_buscar')) {
            $where['nombre'] = $this->input->post('f_buscar');
        }
        
        $pagination = create_pagination('admin/videos/index', $this->video_model->count_by($where), 50);
        
        $tabla['videos'] = $this->video_model->get_many_by($where, $pagination['limit'], $pagination['offset']);        
        $tabla['pagination'] = $pagination;
        $data['tabla'] = $this->load->view('admin/videos/tabla', $tabla, TRUE);
        
        if ($this->input->is_ajax_request()) {
            echo $data['tabla'];
        } else {
            $this->add_asset('js', 'admin/js/admin-filtros.js');
            $this->add_asset('js', 'admin/js/videos/admin-videos.js');
            $this->view('videos/index', $data);
        }
    }
    
    public function form($id = FALSE) {
        $this->load->helper('form');
        $this->load->library('form_validation');        
        
        //valores por default
        $data = array(
            'id' => FALSE,
            'titulo' => '',
            'link' => '',
            'estatus' => 0
        );
        
        $data['titulo_form'] = $id ? 'Editar video' : 'Nuevo video';

        if ($id) {
            $video = $this->video_model->get_by_id($id);
            if (!$video) {
                $this->session->set_flashdata('errores', 'No se encontro el video solicitado');
                redirect('admin/videos');
            }
            $data = array_merge($data, (array) $video);            ;
        }
        
        $this->form_validation->set_rules('titulo', 'Título', 'trim|required');
        $this->form_validation->set_rules('link', 'Link', 'trim|required|valid_url');
        
        if ($this->form_validation->run() == false) {
            if (validation_errors()) {
                $this->session->set_flashdata('errores', validation_errors());
            }
            
            $this->add_asset('js', 'admin/js/videos/admin-videos.js');
            
            $this->view('videos/form', $data);  
        } else {
            $save['titulo'] = $this->input->post('titulo');
            $save['link'] = $this->input->post('link');
            $save['estatus'] = $this->input->post('estatus');
            
            if ($id) {
                $res = $this->video_model->update($id, $save);
            } else {
                $res = $this->video_model->insert($save);
            }
            
            if ($res) {
                $this->session->set_flashdata('mensajes', 'Se ha guardado la video');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo guardar la video');
            }

            redirect('admin/videos/');
        }
    }
    
    public function eliminar($id) {
        if(is_numeric($id)){
            if ($this->pagina_model->eliminar($id)) {
                $this->session->set_flashdata('mensajes', 'Se ha eliminado el video');
            } else {
                $this->session->set_flashdata('errores', 'No fue posible eliminar el video. Por favor vuelva a intentarlo.');
            }
        }
        
        redirect('admin/videos');
    }
}