<?php

class Biblioteca extends Public_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model(array('biblioteca_model', 'categoria_model', 'frase_model'));
        $this->breadcrumbs->push('Biblioteca', 'biblioteca');
    }

    public function index() {

        $this->load->model('frase_model');
        $data['categorias'] = $this->categoria_model->get_many_by(array('modulo' => 'biblioteca'), array('nombre' => 'asc'));

        $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->view('biblioteca', $data);
    }

    public function categoria($slug = FALSE) {

        $this->load->model('frase_model');
        $slug = htmlentities($slug, ENT_QUOTES);
        $categoria = $this->categoria_model->get_por_slug($slug);

        if (!$categoria || $categoria->modulo != 'biblioteca') {
            show_404();
            die;
        }

        if(!$slug || $slug == 'revistas'){
            $order = array('id' => 'desc');
        }else{
            $order = array('titulo' => 'asc');
        }

        $categoria->libros = $this->biblioteca_model->get_many_libros(array('categoria' => $categoria->id, 'estatus' => 1),$order);
        $data['categoria'] = $categoria;
        $this->breadcrumbs->push($categoria->nombre, 'biblioteca/categoria/' . $categoria->slug);

        $data['informacion'] = $this->frase_model->get_by_id(8);

        $this->view('biblioteca_categoria', $data);
    }

    public function libro($slug) {
        $slug = htmlentities($slug, ENT_QUOTES);

        $this->load->model('frase_model');

        $libro = $this->biblioteca_model->get_libro_by_slug($slug);

        if (!$libro) {
            show_404();
            die;
        }

        $data['libro'] = $libro;

        $this->breadcrumbs->push($libro->categoria_nombre, 'biblioteca/categoria/' . $libro->categoria_slug);
         $data['informacion'] = $this->frase_model->get_by_id(8);

        $this->view('biblioteca_detalle', $data);
    }

     public function buscar_categoria()
     {
          $search = $this->input->get('search');
          $params['buscar'] = $search;
          $data['buscar'] = $search ;
          $data['categoria'] = new stdClass();
          $data['categoria']->libros = $this->biblioteca_model->biblioteca_model->get_many_libros($params);
          $data['informacion'] = $this->frase_model->get_by_id(8);




          $this->view('biblioteca_search', $data);
     }
}
