<?php
include dirname(__DIR__) . "/third_party/MailChimp.php";

class Sitio extends Public_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index () {
//        _dump($_SERVER);die;
        $this->load->model(array('evento_model', 'galeria_model', 'banner_model', 'frase_model', 'biblioteca_model', 'evento_model'));
        // $data['posts'] = $this->post_model->get_many(array('in_categoria' => array(6,7), 'estatus' => 1), array('po.fecha' => 'desc'), 4);

          $data['posts'] = $this->post_model->get_many(array(), array('po.id' => 'desc'), 4);

     // _dump($data['posts']);

        $data['eventos'] = $this->post_model->get_many(array('evento' => 1, 'estatus' => 1), array('po.fecha' => 'desc'), 3);
        $data['galerias'] = $this->galeria_model->get_activas_inicio(9);
        $data['aliados'] = $this->banner_model->get_banners_por_grupo(2, TRUE, 100);
        $data['frase'] = $this->frase_model->get_random();
        $data['libro'] = $this->biblioteca_model->get_libro_destacado();
        $data['informacion'] = $this->frase_model->get_by_id(8);
        $data['inicio'] = $this->frase_model->get_by_id(9);
//        _dump($this->db->last_query());die;

        $this->add_asset('css', 'plugins/youtube-video-player/packages/icons/css/icons.min.css');
        $this->add_asset('css', 'plugins/youtube-video-player/css/youtube-video-player.min.css');
        $this->add_asset('js', 'plugins/youtube-video-player/js/youtube-video-player.jquery.min.js');
        $this->add_asset('css', 'plugins/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.css');
        $this->add_asset('js', 'plugins/youtube-video-player/packages/perfect-scrollbar/jquery.mousewheel.js');
        $this->add_asset('js', 'plugins/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.js');

        // $this->add_asset('css', 'admin/js/fullcalendar/bootstrap-fullcalendar.css');



        $this->add_asset('js', 'js/suscribir.js');

          // _dump($data);

        $this->view('home', $data);
    }

    public function pagina($slug) {
        $this->load->model('pagina_model');
        $this->load->model('mesadirectiva_model');
        $this->load->model('frase_model');



        //_dump($miembrosde);


        $slug = strip_tags($slug);
        $pagina = $this->pagina_model->get_by_slug($slug);
        if (!$pagina) {
            show_404();
            die;
        }


        $miembrosde = $this->mesadirectiva_model->get_mesadirectiva_por_grupo(4);

        $miembrosdem = $this->mesadirectiva_model->get_mesadirectiva_por_grupo(3);

        $data['pagina'] = $pagina;

        $data['miembrosde'] = $miembrosde;
        $data['miembrosdem'] = $miembrosdem;

        $data['informacion'] = $this->frase_model->get_by_id(8);

        $this->breadcrumbs->push($pagina->titulo, 'sitio/'.$pagina->slug);

        //$this->mesadirectiva_miembros = $this->mesadirectiva_model->get_mesadirectiva_por_grupo(4);


//         _dump($data);
// die;

        $this->view('pagina', $data);

    }

     public function directorio(){
          $this->load->model('frase_model');
          $this->add_asset('js', 'js/directorio.js');

          $aliados = $this->banner_model->get_banners_directorio();
          $data['aliados']  = array();
          foreach ($aliados as $element) {
              $pais = $element['pais'] != '' ? $element['pais'] :  'Otros';
              $data['aliados'][$pais][] = $element;
          }

//           _dump($this->db->last_query());

          $data['informacion'] = $this->frase_model->get_by_id(8);
          $this->view('directorio', $data);
     }



    public function red_relial() {
        $this->load->model('mapa_model');
        $data = array();

        $paises = $this->mapa_model->get_paises();
        $regiones = [];
        $instituciones = [];
        foreach ($paises as $p) {
            $regiones[$p->clave] = $p->estatus;
            $instituciones[$p->clave] = ['pais' => $p->nombre,'instituciones' => (array) $p->instituciones];
        }
        $data['regiones'] = json_encode($regiones);
        $data['instituciones'] = json_encode($instituciones);

        $this->add_asset('css', 'js/jvectormap/jquery-jvectormap-2.0.3.css');
        $this->add_asset('js', 'js/jvectormap/jquery-jvectormap-2.0.3.min.js');
        $this->add_asset('js', 'js/jvectormap/jvectormap.latinoamerica.js');
        $this->add_asset('js', 'js/map-red-relial.js');
        $this->breadcrumbs->push('La Red RELIAL de América Latina ', 'red_relial');
        $this->view('red_relial', $data);
    }


    public function contacto() {
        $data = array();
        $this->load->model('frase_model');
        $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->breadcrumbs->push('Contacto', 'sitio/contacto');
        $this->add_asset('js', 'js/contacto.js');

        $this->view('contacto', $data);
    }

    public function enviar_contacto() {
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');
            $this->load->helper('form');

            $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
            $this->form_validation->set_rules('email', 'Correo electrónico', 'required|valid_email');
            $this->form_validation->set_rules('asunto', 'Asunto', 'trim|required');
            $this->form_validation->set_rules('comentario', 'Comentario', 'trim|required');

            if ($this->form_validation->run()) {
                $this->load->library('email');

                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;

                $this->email->initialize($config);

                $to = 'relial@relial.org';
                $from = $this->input->post('email');
                $nombre = strip_tags($this->input->post('nombre'));
                $asunto = strip_tags($this->input->post('asunto'));
                $comentario = htmlentities($this->input->post('comentario'));

                $html = '<p>Mensaje enviado desde el formulario de contacto de la página de RELIAL</p>'
                        . '<p>Enviado por: <strong>' . $nombre . '</strong> <i>(' . $from . ')<i></p>'
                        . '<p>Asunto: ' . $asunto . '</p>'
                        . '<p>Comentario: </p>'
                        . '<p>' . nl2br($comentario) . '</p>';


                $this->email->from($from, $nombre);
                $this->email->to($to);
                $this->email->bcc('silvia.mercado@fnst.org,adriana.corona@fnst.org,contacto@sumawebdiseno.com');
                $this->email->subject($asunto);
                $this->email->message($html);

                if ($this->email->send()) {
                    echo json_encode(array(
                        'estatus' => 'ok',
                        'mensaje' => 'Su mensaje ha sido enviado.'
                    ));
                } else {
                    echo json_encode(array(
                        'estatus' => 'error',
                        'mensaje' => 'Su mensaje no ha podido ser enviado, por favor vuelva a intentarlo más tarde.'
                    ));
                }
            } else {
                echo json_encode(array(
                    'estatus' => 'error',
                    'mensaje' => validation_errors()
                ));
            }

        } else {
            show_404();
        }
    }

    public function ici2018() {
        redirect('material/6b075cdf8084d6e8cd717048f945822e/FLIPBOOK_ESPANOL_2018_alta.html');
    }

    public function insert(){
         $this->load->library('form_validation');
         $this->load->helper('form');
         // config imagen
         $config['upload_path'] = 'uploads/banners/';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size'] = 1024 * 1024 * 2;
         $config['encrypt_name'] = true;
         $this->load->library('upload', $config ,'img');

         $this->form_validation->set_rules('titulo','Título','trim|required|max_length[100]');
          $this->form_validation->set_rules('correo', 'Correo','trim|required|max_length[100]');
          if ($this->form_validation->run() === FALSE) {
              if (validation_errors()) {
                 echo json_encode(array('tipo' => 'validacion', 'mensaje' => $this->form_validation->error_array()));
              }
          } else {

              $uploaded = $this->img->do_upload('imagen');
               if ($this->input->post('banner_id')) {
                      $save['id'] = $this->input->post('banner_id');
               }

              $save['titulo'] = strip_tags($this->input->post('titulo'));
              $save['mail'] = $this->input->post('correo');
              $save['telefono'] = $this->input->post('telefono');
              $save['descripcion'] = $this->input->post('descripcion');
              $save['link'] = $this->input->post('link');
              $save['nueva_ventana'] = $this->input->post('nueva_ventana') ? 1 : 0;
              $save['estatus'] =  0;
              $save['banners_grupo_id'] = 2;

              if ($_FILES['imagen']['name'] != '') {


                   if ($uploaded) {
                       $image = $this->img->data();
                       $save['imagen'] = $image['file_name'];
                   }else {
                       echo json_encode(array('tipo' => 'error', 'mensaje' =>  $this->img->display_errors()));
                   }
              }

              $save_id = $this->banner_model->guardar_banner($save);
              if ($save_id) {
                   echo json_encode(array('tipo' => 'success', 'mensaje' =>  'Uno de nuestro colaboradores se pondrá en contacto con usted.'));
              } else {
                 echo json_encode(array('tipo' => 'error', 'mensaje' =>  'Error al enviar la información, intentalo más tarde. '));
              }

    }
  }

       public function suscribir()
       {

            $apikey = 'a67be42c7e7e7a925f483d109c40b593-us19';
            $lista_id = '7822f8ae1a';

            $MailChimp = new MailChimp($apikey, false);   // Instanciar la clase Mailchimp

            $result = $MailChimp->call('lists/'.$lista_id.'/members/', 'POST', array(
                 'email_address'     => $this->input->post('email'),
                 'status'            => 'subscribed')
            );


            if (isset($result['status'])) {
                 $mnsj = array('tipe' => 'nuevo', 'mensaje' => 'Usuario suscrito correctamente');
            }else {
                 $mnsj = array('tipe' => 'error', 'mensaje' => 'Erro al suscribir al Usuario, intenta más tarde');
            }
            echo json_encode($mnsj);
      }


    public function download($name,$id){
        $this->load->helper('download');
        $this->load->model('biblioteca_model');

        $this->biblioteca_model->add_download($id);

        force_download('./uploads/biblioteca/'.$name, NULL,TRUE);
    }


    public function importar()
    {
         require_once 'phpexcel/PHPExcel/IOFactory.php';
         $file_directory = "uploads/";
         $new_file_name = "Biblioteca_RELIAL_v3.xls";
         // $new_file_name = "Categorias.xls";

         $file_type = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
         $file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
         $objReader	= PHPExcel_IOFactory::createReader($file_type);
         $objPHPExcel = $objReader->load($file_directory . $new_file_name);
         $sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

         foreach ($sheet_data as $key => $row){
              if ($key != 1) {
                   $categoria = $this->category_id($row['C']);
                    if ($categoria) {
                         $save['categoria_id'] = $categoria->id;
                         $save['titulo'] =$row['E'];
                         $save['slug'] =$row['F'];
                         $save['autor'] =$row['G'];
                         $save['editorial'] =$row['H'];
                         $save['descripcion'] =$row['I'];
                         $save['portada'] =$row['J'];
                         $save['archivo'] =$row['K'];
                         $save['archivo2'] =$row['L'];
                         $save['estatus'] =$row['M'];
                         $save['anio_publicacion'] =$row['N'];
                         $save['idioma'] =$row['O'];
                         $save['edicion'] =$row['P'];
                         echo $this->db->where('id', $row['A'])->update('biblioteca', $save);
                    }else{
                         echo $this->db->where('id', $row['A'])->delete('biblioteca');
                    }
              }



         }
    }

    public function category_id($nueva_categoría)
    {
       return $this->db->from('categorias')->like('nombre', $nueva_categoría)->get()->row();
    }

    function slugify($text){
         // Strip html tags
         $text=strip_tags($text);
         // Replace non letter or digits by -
         $text = preg_replace('~[^\pL\d]+~u', '-', $text);
         // Transliterate
         setlocale(LC_ALL, 'en_US.utf8');
         $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
         // Remove unwanted characters
         $text = preg_replace('~[^-\w]+~', '', $text);
         // Trim
         $text = trim($text, '-');
         // Remove duplicate -
         $text = preg_replace('~-+~', '-', $text);
         // Lowercase
         $text = strtolower($text);
         // Check if it is empty
         if (empty($text)) { return 'n-a'; }
         // Return result
         return $text;
    }

    public function eventos_caledario() {
         $this->load->model(array('evento_model', 'galeria_model', 'banner_model', 'frase_model', 'biblioteca_model', 'evento_model'));


        $filtros['inicio'] = $this->input->get('start');
        $filtros['fin'] = $this->input->get('end');

        $filtros['inicio'] = date("Y-m-d", $filtros['inicio']);
        $filtros['fin'] = date("Y-m-d", $filtros['fin']);

        $eventos = $this->evento_model->get_many_by($filtros);

        if ($this->input->is_ajax_request()) {
            $res = $this->_prepare_results_fullcalendar($eventos);

            echo json_encode($res);
        }
    }

    private function _prepare_results_fullcalendar($rows) {
        $results = array();
        foreach ($rows as $row) {
               if ($row->en_calendario == 1) {
                    $res = array(
                        'id' => $row->id,
                        'title' => $row->titulo,
                        'allDay' => (bool)$row->dia_completo,
                        'start' => $row->dia_completo ? $row->fecha_inicio : $row->fecha_inicio . ' ' . $row->hora_inicio,
                        'end' => $row->dia_completo ? $row->fecha_fin : $row->fecha_fin . ' ' . $row->hora_fin,
                        'url' => base_url('/programa/'.$row->id),
                         'color' => '#e5141a',
                         'className' => 'link_modal',
                    );
                    $results[] = $res;
               }

        }

        return $results;
    }

     public function programa($id=false)
     {
          $this->load->model(array('evento_model', 'galeria_model', 'banner_model', 'frase_model', 'biblioteca_model', 'evento_model'));

          $evento = array();
          if ($id) {
               $evento = $this->evento_model->get_by_id($id);
          }
          echo json_encode($evento);
     }

}
