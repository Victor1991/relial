<?php

class Videos extends Public_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('post_model');
    }
    
    public function index() {
        $this->load->helper(array('pagination', 'date'));
        $this->load->model('frase_model');
        
        $filtros = array('estatus' => 1, 'categoria' => 23);
        
        $pagination = create_pagination('videos/index', $this->post_model->num_posts($filtros), 5, 3);
        $data['videos'] = $this->post_model->get_many($filtros, array('po.fecha' => 'desc'), $pagination['limit'], $pagination['offset']);
        $data['pagination'] = $pagination;
        
        //$data['sidebar'] = $this->prepare_sidebar();
          $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->breadcrumbs->push('Videos', 'videos');
        $this->view('videos', $data);
    }
    
    public function detalle($slug) {
        $slug = htmlentities($slug, ENT_QUOTES);
         $this->load->model('frase_model');
        $video = $this->post_model->get_by(array(
            'slug' => $slug,
            'estatus' => 1
        ));
        
        if (!$video) {
            show_404();
            die;
        }
        
        $data['video'] = $video;
        //$data['sidebar'] = $this->prepare_sidebar();
          $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->breadcrumbs->push($video->titulo, 'videos/' . $video->slug);
        
        $this->view('video_detalle', $data);
    }
}