<?php

class Galerias extends Public_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('galeria_model');
    }
    
    public function index () {
        $data['galerias'] = $this->galeria_model->get_galerias_year();
        $this->load->model('frase_model');
        $this->breadcrumbs->push('Galerias', 'galerias');
        $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->view('galerias', $data);
    }
    
    public function detalle($id) {
        $id = is_numeric($id) ? (int) $id : 0;
        $galeria = $this->galeria_model->get_by_id($id);
        $this->load->model('frase_model');
        
        if (!$galeria) {
            show_404();
            die;
        }
        
        $galeria->imagenes = $this->galeria_model->get_imagenes($id);
        $data['informacion'] = $this->frase_model->get_by_id(8);
        $data['galeria'] = $galeria;
        $this->view('galeria_detalle', $data);
    }
    
    public function fotos($gal_id) {
        $galeria = $this->galeria_model->get_by_id($gal_id);
        if (!$galeria) {
            show_404();
            die;
        }
          $this->load->model('frase_model');
        $galeria->imagenes = $this->galeria_model->get_imagenes_front($gal_id);
        $data['galeria'] = $galeria;
        
        $this->breadcrumbs->push('Galerías', 'galerias');
        $this->add_asset('css', 'css/jquery.fancybox.css');
        $this->add_asset('js', 'js/isotope.min.js');
        $this->add_asset('js', 'js/jquery.fancybox.pack.js');
        $this->add_asset('js', 'js/gallery.js');
        $data['informacion'] = $this->frase_model->get_by_id(8);
        $this->view('galeria_detalle', $data);
    }
}

