<?php
/**
 * 08/01/2020
 * @VictorHugo
 */
class Eventos extends Public_Controller{

     function __construct(){
          parent::__construct();
          $this->load->model(array('evento_model', 'mapa_model', 'hoteles_model', 'registro_model', 'frase_model'));
          $this->breadcrumbs->push('Eventos', 'eventos');
     }

     private function meses(){
          // code...
     }

     public function index(){
        $data['informacion'] = $this->frase_model->get_by_id(8);

         // $this->add_asset('css', 'css/eventos_lista.css');

          $this->add_asset('css', 'plugins/bootstrap-datepicker/css/bootstrap-datepicker.css');
          $this->add_asset('js', 'plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
          $this->add_asset('js', 'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js');
          $this->add_asset('js', 'js/evento_list.js');

          $this->view('eventos', $data );
     }

     public function evento($id){
          setlocale(LC_ALL,"es_ES");

          $this->add_asset('css', 'css/evento_detalles.css');
          $this->add_asset('css', 'plugins/bootstrap-datepicker/css/bootstrap-datepicker.css');
          $this->add_asset('js', 'plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
          $this->add_asset('js', 'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js');
          $this->add_asset('js', 'js/evento_detalles.js');

          $data['evento'] = $this->evento_model->get_by_id($id);
          if ($data['evento']->pais_id) {
               $data['pais'] = $this->mapa_model->get_pais_id($data['evento']->pais_id);
          }
          if ($data['evento']->estado_id) {
               $data['estado'] = $this->mapa_model->get_estado_id($data['evento']->estado_id);
          }
          $data['hoteles'] = $this->hoteles_model->get_hotele_evento($id);
            $data['informacion'] = $this->frase_model->get_by_id(8);


          $this->breadcrumbs->push($data['evento']->titulo, 'evento');
          $this->view('evento_detalles', $data );
     }

     public function get_eventos(){
          $mes = $this->input->post('mes');
          $anio = $this->input->post('anio');
          $eventos = array();
          $eventos = $this->evento_model->evetos($mes, $anio);
          foreach ($eventos as $key => $evento) {
               $evento->pais = $this->mapa_model->get_pais_id($evento->pais_id);
          }
          echo json_encode($eventos);
     }

     public function insert_registro(){

          $this->load->library('form_validation');
          $this->form_validation->set_rules('nombre','Nombre(s)','trim|required');
          $this->form_validation->set_rules('apellidos','Apellido(s)','trim|required');
          $this->form_validation->set_rules('organizacion','Organización','trim|required');
          $this->form_validation->set_rules('pais','País','trim|required');
          $this->form_validation->set_rules('correo','Correo','trim|required');
          $this->form_validation->set_rules('telefono','Teléfono','trim|required');
          $this->form_validation->set_rules('direccion','Dirección','trim|required');
          if ($this->input->post('viene_acompanado') == 1) {
               $this->form_validation->set_rules('acompanante','Datos del acompañante','trim|required');
          }
          $this->form_validation->set_rules('loca_foraneo',' De donde nos visitas','trim|required');



          $respuesta = array();
          if ($this->form_validation->run() == TRUE) {
               $save['nombre'] = $this->input->post('nombre');
               $save['apellidos'] = $this->input->post('apellidos');
               $save['organizacion'] = $this->input->post('organizacion');
               $save['pais'] = $this->input->post('pais');
               $save['correo'] = $this->input->post('correo');
               $save['telefono'] = $this->input->post('telefono');
               $save['direccion'] = $this->input->post('direccion');
               $save['evento_id'] = $this->input->post('evento_id');
               $save['tipo_asistente'] = $this->input->post('tipo_asistente');

               $save['viene_acompanado'] = $this->input->post('viene_acompanado');
               $save['acompanante'] = $this->input->post('acompanante');

               $save['loca_foraneo'] = $this->input->post('loca_foraneo');


               $save['boleto'] = '';
               if ($_FILES['boleto']['name'] != '') {
                    // Config upload
                    $config['upload_path'] = 'uploads/boletos/';
                    $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
                    $config['max_size'] = 1024 * 1024 * 5;
                    $config['encrypt_name'] = true;
                    $this->load->library('upload', $config);

                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('boleto')) {
                         $save['boleto'] = $this->upload->data('file_name');
                         $save['tipo'] =  pathinfo($_FILES["boleto"]["name"], PATHINFO_EXTENSION);
                    } else {
                         $retirar = array('<p>', '</p>' );
                         $respuesta = array(
                              'tipo' => 'error',
                              'mensaje' => str_replace($retirar, "",$this->upload->display_errors())
                         );
                         echo json_encode($respuesta, true);
                         die;
                    }
               }


               $saved = $this->registro_model->insert($save);

               if ($saved) {
                    $respuesta = array(
                         'tipo' => 'exito',
                         'mensaje' => 'Tu registro se ha completado exitosamente! Por favor dirígete a tu correo electrónico registrado para hacer Check in al Congreso.'
                    );
               }else{
                    $respuesta = array(
                         'tipo' => 'error',
                         'mensaje' => 'Error al registrarte, intentalo más tarde.'
                    );
               }

          }else{
               $respuesta = array(
                    'tipo' => 'validacion',
                    'mensaje' => $this->form_validation->error_array()
               );
          }
          echo json_encode($respuesta, true);



     }
}
