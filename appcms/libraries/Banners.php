<?php

class Banners {

    var $CI;

    function __construct() {
        $this->CI = & get_instance();

        $this->CI->load->model('banner_model');
    }

    function load_banners($grupo_id, $cantidad = 5, $layout = 'default') {
        $data['id'] = $grupo_id;
        $data['banners'] = $this->CI->banner_model->get_banners_por_grupo($grupo_id, true, $cantidad);
        $this->CI->load->view('banners/' . $layout, $data);
    }

}
