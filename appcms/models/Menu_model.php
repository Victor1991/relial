<?php 

class Menu_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_link_by_id($id) {
        return $this->db->where('id', $id)->get('menu_links')->row();
    }
    
    public function get_menu_tree($rol_usuario, $front_end = false) {
        // si se pasa la clave, en lugar del id
        if (!is_numeric($rol_usuario)) {
            $row = $this->rol_model->get_by_clave($rol_usuario);
            $rol_usuario = $row ? $row->id : false;
        }

        $all_links = $this->db->order_by('posicion', 'asc')
                ->get('menu_links')->result_array();

        $this->load->helper('url');

        $links = array();

        // reorganizamos los links y nos deshacemos de los links restringidos
        $all_links = $this->make_url_array($all_links, $rol_usuario, $front_end);
        foreach ($all_links AS $row) {
            $links[$row['id']] = $row;
        }

        unset($all_links);
        
        $link_array = array();

        // creamos el array parent > children
        foreach ($links AS $row) {
            if (array_key_exists($row['parent'], $links)) {
                $links[$row['parent']]['children'][] = & $links[$row['id']];
            }

            if (!isset($links[$row['id']]['children'])) {
                $links[$row['id']]['children'] = array();
            }

            // this is a root link
            if ($row['parent'] == 0) {
                $link_array[] = & $links[$row['id']];
            }
        }

        return $link_array;
    }
    
    public function make_url_array($links, $rol_usuario = false, $front_end = false) {
        $rol = $this->rol_model->get_by_id($rol_usuario);
        
        foreach ($links as $key => &$row) {
            // El acceso es restringido, verificamos el acceso
            if (!empty($row['restringir_roles']) && $front_end) {
                if (!$this->_verificar_acceso($rol, $row['restringir_roles'])) {
                    unset($links[$key]);
                    continue;
                }
            }

            // Si el tipo de link es diferente a URL, completamos el link
            switch ($row['tipo_link']) {
                case 'uri':
                    $row['url'] = site_url($row['uri']);
                    break;
                case 'pagina':
                    $this->load->model('pagina_model');
                    $params = array_filter(array(
                        'id' => $row['pagina_id'],
                        'estatus' => ($front_end ? 1 : FALSE)
                    ));
                    $pagina = $this->pagina_model->get_by($params);
                    if ($pagina) {
                        $row['url'] = site_url('sitio/'.$pagina->slug);
                        // Si la página esta restringida, verificamos acceso
                        if ($front_end && !empty($pagina->restringir_para)) {
                            echo 'verifica acceso a página';
                            if ($this->_verificar_acceso($rol_usuario, $pagina->restringir_para)) {
                                unset($links[$key]);
                            }
                        } 
                    } else {
                        unset($links[$key]);
                    }
                    break;
            }
        }
        
        return $links;
    }
    
    public function actualiza_orden($params) {
        $orden = 0;
        $orden_children = 0;
        
        foreach ($params as $item) {
            $parent_id = 0;
            $this->db->where('id', $item->id)
                    ->update('menu_links', array('parent' => $parent_id, 'posicion' => $orden));
            if (isset($item->children)) {
                $parent_id = $item->id;
                $orden_children = 0;
                foreach ($item->children as $child) {                    
                    $this->db->where('id', $child->id)
                            ->update('menu_links', array('parent' => $parent_id, 'posicion' => $orden_children));
                    $orden_children++;
                    if (isset($child->children)) {
                        $parent_id = $child->id;
                        $orden_children2 = 0;
                        foreach ($child->children as $value) {
                            $this->db->where('id', $value->id)
                                    ->update('menu_links', array('parent' => $parent_id, 'posicion' => $orden_children2));
                            $orden_children2++;
                        }                        
                    }                    
                }
            }
            $orden++;
        }
    }
    
    public function actualizar($id, $data) {
        return $this->db->where('id', $id)->update('menu_links', $data);
    }
    
    public function crear($data) {
        $this->db->insert('menu_links', $data);
        return $this->db->insert_id();
    }
    
    public function eliminar_hijos($id) {
        return $this->db->where('parent', $id)->delete('menu_links');        
    }
    
    public function eliminar($id) {
        return $this->db->where('id', $id)->delete('menu_links');        
    }
    
    private function _verificar_acceso($rol_usuario, $roles_autorizados) {
        $roles_autorizados = (array) explode(',', $roles_autorizados);

        if (!$rol_usuario || ($rol_usuario->clave != 'admin' && !in_array($rol_usuario->id, $roles_autorizados))) {
            return false;
        } else {
            return true;
        }
    }

}