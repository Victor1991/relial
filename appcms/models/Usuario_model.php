<?php

class Usuario_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all() {
        $this->db->select("usuarios.*, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) as nombre_completo, roles.nombre as rol_nombre", FALSE)
                ->from("usuarios")
                ->join("roles", "roles.id = usuarios.rol_id", "inner")
                ->order_by("usuarios.nombre, usuarios.apellidos", "asc");
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_many_by($params = array(), $limit = FALSE, $start = 0) {
        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('usuarios.activo', $params['activo']);
        }

        if (!empty($params['rol_id'])) {
            $this->db->where('usuarios.rol_id', $params['rol_id']);
        }

        if (!empty($params['nombre'])) {
            $this->db->like('usuarios.username', trim($params['nombre']))
                    ->or_like('usuarios.email', trim($params['nombre']))
                    ->or_like('usuarios.nombre', trim($params['nombre']))
                    ->or_like('usuarios.apellidos', trim($params['nombre']));
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->get_all();
    }

    public function get_by_id($id) {
        $query = $this->db->select("usuarios.*, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) as nombre_completo", FALSE)
                ->from("usuarios")
                ->where("id", $id)
                ->get();
        
        return $query->row();
        
    }
    
    public function count_by($params = array()) {
        $this->db->from('usuarios');

        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('usuarios.activo', $params['activo']);
        }

        if (!empty($params['rol_id'])) {
            $this->db->where('usuarios.rol_id', $params['rol_id']);
        }

        if (!empty($params['nombre'])) {
            $this->db->like('usuarios.username', trim($params['nombre']))
                    ->or_like('usuarios.email', trim($params['nombre']))
                    ->or_like('usuarios.nombre', trim($params['nombre']))
                    ->or_like('usuarios.apellidos', trim($params['nombre']));
        }

        return $this->db->count_all_results();
    }

    public function insert($data) {
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $data['fecha_edicion'] = $data['fecha_creacion'];
        
        $this->db->insert('usuarios', $data);
        
        return $this->db->insert_id();
    }
    
    public function update($id, $data) {
        return $this->db->update('usuarios', $data, array('id' => $id));
    }
    
    public function delete($id) {
        return $this->db->delete('usuarios', array('id' => $id));
    }
}