<?php

class Galeria_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all($limit=FALSE, $start=0) {
        $this->db->select('galerias.*, galerias_imagenes.archivo')
                ->from('galerias')
                ->join('galerias_imagenes', 'galerias_imagenes.galeria_id = galerias.id AND galerias_imagenes.principal = 1 ', 'left')
                ->group_by('galerias.id');
        
        if ($limit) {
            $this->db->limit($limit, $start);
        }
                
        return $this->db->get()->result();

    }
    
    public function get_all_year($year) {
        $this->db->select('galerias.*, galerias_imagenes.archivo')
                ->from('galerias')
                ->join('galerias_imagenes', 'galerias_imagenes.galeria_id = galerias.id AND galerias_imagenes.principal = 1 ', 'left')
                ->where("YEAR(galerias.fecha_vista) = $year")
                ->where("estatus",1)
                ->group_by('galerias.id');                
        return $this->db->get()->result();
    }
    
    public function get_galerias_year(){
        $d = $this->db->query('SELECT YEAR(fecha_vista) as date  FROM galerias WHERE estatus = 1 GROUP BY YEAR(fecha_vista) ORDER BY date DESC')->result();
        $dates = [];
        foreach ($d as $value) {
            $dates[$value->date] = $this->get_all_year($value->date);
        }
        return $dates;
    }
    
    public function get_activas($limit=FALSE, $start=0) {
        $this->db->where('galerias.estatus', 1);
        return $this->get_all($limit, $start);        
    }
    
    public function get_activas_inicio($limit=FALSE, $start=0) {
        $this->db->where('galerias.estatus', 1);
        $this->db->where('galerias.inicio', 1);
        return $this->get_all($limit, $start);        
    }
    
    public function get_by_id($galeria) {
        return $this->db->select("galerias.*")
                ->from('galerias')
                ->where('galerias.id', $galeria)
                ->get()
                ->row();
    }


    public function guardar($data) {
        if (isset($data['id']) && !empty($data['id'])) {
            $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
            $this->db->where('id', $data['id']);
            unset($data['id']);
            return $this->db->update('galerias', $data);
        } else {
            $data['fecha_actualizacion'] = $data['fecha_creacion'] = date("Y-m-d H:i:s");
            $data['id'] = NULL;
            $this->db->insert('galerias', $data);

            return $this->db->insert_id();
        }
    }
    
    public function incrementar_imagenes($galeria) {
        $this->db->query("UPDATE galerias SET num_imagenes = num_imagenes+1 WHERE id = $galeria");
    }
    
    public function decrementar_imagenes($galeria) {
        $this->db->query("UPDATE galerias SET num_imagenes = num_imagenes-1 WHERE id = $galeria");
    }
    
    public function get_imagenes_front($galeria) {
        return $this->db->select()
                ->from('galerias_imagenes')
                ->where('galeria_id', $galeria)
                ->where('mostrar', 1)
                ->order_by('principal', 'desc')            
                ->order_by('id', 'desc')
                ->get()
                ->result();
    }
    
    public function get_imagenes($galeria) {
        return $this->db->select()
                ->from('galerias_imagenes')
                ->where('galeria_id', $galeria)
                ->order_by('principal', 'desc')            
                ->order_by('id', 'desc')
                ->get()
                ->result();
    }
    
    public function get_imagen($id) {
        return $this->db->select()
            ->from('galerias_imagenes')
            ->where('id', $id)
            ->get()
            ->row();
    }
    
    public function eliminar($id) {
        return $this->db->delete('galerias', array('id' => $id));
    }
    
    public function eliminar_imagen($id) {
        
        $img = $this->get_imagen($id);
        
        if ($img) {        
            $this->db->where('id', $id)
                    ->delete('galerias_imagenes');

            if ($this->db->affected_rows() == 1) {
                $this->decrementar_imagenes($img->galeria_id);
            }
        }
        
    }
    
    public function eliminar_imagenes_galeria($galeria) {
        $this->db->where('galeria_id', $galeria)->delete('galerias_imagenes');
    }


    public function guardar_imagen($data) {
        if (isset($data['principal']) && $data['principal']) {
            $this->db->update('galerias_imagenes', array('principal' => 0), array('galeria_id' => $data['galeria_id']));
        }
        
        if (isset($data['id']) && !empty($data['id'])) {
            $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
            return $this->db->update('galerias_imagenes', $data, array('id' => $data['id']));
        } else {
            if (!$this->existe_img_principal($data['galeria_id'])) {
                $data['principal'] = 1;
            }
            $data['fecha_creacion'] = $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
            $this->db->insert('galerias_imagenes', $data);            
            $id = $this->db->insert_id();
            return $id;
        }
    }
    
    public function existe_img_principal($galeria) {
        $principal = $this->db->select('id')
                ->where('galeria_id', $galeria)
                ->get('galerias_imagenes')
                ->row();
        
        return $principal ? true : false;
    }


    public function get_imagenes_aleatorias($cant, $gal = 0) {
        $cant = (int)$cant;
        if (is_numeric($gal) && $gal > 0) {
            $gal = (int) $gal;
            $this->db->where("gal.id", $gal);
        }
        
        return $this->db->select("galerias_imagenes.*")
                ->from("galerias_imagenes")
                ->join("galerias", "galerias.id = galerias_imagenes.galeria_id", "inner")
                ->order_by("id", "random")
                ->where("galerias.estatus", 1)
                ->limit($cant)
                ->get()
                ->result();
    }
}