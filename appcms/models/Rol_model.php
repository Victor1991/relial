<?php

class Rol_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_by_id($id) {
        return $this->db->where('id', $id)->get('roles')->row();
    }
    
    public function get_by_clave($clave) {
        return $this->db->where('clave', $clave)->get('roles')->row();
    }
    
    public function get_all() {
        return $this->db->order_by('nombre', 'asc')->get('roles')->result();
    }
    
    public function get_opciones_dropdown($val_cero = NULL) {
        if (empty($val_cero)) {
            $opciones = array();
        } else {
            $opciones = array(0 => $val_cero);
        }
        
        $roles = $this->db->order_by('nombre', 'asc')->get('roles')->result();
        
        foreach ($roles as $rol) {
            $opciones[$rol->id] = $rol->nombre;
        }
        
        return $opciones;
    }
    
    public function agregar($data) {
        $this->db->insert('roles', $data);
        return $this->db->insert_id();
    }
    
    public function actualizar($id, $data) {
        return $this->db->where('id', $id)->update('roles', $data);
    }
    
    public function eliminar($id) {
        return $this->db->where('id', $id)->delete('roles');
    }
}