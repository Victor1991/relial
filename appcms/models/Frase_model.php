<?php

class Frase_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all() {
        return $this->db->get_where('frases',['type' =>0])->result();
    }
    
    public function get_by_id($id) {
        return $this->db->where('id', $id)->limit(1)->get('frases')->row();
    }
    
    public function get_random() {
        return $this->db->where('type',0)->order_by('id', 'random')->limit(1)->get('frases')->row();
    }
    
    public function eliminar($id) {
        return $this->db->where('id', $id)->delete('frases');
    }
    
    public function insert($data) {
        $this->db->insert('frases', $data);
        return $this->db->insert_id();
    }
    
    public function update($id, $data) {
        return $this->db->where('id', $id)->update('frases', $data);
    }
    
    
}