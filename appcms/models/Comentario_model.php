<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Comentario_model extends CI_Model {
    private $_table = 'comentarios';
    //relaciones a realizar de acuerdo al modulo
    private $_relations = array();
    
    public function __construct() {
        parent::__construct();
        $this->_relations['blog'] = array(
            'table' => 'posts po',
            'field_id' => 'po.id',
            'field_title' => 'po.titulo'
        );
        
    }
    
    public function get_all() {
        return $this->db->get($this->_table)->result();
    }
    
    public function get($id) {
        return $this->db->select("c.*")
                        ->select("IF(c.usuario_id > 0, CONCAT(u.nombre, ' ', u.apellidos), c.usuario_nombre) as usuario_nombre", false)
                        ->select("IF(c.usuario_id > 0, u.email, c.usuario_email) as usuario_email", false)
                        ->select("u.username")
                        ->from("comentarios c")
                        ->join("usuarios u", "c.usuario_id = u.id", "left")
                        
                        // si el comentario tiene id de usuario, checamos si aún existe
                        ->where("IF(c.usuario_id > 0, c.usuario_id = u.id, 1)")
                        ->where('c.id', $id)
                        ->get()
                        ->row();
    }

    /**
     * Obtener comentarios recientes
     * 
     * @param string $modulo nombre del módulo
     * @param int $limit El número de comentarios a obtener
     * @param int $estatus por default solocomentarios estatuss
     * @return array
     */
    public function get_recientes($modulo, $limit = 10, $estatus = '1') {
        $this->_get_all_setup($modulo);

        $this->db->where('c.estatus', $estatus)
                ->order_by('c.fecha_creacion', 'desc');

        if ($limit > 0) {
            $this->db->limit($limit);
        }

        return $this->get_all();
    }
    
    public function get_por_entrada($modulo, $entrada, $estatus, $limit = FALSE, $start = 0) {
        $this->_get_all_setup($modulo);
        
        $this->db
                ->where('c.entrada_id', $entrada)
                ->where('c.estatus', $estatus)
                ->order_by('c.fecha_creacion', 'desc');

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->get_all();
    }

    public function insert($data) {
        $data['fecha_creacion'] = date("Y-m-d H:i:s");
        $this->db->insert('comentarios', $data);
        return $this->db->insert_id();
    }
    
    public function update($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('comentarios', $data);
    }

    public function aprobar($id) {
        return $this->update($id, array('estatus' => '1'));
    }

    public function rechazar($id) {
        return $this->update($id, array('estatus' => 'spam'));
    }

    public function eliminar_por_entrada($modulo, $entrad_id) {
        return $this->db->where('modulo', $modulo)
                        ->where('entrada_id', $entrad_id)
                        ->delete('comentarios');
    }
    
    private function _get_all_setup($modulo = NULL) {
        $this->_table = null;
        $this->db
                ->select("c.*")
                ->from("comentarios c")
                ->select("IF(c.usuario_id > 0, CONCAT(u.nombre, ' ', u.apellidos), c.usuario_nombre) as usuario_nombre", false)
                ->select("IF(c.usuario_id > 0, u.email, c.usuario_email) as usuario_email", false)
                ->select("u.username")
                ->join("usuario u", 'c.usuario_id = u.id', 'left');
        
        if (!is_null($modulo)) {
            if (isset($this->_relations[$modulo])) {
                $this->db->select($this->_relations[$modulo]['field_title'] . " as entrada_titulo");
                $this->db->join($this->_relations[$modulo]['table'], $this->_relations[$modulo]['field_id'] . " = c.entrada_id", 'inner');
                $this->db->where('modulo', $modulo);
            } else {
                //si se busca un modulo que no existe, no regresamos nada 
                $this->where('0');
            }
        }
        
        
    }

}