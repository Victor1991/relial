<?php
/**
 *@14/01/2020
 */
 class Registro_model extends CI_Model{
      private $_table = 'registros_eventos';

      public function insert($save){
           $this->db->insert($this->_table, $save);
           return $this->db->insert_id();
      }

      public function update($id, $save){
           $this->db->where('id', $id);
           return $this->db->update($this->_table, $save);
      }

      public function get_by_id($id){
           $this->db->where('id', $id);
           return $this->db->get($this->_table)->row();
      }

      public function params($params = array()){
           $this->db->from($this->_table);

           if (isset($params['nombre'])) {
                $this->db->like("nombre", $params['nombre']);
           }
      }

      public function get_registro_evento($evento_id){
           $this->db->where('evento_id', $evento_id);
           return $this->db->get($this->_table)->result();
      }
 }

 ?>
