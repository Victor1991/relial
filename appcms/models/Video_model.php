<?php

class Video_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all() {
        return $this->db->get('videos')->result();
    }
    
    public function get_many_by($params = array(), $order_by = NULL, $limit = FALSE, $start = 0) {
        
        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('activo', $params['activo']);
        }
        
        if (!empty($params['buscar'])) {
            $this->db->like('titulo', trim($params['buscar']))
                    ->or_like('link', trim($params['buscar']));
        }
        
        if (!is_null($order_by) && is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        
        
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->get_all();
    }
    
    public function count_by($params = array()) {
        $this->db->from('videos');

        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('activo', $params['activo']);
        }

        if (!empty($params['buscar'])) {
            $this->db->like('titulo', trim($params['buscar']))
                    ->or_like('link', trim($params['buscar']));
        }

        return $this->db->count_all_results();
    }
    
    public function get_by_id($id) {
        return $this->db->where('id', $id)->get('videos')->row();
    }
    
    
    /**
     * Devuelve la página según los filtros($params) indicados
     * 
     * @param array $params
     * @return object
     */
    public function get_by($params) {
        $valid_fields = array('id', 'titulo', 'link', 'estatus');
        foreach ($params as $key => $value) {
            if (in_array($key, $valid_fields)) {
                $this->db->where($key, $value);
            }
        }
        
        return $this->db->get('videos')->row();
    }


    public function agregar($data) {
        unset($data['id']);
        $data['fecha_creacion'] = $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->db->insert('videos', $data);
        return $this->db->insert_id();
    }
    
    public function actualizar($id, $data) {
        $data['id'] = $id;
        $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        return $this->db->where('id', $id)->update('videos', $data);
    }
    
    public function eliminar($id) {
        return $this->db->delete('videos', array('id' => $id));
    }
    
}