<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model {
    private $_table = 'posts';
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all2() {
        $this->db
                ->select('blog.*, blog_categories.title AS category_title, blog_categories.slug AS category_slug')
                ->select('users.username, profiles.display_name')
                ->join('blog_categories', 'blog.category_id = blog_categories.id', 'left')
                ->join('profiles', 'profiles.user_id = blog.author_id', 'left')
                ->join('users', 'blog.author_id = users.id', 'left')
                ->order_by('created_on', 'DESC');

        return $this->db->get('blog')->result();
    }
    
    public function get_all() {
        return $this->db->get($this->_table)->result();
    }
    
    public function get_by_id($id) {
        $this->db->where('po.id', $id);
        return $this->_get();
    }
    
    public function get_by_slug($slug) {
        $this->db->where('po.slug', $slug);
        return $this->_get();
    }
    
    public function get_by($params = array()) {
        if (count($params) < 1) {
            return NULL;
        }
        
        if (isset($params['slug'])) {
            $this->db->where('po.slug', $params['slug']);
        }
        
        if (isset($params['categoria_slug'])) {
            $this->db->where('ca.slug', $params['categoria_slug']);
        }
        
        return $this->_get();
    }

    public function ultimos_posts($cant = 3) {
        $this->_get_all_setup();
        
        $this->db->where('po.estatus', 1);
        $this->db->order_by('po.fecha', 'desc');
        $this->db->limit($cant);
        
        return $this->get_all();
    }

    public function post_relacionados($categoria, $cant = 3, $orden = 'desc') {
        $this->_get_all_setup();
        
        $this->db->where('po.estatus', 1);
        $this->db->where('po.categoria_id', $categoria);
        $this->db->order_by('po.fecha', $orden);
        $this->db->limit($cant);
        
        return $this->get_all();
    }

    public function get_many($params = array(), $orden = NULL, $limit = 0, $start = 0) {
        $this->_get_all_setup();
        
        if (!empty($params['estatus'])) {
            $params['estatus'] = $params['estatus'] === 2 ? 0 : $params['estatus'];
            $this->db->where('po.estatus', $params['estatus']);
        }
        
        if (isset($params['categoria'])) {
            $this->db->where('po.categoria_id', $params['categoria']);
        }
        
        if (isset($params['no_categoria'])) {
            $this->db->where('po.categoria_id !=', $params['no_categoria']);
        }
        
        if (isset($params['in_categoria'])) {
            $this->db->where_in('po.categoria_id', $params['in_categoria']);
        }
        
        if (isset($params['evento'])) {
            $this->db->where('po.evento', (int)$params['evento']);
        }
        
        if (isset($params['autor'])) {
            $this->db->where('po.autor_id', $params['autor']);
        }
        
        if (isset($params['titulo'])) {
            $this->db->like('titulo', $params['titulo']);
        }
        
        if (isset($params['categoria_slug'])) {
            $this->db->where('ca.slug', $params['categoria_slug']);
        }
        
        if (!is_null($orden) && is_array($orden)) {
            foreach ($orden as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->get_all();
    }
    
    public function num_total_posts() {
        return $this->db->count_all_results('posts');
    }


    public function num_posts($params = array()) {
        $this->db->from('posts po');

        if (!empty($params['estatus'])) {
            $params['estatus'] = $params['estatus'] === 2 ? 0 : $params['estatus'];
            $this->db->where('po.estatus', $params['estatus']);
        }
        
        if (isset($params['autor'])) {
            $this->db->where('po.autor_id', $params['autor']);
        }
        
        if (isset($params['evento'])) {
            $this->db->where('po.evento', (int)$params['evento']);
        }
        
        if (isset($params['categoria'])) {
            $this->db->where('po.categoria_id', $params['categoria']);
        }
        
        if (isset($params['no_categoria'])) {
            $this->db->where('po.categoria_id !=', $params['no_categoria']);
        }
        
        if (isset($params['in_categoria'])) {
            $this->db->where_in('po.categoria_id', $params['in_categoria']);
        }
        

        if (isset($params['titulo'])) {
            $this->db->like('titulo', $params['titulo']); 
            //$this->db->where("MATCH (po.titulo) AGAINST ('{$params['titulo']}')", NULL, FALSE);
        }

        return $this->db->count_all_results();
    }

    public function insert($data) {
        $data['fecha_creacion'] = $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->db->insert('posts', $data);
        return $this->db->insert_id();
    }
    
    public function update($id, $data) {
        $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->db->where('id', $id);
        return $this->db->update('posts', $data);
    }
    
    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('posts');
    }
    
    function validar_slug($slug, $id = false, $count = false) {
        if ($this->existe_slug($slug . $count, $id)) {
            if (!$count) {
                $count = 1;
            } else {
                $count++;
            }
            return $this->validar_slug($slug, $id, $count);
        } else {
            return $slug . $count;
        }
    }
    
    public function existe_slug($slug, $id) {
        $query = $this->db->select('id')
                ->from('posts')
                ->where('slug', $slug)
                ->where('id !=', (int)$id)
                ->get();
        
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        
        return FALSE;
    }

    private function _get() {
        $this->db->select("po.*");
        $this->db->select("CONCAT(us.nombre, ' ', us.apellidos) as autor_nombre", FALSE); 
        $this->db->select("ca.nombre as categoria_nombre, ca.slug AS categoria_slug");
        $this->db->from("posts po");
        $this->db->join('categorias ca', 'ca.id = po.categoria_id', 'left');
        $this->db->join('usuarios us', 'us.id = po.autor_id', 'left');
        $this->db->order_by('po.fecha', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }
    
    private function _get_all_setup() {
        $this->_table = NULL;
        $this->db->select("po.*");
        $this->db->select("CONCAT(us.nombre, ' ', us.apellidos) as autor_nombre", FALSE); 
        $this->db->select("ca.nombre as categoria_nombre, ca.slug AS categoria_slug");
        $this->db->from('posts po');
        $this->db->join('categorias ca', 'ca.id = po.categoria_id', 'left');
        $this->db->join('usuarios us', 'us.id = po.autor_id', 'left');
        
    }
}