<?php

Class Banner_model extends CI_Model {

    function get_grupos($grupo_id = false) {
          if ($grupo_id) {
                $this->db->where('id', $grupo_id);
          }

        return $this->db->order_by('nombre', 'ASC')->get('banners_grupos')->result();
    }

    function get_grupo($grupo_id) {
        return $this->db->where('id', $grupo_id)->get('banners_grupos')->row();
    }

    function get_banners_directorio() {
         $this->db->select('banners.*, paises.nombre as pais');
         $this->db->from('banners');
         $this->db->where('banners_grupo_id', 2);
         $this->db->where('banners.estatus', 1);
         $this->db->join('paises', 'banners.pais_id = paises.id', 'left' );
         return $this->db->order_by('orden', 'ASC')->get()->result_array();

    }

    function get_banners_por_grupo($grupo_id, $activos = false, $limit = 5) {
        $this->db->where('banners_grupo_id', $grupo_id);
        if ($grupo_id != 1) {
            $banners = $this->db->order_by('orden', 'random')->get('banners')->result();
        } else {
            $banners = $this->db->order_by('orden', 'ASC')->get('banners')->result();
        }

        if ($activos) {
            $return = array();
            foreach ($banners as $banner) {
                if ($banner->fecha_inicio == '0000-00-00' OR is_null($banner->fecha_inicio)) {
                    $fdisponible_existe = false;
                    $fdisponible = '';
                } else {
                    $fdisponible_existe = true;
                    $fdisponible = $banner->fecha_inicio;
                }

                if ($banner->fecha_expira == '0000-00-00' OR is_null($banner->fecha_expira)) {
                    $fexpira_existe = false;
                    $fexpira = '';
                } else {
                    $fexpira_existe = true;
                    $fexpira = $banner->fecha_expira;
                }

                $fecha = date('Y-m-d');

                if ((!$fdisponible_existe || $fecha >= $fdisponible) && (!$fexpira_existe || $fecha < $fexpira)) {
                    $return[] = $banner;
                }

                if (count($return) == $limit) {
                    break;
                }
            }

            return $return;
        } else {
            return $banners;
        }
    }

    public function get_random_banner($group) {
        return $this->db->where('banners_grupo_id', $group)
                ->order_by('id', 'random')
                ->get('banners')
                ->row();
    }

    public function get_banner($banner_id) {
        $this->db->where('id', $banner_id);
        $result = $this->db->get('banners')->row();

        if ($result) {
            if ($result->fecha_inicio == '0000-00-00') {
                $result->fecha_inicio = '';
            }

            if ($result->fecha_expira == '0000-00-00') {
                $result->fecha_expira = '';
            }

            return $result;
        } else {
            return array();
        }
    }

    function guardar_banner($data) {
        if (isset($data['id'])) {
            $data['fecha_actualizacion'] = date('Y-m-d H:i:s');
            $this->db->where('id', $data['id']);
            return $this->db->update('banners', $data);
        } else {
            $data['fecha_creacion'] = $data['fecha_actualizacion'] = date('Y-m-d H:i:s');
            $data['orden'] = $this->get_sig_orden($data['banners_grupo_id']);
            $this->db->insert('banners', $data);
            return $this->db->insert_id();
        }
    }

    function guardar_banners_grupo($data) {
        if (isset($data['id']) && (bool) $data['id']) {
            $data['fecha_actualizacion'] = date('Y-m-d H:i:s');
            $this->db->where('id', $data['id']);
            return $this->db->update('banners_grupos', $data);
        } else {
            $data['fecha_creacion'] = $data['fecha_actualizacion'] = date('Y-m-d H:i:s');
            $this->db->insert('banners_grupos', $data);
            return $this->db->insert_id();
        }
    }

    function eliminar_banner($banner_id) {
        $this->db->where('id', $banner_id);
        return $this->db->delete('banners');
    }

    function eliminar_banners_grupo($grupo_id) {
        $this->db->where('banners_grupo_id', $grupo_id);
        $this->db->delete('banners');

        $this->db->where('id', $grupo_id);
        return $this->db->delete('banners_grupos');
    }

    function get_sig_orden($grupo_id) {
        $this->db->where('banners_grupo_id', $grupo_id);
        $this->db->select('orden');
        $this->db->order_by('orden DESC');
        $this->db->limit(1);
        $result = $this->db->get('banners')->row();
        if ($result) {
            return $result->orden + 1;
        } else {
            return 0;
        }
    }

    public function organizar($banners) {
        foreach ($banners as $key => $value) {
            $this->db->where('id', $key);
            $this->db->update('banners', array('orden' => $value));
        }
    }
}
