<?php

class Evento_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        return $this->db->get('eventos')->result();
    }

    public function get_by_id($id) {
        return $this->db->where('id', $id)
                ->limit(1)
                ->get('eventos')
                ->row();
    }

     public function evetos($mes, $anio){
          $this->db->group_start();
          $this->db->where('MONTH(fecha_inicio)',$mes);
          $this->db->where('YEAR(fecha_inicio)',$anio);
               $this->db->or_group_start();
               $this->db->where('MONTH(fecha_fin)',$mes);
               $this->db->where('YEAR(fecha_fin)',$anio);
               $this->db->group_end();

          $this->db->group_end();

          return $this->get_all();
     }

    public function get_many_by($params = array(), $limit = FALSE, $start = 0) {
        if (isset($params['inicio']) && !empty($params['inicio'])) {
            $this->db->where('fecha_inicio >=', $params['inicio']);
        }

        if (isset($params['fin']) && !empty($params['fin'])) {
            $this->db->where('fecha_fin <=', $params['fin']);
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by('fecha_inicio', 'desc');

        return $this->get_all();
    }

    public function guardar($data) {
        if (isset($data['id']) && $data['id']) {
            $this->db->where('id', $data['id']);
            $this->db->update('eventos', $data);
            return $data['id'];
        } else {
            $data['id'] = NULL;
            $this->db->insert('eventos', $data);
            return $this->db->insert_id();
        }
    }

    public function eliminar($id) {
        return $this->db->delete('eventos', array('id' => $id));
    }

    public function proximos_eventos($fecha = NULL, $cant = 5) {
        if (is_null($fecha)) {
            $fecha = date("Y-m-d");
        }

        $this->db->order_by('fecha_inicio', 'asc');

        return $this->get_many_by(array('inicio' => $fecha), $cant);
    }
}
