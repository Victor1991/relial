<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categoria_model extends CI_Model {

    function __construct() {
        parent::__construct(); 
    }
    
    public function get_all() {
        return $this->db->get('categorias')->result();
    }
    
    public function get($id) {
        return $this->db->where('id', $id)->get('categorias')->row();
    }
    
    public function get_por_slug($slug) {
        return $this->db->where('slug', $slug)->get('categorias')->row();
    }
    
    public function get_many_by($params, $orden=NULL, $limit=FALSE, $start=0) {

        if (isset($params['staus'])) {
            $this->db->where('staus', $params['staus']);
        }

        if (isset($params['modulo'])) {
            $this->db->where('modulo', $params['modulo']);
        }
        
        if (isset($params['parent'])) {
            $this->db->where('parent', $params['parent']);
        }
        
        if (isset($params['exclude'])) {
            $this->db->where('id !=', $params['exclude']);
        }
        
        
        if (isset($params['modulo'])) {
            $this->db->where('modulo', $params['modulo']);
        }
        
        if (isset($params['buscar']) && !empty($params['buscar'])) {
            $this->db->like('nombre', $params['buscar']);
        }
        
        if (!is_null($orden) && is_array($orden)) {
            foreach($orden as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->get_all();
    }
    
    public function insert($data) {
        $data['fecha_creacion'] = $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->db->insert('categorias', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->db->where('id', $id);
        return $this->db->update('categorias', $data);
    }

    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('categorias');
    }
    
    function validar_slug($slug, $id = false, $count = false) {
        if ($this->existe_slug($slug . $count, $id)) {
            if (!$count) {
                $count = 1;
            } else {
                $count++;
            }
            return $this->validar_slug($slug, $id, $count);
        } else {
            return $slug . $count;
        }
    }
    
    public function existe_slug($slug, $id) {
        $query = $this->db->select('id')
                ->from('categorias')
                ->where('slug', $slug)
                ->where('id !=', (int)$id)
                ->get();
        
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        
        return FALSE;
    }
}
