<?php
class Mapa_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_paises() {
        $paises = $this->get_all_paises();

        foreach ($paises as &$p) {
            $p->instituciones = $this->db->get_where('paises_instituciones', array('pais_id' => $p->id))->result();
        }
        return $paises;
    }

    public function save($data) {
        if(isset($data['id'])){
            return $this->db->where('id', $data['id'])->update('paises_instituciones', $data);
        }else{
            return $this->db->insert('paises_instituciones', $data);
        }
    }

    public function delete($id){
        return $this->db->where('id', $id)->delete('paises_instituciones');
    }

     public function get_all_paises(){
          return $this->db->get('paises')->result();
     }

     public function get_estados_pais($pais_id){
          return $this->db->where('pais_id', $pais_id)->get('estado')->result();
     }


     public function get_pais_id($pais_id){
          return $this->db->where('id', $pais_id)->get('paises')->row();
     }

     public function get_estado_id($estado_id){
          return $this->db->where('id', $estado_id)->get('estado')->row();
     }

}
