<?php

class Pagina_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all() {
        return $this->db->get('paginas')->result();
    }
    
    public function get_many_by($params = array(), $order_by = NULL, $limit = FALSE, $start = 0) {
        
        if (!is_null($order_by) && is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        
        
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->get_all();
    }
    
    public function get_by_id($id) {
        return $this->db->where('id', $id)->get('paginas')->row();
    }
    
    public function get_by_slug($slug) {
        return $this->db->where('slug', $slug)->get('paginas')->row();
    }
    
    /**
     * Devuelve la página según los filtros($params) indicados
     * 
     * @param array $params
     * @return object
     */
    public function get_by($params) {
        $valid_fields = array('id', 'parent', 'titulo', 'slug', 'estatus');
        foreach ($params as $key => $value) {
            if (in_array($key, $valid_fields)) {
                $this->db->where($key, $value);
            }
        }
        
        return $this->db->get('paginas')->row();
    }


    public function agregar($data) {
        unset($data['id']);
        $data['fecha_creacion'] = $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->db->insert('paginas', $data);
        return $this->db->insert_id();
    }
    
    public function actualizar($id, $data) {
        $data['id'] = $id;
        $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        return $this->db->where('id', $id)->update('paginas', $data);
    }
    
    public function eliminar($id) {
        return $this->db->delete('paginas', array('id' => $id));
    }
    
    function validar_slug($slug, $id = false, $count = false) {
        if ($this->existe_slug($slug . $count, $id)) {
            if (!$count) {
                $count = 1;
            } else {
                $count++;
            }
            return $this->validar_slug($slug, $id, $count);
        } else {
            return $slug . $count;
        }
    }
    
    public function existe_slug($slug, $id) {
        $query = $this->db->select('id')
                ->from('paginas')
                ->where('slug', $slug)
                ->where('id !=', (int)$id)
                ->get();
        
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        
        return FALSE;
    }
}