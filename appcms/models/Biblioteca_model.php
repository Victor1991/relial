<?php

class Biblioteca_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_libros($limit=FALSE, $start=0) {
        $this->db->select("biblioteca.*, categorias.nombre as categoria_nombre")
                ->from("biblioteca")
                ->join("categorias", "categorias.id = biblioteca.categoria_id", "left");
        
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->db->get()->result();
    }
    
    public function get_libro($id) {
        return $this->db->where('id', $id)->get('biblioteca')->row();
    }
    
    public function get_libro_by_slug($slug) {
        return $this->db->select("biblioteca.*, categorias.nombre as categoria_nombre, categorias.slug as categoria_slug")
                ->from("biblioteca")
                ->join("categorias", "categorias.id = biblioteca.categoria_id", "left")
                ->where('biblioteca.slug', $slug)
                ->limit(1)
                ->get()
                ->row();
    }
    
    public function get_ultimo_libro() {
        return $this->db->select("biblioteca.*, categorias.nombre as categoria_nombre")
                ->from("biblioteca")
                ->join("categorias", "categorias.id = biblioteca.categoria_id", "left")
                ->where('biblioteca.estatus', 1)
//                ->where('biblioteca.id',107)
                ->order_by('id','DESC')
                ->limit(1)
                ->get()
                ->row();
    }
    
    public function get_libro_destacado() {
        return $this->db->select("biblioteca.*, categorias.nombre as categoria_nombre")
                ->from("biblioteca")
                ->join("categorias", "categorias.id = biblioteca.categoria_id", "left")
                ->where('biblioteca.estatus', 1)
                ->where('biblioteca.destacado', 1)
//                ->where('biblioteca.id',107)
                ->order_by('id','DESC')
                ->limit(1)
                ->get()
                ->row();
    }
    
    public function get_many_libros($params, $orden=NULL, $limit=FALSE, $start=0) {
        if (!empty($params['estatus'])) {
            $params['estatus'] = $params['estatus'] === 2 ? 0 : $params['estatus'];
            $this->db->where('biblioteca.estatus', $params['estatus']);
        }
        
        if (isset($params['categoria']) && is_numeric($params['categoria'])) {
            $this->db->where('biblioteca.categoria_id', (int)$params['categoria']);
        }
        
        if (isset($params['titulo']) && strlen($params['titulo']) > 1) {
            $this->db->like('biblioteca.titulo', $params['titulo']);
        }
        
        if (isset($params['autor']) && strlen($params['autor']) > 1) {
            $this->db->like('biblioteca.autor', $params['autor']);
        }
        
        if (isset($params['editorial']) && strlen($params['editorial']) > 1) {
            $this->db->like('biblioteca.editorial', $params['editorial']);
        }
        
        if (isset($params['idioma']) && strlen($params['idioma']) > 1) {
            $this->db->like('biblioteca.idioma', $params['idioma']);
        }
        
        if (isset($params['buscar']) && strlen($params['buscar']) > 1) {
            $this->db->like('biblioteca.titulo', $params['buscar']);
            $this->db->or_like('biblioteca.autor', $params['buscar']);
            $this->db->or_like('biblioteca.editorial', $params['buscar']);
            $this->db->or_like('biblioteca.descripcion', $params['buscar']);
        }
        
        if (is_array($orden)) {
            foreach ($orden as $key => $value) {
                $this->db->order_by('biblioteca.'.$key, $value);
            }
        }
        return $this->get_libros($limit, $start);
    }
    
    public function num_libros($params = array()) {
        $this->db->from('biblioteca');

        if (!empty($params['estatus'])) {
            $params['estatus'] = $params['estatus'] === 2 ? 0 : $params['estatus'];
            $this->db->where('biblioteca.estatus', $params['estatus']);
        }
        
        if (isset($params['categoria']) && is_numeric($params['categoria'])) {
            $this->db->where('biblioteca.categoria_id', (int)$params['categoria']);
        }
        
        if (isset($params['titulo']) && strlen($params['titulo']) > 1) {
            $this->db->like('biblioteca.titulo', $params['titulo']);
        }
        
        if (isset($params['autor']) && strlen($params['autor']) > 1) {
            $this->db->like('biblioteca.autor', $params['autor']);
        }
        
        if (isset($params['editorial']) && strlen($params['editorial']) > 1) {
            $this->db->like('biblioteca.editorial', $params['editorial']);
        }
        
        if (isset($params['idioma']) && strlen($params['idioma']) > 1) {
            $this->db->like('biblioteca.idioma', $params['idioma']);
        }
        
        if (isset($params['buscar']) && strlen($params['buscar']) > 1) {
            $this->db->like('biblioteca.titulo', $params['buscar']);
            $this->db->or_like('biblioteca.autor', $params['buscar']);
            $this->db->or_like('biblioteca.editorial', $params['buscar']);
            $this->db->or_like('biblioteca.descripcion', $params['buscar']);
        }

        return $this->db->count_all_results();
    }
    
    public function agregar_libro($data) {
        $data['fecha_creacion'] = $data['fecha_edicion'] = date("Y-m-d H:i:s");
        $this->db->insert('biblioteca', $data);
        return $this->db->insert_id();
    }
    
    public function actualizar_libro($id, $data) {
        $data['fecha_edicion'] = date("Y-m-d H:i:s");
        return $this->db->where('id', $id)->update('biblioteca', $data);
    }
    
    public function eliminar_libro($id) {
        return $this->db->where('id', $id)->delete('biblioteca');
    }
    
    function validar_slug($slug, $id = false, $count = false) {
        if ($this->existe_slug($slug . $count, $id)) {
            if (!$count) {
                $count = 1;
            } else {
                $count++;
            }
            return $this->validar_slug($slug, $id, $count);
        } else {
            return $slug . $count;
        }
    }
    
    public function existe_slug($slug, $id) {
        $query = $this->db->select('id')
                ->from('biblioteca')
                ->where('slug', $slug)
                ->where('id !=', (int)$id)
                ->get();
        
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        
        return FALSE;
    }
    
    public function add_download($id){
        $libro = $this->db->get_where('biblioteca', array('id' => $id))->row();
        
        if($libro){
            $downloads = ++$libro->descargas;
        }
        
        return $this->db->where('id', $id)->update('biblioteca', ['descargas' => $downloads]);
        
    }
 
    
    public function destacar($id) {
        
        
        $this->db->where('id > 0')->update('biblioteca', ['destacado' => 0]);
        
        $this->db->where('id', $id)->update('biblioteca', ['destacado' => 1]);
        
    }
}