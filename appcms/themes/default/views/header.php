<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">   
        <title>CMS - Suma Web Diseño</title>
        <meta name="description" content="Mi sitio">
        <meta name="keywords" content="HTML5, CSS3, Theme, APPCMS"> 

        <!--bootstrap3 css-->
        <link href="<?php echo theme_assets('bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!--ion icon fonts css-->
        <link href="<?php echo theme_assets('icons/css/ionicons.css'); ?>" rel="stylesheet">
        <!--custom css-->
        <link href="<?php echo theme_assets('css/style.css'); ?>" rel="stylesheet" type="text/css">
        <!--google  font family-->

        <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400italic' rel='stylesheet' type='text/css'>
        <!--flex slider css-->
        <link href="<?php echo theme_assets('css/flexslider.css'); ?>" rel="stylesheet">
        <!--animated  css-->
        <link href="<?php echo theme_assets('css/animate.css'); ?>" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <header class="header-main">
            <div class="top-bar">

                <div class="container">
                    <ul class="info list-inline pull-left hidden-xs">
                        <li><i class="ion-ios-email-outline"></i> mail@domain.com</li>
                        <li><i class="ion-iphone"></i> +91 (123) 456 78 90</li>
                    </ul>  
                </div><!--container end-->
            </div><!--topbar end-->
            <div class="navbar navbar-inverse sticky-nav sticky yamm" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo site_url(); ?>" style="padding: 15px 0;">
                            <img style="max-height: 50px;" src="<?php echo theme_assets('img/logo-suma.png'); ?>" class="img-responsive" alt="">
                        </a>
                    </div>
                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav navbar-right">
                            <?php foreach ($menu_links as $link): ?>                            
                                <?php if (count($link['children'])): ?>
                                <li class="dropdown">
                                    <a href="<?php echo $link['url']; ?>" class="dropdown-toggle js-activated" data-toggle="dropdown"><?php echo $link['titulo'] ?></a>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($link['children'] as $sublink): ?>
                                        <li>
                                            <a href="<?php echo $sublink['url']; ?>" <?php echo ($sublink['target'] == '_blank') ? 'target="_blank"' : ''; ?> >
                                                <?php echo $sublink['titulo']; ?>
                                            </a>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>                                
                                <?php else: ?>
                                <li class="dropdown">
                                    <a href="<?php echo $link['url']; ?>"><?php echo $link['titulo']; ?></a> 
                                </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>

                    </div><!--/.nav-collapse -->
                </div><!--/.container-->
            </div><!--navigation end-->
        </header><!--header main end-->        

