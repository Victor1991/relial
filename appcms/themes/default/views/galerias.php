<section id="content-region-3" class="padding-40 page-tree-bg">
    <div class="container">
        <h3 class="page-tree-text">
            Galerías
        </h3>
    </div>
</section><!--page-tree end here-->
<div class="space-70"></div>

<div class="portfolio-masonary-wrapper">
    <div class="container mas-boxes">
        <?php foreach ($galerias as $galeria): ?>
        <div class="mas-boxes-inner direc-hover-box wow animated fadeInUp">
            <a href="<?php echo site_url('galerias/detalle/'.$galeria->id); ?>">
                <img src="<?php echo $galeria->archivo ? base_url('uploads/galerias/' . $galeria->archivo) : base_url('assets/admin/img/noimg.png'); ?>" class="img-responsive" alt="">
                <div class="direc-overlay">
                    <span><?php echo $galeria->titulo; ?></span>
                </div>
            </a>
        </div><!--masonry box-->
        <?php endforeach; ?>
    </div>

</div><!--masonary wrapper-->