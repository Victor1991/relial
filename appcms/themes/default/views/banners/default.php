<div class="home-slider">
    <div id="bizwrap-Carousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php for ($i = 0; $i < count($banners); $i++): ?>
            <li data-target="#bizwrap-Carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
            <?php endfor; ?>
        </ol>
        <div class="carousel-inner">
            <?php for ($i = 0; $i < count($banners); $i++): ?>
            <div class="item <?php echo $i == 0 ? 'active' : ''; ?>" style="background-image: url(<?php echo base_url('uploads/banners/'.$banners[$i]->imagen); ?>);" id="item-<?php echo ($i+1); ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center slider-wrapper">
                            <h1 class="animated fadeInDown delay-1"><?php echo $banners[$i]->titulo; ?></h1>
                            <p class=" animated fadeInDown delay-2"><?php echo $banners[$i]->descripcion; ?></p>
                            <?php if ($banners[$i]->link): ?>
                            <p><a href="<?php echo $banners[$i]->link; ?>" <?php echo $banners[$i]->nueva_ventana == 1 ? 'target="_blank"' : ''; ?> class="btn theme-btn-color btn-lg animated fadeInDown delay-3 btn-big">Ver</a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div><!--slide item 1-->
            <?php endfor; ?>            
        </div>
        <a class="left carousel-control" href="#bizwrap-Carousel" data-slide="anterior"><i class="ion-ios-arrow-left"></i></a>
        <a class="right carousel-control" href="#bizwrap-Carousel" data-slide="siguiente"><i class="ion-ios-arrow-right"></i></a>
    </div><!-- /.carousel -->
</div><!--home-slider-->