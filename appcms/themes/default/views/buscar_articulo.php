        <section id="content-region-3" class="padding-40 page-tree-bg">
            <div class="container">
                <h3 class="page-tree-text">Resultados de la busqueda</h3>
                <p class="text-center">Se encontraron <?php echo count($posts); ?> resultados</p>
            </div>
        </section><!--page-tree end here-->
        <div class="space-70"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php foreach ($posts as $post): ?>
                    <div class="blog-post-section">
                        <div class="blog-post-img">
                            <img src="<?php echo base_url('uploads/posts/'.$post->imagen); ?>" class="img-responsive" alt="">
                        </div>
                        <div class="blog-post-header">
                            <h3><a href="<?php echo site_url('articulos/'.$post->categoria_slug.'/'.$post->slug); ?>" class="hover-color"><?php echo $post->titulo; ?></a></h3>
                        </div>
                        <div class="blog-post-info">
                            <span><?php echo $post->autor_nombre ?> | <?php echo $post->fecha; ?> | <a href="<?php echo site_url('categoria/'.$post->categoria_slug); ?>" class="hover-color"><?php echo $post->categoria_nombre; ?></a></span>
                        </div>
                        <div class="blog-post-detail">
                            <?php echo $post->resumen; ?>
                        </div>
                        <div class="blog-post-more text-right">
                            <a href="<?php echo site_url('articulos/'.$post->categoria_slug.'/'.$post->slug); ?>" class="btn theme-btn-default btn-lg">Leer</a>
                        </div>
                    </div><!--blog post section end-->
                    <div class="space-40"></div>
                    <?php endforeach; ?>
                </div><!--blog content-->
                
                <div class="col-md-4">
                    <div class="sidebar-box">
                        <div class="widget-search">
                            <form class="search-form" method="get" action="<?php echo site_url('articulos/buscar'); ?>">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="term" id="bar-buscar" >
                                    <div class="input-group-addon">
                                        <button type="submit" class="btn"><i class="ion-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!--sidebar-box--><hr>
                    <div class="sidebar-box">
                        <h4>categorías</h4>
                        <ul class="cat-list">
                            <?php foreach ($this->categorias as $categoria): ?>
                            <li><a href="<?php echo site_url('categoria/'.$categoria->slug); ?>"  class="hover-color"><?php echo $categoria->nombre; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><hr>
<!--                    <div class="sidebar-box">
                        <h4>Text widget</h4>
                        <p>
                            Doloreiur quia commolu ptatemp dolupta oreprerum tibusam eumenis et consent accullignis dentibea autem inisita.
                        </p>
                    </div><hr>-->
                    <div class="sidebar-box">
                        <h4>Últimos articulos</h4>
                        <?php foreach ($ultimos_posts as $post): ?>
                        <div class="recent">
                            <span>
                                <img src="<?php echo base_url('uploads/posts/'.$post->imagen); ?>" class="img-responsive" alt="">
                            </span>
                            <p><a href="<?php echo site_url('articulos/'.$post->categoria_slug.'/'.$post->slug); ?>" class="hover-color"><?php echo $post->titulo ?></a></p>
                            <span class="recent-date"><?php echo mysql_to_date($post->fecha); ?></span>
                        </div>
                        <?php endforeach; ?>                        
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                </div><!--sideabr blog end -->
            </div>

        </div>
        <div class="space-50"></div>
