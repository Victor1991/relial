<section id="content-region-3" class="padding-40 page-tree-bg">
    <div class="container">
        <h3 class="page-tree-text">
            Galeria: <?php echo $galeria->titulo ?>
        </h3>
    </div>
</section><!--page-tree end here-->
<div class="space-70"></div>
<div class="container portfolio-details">
    <div class="row">
        <div class="col-md-offset-1 col-md-10 portfolio-single-slide margin-btm-40">
            <div class="flexslider">
                <ul class="slides">
                    <?php foreach ($galeria->imagenes as $imagen): ?>
                    <li>
                        <img src="<?php echo base_url('uploads/galerias/' . $imagen->archivo); ?>" class="img-responsive" alt="">
                        <?php if (!empty($imagen->descripcion)): ?>
                        <p class="flex-caption"><?php echo $imagen->descripcion; ?></p>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div><!--flex slider-->
        </div>
    </div>
</div>