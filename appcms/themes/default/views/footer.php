
        <div id="footer">
            <div class="container">
                <div class="row">                    
                    <div class="col-md-12">
                        <div class="footer-col text-right">
                            <ul class=" list-inline social-btn">
                                <li><a href="#"><i class="ion-social-facebook" data-toggle="tooltip" data-placement="top" title="" data-original-title="Like On Facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter" data-toggle="tooltip" data-placement="top" title="" data-original-title="Follow On twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-googleplus" data-toggle="tooltip" data-placement="top" title="" data-original-title="Follow On googleplus"></i></a></li>
                                <li><a href="#"><i class="ion-social-pinterest" data-toggle="tooltip" data-placement="top" title="" data-original-title="pinterest"></i></a></li>
                                <li><a href="#"><i class="ion-social-skype" data-toggle="tooltip" data-placement="top" title="" data-original-title="skype"></i></a></li>
                            </ul>
                        </div>
                    </div><!--get in touch col end-->
                </div><!--footer main row end-->  
                <div class="row">
                    <div class="col-md-12 text-center footer-bottom">
                        <a href="<?php echo site_url(); ?>"> <img src="<?php echo theme_assets('img/logo-suma.png'); ?>" style="width: 100px;" alt=""></a>
                        <div class="space-20"></div>
                        <span>&copy; 2016 All right reserved.</span>
                    </div>
                </div><!--footer copyright row end-->
            </div><!--container-->
        </div><!--footer main end-->
        <!--scripts-->
        <!--scripts-->
        <script src="<?php echo theme_assets('js/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/jquery.masonry.min.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/modernizr.custom.97074.js'); ?>"></script>
        <script src="<?php echo theme_assets('bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/jquery.easing.1.3.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/bootstrap-hover-dropdown.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/jquery.sticky.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/jquery.flexslider-min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/jquery.mixitup.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/parallax.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/wow.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/app.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/masonary-custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/modernizr.custom.97074.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo theme_assets('js/jquery.hoverdir.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $(' .mas-boxes > .direc-hover-box ').each(function () {
                    $(this).hoverdir();
                });
            });
        </script>
    </body>
</html>
