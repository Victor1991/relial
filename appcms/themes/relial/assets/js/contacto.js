$(document).ready(function () {
    $('.btn-send').click(function() {
        $.ajax({
            url : '/sitio/enviar_contacto',
            data : $('#form-contacto').serialize(),
            type : 'post',
            dataType : 'json',
            beforeSend: function () {
                $('.loading').show();
                $('.btn-send').hide();
            },
            success : function(data) {
                if (data.estatus == 'ok') {
                    $('.alert-message').html('<div class="alert alert-success alert-dismissible" role="alert">'
                        + ' <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>'
                        + data.mensaje
                        + '</div>');
                    $('#form-contacto')[0].reset();
                } else {
                    $('.alert-message').html('<div class="alert alert-danger alert-dismissible" role="alert">'
                        + ' <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>'
                        + data.mensaje
                        + '</div>');
                }
            },
            error : function(xhr, status) {
                console.log(xhr);
            },
            complete : function(xhr, status) {
                $('.loading').hide();
                $('.btn-send').show();
            }
        });
    });
    
});