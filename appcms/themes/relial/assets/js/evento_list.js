$( document ).ready(function() {
     $('#sandbox-container').datepicker({
         format: "MM / yyyy",
         defaultDate: new Date(),
         startView: 1,
         minViewMode: 1,
         language: "es",
         autoclose: true,
         keyboardNavigation: false
     }).on('changeDate', function(ev){
          get_eventos(ev.date);
     });

     get_eventos(new Date());
});

function initMap(lat1 = 19.432611182986452, lng1 = -99.1332117799011 ) {

  var myLatLng = {lat: lat1, lng: lng1};

  var map = new google.maps.Map(document.getElementById('map1'), {
    zoom: 17,
    scrollwheel: false,
    streetViewControl: false,
    mapTypeControl: false,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Evento'
  });

$('#mapa').modal('show');

}

function get_eventos(date) {
      $('#lista_eventos').html('');
      var today = new Date(date);
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();
     $.ajax({
          url:  base_url + 'eventos/get_eventos',
          type: 'POST',
          dataType: 'json',
          data: {mes: mm, anio:yyyy}
     })
     .done(function(data) {
          $.each(data, function( index, value ) {
               $("#lista_eventos").append('<div class="col-lg-4 col-sm-6 col-md-4" style="height: 620px;>\n\
                    <div class="latestBlogItem">\n\
                        <div class="lbi_thumb">\n\
                              <img src="'+validar_imagen(value.imagen)+'" alt="RPI" style="height: 300px; background: #ddd;" >\n\
                         </div>\n\
                       <div class="lbi_details">\n\
                                <a href="#" class="lbid_date">'+value.pais.nombre+'</a>\n\
                                <h2><a href="/evento/'+value.id+'" >'+value.titulo+'</a></h2>\n\
                                <h4>'+validador_fechas(value.fecha_inicio, value.fecha_fin)+'</h4>\n\
                                <a class="common_btn red_bg " href="/evento/'+value.id+'" ><span>Leer más</span></a>\n\
                            </div>\n\
                    </div>\n\
               </div>');
          });
     })
     .fail(function() {
          console.log("error");
     })
     .always(function() {
          console.log("complete");
     });
}

function validador_fechas(fecha_inicio, fecha_fin) {
     if (fecha_inicio == fecha_fin) {
          return format_fecha(fecha_inicio);
     }else{
          return format_fecha(fecha_inicio) + ' al ' + format_fecha(fecha_fin) ;
     }
}

function format_fecha(date) {
  var monthNames = [
    'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'
  ];
   var date = new Date(date);
  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' de ' + monthNames[monthIndex] + ' del ' + year;
}


function corrtar_texto(texto) {
     var text = texto;
     var count = 300;
     return text.slice(0, count) + (text.length > count ? "..." : "");
}

function validar_imagen(imagen) {
     return imagen == '' || imagen == null ? 'https://soloazar.s3.amazonaws.com/images/2346_crop169004_1024x576_ZhEsqLqfgrYqrvia4aiCvFyYX86k4tAt9Fl.jpg' : base_url + 'uploads/eventos/'+imagen;
}
