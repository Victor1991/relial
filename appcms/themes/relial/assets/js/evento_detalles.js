$( document ).ready(function() {
     setInterval(initMap(), 6000);
});

function initMap() {

  var myLatLng = {lat: parseFloat($('#lat').val()), lng: parseFloat($('#long').val())};

  var map = new google.maps.Map(document.getElementById('map1'), {
    zoom: 17,
    scrollwheel: false,
    streetViewControl: false,
    mapTypeControl: false,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Evento'
  });

}

function initMap2(lat1 = 19.432611182986452, lng1 = -99.1332117799011 ) {

  var myLatLng = {lat: lat1, lng: lng1};

  var map = new google.maps.Map(document.getElementById('map2'), {
    zoom: 17,
    scrollwheel: false,
    streetViewControl: false,
    mapTypeControl: false,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Evento'
  });

$('#mapa').modal('show');

}


function alerta($mensaje, $tipo) {
     swal({
          title: $tipo.toUpperCase(),
          text: $mensaje,
          type: $tipo == 'exito' ? 'success' : 'warning',
          button: "Ok",
     });
}

$("#form_registro").submit(function(e) {
     $( ".valid" ).remove();
     var post_url = $(this).attr("action"); //get form action url
     var request_method = $(this).attr("method"); //get form GET/POST method
     var form_data = $(this).serialize(); //Encode form elements for submission
     var data = new FormData(this);
     e.preventDefault(); // avoid to execute the actual submit of the form.
     $.ajax({
          url : post_url,
          type: request_method,
          data : data,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(data){
               if (data.tipo == 'exito') {
                    alerta(data.mensaje, data.tipo);
                    $('#form_registro')[0].reset();
                    $('#div_acompanante').hide();
                    $('#div_boleto').hide();
               }else if (data.tipo == 'error') {
                    alerta(data.mensaje, data.tipo);
               } else {
                    $.each(data.mensaje, function( index, value ) {
                         $("[name='"+index+"']").after("<span class='valid'>"+value+"</span>");
                    });
               }
          }
     });
});



$('input[type=radio][name=viene_acompanado]').change(function() {
    if (this.value == '1') {
        $('#div_acompanante').show('slow');
    }
    else if (this.value == '2') {
        $('#div_acompanante').hide('slow');
    }
});

$('input[type=radio][name=loca_foraneo]').change(function() {
    if (this.value == '2') {
        $('#div_boleto').show('slow');
    }
    else if (this.value == '1') {
        $('#div_boleto').hide('slow');
    }
});
