$(function() {
    //hang on event of form with id=myform
    $("#subcripcion").submit(function(e) {

        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = e.currentTarget.action;

        //do your own request an handle the results
        $.ajax({
                url: base_url + 'suscribir',
                type: 'post',
                dataType: 'json',
                data: $("#subcripcion").serialize(),
                success: function(data) {
                    if (data.tipe == 'nuevo') {
                         swal("Usuario registrado", data.mensaje, "success");
                    }else if(data.tipe == 'existe') {
                         swal("Ya existe el usuario", data.mensaje, "warning");
                    }else{
                         swal("Error", data.mensaje, "danger");
                    }
                }
        });
         $('#subcripcion')[0].reset();
    });

});
