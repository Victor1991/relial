$("#form_banner").submit(function(e) {
     var post_url = $(this).attr("action"); //get form action url
     var request_method = $(this).attr("method"); //get form GET/POST method
     var form_data = $(this).serialize(); //Encode form elements for submission
     var data = new FormData(this);
     e.preventDefault(); // avoid to execute the actual submit of the form.

     $.ajax({
          url : post_url,
          type: request_method,
          data : data,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(data){

               $('#modal_form_banner').modal('hide');

               swal({title: data.mensaje, text: '', type: data.tipo},
                  function(){
                      location.reload();
                  }
               );
               // alert(data); // show response from the php script.
          }
     });

});


$('#modal_form_banner').on('hidden.bs.modal', function () {
     $('#img_banner').attr('src', '');
     $('#banner_id').val('');
     $('#m_titulo').val('');
     $('#m_correo').val('');
     $('#m_telefono').val('');
     $('#m_descripcion').text('');
     $('#m_link').val('');
     $('#m_nueva_ventana').prop('checked', false);
     $('#m_estatus').prop('checked', false);

});
