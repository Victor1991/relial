  <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                        <h2>Artículos</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>


<section class="commonSection porfolio">
            <div class="container">
                
                <div class="row">
            <div class="row">
                <div class="col-sm-9">
                    <?php foreach($posts as $post): ?>
                    <div class="row m-b-20">
                        <div class="col-sm-4">
                            <img class="img-responsive" src="<?php echo base_url('uploads/posts/' . $post->imagen); ?>" alt="">
                        </div>
                        <div class="col-sm-8">
                            <div class="news-v3">
                                <h2><a href="<?php echo site_url('articulos/'.$post->slug); ?>"><?php echo $post->titulo; ?></a></h2>
                                <ul class="list-inline posted-info m-b-15">
                                    <li><?php echo $post->autor_nombre; ?></li>
                                    <li><a href="<?php echo base_url('articulos/categoria/'.$post->categoria_slug); ?>"><?php echo $post->categoria_nombre; ?></a></li>
                                    <li><?php echo mysql_to_date($post->fecha); ?></li>
                                </ul>
                                <p><?php echo $post->resumen; ?></p>
                                
                                
                                 <a class="common_btn red_bg" href="<?php echo site_url('articulos/'.$post->slug); ?>"><span>Leer más</span></a>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="clearfix m-b-20"><hr></div>
                    <?php endforeach; ?>
                    <?php echo $pagination['links'] ?>
                </div>
                <div class="col-sm-3">
                    <?php echo $sidebar; ?>
                </div>
            </div>
        </div>
    </div>
</section>