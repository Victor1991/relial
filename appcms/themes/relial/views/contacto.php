

<section class="pageBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner_content text-center">
                    <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                    <h2>Contacto</h2>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="commonSection ContactPage">
            <div class="container">
                
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-sm-12 col-md-10 col-md-offset-1">
                        <form action="<?php echo base_url('sitio/envia_contacto'); ?>" method="post" class="contactFrom" id="form-contacto">
                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                    <input class="input-form required" type="text" name="nombre" id="nombre" placeholder="Nombre">
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <input class="input-form required" type="text" name="asunto" id="asunto" placeholder="Asunto">
                                </div>
                                <div class="col-lg-12 col-sm-12">
                                    <input class="input-form required" type="email" name="email" id="email" placeholder="Email ">
                                </div>
                                
                                <div class="col-lg-12 col-sm-12">
                                    <textarea class="input-form required" nname="comentario" id="comentario" placeholder="Comentarios"></textarea>
                                </div>
                            </div>
                            <button class="common_btn red_bg" type="button" id="con_submit"><span>Enviar</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

<!--
<div class="content-section m-b-30">
    <div class="container">
        <div class="padding">
            <div class="row m-b-30">
                <div class="col-sm-9">
                    <div class="alert-message"></div>
                    <form method="post" action="<?php echo base_url('sitio/envia_contacto'); ?>" id="form-contacto" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nombre y Apellidos:</label>
                            <div class="col-sm-9">
                                <input type="text" name="nombre" id="nombre" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Correo Electrónico:</label>
                            <div class="col-sm-9">
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Asunto:</label>
                            <div class="col-sm-9">
                                <input type="text" name="asunto" id="asunto" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Comentarios:</label>
                            <div class="col-sm-9">
                                <textarea rows="6" name="comentario" id="comentario" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="button" class="btn btn-primary btn-send">Enviar</button>
                                <div class="loading hide">
                                    Enviando..&nbsp;<img src="<?php echo theme_assets('images/loading_spinner.gif'); ?>" style="width: 42px;">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3">
                    <?= $informacion->texto ?>
                </div>
            </div>
        </div>
    </div>
</div>
-->