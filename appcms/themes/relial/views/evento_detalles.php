<section class="pageBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner_content text-center">
                    <h4><a href="/">Inicio</a> - <a href="/eventos">Eventos</a> - <?= $evento->titulo ?> </h4>
                    <h2>Evento - <?= $evento->titulo ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>


  <!-- Blog Detail Section -->
        <!--<section class="commonSection blogDetails">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-8">
                        <div class="single_blog">
                            <div class="blog_thumb">
                               <img src="<?=base_url('uploads/eventos/'.$evento->imagen)?>" class="img-responsive img-thumbnail" alt="">
                            </div>
                            <div class="blog_headings">
                                <span class="blog_date"><?= $pais->nombre ?></span>
                                <h2><?= $evento->titulo ?></h2>
                                <p class="blog_metas">
                                <h4>
                                    <i class="fa fa-calendar"></i>
                                    <?php if ($evento->fecha_inicio != $evento->fecha_fin): ?>
                                        <?php echo strftime("%d de %B del %Y", strtotime($evento->fecha_inicio)) . ' al ' . strftime("%d de %B del %Y", strtotime($evento->fecha_fin)); ?>
                                    <?php else: ?>
                                    <?php endif; ?>
                                </h4>
                                <?php if ($evento->dia_completo == 0): ?>
                                    <h4>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <?php echo strftime("%H:%M", strtotime($evento->hora_inicio)) . ' a ' . strftime("%H:%M %p", strtotime($evento->hora_fin)); ?>
                                    </h4>
                                <?php endif; ?>
                                <h4>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <?php if ($pais->nombre): ?>
                                        <strong> Pais :</strong> <?= $pais->nombre ?>,
                                    <?php endif; ?>
                                    <?php if ($estado->nombre): ?>
                                        <strong> Estado o Provincias :</strong> <?= $estado->nombre ?>,
                                    <?php endif; ?>
                                    <?php if ($evento->direccion): ?>
                                        <strong> Dirección :</strong> <?= $evento->direccion ?>
                                    <?php endif; ?>
                                </h4>
                                <?php if ($evento->costo != '' && $evento->costo != '0.00'): ?>
                                    <h4>
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                        $ <?= $evento->costo ?> MXN
                                    </h4>
                                <?php endif; ?>
                                </p>
                            </div>
                            <div class="blog_details">
                                <p style="text-align: justify;">
                                    <?=$evento->descripcion?>
                                </p>
                            </div>
                   
                            <hr>  
                            <div class="row">
                               <input type="hidden" id="long" value="<?=$evento->long?>">
                                        <input type="hidden" id="lat" value="<?=$evento->lat?>">
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <div id="map1" style="width: 100%; height: 500px; border-radius: 5px;"></div>
                                             </div>
                                        </div>
                            </div>
                            <div class="comment_area">
                                
                                     <?php if ($evento->programa != ''): ?>
                                  

                                   <div class="row">
                                        <div class="panel-body">
                                             <div class="col-md-6">
                                                  <h2 style="color: #fff; margin-top: 10px;">Programa</h2>
                                             </div>
                                             <br>
                                             <br>
                                             <div class="col-md-12">
                                             <object data="<?php echo base_url('uploads/programa/' . $evento->programa); ?>" type="application/pdf" style="width:100%; height:400px; background-color: #505050;">
                                                  <embed src="<?php echo base_url('uploads/programa/' . $evento->programa); ?>" type="application/pdf" style="width:100%; height:400px; background-color: #505050;">
                                                       <p>Este navegador no admite archivos PDF. Descargue el PDF para verlo: <a href="<?php echo base_url('uploads/programa/' . $evento->programa); ?>">Descargar PDF</a>.</p>
                                                  </embed>
                                             </object>

                                        </div>
                                   </div>
                              </div>
                              <?php endif; ?>

                            </div>
                            <div class="commentForm">
                                <h3>Leave a Comment</h3>
                                <form action="#" method="post">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input required="" class="input-form" type="text" name="name" placeholder="Your Name">
                                        </div>
                                        <div class="col-lg-6">
                                            <input required="" class="input-form" type="email" name="email" placeholder="Email Address">
                                        </div>
                                        <div class="col-lg-12">
                                            <textarea required="" class="input-form" name="comment" placeholder="Write Message"></textarea>
                                        </div>
                                    </div>
                                    <a class="common_btn red_bg" href="#"><span>Send Message</span></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 sidebar">
              
                        <aside class="widget recent_posts">
                            <h3 class="widget_title">Latest Posts</h3>
                            <div class="meipaly_post_widget">
                                <div class="mpw_item">
                                    <img src="images/blog/11.jpg" alt="">
                                    <a href="#">basic rules of running web agency</a>
                                </div>
                                <div class="mpw_item">
                                    <img src="images/blog/11.jpg" alt="">
                                    <a href="#">Introducing latest mopaly features</a>
                                </div>
                                <div class="mpw_item">
                                    <img src="images/blog/13.jpg" alt="">
                                    <a href="#">become the best sale marketer</a>
                                </div>
                            </div>
                        </aside>
               
                    </div>
                </div>
            </div>
        </section>-->
        <!-- Blog Detail Section -->




<div class="content-section m-b-30" id="about-us">
     <div class="container">
          <div class="padding">
               <div class="col-md-12 m-b-30">
                   

                    <div class="clearfix"></div>

                    <div class="row">
                         <div class="col-lg-12 col-md-12 col-sm-12 text-center ">

                              <img src="<?=base_url('uploads/eventos/'.$evento->imagen)?>" class="img-responsive img-thumbnail" alt="">

                         </div>
                         <div class="col-md-6 col-md-offset-3 titulo-post text-center">
                              <h2 style="color: #fff; margin-top: 10px;"><?=$evento->titulo?></h2>
                         </div>
                         <div class="clearfix">
                         </div>
                         <br>
                         <div class="col-lg-12">
                              <div class="panel panel-default">
                                   <div class="panel-body">
                                        <h4>
                                             <i class="fa fa-calendar"></i>
                                             <?php if($evento->fecha_inicio != $evento->fecha_fin):  ?>
                                                  <?php echo strftime("%d de %B del %Y" ,strtotime($evento->fecha_inicio)).' al '.strftime("%d de %B del %Y" ,strtotime($evento->fecha_fin)); ?>
                                             <?php else: ?>
                                             <?php endif; ?>
                                        </h4>
                                        <?php if ($evento->dia_completo == 0): ?>
                                             <h4>
                                                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                  <?php echo strftime("%H:%M" ,strtotime($evento->hora_inicio)).' a '.strftime("%H:%M %p" ,strtotime($evento->hora_fin)); ?>
                                             </h4>
                                        <?php endif; ?>
                                        <h4>
                                             <i class="fa fa-map-marker" aria-hidden="true"></i>
                                             <?php if($pais->nombre): ?>
                                             <strong> País :</strong> <?=$pais->nombre?><br>
                                             <?php endif; ?>
                                             <?php if($estado->nombre): ?>
                                                  <strong> Estado o provincia :</strong> <?=$estado->nombre?><br>
                                             <?php endif; ?>
                                             <?php if($evento->direccion): ?>
                                                  <strong> Dirección :</strong> <?=$evento->direccion?><br>
                                             <?php endif; ?>
                                        </h4>
                                        <?php if ($evento->costo != '' && $evento->costo != '0.00'): ?>
                                             <h4>
                                                  <i class="fa fa-money" aria-hidden="true"></i>
                                                  $ <?=$evento->costo?> MXN<br>
                                             </h4>
                                        <?php endif; ?>

                                        <hr>
                                        <p style="text-align: justify; font-size: 15px;"><?=$evento->descripcion?></p>
                                        <hr>
                                        <input type="hidden" id="long" value="<?=$evento->long?>">
                                        <input type="hidden" id="lat" value="<?=$evento->lat?>">
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <div id="map1" style="width: 100%; height: 500px; border-radius: 5px;"></div>
                                             </div>
                                        </div>

                                   </div>

                              </div>

                              <!-- Programa -->
                              <?php if ($evento->programa != ''): ?>
                                   <div class="clearfix m-b-30">
                                   </div>

                                   <div class="panel panel-default">
                                        <div class="panel-body">
                                             <div class="col-md-6  titulo-post text-center" style="margin-top: -35px;">
                                                  <h2 style="color: #fff; margin-top: 10px;">Programa</h2>
                                             </div>
                                             <br>
                                             <br>
                                             <div class="col-md-12">
                                             <object data="<?php echo base_url('uploads/programa/' . $evento->programa); ?>" type="application/pdf" style="width:100%; height:400px; background-color: #505050;">
                                                  <embed src="<?php echo base_url('uploads/programa/' . $evento->programa); ?>" type="application/pdf" style="width:100%; height:400px; background-color: #505050;">
                                                       <p>Este navegador no admite archivos PDF. Descargue el PDF para verlo: <a href="<?php echo base_url('uploads/programa/' . $evento->programa); ?>">Descargar PDF</a>.</p>
                                                  </embed>
                                             </object>

                                        </div>
                                   </div>
                              </div>
                              <?php endif; ?>

                              <div class="clearfix m-b-30">
                              </div>

                              <div class="panel panel-default ">

                                   <div class="panel-body">
                                        <div class="col-md-6  titulo-post text-center" style="margin-top: -35px;">
                                             <h2 style="color: #fff; margin-top: 10px;">Registrate</h2>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="col-md-12 m-b-30">
                                             <form id="form_registro" action="<?=base_url('eventos/insert_registro')?>" method="post" enctype="multipart/form-data">
                                                  <input type="hidden" name="evento_id" value="<?=$evento->id?>">
                                                  <fieldset class="form-group col-md-6">
                                                       <label for="nombre">Nombre(s)</label>
                                                       <input type="text" name="nombre" class="form-control"  id="nombre">
                                                  </fieldset>
                                                  <fieldset class="form-group col-md-6">
                                                       <label for="apellidos">Apellidos(s)</label>
                                                       <input type="text" name="apellidos" class="form-control" required id="apellidos">
                                                  </fieldset>
                                                  <fieldset class="form-group col-md-6">
                                                       <label for="organizacion">Organización</label>
                                                       <input type="text" name="organizacion" class="form-control" required id="organizacion" >
                                                  </fieldset>

                                                  <fieldset class="form-group col-md-6">
                                                       <label for="pais">País</label>
                                                       <input type="text" name="pais" class="form-control" required id="pais" >
                                                  </fieldset>
                                                  <fieldset class="form-group col-md-6">
                                                       <label for="correo">Correo</label>
                                                       <input type="email" name="correo" class="form-control" required id="correo" >
                                                  </fieldset>

                                                  <fieldset class="form-group col-md-6">
                                                       <label for="telefono">Teléfono</label>
                                                       <input type="text" name="telefono" class="form-control" required id="telefono" >
                                                  </fieldset>

                                                  <fieldset class="form-group col-md-12">
                                                       <label for="direccion">Dirección organización / casa </label>
                                                       <textarea name="direccion" class="form-control" rows="3" cols="80"></textarea>
                                                  </fieldset>

                                                  <fieldset class="form-group col-md-12">
                                                       <label for="direccion"> ¿ Eres ponente o expectador ? </label>
                                                       <div class="col-md-12" style="border: 1px solid #ccc; border-radius: 5px; padding: 15px;">
                                                            <div class="row">
                                                                 <span class="checkbox-inline">
                                                                     <input type="radio" name="tipo_asistente" value="1">
                                                                     Ponente
                                                                </span>
                                                                <span class="checkbox-inline">
                                                                     <input type="radio" name="tipo_asistente" value="1">
                                                                     Expectador
                                                                </span>
                                                            </div>
                                                       </div>

                                                  </fieldset>
                                                  <br>
                                                  <br>
                                                  <fieldset class="form-group col-md-12">
                                                       <label for="viene_acompanado"> ¿ Vienes acompañado ? </label><br>
                                                       <div class="col-md-12" style="border: 1px solid #ccc; border-radius: 5px; padding: 15px;">
                                                            <div class="row">
                                                                 <span class="checkbox-inline">
                                                                     <input type="radio" name="viene_acompanado" value="1">
                                                                     Si
                                                                </span>
                                                                <span class="checkbox-inline">
                                                                     <input type="radio" name="viene_acompanado" value="2">
                                                                     No
                                                                </span>
                                                            </div>
                                                            <br>
                                                            <p style="text-align:justify;">*<strong>IMPORTANTE</strong>: Por favor tome en cuenta que si no forma parte de ninguna organización el costo tanto del hotel como el costo para participar en las actividades del Congreso deberán ser cubierto por usted. Tarifa noche Hotel: <?=$evento->costo?> USD. Tarifa actividades del Congreso 200 USD.</p>
                                                       </div>
                                                  </fieldset>

                                                  <fieldset class="form-group col-md-12" id="div_acompanante" style="display:none;">
                                                       <label for="acompañante">Datos del acompañante</label>
                                                       <textarea name="acompanante" class="form-control" rows="3" cols="80" placeholder="Espos@ : Juan Pérez
Hij@ : Juanita Pérez"></textarea>
                                                  </fieldset>


                                                  <fieldset class="form-group col-md-12">
                                                       <label for="direccion"> ¿ De donde nos visitas ? </label><br>
                                                       <div class="col-md-12" style="border: 1px solid #ccc; border-radius: 5px; padding: 15px;">
                                                            <div class="row">
                                                                 <span class="checkbox-inline">
                                                                     <input type="radio" name="loca_foraneo" value="1">
                                                                     Soy local
                                                                </span>
                                                                <span class="checkbox-inline">
                                                                     <input type="radio" name="loca_foraneo" value="2">
                                                                     Soy foráneo
                                                                </span>
                                                            </div>
                                                       </div>
                                                  </fieldset>

                                                  <fieldset class="form-group col-md-12" id="div_boleto" style="display:none;">
                                                       <label for="boleto">Adjuntar boleto de avión</label>
                                                       <input type="file" name="boleto" class="form-control"  id="boleto" >
                                                       <p><strong>En caso de no contar con su boleto, podrá registarse pero NO podrá hacer check in (Confirmar su asistencia) al evento hasta tener todos los datos solicitados. Le recomendamos hacer su registro cuando tenga su itinerario definido</strong></p>
                                                  </fieldset>
               <hr>

                                                  <div class="col-md-12 form-group">

                                                       <button type="submit" style="margin-top: 25px; width: 100%;" class="btn btn-primary btn-clock">Enviar</button>

                                                  </div>
                                             </form>


                                        </div>
                                   </div>

                              </div>
                         </div>

                    </div>
               </div>
               <?php if(count($hoteles) > 0):  ?>
                    <div class="col-md-12">
                         <div class="panel panel-default">
                              <div class="panel-body">
                                   <div class="col-md-6  titulo-post text-center" style="margin-top: -35px;">
                                        <h2 style="color: #fff; margin-top: 10px;">Hoteles</h2>
                                   </div>
                                   <br>
                                   <br>
                                   <div class="col-md-12">
                                        <div class="row">
                                             <?php foreach ($hoteles as $key => $hotel): ?>
                                                  <div class="col-sm-4">
                                                       <div class="col-item">
                                                            <div class="photo">
                                                                 <?php if ($hotel->logo): ?>
                                                                      <img src="<?=base_url('uploads/logo_hotel/'.$hotel->logo)?>" class="img-responsive" style="height:100%;" alt="a">
                                                                 <?php else: ?>
                                                                      <img src="https://i.pinimg.com/originals/cb/a4/fd/cba4fdff18fe7e587119fc812a59060e.png" class="img-responsive" style="height:100%;" alt="a">
                                                                 <?php endif; ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="info">
                                                                 <div class="row">
                                                                      <div class="price col-md-12">
                                                                           <h5><i class="fa fa-building" aria-hidden="true"></i> <?=$hotel->nombre?></h5>
                                                                           <h5><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$hotel->ubicacion?></h5>
                                                                      </div>
                                                                 </div>
                                                                 <div class="separator clear-left">
                                                                      <p class="btn-add" onclick="initMap2(<?=$hotel->lat?>,<?=$hotel->long?>)">
                                                                           <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                           <a  class="hidden-sm">Ver mapa</a>
                                                                      </p>
                                                                 </div>
                                                                 <div class="clearfix">
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </div>

                                             <?php endforeach; ?>

                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               <?php endif; ?>
          </div>
     </div>
</div>



<div class="modal fade" id="mapa" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" >
     <div class="modal-dialog modal-lg">
          <div class="modal-content">

               <div class="modal-body" style="padding: 0px">
                    <button type="button" style="position: absolute; background-color: red; z-index: 88; padding: 10px; opacity: 0.7; font-size: 25px;" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div id="map2" style="width: 100%; height: 500px; border-radius: 5px;"></div>
               </div>

          </div>
     </div>
</div>
