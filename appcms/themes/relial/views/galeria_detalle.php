  <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                        <h2>Galerías</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>








<section class="commonSection relatedPortfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                       
                        <h2 class="sec_title"><?php echo $galeria->titulo; ?></h2>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="related_slider">
                            
                            <?php foreach ($galeria->imagenes as $img): ?>
                            <div class="singlefolio">
                                <img src="<?php echo base_url('uploads/galerias/'.$img->archivo); ?>" alt=""/>
                                <div class="folioHover">
                                    <div class="overlay">
                                    <div class="inner-overlay">
                                        <div class="inner-overlay-content with-icons">
                                            <a class="fancybox-pop" title="<?php echo $img->descripcion; ?>" alt="<?php echo $img->descripcion; ?>" href="<?php echo base_url('uploads/galerias/'.$img->archivo); ?>"><i class="fa fa-search"></i> Zoom</a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                             <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

                
          <!--
<div class="portfolio-section port-col">
    <div class="container">
        <div class="padding">
            <div class="row m-b-30">
                <div class="col-sm-9">
                    <h1 class="main-title"><i class="fa fa-caret-right"></i>&nbsp;<?php echo $galeria->titulo; ?></h1>
                </div>
                <div class="col-sm-3">
                    <div class="addthis_sharing_toolbox"></div>
                </div>
            </div>
            <div class="row">
                <div class="isotopeContainer">
                    <?php foreach ($galeria->imagenes as $img): ?>
                    <div class="col-sm-3 isotopeSelector art">
                        <article class="">
                            <figure>
                                <img src="<?php echo base_url('uploads/galerias/'.$img->archivo); ?>" alt="">
                                <div class="overlay-background">
                                    <div class="inner"></div>
                                </div>
                                <div class="overlay">
                                    <div class="inner-overlay">
                                        <div class="inner-overlay-content with-icons">
                                            <a class="fancybox-pop" title="<?php echo $img->descripcion; ?>" alt="<?php echo $img->descripcion; ?>" href="<?php echo base_url('uploads/galerias/'.$img->archivo); ?>"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="article-title"><a href="#"><?php echo $img->descripcion; ?></a></div>
                        </article>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
          -->