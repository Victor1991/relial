<div class="headline-v2">
    <h2><i class="fa fa-caret-right c-red"></i>&nbsp;Categorías</h2>
</div>
<ul class="list-unstyled blog-latest-posts m-b-20">
    <?php foreach ($categorias as $categoria): ?>
    <li>
        <h3><a href="<?php echo site_url('articulos/categoria/'.$categoria->slug); ?>" ><?php echo $categoria->nombre; ?></a></h3>
    </li>
    <?php endforeach; ?>
</ul>

<div class="headline-v2">
    <h2><i class="fa fa-caret-right c-red"></i>&nbsp;Últimos artículos</h2>
</div>
<ul class="list-unstyled blog-latest-posts m-b-20">
    <?php foreach ($ultimos_posts as $post): ?>
    <li>
        <h3><a href="<?php echo site_url('articulos/'.$post->slug); ?>" style="color: #686868"><?php echo $post->titulo ?></a></h3>
        <small><?php echo mysql_to_date($post->fecha); ?> / <a href="#"><?php echo $post->categoria_nombre; ?></a> </small>
    </li>
    <?php endforeach; ?>
</ul>