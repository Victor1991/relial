  <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                        <h2>Videos</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>


<section class="commonSection porfolio" >
            <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="blog m-b-30">
                        <h2><?php echo $video->titulo; ?></h2>
                        <ul class="list-inline posted-info m-b-15">
                            <li><i class="fa fa-edit"></i><?php echo $video->autor_nombre; ?></li>
                            <li><i class="fa fa-calendar"></i><?php echo mysql_to_date($video->fecha); ?></li>
                        </ul>
<!--                        <div class="blog-img">
                            <img class="img-responsive" src="<?php echo base_url('uploads/posts/' . $post->imagen); ?>" alt="">
                        </div>-->
                        <?php echo $video->contenido; ?>
                    </div>
                </div>
            </div>            
        
    </div>
</section>