
<!--


<footer>
            <div id="footer-bottom">
                <div class="container">
                    <ul class="nav-footer">
                        <li><a href="<?php echo site_url('sitio/aviso-legal-privacidad'); ?>">Aviso Legal</a></li>
                        <li><a href="<?php echo site_url('sitio/aviso-legal-privacidad'); ?>">Privacidad</a></li>
                        <li><a href="<?php echo site_url('sitio/fundacion-friedrich-naumann'); ?>"> Fundación Friedrich Naumann</a></li>









                    </ul>
                </div>
            </div>
        </footer>

          <script>
               var base_url = '<?= site_url() ?>';
          </script>


        <script type="text/javascript" src="<?php echo theme_assets('js/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/gmaps.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/jquery.parallax.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/jquery.magnific-popup.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/vegas.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/owl.carousel.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/jquery.nav.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/coundown-timer.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo theme_assets('js/main.js'); ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDp6N78xk7nRnBS0rQ7HW3S73_AMNPB0ic"></script>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js">

     </script>

     <script>
     function ver_modal($json) {
          var obj = JSON.parse($json);
          $('#foto').attr('src', '<?= base_url('uploads/banners/') ?>/'+obj.imagen);
          $('#mod_titulo').html(obj.titulo);
          $('#mod_descripcion').html(obj.descripcion);
          $('#mod_pagina').html('');
          $('#info_mesa').modal('show');
     }
     </script>


<?php if (isset($this->assets['js'])): ?>
    <?php foreach ($this->assets['js'] as $js) : ?>
                <script type="text/javascript" src="<?php echo theme_assets(trim($js, '/')); ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99797303-1', 'auto');
  ga('send', 'pageview');

</script>




<script type="text/javascript">
jQuery(document).ready(function($) {
     $(window).bind("load resize", function(){
          setTimeout(function() {
               var container_width = $('#fb-container').width();
               $('#fb-container').html('<div class="fb-page" ' +
               'data-href="https://www.facebook.com/relial.red/"' +
               ' data-width="' + container_width + '" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://www.facebook.com/IniciativaAutoMat"><a href="https://www.facebook.com/relial.red/">Relial</a></blockquote></div></div>');
               FB.XFBML.parse( );
          }, 100);
     });
});
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5065cb510ad808db"></script>


</body>
</html>
-->







<!-- Footer Section -->
<footer class="footer_1">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6 col-md-5">
                <aside class="widget aboutwidget">
                    <a href="#"><img src="<?php echo theme_assets('images/foo_logo.png'); ?>" alt=""></a>
                    <p>

                        <?= $informacion->texto ?>

                    </p>
                </aside>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4">
                <aside class="widget social_widget">
                    <h3 class="widget_title">Información</h3>


                     <ul >
                        <li><a href="<?php echo site_url('sitio/aviso-legal-privacidad'); ?>">Aviso Legal</a></li>
                        <li><a href="<?php echo site_url('sitio/aviso-legal-privacidad'); ?>">Privacidad</a></li>
                        <li><a href="<?php echo site_url('sitio/fundacion-friedrich-naumann'); ?>"> Fundación Friedrich Naumann</a></li>


                    </ul>



                </aside>
            </div>
            <div class="col-lg-3 col-sm-2 col-md-3">
                <aside class="widget social_widget">
                    <h3 class="widget_title">social</h3>
                    <ul>
                         <li><a href="https://www.instagram.com/red.relial/" target="_blank"><i class="fa fa-instagram"></i>Instagram</a></li>
                        <li><a href="https://twitter.com/relialred?lang=es" target="_blank"><i class="fa fa-twitter"></i>Twitter</a></li>
                        <li><a href="https://www.facebook.com/relial.red/?ref=br_rs" target="_blank"><i class="fa fa-facebook-square"></i>Facebook</a></li>
                        <li><a href="https://www.youtube.com/channel/UCHzzdDcOdgLhAZfwfGBQQxw" target="_blank"><i class="fa fa-youtube-play"></i>Youtube</a></li>
                    </ul>
                </aside>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <div class="copyright">
                    © RELIAL 2020
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section -->

<!-- Bact To To -->
<a id="backToTop" href="#" class=""><i class="fa fa-angle-double-up"></i></a>
<!-- Bact To To -->

<script>
     var base_url = '<?=site_url()?>';
</script>


<!-- Include All JS -->
<script src="<?php echo theme_assets('2020/'); ?>/js/jquery.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

<script src="<?php echo theme_assets('2020/'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/modernizr.custom.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/gmaps.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyDp6N78xk7nRnBS0rQ7HW3S73_AMNPB0ic"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/jquery.themepunch.tools.min.js"></script>
<!-- Rev slider Add on Start -->
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/extensions/revolution.extension.video.min.js"></script>
<!-- Rev slider Add on End -->
<script src="<?php echo theme_assets('2020/'); ?>/js/dlmenu.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/jquery.magnific-popup.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/mixer.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/owl.carousel.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/slick.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/jquery.appear.js"></script>
<script src="<?php echo theme_assets('2020/'); ?>/js/theme.js"></script>




<script type="text/javascript" src="<?php echo theme_assets('assets/plugins/youtube-video-player/js/youtube-video-player.jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_assets('assets/plugins/youtube-video-player/packages/perfect-scrollbar/jquery.mousewheel.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_assets('assets/plugins/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.js'); ?>"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.min.js"></script>

<script>
     function ver_modal($json) {
          var obj = JSON.parse($json);
          $('#foto').attr('src', '<?=base_url('uploads/banners/')?>/'+obj.imagen);
          $('#mod_titulo').html(obj.titulo);
          $('#mod_descripcion').html(obj.descripcion);
          $('#mod_pagina').html('');
          $('#info_mesa').modal('show');
     }
     </script>

        <!-- custom js -->
        <?php if (isset($this->assets['js'])): ?>
            <?php foreach ($this->assets['js'] as $js) : ?>
        <script type="text/javascript" src="<?php echo theme_assets(trim($js, '/')); ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>



<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-99797303-1', 'auto');
    ga('send', 'pageview');

</script>

<script>

$(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var cId = $('#calendar'); //Change the name if you want. I'm also using thsi add button for more actions

    //Generate the Calendar
    cId.fullCalendar({
         eventClick: function(event) {
              if (event.url) {
                   ver_modal(event.url)
                   return false;
              }
         },
         header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        buttonText: {
            today: 'hoy',
            month: 'mes',
            week: 'sem.',
            day: 'día'
        },
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Deciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        editable: true,
        events: '/eventos_caledario'
    });

});


function ver_modal(url) {
     $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
     })
     .done(function(data) {
          $('#tit_mod').html(data.titulo);
          $('#img_mod').attr('src','https://relial.org/uploads/eventos/'+data.imagen);
          $('#desc_mod').html(data.descripcion);
          $('#exampleModal').modal('show');
     })
     .fail(function(error) {
          console.log(error);
     });
}

</script>



<!-- Include All JS -->
</body>
</html>
