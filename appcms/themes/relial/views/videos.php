  <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                        <h2>Videos</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>




                        
<section class="commonSection porfolio" >
            <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <?php foreach($videos as $video): ?>
                    <div class="row m-b-20">
                        <div class="col-sm-12">
                            <div class="news-v3">
                                <h2><a href="<?php echo site_url('videos/detalle/'.$video->slug); ?>"><?php echo $video->titulo; ?></a></h2>
                                <ul class="list-inline videoed-info m-b-15">
                                    <li><?php echo $video->autor_nombre; ?></li>
                                    <li><a href="<?php echo base_url('articulos/videos'); ?>"><?php echo $video->categoria_nombre; ?></a></li>
                                    <li><?php echo mysql_to_date($video->fecha); ?></li>
                                </ul>
                                <div class="content-video">
                                    <?php echo $video->contenido; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix m-b-20"><hr></div>
                    <?php endforeach; ?>
                    <?php echo $pagination['links'] ?>
                </div>
            </div>
        </div>
    
</section>