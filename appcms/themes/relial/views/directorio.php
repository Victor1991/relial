<section class="pageBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner_content text-center">
                    <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                    <h2>DIRECTORIO</h2>
                </div>
            </div>
        </div>
    </div>
</section>





<div style="display:none;">
     <?php _dump($aliados);?>
</div>


<section class="commonSection porfolioPage">
    <div class="container">
        <div class="row">





             <?php foreach ($aliados as $key => $aliado): ?>
                    <div class="row">
                         <div class="col-md-12">
                              <h2><strong><?=$key?></strong> </h2>
                              <hr>
                              <?php foreach ($aliado as $key => $info_aliado): ?>
                                  <?php if ($info_aliado['estatus'] == 1): ?>



                                       <div class="col-lg-4 col-sm-6 col-md-4">
                                            <div class="singleClient" style="border: 1px solid #e7e7e7;">
                                                 <a href="<?php echo $info_aliado['link']?>" <?php echo $info_aliado['nueva_ventana'] ? 'target="_blank"' : ''; ?>>
                                                      <img src="<?= base_url('/uploads/banners/') . '/' . $info_aliado['imagen'] ?>" alt="" style="opacity: 1; max-width: 180px;" >

                                                 </a>


                                            </div>
                                       </div>


                                  <?php endif; ?>
                             <?php endforeach; ?>
                         </div>
                    </div>



             <?php endforeach; ?>




        </div>

    </div>
</section>


<!--
<section class="commonSection ready_2">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-sm-8 col-md-9">
                <h2 class="sec_title white">Forma parte de nuestra red</h2>
            </div>
            <div class="col-lg-3 col-sm-4 col-md-3 text-right">
                <a class="common_btn red_bg" onclick="$('#modal_form_banner').modal('show');" ><span>Regístrate</span></a>
            </div>
        </div>
    </div>
</section>
-->


<!--




<div class="content-section m-b-30">
     <div class="container">
          <div class="menu">
             <div class="container-fluid">
                    <div>
                         <ul class="nav navbar-nav navbar-right">
                              <li><a onclick="$('#modal_form_banner').modal('show');" ><span class="fa fa-plus"></span> Registrate</a></li>
                         </ul>
                    </div>
               </div>
          </div>

          <div class="padding">


               <div class="row m-b-30">
<?php foreach ($aliados as $key => $aliado): ?>
    <?php if ($aliado->estatus == 1): ?>
                                      <div class="col-md-6" style="min-height: 150px;">
                                           <div class="well well-sm" style="background: #efefef; box-shadow: 2px -2px 6px 0px; border: 0px;">
                                                <div class="row">
                                                     <div class="col-xs-3 col-md-3 text-center">
                                                          <img src="<?= base_url('/uploads/banners/') . '/' . $aliado->imagen ?>" alt="bootsnipp"
                                                          class="img-rounded img-responsive" style=" width: 100%; height: 70px;"/>
                                                     </div>
                                                     <div class="col-xs-9 col-md-9 section-box">
                                                          <h2>
                                                               <i class="fa fa-building" aria-hidden="true"></i>
        <?= $aliado->titulo ?>
                                                          </h2>
        <?php if ($aliado->descripcion != ''): ?>
                                                                   <p class="list-group-item-text">
                                                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
            <?= $aliado->descripcion ?>
                                                                   </p>
        <?php endif; ?>
        <?php if ($aliado->mail != ''): ?>
                                                                   <p class="list-group-item-text">
                                                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <?= $aliado->mail ?>
                                                                   </p>
        <?php endif; ?>
        <?php if ($aliado->telefono != ''): ?>
                                                                   <p class="list-group-item-text">
                                                                        <i class="fa fa-phone" aria-hidden="true"></i>
            <?= $aliado->telefono ?>
                                                                   </p>
        <?php endif; ?>


                                                     </div>
                                                </div>
                                           </div>
                                      </div>
    <?php endif; ?>
<?php endforeach; ?>


               </div>
          </div>
     </div>
</div>
</div>



-->




<div class="modal fade" id="modal_form_banner" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4> Regístrate</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('insert') ?>" id="form_banner" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="banner_id" id="banner_id" value="">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Título</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="titulo" id="m_titulo" required placeholder="Titulo" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Correo</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="correo" id="m_correo" placeholder="Correo" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Teléfono</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="telefono" id="m_telefono" placeholder="Teléfono" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="link" id="m_link" placeholder="http://ejemplo.com" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="nueva_ventana" value="1"
                                           id="m_nueva_ventana">
                                    <i class="input-helper"></i>
                                    Abrir link en una nueva ventana
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Descripción</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <textarea rows="2" data-max-len="" name="descripcion" id="m_descripcion" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Imagen <span class="help-block"></span></label>
                        <div class="col-sm-10">
                            <input type="file" name="imagen" id="m_imagen" data-width="" data-height="">
                            <img src="" class="img-responsive" style="max-height:250px;" id="img_banner" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <div class="form-actions" style="float: right;">
                                <button type="button" class="common_btn red_bg" data-dismiss="modal"><span>Cancelar</span></button>

                                <button type="submit" class="common_btn red_bg " id="btn-guardar"><span>Envíar información</span></button>

                            </div>


                            <div class="form-loading text-center" style="display:none">
                                <img src="/assets/admin/img/circular_load.GIF">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
