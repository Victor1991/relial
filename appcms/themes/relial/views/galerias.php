  <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                        <h2>Galerías</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

                
          
                    
                    
                    
                    
                        
                        
                        
                        
                        
                   <?php foreach ($galerias as $key => $year): ?>      
                        
<section class="commonSection porfolio" style="padding-bottom: 0px; padding: 10px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 class="sub_title">Galerias</h4>
                        <h2 class="sec_title"><?= $key ?></h2>
                        
                    </div>
                </div>
                
                
                <div class="row">
                     <?php foreach ($year as $galeria): ?>
                    <div class="col-lg-4 col-sm-6 col-md-4">
                        <div class="singlefolio">
                            <img src="<?php echo base_url('uploads/galerias/' . $galeria->archivo); ?>" alt=""/>
                            <div class="folioHover">
                             
                                <h4 ><a href="<?php echo base_url('galerias/fotos/' . $galeria->id); ?>" style="line-height: 1;"><?php echo $galeria->titulo ?></a></h4>
                            </div>
                        </div>
                    </div>
                      <?php endforeach; ?>
                    
                </div>
            </div>
        </section>
                        
                          <?php endforeach; ?>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
    
                
                       
   