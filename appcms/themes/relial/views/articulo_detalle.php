

  <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                        <h2>Artículos</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>



<section class="commonSection porfolio">
            <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="blog m-b-30">
                        <h2><a href="<?php echo site_url('articulos/'.$post->slug); ?>"><?php echo $post->titulo; ?></a></h2>
                        <ul class="list-inline posted-info m-b-15">
                            <li><i class="fa fa-edit"></i><?php echo $post->autor_nombre; ?></li>
                            <li><a href="<?php echo site_url('articulos/categoria/'.$post->categoria_slug); ?>"><i class="fa fa-folder"></i><?php echo $post->categoria_nombre; ?></a></li>
                            <li><i class="fa fa-calendar"></i><?php echo mysql_to_date($post->fecha); ?></li>
                        </ul>
                        <div class="blog-img">
                            <img class="img-responsive" src="<?php echo base_url('uploads/posts/' . $post->imagen); ?>" alt="">
                        </div>
                        <?php echo $post->contenido; ?>
                    </div>
                    <div class="addthis_sharing_toolbox"></div>
                </div>
                <div class="col-sm-3">
                    <?php echo $sidebar; ?>
                </div>
            </div>            
      
    </div>
</section>