<section class="pageBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner_content text-center">
                    <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                    <h2>Biblioteca</h2>
                </div>
            </div>
        </div>
    </div>
</section>








<section class="commonSection porfolioDetail">
            <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row m-b-20">
                        <div class="col-md-3">
                            <div class="thumb-portada">
                                <img class="img-responsive" src="<?php echo base_url('uploads/biblioteca/' . $libro->portada); ?>" alt="">
                            </div>
                            <br>
                            <div class="addthis_sharing_toolbox"></div>
                        </div>
                        <div class="col-sm-9">
                            <div class="news-v3">
                                <h2><?php echo $libro->titulo; ?></h2>
                                <table class="table m-t-20">
                                    <tr>
                                        <td class="col-sm-1"><strong>Autor(es): </strong></td>
                                        <td><?php echo $libro->autor; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-1"><strong>Editorial: </strong></td>
                                        <td><?php echo $libro->editorial; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-1"><strong>Año de publicación: </strong></td>
                                        <td><?php echo $libro->anio_publicacion; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-1"><strong>Edición: </strong></td>
                                        <td><?php echo $libro->edicion; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-1"><strong>Idioma: </strong></td>
                                        <td><?php echo $libro->idioma; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-1"><strong>Descripción: </strong></td>
                                        <td><?php echo nl2br($libro->descripcion); ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-1"><strong>Descargas: </strong></td>
                                        <td><?php echo $libro->descargas ?></td>
                                    </tr>
                                    <?php if ($libro->archivo): ?>
                                        <tr>
                                            <td <?= $libro->archivo2 ? '' : 'colspan="2"' ?> class="text-center">
                                                <a href="<?php echo base_url('download/' . $libro->archivo.'/'.$libro->id); ?>" class="common_btn red_bg">
                                                    <i class="fa fa-download"></i>&nbsp;<span>Descargar</span>
                                                </a>
                                            </td>
                                            <?php if ($libro->archivo2): ?>
                                                <td class="text-center">
                                                    <a href="<?= $libro->archivo2 ?>" class="common_btn red_bg" target="_blank">
                                                        <i class="fa fa-eye"></i>&nbsp;Ver Flipbook
                                                    </a>
                                                </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endif; ?>

                                    <?php if ($libro->archivo_epub OR $libro->archivo_ebook OR $libro->archivo_mobi): ?>
                                        <tr>
                                            <td colspan="2" class="text-center">
                                                <strong>Libros Digitales</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="table">
                                                    <tr>
                                                        <?php if ($libro->archivo_epub): ?>
                                                            <td class="text-center">
                                                                <a href="<?= base_url('download/'.$libro->archivo_epub.'/'.$libro->id)?>" class="common_btn red_bg">
                                                                    <i class="fa fa-file-pdf-o"></i>&nbsp;EPUB
                                                                </a>
                                                            </td>            
                                                        <?php endif; ?>
                                                        <?php if ($libro->archivo_ebook): ?>
                                                            <td class="text-center">
                                                                <a href="<?= base_url('download/'.$libro->archivo_ebook.'/'.$libro->id)?>" class="common_btn red_bg">
                                                                    <i class="fa fa-file-pdf-o"></i>&nbsp;PDF
                                                                </a>
                                                            </td>
                                                        <?php endif; ?>
                                                        <?php if ($libro->archivo_mobi): ?>
                                                            <td class="text-center">
                                                                <a href="<?= base_url('download/'.$libro->archivo_mobi.'/'.$libro->id)?>" class="common_btn red_bg">
                                                                    <i class="fa fa-file-pdf-o"></i>&nbsp;MOBI
                                                                </a>
                                                            </td>
                                                        <?php endif; ?>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    <?php endif; ?>


                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
       
    </div>
</div>




