
 <!-- Page Banner -->
    <section class="pageBanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <h4><a href="<?php echo site_url(); ?>">Inicio</a> - Eventos</h4>
                        <h2>Eventos</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
 <!-- Page Banner -->

 <div class="container" style="margin-top: 50px;">
     <div class="row">
         <div class="col-lg-4 col-sm-5 col-md-4">
             <h2 class="sec_title">Eventos</h2>
         </div>
         <div class="col-lg-8 col-sm-7 col-md-8">
             <div class="subscribefrom">
                 <input type="text" name="email" id="sandbox-container" value=" <?php  setlocale(LC_ALL, 'es_ES'); echo  strftime(' %B / %Y') ?>"  >
                 <button class="common_btn red_bg" type="button"><span> <i class="fa fa-calendar" aria-hidden="true"></i> Buscar</span></button>
             </div>


         </div>
     </div>
 </div>



   <!-- Blog Section -->
        <section class="commonSection blogPage">
            <div class="container">
                <div class="row" id="lista_eventos">
                   
                </div>
            </div>
        </section>
        <!-- Blog Section -->