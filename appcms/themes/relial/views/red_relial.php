<div class="breadcrumbs">
    <div class="container">
        <?php echo $this->breadcrumbs->show(); ?>
    </div><!--/container-->
</div>

<div class="content-section m-b-30">
    <div class="container">
        <div class="padding">
            <div class="row m-b-30">
                <div class="col-sm-9">
                    <h1 class="content-title">
                        <i class="fa fa-caret-right"></i>&nbsp;La Red RELIAL de América Latina
                    </h1>
                    <div id="world-map" style="width: 580px; height: 800px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalRed" tabindex="-1" role="dialog" aria-labelledby="modalRedLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalRedLabel"></h4>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>
<script>

var regions = JSON.parse('<?= $regiones ?>');
var red = JSON.parse('<?= $instituciones ?>');

</script>