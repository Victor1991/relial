
<!--
<div id="about-us" class="parallax-section">
<div class="overlay-bg"></div>
<div class="container">
<div class="padding">
<div class="row">
<div class="col-sm-1 ">
<img src="<?php echo theme_assets('images/map1.png'); ?>" class="img-responsive"></div>
<div class="col-sm-5">
<h2 class="title-section">RELIAL</h2>
<?= $inicio->texto ?>

<br><br><br>

<i><blockquote style="font-size: 14px; font-weight: 600">
"<?php echo $frase->texto; ?>"
</blockquote></i>
<h5>— <?php echo $frase->autor; ?></h5>


</div>
<div class="col-sm-6">


<img src="<?php echo theme_assets('images/others/redrelial.png'); ?>" class="img-responsive">

</div>
</div>
</div>
</div>
</div>



<div id="eventos-section" class="parallax-section">
<div class="container">
<div class="row">

<div class="col-md-12 section_video">
<h3 class="subtitle-section"><i class="fa fa-caret-right"></i>YouTube</h3>
<div id="relialVideoList"></div>
<br>
<br>
<br>
</div>

</div>
</div>
</div>





<div id="primary-article">
<div class="container">
<div class="padding">
<h2 class="title-section"><i class="fa fa-caret-right"></i>Actualidad y opinión</h2>
<div class="padding-middle">
<div class="row">
<?php if (count($posts)): ?>
    <div class="col-sm-4">
    <div class="entry-media">
    <?php if ($posts[0]->imagen): ?>
        <img src="<?php echo base_url('uploads/posts/' . $posts[0]->imagen); ?>" class="img-responsive">
    <?php else: ?>
        <img src="http://placehold.it/740x400" class="img-responsive">
    <?php endif; ?>
    </div>
    </div>
    <div class="col-sm-8">
    <div class="entry entry-padding">
    <h3 class="entry-title">
    <a href="<?php echo base_url('articulos/' . $posts[0]->slug); ?>">
    <i class="fa fa-caret-right"></i><?php echo $posts[0]->titulo ?>
    </a>
    </h3>
    <div class="entry-excerpt">
    <p><?php echo nl2br($posts[0]->resumen); ?></p>
    </div>
    <div class="entry-link">
    <a href="<?php echo base_url('articulos/' . $posts[0]->slug); ?>">
    <i class="fa fa-caret-right"></i>Leer más
    </a>
    </div>
    </div>
    </div>
<?php endif; ?>
</div>
</div>
</div>
</div>
</div>

<div id="secondary-articles">
<div class="container">
<div class="padding-middle">
<div class="row">
<?php if (count($posts) > 1): ?>
    <?php for ($i = 1; $i < count($posts) && $i < 4; $i++): ?>
        <div class="col-sm-4" style="border-right: 1px solid rgb(0, 0, 0);">
        <img src="<?= base_url('uploads/posts/' . $posts[$i]->imagen) ?>" alt="" class="img-thumbnail">
        <div class="entry">
        <h3 class="entry-title">
        <a href="<?php echo base_url('articulos/' . $posts[$i]->slug); ?>">
        <i class="fa fa-caret-right"></i><?php echo $posts[$i]->titulo ?>
        </a>
        </h3>
        <div class="entry-excerpt">
        <p><?php echo nl2br($posts[$i]->resumen); ?></p>
        </div>
        <div class="entry-link">
        <a href="<?php echo base_url('articulos/' . $posts[$i]->slug); ?>">
        <i class="fa fa-caret-right"></i>Leer más
        </a>
        </div>
        </div>
        </div>
    <?php endfor; ?>
<?php endif; ?>
</div>
</div>
</div>
</div>

<div id="eventos-section" class="parallax-section">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-6" style=" height: 573px;" >
<div class="padding-middle">
<h2 class="title-section"><i class="fa fa-caret-right"></i>Eventos</h2>
<div align="center">
<div id="events-carousel" class="carousel slide" data-ride="carousel">

<div class="carousel-inner" role="listbox">
<?php
$first = true;
foreach ($eventos as $evento):
    ?>
    <div class="item <?php echo $first ? 'active' : '' ?>">
    <a href="<?php echo site_url('articulos/' . $evento->slug); ?>">
    <?php if ($evento->imagen): ?>
        <img class="img-responsive" src="<?php echo base_url('uploads/posts/' . $evento->imagen); ?>" alt="Imagen" >
    <?php else: ?>
        <img class="img-responsive" src="http://placehold.it/400x450" alt="Imagen" >
    <?php endif; ?>
    <div class="carousel-caption"><?php echo $evento->titulo ?></div>
    </a>
    </div>
    <?php
    $first = false;
endforeach;
?>
</div>



<a class="left carousel-control" href="#events-carousel" role="button" data-slide="prev">
<i class="fa fa-chevron-left" aria-hidden="true"></i>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#events-carousel" role="button" data-slide="next">
<i class="fa fa-chevron-right" aria-hidden="true"></i>
<span class="sr-only">Next</span>
</a>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>


<div class="col-sm-12 col-md-6 " style=" height: 573px; padding: 40px 20px;" >
<div class="book-box">
<div class="book-portada">
<img src="<?php echo base_url('uploads/biblioteca/' . $libro->portada); ?>" class="img-responsive">
</div>
<div class="book-info-link text-right">
<a href="<?php echo site_url('/biblioteca/libro/' . $libro->slug); ?>">Ver más</a>
</div>
</div>
</div>
</div>
</div>

</div>





<div id="social-widgets">
<div class="container">
<div class="padding-middle">
<h2 class="title-section"><i class="fa fa-caret-right"></i>Redes sociales</h2>
<div class="plans">
<div class="row">
<div class="col-sm-6">
<h3 class="subtitle-section"><i class="fa fa-caret-right"></i>Facebook</h3>
<div id="fb-container">
<div class="fb-page" data-href="https://www.facebook.com/relial.red/" data-tabs="timeline" data-height="400"  data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/relial.red/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/relial.red/">Red Liberal de América Latina - Relial</a></blockquote></div>
</div>
<div id="fb-root"></div>

<script>(function (d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id))
return;
js = d.createElement(s);
js.id = id;
js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</div>
<div class="col-sm-6">
<h3 class="subtitle-section"><i class="fa fa-caret-right"></i>Twitter</h3>
<a class="twitter-timeline" data-height="500" href="https://twitter.com/RELIALred">Tweets by RELIALred</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>
<br>


</div>
</div>
</div>
</div>
</div>


<div id="brands">
<div class="container">
<div class="padding-middle text-center">
<div class="row">
<div class="col-sm-12">
<div id="brands-carousel" class="carousel slide">

<div class="carousel-inner">
<?php for ($i = 0; $i < count($aliados); $i++): ?>
    <?php if ($i == 0 || !($i % 4)): ?>
        <div class="item <?php echo ($i == 0) ? 'active' : ''; ?>">
        <div class="row">
    <?php endif; ?>
    <div class="col-sm-3">
    <a href="<?php echo $aliados[$i]->link ?>" <?php echo $aliados[$i]->nueva_ventana ? 'target="_blank"' : ''; ?> class="thumbnail">
    <img src="<?php echo base_url('uploads/banners/' . $aliados[$i]->imagen) ?>" alt="<?php echo $aliados[$i]->titulo; ?>" style="max-width:100%;" />
    </a>
    </div>
    <?php if (!(($i + 1) % 4) || $i == count($aliados) - 1): ?>
        </div>
        </div>
    <?php endif; ?>
<?php endfor; ?>

</div>

<a data-slide="prev" href="#brands-carousel" class="left carousel-control">
<i class="fa fa-chevron-left"></i>
</a>
<a data-slide="next" href="#brands-carousel" class="right carousel-control">
<i class="fa fa-chevron-right"></i>
</a>
</div>
</div>
<div class="clearfix">
</div>
<br>
<div class="col-sm-12">
<a type="button" href="<?= base_url('/directorio') ?>" class="btn btn-primary">Ver más</a>
</div>
</div>
</div>
</div>
</div>

<hr>
<div id="pictures-gallery">
<div class="container">
<div class="padding-middle text-center">
<div class="row">
<div class="col-sm-12 alt-carousel">
<div id="pictures-carousel" class="carousel slide">

<div class="carousel-inner">
<?php for ($i = 0; $i < count($galerias); $i++): ?>
    <?php if ($i == 0 || !($i % 3)): ?>
        <div class="item <?php echo ($i == 0) ? 'active' : ''; ?>">
        <div class="row">
    <?php endif; ?>
    <div class="col-md-4">
    <div class="portfolio-item">
    <div class="portfolio-img" style="background-image: url(<?php echo base_url('uploads/galerias/' . $galerias[$i]->archivo); ?>)">
    </div>
    <div class="info">
    <h3>
    <a href="<?php echo base_url('galerias/fotos/' . $galerias[$i]->id); ?>"><?php echo $galerias[$i]->titulo ?></a>
    </h3>
    </div>
    </div>
    </div>
    <?php if (!(($i + 1) % 3) || $i == count($galerias) - 1): ?>
        </div>
        </div>
    <?php endif; ?>
<?php endfor; ?>
</div>

<a data-slide="prev" href="#pictures-carousel" class="left carousel-control" style="font-size: 60px">
<i class="fa fa-caret-left"></i>
</a>
<a data-slide="next" href="#pictures-carousel" class="right carousel-control" style="font-size: 60px">
<i class="fa fa-caret-right"></i>
</a>
</div>
</div>
</div>
</div>
</div>
</div>

<hr>

<div id="contact-us" class="product-details-grid">
<div class="product-details part-contact">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-6">
<div class="contact-info">
<h2 class="title-section"><i class="fa fa-caret-right"></i>Contacto</h2>
<?= $informacion->texto ?>
</div>
</div>
</div>
</div>
<div class="product-image">
<img class="img-responsive" src="<?php echo base_url('uploads/banners/' . $this->banner_header->imagen); ?>">
</div>
</div>
</div>
-->







<section class="rev_slider slider_2">
    <div class="rev_slider_wrapper">
        <div id="rev_slider_2" class="rev_slider" data-version="5.4.5">
            <ul>
                <!-- MINIMUM SLIDE STRUCTURE -->
                <li data-transition="random" data-masterspeed="1000">
                    <!-- SLIDE'S MAIN BACKGROUND IMAGE -->
                    <img src="<?php echo base_url('uploads/banners/' . $this->banner_header->imagen); ?>" alt="Sky" class="rev-slidebg">

                    <div class="tp-caption  normalWraping layer_2"

                         data-frames='[{"delay":1600,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},
                         {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'

                         data-x="left"
                         data-y="center"
                         data-width="600px"
                         data-hoffset="['0', '100', '120', '0']"
                         data-voffset="['8', '0', '0', '0']"

                         data-textAlign="['left', 'left', 'left', 'center']"
                         >

                        <img src="<?php echo theme_assets('images/logohome.png'); ?>" class="img-responsive" >


                    </div>

                </li>
                <li data-transition="random" data-masterspeed="1000">
                    <!-- SLIDE'S MAIN BACKGROUND IMAGE -->
                    <img src="<?php echo base_url('uploads/banners/' . $this->banner_header->imagen); ?>" alt="Sky" class="rev-slidebg">

                    <div class="tp-caption tp-resizeme normalWraping layer_2"

                         data-frames='[{"delay":1600,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},
                         {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'

                         data-x="left"
                         data-y="center"
                         data-hoffset="['0', '100', '120', '0']"
                         data-voffset="['8', '0', '0', '0']"
                         data-width="100%"
                         data-height="['auto]"
                         data-whitesapce="['normal']"
                         data-word-wrap="['normal']"
                         data-white-break="['break-all']"
                         data-fontsize="['110', '80', '70', '40']"
                         data-lineheight="['112', '90', '80', '44']"
                         data-fontweight="700"
                         data-letterspacing="['4.4', '4.4', '4.4', '2']"
                         data-color="#000"
                         data-textAlign="['left', 'left', 'left', 'center']"
                         ><img src="<?php echo theme_assets('images/logohome.png'); ?>" class="img-responsive" ></div>

                </li>

            </ul><!-- END SLIDES LIST -->
        </div>
    </div>
</section>
<!-- Revolution Slider -->

<!-- Services Section -->
<section class="service_section commonSection" style="background-color: #686868;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="sub_title" style="color:#000;">  <?php echo $frase->autor; ?></h4>
                <h2 class="sec_title white">  <?php echo $frase->texto; ?></h2>

            </div>
        </div>


        <!--
        <div class="row custom_column">
        <div class="col-lg-3 col-sm-4 col-md-3">
        <a href="service_detail.html" class="icon_box_1 text-center">
        <div class="flipper">
        <div class="front">
        <i class="mei-web-design"></i>
        <h3>Website Development</h3>
   </div>
   <div class="back">
   <i class="mei-web-design"></i>
   <h3>Website Development</h3>
</div>
</div>
</a>
</div>
<div class="col-lg-3 col-sm-4 col-md-3">
<a href="service_detail.html" class="icon_box_1 text-center">
<div class="flipper">
<div class="front">
<i class="mei-computer-graphic"></i>
<h3>Graphic Designing</h3>
</div>
<div class="back">
<i class="mei-computer-graphic"></i>
<h3>Graphic Designing</h3>
</div>
</div>
</a>
</div>
<div class="col-lg-3 col-sm-4 col-md-3">
<a href="service_detail.html" class="icon_box_1 text-center">
<div class="flipper">
<div class="front">
<i class="mei-development-1"></i>
<h3>Digital Marketing</h3>
</div>
<div class="back">
<i class="mei-development-1"></i>
<h3>Digital Marketing</h3>
</div>
</div>
</a>
</div>
<div class="col-lg-3 col-sm-4 col-md-3">
<a href="service_detail.html" class="icon_box_1 text-center">
<div class="flipper">
<div class="front">
<i class="mei-development"></i>
<h3>SEo & Content Writing</h3>
</div>
<div class="back">
<i class="mei-development"></i>
<h3>SEo & Content Writing</h3>
</div>
</div>
</a>
</div>
<div class="col-lg-3 col-sm-4 col-md-3">
<a href="service_detail.html" class="icon_box_1 text-center">
<div class="flipper">
<div class="front">
<i class="mei-app-development"></i>
<h3>App Development</h3>
</div>
<div class="back">
<i class="mei-app-development"></i>
<h3>App Development</h3>
</div>
</div>
</a>
</div>
        -->

    </div>
</div>
</section>
<!-- Services Section -->

<!-- About Agency Section -->
<section class="commonSection ab_agency">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 PR_79">

                <h2 class="sec_title MB_45">RELIAL</h2>
                <p class="sec_desc">
                    <?= $inicio->texto ?>
                </p>
                <a class="common_btn red_bg" href="<?php echo site_url('/sitio/acerca-de-relial'); ?>"><span>Leer más</span></a>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6">
                <div class="ab_img1">
                    <img src="<?php echo theme_assets('2020/images/triangulo1.png'); ?>" alt=""/>
                </div>
                <div class="ab_img2">
                    <img src="<?php echo theme_assets('2020/images/triangulo2.png'); ?>" alt=""/>
                </div>
            </div>
        </div>
    </div>
</section>





<section class="commonSection funfact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 noPadding BR">
                <div class="singlefunfact text-center" style="padding-top: 163px;">
                    <h2 class="sec_title" >Actualidad y opinión</h2>

                </div>
            </div>

        </div>
    </div>
</section>
<!-- FunFact Section -->






<?php if (count($posts)): ?>
    <!-- Trust Clients Section -->
    <section class="commonSection trustClient" style=" background-color:#e2e2e2; ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="CL_content">
                        <?php if ($posts[0]->imagen): ?>
                            <img src="<?php echo base_url('uploads/posts/' . $posts[0]->imagen); ?>" class="img-responsive">
                        <?php else: ?>
                            <img src="http://placehold.it/740x400" class="img-responsive">
                        <?php endif; ?>
                        <div class="abc_inner" style="margin-top: -350px;">
                            <div class="row">
                                <div class="col-lg-5 col-sm-5 col-md-5">
                                </div>
                                <div class="col-lg-7 col-sm-7 col-md-7">
                                    <div class="abci_content">
                                        <h2><?php echo $posts[0]->titulo ?></h2>
                                        <p>
                                            <?php echo nl2br($posts[0]->resumen); ?>
                                        </p>
                                        <a class="common_btn red_bg" href="<?php echo base_url('articulos/' . $posts[0]->slug); ?>"><span>Leer más</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>










<section class="commonSection blogPage" style=" background-color:#e2e2e2; background: url(<?php echo theme_assets('images/96.png'); ?>) no-repeat center center / cover;">






    <div class="container">
        <div class="row">

            <?php if (count($posts) > 1): ?>
                <?php for ($i = 1; $i < count($posts) && $i < 4; $i++): ?>


                    <div class="col-lg-4 col-sm-6 col-md-4">
                        <div class="latestBlogItem">
                            <div class="lbi_thumb">
                                <img src="<?= base_url('uploads/posts/' . $posts[$i]->imagen) ?>" alt="">
                            </div>
                            <div class="lbi_details">
                                <a href="<?php echo base_url('articulos/' . $posts[$i]->slug); ?>" class="lbid_date">26 NOV</a>
                                <h2><a href="<?php echo base_url('articulos/' . $posts[$i]->slug); ?>"><?php echo $posts[$i]->titulo ?></a></h2>
                                <a class="learnM" href="<?php echo base_url('articulos/' . $posts[$i]->slug); ?>">Ler más</a>
                            </div>
                        </div>
                    </div>

                <?php endfor; ?>
            <?php endif; ?>


        </div>

    </div>
</section>
















<section class="commonSection client">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="sub_title">Nuestra red de</h4>
                <h2 class="sec_title">Organizaciones</h2>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="client_slider">





                    <?php for ($i = 0; $i < count($aliados); $i++): ?>


                        <div class="singleClient">
                            <a href="<?php echo $aliados[$i]->link ?>" <?php echo $aliados[$i]->nueva_ventana ? 'target="_blank"' : ''; ?>>
                                <img src="<?php echo base_url('uploads/banners/' . $aliados[$i]->imagen) ?>" alt="<?php echo $aliados[$i]->titulo; ?>"/>
                            </a>
                        </div>


                    <?php endfor; ?>








                </div>

                <a class="common_btn red_bg" href="<?php echo base_url('/directorio'); ?>"><span>Ver directorio</span></a>
            </div>
        </div>
    </div>
</section>

<section class="commonSection ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="sub_title">En youtube</h4>
                <h2 class="sec_title">Multimedia</h2>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-12">

                <!-- <div class="col-md-12 section_video">
                <h3 class="subtitle-section"><i class="fa fa-caret-right"></i>YouTube</h3>
                <div id="relialVideoList" style=" width: 100%; height: 500px;"></div>
           </div> -->


                <div class="videoWrap">
                    <div class="row">
                        <div class="col-md-12">


                            <div class="embed-container">
                                <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/EZv0mipBYJw" frameborder="0" allowfullscreen></iframe>
                            </div>

                        </div>

                    </div>

                </div>

                <style media="screen">
                    .embed-container {
                        position: relative;
                        padding-bottom: 56.25%;
                        height: 0;
                        overflow: hidden;
                    }
                    .embed-container iframe {
                        position: absolute;
                        top:0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                    }
                </style>


            </div>
        </div>



        <div class="row custom_column">
            <div class="col-lg-3 col-sm-3 col-md-3">
                <a href="<?php echo base_url('biblioteca'); ?>" class="icon_box_1 text-center">
                    <div class="flipper">
                        <div class="front">
                            <i class="mei-web-design"></i>
                            <h3>Biblioteca</h3>
                        </div>
                        <div class="back">
                            <i class="mei-web-design"></i>
                            <h3>Biblioteca</h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3">
                <a href="<?php echo base_url('galerias'); ?>" class="icon_box_1 text-center">
                    <div class="flipper">
                        <div class="front">
                            <i class="mei-computer-graphic"></i>
                            <h3>Galerías</h3>
                        </div>
                        <div class="back">
                            <i class="mei-computer-graphic"></i>
                            <h3>Galerias</h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3">
                <a href="<?php echo base_url('videos'); ?>" class="icon_box_1 text-center">
                    <div class="flipper">
                        <div class="front">
                            <i class="mei-development-1"></i>
                            <h3>Videos</h3>
                        </div>
                        <div class="back">
                            <i class="mei-development-1"></i>
                            <h3>Videos</h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3">
                <a href="<?php echo base_url('articulos'); ?>" class="icon_box_1 text-center">
                    <div class="flipper">
                        <div class="front">
                            <i class="mei-development"></i>
                            <h3>Artículos</h3>
                        </div>
                        <div class="back">
                            <i class="mei-development"></i>
                            <h3>Artículos</h3>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-sm-3 col-md-3">
                <a href="<?php echo base_url('eventos'); ?>" class="icon_box_1 text-center">
                    <div class="flipper">
                        <div class="front">
                            <i class=" mei-production"></i>
                            <h3>Eventos</h3>
                        </div>
                        <div class="back">
                            <i class="mei-production"></i>
                            <h3>Eventos</h3>
                        </div>
                    </div>
                </a>
            </div>





        </div>


    </div>
</section>


<section class="commonSection testimonial">
    <div class="container">
        <div class="row">
            <div class="col-lg-4  text-center">
                <div class="testimonial_content">
                    <div class="testi_icon">
                        <img src="<?php echo base_url('uploads/biblioteca/' . $libro->portada); ?>" class="img-responsive">
                    </div>
                </div>
            </div>

            <div class="col-lg-8 text-left">
                <div class="testimonial_content">
                    <h2><?php echo $libro->titulo; ?></h2>
                    <a href="<?php echo site_url('/biblioteca/libro/' . $libro->slug); ?>" class="common_btn red_bg"><span>Ver más</span></a>
                </div>
            </div>

        </div>
    </div>
</section>


<section class="commonSection  " style="padding: 30px 0px 0px;">
    <div class="container">

        <div class="row"  id="calendario" >
            <div class="col-lg-4 col-sm-5 col-md-12">
                <h4 class="sub_title">Mantente informado</h4>
                <h2 class="sec_title">Suscríbete</h2>
            </div>
            <div class="col-md-12" style="padding: 0px 0px 30px;">
                <form action="" method="post" style="display: flex;" id="subcripcion" class="subscribefrom">
                    <input style="width: 100%;" type="email" placeholder="Ingresa tu correo electrónico" name="email">
                    <button style="padding: 15px 42.5px; line-height: 20px;" class="common_btn red_bg" type="submit" name="submit"><span>Suscríbete ahora</span></button>
                </form>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12" >
                <div id="calendar" class="has-toolbar" style="line-height: 18px;"></div>
            </div>
        </div>
    </div>

</section>
<section class="commonSectio " style="padding: 0px 0;">

    <div class="container">




        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="background-color:#9f1a15;">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 style="color:#fff;" id="tit_mod"> </h3>
                            </div>
                            <div class="col-md-12" style="background-color:#dadada; padding-bottom: 9px; ">
                                <br>
                                <img id="img_mod" src="" class="img-responsive" alt="" style="margin:0 auto;">
                                <hr>
                                <p id="desc_mod" style="color:#000;"></p>
                            </div>
                            <div class="col-md-12" style="text-align:right; padding-top: 9px;">
                                <button type="button" style="background-color: #000000; border-color:#000000; " class="btn btn-info btn-lg" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  -->
        <br>
    </div>
</section>
