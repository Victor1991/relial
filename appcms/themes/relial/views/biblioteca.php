<section class="pageBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner_content text-center">
                    <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                    <h2>Biblioteca</h2>
                </div>
            </div>
        </div>
    </div>
</section>





<section class="commonSection porfolio">
            <div class="container">

                 <div class="row">
                      <div class="col-12 col-md-12">
                         <form action="<?=base_url('biblioteca/buscar_categoria')?>" method="get">
                                <div class="input-group">
                                     <div class="form-group  has-feedback">
                                          <input type="text" class="input-form" style="margin: 0 0px -15px;" name="search" id="inputSuccess5">
                                          <span class="fa fa-search form-control-feedback" aria-hidden="true"></span>
                                     </div>
                                     <span class="input-group-btn">
                                          <button class="common_btn red_bg" type="submit">Buscar categoria !</button>
                                     </span>
                                </div>
                         </form>
                      </div>

                 </div>
          <br>

                <div class="row">


                    <?php foreach ($categorias as $categoria): ?>
                    <!--
                    <div class="col-lg-4 col-sm-6 col-md-4">
                        <div class="singlefolio">
                            <img src="images/portfolio/1.jpg" alt=""/>
                            <div class="folioHover">

                                <h4><a href="<?php echo site_url('biblioteca/categoria/'.$categoria->slug); ?>"><?php echo $categoria->nombre ?></a></h4>
                            </div>
                        </div>
                    </div>
                    -->
                    <?php //echo _dump($categoria); ?>

                    <!--<div class="col-lg-4 col-sm-6 col-md-4" style="padding-bottom: 10px;">
                        <div class="icon_box_2 text-center" >
                            <a href="<?php echo site_url('biblioteca/categoria/'.$categoria->slug); ?>">
                            <h3 style="color: #ff0000"><?php echo $categoria->nombre ?></h3>
                            </a>

                            <div class="iconWrap">
                                <a href="<?php echo site_url('biblioteca/categoria/'.$categoria->slug); ?>"> <i class="mei-menu"></i></a>
                            </div>
                            <a href="<?php echo site_url('biblioteca/categoria/'.$categoria->slug); ?>">Ver documentos disponibles</a>
                        </div>
                    </div>
                    -->



                    <div class="col-lg-4 col-sm-6 col-md-4 mix marketing graphic">
                            <div class="singlefolio">
                                <img src="<?php echo theme_assets('2020/images/'.$categoria->id.'.jpg'); ?>" alt=""/>
                                <div class="folioHover">
                                    <a class="cate" href="<?php echo site_url('biblioteca/categoria/'.$categoria->slug); ?>"><?php echo $categoria->nombre ?></a>
                                    <h4><a href="<?php echo site_url('biblioteca/categoria/'.$categoria->slug); ?>">Ver documentos</a></h4>
                                </div>
                            </div>
                        </div>
                     <?php endforeach; ?>



                </div>
            </div>
        </section>
