<section class="pageBanner">
     <div class="container">
          <div class="row">
               <div class="col-lg-12">
                    <div class="banner_content text-center">
                         <h4><?php echo $this->breadcrumbs->show(); ?></h4>
                         <h2>Biblioteca</h2>
                    </div>
               </div>
          </div>
     </div>
</section>






<section class="commonSection porfolioDetail">
     <div class="container">

          <div class="row" style="text-align:center">
               <h1>Libro buscado :
                    <br>
                    <?=$buscar?>
               </h1>
          </div>
          <br>
          <br>
          <br>
          <hr>


          <?php if (count($categoria->libros) > 0): ?>
               <?php foreach($categoria->libros as $libro): ?>
                    <div class="row" style="border-bottom: 1px solid #e7e7e7; margin-bottom: 20px;">
                         <div class="col-lg-3 col-sm-6 col-md-3">
                              <div class="portDetailThumb">
                                   <img src="<?php echo base_url('uploads/biblioteca/' . $libro->portada); ?>" alt="" class=" img-responsive"/>
                              </div>

                         </div>
                         <div class="col-lg-9 col-sm-6 col-md-9">
                              <div class="singlePortfoio_content">
                                   <h3 style="line-height: 1;"><?php echo $libro->titulo; ?></h3>
                                   <p>
                                        <?php echo $libro->descripcion; ?>
                                   </p>

                                   <a class="common_btn red_bg" href="<?php echo site_url('biblioteca/libro/'.$libro->slug); ?>"><span>Leer más</span></a>
                              </div>

                         </div>
                    </div>


               <?php endforeach; ?>
          <?php else: ?>
               <p class="text-center m-b-20 m-t-20">No hay libros</p>
          <?php endif; ?>

     </div>
</section>
