<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>RELIAL Red Liberal de America Latina - Relial</title>
        <meta name="viewport" content="width=device-width">

        <!-- Include All CSS here-->
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/bootstrap.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/owl.carousel.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/owl.theme.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/font-awesome.min.cs'); ?>s"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/animate.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/magnific-popup.css'); ?>"/>
         <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/settings.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/slick.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/icons.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/preset.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/theme.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/responsive.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('2020/css/presets/color1.css'); ?>" id="colorChange"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" id="colorChange"/>
        <!-- End Include All CSS -->

        <!-- Favicon Icon -->

         <link rel="icon" href="<?php echo theme_assets('images/ico/favicon.ico'); ?>">

            <!-- custom css -->
        <?php if (isset($this->assets['css'])): ?>
            <?php foreach ($this->assets['css'] as $css) : ?>
                <link href="<?php echo theme_assets(trim($css, '/')); ?>" type="text/css" rel="stylesheet">
            <?php endforeach; ?>
        <?php endif; ?>

    </head>
    <body >
        <!-- Preloading -->
        <div class="preloader text-center">
            <div class="la-ball-circus la-2x">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- Preloading -->

        <!-- Header 01 -->
        <header class="header_01" id="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-sm-3 col-md-3">
                        <div class="logo">
                            <a href="<?php echo site_url(); ?>"><img src="<?php echo theme_assets('images/logon.png'); ?>" alt="RELIAL"/></a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-7 col-md-7">
                        <nav class="mainmenu text-center">
                            <ul>







                                <?php foreach ($menu_links as $link): ?>
                            <?php if (count($link['children'])): ?>
                                <li class="menu-item-has-children">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-right"></i><?php echo $link['titulo'] ?> <b class="caret"></b></a>
                                     <ul class="sub-menu">
                                        <?php foreach ($link['children'] as $child): ?>
                                            <?php if (count($child['children'])): ?>
                                                <li class="menu-item-has-children">
                                                    <a tabindex="-1" href="#"><?php echo $child['titulo'] ?> <b class="caret"></b></a>
                                                     <ul class="sub-menu">
                                                        <?php foreach ($child['children'] as $value): ?>
                                                            <li><a href="<?php echo $value['url'] ?>" tabindex="-1"  <?= $value['target'] ? ' target="_blank" ' : ''; ?>  ><?php echo $value['titulo'] ?></a></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                            <?php else: ?>
                                                <li><a href="<?php echo $child['url'] ?>"><?php echo $child['titulo'] ?></a></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                 <li class="menu-item-has-children">
                                    <a href="<?php echo $link['url'] ?>"><i class="fa fa-caret-right"></i><?php echo $link['titulo'] ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>








                                <!--

                                <li class="menu-item-has-children">
                                    <a href="#">home</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Home 01</a></li>
                                        <li><a href="index2.html">Home 02</a></li>
                                        <li><a href="index3.html">Home 03</a></li>
                                    </ul>
                                </li>
                                <li><a href="about.html">About</a></li>
                                <li class="menu-item-has-children"><a href="#">Services</a>
                                    <ul class="sub-menu">
                                        <li><a href="services.html">Service</a></li>
                                        <li><a href="service_detail.html">Service Detail</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Portfolio</a>
                                    <ul class="sub-menu">
                                        <li><a href="portfolio.html">Portfolio v1</a></li>
                                        <li><a href="portfolio2.html">Portfolio v2</a></li>
                                        <li><a href="portfolio_detail.html">Portfolio Detail</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Blog</a>
                                    <ul class="sub-menu">
                                        <li><a href="blog.html">Blog v1</a></li>
                                        <li><a href="blog2.html">Blog v2</a></li>
                                        <li><a href="blog3.html">Blog v3</a></li>
                                        <li><a href="blog_single.html">Blog Single</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Contact</a></li>
                                -->

                            </ul>
                        </nav>
                    </div>

                </div>
            </div>
        </header>





<!-- <!DOCTYPE html>
<html lang="es-MX">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>RELIAL Red Liberal de America Latina - Relial</title>


        <link href="<?php echo theme_assets('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/font-awesome.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/animation.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/magnific-popup.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/vegas.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/owl.carousel.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/main.css'); ?>" rel="stylesheet">
        <link href="<?php echo theme_assets('css/responsive.css'); ?>" rel="stylesheet">


        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,700,800,100,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,900' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Lora:400,400italic,700%7CMontserrat:400,700" rel="stylesheet" type="text/css">


        <link rel="shortcut icon" href="<?php echo theme_assets('images/ico/favicon.ico'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_assets('images/ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_assets('images/ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_assets('images/ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo theme_assets('images/ico/apple-touch-icon-57-precomposed.png'); ?>">

          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


        <?php if (isset($this->assets['css'])): ?>
            <?php foreach ($this->assets['css'] as $css) : ?>
                <link href="<?php echo theme_assets(trim($css, '/')); ?>" type="text/css" rel="stylesheet">
            <?php endforeach; ?>
        <?php endif; ?>
    </head>
    <body>





        <div id="main-slider-wrapper">
            <div id="home-section" style="background-image: url(<?php echo base_url('uploads/banners/' . $this->banner_header->imagen); ?>);">
                <div class="container main-slider">
                    <div class="slider-content">
                        <div class="row">
                            <div style=" position: relative; margin-bottom: 20px; margin-right: 20px; color: #b4b3b3; position: absolute;

                                 margin-top: 369px;

                                 margin-left: 80%;



                                 float: right;

                                 font-size: 12px;">
                                 <?php echo $this->banner_header->descripcion; ?>
                            </div>
                            <div class="col-sm-8 col-sm-offset-2 text-center">
                                <img class="logo-header img-responsive" src="<?php echo theme_assets('images/log.png'); ?>" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="navigation">
            <div class="navbar" role="banner">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://relial.org/">
                        <img class="img-responsive" src="<?php echo theme_assets('images/others/logomenu.png'); ?>" alt="Logotipo RELIAL">
                    </a>
                </div>
                <nav id="main-menu" class="collapse navbar-collapse" style="margin: 0 auto;">
                    <ul class="nav navbar-nav" style="margin-left: 2%">
                        <?php foreach ($menu_links as $link): ?>
                            <?php if (count($link['children'])): ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-right"></i><?php echo $link['titulo'] ?> <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                        <?php foreach ($link['children'] as $child): ?>
                                            <?php if (count($child['children'])): ?>
                                                <li class="dropdown-submenu">
                                                    <a tabindex="-1" href="#"><?php echo $child['titulo'] ?> <b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <?php foreach ($child['children'] as $value): ?>
                                                            <li><a href="<?php echo $value['url'] ?>" tabindex="-1"  <?= $value['target'] ? ' target="_blank" ' : ''; ?>  ><?php echo $value['titulo'] ?></a></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                            <?php else: ?>
                                                <li><a href="<?php echo $child['url'] ?>"><?php echo $child['titulo'] ?></a></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a href="<?php echo $link['url'] ?>"><i class="fa fa-caret-right"></i><?php echo $link['titulo'] ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>

                    </ul>
                </nav>
            </div>
        </div>

-->
